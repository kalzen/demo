<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'edit'=>'Edit',
    'delete'=>'Delete',
    'TYPICAL' => 'TYPICAL IMPROVEMENT PROJECT',
    'hello' => 'Hello',
    'manage.member'=>'Manage member information, check projects that your group manages, help our Kaizen system work more efficiently',
    'language'=>'Language',
    'slogan'=>'The official kaizen website of OURANSOFT Technology Joint Stock Company',
    'name'=>'Name',
    'department'=>'DEPARTMENT',
    'noproject'=>'No of PJ',
    'creatorofmonth'=>'CREATOR OF MONTH',
    'creatorofyear'=>'CREATOR OF YEAR',
    'infomation'=>'INFORMATION CATEGORY',
    'personal_page'=>'Personal page',
    'personal_page_description'=>'Manage personal management projects',
    'go'=>'Go',
    'project_review'=>'Project review',
    'project_review_description1'=>'Verify content and Browse satisfactory projects',
    'project_review_description2'=>'Project data management',
    'manage_member'=>'Manage member',
    'manage_member_description'=>'Manage member information',
    'system_management'=>'System management',
    'system_management_description'=>'Manage parameters of system Application and website',
    
    

];
