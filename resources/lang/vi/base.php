<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'edit'=>'Chỉnh sửa',
    'delete'=>'Xóa',
    'TYPICAL' => 'DỰ ÁN CẢI TIẾN TIÊU BIỂU',
    'hello' => 'Xin chào',
    'manage.member'=>'Quản lý thông tin thành viên, xét duyệt các đề án mà nhóm bạn quản lý, giúp hệ thống Kaizen của chúng ta hoạt động hiệu quả hơn',
    'language'=>'Ngôn ngữ',
    'slogan'=>'Trang web kaizen chính thức của Công ty OURANSOFT Technology Joint Stock',
    'name'=>'HỌ TÊN',
    'noproject'=>'SỐ ĐỀ ÁN',
    'creatorofmonth'=>'XẾP HẠNG TRONG THÁNG',
    'creatorofyear'=>'XẾP HẠNG TRONG NĂM',
    'infomation'=>'DANH MỤC THÔNG TIN',
    'personal_page'=>'Trang cá nhân',
    'personal_page_description'=>'Quản lý các đề án quản lý cá nhân',
    'go'=>'Đi đến',
    'project_review'=>'Xét duyệt đề án',
    'project_review_description1'=>'Xác nhận nội dung và Duyệt những đề án đạt yêu cầu',
    'project_review_description2'=>'Quản lý dữ liệu đề án',
    'manage_member'=>'Quản lý thành viên',
    'manage_member_description'=>'Quản lý thông tin thành viên',
    'system_management'=>'Quản lý hệ thống',
    'system_management_description'=>'Quản lý thông số của hệ thống Application và website',
];
