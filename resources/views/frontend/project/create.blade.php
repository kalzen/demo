@extends('frontend.layouts.admin')
@section('content')
<body class="page-body">
    <div class="row" style="margin:0px;">
        @include('frontend.project.sidebar')
        <div class="col-md-10">
            <div class="content project-content">
                <h4 style="margin-top:25px">NHẬP THÔNG TIN ĐỀ ÁN CẢI TIẾN MỚI</h4>
                <div class="form-create">
                    <form method="POST" action="{{route('frontend.project.store')}}" id="FrmCreateProject">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <div class="row" style="margin-bottom: 30px;">
                            <div class="notification-member col-md-6">
                                <div class="row" id='info_member' style="border: 1px solid;margin-left: 0px;border-radius: 4px;">
                                    <div class="col-md-3" style='padding-left: 0px'>
                                        <div class="img-member" style="line-height:162px;">
                                            <img src="{!!\Auth::guard('member')->user()->avatar!!}" style="width: 100%;">
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <h3>{!!\Auth::guard('member')->user()->full_name!!}</h3>
                                        <p>Mã nhân viên: <span>{!!\Auth::guard('member')->user()->login_id!!}</span></p>
                                        <p>Chức vụ: <span>{!!\Auth::guard('member')->user()->position->name!!}</span></p>
                                        <p>Bộ phận: <span>@if(\Auth::guard('member')->user()->part) {!!\Auth::guard('member')->user()->part->name!!} @endif</span></p>
                                        <p>Team: <span>@if(\Auth::guard('member')->user()->team) {!!\Auth::guard('member')->user()->team->name!!} @endif</span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="check_member" style="width:22px;height:22px;">
                                    <label class="form-check-label" for="check_member" style="margin: 4px 0px 0px 14px;">Trường hợp nhập cho người khác</label>
                                </div>
                                <div class="form-group row" style="margin-top: 25px;">
                                    <label for="inputPassword" class="col-sm-4 col-form-label">Chọn mã nhân viên</label>
                                    <div class="col-sm-6">
                                        <select class="form-control select-search choose-member" name="member_id" data-placeholder="Chọn mã nhân viên" disabled>
                                            {!!$member_html!!}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-2 col-form-label">TÊN ĐỀ TÀI</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="name" type="text" required=""> 
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-12 required control-label text-left text-semibold"><span class="orange">TRƯỚC</span> CẢI TIẾN: </label>
                            <div class="col-md-6">
                                <textarea class="form-control" id="before_content" name="before_content">{!!old('before_content')!!}</textarea>
                            </div>
                            <div class="col-md-6 div-image">
                                <div class="file-input file-input-ajax-new">
                                    <div class="input-group file-caption-main">
                                        <div class="input-group-btn input-group-append">
                                            <div tabindex="500" class="btn btn-primary btn-file"><span class="hidden-xs">Chọn ảnh</span>
                                                <input type="file" class="upload-images" multiple="multiple" name="before_upload[]" data-fouc="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="file-preview">
                                        <div class=" file-drop-zone" ondrop="dropHandler(event);"  ondragover="return false">
                         
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="before_images" class="image_data">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-12 required control-label text-left text-semibold"><span class="orange">SAU</span> CẢI TIẾN: </label>
                            <div class="col-md-6">
                                <textarea class="form-control" id="after_content" name="after_content">{!!old('after_content')!!}</textarea>
                            </div>
                            <div class="col-md-6 div-image">
                                <div class="file-input file-input-ajax-new">
                                    <div class="input-group file-caption-main">
                                        <div class="input-group-btn input-group-append">
                                            <div tabindex="500" class="btn btn-primary btn-file"><span class="hidden-xs">Chọn ảnh</span>
                                                <input type="file" class="upload-images" multiple="multiple" name="after_upload[]" data-fouc="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="file-preview ">
                                         <div class=" file-drop-zone" ondrop="dropHandler(event);"  ondragover="return false">
                         
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="after_images" class="image_data">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-check col-md-3" style="margin-left: 16px;">
                                <input type="checkbox" class="form-check-input" id="check_level" style="width:22px;height:22px;">
                                <label class="form-check-label" for="check_level" style="margin: 4px 0px 0px 14px;">XEM XÉT ĐỀ ÁN CẤP ĐỘ CAO</label>
                            </div>
                            <div class="form-group col-md-3">
                                <select class="form-control select-search choose-level" name="level" data-placeholder="Chọn cấp độ" disabled="">
                                    {!!$level_html!!}
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-check col-md-12" style="margin-left: 0px;padding-left:15px;">
                                <div class="radio">
                                    <label for="check_personal"><input type="radio" name="type" id="check_personal" value="1" checked style="width:22px;height:22px!important;"><span style="margin-bottom: 13px;position: absolute;left: 50px;">ĐỀ ÁN CÁ NHÂN</span></label>
                                </div>
                            </div>
                            <div class="form-check col-md-12" style="margin-left: 0px;padding-left:15px;">
                                <div class="radio">
                                    <label for="check_team"><input type="radio" name="type" id="check_team" value="2" style="width:22px;height:22px!important;"><span style="margin-bottom: 13px;position: absolute;left: 50px;">ĐỀ ÁN NHÓM</span></label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-submit" type="submit" style="background:#fd7700;"><img src="/assets2/img/send.png">  GỬI</button>
                            <button class="btn btn-submit" name='draft' value='1' style="background: #fff;border:1px solid #a6a5a5;" title="Nháp"><img style="width:24px;" src="/assets2/img/documents.png"></button>
                            <a href="{{route('frontend.project.index')}}" style="padding: 11px 15px 15px;border: 1px solid #a6a5a5;color:black">Quay lại</a>
                            <!--<a href="javascript:void(0)" class="remove-form" style="padding: 11px 15px 15px;border: 1px solid #a6a5a5;"><img style="width:24px;" src="http://de-an.local/assets2/img/trash.png"></a>-->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
@stop
@section('script')
@parent
<script src="{!! asset('assets/global_assets/js/plugins/forms/selects/select2.min.js') !!}"></script>
<script src="{!! asset('assets2/js/project.js') !!}"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>
<script>
    $('#check_member').change(function(){
        if($(this).is(':checked')){
            $('.choose-member').prop('disabled', false);
        }else{
             $('.choose-member').prop('disabled', 'disabled');
        }
    })
    $('#check_level').change(function(){
        if($(this).is(':checked')){
            $('.choose-level').prop('disabled', false);
        }else{
             $('.choose-level').prop('disabled', 'disabled');
        }
    })
    $('.choose-member').change(function(){
        var member_id = $(this).val();
        $.ajax({
            url:'/api/getInfoMember',
            method:'POST',
            data:{member_id:member_id},
            success: function(response){
                $('#info_member').html(response.html);
            }
        })
    })
    $.ajaxSetup ({
        cache: false
    });
    $('.load-page').click(function(){
        $('.load-page').removeClass('active');
        $(this).addClass('active');
        var ajax_load = "<img src='/public/assets2/img/small-loading.gif' alt='loading...' />";
        $(".project-content").html(ajax_load).load($(this).data('href')); 
    })
    $('.remove-form').click(function(e){
        $('#FrmCreateProject')[0].reset();
        if ($(CKEDITOR.instances).length) {
            for (var key in CKEDITOR.instances) {
                var instance = CKEDITOR.instances[key];
                if ($(instance.element.$).closest('form').attr('name') == $(e.target).attr('name')) {
                    instance.setData(instance.element.$.defaultValue);
                }
            }
        }
    })
    ClassicEditor
            .create( document.querySelector( '#before_content' ) )
            .catch( error => {
                console.error( error );
    } );
    ClassicEditor
            .create( document.querySelector( '#after_content' ) )
            .catch( error => {
                console.error( error );
    } );
</script>
@stop