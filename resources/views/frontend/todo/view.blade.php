@extends('frontend.layouts.create_schedule')
@section('content')
<table class="global_navi_title" cellpadding="0" cellmargin="0" style="padding:0px;">
   <tbody>
      <tr>
         <td width="100%" valign="bottom" nowrap="">
            <div role="heading" aria-level="2">
               <img src="{{asset('/img/todo20.gif')}}" border="0" alt="">To-Do List (To-Dos)
            </div>
         </td>
         <td align="right" valign="bottom" nowrap="">
         </td>
      </tr>
   </tbody>
</table>
<div class="mainarea ">
   <!--delete-->
   <script language="javascript" type="text/javascript">
      // on page load
      YAHOO.util.Event.onDOMReady(function(){
        // register event for delete handler
        var handler = ['lnk_delete']; 
        jQuery.each( handler, function(index, value){
          var ele = jQuery("#" + value);
          if( ele.length == 0 ) return;
          ele.click(function(event){
              var title = "Delete To-Do";
              var content = document.createElement("div");
              content.innerHTML = "<form name=\"todo\/delete\" method=\"get\" action=\"\{{route('frontend.todo.destroy',$record->id)}}\"><input type=\"hidden\" name=\"_token\" value=\"{{ csrf_token() }}\">\n <p>Do you want to delete the following To-Do?<br>\n <img src=\"\/garoon3\/grn\/image\/cybozu\/todo20.gif?20200925.text\" border=\"0\" alt=\"\"><span class=\"bold\">Prepare materials for ABC c...<\/span>\n <\/p>\n<input type=\"hidden\" name=\"tid\" value=\"8\">\n<\/form>";
              //get form element
              var f = content.getElementsByTagName("form")[0];
              if(!f) return; 
                  var ui_options = {};
                  // real HTML of $content is not equal to $content.innerHTML so message is not cerrect (browser behaviour about innerHTML attribute)
                  // So for this case we need to pass whole $content DOM object
                  var message = content.innerHTML;
                  
                  // show message box
                  GRN_MsgBox.show(message, title, GRN_MsgBoxButtons.yesno,
                      { ui       : ui_options,
                        caption : { ok: 'OK', cancel: 'Cancel', yes: 'Yes', no: 'No' },
                        callback : function(result, form){
                            if(result == GRN_MsgBoxResult.yes){
                               // submit form
                               jQuery('#msgbox').find('input[type="button"]').each(function(index, value){jQuery(this).prop( "disabled", true );});
                               document.body.style.cursor = 'progress';form.submit();
                            }
                        }
                      });
              event.stopPropagation();
              event.preventDefault();
          });
        });
      }); 
   </script>
   <!--delete-->
   <!--menubar-->
   <div id="main_menu_part">
      <span class="float_left nowrap-grn">
         <form name="top_finish" method="post" action="{{route('frontend.todo.update',$record->id)}}" class="inline_block_grn">
            <input type="hidden" name="id" value="@if($record->status == \App\ToDo::STATUS_COMPLETE) \App\ToDo::STATUS_UNCOMPLETE @else \App\ToDo::STATUS_COMPLETE @endif">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input type="hidden" name="ldate_year" value="{{date('Y',strtotime($record->due_date))}}" />
            <input type="hidden" name="ldate_month" value="{{date('m',strtotime($record->due_date))}}" />
            <input type="hidden" name="ldate_day" value="{{date('d',strtotime($record->due_date))}}" />
            <span class="aButtonStandard-grn"><button id="complete-todo-submit" type="button" tabindex="0" title="Complete" aria-label="Complete" class="mRight10" onclick="submit(this.form);">Complete</button></span>
         </form>
         @if(isset($membertodo) && $membertodo->join == 0)
         <span class="menu_item"><span class="nowrap-grn "><a href="{{route('frontend.todo.join',$record->id)}}"><img src="{{asset('/img/loginuser20.gif')}}" border="0" alt="">Join Todo</a></span></span>
         <span class="menu_item"><span class="nowrap-grn "><a href="javascipt:void(0)" class="unjoin-todo" data-toggle="modal" data-target="#modal_unjoin_todo"><img src="{{asset('/img/out_schedule20.gif')}}" border="0" alt="">Unjoin Todo</a></span></span>
         @endif
         @if($record->created_by == \Auth::guard('member')->user()->id)
         <span class="menu_item"><span class="nowrap-grn "><a href="{{route('frontend.todo.edit',$record->id)}}"><img src="{{asset('/img/modify20.gif')}}" border="0" alt="">Edit</a></span></span>
         <span class="menu_item"><span class="nowrap-grn "><a id="lnk_delete" href="javascript:void(0);"><img src="{{asset('/img/delete20.gif')}}" border="0" alt="">Delete</a></span></span>
         @endif
      </span>
      <div class="clear_both_0px"></div>
   </div>
   <!--menubar_end-->
   <h2 class="todo inline_block_grn mBottom10">{{$record->title}}</h2>
   <table class="view_table" width="80%">
      <colgroup>
         <col width="20%">
         <col width="80%">
      </colgroup>
      <tbody>
         <tr>
            <th>Category</th>
            <td id="todo-category">@if($record->category){{$record->category->title}} @else None @endif</td>
         </tr>
         <tr>
            <th>To-Do</th>
            <td id="todo-title">{{$record->title}}</td>
         </tr>
         <tr>
            <th>Start date</th>
            <td id="todo-limit">{{$record->start_date()}}</td>
         </tr>
         <tr>
            <th>End date</th>
            <td id="todo-limit">{{$record->end_date()}}</td>
         </tr>
         <tr>
            <th>Status</th>
            <td>
                <span class="badge badge-danger">{{$record->nameStatus()}}</span>
            </td>
         </tr>
         <tr>
            <th>Priority</th>
            <td>
                <span class="badge badge-danger">{{$record->namePriority()}}</span>
            </td>
         </tr>
         @if($record->member())
         <tr>
            <th>Member</th>
            <td>
                @foreach($record->member as $key=>$val)
                   @if($val->pivot->join == 1)
                   <span class="badge badge-primary">{{$val->full_name}}</span>
                   @elseif($val->pivot->join == 0)
                   <span class="badge badge-secondary">{{$val->full_name}}</span>
                   @else
                   <button style="height: 24px;" type="button" class="badge badge-danger popover-dismiss" data-toggle="popover" title="Lí do không tham gia" data-content="{{$val->pivot->reason}}" >{{$val->full_name}}</button>
                   @endif
                 @endforeach
            </td>
         </tr>
         @endif
         <tr valign="top">
            <th>Notes</th>
            <td id="todo-memo">
               <pre class="format_contents" style="white-space:-moz-pre-wrap;white-space:pre-wrap;display:inline;">{{$record->note}}</pre>
            </td>
         </tr>
      </tbody>
   </table>
   <div class="ui feed">
        <div class="ui comments">
            @foreach($comments as $key=>$comment)
            <div class="comment" id="comment{{$comment->id}}">
                   <div class="ui icon basic tiny buttons hide"><a class="ui button delete-comment" data-comment_id="{{$comment->id}}" href="javascript:void(0)"><i class="icon-cross"></i></a></div>
                   <a class="avatar"><img class="ui avatar image" src="{{is_null($comment->member->avatar) ? asset('img/user30.png') : $comment->member->avatar}}"></a>
                   <div class="content">
                      <a class="author">{{$comment->member->full_name}}</a>
                      <div class="metadata">
                         <div class="date">{{\App\Helpers\StringHelper::time_ago($comment->created_at)}}</div>
                      </div>
                      <div class="text">{{$comment->comment}}</div>
                      <div class="extra images"></div>
                   </div>
           </div>
           @endforeach
           <div class="comment">
               <a class="avatar">
                    <img class="ui avatar image" src="{{asset('img/user30.png')}}">
               </a>
               <div class="content">
                   <div>
                       <div class="pull-right" style="width:50px;padding-left:5px;">
                            <input type="button" style="display:none;" class="ui button mini" value="Gửi">
                       </div>
                       <div style="margin-right:50px;">
                           <textarea name="content" id="projecttask_comment_61549" placeholder="Viết bình luận..."></textarea>
                           <input type="file" id="ffile-61549" name="ffile" multiple="" class="hide">
                       </div>
                   </div>
               </div>
           </div>
        </div>
   </div>
</div>
<div class="modal fade" id="modal_unjoin_todo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom:0px">
                <h5 class="modal-title" id="exampleModalLabel">LÝ DO<span class="orange"> KHÔNG THAM GIA</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form method="post" action="{{route('frontend.todo.unjoin')}}">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <input type='hidden' name='id' value='{{$record->id}}'>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <textarea class='form-control' name='reason' rows="10"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class='text-center'>
                            <button type="submit" class='btn submit-return-project' style="height: 100%;"> GỬI </button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@stop
@section('script')
@parent
<script>
    $(document).ready(function(){
     $('.popover-dismiss').popover({
      trigger: 'focus'
    })
   });
    $('textarea').keypress(
        function(e){
            var comment = $(this).val();
            var todo_id = {{$record->id}}
            if (e.keyCode == 13) {
                if ($(this).index() == 0) {
                    $.ajax({
                        url:'/api/send-comment',
                        method:'POST',
                        data:{comment:comment,todo_id:todo_id},
                        success: function(response){
                            $('textarea').val('');
                            $('.ui.comments').prepend(response.html);
                        }
                    })
                }
                else {
                    // code for others
                }
            }
        });
        $('.delete-comment').click(function(){
            var comment_id = $(this).data('comment_id');
            $.ajax({
                url:'/api/delete-comment',
                method:'POST',
                data:{comment_id:comment_id},
                success: function(response){
                    $('#'+response.id).remove();
                }
            })
        })
</script>
@stop