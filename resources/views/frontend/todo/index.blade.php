@extends('frontend.layouts.create_schedule')
@section('content')
<table class="global_navi_title" cellpadding="0" cellmargin="0" style="padding:0px;">
   <tbody>
      <tr>
         <td width="100%" valign="bottom" nowrap="">
            <div role="heading" aria-level="2">
               <img src="{{asset('/img/todo20.gif')}}" border="0" alt="">To-Do List
            </div>
         </td>
         <td align="right" valign="bottom" nowrap="">
         </td>
      </tr>
   </tbody>
</table>
<div class="mainarea">
   <form name="todo/index" method="post" action="{!!route('frontend.todo.update_status')!!}">
      <input type="hidden" name="csrf_ticket" value="f034fcbb711af47845b5ae7183e8f743">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <!--tab-->
      <!--tab_end-->
      <table class="maincontents_list">
         <tbody>
            <tr>
               <td class="maincontents_list_td">
                  <table style="width:100%">
                     <tbody>
                        <tr valign="top">
                           <td id="tree_part">
                              <!--tree_structure-->
                              <div id="tree_structure">
                                 <div class="items">
                                    <div class="parent">
                                       <div class="tree_item">
                                           @if(!isset($_GET['category_id']))
                                             <span class="hilight"><span class="bold"><a class="" href="{{route('frontend.todo.index')}}"><img src="{{asset('/img/category20.gif')}}" border="0" alt="(All)" title="(All)">(All)</a></span></span>
                                           @else
                                             <nobr><a class="" href="{{route('frontend.todo.index')}}"><img src="{{asset('/img/category20.gif')}}" border="0" alt="(All)" title="(All)">(All)</a></nobr>
                                           @endif
                                       </div>
                                       @foreach($categorys as $key=>$category)
                                       <div class="tree_item ml25">
                                            @if(isset($_GET['category_id']) && $_GET['category_id'] == $category->id)
                                                <span class="hilight"><span class="bold"><a class="" href="{{route('frontend.todo.index',['category_id'=>$category->id])}}"><img src="{{asset('/img/category20.gif')}}">{!!$category->title!!}</a></span></span>
                                            @else
                                                <nobr><a class="" href="{{route('frontend.todo.index',['category_id'=>$category->id])}}"><img src="{{asset('/img/category20.gif')}}" border="0" alt="1" title="1">{!!$category->title!!}</a></nobr>
                                            @endif
                                         </div>
                                       @endforeach
                                    </div>
                                 </div>
                                 <div class="br">&nbsp;</div>
                              </div>
                              <!--structure_end-->
                           </td>
                           <td id="view_part" width="70%">
                              <div class="list_menu" style="display: flex;position: relative;margin-bottom: 40px;">
                                   <div class='list-left'>
                                        <span class="list_menu_item">
                                            <a class='active' href="{{route('frontend.todo.index')}}">Uncompleted</a>
                                        </span>
                                       <span style="font-size: 20px;">|</span>
                                        <span class="list_menu_item">
                                            <a href="{{route('frontend.todo.history')}}">Complete</a>
                                        </span>  
                                    </div>
                                   <div class='list-right' style="position:absolute;right:0px;">
                                        <span class="menu_item"><span class="nowrap-grn "><a id="todo-add" href="{!!route('frontend.todo.create')!!}"><img src="{!!asset('/img/todo20.gif')!!}" border="0" alt="">New To-Do</a></span></span>
                                        <span class="menu_item"><span class="nowrap-grn "><a id="category-add" href="{!!route('frontend.todo.editcategory')!!}"><img src="{!!asset('/img/category20.gif')!!}" border="0" alt="">Edit categories</a></span></span>
                                    </div>
                              </div>
                              <div class="list_menu">
                                 <span class="list_menu_item">
                                    <script language="JavaScript" type="text/javascript">
                                       <!--
                                       var enable_shift_click = false;
                                       var element_name = 'id[]';
                                       function CheckAll()
                                       
                                       {
                                           var e = document.forms["todo/index"].elements;
                                           var l = e.length;
                                           var checked = false;
                                           for( i = 0 ; i < l ; i ++ ) { if( e[i].name == "id[]" && e[i].style.display=="" && ! e[i].checked  ) { checked = true;break; } }
                                           for( i = 0 ; i < l ; i ++ ) { if( e[i].name == "id[]" && e[i].style.display=="" ) { e[i].checked=checked; } }
                                           if( typeof setHightlight == 'function' )
                                               setHightlight();
                                       }
                                       
                                       // apply shift click
                                       
                                       applyShiftClick();
                                       
                                       function setHightlight()
                                       {
                                           if (!enable_shift_click)
                                               return;
                                       
                                           var table = jQuery("table.list_column").get(0);
                                           if (table) {
                                               var rows = jQuery(table).find("tr");
                                               rows.each(function (index, row) {
                                                   var row_jquery_obj = jQuery(row);
                                                   var chkbox = row_jquery_obj.find("input[type='checkbox']").eq(0);
                                                   if (chkbox.length == 0)
                                                       return;
                                       
                                                   if (chkbox.is(":checked")) {
                                                       row_jquery_obj.addClass("row_highlight");
                                                   }
                                                   else {
                                                       row_jquery_obj.removeClass("row_highlight");
                                                   }
                                       
                                               });
                                           }
                                       }
                                       function applyShiftClick()
                                       {
                                           if (!enable_shift_click)
                                               return;
                                       
                                           var list = jQuery("table.list_column").get(0);
                                           var row = null;
                                           var row2 = null;
                                           if (list) {
                                               var chkboxs = fastGetElementsByName(list, 'input', element_name);
                                               var i = 0;
                                               jQuery.each(chkboxs, function (index, chkbox) {
                                                   chkbox.index = i;
                                       
                                                   var chkbox_jpuery_object = jQuery(chkbox);
                                                   chkbox_jpuery_object.on("click", function (e) {
                                                       row = chkbox_jpuery_object.parents("tr").eq(0);
                                                       if (this.checked) {
                                                           row.addClass("row_highlight");
                                                       }
                                                       else {
                                                           row.removeClass("row_highlight");
                                                       }
                                       
                                                       //handle Shift click
                                                       var last_click = list.last_click;
                                                       var last_click_index = last_click ? last_click.index : 0;
                                                       var current = this;
                                                       var current_index = this.index;
                                                       var low = Math.min(last_click_index, current_index);
                                                       var high = Math.max(last_click_index, current_index);
                                       
                                                       if (e.shiftKey) {
                                                           var chkboxs2 = fastGetElementsByName(list, 'input', element_name);
                                                           jQuery(chkboxs2).each(function (index, item) {
                                                               row2 = jQuery(item).parents("tr").eq(0);
                                       
                                                               if (item.index >= low && item.index <= high) {
                                                                   item.checked = current.checked;
                                                               }
                                       
                                                               if (item.checked) {
                                                                   row2.addClass('row_highlight');
                                                               }
                                                               else {
                                                                   row2.removeClass('row_highlight');
                                                               }
                                                           });
                                                       }
                                                       list.last_click = e.target;
                                                   });
                                                   i++;
                                               });
                                           }
                                       }
                                       
                                       //-->
                                    </script>
                                    <button class="check_button" style="width:20px; height:20px" onclick="javascript:CheckAll();return false"><img src="{{asset('/img/check10.gif')}}" border="0" alt="Select or clear all check boxes" title="Select or clear all check boxes"></button>
                                 </span>
                                 <span class="list_menu_item"><input class="" type="button" value="Completed" onclick="if(! grn_is_checked(this.form,'id[]')) return false;submit(this.form)" style=""></span>  
                              </div>
                              <table class="table table-bordered table-member" style="margin-top:5px;">
                                    <thead class="thead-dark">
                                        <tr style="box-shadow: none;">
                                            <th>STT</th>
                                            <th>Công việc</th>
                                            <th>Thành viên</th>
                                            <th>Ngày tạo</th>
                                            <th>Trạng thái</th>
                                            <th>Tác vụ</th>
                                        </tr>
                                    </thead>
                                    <tbody id='records_list_project'>
                                        @foreach($records as $key=>$record)
                                        <tr>
                                            <td  class="middle">{{$key + 1}}</td>
                                            <td  class="middle"><a href="{{route('frontend.todo.view',$record->id)}}">{{$record->title}}</a></td>
                                            <td  class="middle">
                                             @foreach($record->member as $key=>$val)
                                               @if($val->pivot->join == 1)
                                               <span class="badge badge-primary">{{$val->full_name}}</span>
                                               @elseif($val->pivot->join == 0)
                                               <span class="badge badge-secondary">{{$val->full_name}}</span>
                                               @else
                                               <button style="height: 24px;" type="button" class="badge badge-danger popover-dismiss" data-toggle="popover" title="Lí do không tham gia" data-content="{{$val->pivot->reason}}" >{{$val->full_name}}</button>
                                               @endif
                                             @endforeach
                                             </td>
                                            <td  class="middle">{{$record->created_at()}}</td>
                                            <td class="middle"><span class="badge badge-danger">{{$record->nameStatus()}}</span></td>
                                            <td class="middle">
                                               @if(in_array(\Auth::guard('member')->user()->id,$record->member()->pluck('id')->toArray()))
                                               <a href="javascipt:void(0)" class="change-status" data-id="{{$record->id}}"><i class="icon-file-check"></i></a>
                                               @endif
                                               @if(\Auth::guard('member')->user()->id == $record->created_by)
                                               <a href="{{route('frontend.todo.edit',$record->id)}}"><i class="icon-pencil"></i></a> 
                                               <a href="{{route('frontend.todo.destroy',$record->id)}}"><i class="icon-bin"></i></a>
                                               @endif
                                             </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                              </table>
                              <div class="list_menu">
                                 <span class="list_menu_item">
                                    <script language="JavaScript" type="text/javascript">
                                       <!--
                                       var enable_shift_click = false;
                                       var element_name = 'id[]';
                                       function CheckAll()
                                       
                                       {
                                           var e = document.forms["todo/index"].elements;
                                           var l = e.length;
                                           var checked = false;
                                           for( i = 0 ; i < l ; i ++ ) { if( e[i].name == "id[]" && e[i].style.display=="" && ! e[i].checked  ) { checked = true;break; } }
                                           for( i = 0 ; i < l ; i ++ ) { if( e[i].name == "id[]" && e[i].style.display=="" ) { e[i].checked=checked; } }
                                           if( typeof setHightlight == 'function' )
                                               setHightlight();
                                       }
                                       
                                       // apply shift click
                                       
                                       applyShiftClick();
                                       
                                       function setHightlight()
                                       {
                                           if (!enable_shift_click)
                                               return;
                                       
                                           var table = jQuery("table.list_column").get(0);
                                           if (table) {
                                               var rows = jQuery(table).find("tr");
                                               rows.each(function (index, row) {
                                                   var row_jquery_obj = jQuery(row);
                                                   var chkbox = row_jquery_obj.find("input[type='checkbox']").eq(0);
                                                   if (chkbox.length == 0)
                                                       return;
                                       
                                                   if (chkbox.is(":checked")) {
                                                       row_jquery_obj.addClass("row_highlight");
                                                   }
                                                   else {
                                                       row_jquery_obj.removeClass("row_highlight");
                                                   }
                                       
                                               });
                                           }
                                       }
                                       function applyShiftClick()
                                       {
                                           if (!enable_shift_click)
                                               return;
                                       
                                           var list = jQuery("table.list_column").get(0);
                                           var row = null;
                                           var row2 = null;
                                           if (list) {
                                               var chkboxs = fastGetElementsByName(list, 'input', element_name);
                                               var i = 0;
                                               jQuery.each(chkboxs, function (index, chkbox) {
                                                   chkbox.index = i;
                                       
                                                   var chkbox_jpuery_object = jQuery(chkbox);
                                                   chkbox_jpuery_object.on("click", function (e) {
                                                       row = chkbox_jpuery_object.parents("tr").eq(0);
                                                       if (this.checked) {
                                                           row.addClass("row_highlight");
                                                       }
                                                       else {
                                                           row.removeClass("row_highlight");
                                                       }
                                       
                                                       //handle Shift click
                                                       var last_click = list.last_click;
                                                       var last_click_index = last_click ? last_click.index : 0;
                                                       var current = this;
                                                       var current_index = this.index;
                                                       var low = Math.min(last_click_index, current_index);
                                                       var high = Math.max(last_click_index, current_index);
                                       
                                                       if (e.shiftKey) {
                                                           var chkboxs2 = fastGetElementsByName(list, 'input', element_name);
                                                           jQuery(chkboxs2).each(function (index, item) {
                                                               row2 = jQuery(item).parents("tr").eq(0);
                                       
                                                               if (item.index >= low && item.index <= high) {
                                                                   item.checked = current.checked;
                                                               }
                                       
                                                               if (item.checked) {
                                                                   row2.addClass('row_highlight');
                                                               }
                                                               else {
                                                                   row2.removeClass('row_highlight');
                                                               }
                                                           });
                                                       }
                                                       list.last_click = e.target;
                                                   });
                                                   i++;
                                               });
                                           }
                                       }
                                       
                                       //-->
                                    </script>
                                    <button class="check_button" style="width:20px; height:20px" onclick="javascript:CheckAll();return false"><img src="{{asset('/img/check10.gif')}}" border="0" alt="Select or clear all check boxes" title="Select or clear all check boxes"></button>
                                 </span>
                                 <span class="list_menu_item"><input class="" type="button" value="Completed" onclick="if(! grn_is_checked(this.form,'id[]')) return false;submit(this.form)" style=""></span>  
                              </div>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </td>
            </tr>
         </tbody>
      </table>
   </form>
   <!--end of maincontents_list-->
</div>
<div class="modal fade" id="modal_unjoin_todo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom:0px">
                <h5 class="modal-title" id="exampleModalLabel">CẬP NHẬT TRẠNG THÁI CÔNG VIỆC</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form method="post" action="{{route('frontend.todo.changeStatus')}}">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <input type='hidden' name='id' id="todo_id">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <select class="from-control select-search" id="select_status" name="status" data-placeholder="Chọn trạng thái">
                                        
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class='text-center'>
                            <button type="submit" class='btn submit-return-project' style="height: 100%;"> GỬI </button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@stop
@section('script')
@parent
<script>
$(document).ready(function(){
     $('.popover-dismiss').popover({
      trigger: 'focus'
    })
});
$('body').delegate('.change-status','click',function(){
    var id = $(this).data('id');
    $.ajax({
            url:'/api/getSelectStatus',
            method:'POST',
            data:{id:id},
            success: function(response){
                $('#select_status').html(response.html);
                $('#todo_id').val(id);
                $('#modal_unjoin_todo').modal('show');
            }
        })
})
</script>
@stop