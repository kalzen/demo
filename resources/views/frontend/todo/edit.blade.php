@extends('frontend.layouts.create_schedule')
@section('content')
<table class="global_navi_title" cellpadding="0" cellmargin="0" style="padding:0px;">
   <tbody>
      <tr>
         <td width="100%" valign="bottom" nowrap="">
            <div role="heading" aria-level="2">
               <img src="{{asset('/img/todo20.gif')}}" border="0" alt="">To-Do List (To-Dos)
            </div>
         </td>
         <td align="right" valign="bottom" nowrap="">
         </td>
      </tr>
   </tbody>
</table>
<div class="mainarea ">
   <h2 style="display:inline;" class="todo">Edit To-Do</h2>
   <form name="todo/modify" method="post" action="{{route('frontend.todo.update',$record->id)}}">
      <input type="hidden" name="member_id" value="{{\Auth::guard('member')->user()->id}}">
      <input type="hidden" name="_token" value="{{ csrf_token() }}" />
      <p>
      </p>
      <table class="std_form" style="border-collapse: separate;">
         <tbody>
            <tr>
               <th>To-Do<span class="attention">*</span></th>
               <td><input type="text" name="title" class="form-control" value="{{$record->title}}" /></td>
            </tr>
            <tr>
               <th>Start date</th>
               <td>
                  <input type="date" name="start_date" value="{{date('Y-m-d',strtotime($record->start_date))}}" class="form-control"/>
               </td>
            </tr>
            <tr>
               <th>End date</th>
               <td>
                  <input type="date" name="end_date"  value="{{date('Y-m-d',strtotime($record->end_date))}}" class="form-control"/>
               </td>
            </tr>
            <tr>
               <th>Status</th>
               <td>
                   <select class="form-control select" name="status" data-placeholder="Tất cả">
                        {!!$status_html!!}
                   </select>
               </td>
            </tr>
            <tr>
               <th>Priority</th>
               <td>
                  <select class="form-control select" name="priority" data-placeholder="Tất cả">
                        {!!$priority_html!!}
                   </select>   
               </td>
            </tr>
            <tr>
               <th>Member</th>
               <td>
                  <select class="form-control select-search" name="member_id" multiple="multiple" data-placeholder="Thành viên tham gia">
                        {!!$member_html!!}
                   </select>   
               </td>
            </tr>
            <tr valign="top">
               <th>Notes</th>
               <td><textarea name="note" class="form-control" cols="50" rows="5">{{$record->note}}</textarea></td>
            </tr>
            <tr>
               <td></td>
               <td>
                    <div class="mTop10">
                      <span id="todo_button_add" class="button_grn_js button1_main_grn  button1_r_margin2_grn" onclick="grn.component.button.util.submit(this);" name="todo-add-submit" data-auto-disable="1">
                          <a href="javascript:void(0);" role="button">Add</a>
                      </span>
                      <span id="todo_button_cancel" class="button_grn_js button1_normal_grn" onclick="grn.component.button.util.redirect(this,'/todo/index?cid=10');">
                      <a href="javascript:void(0);" role="button">Cancel</a>
                      </span>
                    </div>
               </td>
            </tr>
         </tbody>
      </table>
   </form>
</div>
@stop
@section('script')
@parent
<script></script>
@stop