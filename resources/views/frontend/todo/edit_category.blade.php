@extends('frontend.layouts.create_schedule')
@section('content')
<div class="global_navi" role="heading" aria-level="2">&nbsp;<img src="{!!asset('/img/todo20.gif')!!}" border="0" alt=""><span class="globalNavi-item-grn"><a href="{!!route('frontend.todo.index')!!}">To-Do List (To-Dos)</a></span><span class="globalNavi-item-grn-image"></span><span class="globalNavi-item-last-grn">Edit categories</span></div>
<div class="mainarea ">
   <h2 style="display:inline;" class="todo">Edit categories</h2>
   <form name="todo/category_set" method="post" action="{{route('frontend.todo.updatecategory')}}">
      <input type="hidden" name="csrf_ticket" value="b42be3a869914aff1196c3e9dc05b9f2">
      <input type="hidden" name="_token" value="{{ csrf_token() }}" />
      <input type="hidden" name="member_id" value="{!!\Auth::guard('member')->user()->id!!}">
      <div id="one_parts">
         <div id="action">
            <div class="explanation">Enter To-Do's categories one by one. Those will be displayed in the order of inputting.</div>
            <textarea id="textarea_id" name="category" class="autoexpand" wrap="virtual" role="form" cols="50" rows="10" style="white-space: pre-wrap; min-height: 190px; height: 216px;">{!!$category!!}
            </textarea>
            <textarea style="white-space: pre-wrap; left: -9999px; position: absolute; top: 0px; height: 216px;" id="dummy_textarea_textarea_id" class="" wrap="virtual" cols="50" rows="10" disabled="">Work
Private
</textarea>
         </div>
      </div>
      <div class="mTop15 mBottom15"><span id="todo-category-set-submit" class="button_grn_js button1_main_grn  button1_r_margin2_grn" onclick="grn.component.button.util.submit(this);" data-auto-disable="true"><a href="javascript:void(0);" role="button">Save</a></span><span id="todo-category-set-cancel" class="button_grn_js button1_normal_grn" onclick="grn.component.button.util.redirect(this,'history_back');"><a href="javascript:void(0);" role="button">Cancel</a></span></div>
   </form>
</div>
@stop
@section('script')
@parent
<script></script>
@stop