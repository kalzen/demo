@extends('frontend.layouts.create_schedule')
@section('content')
<table class="global_navi_title" cellpadding="0" cellmargin="0" style="padding:0px;">
   <tbody>
      <tr>
         <td width="100%" valign="bottom" nowrap="">
            <div role="heading" aria-level="2">
               <img src="{{asset('/img/todo20.gif')}}" border="0" alt="">To-Do List
            </div>
         </td>
         <td align="right" valign="bottom" nowrap="">
         </td>
      </tr>
   </tbody>
</table>
<div class="mainarea">
   <!--delete-->
   <script language="javascript" type="text/javascript">
      // on page load
      YAHOO.util.Event.onDOMReady(function(){
        // register event for delete handler
        var handler = ['lnk_delete_all']; 
        jQuery.each( handler, function(index, value){
          var ele = jQuery("#" + value);
          if( ele.length == 0 ) return;
          ele.click(function(event){
              var title = "Delete all completed To-Do";
              var content = document.createElement("div");
              content.innerHTML = "<form name=\"todo\/history_delete_all\" method=\"post\" action=\"{!!route('frontend.todo.destroyall')!!}\"><input type=\"hidden\" name=\"_token\" value=\"{{ csrf_token() }}\">\n <p>Do you want to delete all completed To-Do?<\/p>\n<input type=\"hidden\" name=\"tid\" value=\"\">\n<\/form>";
              //get form element
              var f = content.getElementsByTagName("form")[0];
              if(!f) return; 
                  var ui_options = {};
                  // real HTML of $content is not equal to $content.innerHTML so message is not cerrect (browser behaviour about innerHTML attribute)
                  // So for this case we need to pass whole $content DOM object
                  var message = content.innerHTML;
                  
                  // show message box
                  GRN_MsgBox.show(message, title, GRN_MsgBoxButtons.yesno,
                      { ui       : ui_options,
                        caption : { ok: 'OK', cancel: 'Cancel', yes: 'Yes', no: 'No' },
                        callback : function(result, form){
                            if(result == GRN_MsgBoxResult.yes){
                               // submit form
                               jQuery('#msgbox').find('input[type="button"]').each(function(index, value){jQuery(this).prop( "disabled", true );});
                               document.body.style.cursor = 'progress';form.submit();
                            }
                        }
                      });
              event.stopPropagation();
              event.preventDefault();
          });
        });
      }); 
   </script>
   <script language="javascript" type="text/javascript">
      // on page load
      YAHOO.util.Event.onDOMReady(function(){
        // register event for delete handler
        var handler = ['btn_delete_multi1','btn_delete_multi2']; 
        jQuery.each( handler, function(index, value){
          var ele = jQuery("#" + value);
          if( ele.length == 0 ) return;
          ele.click(function(event){
              var title = "Delete completed To-Dos";
              var content = document.createElement("div");
              content.innerHTML = "<form name=\"todo\/history_delete_multi\" method=\"post\" action=\"{{route('frontend.todo.destroymulti')}}\"><input type=\"hidden\" name=\"_token\" value=\"{{ csrf_token() }}\">\n <p>Are you sure you want to delete the completed To-Do?<br>\n Number of items:<span class=\"bold delete_count\"><\/span><br>\n <\/p>\n<\/form>";
              //get form element
              var f = content.getElementsByTagName("form")[0];
              if(!f) return; 
              var list = jQuery("form[name='todo\/history'] input[name='id[]']").filter(function(i){return jQuery(this).prop( "checked" );});
              if( list.length == 0 ) return;    // do nothing if these is not any item is selected
              //update number of items to delete
              var delete_number_span = content.getElementsByTagName("span");
              for (i = 0; i < delete_number_span.length; i++) {
                  if ( jQuery(delete_number_span[i]).hasClass("delete_count") )
                  {
                      var delete_number = delete_number_span[i];
                  }
              }
              if(delete_number) delete_number.innerHTML = list.length;
      
              //add hidden field
              jQuery.each( list, function(index, value){
                  var hidden_input = document.createElement("input");
                  var input_name = jQuery(this).prop( "name" );
                  var input_value = jQuery(this).val();
                  hidden_input.setAttribute("type", "hidden");
                  hidden_input.setAttribute("name", input_name);
                  hidden_input.setAttribute("value", input_value);
                  f.appendChild( hidden_input );
              });
                  var ui_options = {};
                  // real HTML of $content is not equal to $content.innerHTML so message is not cerrect (browser behaviour about innerHTML attribute)
                  // So for this case we need to pass whole $content DOM object
                  var message = content.innerHTML;
                  
                  // show message box
                  GRN_MsgBox.show(message, title, GRN_MsgBoxButtons.yesno,
                      { ui       : ui_options,
                        caption : { ok: 'OK', cancel: 'Cancel', yes: 'Yes', no: 'No' },
                        callback : function(result, form){
                            if(result == GRN_MsgBoxResult.yes){
                               // submit form
                               jQuery('#msgbox').find('input[type="button"]').each(function(index, value){jQuery(this).prop( "disabled", true );});
                               document.body.style.cursor = 'progress';form.submit();
                            }
                        }
                      });
              event.stopPropagation();
              event.preventDefault();
          });
        });
      }); 
   </script>
   <!--delete-->
 
   <!--tab-->
   <!--tab_end-->
   <table class="maincontents_list">
      <tbody>
         <tr>
            <td class="maincontents_list_td">
               <table style="width:100%">
                  <tbody>
                     <tr valign="top">
                        <td id="tree_part">
                           <!--tree_structure-->
                           <div id="tree_structure">
                                 <div class="items">
                                    <div class="parent">
                                       <div class="tree_item">
                                           @if(!isset($_GET['category_id']))
                                             <span class="hilight"><span class="bold"><a class="" href="{{route('frontend.todo.history')}}"><img src="{{asset('/img/category20.gif')}}" border="0" alt="(All)" title="(All)">(All)</a></span></span>
                                           @else
                                             <nobr><a class="" href="{{route('frontend.todo.history')}}"><img src="{{asset('/img/category20.gif')}}" border="0" alt="(All)" title="(All)">(All)</a></nobr>
                                           @endif
                                       </div>
                                       @foreach($categorys as $key=>$category)
                                       <div class="tree_item ml25">
                                            @if(isset($_GET['category_id']) && $_GET['category_id'] == $category->id)
                                                <span class="hilight"><span class="bold"><a class="" href="{{route('frontend.todo.history',['category_id'=>$category->id])}}"><img src="{{asset('/img/category20.gif')}}">{!!$category->title!!}</a></span></span>
                                            @else
                                                <nobr><a class="" href="{{route('frontend.todo.history',['category_id'=>$category->id])}}"><img src="{{asset('/img/category20.gif')}}" border="0" alt="1" title="1">{!!$category->title!!}</a></nobr>
                                            @endif
                                         </div>
                                       @endforeach
                                    </div>
                                 </div>
                                 <div class="br">&nbsp;</div>
                            </div>
                           <!--tree_structure_end-->
                        </td>
                        <td id="view_part" width="70%">
                            <div class="list_menu" style="display: flex;position: relative;margin-bottom: 40px;">
                                   <div class='list-left'>
                                        <span class="list_menu_item">
                                            <a href="{{route('frontend.todo.index')}}">Uncompleted</a>
                                        </span>
                                       <span style="font-size: 20px;">|</span>
                                        <span class="list_menu_item">
                                            <a class='active' href="{{route('frontend.todo.history')}}">Complete</a>
                                        </span>  
                                    </div>
                                   <div class='list-right' style="position:absolute;right:0px;">
                                        <span class="menu_item"><span class="nowrap-grn "><a id="lnk_delete_all" href="javascript:void(0);"><img src="{{asset('/img/delete20.gif')}}" border="0" alt="">Delete all completed To-Do</a></span></span>
                                        
                                    </div>
                            </div>
                           <form name="todo/history" method="post" action="{{route('frontend.todo.destroymulti')}}">
                              <input type="hidden" name="csrf_ticket" value="b0955c3c0ece7761126bd762c9073898">
                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                              <div class="list_menu">
                                 <span class="list_menu_item">
                                    <script language="JavaScript" type="text/javascript">
                                       <!--
                                       var enable_shift_click = false;
                                       var element_name = 'id[]';
                                       function CheckAll()
                                       
                                       {
                                           var e = document.forms["todo/history"].elements;
                                           var l = e.length;
                                           var checked = false;
                                           for( i = 0 ; i < l ; i ++ ) { if( e[i].name == "id[]" && e[i].style.display=="" && ! e[i].checked  ) { checked = true;break; } }
                                           for( i = 0 ; i < l ; i ++ ) { if( e[i].name == "id[]" && e[i].style.display=="" ) { e[i].checked=checked; } }
                                           if( typeof setHightlight == 'function' )
                                               setHightlight();
                                       }
                                       
                                       // apply shift click
                                       
                                       applyShiftClick();
                                       
                                       function setHightlight()
                                       {
                                           if (!enable_shift_click)
                                               return;
                                       
                                           var table = jQuery("table.list_column").get(0);
                                           if (table) {
                                               var rows = jQuery(table).find("tr");
                                               rows.each(function (index, row) {
                                                   var row_jquery_obj = jQuery(row);
                                                   var chkbox = row_jquery_obj.find("input[type='checkbox']").eq(0);
                                                   if (chkbox.length == 0)
                                                       return;
                                       
                                                   if (chkbox.is(":checked")) {
                                                       row_jquery_obj.addClass("row_highlight");
                                                   }
                                                   else {
                                                       row_jquery_obj.removeClass("row_highlight");
                                                   }
                                       
                                               });
                                           }
                                       }
                                       function applyShiftClick()
                                       {
                                           if (!enable_shift_click)
                                               return;
                                       
                                           var list = jQuery("table.list_column").get(0);
                                           var row = null;
                                           var row2 = null;
                                           if (list) {
                                               var chkboxs = fastGetElementsByName(list, 'input', element_name);
                                               var i = 0;
                                               jQuery.each(chkboxs, function (index, chkbox) {
                                                   chkbox.index = i;
                                       
                                                   var chkbox_jpuery_object = jQuery(chkbox);
                                                   chkbox_jpuery_object.on("click", function (e) {
                                                       row = chkbox_jpuery_object.parents("tr").eq(0);
                                                       if (this.checked) {
                                                           row.addClass("row_highlight");
                                                       }
                                                       else {
                                                           row.removeClass("row_highlight");
                                                       }
                                       
                                                       //handle Shift click
                                                       var last_click = list.last_click;
                                                       var last_click_index = last_click ? last_click.index : 0;
                                                       var current = this;
                                                       var current_index = this.index;
                                                       var low = Math.min(last_click_index, current_index);
                                                       var high = Math.max(last_click_index, current_index);
                                       
                                                       if (e.shiftKey) {
                                                           var chkboxs2 = fastGetElementsByName(list, 'input', element_name);
                                                           jQuery(chkboxs2).each(function (index, item) {
                                                               row2 = jQuery(item).parents("tr").eq(0);
                                       
                                                               if (item.index >= low && item.index <= high) {
                                                                   item.checked = current.checked;
                                                               }
                                       
                                                               if (item.checked) {
                                                                   row2.addClass('row_highlight');
                                                               }
                                                               else {
                                                                   row2.removeClass('row_highlight');
                                                               }
                                                           });
                                                       }
                                                       list.last_click = e.target;
                                                   });
                                                   i++;
                                               });
                                           }
                                       }
                                       
                                       //-->
                                    </script>
                                    <button class="check_button" style="width:20px; height:20px" onclick="javascript:CheckAll();return false"><img src="{{asset('/img/check10.gif')}}" border="0" alt="Select or clear all check boxes" title="Select or clear all check boxes"></button>
                                 </span>
                                 <span class="list_menu_item"><input class="" type="button" value="Delete" onclick="return false;submit(this.form)" style="" id="btn_delete_multi1"></span>  
                              </div>
                              <table class="table table-bordered table-member" style="margin-top:5px;">
                                    <thead class="thead-dark">
                                        <tr style="box-shadow: none;">
                                            <th>STT</th>
                                            <th>Công việc</th>
                                            <th>Ngày tạo</th>
                                            <th>Trạng thái</th>
                                        </tr>
                                    </thead>
                                    <tbody id='records_list_project'>
                                        @foreach($records as $key=>$record)
                                        <tr>
                                            <td  class="middle">{{$key + 1}}</td>
                                            <td  class="middle"><a href="{{route('frontend.todo.edit',$record->id)}}">{{$record->title}}</a></td>
                                            <td  class="middle">{{$record->created_at()}}</td>
                                            <td class="middle"><span class="badge badge-danger">{{$record->nameStatus()}}</span></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                              </table>
                              <script language="JavaScript" text="text/javascript"><!--
                                 function on_delete( f, ename )
                                 {
                                     if( ! grn_is_checked( f, ename ) ) return false;
                                     return true;
                                 }
                                 //-->
                              </script>
                              <div class="list_menu">
                                 <span class="list_menu_item">
                                    <script language="JavaScript" type="text/javascript">
                                       <!--
                                       var enable_shift_click = false;
                                       var element_name = 'id[]';
                                       function CheckAll()
                                       
                                       {
                                           var e = document.forms["todo/history"].elements;
                                           var l = e.length;
                                           var checked = false;
                                           for( i = 0 ; i < l ; i ++ ) { if( e[i].name == "id[]" && e[i].style.display=="" && ! e[i].checked  ) { checked = true;break; } }
                                           for( i = 0 ; i < l ; i ++ ) { if( e[i].name == "id[]" && e[i].style.display=="" ) { e[i].checked=checked; } }
                                           if( typeof setHightlight == 'function' )
                                               setHightlight();
                                       }
                                       
                                       // apply shift click
                                       
                                       applyShiftClick();
                                       
                                       function setHightlight()
                                       {
                                           if (!enable_shift_click)
                                               return;
                                       
                                           var table = jQuery("table.list_column").get(0);
                                           if (table) {
                                               var rows = jQuery(table).find("tr");
                                               rows.each(function (index, row) {
                                                   var row_jquery_obj = jQuery(row);
                                                   var chkbox = row_jquery_obj.find("input[type='checkbox']").eq(0);
                                                   if (chkbox.length == 0)
                                                       return;
                                       
                                                   if (chkbox.is(":checked")) {
                                                       row_jquery_obj.addClass("row_highlight");
                                                   }
                                                   else {
                                                       row_jquery_obj.removeClass("row_highlight");
                                                   }
                                       
                                               });
                                           }
                                       }
                                       function applyShiftClick()
                                       {
                                           if (!enable_shift_click)
                                               return;
                                       
                                           var list = jQuery("table.list_column").get(0);
                                           var row = null;
                                           var row2 = null;
                                           if (list) {
                                               var chkboxs = fastGetElementsByName(list, 'input', element_name);
                                               var i = 0;
                                               jQuery.each(chkboxs, function (index, chkbox) {
                                                   chkbox.index = i;
                                       
                                                   var chkbox_jpuery_object = jQuery(chkbox);
                                                   chkbox_jpuery_object.on("click", function (e) {
                                                       row = chkbox_jpuery_object.parents("tr").eq(0);
                                                       if (this.checked) {
                                                           row.addClass("row_highlight");
                                                       }
                                                       else {
                                                           row.removeClass("row_highlight");
                                                       }
                                       
                                                       //handle Shift click
                                                       var last_click = list.last_click;
                                                       var last_click_index = last_click ? last_click.index : 0;
                                                       var current = this;
                                                       var current_index = this.index;
                                                       var low = Math.min(last_click_index, current_index);
                                                       var high = Math.max(last_click_index, current_index);
                                       
                                                       if (e.shiftKey) {
                                                           var chkboxs2 = fastGetElementsByName(list, 'input', element_name);
                                                           jQuery(chkboxs2).each(function (index, item) {
                                                               row2 = jQuery(item).parents("tr").eq(0);
                                       
                                                               if (item.index >= low && item.index <= high) {
                                                                   item.checked = current.checked;
                                                               }
                                       
                                                               if (item.checked) {
                                                                   row2.addClass('row_highlight');
                                                               }
                                                               else {
                                                                   row2.removeClass('row_highlight');
                                                               }
                                                           });
                                                       }
                                                       list.last_click = e.target;
                                                   });
                                                   i++;
                                               });
                                           }
                                       }
                                       
                                       //-->
                                    </script>
                                    <button class="check_button" style="width:20px; height:20px" onclick="javascript:CheckAll();return false"><img src="{{asset('/img/check10.gif')}}" border="0" alt="Select or clear all check boxes" title="Select or clear all check boxes"></button>
                                 </span>
                                 <span class="list_menu_item"><input class="" type="button" value="Delete" onclick="return false;submit(this.form)" style="" id="btn_delete_multi2"></span>  
                              </div>
                              
                           </form>
                        </td>
                     </tr>
                  </tbody>
               </table>
            </td>
         </tr>
      </tbody>
   </table>
   <!--end of maincontents_list-->
</div>
@stop
@section('script')
@parent
<script></script>
@stop