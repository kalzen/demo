@extends('frontend.layouts.admin')
@section('content')
<body class="page-body">
    <div class="content" style="padding: 40px 0px">
        <div class="container">
            <div class="row">
                <div class="welcome-home text-center" style="width:100%">
                    <div class="img-avatar">
                        <img src="@if(is_null(\Auth::guard('member')->user()->avatar)){!!asset('assets2/img/man.png')!!} @else {{\Auth::guard('member')->user()->avatar}} @endif" style="width:250px;">
                    </div>
                    <h3 class='orange'>{{trans('base.hello')}} {{\Auth::guard('member')->user()->full_name}}</h3>
                    <p>{{trans('base.manage.member')}}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-5" style="margin-right: 25px;margin-bottom: 50px">
                    <a href="{!!route('frontend.project.index')!!}" class='div-choose'>
                        <div class="row box-choose">
                            <div class="col-md-4" style="text-align: center;background: #fd7600;border-radius: 50%;padding: 15px;">
                                <img src="{!!asset('assets2/img/tie (1).png')!!}">
                            </div>
                            <div class="col-md-8" style="padding-left: 36px;">
                                <h4>{{trans('base.personal_page')}}</h4>
                                <p>{{trans('base.personal_page_description')}}</p>
                            </div>
                        </div>
                    </a>
                    <a href="{!!route('frontend.project.index')!!}" class='go-box'> {{trans('base.go')}} >></a>
                </div>
                <div class="col-md-5" style="margin-bottom: 50px"> 
                    <a href="{!!route('frontend.project.list')!!}" class='div-choose'>
                        <div class="row box-choose">
                            <div class="col-md-4" style="text-align: center;background: #fd7600;border-radius: 50%;padding: 15px;">
                                <img src="{!!asset('assets2/img/review.png')!!}">
                            </div>
                            <div class="col-md-8" style="padding-left: 36px;">
                                <h4>{{trans('base.project_review')}}</h4>
                                <p style="margin-bottom:0px;">{{trans('base.project_review_description1')}}</p>
                                <p>{{trans('base.project_review_description2')}}</p>
                            </div>
                        </div>
                    </a>
                    <a href="{!!route('frontend.project.list')!!}" class='go-box'> {{trans('base.go')}} >></a>
                </div>
                @if(\Auth::guard('member')->user()->level == 4)
                <div class="col-md-1"></div>
                <div class="col-md-5" style="margin-right: 25px;">
                    <a href="{!!route('frontend.member.index')!!}" class='div-choose'>
                        <div class="row box-choose">
                            <div class="col-md-4" style="text-align: center;background: #fd7600;border-radius: 50%;padding: 15px;">
                                <img src="{!!asset('assets2/img/team.png')!!}">
                            </div>
                            <div class="col-md-8" style="padding-left: 36px;">
                                <h4>{{trans('base.manage_member')}}</h4>
                                <p>{{trans('base.manage_member_description')}}</p>
                            </div>
                        </div>
                    </a>
                    <a href="{!!route('frontend.member.index')!!}" class='go-box'> {{trans('base.go')}} >></a>
                </div>
                <div class="col-md-5" style="margin-bottom: 50px">
                    <a href="{!!route('frontend.part.index')!!}" class='div-choose'>
                        <div class="row box-choose">
                            <div class="col-md-4" style="text-align: center;background: #fd7600;border-radius: 50%;padding: 15px;">
                                <img src="{!!asset('assets2/img/gear.png')!!}">
                            </div>
                            <div class="col-md-8" style="padding-left: 36px;">
                                <h4>{{trans('base.system_management')}}</h4>
                                <p>{{trans('base.system_management_description')}}<p>
                            </div>
                        </div>
                    </a>
                    <a href="{{route('frontend.part.index')}}" class='go-box'> {{trans('base.go')}} >></a>
                </div>
                @endif
            </div>
        </div>
    </div>
</body>
@stop