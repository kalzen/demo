@extends('frontend.layouts.master')
@section('content')
<header>
    <div class="bottom-header" style="background: #333333;">
        <div class="container">
            <div class="row">
                <div class="bottom-header-left col-md-3">
                    <div class="logo-header">
                        <a href="/"><img src="{!!asset('assets2/img/logo1.jpg')!!}"></a>
                    </div>
                </div>
                <div class="bottom-header-right col-md-7" style="padding-top: 18px;text-align: right;display: block;">
                    <form action="{!!route('loginMember')!!}" method="POST">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <div class="form-group">
                            <input type="text" class="form-control" name='login_id' placeholder="COMPANY ID">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name='password' placeholder="PASSWORD">
                        </div>
                        <div class="form-group">
                            <button type='submit' class="btn btn-color">Login</button>
                        </div>
                    </form>
                </div>
                <div class="top-header-right col-md-2">
                    <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex" style="padding-top:18px;">
                        <li class="nav-item dropdown">
                          <a class="nav-item nav-link dropdown-toggle mr-md-2 nav-header" style="color:#fff" href="#" id="bd-versions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {!!trans('base.language')!!}
                          </a>
                          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="bd-versions">
                            <a class="dropdown-item @if(session('locale') == 'en') active @endif" href="{{route('frontend.language.change',['locale'=>'en'])}}">Tiếng anh</a>
                            <a class="dropdown-item @if(session('locale') == 'vi') active @endif" href="{{route('frontend.language.change',['locale'=>'vi'])}}">Tiếng Việt</a>
                          </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="slider" style="position:relative">
    <img src='{!!asset('assets2/img/banner.png')!!}' style="width:100%">
    <div class='slide-member'>
        <div class="owl-carousel slider-carousel">
            <div class="item">
                <h5>{{trans('base.creatorofmonth')}}</h5>
                <table class="table-slider">
                    <thead>
                        <th></th>
                        <th>{{trans('base.name')}}</th>
                        <th>ID</th>
                        <th>{{trans('base.department')}}</th>
                        <th>{{trans('base.noproject')}}</th>
                    </thead>
                    <tbody>
                        @foreach($rank_month as $key=>$val)
                        <tr>
                            <td><span @if($key == 0) class="active" @endif>{{$key + 1}}</span></td>
                            <td @if($key == 0) class="active" @endif>{{\App\Member::find($val->id)->full_name}}</td>
                            <td @if($key == 0) class="active" @endif>{{\App\Member::find($val->id)->login_id}}</td>
                            <td @if($key == 0) class="active" @endif>{{\App\Member::find($val->id)->department->name}}</td>
                            <td @if($key == 0) class="active" @endif>{{$val->count}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="item">
                <h5>{{trans('base.creatorofyear')}}</h5>
                <table class="table-slider">
                    <thead>
                        <th></th>
                        <th>{{trans('base.name')}}</th>
                        <th>ID</th>
                        <th>{{trans('base.department')}}</th>
                        <th>{{trans('base.noproject')}}</th>
                    </thead>
                    <tbody>
                        @foreach($rank_year as $key=>$val)
                        <tr>
                            <td><span @if($key == 0) class="active" @endif>{{$key + 1}}</span></td>
                            <td @if($key == 0) class="active" @endif>{{\App\Member::find($val->id)->full_name}}</td>
                            <td @if($key == 0) class="active" @endif>{{\App\Member::find($val->id)->login_id}}</td>
                            <td @if($key == 0) class="active" @endif>{{\App\Member::find($val->id)->department->name}}</td>
                            <td @if($key == 0) class="active" @endif>{{$val->count}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<body class="page-body">
<main class="main-content">
    <section class="content-section owl-carousel-spotlight carousel-spotlight ig-carousel">
        <div class="container">
            <header class="header">
                <h5>@lang('base.TYPICAL')</h5>
            </header>
            <div class="position-relative">
                <div id="color_sel_Carousel-content" class="tab-content fl-scn relative w-100" style="padding:20px">
                    <!-- tab item -->
                    <div class="tab-pane fade show active" id="mp-01-c" role="tabpanel" aria-labelledby="mp-01-tab">
                        <div class="owl-carousel gs-carousel" data-carousel-margin="30" data-carousel-nav="true" data-carousel-navText="<span class='icon-cl-next pe-7s-angle-left'></span>, <span class='icon-cl-next pe-7s-angle-right'></span>">
                            @foreach($slides as $key=>$slide)
                            <div class="item">
                                <div class="item-cont">
                                    <figure class="owl_item_review">
                                        <div>
                                            <div class="position-relative overflow-hidden overlay-gradient">
                                                <img class="m-0-auto" src="{!!$slide->image!!}" alt="{{$slide->title}}" style="height:600px">
                                                <div class='over-text'>
                                                    <h5>{{$slide->title}}</h5>
                                                </div>
                                                <div class="overlay overlay-gradient"></div>
                                            </div>
                                        </div>
                                    </figure>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
</body>
@stop
@section('script')
@parent
@if (Session::has('error'))
<script>
    var notifier = new Notifier();
    var notification = notifier.notify("warning", "Đăng nhập không thành công");
    notification.push();
</script>
@endif
@stop