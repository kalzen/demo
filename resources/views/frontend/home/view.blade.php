@extends('frontend.layouts.create_schedule')
@section('content')
<body class="page-body">
   <div class="col-md-12" style="padding-top: 15px;">
   <div class="portlet_base_grn ">
      <style type="text/css">
         .tableFixed{
         width:100%;
         table-layout:fixed;
         }
         .normalEvent {}
         .normalEventElement{}
         .showEventTitle{
         position:absolute;
         max-width: 300px;
         overflow:hidden;
         z-index: 20;
         }
         .hideEventTitle{}
         .hideEventTitle .normalEvent {
         overflow:hidden;
         vertical-align:top;
         }
         .userElement{
         overflow:hidden;
         }
         .nowrap_class{
         white-space:nowrap;
         padding:2px;
         overflow:hidden;
         }
      </style>
      <style type="text/css">
         .differ_tz_color{
              background-color:#FFDBDE;
         }
         .hide_event{
              display:none;
         }
      </style>
      <table class="top_title">
         <tbody>
            <tr>
               <td><span class="portlet_title_grn"><a href="/schedule/index?">Scheduler Groups (Week)</a></span></td>
               <td align="right">
                  <div class="search_navi">
                     <form name="user_week336" method="GET" action="/schedule/index?">
                        <input type="hidden" name="gid" value="search">
                        <input type="hidden" name="type_search" value="user">
                        <div class="searchboxChangeTarget-grn">
                           <div class="searchbox-grn">
                              <div class="searchbox-select-schedule"></div>
                              <div id="searchbox-schedule-336" class="input-text-outer-schedule searchbox-keyword-schedule">
                                 <input type="text" name="search_text" value="User/Facility search" autocomplete="off" class="input-text-schedule prefix-grn" id="schedules_search_text" maxlength="100">
                                 <input type="button" id="searchbox-submit-schedules-336" class="searchbox-submit-schedule" onclick="JavaScript:void(0);">
                              </div>
                           </div>
                        </div>
                     </form>
                  </div>
               </td>
            </tr>
         </tbody>
      </table>
      <form name="schedule/index" method="GET" action="/schedule/index?">
         <input type="hidden" name="bdate" value="{{date('Y-m-d')}}">
         <div class="portal_frame portal_frame_schedule_groupweek_grn">
            <div class="margin_bottom">
                <table width="100%">
                   <tbody>
                      <tr>
                         <td class="v-allign-middle" nowrap="nowrap">
                            <table cellspacing="0" cellpadding="0" border="0">
                               <tbody>
                                  <tr>
                                     <td nowrap="nowrap">
                                        <link href="{!!asset('assets/css/fag_tree.css')!!}" rel="stylesheet" type="text/css">
                                        <table id="group-select" border="0" cellspacing="0" cellpadding="0" class="wrap_dropdown_menu" style="width: 332px;">
                                           <tbody>
                                              <tr height="20">
                                                 <td id="title" class="dropdown_menu_current" height="20" nowrap="">Chọn theo phòng ban</td>
                                                 <td id="user-button" class="dropdown_menu_user" style="margin:0px;paddig:0px;" valign="center" aligh="left" width="37" height="20">
                                                    <img src="{{asset('/img/user-dropdown30x20.gif')}}" style="margin:0px;paddig:0px;" border="0">
                                                 </td>
                                                 <td id="facility-button" class="dropdown_menu_facility" style="margin:0px;paddig:0px;" valign="center" aligh="left" width="37" height="20">
                                                    <img src="{{asset('/img/facility-dropdown30x20.gif')}}" style="margin:0px;paddig:0px;" border="0">
                                                 </td>
                                              </tr>
                                           </tbody>
                                        </table>
                                        <div id="user-popup" class="wrap_dropdown_option" style="visibility: visible; display: none;">
                                        </div>
                                        <div id="facility-popup" class="wrap_dropdown_option" style="display: none;">  
                                        </div>
                                        <div id="dummy-popup" class="wrap_dropdown_option"></div>
                                        <div></div>
                                        <div id="facility-popup-dummy_root" style="border:1px solid #000000; top:-10000px; left:-10000px; position:absolute; background-color:#FFFFFF; overflow:scroll; width:1px;height:1px;">
                                           <div id="facility-popup-dummy_tree_wrap_tree1" class="wrap_tree1" style="width: 320px;">
                                              <div id="facility-popup-dummy_tree_wrap_tree2" class="wrap_tree2" style="width: 298px; overflow-x: scroll; height: 240px;">
                                                 <div id="facility-popup-dummy_tree">
                                                    <div class="ygtvitem">
                                                       
                                                    </div>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>
                                        <script type="text/javascript">
                                           (function () {

                                               var group_select_id    = 'group-select';
                                               var title_id           = 'title';
                                               var user_button_id     = 'user-button';
                                               var facility_button_id = 'facility-button';
                                               var user_popup_id      = 'user-popup';
                                               var facility_popup_id  = 'facility-popup';
                                               var is_multi_view      = false;

                                               var dropdown = new GRN_DropdownMenu(
                                                   group_select_id, title_id, user_button_id, facility_button_id,
                                                   GRN_DropdownMenu.prototype.PreferOrganization,
                                                   user_popup_id, facility_popup_id,
                                                   clickOrganizationCallback, clickFacilityGroupCallback,
                                                   "" );

                                               function updateTitle( title ) {
                                                   var old_width = jQuery('#' + group_select_id).outerWidth();
                                                   jQuery('#' + group_select_id).css( {'width':''} );
                                                   jQuery('#' + title_id).html( title );
                                                   if( old_width > jQuery('#' + group_select_id).outerWidth() ) {
                                                       jQuery('#' + group_select_id).css( { 'width': old_width + 'px'} );
                                                   }
                                               }

                                               function clickOrganizationCallback( group_item ) {
                                                   return function(){
                                                       updateTitle( group_item.name )
                                                       dropdown.organization.hide();

                                                       if (is_multi_view) {

                                                           jQuery(document).trigger("scheduler.select_user_org_facility_dropdownlist.select", {gid: group_item.gid, target: dropdown});
                                                           return;
                                                       }
                                                       location.href = "/schedule/index?"+ '&bdate=' + document.forms["schedule/index"].bdate.value + '&gid='+group_item.gid;
                                                   }
                                               }

                                               function clickFacilityGroupCallback( node ) {
                                                   if( node.extra_param ) { 
                                                       updateTitle( node.label );
                                                   }
                                                   else {
                                                       if( node.oid == 'f' ) {
                                                           updateTitle( '(All facilities)' );
                                                       }else{
                                                           updateTitle(  node.label + ' (Facility group)' );
                                                       }
                                                   }
                                                   dropdown.facility.hide();

                                                   var oid = node.oid;
                                                   

                                                   location.href = "/schedule/index?"+ '&bdate=' + document.forms["schedule/index"].bdate.value + '&eid='+oid + '&p='+node.extra_param;
                                               }
                                               dropdown.initializeOrganization(
                                                   new Array(
                                                   {!!$department!!}
                                                            ) );

                                               var group_select_width = dropdown.organization.getWidth( jQuery('#' + title_id).outerWidth() );
                                               jQuery('#' + group_select_id).css( {'width': group_select_width +"px"} );

                                               dropdown.updateTitle = updateTitle;

                                               dropdown.initializeFacilityGroup( { 'page_name': "schedule/index",
                                                                                   'ajax_path':'/schedule/json/accessible_facility_tree?',
                                                                                   'csrf_ticket':'20f137f8be0895ef4f146389639bbf0c',
                                                                                   'callback':clickFacilityGroupCallback,
                                                                                   'selectedOID':"x",
                                                                                   'title_width': jQuery('#' + title_id).outerWidth(),
                                                                                   'node_info':
                                                                                   [{!!$equipment!!}]
                                                                                 });
                                           }());

                                        </script>
                                     </td>
                                     <td nowrap="nowrap"><a class="selectPulldownSub-grn" href="javascript:void(0);" onclick="javascript:popupWin('/schedule/popup_user_select?plugin_session_name=schedule%2Findex&amp;plugin_data_name=access_plugin&amp;is_post_message=&amp;selected_tid=&amp;form_name=schedule%2Findex&amp;select_name=sUID%5B%5D&amp;app_id=schedule&amp;return_page=&amp;plid=&amp;system=0&amp;include_org=1&amp;system_display=0&amp;no_multiple=0&amp;send_cgi_parameter=0&amp;multi_apply=1&amp;require_role_tab=0&amp;is_calendar=0&amp;session=start;show_group_role=1&amp;require_dynamic_roles=0&amp;require_administrator_role=0','html','','',0,0,1,1,0,1);" title="Select users"><img src="{!!asset('/img/blankB16.png')!!}" border="0" alt=""></a></td>
                                  </tr>
                               </tbody>
                            </table>
                         </td>
                         <td class="v-allign-middle" align="center">
                            <span class="displaydate">Tue, January 12, 2021</span>
                         </td>
                         <td class="v-allign-middle" nowrap="nowrap" align="right">
                            <div class="moveButtonBlock-grn">
                               <span class="moveButtonBase-grn">
                                   <a href="javascript:void(0)" class='load-view' data-action ='prev' data-day='7' title="Previous week"><span class="moveButtonArrowLeftTwo-grn"></span></a>  
                               </span>
                               <span class="moveButtonBase-grn">
                                    <a href="javascript:void(0)" class='load-view' data-action ='prev' data-day='1' title="Previous day"><span class="moveButtonArrowLeft-grn"></span></a>    
                                </span>
                                <span class="moveButtonBase-grn" title=""><a href="javascript:void(0)" class='load-today' data-start_day='{{date('Y-m-d')}}' data-action ='next' data-day='0'>Today</a></span>
                                <span class="moveButtonBase-grn"><a href="javascript:void(0)" class='load-view' data-action ='next' data-day='1' title="Next day"><span class="moveButtonArrowRight-grn"></span></a></span>
                                <span class="moveButtonBase-grn"><a href="javascript:void(0)" class='load-view' data-action ='next' data-day='7' title="Next week"><span class="moveButtonArrowRightTwo-grn"></span></a></span>
                            </div>
                         </td>
                      </tr>
                   </tbody>
                </table>
             </div>
            <div id="gw_336_cal_div" class="js_customization_schedule_view_type_GROUP_WEEK">
               <table id="schedule_groupweek_336" class="calendar scheduleWrapper scheduleWrapperGroupWeek portlet_groupweek " width="100%" cellspacing="0" cellpadding="0" style="font-size:100%;;">
                  {!!$html!!}
               </table>
            </div>
            <script src="{!!asset('/js/schedule_gw.js')!!}" type="text/javascript"></script>
         </div>
         <!--end of portal_frame -->
      </form>
      <script src="{!!asset('/js/dragdrop-min.js')!!}" type="text/javascript"></script>
      <script src="{!!asset('/js/selector-min.js')!!}" type="text/javascript"></script>
      <script src="{!!asset('/js/schedules.js')!!}" type="text/javascript"></script>
      <script src="{!!asset('/js/schedule.js')!!}" type="text/javascript"></script>
   </div>
   </div>
   <div class="row" style="padding: 0 15px;">
        <div class="col-md-8">
            <div class="dashboard">
                <div class="dashboardHeader">
                    <div class="pull-left" style="color:#fe7701">Notifications</div>
                    <div class='tap-all2'>
                        <a class='tap-all active notifitap' data-status ='0' href="javascript:void(0)">Unread</a> 
                         <a class='tap-all notifitap' data-status ='1' href="javascript:void(0)">Read</a>    
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="dashboardContent" style="overflow-y: scroll;width:100%;height:125px;">
                    <div class="table-content" style="margin-top:0px;padding:0 20px;">
                    <table class="list_column">
                           <colgroup>
                              <col width="15%">
                              <col width="50%">
                              <col width="25%">
                           </colgroup>
                           <tbody id='list_notification'>
                               <tr valign="top">
                                    <th nowrap="">
                                       <div style="padding-top: 2px;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Người gửi</font></font></div>
                                    </th>
                                    <th nowrap="">Nội dung</th>
                                    <th nowrap="">Ngày tạo</th>
                                </tr>
                                @foreach( \Auth::guard('member')->user()->unreadNotifications as $val)
                                <tr valign="top">
                                    <td nowrap=""><span class="selectlist_user_grn"></span>{{$val->data['full_name']}}</th>
                                    <td nowrap="">
                                        <a href="{{$val->data['link']}}">{{$val->data['content']}}</a>
                                    </td>
                                    <td nowrap="">{{date('d/m',strtotime($val->created_at))}} ({{$val->data['time']}})</th>
                                </tr>
                                @endforeach
                           </tbody>
                        
                    </table>
                    </div>
                </div>
            </div>
            <div class="dashboard">
                <form name="todo/index" method="post" action="{!!route('frontend.todo.update_status')!!}">
                <div class="dashboardHeader">
                    <div class="pull-left">To-do List</div>
                    <div class='tap-all1'>
                        <a class='tap-all active todo-tap' data-status="pending" href="javascript:void(0)">Uncomplete</a> 
                         <a class='tap-all todo-tap' data-status="complete" href="javascript:void(0)">Complete</a>    
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="dashboardContent" style="overflow-y: scroll;width:100%;height:216px;">
                    <div class="table-content" style="margin-top:0px;padding:0 20px;">                                                                                                                    
                        <div class="list_menu">
                           <span class="list_menu_item">

                              <button class="check_button" style="width:20px; height:20px" onclick="javascript:CheckAll();return false"><img src="{{asset('/img/check10.gif')}}" border="0" alt="Chọn / Bỏ chọn Tất cả" title="Chọn / Bỏ chọn Tất cả"></button>
                           </span>
                            <span class="list_menu_item check-list-todo"><a class="complete-list-todo" href="javascript:void(0)" style='height:28px!important;' onclick="" style="">Complete</a></span>  
                        </div>
                        <table class="list_column">
                           <colgroup>
                              <col width="1%">
                              <col width="35%">
                              <col width="25%">
                              <col width="25%">
                              <col width="15%">
                           </colgroup>
                           <tbody id='list_todo'>
                              <tr valign="top">
                                 <th nowrap=""></th>
                                 <th nowrap="">
                                    <div style="padding-top: 2px;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">To-do</font></font></div>
                                 </th>
                                 <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Status</font></font></th>
                                 <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Due date</font></font></th>
                                 <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Priority</font></font></th>
                              </tr>
                              @foreach($todos as $key=>$todo)
                              <tr class="linetwo">
                                <td nowrap=""><input type="checkbox" name="id[]" id="id[]" class="" value="{{$todo->id}}"></td>
                                <td><span class="nowrap-grn "><a href="{{route('frontend.todo.view',$todo->id)}}">{{$todo->title}}</a></span></td>
                                <td nowrap=""><span class="badge badge-danger">{{$todo->nameStatus()}}</span></td>
                                <td nowrap="">{{$todo->end_date()}}</td>
                                <td nowrap="">
                                    <span class="badge badge-danger">{{$todo->namePriority()}}</span>
                                </td>
                             </tr>
                              @endforeach
                           </tbody>
                        </table>                                                                                                                                                                                                                                  
                    </div>
                </div>
                </form>
            </div>
        </div>
        <div class="col-md-4">
            <div class="dashboard">
                <div class="dashboardHeader">
                    <div class="pull-left">Time set</div>
                    <div class="clearfix"></div>
                </div>
                <div class="dashboardContent">
                    <div class="table-content">
                        <div class='row'>
                            <div class='col-md-6'>
                                <p style="margin-left:30px"> Start at 7:45 AM </p>
                            </div>
                            <div class='col-md-6'>
                                <a class='tap-all active'>End</a> 
                                <a class='tap-all'>Out</a>   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dashboard">
                <div class="dashboardHeader">
                    <div class="pull-left">Kaizen systems</div>
                    
                    <div class="clearfix"></div>
                </div>
                <div class="dashboardContent" style="overflow-y: scroll;width:100%;height:216px;">
                    <div class="table-content" style="margin-top:0px;">
                            <div class="col-md-12">
                                <fieldset>
                                    <legend><span class="orange">TOP TEAMS</span> of Month</legend>
                                    @foreach($rank_team as $key=>$val)
                                    <p>{{$key + 1}}. Tem {{$val->name}} <span>{{$val->count}} đề án</span></p>
                                    @endforeach
                                </fieldset>
                            </div>
                            <div class="col-md-12">
                                <fieldset>
                                    <legend><span class="orange">TOP USER</span> of Quarter</legend>
                                    <table class="top-user" style="margin:0px;">
                                        <tbody>
                                            @foreach($rank_quarter as $key=>$val)
                                            <tr>
                                                <td style="width:40%"><p>{{$key + 1}}. {{\App\Member::find($val->id)->full_name}}</p></td>
                                                <td><p>{{\App\Member::find($val->id)->login_id}}</p></td>
                                                <td><p>Team @if(\App\Member::find($val->id)->team) {{\App\Member::find($val->id)->team->name}} @endif</p></td>
                                                <td><p>{{$val->count}} đề án</p></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </fieldset>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
@stop
@section('script')
@parent
<script>
   $('body').delegate('.load-view','click',function(){
        var bdate = $('#day_start').val();
        var day = $(this).data('day');
        var action = $(this).data('action');
        $.ajax({
            url:'/api/load-schedule',
            method:'POST',
            data:{bdate:bdate,day:day,action:action},
            success: function(response){
                $('#schedule_groupweek_336').html(response.html);
                $('.displaydate').html(response.date);
            }
        })
   })
   $('body').delegate('.load-today','click',function(){
        var bdate = $('#day_start').val();
        var day = 0;
        var action = $(this).data('action');
        $.ajax({
            url:'/api/load-schedule',
            method:'POST',
            data:{bdate:bdate,day:day,action:action},
            success: function(response){
                $('#schedule_groupweek_336').html(response.html);
                $('.displaydate').html(response.date);
            }
        })
   })
   $('.todo-tap').click(function(){
        var status = $(this).data('status');
        $('.todo-tap').removeClass('active');
        $(this).addClass('active');
        $.ajax({
            url:'/api/load-todo',
            method:'POST',
            data:{status:status},
            success: function(response){
                $('#list_todo').html(response.html);
                $('.check-list-todo').html(response.check_list_html);
            }
        })
   })
   $('body').delegate('.notifitap','click',function(){
        var status = $(this).data('status');
        $('.notifitap').removeClass('active');
        $(this).addClass('active');
        $.ajax({
            url:'/api/load-notification',
            method:'POST',
            data:{status:status},
            success: function(response){
                $('#list_notification').html(response.html);
                $('.check-list-todo').html(response.check_list_html);
            }
        })
    })
   $('body').delegate('.complete-list-todo','click',function(){
        var id = [];
        $("input[type=checkbox][name='id[]']:checked").each(function(){
            id.push($(this).val());
        });
        $.ajax({
            url:'/api/complete-list-todo',
            method:'POST',
            data:{id:id},
            success: function(response){
                $('#list_todo').html(response.html);
            }
        })
   })
   $('body').delegate('.delete-list-todo','click',function(){
        var id = [];
        $("input[type=checkbox][name='id[]']:checked").each(function(){
            id.push($(this).val());
        });
        $.ajax({
            url:'/api/delete-list-todo',
            method:'POST',
            data:{id:id},
            success: function(response){
                $('#list_todo').html(response.html);
            }
        })
   })
</script>
<script language="JavaScript" type="text/javascript">
    var enable_shift_click = false;
    var element_name = 'id[]';
    function CheckAll()

    {
        var e = document.forms["todo/index"].elements;
        var l = e.length;
        var checked = false;
        for( i = 0 ; i < l ; i ++ ) { if( e[i].name == "id[]" && e[i].style.display=="" && ! e[i].checked  ) { checked = true;break; } }
        for( i = 0 ; i < l ; i ++ ) { if( e[i].name == "id[]" && e[i].style.display=="" ) { e[i].checked=checked; } }
        if( typeof setHightlight == 'function' )
            setHightlight();
    }

    applyShiftClick();

    function setHightlight()
    {
        if (!enable_shift_click)
            return;

        var table = jQuery("table.list_column").get(0);
        if (table) {
            var rows = jQuery(table).find("tr");
            rows.each(function (index, row) {
                var row_jquery_obj = jQuery(row);
                var chkbox = row_jquery_obj.find("input[type='checkbox']").eq(0);
                if (chkbox.length == 0)
                    return;

                if (chkbox.is(":checked")) {
                    row_jquery_obj.addClass("row_highlight");
                }
                else {
                    row_jquery_obj.removeClass("row_highlight");
                }

            });
        }
    }
    function applyShiftClick()
    {
        if (!enable_shift_click)
            return;

        var list = jQuery("table.list_column").get(0);
        var row = null;
        var row2 = null;
        if (list) {
            var chkboxs = fastGetElementsByName(list, 'input', element_name);
            var i = 0;
            jQuery.each(chkboxs, function (index, chkbox) {
                chkbox.index = i;

                var chkbox_jpuery_object = jQuery(chkbox);
                chkbox_jpuery_object.on("click", function (e) {
                    row = chkbox_jpuery_object.parents("tr").eq(0);
                    if (this.checked) {
                        row.addClass("row_highlight");
                    }
                    else {
                        row.removeClass("row_highlight");
                    }

                    //handle Shift click
                    var last_click = list.last_click;
                    var last_click_index = last_click ? last_click.index : 0;
                    var current = this;
                    var current_index = this.index;
                    var low = Math.min(last_click_index, current_index);
                    var high = Math.max(last_click_index, current_index);

                    if (e.shiftKey) {
                        var chkboxs2 = fastGetElementsByName(list, 'input', element_name);
                        jQuery(chkboxs2).each(function (index, item) {
                            row2 = jQuery(item).parents("tr").eq(0);

                            if (item.index >= low && item.index <= high) {
                                item.checked = current.checked;
                            }

                            if (item.checked) {
                                row2.addClass('row_highlight');
                            }
                            else {
                                row2.removeClass('row_highlight');
                            }
                        });
                    }
                    list.last_click = e.target;
                });
                i++;
            });
        }
    }
 </script>
@stop
