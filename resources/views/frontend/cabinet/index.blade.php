@extends('frontend.layouts.create_schedule')
@section('content')
<script src="{!!asset('/js/cabinet.js')!!}" type="text/javascript"></script>
<script src="{!!asset('/js/star.js')!!}" type="text/javascript"></script>
<script src="{!!asset('/js/download.js')!!}" type="text/javascript"></script>
<table class="global_navi_title" cellpadding="0" cellmargin="0" style="padding:0px;">
   <tbody>
      <tr>
         <td width="100%" valign="bottom" nowrap="">
            <div role="heading" aria-level="2">
               <img src="{{asset('/img/file20.gif')}}" border="0" alt="">Cabinet
            </div>
         </td>
         <td align="right" valign="bottom" nowrap="">
         </td>
      </tr>
   </tbody>
</table>
<div class="mainarea">
   <div id="js_cabinet_main_menu">
      <div id="menu_part">
         <div id="smart_main_menu_part">
            <span class="menu_item">
            <span class="nowrap-grn "><a href="{{route('frontend.cabinet.create')}}"><img src="{{asset('/img/write20.gif')}}" border="0" alt="">Add file</a></span>
            </span>
            <span class="menu_item">
               <nobr>
                  <a href="javascript:" onclick="__bundleDownLoad.showBundleDownloadWindow();"><img src="{{asset('/img/downloadmore20.gif')}}" border="0" alt="">Download multiple files</a>
               </nobr>
               &nbsp;
            </span>
            <span class="menu_item">
               <nobr>
                  <a href="{{route('frontend.cabinet.create_folder')}}"><img src="{{asset('/img/folder20.gif')}}" border="0" alt="">Add folder</a>
               </nobr>
               &nbsp;
            </span>
            &nbsp;
            <span class="menu_item">
               <script src="{{asset('/js/display_options.js')}}" type="text/javascript"></script>
               <link href="{{asset('assets/css/display_options.css')}}" rel="stylesheet" type="text/css">
               <script type="text/javascript" language="javascript">
                  <!--
                  
                  GRN_DisplayOptions[''] = GRN_DisplayOptionFactory.create();
                  
                  GRN_DisplayOptions[''].setPage('');
                  GRN_DisplayOptions[''].setCSRFTicket('b0955c3c0ece7761126bd762c9073898');
                  GRN_DisplayOptions[''].setListID('');
                  GRN_DisplayOptions[''].setPLID('');
                  jQuery(document).ready(function(){
                      GRN_DisplayOptions[''].init();
                  });
                  
                  //-->
               </script>
            </span>
         </div>
         <div id="smart_rare_menu_part">
            <div align="right" valign="bottom" nowrap="">
               <div class="search_navi">
                  <form name="search" action="/scripts/garoon/grn.exe/cabinet/search?"><input type="hidden" name="hid" value="1"><input type="hidden" name="s_hid" value="1"><input type="text" name="text" size="20" maxlength="100">&nbsp;<input class="small" type="submit" value="Cabinet search">&nbsp;</form>
               </div>
            </div>
         </div>
      </div>
   </div>
   <table class="maincontents_list tree_width_ajust_grn js_cabinet_index" style="visibility: visible;">
      <tbody>
         <tr>
            <td class="maincontents_list_td">
               <table class="table_grn" style="width:100%">
                  <tbody>
                     <tr valign="top">
                        <td id="tree_part" style="width: 190px; height: 296px; position: relative;">
                           <script type="text/javascript" src="{{asset('/js/yahoo-min.js')}}"></script>
                           <script type="text/javascript" src="{{asset('/js/event-min.js')}}"></script>
                           <script type="text/javascript" src="{{asset('/js/dom-min.js')}}"></script>
                           <script type="text/javascript" src="{{asset('/js/treeview-min.js')}}"></script>
                           <script type="text/javascript" src="{{asset('/js/connection-min.js')}}"></script>
                           <script type="text/javascript" src="{{asset('/js/org_tree_26.js')}}"></script>
                           <script language="javascript">
                              <!--
                              YAHOO.grn.orgTree.csrf_ticket= "b0955c3c0ece7761126bd762c9073898";
                              YAHOO.grn.orgTree.app_path= "/garoon3";
                              //-->
                           </script>
                           <div class="js_tree_view_position" style="width: 190px;"></div>
                           <div id="tree_view" class="tree_view_scroll_grn scrollbar_s_grn js_tree_view nowrap-grn" style="width: 190px; height: 296px;">
                              <span id="folder_tree_top" class="tree-node">
                              <a class="js_root tree-select-current" href="javascript:">Library</a>
                              </span>
                              <div id="folder_tree" class="js_tree_folder">
                                 <div class="ygtvitem" id="ygtv0">
                                    <div class="ygtvchildren" id="ygtvc0">
                                    @foreach($folders as $folder)
                                       <div class="ygtvitem" id="ygtv1">
                                          <table id="ygtvtableel1" border="0" cellpadding="0" cellspacing="0" class="ygtvtable ygtvdepth0 ygtv-collapsed ygtv-highlight0">
                                             <tbody>
                                                <tr class="ygtvrow">
                                                   <td id="ygtvt1" class="ygtvcell ygtvtp"><a href="javascript:void(0)" class="ygtvspacer"></a></td>
                                                   <td id="ygtvcontentel1" class="ygtvcell ygtvhtml ygtvcontent">
                                                       <div class="tree-node"><a id="folder_tree-node-2" href="{{route('frontend.cabinet.index',['folder_id'=>$folder->id])}}"><img src="http://schedule.local/img/folder20.gif" border="0" alt="">  {{$folder->name}}</a> <span class="tree-unread-num" style="display:none;" id="tree-unread-num-2">0</span></div>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                           <div class="ygtvchildren" id="ygtvc1" style="display:none;">
                                                <div class="ygtvitem" id="ygtv10">
                                                    <table id="ygtvtableel10" border="0" cellpadding="0" cellspacing="0" class="ygtvtable ygtvdepth1 ygtv-collapsed ygtv-highlight0">
                                                       <tbody>
                                                          <tr class="ygtvrow">
                                                             
                                                             <td id="ygtvt10" class="ygtvcell ygtvtn"><a href="javascript:void(0)" class="ygtvspacer"></a></td>
                                                             <td id="ygtvcontentel10" class="ygtvcell ygtvhtml ygtvcontent">
                                                                <div class="tree-node"><img src="http://schedule.local/img/folder20.gif" border="0" alt=""><a id="folder_tree-node-13" href="/scripts/garoon/grn.exe/cabinet/index?hid=13">Thư mục con</a> <span class="tree-unread-num" style="display:none;" id="tree-unread-num-13">0</span></div>
                                                             </td>
                                                          </tr>
                                                       </tbody>
                                                    </table>
                                                    <div class="ygtvchildren" id="ygtvc10" style="display:none;"></div>
                                                </div>
                                                <div class="ygtvitem" id="ygtv10">
                                                    <table id="ygtvtableel10" border="0" cellpadding="0" cellspacing="0" class="ygtvtable ygtvdepth1 ygtv-collapsed ygtv-highlight0">
                                                       <tbody>
                                                          <tr class="ygtvrow">
                                                             
                                                             <td id="ygtvt10" class="ygtvcell ygtvtn"><a href="javascript:void(0)" class="ygtvspacer"></a></td>
                                                             <td id="ygtvcontentel10" class="ygtvcell ygtvhtml ygtvcontent">
                                                                <div class="tree-node"><img src="http://schedule.local/img/folder20.gif" border="0" alt=""><a id="folder_tree-node-13" href="/scripts/garoon/grn.exe/cabinet/index?hid=13">Thư mục con 1</a> <span class="tree-unread-num" style="display:none;" id="tree-unread-num-13">0</span></div>
                                                             </td>
                                                          </tr>
                                                       </tbody>
                                                    </table>
                                                    <div class="ygtvchildren" id="ygtvc10" style="display:none;"></div>
                                                </div>
                                           </div>
                                       </div>
                                    @endforeach  
                                    </div>
                                 </div>
                                 <table border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                       <tr class="ygtvrow">
                                          <td class="ygtvcell ygtvln">
                                             <a href="#" class="ygtvspacer"></a>
                                          </td>
                                          <td class="ygtvcell ygtvhtml ygtvcontent">
                                             <div class="tree-node">
                                                 <a href="{{route('frontend.cabinet.index',['is_deleted'=>1])}}">Trash</a>
                                             </div>
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                           <div class="trigger_area_grn" title="Drag to move" style="position: fixed; top: 232px; left: 206px; height: 146px;"></div>
                                                     
                        </td>
                        <td id="view_part" class="js_list_content">
                           <div id="js_cabinet_memo"></div>
                           <div id="js_cabinet_warning_label"></div>
                           <div class="spinnerBoxBase-grn spinnerCentered js_list_spinner" style="display: none;">
                              <div class="spinnerBox-grn"></div>
                           </div>
                           <div class="js_pagination_top">
                              <div class="navi">
                                 <div class="list_paging_grn inline_block_grn js_inner"><span class="action_disable_grn top_page_off"><span class="arrow_first_button_grn" role="button" title="First row" aria-disabled="true" aria-label="First row"></span></span><span class="action_disable_grn prev_page_off"><span class="arrow_left_button_grn" role="button" title="Previous " aria-disabled="true" aria-label="Previous "></span></span><span class="action_disable_grn next_page_off"><span class="arrow_right_button_grn" role="button" title="Next " aria-disabled="true" aria-label="Next "></span></span></div>
                              </div>
                           </div>
                           <div class="js_content_body" style="">
                              <form name="cabinet/index" method="post" action="/scripts/garoon/grn.exe/cabinet/command_index?">
                                 <input type="hidden" name="csrf_ticket" value="b0955c3c0ece7761126bd762c9073898">
                                 <input type="hidden" name="hid" value="1">
                                 <input type="hidden" name="cmd">
                                 <div class="list_menu">
                                    <nobr>
                                       <span class="list_menu_item">
                                          <script language="JavaScript" type="text/javascript">
                                             <!--
                                             var enable_shift_click = false;
                                             var element_name = 'id[]';
                                             function CheckAll()
                                             
                                             {
                                                 var e = document.forms["cabinet/index"].elements;
                                                 var l = e.length;
                                                 var checked = false;
                                                 for( i = 0 ; i < l ; i ++ ) { if( e[i].name == "id[]" && e[i].style.display=="" && ! e[i].checked  ) { checked = true;break; } }
                                                 for( i = 0 ; i < l ; i ++ ) { if( e[i].name == "id[]" && e[i].style.display=="" ) { e[i].checked=checked; } }
                                                 if( typeof setHightlight == 'function' )
                                                     setHightlight();
                                             }
                                             
                                             // apply shift click
                                             
                                             applyShiftClick();
                                             
                                             function setHightlight()
                                             {
                                                 if (!enable_shift_click)
                                                     return;
                                             
                                                 var table = jQuery("table.list_column").get(0);
                                                 if (table) {
                                                     var rows = jQuery(table).find("tr");
                                                     rows.each(function (index, row) {
                                                         var row_jquery_obj = jQuery(row);
                                                         var chkbox = row_jquery_obj.find("input[type='checkbox']").eq(0);
                                                         if (chkbox.length == 0)
                                                             return;
                                             
                                                         if (chkbox.is(":checked")) {
                                                             row_jquery_obj.addClass("row_highlight");
                                                         }
                                                         else {
                                                             row_jquery_obj.removeClass("row_highlight");
                                                         }
                                             
                                                     });
                                                 }
                                             }
                                             function applyShiftClick()
                                             {
                                                 if (!enable_shift_click)
                                                     return;
                                             
                                                 var list = jQuery("table.list_column").get(0);
                                                 var row = null;
                                                 var row2 = null;
                                                 if (list) {
                                                     var chkboxs = fastGetElementsByName(list, 'input', element_name);
                                                     var i = 0;
                                                     jQuery.each(chkboxs, function (index, chkbox) {
                                                         chkbox.index = i;
                                             
                                                         var chkbox_jpuery_object = jQuery(chkbox);
                                                         chkbox_jpuery_object.on("click", function (e) {
                                                             row = chkbox_jpuery_object.parents("tr").eq(0);
                                                             if (this.checked) {
                                                                 row.addClass("row_highlight");
                                                             }
                                                             else {
                                                                 row.removeClass("row_highlight");
                                                             }
                                             
                                                             //handle Shift click
                                                             var last_click = list.last_click;
                                                             var last_click_index = last_click ? last_click.index : 0;
                                                             var current = this;
                                                             var current_index = this.index;
                                                             var low = Math.min(last_click_index, current_index);
                                                             var high = Math.max(last_click_index, current_index);
                                             
                                                             if (e.shiftKey) {
                                                                 var chkboxs2 = fastGetElementsByName(list, 'input', element_name);
                                                                 jQuery(chkboxs2).each(function (index, item) {
                                                                     row2 = jQuery(item).parents("tr").eq(0);
                                             
                                                                     if (item.index >= low && item.index <= high) {
                                                                         item.checked = current.checked;
                                                                     }
                                             
                                                                     if (item.checked) {
                                                                         row2.addClass('row_highlight');
                                                                     }
                                                                     else {
                                                                         row2.removeClass('row_highlight');
                                                                     }
                                                                 });
                                                             }
                                                             list.last_click = e.target;
                                                         });
                                                         i++;
                                                     });
                                                 }
                                             }
                                             
                                             //-->
                                          </script>
                                          <button class="check_button" style="width:20px; height:20px" onclick="javascript:CheckAll();return false"><img src="{{asset('/img/check10.gif')}}" border="0" alt="Select or clear all check boxes" title="Select or clear all check boxes"></button>
                                       </span>
                                       <input class="" type="button" value="Delete" onclick="return false;submit(this.form)" style="" id="btn_delete_multi1">&nbsp;
      
                                    </nobr>
                                 </div>
                                 <script language="javascript" text="text/javascript">
                                 var options = {url: '/scripts/garoon/grn.exe/star/ajax_request?',
                                               csrf_ticket: 'b0955c3c0ece7761126bd762c9073898',
                                               list_id: 'files_list'               };
                                 var obj_star_list = new GRN_StarList(options);
                                 </script>
                                 <table class="list_column" style="width:100%;" id="files_list">
                                    <colgroup>
                                       <col width="1%">
                                       <col width="1%">
                                       <col width="17%">
                                       <col width="5%">
                                       <col width="20%">
                                       <col width="20%">
                                       <col width="15%">
                                       <col width="10%">
                                    </colgroup>
                                    <tbody>
                                       <tr valign="top">
                                          <th nowrap=""></th>
                                          <th nowrap=""></th>
                                          <th nowrap="">
                                             Subject                                             
                                                @if(isset($_GET['sort']) && $_GET['sort'] == 'subject' && $_GET['sortby'] == 'DESC')
                                                <a href="{{route('frontend.cabinet.index',['sort'=>'subject','sortby'=>'ASC'])}}" title="Sort">
                                                   <img src="{{asset('/img/sortdown16.gif')}}" border="0" alt="Sort" title="Sort">
                                                </a>
                                                 @else
                                                <a href="{{route('frontend.cabinet.index',['sort'=>'subject','sortby'=>'DESC'])}}" title="Sort">
                                                   <img src="{{asset('/img/sortup16_n.gif')}}" border="0" alt="Sort" title="Sort">
                                                </a>
                                                @endif                                      
                                          </th>
                                          <th><br></th>
                                          <th nowrap="">
                                               File name
                                               @if(isset($_GET['sort']) && $_GET['sort'] == 'file_name' && $_GET['sortby'] == 'DESC')
                                                <a href="{{route('frontend.cabinet.index',['sort'=>'file_name','sortby'=>'ASC'])}}" title="Sort">
                                                   <img src="{{asset('/img/sortdown16.gif')}}" border="0" alt="Sort" title="Sort">
                                                </a>
                                                 @else
                                                <a href="{{route('frontend.cabinet.index',['sort'=>'file_name','sortby'=>'DESC'])}}" title="Sort">
                                                   <img src="{{asset('/img/sortup16_n.gif')}}" border="0" alt="Sort" title="Sort">
                                                </a>
                                                @endif          
                                          </th>
                                          <th nowrap="">
                                             Updater
                                             @if(isset($_GET['sort']) && $_GET['sort'] == 'updater' && $_GET['sortby'] == 'DESC')
                                                <a href="{{route('frontend.cabinet.index',['sort'=>'updater','sortby'=>'ASC'])}}" title="Sort">
                                                   <img src="{{asset('/img/sortdown16.gif')}}" border="0" alt="Sort" title="Sort">
                                                </a>
                                                 @else
                                                <a href="{{route('frontend.cabinet.index',['sort'=>'updater','sortby'=>'DESC'])}}" title="Sort">
                                                   <img src="{{asset('/img/sortup16_n.gif')}}" border="0" alt="Sort" title="Sort">
                                                </a>
                                            @endif         
                                          </th>
                                          <th nowrap="">
                                             Updated time
                                             @if(isset($_GET['sort']) && $_GET['sort'] == 'update_time' && $_GET['sortby'] == 'DESC')
                                                <a href="{{route('frontend.cabinet.index',['sort'=>'update_time','sortby'=>'ASC'])}}" title="Sort">
                                                   <img src="{{asset('/img/sortdown16.gif')}}" border="0" alt="Sort" title="Sort">
                                                </a>
                                                 @else
                                                <a href="{{route('frontend.cabinet.index',['sort'=>'update_time','sortby'=>'DESC'])}}" title="Sort">
                                                   <img src="{{asset('/img/sortup16_n.gif')}}" border="0" alt="Sort" title="Sort">
                                                </a>
                                            @endif         
                                          </th>
                                          <th nowrap="">
                                             Size
                                             @if(isset($_GET['sort']) && $_GET['sort'] == 'size' && $_GET['sortby'] == 'DESC')
                                                <a href="{{route('frontend.cabinet.index',['sort'=>'size','sortby'=>'ASC'])}}" title="Sort">
                                                   <img src="{{asset('/img/sortdown16.gif')}}" border="0" alt="Sort" title="Sort">
                                                </a>
                                                 @else
                                                <a href="{{route('frontend.cabinet.index',['sort'=>'size','sortby'=>'DESC'])}}" title="Sort">
                                                   <img src="{{asset('/img/sortup16_n.gif')}}" border="0" alt="Sort" title="Sort">
                                                </a>
                                            @endif        
                                          </th>
                                       </tr>
                                       @foreach($records as $key=>$record)
                                       <tr class="linetwo" valign="top">
                                           <td nowrap=""><input type="checkbox" name="id[]" value="{{$record->id}}"></td>
                                          <td><span style="display:inline;" class="star off" id="grn.cabinet:fid_2:hid_1" app_path="/garoon3/grn/image/cybozu/" onclick="obj_star_list._onClick(this);"><img src="{{asset('/img/star_off.png')}}" border="0" alt=""></span></td>
                                          <td nowrap="">
                                                  <span class="nowrap-grn "><a href="{{route('frontend.cabinet.view',$record->id)}}"><img src="{{asset('/img/file20.gif')}}" border="0" alt=""><span class="bold">{{$record->subject}}</span></a></span>
                                          </td>
                                          <td nowrap="">
                                              <span class="nowrap-grn "><a href="{{$record->link}}" download=""><img src="{{asset('/img/downloadbutton20.gif')}}" border="0" alt="Download" title="Download"></a></span>
                                             <span class="nowrap-grn">
                                                <a class="" href="{{route('frontend.cabinet.view_update',$record->id)}}">
                                             <img src="{{asset('/img/uploadbutton20.gif')}}" alt="Update" title="Update" border="0">
                                             </a>
                                             </span>
                                          </td>
                                          <td nowrap="">
                                            {{$record->file_name}}
                                          </td>
                                          <td nowrap="">
                                             <a href="javascript:popupWin('/scripts/garoon/grn.exe/grn/user_view?uid=6','user_view',1034,675,0,0,0,1,0,1)">@if($record->member->id == \Auth::guard('member')->user()->id )<img src="{{asset('/img/loginuser20.gif')}}" border="0" alt=""> @else <img src="{{asset('/img/user20.gif')}}" border="0" alt=""> @endif{{$record->member->full_name}}</a>
                                          </td>
                                          <td nowrap="">{{date('l, F d, Y',strtotime($record->update_time))}}</td>
                                          <td nowrap="">{{$record->size}}</td>
                                       </tr>
                                       @endforeach
                                    </tbody>
                                 </table>
                                 <div class="list_menu">
                                    <nobr>
                                       <span class="list_menu_item">
                                          <script language="JavaScript" type="text/javascript">
                                             <!--
                                             var enable_shift_click = false;
                                             var element_name = 'id[]';
                                             function CheckAll()
                                             
                                             {
                                                 var e = document.forms["cabinet/index"].elements;
                                                 var l = e.length;
                                                 var checked = false;
                                                 for( i = 0 ; i < l ; i ++ ) { if( e[i].name == "id[]" && e[i].style.display=="" && ! e[i].checked  ) { checked = true;break; } }
                                                 for( i = 0 ; i < l ; i ++ ) { if( e[i].name == "id[]" && e[i].style.display=="" ) { e[i].checked=checked; } }
                                                 if( typeof setHightlight == 'function' )
                                                     setHightlight();
                                             }
                                             
                                             // apply shift click
                                             
                                             applyShiftClick();
                                             
                                             function setHightlight()
                                             {
                                                 if (!enable_shift_click)
                                                     return;
                                             
                                                 var table = jQuery("table.list_column").get(0);
                                                 if (table) {
                                                     var rows = jQuery(table).find("tr");
                                                     rows.each(function (index, row) {
                                                         var row_jquery_obj = jQuery(row);
                                                         var chkbox = row_jquery_obj.find("input[type='checkbox']").eq(0);
                                                         if (chkbox.length == 0)
                                                             return;
                                             
                                                         if (chkbox.is(":checked")) {
                                                             row_jquery_obj.addClass("row_highlight");
                                                         }
                                                         else {
                                                             row_jquery_obj.removeClass("row_highlight");
                                                         }
                                             
                                                     });
                                                 }
                                             }
                                             function applyShiftClick()
                                             {
                                                 if (!enable_shift_click)
                                                     return;
                                             
                                                 var list = jQuery("table.list_column").get(0);
                                                 var row = null;
                                                 var row2 = null;
                                                 if (list) {
                                                     var chkboxs = fastGetElementsByName(list, 'input', element_name);
                                                     var i = 0;
                                                     jQuery.each(chkboxs, function (index, chkbox) {
                                                         chkbox.index = i;
                                             
                                                         var chkbox_jpuery_object = jQuery(chkbox);
                                                         chkbox_jpuery_object.on("click", function (e) {
                                                             row = chkbox_jpuery_object.parents("tr").eq(0);
                                                             if (this.checked) {
                                                                 row.addClass("row_highlight");
                                                             }
                                                             else {
                                                                 row.removeClass("row_highlight");
                                                             }
                                             
                                                             //handle Shift click
                                                             var last_click = list.last_click;
                                                             var last_click_index = last_click ? last_click.index : 0;
                                                             var current = this;
                                                             var current_index = this.index;
                                                             var low = Math.min(last_click_index, current_index);
                                                             var high = Math.max(last_click_index, current_index);
                                             
                                                             if (e.shiftKey) {
                                                                 var chkboxs2 = fastGetElementsByName(list, 'input', element_name);
                                                                 jQuery(chkboxs2).each(function (index, item) {
                                                                     row2 = jQuery(item).parents("tr").eq(0);
                                             
                                                                     if (item.index >= low && item.index <= high) {
                                                                         item.checked = current.checked;
                                                                     }
                                             
                                                                     if (item.checked) {
                                                                         row2.addClass('row_highlight');
                                                                     }
                                                                     else {
                                                                         row2.removeClass('row_highlight');
                                                                     }
                                                                 });
                                                             }
                                                             list.last_click = e.target;
                                                         });
                                                         i++;
                                                     });
                                                 }
                                             }
                                             
                                             //-->
                                          </script>
                                          <button class="check_button" style="width:20px; height:20px" onclick="javascript:CheckAll();return false"><img src="{{asset('/img/check10.gif')}}" border="0" alt="Select or clear all check boxes" title="Select or clear all check boxes"></button>
                                       </span>
                                       <input class="" type="button" value="Delete" onclick="return false;submit(this.form)" style="" id="btn_delete_multi2">&nbsp;
                                      
                                    </nobr>
                                 </div>
                              </form>
                              <script language="javascript" type="text/javascript">
                                 // on page load
                                 YAHOO.util.Event.onDOMReady(function(){
                                   // register event for delete handler
                                   var handler = ['btn_delete_multi1','btn_delete_multi2']; 
                                   jQuery.each( handler, function(index, value){
                                     var ele = jQuery("#" + value);
                                     if( ele.length == 0 ) return;
                                     ele.click(function(event){
                                         var title = "Delete files";
                                         var content = document.createElement("div");
                                         content.innerHTML = "\n<form name=\"cabinet\/delete_multi\" method=\"post\" action=\"\{{route('frontend.cabinet.destroy_multi')}}\"><input type=\"hidden\" name=\"_method\" value=\"DELETE\"><input type=\"hidden\" name=\"_token\" value=\"{{ csrf_token() }}\" /><input type=\"hidden\" name=\"csrf_ticket\" value=\"b0955c3c0ece7761126bd762c9073898\">\n\n      <p>Do you want to move the file from this folder to Trash?<br>\n    Folder:<span class=\"bold\"><span class=\"\"><img src=\"\{{asset('/img/folder20.gif')}}\" border=\"0\" alt=\"\">ルート<\/span><\/span><br>\n    Number of items：<span class=\"bold  delete_count\">1<\/span>\n    <\/p>\n   <input type=\"hidden\" name=\"hid\" value=\"1\">\n\n<\/form>\n";
                                         //get form element
                                         var f = content.getElementsByTagName("form")[0];
                                         if(!f) return; 
                                         var list = jQuery("form[name='cabinet\/index'] input[name='id[]']").filter(function(i){return jQuery(this).prop( "checked" );});
                                         if( list.length == 0 ) return;    // do nothing if these is not any item is selected
                                         //update number of items to delete
                                         var delete_number_span = content.getElementsByTagName("span");
                                         for (i = 0; i < delete_number_span.length; i++) {
                                             if ( jQuery(delete_number_span[i]).hasClass("delete_count") )
                                             {
                                                 var delete_number = delete_number_span[i];
                                             }
                                         }
                                         if(delete_number) delete_number.innerHTML = list.length;
                                 
                                         //add hidden field
                                         jQuery.each( list, function(index, value){
                                             var hidden_input = document.createElement("input");
                                             var input_name = jQuery(this).prop( "name" );
                                             var input_value = jQuery(this).val();
                                             hidden_input.setAttribute("type", "hidden");
                                             hidden_input.setAttribute("name", input_name);
                                             hidden_input.setAttribute("value", input_value);
                                             f.appendChild( hidden_input );
                                         });
                                             var ui_options = {};
                                             // real HTML of $content is not equal to $content.innerHTML so message is not cerrect (browser behaviour about innerHTML attribute)
                                             // So for this case we need to pass whole $content DOM object
                                             var message = content.innerHTML;
                                             
                                             // show message box
                                             GRN_MsgBox.show(message, title, GRN_MsgBoxButtons.yesno,
                                                { ui       : ui_options,
                                                  caption : { ok: 'OK', cancel: 'Cancel', yes: 'Yes', no: 'No' },
                                                  callback : function(result, form){
                                                      if(result == GRN_MsgBoxResult.yes){
                                                         // submit form
                                                         jQuery('#msgbox').find('input[type="button"]').each(function(index, value){jQuery(this).prop( "disabled", true );});
                                                         document.body.style.cursor = 'progress';form.submit();
                                                      }
                                                  }
                                             });
                                         event.stopPropagation();
                                         event.preventDefault();
                                     });
                                   });
                                 }); 
                              </script>
                           </div>
                           <div class="js_pagination_bottom">
                              <div class="navi">
                                 <div class="list_paging_grn inline_block_grn js_inner"><span class="action_disable_grn top_page_off"><span class="arrow_first_button_grn" role="button" title="First row" aria-disabled="true" aria-label="First row"></span></span><span class="action_disable_grn prev_page_off"><span class="arrow_left_button_grn" role="button" title="Previous " aria-disabled="true" aria-label="Previous "></span></span><span class="action_disable_grn next_page_off"><span class="arrow_right_button_grn" role="button" title="Next " aria-disabled="true" aria-label="Next "></span></span></div>
                              </div>
                           </div>
                           <form name="cabinet/lock" method="post" action="/scripts/garoon/grn.exe/cabinet/file_update?"><input type="hidden" name="csrf_ticket" value="b0955c3c0ece7761126bd762c9073898">
                              <input type="hidden" name="fid" value="">
                              <input type="hidden" name="hid" value="4">
                              <input type="hidden" name="index_return" value="1">
                              <input type="hidden" name="upload" value="Upload">
                           </form>
                        </td>
                     </tr>
                  </tbody>
               </table>
            </td>
         </tr>
      </tbody>
   </table>
   <!--end of maincontents_list-->
   <div id="js_cabinet_download_multi">
      <div id="background" class=""></div>
      <div id="bundleDownloadWindow" class="msgbox" style="display:none;">
         <div class="title" id="bundleDownloadTitle">
            Select files
            <a style="position: absolute; right: 5px;top:5px;text-decoration:none;" onclick="__bundleDownLoad.closeBundleDownloadWindow();" href="javascript:;"><img src="{{asset('/img/close20.gif')}}" border="0" alt=""></a>
         </div>
         <form id="bundle_download_form" name="bundle_download_form" action="/cabinet/download_multi" method="POST" style="height:100%;">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input type="hidden" name="filename" value="ルート">
            <input type="hidden" name="folder_id" value="1">
            <div style="padding:1em;height:80%;">
               <div id="bundleFiles">
                  <table class="list_column" width="100%">
                     <colgroup>
                        <col width="1%">
                        <col width="25%">
                        <!-- タイトル -->
                        <col width="25%">
                        <!-- ファイル名 -->
                        <col width="25%">
                        <!-- 更新者 -->
                        <col width="12%">
                        <!-- サイズ -->
                        <col width="12%">
                        <!-- 更新日時 -->
                     </colgroup>
                     <thead>
                        <tr>
                           <td align="center"><button style="width:20px; height:20px; padding-left: 3px;" onclick="__bundleDownLoad.check_download_files();return false"><img src="{{asset('/img/check10.gif')}}" border="0" alt="Select or clear all check boxes" title="Select or clear all check boxes"></button></td>
                           <td>Subject</td>
                           <td>Content</td>
                           <td>Author</td>
                           <td>Size</td>
                           <td>Updated</td>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach($records as $key=>$record)
                        <tr id="tr_2" class="lineone" onmouseover="__bundleDownLoad.setSelectableView(2);" onmouseout="__bundleDownLoad.setUnselectableView(2);">
                           <td style="padding-left:10px;">
                                  <input type="checkbox" id="download_2" name="id[]" onclick="__bundleDownLoad.calcTotalFileSize(this);" size="10403" value="{{$record->id}}">
                           </td>
                           <td>{{$record->subject}}</td>
                           <td>{{$record->file_name}}</td>
                           <td><a href="javascript:popupWin('/scripts/garoon/grn.exe/grn/user_view?uid=6','user_view',1034,675,0,0,0,1,0,1)">@if($record->member->id == \Auth::guard('member')->user()->id )<img src="{{asset('/img/loginuser20.gif')}}" border="0" alt=""> @else <img src="{{asset('/img/user20.gif')}}" border="0" alt=""> @endif{{$record->member->full_name}}</a></td>
                           <td>{{$record->size}}</td>
                           <td>{{date('l, F d, Y',strtotime($record->update_time))}}</td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
            <div style="background-color:white; padding-bottom: 15px;">
               <table border="0" width="100%" class="bundleDownloadWindow_info">
                  <tbody>
                     <tr>
                        <td width="1%">
                           <span id="bundleSizePercentage">0%</span>
                        </td>
                        <td>
                           <div style="border: 1px solid rgb(176, 176, 176); padding: 0px; width: 99.5%; height: 10px;">
                              <div id="inner_progress" style="width:0%;background-color:lightgreen;height:100%;float:left;"></div>
                              <div id="assumed_progress" style="width:0%;background-color:yellow;height:10px;float:left;"></div>
                           </div>
                        </td>
                        <td width="10px"></td>
                        <td width="25%">
                           Total file size:<span id="bundleDownloadSize">0B</span>
                        </td>
                        <td width="20%">
                           Size limit:<span id="bundleMaxSize">30MB</span>
                        </td>
                     </tr>
                  </tbody>
               </table>
               <div style="text-align:center;background-color:white;">
                  <span id="bundleDownloadButton" class="button_grn_js button1_main_grn  button1_r_margin2_grn"><a href="javascript:void(0);" role="button">Download</a></span><span id="bundleCancelButton" class="button_grn_js button1_normal_grn" onclick="__bundleDownLoad.closeBundleDownloadWindow();"><a href="javascript:void(0);" role="button">Cancel</a></span>
               </div>
            </div>
         </form>
      </div>
      <script type="text/javascript">
         <!--
         var __bundleDownLoad = grn.html.download;
         __bundleDownLoad.filename = "ルート";
         __bundleDownLoad.filename = __bundleDownLoad.filename.replace(/[\?\*\/\\"':|<>]/g,'_');
         __bundleDownLoad.filename = encodeURIComponent(__bundleDownLoad.filename);
         __bundleDownLoad.max_download_size = 31457280;
         
         jQuery("#bundleMaxSize").html( __bundleDownLoad.getSizeString(__bundleDownLoad.max_download_size) );
         
         grn.component.button("#bundleDownloadButton").on("click", function() {
             __bundleDownLoad.pre_submit();
             document.forms['bundle_download_form'].submit();
         });
         //-->
      </script>
      <style type="text/css">
         <!--
            #bundleDownloadWindow
            {
                position:absolute;
                background-color:white;
                top:20%;
                left:15%;
                border:1px solid #B0B0B0;
            }
            
            #bundleFiles
            {
                top:10%;
                height:100%;
                border:1px solid #B0B0B0;
                overflow: scroll;
            }
            
            #bundleFiles table.list_column thead img {
                vertical-align: 1px;
            }
            
            div.cover
            {
                position:fixed;
                top:0;
                left:0;
                bottom: 0;
                right: 0;
                background-color:black;
                -moz-opacity:0.50; /*FF*/
                opacity:0.50;
                z-index : 1001;
            }
            //-->
      </style>
   </div>
   <script language="javascript">
      function prepare_file_upload(form_name, folder_id, file_upload_id, index_return) {
          form_upload = document.forms[form_name];
          form_upload['fid'].value = file_upload_id;
          form_upload['hid'].value = folder_id;
          form_upload['index_return'].value = index_return;
      }
      
      YAHOO.util.Event.onDOMReady(function () {
          new grn.js.page.cabinet.Index({
              element: '.js_cabinet_index',
              
              params: {"sort":"ftu","folderId":4},
              pagination:{"previousPageOffset":-1,"nextPageOffset":-1,"rangeNumberStart":1,"rangeNumberEnd":4},
              
              splitScreenLayout: {
                  defaultWidth: 190,
                  saveSplitterUrl: 'cabinet/ajax/command_save_splitter',
              },
          });
      
      });
      
   </script>
</div>
@stop
@section('script')
@parent
<script>
      $('.ygtvspacer').click(function(){
         if($(this).parent().hasClass('ygtvtp')){
             $(this).parent().addClass('ygtvtm');
             $(this).parent().removeClass('ygtvtp');
             $(this).parents('.ygtvtable ').next().attr('style','');
         }else{
            $(this).parent().addClass('ygtvtp');
            $(this).parent().removeClass('ygtvtm');
            $(this).parents('.ygtvtable ').next().attr('style','display:none');
         }
      })                                                                                                                                  
</script>
@stop