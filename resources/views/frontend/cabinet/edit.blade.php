@extends('frontend.layouts.create_schedule')
@section('content')
<div class="global_navi" role="heading" aria-level="2">&nbsp;<img src="{{asset('/img/file20.gif')}}" border="0" alt=""><span class="globalNavi-item-grn"><a href="{!!route('frontend.cabinet.index')!!}">Cabinet</a></span></div>
<div class="mainarea ">
   <h2 style="display:inline;" class="cabinet">Edit file information</h2>
   <form name="cabinet/modify" method="post" action="{!!route('frontend.cabinet.update',$record->id)!!}">
      <input type="hidden" name="csrf_ticket" value="3f2347abbf1943a80f87aacb85ed2246">
      <input type="hidden" name="_token" value="{{ csrf_token() }}" />
      <table class="std_form">
         <tbody>
            <tr>
               <th>Position</th>
               <td>
                  <img src="{{asset('/img/folder20.gif')}}" border="0" alt="">
                  {{$record->folder->name}}
               </td>
            </tr>
            <tr>
               <th>File name</th>
               <td>{{$record->file_name}}</td>
            </tr>
            <tr>
               <th>Subject</th>
               <td><input type="text" name="subject" class="cb_text_ess" size="50" maxlength="100" title="Subject" onkeypress="return event.keyCode != 13;" value="{{$record->subject}}"></td>
            </tr>
            <tr>
               <th>Versioning</th>
               <td>
                  <select name="version">
                     @for($i=0;$i < 10;$i++ )
                     @if($i == 0)
                     <option value="0" selected="">(None)</option>
                     @else
                     <option value="{{$i}}" @if($i == $record->max_version) checked @endif>{{$i}}</option>
                     @endif
                     @endfor
                  </select>
                  &nbsp; latest versions
               </td>
            </tr>
            <tr valign="top">
               <th>Description</th>
               <td><textarea id="textarea_id" name="memo" class="autoexpand" wrap="virtual" role="form" cols="50" rows="5" title="Description" style="white-space: pre-wrap; min-height: 95px; height: 121px;">{{$record->memo}}</textarea><textarea style="white-space: pre-wrap; left: -9999px; position: absolute; top: 0px; height: 121px;" id="dummy_textarea_textarea_id" class="" wrap="virtual" cols="50" rows="5" title="Description" disabled=""></textarea></td>
            </tr>
            <tr>
               <td></td>
               <td>
                  <div class="mTop10">
                     <span id="filemodify_button_save" class="button_grn_js button1_main_grn  button1_r_margin2_grn" onclick="grn.component.button.util.submit(this);" data-auto-disable="true">
                         <a href="javascript:void(0);" role="button">Save</a>
                     </span>
                      <span id="filemodify_button_cancel" class="button_grn_js button1_normal_grn" onclick="grn.component.button.util.redirect(this,'history_back');">
                          <a href="javascript:void(0);" role="button">Cancel</a>
                      </span>
                  </div>
               </td>
            </tr>
         </tbody>
      </table>
   </form>
</div>
@stop
@section('script')
@parent
<script></script>
@stop
