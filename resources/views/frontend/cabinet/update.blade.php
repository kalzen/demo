@extends('frontend.layouts.create_schedule')
@section('content')
<div class="global_navi" role="heading" aria-level="2">&nbsp;<img src="{{asset('/img/file20.gif')}}" border="0" alt=""><span class="globalNavi-item-grn"><a href="/scripts/garoon/grn.exe/cabinet/index?sf=1">Cabinet</a></span></div>
<div class="mainarea ">
   <h2 style="display:inline;" class="cabinet">Update file</h2>
   <form name="cabinet/upload" method="post" enctype="multipart/form-data" action="{{route('frontend.cabinet.update',$record->id)}}">
      <input type="hidden" name="csrf_ticket" value="3f2347abbf1943a80f87aacb85ed2246">
      <input type="hidden" name="fid" value="5">
      <input type="hidden" name="_token" value="{{ csrf_token() }}" />
      <div class=""><span class="attention">*</span> is required.</div>
      <input type="hidden" name="upload_ticket" value="c1bb2ca3f004bc7b248ca9ea2eb0e6f0c55549363e89fd5bf25a08070182a0a1">
      <table class="std_form">
         <tbody>
            <tr>
               <td colspan="2">
                  <div class="sub_title">Current file</div>
               </td>
            </tr>
            <tr>
               <th nowrap="">
                  File name
               </th>
               <td>
                   <a class="with_lang" href="{!!$record->link!!}">
                     <nobr></nobr>
                     {!!$record->file_name!!}
                  </a>
               </td>
            </tr>
            <tr>
               <th nowrap="">
                  Size
               </th>
               <td>
                  {!!$record->size!!}
               </td>
            </tr>
            <tr>
               <td colspan="2">
                  <br>
                  <div class="sub_title">New file</div>
               </td>
            </tr>
            <tr>
               <th nowrap="">
                  File<span class="attention">*</span>
               </th>
               <td>
                  <script language="Javascript" type="text/javascript">
                     <!--
                     __upload_ticket = "c1bb2ca3f004bc7b248ca9ea2eb0e6f0c55549363e89fd5bf25a08070182a0a1";
                     __upload_files_url = '/scripts/garoon/grn.exe/grn/uploaded_files?';
                     
                     __upload_msg_error = "Failed to upload the file. (Error: ";
                     __upload_msg_error_suffix   = ")";
                     __upload_msg_filesizeover_1 = "Cannot upload the file. The file size exceeds the size limit of ";
                     __upload_msg_filesizeover_2 = ".";
                     __upload_msg_zerobyte_file  = "Cannot upload the 0 byte file.";
                     __upload_msg_cancel = "Cancel";
                     __upload_url = "/api/add_file_cabinet?";
                     os_type = "";
                     browser_type = "chrome";
                     browser_ver_major = "87";
                     //-->
                  </script>
                  <script src="{!!asset('/js/upload.js')!!}" type="text/javascript"></script>
                  <script src="{!!asset('/js/upload_single.js')!!}" type="text/javascript"></script>
                  <div id="html5_content" style="">
                     <div id="drop_" class="drop">
                        Drop files here.
                     </div>
                     <div class="file_input_div">
                        Attach files
                        <input type="file" class="file_html5 file_input_hidden" name="newfile" size="29" title="File" id="file_upload_" style='display:inline-block'>
                     </div>
                     <input type="hidden" name="html5" value="true" size="100">
                     <div id="upload_message" style="font-size:small;">
                     </div>
                     <table id="upload_table" class="attachment_list_base_grn">
                        <tbody>
                           <tr>
                              <td></td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <div class="attachment_legacy_base_grn">
                  </div>
               </td>
            </tr>
            <tr>
               <th nowrap="">
                  Updater
               </th>
               <td>
                  <a href="javascript:popupWin('/scripts/garoon/grn.exe/grn/user_view?uid=58','user_view',1034,675,0,0,0,1,0,1)"><img src="{!!asset('/img/loginuser20.gif')!!}" border="0" alt="">{{\Auth::guard('member')->user()->full_name}}</a>
               </td>
            </tr>
            <tr valign="top">
               <th nowrap="">
                  Update comment
               </th>
               <td>
                  <textarea id="textarea_id" name="comment" class="autoexpand" wrap="virtual" role="form" cols="50" rows="5" title="Update comment" style="white-space: pre-wrap; min-height: 95px; height: 121px;"></textarea>
                  <textarea style="white-space: pre-wrap; left: -9999px; position: absolute; top: 0px; height: 121px;" id="dummy_textarea_textarea_id" class="" wrap="virtual" cols="50" rows="5" title="Update comment" disabled=""></textarea>
                  <br>
               </td>
            </tr>
            <tr>
               <td></td>
               <td>
                  <div class="mTop10">
                     <span id="fileupload_button_upload" class="button_grn_js button1_main_grn  button1_r_margin2_grn" onclick="jQuery(&quot;#button_command&quot;).attr({name: 'upload', value: 'Update'}); grn.component.button.util.submit(this);" data-auto-disable="true"><a href="javascript:void(0);" role="button">Update</a></span><span id="fileupload_button_cancel" class="button_grn_js button1_normal_grn   button_submit_grn" onclick="jQuery(&quot;#button_command&quot;).attr({name: 'cancel', value: 'Cancel'}); grn.component.button.util.submit(this);"><a href="javascript:void(0);" role="button">Cancel</a></span>
                     <input type="hidden" name="" value="" id="button_command">
                  </div>
               </td>
            </tr>
         </tbody>
      </table>
   </form>
</div>
@stop
@section('script')
@parent
<script></script>
@stop
