@extends('frontend.layouts.create_schedule')
@section('content')
<table class="global_navi_title" cellpadding="0" cellmargin="0" style="padding:0px;">
   <tbody>
      <tr>
         <td width="100%" valign="bottom" nowrap="">
            <div role="heading" aria-level="2">
               <img src="{{asset('/img/file20.gif')}}" border="0" alt="">Cabinet
            </div>
         </td>
         <td align="right" valign="bottom" nowrap="">
         </td>
      </tr>
   </tbody>
</table>
<div class="mainarea ">
    <h2 style="display:inline;" class="cabinet">Add file</h2>
    <form name="cabinet/add" method="post" enctype="multipart/form-data" action="{{route('frontend.cabinet.store')}}">
       <input type="hidden" name="csrf_ticket" value="b0955c3c0ece7761126bd762c9073898">
       <div class="explanation">
          Specify the file and enter the details.
       </div>
       <script language="Javascript" type="text/javascript">
          <!--
              __upload_msg_error          = "Failed to upload the file. (Error: ";
              __upload_msg_error_suffix   = ")";
              __upload_msg_filesizeover_1 = "Cannot upload the file. The file size exceeds the size limit of ";
              __upload_msg_filesizeover_2 = ".";
              __upload_msg_zerobyte_file  = "Cannot upload the 0 byte file.";
              __upload_msg_version_suffix = " latest versions";
              __upload_msg_cancel         = "Cancel";
              __upload_msg_confirm1       = "Failed to attach files.";
              __upload_msg_confirm2       = "Only the files which have already uploaded will be attached.";
              __upload_ticket ="ea619eff90ba15848b18a0c35e9b0f249b5001dbc80ba31385fdab220e5a74aa";
              __upload_url = "/api/add_file_cabinet?";
              os_type = "";
              browser_type = "chrome";
              browser_ver_major = "87";
          //-->
       </script>
       <input type="hidden" name="created_by" value="{!!\Auth::guard('member')->user()->id!!}">
       <input type="hidden" name="_token" value="{{ csrf_token() }}" />
       <div>
          <div class=""><span class="attention">*</span> is required.</div>
          <style type="text/css">
             <!--
                .list_column td
                {
                    border-top:1px solid #000;
                }
                -->
          </style>
          <table class="std_form">
             <tr>
                <th>Registrant</th>
                <td><a href="javascript:popupWin('/scripts/garoon/grn.exe/grn/user_view?uid=58','user_view',1034,675,0,0,0,1,0,1)"><img src="{!!asset('/img/loginuser20.gif')!!}" border="0" alt="">{!!\Auth::guard('member')->user()->full_name!!}</a></td>
             </tr>
             <tr>
                <th>Position</th>
                <td>
                    <select name='folder_id'>
                        @foreach($folders as $folder)
                        <option value='{{$folder->id}}'>{{$folder->name}}</option>
                        @endforeach
                    </select>
                </td>
             </tr>
             <tr>
                <th>File<span class="attention">*</span></th>
                <td>
                   <div id="html5_content" style="display:none;">
                      <div id="drop_" class="drop">
                         Drop files here.
                      </div>
                      <div class="file_input_div">
                         Attach files
                         <input type="file" class="file_input_hidden" name="file" size="40" title="File" id="file_upload_" multiple style="display:block;">
                      </div>
                      <input type="hidden" name="html5" value="true" size="100">
                   </div>
                   <div id="not_support_html5_content" style="display:none;">
                      <span class="icon_information_sub_grn messageSub-grn">Your Web browser does not support uploading a file.</span>
                   </div>
                </td>
             </tr>
          </table>
          <div style="display:none;">
             <select id="version_select" name="max_version_fileid">
                <option value="0" selected>(None)</option>
                <option value="1" >1</option>
                <option value="2" >2</option>
                <option value="3" >3</option>
                <option value="4" >4</option>
                <option value="5" >5</option>
                <option value="6" >6</option>
                <option value="7" >7</option>
                <option value="8" >8</option>
                <option value="9" >9</option>
                <option value="10" >10</option>
                <option value="-1" >Unlimited</option>
             </select>
          </div>
          <div id="upload_message" style="margin-bottom:10px;"></div>
          <div>
             <table id="fileTable" class="fileup_info">
                <colgroup>
                   <col width="1%" nowrap>
                   <col nowrap>
                   <col nowrap>
                   <col width="1%" nowrap>
                   <col width="1%" nowrap>
                   <col width="200px" nowrap>
                   <col nowrap>
                   <col width="300px" nowrap>
                </colgroup>
                <tr>
                   <th nowrap>&nbsp;</th>
                   <th nowrap>File</th>
                   <th nowrap>Size</th>
                   <th nowrap></th>
                   <th nowrap></th>
                   <th nowrap>Subject</th>
                   <th nowrap>Versioning</th>
                   <th nowrap>Description</th>
                </tr>
             </table>
          </div>
          <div class="mTop15 mBottom15"><span id="cabinet_button_add" class="button_grn_js button1_main_grn  button1_r_margin2_grn" onclick="if(grn.component.button(&quot;#cabinet_button_add&quot;).isDisabled())return false;grn.component.button(&quot;#cabinet_button_add&quot;).showSpinner();if(confirmIfExistFailedFile())document.forms['cabinet/add'].submit();else grn.component.button(&quot;#cabinet_button_add&quot;).hideSpinner();"  ><a href="javascript:void(0);" role="button">Add</a></span><span id="cabinet_button_cancel" class="button_grn_js button1_normal_grn" onclick="grn.component.button.util.redirect(this,'/scripts/garoon/grn.exe/cabinet/index?hid=1');"  ><a href="javascript:void(0);" role="button">Cancel</a></span></div>
       </div>
       <div class="clear_both_0px"></div>
       <script src="{{asset('/js/upload_cabinet.js')}}" type="text/javascript"></script>
    </form>
 </div>
@stop
@section('script')
@parent
<script></script>
@stop