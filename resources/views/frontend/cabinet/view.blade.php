@extends('frontend.layouts.create_schedule')
@section('content')
<div class="global_navi" role="heading" aria-level="2">&nbsp;<img src="{{asset('/img/file20.gif')}}" border="0" alt=""><span class="globalNavi-item-grn"><a href="{{route('frontend.cabinet.index')}}">Cabinet</a></span><span class="globalNavi-item-grn-image"></span><span class="globalNavi-item-grn"><a href="{{route('frontend.cabinet.index',['folder_id'=>$record->folder_id])}}">{{$record->folder->name}}</a></span><span class="globalNavi-item-grn-image"></span><span class="globalNavi-item-last-grn">File details</span></div>
<div class="mainarea ">
   <script language="javascript" type="text/javascript">
      // on page load
      YAHOO.util.Event.onDOMReady(function(){
        // register event for delete handler
        var handler = ['lnk_delete']; 
        jQuery.each( handler, function(index, value){
          var ele = jQuery("#" + value);
          if( ele.length == 0 ) return;
          ele.click(function(event){
              var title = "Delete file";
              var content = document.createElement("div");
              content.innerHTML = "<form name=\"cabinet\/delete\" method=\"post\" action=\"\{{route('frontend.cabinet.destroy',$record->id)}}\"><input type=\"hidden\" name=\"_method\" value=\"DELETE\"><input type=\"hidden\" name=\"_token\" value=\"{{ csrf_token() }}\" /><input type=\"hidden\" name=\"csrf_ticket\" value=\"3f2347abbf1943a80f87aacb85ed2246\">\n<input type=\"hidden\" name=\"fid\" value=\"5\">\nDo you want to move the file to Trash?<br>\n <p>File:<span class=\"bold\"><img src=\"\/garoon3\/grn\/image\/cybozu\/file20.gif?20200925.text\" border=\"0\" alt=\"\">({{$record->file_name}}<\/span>\n <\/p>\n<input type=\"hidden\" name=\"hid\" value=\"1\">\n<\/form>";
              //get form element
              var f = content.getElementsByTagName("form")[0];
              if(!f) return; 
                  var ui_options = {};
                  // real HTML of $content is not equal to $content.innerHTML so message is not cerrect (browser behaviour about innerHTML attribute)
                  // So for this case we need to pass whole $content DOM object
                  var message = content.innerHTML;
                  
                  // show message box
                  GRN_MsgBox.show(message, title, GRN_MsgBoxButtons.yesno,
                      { ui       : ui_options,
                        caption : { ok: 'OK', cancel: 'Cancel', yes: 'Yes', no: 'No' },
                        callback : function(result, form){
                            if(result == GRN_MsgBoxResult.yes){
                               // submit form
                               jQuery('#msgbox').find('input[type="button"]').each(function(index, value){jQuery(this).prop( "disabled", true );});
                               document.body.style.cursor = 'progress';form.submit();
                            }
                        }
                      });
              event.stopPropagation();
              event.preventDefault();
          });
        });
      }); 
   </script>
   <script language="javascript" type="text/javascript">
      // on page load
      YAHOO.util.Event.onDOMReady(function(){
        // register event for delete handler
        var handler = ['btn_lock']; 
        jQuery.each( handler, function(index, value){
          var ele = jQuery("#" + value);
          if( ele.length == 0 ) return;
          ele.click(function(event){
              var title = "Edit file";
              var content = document.createElement("div");
              content.innerHTML = "<form name=\"cabinet\/lock\" method=\"post\" action=\"\/scripts\/garoon\/grn.exe\/cabinet\/file_update?\"><input type=\"hidden\" name=\"csrf_ticket\" value=\"3f2347abbf1943a80f87aacb85ed2246\">\n    <div class=\"attention\">Cannot update the file while other user is editing.<\/div>\n\n<table class=\"std_form\">\n    <tr>\n        <th nowrap>\n            Editor\n        <\/th>\n        <td>\n            <a href=\"javascript:popupWin(\'\/scripts\/garoon\/grn.exe\/grn\/user_view?uid=58\',\'user_view\',1034,675,0,0,0,1,0,1)\"><img src=\"\/garoon3\/grn\/image\/cybozu\/loginuser20.gif?20200925.text\" border=\"0\" alt=\"\">Foster Brown<\/a>\n        <\/td>\n    <\/tr>\n    <tr>\n        <th nowrap>\n            File\n        <\/th>\n        <td>\n            <a class=\"with_lang\"\n               href=\"\/scripts\/garoon\/grn.exe\/cabinet\/download\/-\/rule+of+employment.docx?fid=5&amp;ticket=&amp;time=1365954639&amp;hid=1&amp;.docx\"><nobr><\/nobr>{{$record->file_name}}            <\/a>&nbsp;&nbsp;(application\/vnd.openxmlformats-officedocument.wordprocessingml.document)\n        <\/td>\n    <\/tr>\n    <tr>\n        <td><\/td>\n        <td>\n            <div class=\"mTop10\">\n                <span id=\"filelock_button_upload\" class=\"button_grn_js button1_main_grn  button1_r_margin2_grn\" onclick=\"jQuery(&quot;#button_command&quot;).attr({name: \'upload\', value: \'Upload\'}); grn.component.button.util.submit(this);\"  data-auto-disable=\"true\"><a href=\"javascript:void(0);\" role=\"button\">Upload<\/a><\/span><span id=\"filelock_button_cancel\" class=\"button_grn_js button1_normal_grn\" onclick=\"jQuery(&quot;#button_command&quot;).attr({name: \'cancel\', value: \'Cancel editing\'}); grn.component.button.util.submit(this);\"  ><a href=\"javascript:void(0);\" role=\"button\">Cancel editing<\/a><\/span>\n                <input type=\"hidden\" name=\"\" value=\"\" id=\"button_command\">\n            <\/div>\n        <\/td>\n    <\/tr>\n<\/table>\n\n<ol class=\"process\">\n    <li>Download the above-mentioned file, and then edit it.<br>To cancel this operation, click [Cancel editing]. <\/li>\n    <li>Click [Upload] to update the edited file.<br>It will be recorded in updated information.<\/li>\n<\/ol>\n\n<input type=\"hidden\" name=\"fid\" value=\"5\"><input type=\"hidden\" name=\"hid\" value=\"1\">\n<input type=\"hidden\" name=\"upload\" value=\"Upload\">\n<\/form>";
              //get form element
              var f = content.getElementsByTagName("form")[0];
              if(!f) return; 
                  if( f ){
                      content.style.display = "none";
                      document.body.appendChild(content);f.submit();
                  }
              event.stopPropagation();
              event.preventDefault();
          });
        });
      }); 
   </script>
   <!--menubar-->
   <div id="main_menu_part">
      <span class="float_left nowrap-grn">
          <span class="aButtonStandard-grn"><a href="{{route('frontend.cabinet.view_update',$record->id)}}" tabindex="0" title="Update" aria-label="Update" class="mRight10">Update</a></span>
          <span class="menu_item"><span class="nowrap-grn "><a class="menu_item" href="{{route('frontend.cabinet.view_edit',$record->id)}}"><img src="{{asset('/img/modify20.gif')}}" border="0" alt="" class="menu_item">Edit info</a></span></span>
      <span class="menu_item"><span class="nowrap-grn "><a id="lnk_delete" class="menu_item" href="javascript:void(0);"><img src="{{asset('/img/delete20.gif')}}" border="0" alt="" class="menu_item">Delete</a></span></span>
      </span>
      <span class="float_right nowrap-grn mBottom2">
         <div class="moveButtonBlock-grn">
            <span class="moveButtonBase-grn button_disable_filter_grn"><a href="#" aria-disabled="true"><span class="moveButtonArrowLeft-grn"></span></a></span><span class="moveButtonBase-grn"><a href="/scripts/garoon/grn.exe/cabinet/view?hid=1&amp;fid=4" aria-label="Next" title="Next"><span class="moveButtonArrowRight-grn"></span></a></span>
         </div>
      </span>
      <div class="clear_both_0px"></div>
   </div>
   <script language="javascript" text="text/javascript" src="{{asset('/js/star.js')}}"></script>
   <script language="javascript" text="text/javascript">
      var options = {url: '/scripts/garoon/grn.exe/star/ajax_request?',
                     csrf_ticket: '3f2347abbf1943a80f87aacb85ed2246',
                     list_id: 'star_list'               };
      
      var obj_star_list = new GRN_StarList(options);
   </script>
   <div id="star_list">
      <span class="star inline_block_grn off" id="grn.cabinet:fid_5:hid_1" app_path="/garoon3/grn/image/cybozu/" onclick="obj_star_list._onClick(this);"><img src="/garoon3/grn/image/cybozu/star_off.png?20200925.text" border="0" alt=""></span>
      <h2 class="cabinet inline_block_grn mBottom10">{{$record->file_name}}</h2>
   </div>
   <form name="unlock" method="post" action="/scripts/garoon/grn.exe/cabinet/command_unlock?">
      <input type="hidden" name="csrf_ticket" value="3f2347abbf1943a80f87aacb85ed2246">
      <input type="hidden" name="hid" value="1">
      <input type="hidden" name="fid" value="5">
   </form>
   <p>
   </p>
   <table class="view_table" width="80%">
      <colgroup>
         <col width="20%">
         <col width="80%">
      </colgroup>
      <tbody>
         <tr>
            <th>Position</th>
            <td>
               <img src="{{asset('/img/folder20.gif')}}" border="0" alt="">
               <span class="nowrap-grn "><a href="{{route('frontend.cabinet.index',['folder_id'=>$record->folder_id])}}">{{$record->folder->name}}</a></span>
            </td>
         </tr>
      </tbody>
   </table>
   <p></p>
   <div class="sub_title">File</div>
   <table class="view_table" width="80%">
      <colgroup>
         <col width="20%">
         <col width="80%">
      </colgroup>
      <tbody>
         <tr>
            <th nowrap="">Name</th>
            <td>
               <a class="with_lang" href="{{$record->link}}">
                  <nobr></nobr>
                  {{$record->file_name}}
               </a>
            </td>
         </tr>
         <tr>
            <th nowrap="">Size</th>
            <td>{{$record->size}}</td>
         </tr>
      </tbody>
   </table>
   <p></p>
   <div class="sub_title">File information</div>
   <table class="view_table" width="80%">
      <colgroup>
         <col width="20%">
         <col width="80%">
      </colgroup>
      <tbody>
         <tr>
            <th nowrap="">Subject</th>
            <td>
               <a name="title">{{$record->subject}}</a>&nbsp;
            </td>
         </tr>
         <tr>
            <th nowrap="">Versioning</th>
            <td>
               @if($record->max_version == 0) Don't set @else {{$record->max_version}} @endif
            </td>
         </tr>
         <tr>
            <th nowrap="">Registered</th>
            <td nowrap=""><a href="javascript:popupWin('/scripts/garoon/grn.exe/grn/user_view?uid=6','user_view',1034,675,0,0,0,1,0,1)"><img src="{{asset('/img/user20.gif')}}" border="0" alt="">{{$record->updaters->full_name}}</a>&nbsp;{{date('l, F d, Y H:i A',strtotime($record->created_at))}}</td>
         </tr>
         <tr>
            <th nowrap="">Updated</th>
            <td nowrap=""><a href="javascript:popupWin('/scripts/garoon/grn.exe/grn/user_view?uid=6','user_view',1034,675,0,0,0,1,0,1)"><img src="{{asset('/img/user20.gif')}}" border="0" alt="">{{$record->member->full_name}}</a>&nbsp;{{date('l, F d, Y H:i A',strtotime($record->update_time))}}</td>
         </tr>
         <tr valign="top">
            <th nowrap="">Description</th>
            <td>
               <br>
            </td>
         </tr>
      </tbody>
   </table>
   <p>
   </p>
   <div class="sub_title">Updated</div>
   <table class="admin_list_table">
      <colgroup>
         <col width="1%">
         <col width="20%">
         <col width="20%">
         <col width="20%">
         <col width="5%">
         <col width="30%">
      </colgroup>
      <tbody>
         <tr>
            <th nowrap="">Ver.</th>
            <th nowrap="">Date and time<a href="/scripts/garoon/grn.exe/cabinet/view?fid=5&amp;sort=tu&amp;sp=0&amp;hid=1" title="Sort"><img src="{{asset('/img/sortdown16.gif')}}" border="0" alt="Sort" title="Sort"></a></th>
            <th nowrap="">Updater<a href="/scripts/garoon/grn.exe/cabinet/view?fid=5&amp;sort=ud&amp;sp=0&amp;hid=1" title="Sort"><img src="{{asset('/img/sortdown16_n.gif')}}" border="0" alt="Sort" title="Sort"></a></th>
            <th nowrap="">File name<a href="/scripts/garoon/grn.exe/cabinet/view?fid=5&amp;sort=nd&amp;sp=0&amp;hid=1" title="Sort"><img src="{{asset('/img/sortdown16_n.gif')}}" border="0" alt="Sort" title="Sort"></a></th>
            <th nowrap="">Action</th>
            <th nowrap="">Update comment</th>
         </tr>
         @foreach($historys as $history)
         <tr valign="top">
            <td nowrap="">{!!$history->id!!}</td>
            <td nowrap="">{!!date('l, F d, Y H:i A',strtotime($history->update_time))!!}</td>
            <td nowrap=""><a href="javascript:popupWin('/scripts/garoon/grn.exe/grn/user_view?uid=6','user_view',1034,675,0,0,0,1,0,1)"><img src="{{asset('/img/user20.gif')}}" border="0" alt="">{!!$record->member->full_name!!}</a></td>
            <td nowrap="">{!!$history->file_name!!}</td>
            <td nowrap="">{!!$history->action!!}</td>
            <td>
               <pre class="format_contents" style="white-space:-moz-pre-wrap;white-space:pre-wrap;display:inline;">{{$history->comment}}</pre>
            </td>
         </tr>
         @endforeach
      </tbody>
   </table>
   <div class="navi">
      <nobr>
         <span class="navi_off">First row</span>
         &nbsp;|&nbsp;
         <span class="navi_off">&lt;&lt;Previous  20 </span>
         &nbsp;|&nbsp;
         <span class="navi_off">Next  20 &gt;&gt;</span>
      </nobr>
   </div>
</div>
@stop
@section('script')
@parent
<script></script>
@stop
