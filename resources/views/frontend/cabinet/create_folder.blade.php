@extends('frontend.layouts.create_schedule')
@section('content')
<div class="global_navi" role="heading" aria-level="2">&nbsp;<img src="{{asset('/img/file20.gif')}}" border="0" alt=""><span class="globalNavi-item-grn"><a href="{!!route('frontend.cabinet.index')!!}">Folder</a></span></div>
<div class="mainarea ">
   <h2 style="display:inline;" class="cabinet">Add folder</h2>
   <form name="cabinet/modify" method="post" action="{!!route('frontend.cabinet.store_folder')!!}">
      <input type="hidden" name="_token" value="{{ csrf_token() }}" />
      <table class="std_form" style="border-collapse: separate;">
         <tbody>
            <tr>
               <th>Position</th>
               <td>
                   <select name="parent_id">
                       {!!$parent_html!!}
                   </select>
               </td>
            </tr>
            <tr>
               <th>Name folder</th>
               <td><input type="text" name="name" class="cb_text_ess" size="50" maxlength="100" title="Name folder"></td>
            </tr>
            <tr>
               <th>Ordering</th>
               <td><input type="text" name="ordering" class="cb_text_ess" size="50" maxlength="100" title="Ordering"></td>
            </tr>
            <tr>
                <th>Visibility</th>
                <td>
                     <span class="radiobutton_base_grn">
                         <input type="radio" name="private" id="1" value="0" onclick="display_off('private_select')" checked="">
                         <label for="1" onmouseover="this.style.color='#ff0000'" onmouseout="this.style.color=''">Public</label>
                     </span>
                     <span class="radiobutton_base_grn">
                         <input type="radio" name="private" id="2" value="1" onclick="display_on('private_select')"><label for="2" onmouseover="this.style.color='#ff0000'" onmouseout="this.style.color=''" style="">Set private permission▼</label>
                     </span>
                </td>
            </tr>
            <tr id="private_select" valign="top" style="display: none;">
                <th>Watchers</th>
                <td>
                  <style type="text/css">
                      #spinner-loading-p_CGID {
                        background-image: url({!!asset('public/img/spinner.gif')!!});
                      }
                  </style>
                  <link href="{!!asset('assets/css/pulldown_menu.css')!!}" rel="stylesheet" type="text/css">
                  <script src="{!!asset('/js/pubsub.js')!!}" type="text/javascript"></script>
                  <script src="{!!asset('/js/member_add.js')!!}" type="text/javascript"></script>
                  <script src="{!!asset('/js/member_select_list.js')!!}" type="text/javascript"></script>
                  <script src="{!!asset('/js/pulldown_menu.js')!!}" type="text/javascript"></script>
                   <script language="JavaScript" type="text/javascript">
                      <!--
                      new grn.component.member_add.MemberAdd("private_select", "schedule/add", ["p_sUID"], "p_CID",
                              {
                                  categorySelectUrl: grn.component.url.page('api/ajax_user_add_select_by_group'),
                                  searchBoxOptions: {"is_use":true,"id_searchbox":"private_menu","url":"api/search_members_by_keyword","append_post_data":[]},
                                  pulldownPartsOptions: {"is_use":true,"pulldown_id":"p_CID_pulldown"},
                                  appId: "schedule",
                                  isCalendar: false,
                                  showGroupRole: true,
                                  includeOrg: "1",
                                  accessPlugin: true,
                                  accessPluginEncoded: "YToyOntzOjQ6Im5hbWUiO3M6ODoic2NoZWR1bGUiO3M6NjoicGFyYW1zIjthOjI6e3M6NjoiYWN0aW9uIjthOjE6e2k6MDtzOjQ6InJlYWQiO31zOjEyOiJzZXNzaW9uX25hbWUiO3M6MTc6InNjaGVkdWxlL2FkZC92aWV3Ijt9fQ==",
                                  pluginSessionName: "private_menu",
                                  pluginDataName: "access_plugin_private_menu",
                                  addOrgWithUsers: false,
                                  showOmitted: false,
                                  useCandidateSupportParts: false,
                                  operatorAddName: "",
                                  selectAllUsersInSearch: false            }
                      );
                      //-->
                   </script>
                   <input type="hidden" name="member_id" id='selected_users_p_sUID' value="">
                   <table class="table_plain_grn selectlist_base_grn">
                      <tbody>
                         <tr>
                            <td class="vAlignTop-grn" style="padding-left:0">
                               <table class="table_plain_grn">
                                  <tbody>
                                     <tr>
                                        <td class="buttonSlectOrder-grn">
                                           <div id="p_sUID_order_top" class="mBottom10">
                                              <a class="order_top_grn" aria-label="Move to top" title="Move to top" href="javascript:void(0)"></a>
                                           </div>
                                           <div id="p_sUID_order_up" class="mBottom10">
                                              <a class="order_up_grn" aria-label="Move up" title="Move up" href="javascript:void(0)"></a>
                                           </div>
                                           <div id="p_sUID_order_down" class="mBottom10">
                                              <a class="order_down_grn" aria-label="Move down" title="Move down" href="javascript:void(0)"></a>
                                           </div>
                                           <div id="p_sUID_order_bottom" class="mBottom10">
                                              <a class="order_bottom_grn" aria-label="Move to bottom" title="Move to bottom" href="javascript:void(0)"></a>
                                           </div>
                                        </td>
                                        <td class="item_select">
                                           <div class="selectlist_area_grn">
                                              <span id="spinner_selectlist_p_sUID" style="display:none;position:absolute;z-index:1;"><img src="{{asset('/img/spinner.gif')}}" border="0" alt=""></span>
                                              <div id="selectlist_base_selectlist_p_sUID" aria-multiselectable="true" class="selectlist_grn selectlist_l_grn focus_grn" tabindex="0" style="">
                                                 <ul id="ul_selectlist_p_sUID">
                                                 </ul>
                                                 <span id="spinner_scroll_selectlist_p_sUID" style="display:none;"><img src="{{asset('/img/spinner.gif')}}" border="0" alt=""></span>
                                              </div>
                                           </div>
                                           <div class="textSub-grn mTop5"><a id="select_all_selectlist_p_sUID" class="mRight20" href="javascript:void(0);">Select all</a><a id="un_select_all_selectlist_p_sUID" style="display:none" class="mRight20" href="javascript:void(0);">Clear all</a></div>
                                        </td>
                                     </tr>
                                  </tbody>
                               </table>
                            </td>
                            <td class="vAlignTop-grn item_right_left">
                               <div class="buttonSelectMove-grn">
                                  <div class="mBottom15">
                                     <span class="aButtonStandard-grn">
                                     <a role="button" id="btn_add_p_sUID" style="padding-left:0;" href="javascript:void(0);">
                                     <span class="icon-buttonArrowLeft-grn"></span><span class="aButtonText-grn">Add</span>
                                     </a>
                                     </span>
                                  </div>
                                  <div>
                                     <span class="aButtonStandard-grn">
                                     <a role="button" id="btn_rmv_p_sUID" style="padding-right:0;" href="javascript:void(0);">
                                     <span class="aButtonText-grn">Remove</span><span class="icon-buttonArrowRightBehind-grn"></span>
                                     </a>
                                     </span>
                                  </div>
                               </div>
                            </td>
                            <td class="vAlignTop-grn">
                               <div class="mTop3 mBottom7 clearFix-cybozu">
                                  <div class="search_navi">
                                     <div class="searchbox-grn">
                                        <div id="searchbox-cybozu-private_menu" class="input-text-outer-cybozu searchbox-keyword-area searchbox_keyword_grn">
                                           <input class="input-text-cybozu prefix-grn" style="width:246px;" type="text" id="keyword_private_menu" name="" autocomplete="off" value="User search" maxlength="">
                                           <button id="searchbox-submit-private_menu" class="searchbox-submit-cybozu" type="button" title="Search" aria-label="Search"></button>
                                        </div>
                                        <div class="clear_both_0px"></div>
                                     </div>
                                  </div>
                               </div>
                               <!-- category select -->
                               <div class="mBottom7 nowrap-grn">
                                  <dl id="p_CID_pulldown" class="selectmenu_grn selectmenu_base_grn">
                                     <dt><a href="javascript:void(0)" class="nowrap-grn"><span></span><span class="selectlist_selectmenu_item_grn pulldown_head"></span><span class="pulldownbutton_arrow_down_grn mLeft3"></span></a></dt>
                                     <dd>
                                        <div class="pulldown_menu_grn" style="display: none;">
                                            <ul>
                                               <li ><a href="javascript:void(0)"><span class="vAlignMiddle-grn">Tất cả</span></a></li>
                                               @foreach($departments as $key=>$val)
                                               <li data-value="{{$val->id}}" ><a href="javascript:void(0)"><span class="vAlignMiddle-grn">{{$val->name}} </span></a></li>
                                               @endforeach
                                            </ul>
                                         </div>
                                     </dd>
                                  </dl>
                                  
                               </div>
                               <!-- category select -->
                               <div class="selectlist_area_grn">
                                  <span id="spinner_selectlist_p_CID" style="position: absolute; z-index: 1; display: none;"><img src="{{asset('/img/spinner.gif')}}" border="0" alt=""></span>
                                  <div id="selectlist_base_selectlist_p_CID" aria-multiselectable="true" class="selectlist_grn selectlist_r_grn" tabindex="0" style="">
                                     <ul id="ul_selectlist_p_CID">
                                        
                                     </ul>
                                     <span id="spinner_scroll_selectlist_p_CID" style="display:none;"><img src="{{asset('/img/spinner.gif')}}" border="0" alt=""></span>
                                  </div>
                               </div>
                            </td>
                         </tr>
                      </tbody>
                   </table>
                </td>
            </tr>
            <tr>
               <td></td>
               <td>
                  <div class="mTop10">
                     <span id="filemodify_button_save" class="button_grn_js button1_main_grn  button1_r_margin2_grn" onclick="submit_form(this);" data-auto-disable="true">
                         <a href="javascript:void(0);" role="button">Save</a>
                     </span>
                      <span id="filemodify_button_cancel" class="button_grn_js button1_normal_grn" onclick="grn.component.button.util.redirect(this,'history_back');">
                          <a href="javascript:void(0);" role="button">Cancel</a>
                      </span>
                  </div>
               </td>
            </tr>
         </tbody>
      </table>
   </form>
</div>
<script>
    function submit_form(e){
        var member_id =[];
        $('#ul_selectlist_p_sUID .selectlist_p_sUID').each(function(){
             member_id.push($(this).attr('data-value'));
        })
        member_id = member_id.join(':');
        $('#selected_users_p_sUID').val(member_id);
        $('form').submit();
    }
</script>
@stop
@section('script')
@parent
@stop
