<div class="sidebar-top">
    <ul>
       <li>
          <a href="{{route('home.view')}}">
             <div class="">
                <img src="{{asset('/img/Portal.png')}}">
                <h6>Portal</h6>
             </div>
          </a>
       </li>
       <li>
          <a href="{{route('frontend.schedule.index')}}?">
             <div class="">
                <img src="{{asset('/img/Scheduler.png')}}">
                <h6>Scheduler</h6>
             </div>
          </a>
       </li>
       <li>
          <a href="{!!route('frontend.cabinet.index')!!}">
             <div class="">
                <img src="{{asset('/img/Cabinet.png')}}">
                <h6>Cabinet</h6>
             </div>
          </a>
       </li>
       <li>
          <a href="{!!route('frontend.todo.index')!!}">
             <div class="">
                <img src="{{asset('/img/To_do_list.png')}}">
                <h6>To-do List</h6>
             </div>
          </a>
       </li>
       <li>
          <a href="#">
             <div class="">
                <img src="{{asset('/img/Time set.png')}}">
                <h6>Time set</h6>
             </div>
          </a>
       </li>
       <li>
          <a href="{!!route('home.scheme')!!}">
             <div class="">
                <img src="{{asset('/img/kaizen.png')}}">
                <h6>Kaizen</h6>
             </div>
          </a>
       </li>
    </ul>
    <div class="sidebar-bottom">
       <div class="button-off">
          <a class="sidebar-top-off on"><i class="fas fa-chevron-up"></i></a>
       </div>
    </div>
</div>
<script>
   $('.sidebar-top-off').click(function () {
   if ($(this).hasClass('on')) {
   $('.sidebar-top ul').hide(300);
   $(this).html('<i class="fas fa-chevron-down"></i>')
   $(this).removeClass('on');
   } else {
   $('.sidebar-top ul').show(300);
   $(this).html('<i class="fas fa-chevron-up"></i>');
   $(this).addClass('on');
   }
   })
</script>
