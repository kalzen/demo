@extends('frontend.layouts.create_schedule')
@section('content')
<div class="global_navi" role="heading" aria-level="2" style="border:none;" style="border:none;margin-bottom: 20px;">
   <a href="@if(Session::get('redirect_route')) {{route(Session::get('redirect_route'))}} @endif" style="height: 34px;vertical-align: middle;line-height: 50px;display: inline-block;padding: 0px 20px;font-size: 20px;border-right: 1px solid #333;margin-top: 10px;line-height: 4px;color: #333;">
   <img src="{{asset('/img/schedule20.gif')}}" border="0" alt="" style="margin-bottom: 10px;width: 30px;">
   Scheduler
   </a>
   <span class="globalNavi-item-last-grn" style="padding: 0 15px;top: 6px;font-size: 20px;color: #fe7701;font-weight: 400;">
   Edit Apointerment
   </span>
</div>
<div class="mainarea ">
   <div class="mainareaSchedule-grn">
      <script src="{{asset('/js/schedules.js')}}" type="text/javascript"></script>
      <div><span class="bold">{{date('D, F d, Y')}}</span></div>
      <h2 style="display:inline-block;" class="schedule">Edit appointment</h2>
      <form class="js_customization_form" id="schedule/repeat_modify" name="schedule/repeat_modify" method="post" action="{{route('frontend.schedule.update',$schedule->id)}}">
         <input type="hidden" name="csrf_ticket" value="c5fa6d49f53be0b2b9b23a0f404b9df5">
         <div class="js_customization_schedule_header_space"></div>
         <div id="show_error" class="attention" style="display:none"><span class="bold" >Select range to change</span></div>
         <div class=""><span class="attention">*</span> is required.</div>
         <table class="std_form" style="border-collapse: separate;">
            <tr valign="top">
               <th>Range<span class="attention">*</span></th>
               <td>
                  <div class="mBottom3">
                     <span class="radiobutton_base_grn">
                     <input type="radio" name="apply" id="apply3" value="this" checked="" onclick="selectOff('ymd',this.form,'start_');selectOff('ymd',this.form,'end_');" ><label for="apply3" onMouseOver="this.style.color='#ff0000'" onMouseOut="this.style.color=''">This appointment only {{date('l, F d, Y',strtotime($schedule->start_date))}}</label>
                     </span>
                  </div>
                  <div class="mBottom3">
                     <span class="radiobutton_base_grn">
                     <input type="radio" name="apply" id="apply4" value="after" onclick="selectOn('ymd',this.form,'end_');changeDate(this.form,'start_',{{date('Y',strtotime($schedule->start_repeat))}},{{date('m',strtotime($schedule->start_repeat))}},{{date('d',strtotime($schedule->start_repeat))}});selectOff('ymd',this.form,'start_');"><label for="apply4" onMouseOver="this.style.color='#ff0000'" onMouseOut="this.style.color=''">Appointments on and after {{date('l, F d, Y',strtotime($schedule->start_date))}}</label>
                     </span>
                  </div>
                  <div class="mBottom3">
                     <span class="radiobutton_base_grn">
                     <input type="radio" name="apply" id="apply5" value="all" onclick="selectOn('ymd',this.form,'start_');selectOn('ymd',this.form,'end_');changeDate(this.form,'start_',{{date('Y',strtotime($schedule->start_repeat))}},{{date('m',strtotime($schedule->start_repeat))}},{{date('d',strtotime($schedule->start_repeat))}})"><label for="apply5" onMouseOver="this.style.color='#ff0000'" onMouseOut="this.style.color=''" >All appointments</label>
                     </span>
                  </div>
               </td>
            </tr>
            <tr valign="top">
              <th>Date</th>
              <td>
                 <div class="sub_title mBottom3">Repeating conditions</div>
                 <div class="mBottom3">
                    <span class="radiobutton_base_grn"><input type="radio" name="type" id="day" value="day"  @if($schedule->type_repeat == 'day') checked @endif><label for="day" onMouseOver="this.style.color='#ff0000'" onMouseOut="this.style.color=''">Every day</label></span>
                 </div>
                 <div class="mBottom3">
                    <span class="radiobutton_base_grn"><input type="radio" name="type" id="weekday" value="weekday"  @if($schedule->type_repeat == 'weekday') checked @endif><label for="weekday" onMouseOver="this.style.color='#ff0000'" onMouseOut="this.style.color=''">Every weekday</label></span>
                 </div>
                 <div class="mBottom3">
                    <span class="radiobutton_base_grn">
                       <input type="radio" name="type" id="week" value="week" @if($schedule->type_repeat == 'week') checked @endif>
                        Every week
                       <select name="wday[]" class="select" onclick="radio_select('schedule/repeat_modify','type','week')" multiple="" data-placeholder="">
                         {!!$select_week_html!!}
                       </select>
                    </span>
                 </div>
                 <div class="mBottom15">
                    <span class="radiobutton_base_grn">
                       <input type="radio" name="type" id="3month" value="month" @if($schedule->type_repeat == 'month') checked @endif ><label for="3month" onMouseOver="this.style.color='#ff0000'" onMouseOut="this.style.color=''">Every month</label>
                       <select name="day" onclick="radio_select('schedule/repeat_modify','type','month')">
                          {!!$select_month_html!!}
                       </select>
                    </span>
                 </div>
                 <div class="sub_title mBottom3">Period</div>
                 <script language="JavaScript" type="text/javascript">
                    function change_enddate()
                    {
                        form = document.forms["schedule/repeat_modify"];
                        cb_ui_select_date_change_enddate( form );
                    }
                    //-->
                 </script>
                 <script lang="javascript" type="text/javascript">
                    <!--
                    var wday_name = new Array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
                    var openIds = new Array(0);
                    //-->
                 </script>
                 <script lang="javascript" type="text/javascript" src="{{asset('/js/select_date.js')}}"></script>
                 <script language="javascript" type="text/javascript"><!--
                    var g_arrayHolidays = new Array();
                    g_arrayHolidays[0] = "2011-01-01";
                    g_arrayHolidays[1] = "2011-01-10";
                    g_arrayHolidays[2] = "2011-02-11";
                    g_arrayHolidays[3] = "2011-03-21";
                    g_arrayHolidays[4] = "2011-04-29";
                    g_arrayHolidays[5] = "2011-05-03";
                    g_arrayHolidays[6] = "2011-05-04";
                    g_arrayHolidays[7] = "2011-05-05";
                    g_arrayHolidays[8] = "2011-07-18";
                    g_arrayHolidays[9] = "2011-09-19";
                    g_arrayHolidays[10] = "2011-09-23";
                    g_arrayHolidays[11] = "2011-10-10";
                    g_arrayHolidays[12] = "2011-11-03";
                    g_arrayHolidays[13] = "2011-11-23";
                    g_arrayHolidays[14] = "2011-12-23";
                    g_arrayHolidays[15] = "2012-01-01";
                    g_arrayHolidays[16] = "2012-01-02";
                    g_arrayHolidays[17] = "2012-01-09";
                    g_arrayHolidays[18] = "2012-02-11";
                    g_arrayHolidays[19] = "2012-03-20";
                    g_arrayHolidays[20] = "2012-04-29";
                    g_arrayHolidays[21] = "2012-04-30";
                    g_arrayHolidays[22] = "2012-05-03";
                    g_arrayHolidays[23] = "2012-05-04";
                    g_arrayHolidays[24] = "2012-05-05";
                    g_arrayHolidays[25] = "2012-07-16";
                    g_arrayHolidays[26] = "2012-09-17";
                    g_arrayHolidays[27] = "2012-09-22";
                    g_arrayHolidays[28] = "2012-10-08";
                    g_arrayHolidays[29] = "2012-11-03";
                    g_arrayHolidays[30] = "2012-11-23";
                    g_arrayHolidays[31] = "2012-12-23";
                    g_arrayHolidays[32] = "2012-12-24";
                    g_arrayHolidays[33] = "2013-01-01";
                    g_arrayHolidays[34] = "2013-01-14";
                    g_arrayHolidays[35] = "2013-02-11";
                    g_arrayHolidays[36] = "2013-03-20";
                    g_arrayHolidays[37] = "2013-04-29";
                    g_arrayHolidays[38] = "2013-05-03";
                    g_arrayHolidays[39] = "2013-05-04";
                    g_arrayHolidays[40] = "2013-05-05";
                    g_arrayHolidays[41] = "2013-05-06";
                    g_arrayHolidays[42] = "2013-07-15";
                    g_arrayHolidays[43] = "2013-09-16";
                    g_arrayHolidays[44] = "2013-09-23";
                    g_arrayHolidays[45] = "2013-10-14";
                    g_arrayHolidays[46] = "2013-11-03";
                    g_arrayHolidays[47] = "2013-11-04";
                    g_arrayHolidays[48] = "2013-11-23";
                    g_arrayHolidays[49] = "2013-12-23";
                    g_arrayHolidays[50] = "2014-01-01";
                    g_arrayHolidays[51] = "2014-01-13";
                    g_arrayHolidays[52] = "2014-02-11";
                    g_arrayHolidays[53] = "2014-03-21";
                    g_arrayHolidays[54] = "2014-04-29";
                    g_arrayHolidays[55] = "2014-05-03";
                    g_arrayHolidays[56] = "2014-05-04";
                    g_arrayHolidays[57] = "2014-05-05";
                    g_arrayHolidays[58] = "2014-05-06";
                    g_arrayHolidays[59] = "2014-07-21";
                    g_arrayHolidays[60] = "2014-09-15";
                    g_arrayHolidays[61] = "2014-09-23";
                    g_arrayHolidays[62] = "2014-10-13";
                    g_arrayHolidays[63] = "2014-11-03";
                    g_arrayHolidays[64] = "2014-11-23";
                    g_arrayHolidays[65] = "2014-11-24";
                    g_arrayHolidays[66] = "2014-12-23";
                    g_arrayHolidays[67] = "2015-01-01";
                    g_arrayHolidays[68] = "2015-01-12";
                    g_arrayHolidays[69] = "2015-02-11";
                    g_arrayHolidays[70] = "2015-03-21";
                    g_arrayHolidays[71] = "2015-04-29";
                    g_arrayHolidays[72] = "2015-05-03";
                    g_arrayHolidays[73] = "2015-05-04";
                    g_arrayHolidays[74] = "2015-05-05";
                    g_arrayHolidays[75] = "2015-05-06";
                    g_arrayHolidays[76] = "2015-07-20";
                    g_arrayHolidays[77] = "2015-09-21";
                    g_arrayHolidays[78] = "2015-09-22";
                    g_arrayHolidays[79] = "2015-09-23";
                    g_arrayHolidays[80] = "2015-10-12";
                    g_arrayHolidays[81] = "2015-11-03";
                    g_arrayHolidays[82] = "2015-11-23";
                    g_arrayHolidays[83] = "2015-12-23";
                    
                    var g_arrayWorkdays = new Array();
                    
                    function cb_ui_select_date_init_date_select( form_name, year, month, day, prefix)
                    {
                        var year_select = document.forms[form_name].elements[prefix+"year"];
                        if(year_select == undefined) return false;
                        var month_select = document.forms[form_name].elements[prefix+"month"];
                        if(month_select == undefined) return false;
                        var day_select = document.forms[form_name].elements[prefix+"day"];
                        if(day_select == undefined) return false;
                    
                        var year_options = year_select.options;
                        var month_options = month_select.options;
                        var day_options = day_select.options;
                        var select_year = 0;
                    
                        for(var i = 0 ; i <= year_options.length - 1 ; i++)
                        {
                            if(year_options[i].value == year)
                            {
                                year_options[i].selected = true;
                                select_year = 1;
                            }
                        }
                    
                        if( ! select_year)
                        {
                            if(year_options[0].value > year)
                            {
                                var o = document.createElement( "OPTION" );
                                year_options.add(o);
                                for(var i = year_options.length - 1 ; i > 0 ; i --)
                                {
                                    var dst = year_options[i];
                                    var src = year_options[i - 1];
                                    dst.value = src.value;
                                    dst.text = src.text;
                                    dst.selected = false;
                                }
                                year_options[0].value = year;
                                year_options[0].text = year + ' year';
                                year_options[0].selected = true;
                            }
                            else
                            {
                                var o = document.createElement( "OPTION" );
                                o.value = year;
                                o.text = year + ' year';
                                o.selected = true;
                                year_options.add( o );
                            }
                        }
                    
                        for(var i = 0 ; i <= month_options.length - 1 ; i++)
                        {
                            if(month_options[i].value == month)
                            {
                                month_options[i].selected = true;
                            }
                        }
                    
                        var start_date = new Date(year, month - 1, 1);
                        var wday = start_date.getDay();
                        var last_day = cb_ui_select_date_get_last_day(year, month);
                        var offset = 0;
                        if( day_options.length > 0 && day_options[0].value < 1 )
                        {
                            offset = 1;
                            day_options[0].className = "";
                            day_options[0].text = "--";
                        }
                    
                        for(i = 0 ; i < last_day ; i ++)
                        {
                            day_option = i + 1;
                            opt_idx = i + offset;
                    
                            if(opt_idx >= day_options.length)
                            {
                                cb_ui_select_date_add_option(day_options, day_option);
                            }
                            day_options[opt_idx].value = day_option;
                    
                            if(wday == 0)
                            {
                                day_options[opt_idx].className = "s_date_sunday";
                            }
                            else if(wday == 6)
                            {
                                day_options[opt_idx].className = "s_date_saturday";
                            }
                            else
                            {
                                day_options[opt_idx].className = "";
                            }
                    
                            if(g_arrayHolidays != null && g_arrayHolidays.length > 0)
                            {
                                if(cb_ui_select_date_check_holiday(g_arrayHolidays, year, month, day_option))
                                {
                                    day_options[opt_idx].className = "s_date_holiday";
                                }
                            }
                    
                            day_options[opt_idx].text = day_option + "(" + wday_name[wday] + ")";
                            if(day_options[opt_idx].value == day)
                            {
                                day_options[opt_idx].selected = true;
                            }
                    
                            wday ++;
                            if(wday > 6)
                            {
                                wday = 0;
                            }
                        }
                        while((last_day+offset) < day_options.length)
                        {
                            if(day_options.remove)
                            {
                                day_options.remove(day_options.length - 1);
                            }
                            else
                            {
                                day_options[day_options.length - 1] = null;
                            }
                        }
                    
                        if ( year == "1970" && month == "1" && day_options[offset].value == "1")
                        {
                            if(day_options.remove)
                            {
                                day_options.remove(offset);
                                day_options.remove(offset);
                            }
                            else
                            {
                                day_options[offset] = null;
                                day_options[offset] = null;
                            }
                        }
                    
                    }
                    var G_yearUnit = "";
                    
                    function open_iframe (form_name, prefix, frame_src)
                    {
                        if (form_name == null || form_name == "")
                        {
                            var select_year  = document.getElementById(prefix+"year");
                            var select_month = document.getElementById(prefix+"month");
                            var select_day   = document.getElementById(prefix+"day");
                        }
                        else
                        {
                            form = document.forms[form_name];
                            var select_year  = form.elements[prefix+"year"];
                            var select_month = form.elements[prefix+"month"];
                            var select_day   = form.elements[prefix+"day"];
                        }
                    
                        if(!select_year.disabled)
                        {
                            var year = select_year.options[select_year.selectedIndex].value;
                            var month = select_month.options[select_month.selectedIndex].value;
                            var day = select_day.options[select_day.selectedIndex].value;
                            frame_src = frame_src + '&date=' + year + '-' + month + '-' + day;
                    
                            var id = form_name+prefix+"SetDateCal";
                            for(i=0; i < openIds.length; i++)
                            {
                                if (openIds[i] != id)
                                {
                                    e = document.getElementById(openIds[i]);
                                    if( e && e.style )
                                    {
                                        if( e.style.display == "" )
                                        {
                                            e.style.display = "none";
                                        }
                                    }
                                }
                            }
                            var f = document.getElementById(id);
                            if(f && f.src)
                            {
                                f.src = frame_src;
                            }
                            cb_ui_select_date_display_calendar(id);
                            openIds = new Array(id);
                            if( typeof parent.update_back_step == 'function' )
                            {
                                parent.update_back_step();
                            }
                        }
                    }
                    //-->
                 </script>
                 <select id="start_month" name="start_month" onChange="cb_ui_select_date_init_day(this.form, 'start_');change_enddate();">
                    {!!$start_month_html!!}
                 </select>
                 <select id="start_day" name="start_day" onChange="change_enddate();">
                    {!!$start_day_html!!}
                 </select>
                 <select id="start_year" name="start_year"onChange="cb_ui_select_date_init_day(this.form, 'start_');cb_ui_select_date_reset_year_range(this.form, 'start_');change_enddate();">
                    {!!$start_year_html!!}
                 </select>
                 <script language="javascript" type="text/javascript">
                    function reinit_day()
                    {
                        var _prefix = "start_";
                        var _form = document.forms["schedule/repeat_modify"];
                        var _select_day = _form.elements[_prefix + "day"];
                        if(_select_day != null)
                        {
                            var _day = _select_day.options[_select_day.selectedIndex].value;
                    
                            cb_ui_select_date_init_day(_form, _prefix);
                    
                            for(i = 0 ; i < _select_day.options.length ; i ++)
                            {
                                if(_select_day.options[i].value == _day) 
                                {
                                    _select_day.options[i].selected = true;
                                }
                            }
                        }
                    }
                    
                    if (window.addEventListener)
                    {
                        window.addEventListener('load', reinit_day, false);
                    }
                    else if (window.attachEvent)
                    {
                        window.attachEvent('onload', reinit_day);
                    }
                    
                 </script><a name="" style="cursor: pointer"><img src="{{asset('/img/calendar20.gif')}}" border="0" align="absmiddle" style="cursor:hand;" onClick='open_iframe("schedule/repeat_modify", "start_", "/schedule/popup_calendar?prefix=start_&form_name=schedule/repeat_modify&on_change=change_enddate();");' title="To select a date in the past three years or more, use the date selection calendar." ></a>
                 <iframe id="schedule/repeat_modifystart_SetDateCal" frameborder="no" scrolling="no" style="display:none; position:absolute; width:308px; height:17.5em;z-index:10;" src=""></iframe> - 
                 <select id="end_month" name="end_month" onChange="cb_ui_select_date_init_day(this.form, 'end_');">
                    {!!$end_month_html!!}
                 </select>
                 <select id="end_day" name="end_day">
                    {!!$end_day_html!!}
                 </select>
                 <select id="end_year" name="end_year"onChange="cb_ui_select_date_init_day(this.form, 'end_');cb_ui_select_date_reset_year_range(this.form, 'end_');">
                    {!!$end_year_html!!}
                 </select>
                 <script language="javascript" type="text/javascript">
                    function reinit_day()
                    {
                        var _prefix = "end_";
                        var _form = document.forms["schedule/repeat_modify"];
                        var _select_day = _form.elements[_prefix + "day"];
                        if(_select_day != null)
                        {
                            var _day = _select_day.options[_select_day.selectedIndex].value;
                    
                            cb_ui_select_date_init_day(_form, _prefix);
                    
                            for(i = 0 ; i < _select_day.options.length ; i ++)
                            {
                                if(_select_day.options[i].value == _day) 
                                {
                                    _select_day.options[i].selected = true;
                                }
                            }
                        }
                    }
                    
                    if (window.addEventListener)
                    {
                        window.addEventListener('load', reinit_day, false);
                    }
                    else if (window.attachEvent)
                    {
                        window.attachEvent('onload', reinit_day);
                    }
                 </script><a name="" style="cursor: pointer"><img src="{{asset('/img/calendar20.gif')}}" border="0" align="absmiddle" style="cursor:hand;" onClick='open_iframe("schedule/repeat_modify", "end_", "/schedule/popup_calendar?prefix=end_&form_name=schedule/repeat_modify&on_change=");' title="To select a date in the past three years or more, use the date selection calendar." ></a>
                 <iframe id="schedule/repeat_modifyend_SetDateCal" frameborder="no" scrolling="no" style="display:none; position:absolute; width:308px; height:17.5em;z-index:10;" src=""></iframe>
                 <div><span id="span_time_span" class="margin_top sub_explanation"><span class="attention" id="invalid_date" style="display:none;">The end date must follow the start date.</span></span></div>
                 <div class="margin_top"><span class="sub_explanation">You can specify the period for&nbsp;1 years from the start date</span></div>
                 <p>
              </td>
            </tr>
            <tr>
               <th>Time</th>
               <td>
                  <select id="start_hour" name="start_hour"   >
                  {!!$start_hour_html!!}
                  </select>
                  <select id="start_minute" name="start_minute"  >
                  {!!$start_minute_html!!}
                  </select>
                  <script src="{{asset('/js/time_selector.js')}}" type="text/javascript"></script><script language="JavaScript" type="text/javascript">var grn_space_url = '/grn/space?';</script><img src="{{asset('/img/event.gif')}}" id="time_selector" name="time_selector" align="absmiddle" height="20" width="20" style="cursor: pointer;" title="You can specify the period with the start time and the ending time." onclick="grn_show_hide('time_selector', '/schedule/popup_time_selector?', 'schedule/repeat_modify', 'start_hour', 'end_hour', '0');"> - 
                  <select id="end_hour" name="end_hour"   >
                  {!!$end_hour_html!!}
                  </select>
                  <select id="end_minute" name="end_minute"  >
                  {!!$end_minute_html!!}
                  </select>
                  <span id="span_time_span" class="repeat_time_span margin_top sub_explanation"><span id="time_span">Required duration:</span><span id="hours_num" style="display:none;"></span><span id="hours_unit" style="display:none;"> h </span><span id="minutes_num" style="display:none;"></span><span id="minutes_unit" style="display:none;"> min.</span><span class="attention" id="validate_repeat_date" style="display:none;">Start time is empty.</span></span>
                  <script type="text/javascript" src="{{asset('/js/schedule_tz.js')}}" ></script>
                  <div class="overlay" style="display:none;" id="background"></div>
                  <div id="timezone_dialog" class="msgbox center" style="width:auto;display:none;">
                     <div id="timezone_title" class="title">Select time zone<a id="closeRelationDialog" style="position: absolute; right: 5px;top:5px;text-decoration:none;" onclick="GRN_ScheduleSelectTimezone.closeTimezoneDialog();" href="javascript:;"><img src="{{asset('/img/close20.gif')}}" border="0" alt=""></a></div>
                     <div id="timezone_content" class="content" style="width:450px;">
                        <div class="sub_text">Select time zone you want to apply.</div>
                        <div id="timezone_block" class="timezone_block">
                           <div id="div_timezone" class="select_timezone">
                              <div id="timezone_candidate-region-block" class="div_select_timezone">
                                 <select id="timezone_candidate-region" name="timezone_candidate-region" onchange="GRN_System_I18N_SelectTimezone.switchTimezoneRegion(this.options[this.selectedIndex].value, 'timezone_candidate');" class="select_timezone">
                                    <option value="(Favourite)">Frequently-used time zones</option>
                                    <option value="(All)">(All)</option>
                                    <option value="Etc">Other</option>
                                    <option value="Pacific">Pacific</option>
                                    <option value="America">America</option>
                                    <option value="Atlantic">Atlantic</option>
                                    <option value="Europe">Europe</option>
                                    <option value="Africa">Africa</option>
                                    <option value="Asia">Asia</option>
                                    <option value="Indian">Indian</option>
                                    <option value="Australia">Australia</option>
                                 </select>
                              </div>
                              <div id="all-timezone_candidate-block"  style="display:none" class="div_select_timezone">
                                 <select id="all-timezone_candidate" name="all-timezone_candidate" class="select_timezone">
                                    <option value="Etc/GMT+12">(UTC-12:00) UTC-12</option>
                                    <option value="Etc/GMT+11">(UTC-11:00) UTC-11</option>
                                    <option value="Etc/GMT+10">(UTC-10:00) UTC-10</option>
                                    <option value="Pacific/Honolulu">(UTC-10:00) Honolulu</option>
                                    <option value="America/Anchorage">(UTC-09:00) Anchorage</option>
                                    <option value="Etc/GMT+9">(UTC-09:00) UTC-9</option>
                                    <option value="America/Los_Angeles">(UTC-08:00) Los Angeles</option>
                                    <option value="America/Tijuana">(UTC-08:00) Tijuana</option>
                                    <option value="Etc/GMT+8">(UTC-08:00) UTC-8</option>
                                    <option value="America/Chihuahua">(UTC-07:00) Chihuahua</option>
                                    <option value="America/Denver">(UTC-07:00) Denver</option>
                                    <option value="America/Phoenix">(UTC-07:00) Phoenix</option>
                                    <option value="Etc/GMT+7">(UTC-07:00) UTC-7</option>
                                    <option value="America/Chicago">(UTC-06:00) Chicago</option>
                                    <option value="America/Guatemala">(UTC-06:00) Guatemala</option>
                                    <option value="America/Mexico_City">(UTC-06:00) Mexico City</option>
                                    <option value="America/Regina">(UTC-06:00) Regina</option>
                                    <option value="Etc/GMT+6">(UTC-06:00) UTC-6</option>
                                    <option value="America/Bogota">(UTC-05:00) Bogota</option>
                                    <option value="America/Indiana/Indianapolis">(UTC-05:00) Indianapolis, Indiana</option>
                                    <option value="America/New_York">(UTC-05:00) New York</option>
                                    <option value="Etc/GMT+5">(UTC-05:00) UTC-5</option>
                                    <option value="America/Asuncion">(UTC-04:00) Asunción</option>
                                    <option value="America/Caracas">(UTC-04:00) Caracas</option>
                                    <option value="America/Cuiaba">(UTC-04:00) Cuiaba</option>
                                    <option value="America/Halifax">(UTC-04:00) Halifax</option>
                                    <option value="America/La_Paz">(UTC-04:00) La Paz</option>
                                    <option value="America/Manaus">(UTC-04:00) Manaus</option>
                                    <option value="America/Santiago">(UTC-04:00) Santiago</option>
                                    <option value="Etc/GMT+4">(UTC-04:00) UTC-4</option>
                                    <option value="America/St_Johns">(UTC-03:30) St Johns</option>
                                    <option value="America/Argentina/Buenos_Aires">(UTC-03:00) Buenos Aires</option>
                                    <option value="America/Cayenne">(UTC-03:00) Cayenne</option>
                                    <option value="America/Godthab">(UTC-03:00) Godthab</option>
                                    <option value="America/Montevideo">(UTC-03:00) Montevideo</option>
                                    <option value="America/Sao_Paulo">(UTC-03:00) Sao Paulo</option>
                                    <option value="Etc/GMT+3">(UTC-03:00) UTC-3</option>
                                    <option value="Atlantic/South_Georgia">(UTC-02:00) South Georgia</option>
                                    <option value="Etc/GMT+2">(UTC-02:00) UTC-2</option>
                                    <option value="Atlantic/Azores">(UTC-01:00) Azores</option>
                                    <option value="Atlantic/Cape_Verde">(UTC-01:00) Cape Verde</option>
                                    <option value="Etc/GMT+1">(UTC-01:00) UTC-1</option>
                                    <option value="Atlantic/Reykjavik">(UTC+00:00) Reykjavik</option>
                                    <option value="Europe/London">(UTC+00:00) London</option>
                                    <option value="UTC">(UTC+00:00) UTC</option>
                                    <option value="Africa/Casablanca">(UTC+01:00) Casablanca</option>
                                    <option value="Africa/Lagos">(UTC+01:00) Lagos</option>
                                    <option value="Etc/GMT-1">(UTC+01:00) UTC+1</option>
                                    <option value="Europe/Berlin">(UTC+01:00) Berlin</option>
                                    <option value="Europe/Budapest">(UTC+01:00) Budapest</option>
                                    <option value="Europe/Paris">(UTC+01:00) Paris</option>
                                    <option value="Europe/Warsaw">(UTC+01:00) Warsaw</option>
                                    <option value="Africa/Cairo">(UTC+02:00) Cairo</option>
                                    <option value="Africa/Johannesburg">(UTC+02:00) Johannesburg</option>
                                    <option value="Africa/Windhoek">(UTC+02:00) Windhoek</option>
                                    <option value="Asia/Amman">(UTC+02:00) Amman</option>
                                    <option value="Asia/Beirut">(UTC+02:00) Beirut</option>
                                    <option value="Asia/Damascus">(UTC+02:00) Damascus</option>
                                    <option value="Asia/Jerusalem">(UTC+02:00) Jerusalem</option>
                                    <option value="Etc/GMT-2">(UTC+02:00) UTC+2</option>
                                    <option value="Europe/Kiev">(UTC+02:00) Kiev</option>
                                    <option value="Africa/Nairobi">(UTC+03:00) Nairobi</option>
                                    <option value="Asia/Baghdad">(UTC+03:00) Baghdad</option>
                                    <option value="Asia/Riyadh">(UTC+03:00) Riyadh</option>
                                    <option value="Etc/GMT-3">(UTC+03:00) UTC+3</option>
                                    <option value="Europe/Istanbul">(UTC+03:00) Istanbul</option>
                                    <option value="Europe/Minsk">(UTC+03:00) Minsk</option>
                                    <option value="Europe/Moscow">(UTC+03:00) Moscow</option>
                                    <option value="Asia/Tehran">(UTC+03:30) Tehran</option>
                                    <option value="Asia/Baku">(UTC+04:00) Baku</option>
                                    <option value="Asia/Dubai">(UTC+04:00) Dubai</option>
                                    <option value="Asia/Tbilisi">(UTC+04:00) Tbilisi</option>
                                    <option value="Asia/Yerevan">(UTC+04:00) Yerevan</option>
                                    <option value="Etc/GMT-4">(UTC+04:00) UTC+4</option>
                                    <option value="Indian/Mauritius">(UTC+04:00) Mauritius</option>
                                    <option value="Asia/Kabul">(UTC+04:30) Kabul</option>
                                    <option value="Asia/Karachi">(UTC+05:00) Karachi</option>
                                    <option value="Asia/Tashkent">(UTC+05:00) Tashkent</option>
                                    <option value="Asia/Yekaterinburg">(UTC+05:00) Yekaterinburg</option>
                                    <option value="Etc/GMT-5">(UTC+05:00) UTC+5</option>
                                    <option value="Asia/Colombo">(UTC+05:30) Colombo</option>
                                    <option value="Asia/Kolkata">(UTC+05:30) Kolkata</option>
                                    <option value="Asia/Kathmandu">(UTC+05:45) Kathmandu</option>
                                    <option value="Asia/Almaty">(UTC+06:00) Almaty</option>
                                    <option value="Asia/Dhaka">(UTC+06:00) Dhaka</option>
                                    <option value="Etc/GMT-6">(UTC+06:00) UTC+6</option>
                                    <option value="Asia/Yangon">(UTC+06:30) Yangon</option>
                                    <option value="Asia/Bangkok">(UTC+07:00) Bangkok</option>
                                    <option value="Asia/Krasnoyarsk">(UTC+07:00) Krasnoyarsk</option>
                                    <option value="Asia/Novosibirsk">(UTC+07:00) Novosibirsk</option>
                                    <option value="Etc/GMT-7">(UTC+07:00) UTC+7</option>
                                    <option value="Asia/Irkutsk">(UTC+08:00) Irkutsk</option>
                                    <option value="Asia/Shanghai">(UTC+08:00) Beijing</option>
                                    <option value="Asia/Singapore">(UTC+08:00) Singapore</option>
                                    <option value="Asia/Taipei">(UTC+08:00) Taipei</option>
                                    <option value="Asia/Ulaanbaatar">(UTC+08:00) Ulaanbaatar</option>
                                    <option value="Australia/Perth">(UTC+08:00) Perth</option>
                                    <option value="Etc/GMT-8">(UTC+08:00) UTC+8</option>
                                    <option value="Asia/Seoul">(UTC+09:00) Seoul</option>
                                    <option value="Asia/Tokyo" selected>(UTC+09:00) Tokyo</option>
                                    <option value="Asia/Yakutsk">(UTC+09:00) Yakutsk</option>
                                    <option value="Etc/GMT-9">(UTC+09:00) UTC+9</option>
                                    <option value="Australia/Adelaide">(UTC+09:30) Adelaide</option>
                                    <option value="Australia/Darwin">(UTC+09:30) Darwin</option>
                                    <option value="Asia/Vladivostok">(UTC+10:00) Vladivostok</option>
                                    <option value="Australia/Brisbane">(UTC+10:00) Brisbane</option>
                                    <option value="Australia/Hobart">(UTC+10:00) Hobart</option>
                                    <option value="Australia/Sydney">(UTC+10:00) Sydney</option>
                                    <option value="Etc/GMT-10">(UTC+10:00) UTC+10</option>
                                    <option value="Pacific/Port_Moresby">(UTC+10:00) Port Moresby</option>
                                    <option value="Asia/Magadan">(UTC+11:00) Magadan</option>
                                    <option value="Etc/GMT-11">(UTC+11:00) UTC+11</option>
                                    <option value="Pacific/Guadalcanal">(UTC+11:00) Guadalcanal</option>
                                    <option value="Asia/Kamchatka">(UTC+12:00) Kamchatka</option>
                                    <option value="Etc/GMT-12">(UTC+12:00) UTC+12</option>
                                    <option value="Pacific/Auckland">(UTC+12:00) Auckland</option>
                                    <option value="Pacific/Fiji">(UTC+12:00) Fiji</option>
                                    <option value="Pacific/Apia">(UTC+13:00) Apia</option>
                                    <option value="Pacific/Tongatapu">(UTC+13:00) Tongatapu</option>
                                 </select>
                              </div>
                              <div id="timezone_candidate-block" class="div_select_timezone">
                                 <select id="timezone_candidate" name="timezone_candidate" class="select_timezone">
                                    <option value="Asia/Tokyo" selected>(UTC+09:00) Tokyo</option>
                                    <option value="America/Los_Angeles">(UTC-08:00) Los Angeles</option>
                                    <option value="Asia/Singapore">(UTC+08:00) Singapore</option>
                                 </select>
                              </div>
                              <script type="text/javascript">
                                 var GRN_System_I18N_SelectTimezone = {
                                     allTimezoneOptions : (function(){
                                                              var options = [],
                                                                  elements = document.getElementById('all-timezone_candidate').getElementsByTagName('option'),
                                                                  elementsLength = elements.length;
                                                              for( var i=0; i<elementsLength; i+=1 ){
                                                                  options.push(elements[i].cloneNode(true));
                                                              }
                                                              return options;
                                                          }()),
                                     favouriteTimezoneOptions : (function(){
                                                              var options = [],
                                                                  elements = document.getElementById('timezone_candidate').getElementsByTagName('option'),
                                                                  elementsLength = elements.length;
                                                              for( var i=0; i<elementsLength; i+=1 ){
                                                                  options.push(elements[i].cloneNode(true));
                                                              }
                                                              return options;
                                                          }()),
                                     switchTimezoneRegion : function(region, key) {
                                         var options,
                                             max,
                                             regionLength = region.length;
                                         
                                         if( region === '(Favourite)' ){
                                             options = this.favouriteTimezoneOptions;
                                         }else{
                                             options = this.allTimezoneOptions;
                                         }
                                         max = options.length;
                                 
                                         document.getElementById(key).innerHTML = '';
                                         
                                         for( var i=0; i<max; i+=1 ){
                                             if( region === '(All)' || region === '(Favourite)' || region === options[i].value.substr(0, regionLength) || options[i].value === '' ){
                                                 document.getElementById(key).appendChild(options[i].cloneNode(true));
                                             }
                                         }
                                     }
                                 };
                              </script>
                           </div>
                        </div>
                     </div>
                     <div id="timezone_dialog_buttons" class="command">
                        <span id="apply_timezone" class="button_grn_js button1_main_grn  button1_r_margin2_grn" onclick="GRN_ScheduleSelectTimezone.applyTimezone();"  data-auto-disable=><a href="javascript:void(0);" role="button">Apply</a></span><span id="cancel_timezone" class="button_grn_js button1_normal_grn" onclick="GRN_ScheduleSelectTimezone.closeTimezoneDialog();"  data-auto-disable=><a href="javascript:void(0);" role="button">Cancel</a></span>
                     </div>
                  </div>
                  <input type='hidden' id='timezone' name='timezone' value='Asia/Tokyo' />
                  <input type='hidden' id='end_timezone' name='end_timezone' value='Asia/Tokyo' />
                  <div id="another_timezone" class="link_timezone"><span id="timezone_to_link"><span id="current_timezone">(UTC+07:00) VietNam</span><span>&nbsp;→&nbsp;</span></span><a id="opne_timezone_dialog" href="javascript:;" onclick="GRN_ScheduleSelectTimezone.openTimezoneDialog();">Other time zones</a></div>
               </td>
            </tr>
            <tr>
               <th>Subject</th>
               <td>
                  <select menu="schedule_menu" name="menu" class="hidden">
                     @if(!is_null($schedule->menu))
                     <option value="{{$schedule->menu}}">{{explode(',',$schedule->menu)[0]}}</option>
                     @endif
                     <option value="">-----</option>
                     <option value="Cuộc họp;#5">Cuộc họp</option>
                     <option value="Hội thảo;#2">Hội thảo</option>
                     <option value="Đào tạo;#1">Đào tạo</option>
                     <option value="Công tác;#7">Công tác</option>
                     <option value="Ngày lễ;#6">Ngày lễ</option>
                     <option value="Phỏng vấn;#3">Phỏng vấn</option>
                     <option value="Gặp gỡ;#4">Gặp gỡ</option>
                  </select>
                  &nbsp;<input  type="text" name="title" size="80" maxlength="100" value="{{$schedule->title}}" onKeyPress="return event.keyCode != 13;">
               </td>
            </tr>
            <tr valign="top">
               <th>Attendees</th>
               <td>
                  <style type="text/css">
                     #spinner-loading-CGID {
                     background-image: url({{asset('/img/spinner.gif')}});
                     }
                  </style>
                  <script src="{{asset('/js/pubsub.js')}}" type="text/javascript"></script>
                  <script src="{{asset('/js/member_add.js')}}" type="text/javascript"></script>
                  <script src="{{asset('/js/member_select_list.js')}}" type="text/javascript"></script>
                  <script src="{{asset('/js/pulldown_menu.js')}}" type="text/javascript"></script>
                  <script language="JavaScript" type="text/javascript">
                     <!--
                     new grn.component.member_add.MemberAdd("member_select", "schedule/repeat_modify", ["sUID"], "CID",
                             {
                                 categorySelectUrl: '/api/ajax_user_add_select_by_group',
                                 searchBoxOptions: {"is_use":true,"id_searchbox":"user","url":"/api/search_members_by_keyword","append_post_data":[]},
                                 pulldownPartsOptions: {"is_use":true,"pulldown_id":"CID_pulldown"},
                                 appId: "schedule",
                                 isCalendar: true,
                                 showGroupRole: true,
                                 includeOrg: "1",
                                 accessPlugin: true,
                                 accessPluginEncoded: "YToyOntzOjQ6Im5hbWUiO3M6ODoic2NoZWR1bGUiO3M6NjoicGFyYW1zIjthOjM6e3M6NjoiYWN0aW9uIjthOjM6e2k6MDtzOjQ6InJlYWQiO2k6MTtzOjM6ImFkZCI7aToyO3M6NjoibW9kaWZ5Ijt9czoxMjoibGF4X2V2YWx1YXRlIjtiOjE7czoxMjoic2Vzc2lvbl9uYW1lIjtzOjIyOiJzY2hlZHVsZS9yZXBlYXRfbW9kaWZ5Ijt9fQ==",
                                 pluginSessionName: "schedule/repeat_modify",
                                 pluginDataName: "access_plugin",
                                 addOrgWithUsers: false,
                                 showOmitted: false,
                                 useCandidateSupportParts: false,
                                 operatorAddName: "",
                                 selectAllUsersInSearch: false            }
                     );
                     //-->
                  </script>
                  <input type="hidden" name="selected_users_sUID" value="">
                  <input type="hidden" name="selected_groups_sUID" value="">
                  <input type="hidden" name="selected_roles_sUID" value="">
                  <input type="hidden" name="for_redirect_at_non_command_page[]" value="">
                  <table class="table_plain_grn selectlist_base_grn">
                     <tr>
                        <td class="vAlignTop-grn" style="padding-left:0">
                           <table class="table_plain_grn">
                              <tr>
                                 <td class="buttonSlectOrder-grn">
                                    <div id="sUID_order_top" class="mBottom10">
                                       <a class="order_top_grn"
                                          aria-label="Move to top"
                                          title="Move to top"
                                          href="javascript:void(0)"></a>
                                    </div>
                                    <div id="sUID_order_up" class="mBottom10">
                                       <a class="order_up_grn"
                                          aria-label="Move up"
                                          title="Move up"
                                          href="javascript:void(0)"></a>
                                    </div>
                                    <div id="sUID_order_down" class="mBottom10">
                                       <a class="order_down_grn"
                                          aria-label="Move down"
                                          title="Move down"
                                          href="javascript:void(0)"></a>
                                    </div>
                                    <div id="sUID_order_bottom" class="mBottom10">
                                       <a class="order_bottom_grn"
                                          aria-label="Move to bottom"
                                          title="Move to bottom"
                                          href="javascript:void(0)"></a>
                                    </div>
                                 </td>
                                 <td class="item_select">
                                    <div class="selectlist_area_grn">
                                       <span id="spinner_selectlist_sUID" style="display:none;position:absolute;z-index:1;"><img src="/garoon3/grn/image/cybozu/spinner.gif?20200925.text" border="0" alt=""></span>
                                       <div id="selectlist_base_selectlist_sUID" aria-multiselectable="true" class="selectlist_grn selectlist_l_grn focus_grn" tabindex="0" style="">
                                          <ul id="ul_selectlist_sUID">
                                             @foreach($schedule->member as $key=>$val)
                                             <li
                                                id="selectlist_sUID_member_user_{{$val->id}}"
                                                class="selectlist_sUID"
                                                data-value="{{$val->id}}"
                                                data-type="user"
                                                data-name="{{$val->full_name}}"
                                                data-code="brown"                    data-id="{{$val->id}}"
                                                data-group-path=""
                                                data-url="">
                                                <span class="@if($val->id == \Auth::guard('member')->user()->id)selectlist_user_login_grn @else selectlist_user_grn @endif" aria-label="Login user"
                                                   title="Login user"></span>
                                                <span class="selectlist_text_grn">{{$val->full_name}}</span>
                                             </li>
                                             @endforeach
                                          </ul>
                                          <span id="spinner_scroll_selectlist_sUID" style="display:none;"><img src="/garoon3/grn/image/cybozu/spinner.gif?20200925.text" border="0" alt=""></span>
                                       </div>
                                    </div>
                                    <div class="textSub-grn mTop5"><a id="select_all_selectlist_sUID" class="mRight20" href="javascript:void(0);">Select all</a><a id="un_select_all_selectlist_sUID" style="display:none" class="mRight20" href="javascript:void(0);">Clear all</a></div>
                                 </td>
                              </tr>
                           </table>
                        </td>
                        <td class="vAlignTop-grn item_right_left">
                           <div class="buttonSelectMove-grn">
                              <div class="mBottom15">
                                 <span class="aButtonStandard-grn">
                                 <a role="button" id="btn_add_sUID" style="padding-left:0;" href="javascript:void(0);">
                                 <span class="icon-buttonArrowLeft-grn"></span><span class="aButtonText-grn">Add</span>
                                 </a>
                                 </span>
                              </div>
                              <div>
                                 <span class="aButtonStandard-grn">
                                 <a role="button" id="btn_rmv_sUID" style="padding-right:0;" href="javascript:void(0);">
                                 <span class="aButtonText-grn">Remove</span><span class="icon-buttonArrowRightBehind-grn"></span>
                                 </a>
                                 </span>
                              </div>
                           </div>
                        </td>
                        <td class="vAlignTop-grn">
                           <div class="mTop3 mBottom7 clearFix-cybozu">
                              <script src="{{asset('/js/search_box.js')}}" type="text/javascript"></script>
                              <div class="search_navi">
                                 <div class="searchbox-grn">
                                    <div id="searchbox-cybozu-user"
                                       class="input-text-outer-cybozu searchbox-keyword-area searchbox_keyword_grn">
                                       <input class="input-text-cybozu prefix-grn" style="width:246px;" type="text"
                                          id="keyword_user" name="" autocomplete="off"
                                          value="User search"
                                          maxlength="">
                                       <button id="searchbox-submit-user" class="searchbox-submit-cybozu" type="button"
                                          title="Search"
                                          aria-label="Search"></button>
                                    </div>
                                    <div class="clear_both_0px"></div>
                                 </div>
                              </div>
                           </div>
                           <!-- category select -->
                           <link href="{{asset('assets/css/pulldown_menu.css')}}" rel="stylesheet" type="text/css">
                           <div class="mBottom7 nowrap-grn">
                              <dl id="CID_pulldown" class="selectmenu_grn selectmenu_base_grn">
                                 <dt><a href="javascript:void(0)" class="nowrap-grn"><span></span><span class="selectlist_selectmenu_item_grn pulldown_head"></span><span class="pulldownbutton_arrow_down_grn mLeft3"></span></a></dt>
                                 <dd>
                                    <div class="pulldown_menu_grn" style="display: none;">
                                       <ul>
                                          <li ><a href="javascript:void(0)"><span class="vAlignMiddle-grn">Tất cả</span></a></li>
                                          @foreach($department as $key=>$val)
                                          <li data-value="{{$val->id}}" ><a href="javascript:void(0)"><span class="vAlignMiddle-grn">{{$val->name}} </span></a></li>
                                          @endforeach
                                       </ul>
                                    </div>
                                 </dd>
                              </dl>
                              <a class="selectPulldownSub-grn" href="javascript:void(0);"onClick="javascript:popupWin('/grn/popup_member_select?plugin_session_name=schedule%2Frepeat_modify&amp;plugin_data_name=access_plugin&amp;is_post_message=&amp;selected_tid=&amp;form_name=schedule%2Frepeat_modify&amp;select_name=sUID&amp;app_id=schedule&amp;return_page=&amp;plid=&amp;system=0&amp;include_org=1&amp;system_display=0&amp;no_multiple=0&amp;send_cgi_parameter=0&amp;multi_apply=0&amp;require_role_tab=0&amp;is_calendar=1&amp;show_group_role=1&amp;require_dynamic_roles=0&amp;require_administrator_role=0','html','1014','675',0,0,1,1,0,1);"title="Select from all organizations"><img src="{{asset('/img/blankB16.png')}}" border="0" alt=""></a>
                           </div>
                           <!-- category select -->
                           <div class="selectlist_area_grn">
                              <span id="spinner_selectlist_CID" style="display:none;position:absolute;z-index:1;"><img src="/garoon3/grn/image/cybozu/spinner.gif?20200925.text" border="0" alt=""></span>
                              <div id="selectlist_base_selectlist_CID" aria-multiselectable="true" class="selectlist_grn selectlist_r_grn" tabindex="0" style="">
                                 <ul id="ul_selectlist_CID">
                                 </ul>
                                 <span id="spinner_scroll_selectlist_CID" style="display:none;"><img src="{{asset('/img/spinner.gif')}}" border="0" alt=""></span>
                              </div>
                           </div>
                           <div class="textSub-grn mTop5">
                              <a id="select_all_selectlist_CID" class="mRight20" href="javascript:void(0);">Select all</a><a id="un_select_all_selectlist_CID" style="display:none" class="mRight20" href="javascript:void(0);">Clear all</a>    <script language="JavaScript" type="text/javascript">
                                 <!--
                                 var popupMember_CID_count = 0;
                                 function popupMember_CID(Aform)
                                 {
                                     if ( ! popupMember_CID_count || document.getElementById('form_CID[]') == null)
                                     {
                                         var p = document.getElementById('div_CID[]');
                                         var c = document.createElement('DIV');
                                         p.style.display = "";
                                         p.appendChild(c);
                                         c.innerHTML = '<iframe id="form_CID[]" name="form_CID[]" frameborder="no" scrolling="no" style="position:absolute; width:0em; height:0em;" src=""></iframe>';
                                     }
                                 
                                     popupWin("","user_list",1034,675,0,0,0,1,0,1);
                                     var url = '/grn/popup_user_list?';
                                     var html = '<!DOCTYPE html><html lang="en"><body><form method="post" target="user_list" action="' + url + '">';
                                     html += '<input type="hidden" name="system_display" value="0">';
                                     var form_frame = document.getElementById('form_CID[]').contentWindow.document;
                                     var instance = grn.component.member_select_list.get_instance("CID");
                                     var values = instance.getSelectedUsersValues();
                                     html += '<input type="hidden" name="cid" value="' + values.join(':') + '">';
                                     html += '</form></body></html>';
                                     form_frame.write( html );
                                     form_frame.close();
                                     form_frame.forms[0].submit();
                                     popupMember_CID_count = 1;
                                     if( !grn.browser.chrome && !grn.browser.safari && typeof update_back_step == 'function' )
                                     {
                                         update_back_step();
                                     }
                                     return true;
                                 }
                                 //-->
                              </script>
                              <div id="div_CID[]" style="display:none; position:absolute; width:0em; height:0em;">&nbsp;</div>
                              <a href="javascript:void(0);" onClick="popupMember_CID();">User details</a>
                           </div>
                        </td>
                     </tr>
                  </table>
               </td>
            </tr>
            <tr valign="top">
               <th nowrap>Shared with</th>
               <td>
                  <style type="text/css">
                     #spinner-loading-p_CGID {
                     background-image: url({{asset('/img/spinner.gif')}});
                     }
                  </style>
                  <script language="JavaScript" type="text/javascript">
                     <!--
                     compo = new grn.component.member_add.MemberAdd("private_select", "schedule/repeat_modify", ["p_sUID"], "p_CID",
                             {
                                 categorySelectUrl: '/api/ajax_user_add_select_by_group',
                                 searchBoxOptions: {"is_use":true,"id_searchbox":"private_menu","url":"/api/search_members_by_keyword","append_post_data":[]},
                                 pulldownPartsOptions: {"is_use":true,"pulldown_id":"p_CID_pulldown"},
                                 appId: "schedule",
                                 isCalendar: false,
                                 showGroupRole: true,
                                 includeOrg: "1",
                                 accessPlugin: true,
                                 accessPluginEncoded: "YToyOntzOjQ6Im5hbWUiO3M6ODoic2NoZWR1bGUiO3M6NjoicGFyYW1zIjthOjI6e3M6NjoiYWN0aW9uIjthOjE6e2k6MDtzOjQ6InJlYWQiO31zOjEyOiJzZXNzaW9uX25hbWUiO3M6MTc6InNjaGVkdWxlL2FkZC92aWV3Ijt9fQ==",
                                 pluginSessionName: "private_menu",
                                 pluginDataName: "access_plugin_private_menu",
                                 addOrgWithUsers: false,
                                 showOmitted: false,
                                 useCandidateSupportParts: false,
                                 operatorAddName: "",
                                 selectAllUsersInSearch: false            }
                     );
                     
                     //-->
                  </script>
                  <input type="hidden" name="selected_users_p_sUID" value="">
                  <input type="hidden" name="selected_groups_p_sUID" value="">
                  <input type="hidden" name="selected_roles_p_sUID" value="">
                  <table class="table_plain_grn selectlist_base_grn">
                     <tr>
                        <td class="vAlignTop-grn" style="padding-left:0">
                           <table class="table_plain_grn">
                              <tr>
                                 <td class="buttonSlectOrder-grn">
                                    <div id="p_sUID_order_top" class="mBottom10">
                                       <a class="order_top_grn"
                                          aria-label="Move to top"
                                          title="Move to top"
                                          href="javascript:void(0)"></a>
                                    </div>
                                    <div id="p_sUID_order_up" class="mBottom10">
                                       <a class="order_up_grn"
                                          aria-label="Move up"
                                          title="Move up"
                                          href="javascript:void(0)"></a>
                                    </div>
                                    <div id="p_sUID_order_down" class="mBottom10">
                                       <a class="order_down_grn"
                                          aria-label="Move down"
                                          title="Move down"
                                          href="javascript:void(0)"></a>
                                    </div>
                                    <div id="p_sUID_order_bottom" class="mBottom10">
                                       <a class="order_bottom_grn"
                                          aria-label="Move to bottom"
                                          title="Move to bottom"
                                          href="javascript:void(0)"></a>
                                    </div>
                                 </td>
                                 <td class="item_select">
                                    <div class="selectlist_area_grn">
                                       <span id="spinner_selectlist_p_sUID" style="display:none;position:absolute;z-index:1;"><img src="{{asset('/img/spinner.gif')}}" border="0" alt=""></span>
                                       <div id="selectlist_base_selectlist_p_sUID" aria-multiselectable="true" class="selectlist_grn selectlist_l_grn focus_grn" tabindex="0" style="">
                                          <ul id="ul_selectlist_p_sUID">
                                             @foreach($schedule->seen as $key=>$val)
                                             <li
                                                id="selectlist_p_sUID_member_user_{{$val->id}}"
                                                class="selectlist_p_sUID"
                                                data-value="{{$val->id}}"
                                                data-type="user"
                                                data-name="{{$val->full_name}}"
                                                data-code="brown"                    data-id="{{$val->id}}"
                                                data-group-path=""
                                                data-url="">
                                                <span class="@if($schedule->uid == $val->id )selectlist_user_login_grn @else selectlist_user_grn @endif" aria-label="Login user"
                                                   title="Login user"></span>
                                                <span class="selectlist_text_grn">{{$val->full_name}}</span>
                                             </li>
                                             @endforeach
                                          </ul>
                                          <span id="spinner_scroll_selectlist_p_sUID" style="display:none;"><img src="{{asset('/img/spinner.gif')}}" border="0" alt=""></span>
                                       </div>
                                    </div>
                                    <div class="textSub-grn mTop5"><a id="select_all_selectlist_p_sUID" class="mRight20" href="javascript:void(0);">Select all</a><a id="un_select_all_selectlist_p_sUID" style="display:none" class="mRight20" href="javascript:void(0);">Clear all</a></div>
                                 </td>
                              </tr>
                           </table>
                        </td>
                        <td class="vAlignTop-grn item_right_left">
                           <div class="buttonSelectMove-grn">
                              <div class="mBottom15">
                                 <span class="aButtonStandard-grn">
                                 <a role="button" id="btn_add_p_sUID" style="padding-left:0;" href="javascript:void(0);">
                                 <span class="icon-buttonArrowLeft-grn"></span><span class="aButtonText-grn">Add</span>
                                 </a>
                                 </span>
                              </div>
                              <div>
                                 <span class="aButtonStandard-grn">
                                 <a role="button" id="btn_rmv_p_sUID" style="padding-right:0;" href="javascript:void(0);">
                                 <span class="aButtonText-grn">Remove</span><span class="icon-buttonArrowRightBehind-grn"></span>
                                 </a>
                                 </span>
                              </div>
                           </div>
                        </td>
                        <td class="vAlignTop-grn">
                           <div class="mTop3 mBottom7 clearFix-cybozu">
                              <div class="search_navi">
                                 <div class="searchbox-grn">
                                    <div id="searchbox-cybozu-private_menu"
                                       class="input-text-outer-cybozu searchbox-keyword-area searchbox_keyword_grn">
                                       <input class="input-text-cybozu prefix-grn" style="width:246px;" type="text"
                                          id="keyword_private_menu" name="" autocomplete="off"
                                          value="User search"
                                          maxlength="">
                                       <button id="searchbox-submit-private_menu" class="searchbox-submit-cybozu" type="button"
                                          title="Search"
                                          aria-label="Search"></button>
                                    </div>
                                    <div class="clear_both_0px"></div>
                                 </div>
                              </div>
                           </div>
                           <!-- category select -->
                           <div class="mBottom7 nowrap-grn">
                              <dl id="p_CID_pulldown" class="selectmenu_grn selectmenu_base_grn">
                                 <dt>
                                    <a href="javascript:void(0)" class="nowrap-grn">
                                    <span>
                                    </span>
                                    <span class="selectlist_selectmenu_item_grn pulldown_head">                                                                                                                                                                                                                                             
                                    </span>
                                    <span class="pulldownbutton_arrow_down_grn mLeft3"></span>
                                    </a>
                                 </dt>
                                 <dd>
                                    <div class="pulldown_menu_grn" style="display: none;">
                                       <ul>
                                          <li ><a href="javascript:void(0)"><span class="vAlignMiddle-grn">Tất cả</span></a></li>
                                          @foreach($department as $key=>$val)
                                          <li data-value="{{$val->id}}" ><a href="javascript:void(0)"><span class="vAlignMiddle-grn">{{$val->name}} </span></a></li>
                                          @endforeach
                                       </ul>
                                    </div>
                                 </dd>
                              </dl>
                              <a class="selectPulldownSub-grn" href="javascript:void(0);"onClick="javascript:popupWin('/schedule/popup_member_select?plugin_session_name=private_menu&amp;plugin_data_name=access_plugin_private_menu&amp;session=start;is_post_message=&amp;selected_tid=&amp;form_name=schedule%2Fadd&amp;select_name=p_sUID&amp;app_id=schedule&amp;return_page=&amp;plid=&amp;system=0&amp;include_org=1&amp;system_display=0&amp;no_multiple=0&amp;send_cgi_parameter=0&amp;multi_apply=0&amp;require_role_tab=1&amp;is_calendar=0&amp;show_group_role=1&amp;require_dynamic_roles=0&amp;require_administrator_role=0','html','1014','675',0,0,1,1,0,1);"title="Select from all organizations"><img src="{!!asset('/img/blankB16.png')!!}" border="0" alt=""></a>
                           </div>
                           <!-- category select -->
                           <div class="selectlist_area_grn">
                              <span id="spinner_selectlist_p_CID" style="display:none;position:absolute;z-index:1;"><img src="{{asset('/img/spinner.gif')}}" border="0" alt=""></span>
                              <div id="selectlist_base_selectlist_p_CID" aria-multiselectable="true" class="selectlist_grn selectlist_r_grn" tabindex="0" style="">
                                 <ul id="ul_selectlist_p_CID">
                                 </ul>
                                 <span id="spinner_scroll_selectlist_p_CID" style="display:none;"><img src="{{asset('/img/spinner.gif')}}" border="0" alt=""></span>
                              </div>
                           </div>
                           <div class="textSub-grn mTop5">
                              <a id="select_all_selectlist_p_CID" class="mRight20" href="javascript:void(0);">Select all</a><a id="un_select_all_selectlist_p_CID" style="display:none" class="mRight20" href="javascript:void(0);">Clear all</a>    <script language="JavaScript" type="text/javascript">
                                 <!--
                                 var popupMember_p_CID_count = 0;
                                 function popupMember_p_CID(Aform)
                                 {
                                     if ( ! popupMember_p_CID_count || document.getElementById('form_CID[]') == null)
                                     {
                                         var p = document.getElementById('div_CID[]');
                                         var c = document.createElement('DIV');
                                         p.style.display = "";
                                         p.appendChild(c);
                                         c.innerHTML = '<iframe id="form_CID[]" name="form_CID[]" frameborder="no" scrolling="no" style="position:absolute; width:0em; height:0em;" src=""></iframe>';
                                     }
                                 
                                     popupWin("","user_list",1034,675,0,0,0,1,0,1);
                                     var url = '/grn/popup_user_list?';
                                     var html = '<!DOCTYPE html><html lang="en"><body><form method="post" target="user_list" action="' + url + '">';
                                     html += '<input type="hidden" name="system_display" value="0">';
                                     var form_frame = document.getElementById('form_CID[]').contentWindow.document;
                                     var instance = grn.component.member_select_list.get_instance("p_CID");
                                     var values = instance.getSelectedUsersValues();
                                     html += '<input type="hidden" name="cid" value="' + values.join(':') + '">';
                                     html += '</form></body></html>';
                                     form_frame.write( html );
                                     form_frame.close();
                                     form_frame.forms[0].submit();
                                     popupMember_p_CID_count = 1;
                                     if( !grn.browser.chrome && !grn.browser.safari && typeof update_back_step == 'function' )
                                     {
                                         update_back_step();
                                     }
                                     return true;
                                 }
                                 //-->
                              </script>
                              <div id="div_CID[]" style="display:none; position:absolute; width:0em; height:0em;">&nbsp;</div>
                              <a href="javascript:void(0);" onClick="popupMember_p_CID();"></a>
                           </div>
                        </td>
                     </tr>
                  </table>
               </td>
            </tr>
            <tr valign="top">
               <th>Facilities</th>
               <td>
                  <script src="{{asset('/js/facility_add.js')}}" type="text/javascript"></script>
                  <script language="JavaScript" type="text/javascript">
                     <!--
                     
                     jQuery(function () {
                         new grn.component.facility_add.FacilityAdd("facility_select", "schedule/repeat_modify", "schedule/repeat_modify", "sITEM", "cITEM",
                                 {
                                     "idSearchBox": "facilities",
                                     "searchUrl": grn.component.url.page('schedule/json/search_facility'),
                                     "categorySelectUrl": grn.component.url.page('schedule/json/accessible_facility'),
                                     "initUsingPurpose": "",
                                     "csrfTicket": "c5fa6d49f53be0b2b9b23a0f404b9df5"
                                     });
                         });
                     //-->
                  </script>
                  <input type="hidden" name="selected_users_sITEM" value="">
                  <table class="table_plain_grn selectlist_base_grn">
                     <tr>
                        <td class="vAlignTop-grn" style="padding-left: 0;">
                           <table class="table_plain_grn">
                              <tr>
                                 <td class="buttonSlectOrder-grn">
                                    <div id="sITEM_order_up" class="mBottom10">
                                       <a class="order_up_grn"
                                          aria-label="Move up"
                                          title="Move up"
                                          href="javascript:void(0)"></a>
                                    </div>
                                    <div id="sITEM_order_down" class="mBottom10">
                                       <a class="order_down_grn"
                                          aria-label="Move down"
                                          title="Move down"
                                          href="javascript:void(0)"></a>
                                    </div>
                                 </td>
                                 <td class="item_select">
                                    <div class="selectlist_base_grn">
                                       <div id="selectlist_base_selectlist_sITEM" aria-multiselectable="true" class="selectlist_grn selectlist_l_grn focus_grn"
                                          style="height:151px;" tabindex="0">
                                          <span id="spinner_selectlist_sITEM"
                                             style="display:none;position:absolute;"><img src="/garoon3/grn/image/cybozu/spinner.gif?20200925.text" border="0" alt=""></span>
                                          <ul id="ul_selectlist_sITEM">
                                             @foreach($schedule->equipment as $key=>$val)
                                             <li id="selectlist_sITEM_member_facility_{{$val->name}}" class="selectlist_sITEM"
                                                data-value="{{$val->name}}" data-approval="0" data-repeat="1" data-ancestors="" data-code="salesevent" data-name="{{$val->name}}">
                                                <span class="selectlist_facility_grn"></span>
                                                <span class="selectlist_text2_grn">{{$val->name}}</span>
                                             </li>
                                             @endforeach
                                          </ul>
                                       </div>
                                    </div>
                                    <div class="textSub-grn mTop5">
                                       <a id="select_all_selectlist_sITEM" class="mRight20"
                                          href="javascript:void(0);">Select all</a>
                                       <a id="un_select_all_selectlist_sITEM" style="display:none" class="mRight20"
                                          href="javascript:void(0);">Clear all</a>
                                    </div>
                                 </td>
                              </tr>
                           </table>
                        </td>
                        <td class="vAlignTop-grn item_right_left">
                           <div class="buttonSelectMove-grn">
                              <div class="mBottom15">
                                 <span class="aButtonStandard-grn">
                                 <a role="button" id="btn_add_cITEM" style="padding-left:0;" href="javascript:void(0);">
                                 <span class="icon-buttonArrowLeft-grn"></span><span class="aButtonText-grn">Add</span>
                                 </a>
                                 </span>
                              </div>
                              <div>
                                 <span class="aButtonStandard-grn">
                                 <a role="button" id="btn_rmv_cITEM" style="padding-right:0;" href="javascript:void(0);">
                                 <span class="aButtonText-grn">Delete</span><span class="icon-buttonArrowRightBehind-grn"></span>
                                 </a>
                                 </span>
                              </div>
                           </div>
                        </td>
                        <td class="vAlignTop-grn">
                           <div class="mTop3 mBottom7 clearFix-schedule">
                              <div class="search_navi">
                                 <div class="searchbox-grn">
                                    <div id="searchbox-cybozu-facilities"
                                       class="input-text-outer-cybozu searchbox-keyword-area searchbox_keyword_grn">
                                       <input class="input-text-cybozu prefix-grn" style="width:246px;" type="text"
                                          id="keyword_facilities" name="" autocomplete="off"
                                          value="Facility search"
                                          maxlength="">
                                       <button id="searchbox-submit-facilities" class="searchbox-submit-cybozu" type="button"
                                          title="Search"
                                          aria-label="Search"></button>
                                    </div>
                                    <div class="clear_both_0px"></div>
                                 </div>
                              </div>
                           </div>
                           <div class="clear_both_0px"></div>
                           <div class="mBottom7">
                              <link href="/garoon3/grn/html/fag_tree_26.css?20200925.text" rel="stylesheet" type="text/css">
                              <dl id="group-select" class="selectmenu_grn selectmenu_base_grn">
                                 <dt><a id="facility-button" href="javascript:void(0)" class="nowrap-grn"><span></span><span id="dropdown_title" class="selectlist_selectmenu_item_grn pulldown_head">(All facilities)</span><span id="dropdown_arrow_image" class="pulldownbutton_arrow_down_grn mLeft3"></span><input type="hidden" name="pulldown_head" value="0"></a></dt>
                              </dl>
                              <div id="user-popup" class="wrap_dropdown_option"></div>
                              <div id="facility-popup" class="wrap_dropdown_option"></div>
                              <div id="dummy-popup" class="wrap_dropdown_option"></div>
                              <div></div>
                              <div id="facility-popup-dummy_root" style="border:1px solid #000000; top:-10000px; left:-10000px; position:absolute; background-color:#FFFFFF; overflow:scroll; width:1px;height:1px;">
                                 <div id="facility-popup-dummy_tree_wrap_tree1" class="wrap_tree1">
                                    <div id="facility-popup-dummy_tree_wrap_tree2" class="wrap_tree2">
                                       <div id="facility-popup-dummy_tree"></div>
                                    </div>
                                 </div>
                              </div>
                              <script type="text/javascript">
                                 (function () {
                                 
                                     var group_select_id    = 'group-select';
                                     var title_id           = 'dropdown_title';
                                     var user_button_id     = 'user-button';
                                     var facility_button_id = 'facility-button';
                                     var user_popup_id      = 'user-popup';
                                     var facility_popup_id  = 'facility-popup';
                                 
                                     var dropdown = new GRN_DropdownMenu(
                                         group_select_id, title_id, user_button_id, facility_button_id,
                                         GRN_DropdownMenu.prototype.PreferFacilityGroup,
                                         user_popup_id, facility_popup_id,
                                         clickOrganizationCallback, clickFacilityGroupCallback,
                                         "/garoon3/grn/html/space.html?20200925.text" );
                                 
                                     function updateTitle( title ) {
                                         var group_select = jQuery("#" + group_select_id);
                                         var old_width = parseInt(group_select.outerWidth());
                                         group_select.css("width", "");
                                         jQuery("#" + title_id).html( title );
                                         if( old_width > parseInt(group_select.outerWidth()) ) {
                                             group_select.css("width", old_width + "px");
                                         }
                                     }
                                 
                                     function clickOrganizationCallback( group_item ) {
                                         return function(){
                                             updateTitle( group_item.name );
                                             dropdown.organization.hide();
                                             location.href = "/index?"+ '&bdate=' + document.forms["schedule/repeat_modify"].bdate.value + '&gid='+group_item.gid;
                                         }
                                     }
                                 
                                     function clickFacilityGroupCallback( node ) {
                                         node.tree.fagTree.removeSpecialNode( 's' );
                                         if( node.extra_param ) { //よく使う施設グループ or 最近選択した施設グループ
                                             updateTitle( node.label );
                                         }
                                         else {
                                             if( node.oid == 'f' ) {
                                                 updateTitle( '(All facilities)' );
                                             }else{
                                                 updateTitle(  node.label );
                                             }
                                         }
                                         dropdown.facility.hide();
                                 
                                         var oid = node.oid;
                                         if( oid.substr(0, 1) == 'x' ) {
                                             oid = oid.substr( 1 );
                                         }
                                         grn.base.namespace("grn.component.facility_add");
                                         if (typeof grn.component.facility_add.get_instance === "function") {
                                             var facility_select = grn.component.facility_add.get_instance("facility_select");
                                             if (facility_select) {
                                                 var facility_group_id = oid ? oid : 0;
                                                 facility_select.changeCategory(facility_group_id);
                                                 return;
                                             }
                                         }
                                         ChangeITEM( "schedule/repeat_modify", {'fagid': oid } );
                                     }
                                 
                                     dropdown.initializeOrganization(
                                         new Array(
                                             
                                                  ) );
                                 
                                     var icon_size = 37;
                                     var min_width = 200;
                                 
                                     var group_select = jQuery("#" + group_select_id);
                                     var current_width = group_select.outerWidth();
                                 
                                     if ( current_width  < min_width ){
                                         group_select.css( "width", (min_width + icon_size) + "px" );
                                     }
                                 
                                     var title_width  = group_select.outerWidth();
                                 
                                     dropdown.initializeFacilityGroup( { 'page_name': "schedule/repeat_modify",
                                                                         'ajax_path':'/schedule/json/accessible_facility_tree?',
                                                                         'csrf_ticket':'c5fa6d49f53be0b2b9b23a0f404b9df5',
                                                                         'callback':clickFacilityGroupCallback,
                                                                         'selectedOID':"x",
                                                                         'title_width': title_width,
                                                                         'node_info':
                                                                         [{"oid":"x1", "name":"本社 (Recently selected facility group)", "is_selected":"0", "extra_param":"2", "count":"0", "expanded":false, "children":[]},{"oid":"x3", "name":"overseas branches (Recently selected facility group)", "is_selected":"0", "extra_param":"2", "count":"0", "expanded":false, "children":[]},{"oid":"xr", "name":"(Recently selected facility)", "is_selected":"0", "extra_param":"1", "count":"0", "expanded":false, "children":[]},{"oid":"x", "name":"(All facilities)", "is_selected":"0", "extra_param":"1", "count":"0", "expanded":false, "children":[]},{"oid":"1", "name":"本社", "is_selected":"0", "extra_param":"", "count":"8", "expanded":false, "children":[]},{"oid":"9", "name":"東京支店", "is_selected":"0", "extra_param":"", "count":"0", "expanded":false, "children":[]},{"oid":"10", "name":"大阪支店", "is_selected":"0", "extra_param":"", "count":"0", "expanded":false, "children":[]},{"oid":"11", "name":"福岡支店", "is_selected":"0", "extra_param":"", "count":"0", "expanded":false, "children":[]},{"oid":"4", "name":"Video Conferencing System", "is_selected":"0", "extra_param":"", "count":"0", "expanded":false, "children":[]}]
                                                                       });
                                 
                                     YAHOO.namespace("global");
                                     YAHOO.global.dropdown = dropdown;
                                     YAHOO.global.updateTitle = updateTitle;
                                 }());
                                 
                              </script>            
                           </div>
                           <div class="mBottom5">
                              <div class="selectlist_base_grn">
                                 <div id="selectlist_base_selectlist_cITEM" aria-multiselectable="true" class="selectlist_grn selectlist_r_grn"
                                    style="height:90px;" tabindex="0">
                                    <span id="spinner_selectlist_cITEM"
                                       style="display:none;position:absolute;"><img src="{{asset('/img/spinner.gif')}}" border="0" alt=""></span>
                                    <ul id="ul_selectlist_cITEM">
                                       {!!$equipment_html!!}
                                    </ul>
                                 </div>
                              </div>
                              <div class="textSub-grn mTop5">
                                 <a id="select_all_selectlist_cITEM" class="mRight20"
                                    href="javascript:void(0);">Select all</a>
                                 <a id="un_select_all_selectlist_cITEM" style="display:none" class="mRight20"
                                    href="javascript:void(0);">Clear all</a>
                                 <script language="JavaScript" type="text/javascript">
                                    <!--
                                    var popupFacilityList_count = 0;
                                    function popupFacilityList(Aform)
                                    {
                                     if ( ! popupFacilityList_count)
                                     {
                                    
                                       var p = document.getElementById('div_cITEM');
                                       var c = document.createElement('DIV');
                                       p.style.display = "";
                                       p.appendChild(c);
                                       c.innerHTML = '<iframe id="form_cITEM" name="form_cITEM" frameborder="no" scrolling="no" style="position:absolute; width:0em; height:0em;" src=""></iframe>';
                                    
                                     }
                                    
                                        popupWin("","facility_list",1034,675,0,0,0,1,0,1);
                                    
                                     var form_frame = window.frames['form_cITEM'].document;
                                     var url = '/schedule/popup_facility_list?';
                                     var html = '<!DOCTYPE html><html><body lang="{$html_tag_lang|escape}"><form method="post" target="facility_list" action="' + url + '">';
                                     html += '<input type="hidden" name="system_display" value="0">';
                                    
                                    
                                        var instance = grn.component.member_select_list.get_instance("cITEM");
                                        var values = instance.getSelectedUsersValues();
                                        html += jQuery.map(values, function(value, count){
                                            if ( value.length > 0 && isFinite( value ) ) {
                                                return '<input type="hidden" name="cid[' + (count) + ']" value="' + value + '">'
                                            }else{
                                                return '';
                                            }
                                        }).join();
                                    
                                    
                                     html += '</form></body></html>';
                                     form_frame.write( html );
                                     form_frame.close();
                                     form_frame.forms[0].submit();
                                     popupfacilityList_count = 1;
                                     if( !grn.browser.chrome && !grn.browser.safari && typeof update_back_step == 'function' )
                                     {
                                        update_back_step();
                                     }
                                     return true;
                                    }
                                    //-->
                                 </script>
                                 <div id="div_cITEM" style="display:none; position:absolute; width:0; height:0;">&nbsp;</div>
                              </div>
                           </div>
                        </td>
                     </tr>
                  </table>
                  <!--extended_items-->
                  <div id="extended_items" name="extended_items" style="margin-left:20px;">
                     <table></table>
                  </div>
                  <!--extended_items_end-->
               </td>
            </tr>
            <tr>
               <th>Availability</th>
               <td>
                  <script language="JavaScript" type="text/javascript">
                     <!--
                     function appendSingleSelection(form,name,src_html)
                     {
                       var html = src_html;
                       var box = form.elements[name];
                       if ( ! box ) return html;
                       var box_value = box.options[box.selectedIndex].value;
                       
                       if ( box_value.length < 1 ) return html;
                       html += '<input type="hidden" name="' + name + '" value="' + box_value + '">';
                       return html;
                     }
                     function appendMultipleSelection(form,name,src_html)
                     {
                       var html = src_html;
                       var box = form.elements[name + '[]'];
                       if ( ! box ) return html;
                       var box_options = box.options;
                       var count = 0;
                     
                       for ( i = 0; i < box_options.length; i++ )
                       {
                         var box_option = box_options[i];
                         if (name != 'sITEM' && name != 'sUID' && ! box_option.selected ) continue;
                         if ( box_option.value.length < 1 ) continue;
                         var box_option_value = box_option.value.split(':');
                         html += '<input type="hidden" name="' + name + '[' + (count++) + ']" value="' + box_option_value[0] + '">';
                       }
                       return html;
                     }
                     var openConfirm_count = 0;
                     function openConfirm(form, pagename)
                     {
                     
                      if ( ! openConfirm_count)
                      {
                     
                        var p = document.getElementById('form_frame');
                        var c = document.createElement('DIV');
                        p.style.display = "";
                        p.appendChild(c);
                        c.innerHTML = '<iframe id="form_frame" name="form_frame" frameborder="no" scrolling="no" style="position:absolute; width:0em; height:0em;" src=""></iframe>';
                     
                      }
                     
                       var popupWindow   = popupWin_returnWin("","popup",1034,675,0,0,0,1,0,1);
                       window.addEventListener('unload', function(){if(popupWindow && !popupWindow.closed){popupWindow.close();}});
                     
                       var form_frame = window.frames['form_frame'].document;
                       var url = '/schedule/confirm?';
                       var member_add = grn.component.member_add.get_instance('member_select');
                       var html = '<!DOCTYPE html><html lang="{$html_tag_lang|escape}"><body><form method="POST" target="popup" action="' + url + '">';
                       html += '<input type="hidden" name="session_no_use" value="1">';
                       ml += '<input type="hidden" name="_token" value="{{ csrf_token() }}" />';
                     
                       //single select
                       html = appendSingleSelection(form,'start_year',html);
                       html = appendSingleSelection(form,'start_month',html);
                       html = appendSingleSelection(form,'start_day',html);
                       html = appendSingleSelection(form,'start_hour',html);
                       html = appendSingleSelection(form,'start_minute',html);
                       html = appendSingleSelection(form,'end_year',html);
                       html = appendSingleSelection(form,'end_month',html);
                       html = appendSingleSelection(form,'end_day',html);
                       html = appendSingleSelection(form,'end_hour',html);
                       html = appendSingleSelection(form,'end_minute',html);
                       //multiple select
                       html += member_add.getFirstMemberSelectList().makeHTMLTextForConfirmView(false);
                       html += member_add.candidateList.makeHTMLTextForConfirmView(true);
                       if (grn.component.facility_add) {
                         var facility_add = grn.component.facility_add.get_instance('facility_select');
                         html += facility_add.facilityList.makeHTMLTextForConfirmView(false);
                         html += facility_add.candidateList.makeHTMLTextForConfirmView(true);
                       }
                     
                     
                       if ( form.elements['uid'] )
                       {
                         html += '<input type="hidden" name="uid" value="' + form.elements['uid'].value + '">';
                       }
                       if ( form.elements['gid'] )
                       {
                         html += '<input type="hidden" name="gid" value="' + encodeURIComponent(form.elements['gid'].value) + '">';
                       }
                       if ( form.elements['bdate'] )
                       {
                         html += '<input type="hidden" name="bdate" value="' + form.elements['bdate'].value + '">';
                       }
                       html += '<input type="hidden" name="parent_page_name" value="' + pagename + '">';
                       html += '<input type="hidden" name="timezone" value="' + form.elements['timezone'].value + '">';
                       html += '<input type="hidden" name="end_timezone" value="' + form.elements['end_timezone'].value + '">';
                     
                       html += '</form></body></html>';
                       form_frame.write( html );
                       form_frame.close();
                       form_frame.forms[0].submit();
                       openConfirm_count = 1;
                       if( !grn.browser.chrome && !grn.browser.safari && typeof update_back_step == 'function' )
                       {
                           update_back_step();
                       }
                       return false;
                     }
                     //-->
                  </script>
                  <div id="form_frame" style="display:none; position:absolute; width:0em; height:0em;">&nbsp;</div>
                  <script language="JavaScript" type="text/javascript">
                     var form_name = document.getElementById('schedule/repeat_modify');
                     
                  </script>
                  <span class="aButtonHighlight-grn">
                  <a href="javascript:void(0);" onClick="javascript:openConfirm(form_name, 'repeat_modify');">
                  <span class="icon-blankB-grn"></span>
                  <span class="aButtonText-grn">Check availability of attendees and facilities</span>
                  </a>
                  </span>   
               </td>
            </tr>
            <tr>
               <th>Visibility</th>
               <td>
                  <span class="radiobutton_base_grn">
                  <input type="radio" name="private" id="1" value="0" onclick="display_off('private_select')" checked >
                  <label for="1" onMouseOver="this.style.color='#ff0000'" onMouseOut="this.style.color=''">Public</label>
                  </span>
                  <span class="radiobutton_base_grn">
                  <input type="radio" name="private" id="2" value="1" onclick="display_off('private_select')" >
                  <label for="2" onMouseOver="this.style.color='#ff0000'" onMouseOut="this.style.color=''">Private</label>
                  </span>
               </td>
            </tr>
            <tr valign="top">
               <th>Company information</th>
               <td>
                  <a href="javascript:display_on_off('address_form')">Add company information▼</a>
                   <table class="address" id="address_form" style="display:none;">
                     <colgroup>
                        <col width="5%">
                        <col width="95%">
                     </colgroup>
                     <tr valign="top">
                        <th nowrap>Company name</th>
                        <td><input  type="text" name="company_name" size="52" maxlength="100" value="{{$schedule->company_name}}" onKeyPress="return event.keyCode != 13;"></td>
                     </tr>
                     <tr valign="top">
                        <th nowrap>Address</th>
                        <td><input  type="text" name="physical_address" size="52" maxlength="65535" value="{{$schedule->physical_address}}" onKeyPress="return event.keyCode != 13;"></td>
                     </tr>
                     <tr valign="top">
                        <th nowrap>Company phone number</th>
                        <td><input  type="text" name="company_telephone_number" size="50" maxlength="100" value="{{$schedule->company_telephone_number}}" onKeyPress="return event.keyCode != 13;"></td>
                     </tr>
                  </table>
               </td>
            </tr>
            <tr valign="top">
               <th>Notes</th>
               <td><textarea id="textarea_id" name="memo" class="autoexpand" wrap="virtual" role="form" cols="65" rows="5" style="white-space: pre-wrap; min-height: 95px; height: 121px;border: 1px solid rgb(153, 153, 153)">{{$schedule->memo}}</textarea></td>
            </tr>
            <tr>
               <td></td>
               <td>
                  <script text="text/javascript">
                     // namespace
                     grn.base.namespace("grn.component.error_handler");
                     
                     (function () {
                         var G = grn.component.error_handler;
                     
                         G.show = function (request, my_callback) {
                             var s = request.responseText;
                             if (s != undefined) {
                                 var title = '';
                                 var msg = '';
                                 var json = null;
                     
                                 try {
                                     json = JSON.parse(s);
                                 } catch(e){}
                     
                                 if (json) {
                                     var show_backtrace = "";
                                     title = '';//json.code;
                     
                                     if (show_backtrace) {
                                         msg = msg + "<div style='height:145px; overflow: auto;'><table><tr><td>";
                                     }
                                     msg = msg + "<div><img border='0' src='/garoon3/grn/image/cybozu/warn100x60.gif?20200925.text'></div>";
                                     msg = msg + "<div class='bold'>" + 'Error (' + json.code + ")</div><div>" + json.diagnosis + "</div><br>";
                                     msg = msg + "<div class='bold'>" + 'Cause' + "</div><div>" + json.cause + "</div><br>";
                                     msg = msg + "<div class='bold'>" + 'Countermeasure' + "</div><div>" + json.counter_measure + "</div>";
                     
                                     if (show_backtrace) {
                                         msg = msg + "<br><hr>";
                                         msg = msg + "<div class='bold'>( Beta/Debug only. by common.ini )</div><br>";
                                         msg = msg + "<div class='bold'>Developer Info</div><br>";
                                         if (json.developer_info) {
                                             msg = msg + json.developer_info;
                                         }
                                         msg = msg + "<div class='bold'>Backtrace</div><br>";
                                         msg = msg + "How to read backtraces(to be written) / How to read backtraces (to be written)<br>";
                                         msg = msg + "<pre style='border:1px solid #6666ff; background:#eeeeff; padding:10px;'>";
                                         if (json.backtrace) {
                                             msg = msg + json.backtrace;
                                         }
                                         msg = msg + "</pre>";
                                         msg = msg + "<br>$G_INPUT<br>";
                                         msg = msg + "<pre style='border:1px solid #6666ff; background:#eeeeff; padding:10px;'>";
                                         if (json.input) {
                                             msg = msg + json.input;
                                         }
                                         msg = msg + "</pre>";
                                         msg = msg + "</td></tr></table></div>";
                                     }
                                 }
                                 else {
                                     title = 'Error';
                                     msg = grn_split_tags(s, 1000);
                                 }
                             }
                             else {
                                 title = "Error";
                                 msg = "Connection failed.";
                             }
                     
                             GRN_MsgBox.show(msg, title, GRN_MsgBoxButtons.ok, {
                                 ui: [],
                                 caption: {
                                     ok: 'OK'
                                 },
                     
                                 callback: function (result, form) {
                                     GRN_MsgBox._remove();
                                     if (typeof my_callback != 'undefined') my_callback();
                                 }
                             });
                         };
                         G.getHeader = function (request) {
                             if (typeof(request.getAllResponseHeaders) == 'function') {
                                 return request.getAllResponseHeaders();
                             }
                             else {
                                 return request.getAllResponseHeaders; // for YAHOO.util.Connect.asyncRequest
                             }
                         };
                         G.hasCybozuError = function (request) {
                             var headers = G.getHeader(request);
                             return (headers && headers.match(new RegExp(/X-Cybozu-Error/i))) ? true : false;
                         };
                         G.hasCybozuLogin = function (request) {
                             var headers = G.getHeader(request);
                             return (headers.match(new RegExp(/X-CybozuLogin/i)) || !headers.match(new RegExp(/X-Cybozu-User/i)));
                         };
                     })();
                     
                  </script>
                  <script src="{{asset('/js/add.js')}}" type="text/javascript"></script>
                  <script language="JavaScript" type="text/javascript">
                     <!--
                     var check_add = false;
                     var browser_type = "chrome";
                     var browser_ver_major = "87";
                     var allow_file_attachment = false;
                     var is_ios = false;
                     var is_android = false;
                     var handle_type = 'modify';
                     function _submitNormal(f,ajax_page)
                     {
                         url_redirect = '/schedule/view?';
                         grn.component.button("#schedule_submit_button").showSpinner();
                         grn.component.button("#schedule_submit_button_top").showSpinner();
                     
                         var request = new grn.component.ajax.request({
                                     url: ajax_page,
                                     dataType: "json",
                                     data: jQuery(f).serialize(),
                                     method: "post"
                                 }
                         );
                     
                         request.on('beforeShowError', function (event, jqXHR) {
                             if (typeof jqXHR.responseJSON !== "undefined") {
                                 var json_obj = jqXHR.responseJSON;
                                 if (typeof json_obj.validation !== "undefined" && JSON.parse(json_obj.validation) == false) {
                                     event.preventDefault();
                                     jQuery('#show_error').css({display: ''});
                                     window.scrollTo(0, 0);
                                 }
                             }
                         });
                     
                         request.send()
                                 .done(function (data, textStatus, jqXHR) {
                                     // remove ajax flag element
                                     removeAjaxElement(f);
                                     // remove refresh status dialog flag element
                                     removeShowRefreshDialogFlag(f);
                     
                                     var json_obj = grn_parseJson(jqXHR.responseText);
                                     if (json_obj.conflict_facility == 1) {
                                         showYN(jqXHR, function (except_date) {
                                             //my_callback
                                             (function ($) {
                     
                                                 jQuery("#hfExcept").val(except_date);
                                                 check_add = false;
                                                 if (grn.base.isNamespaceDefined("grn.page.schedule.add")) {
                                                     grn.page.schedule.add.schedule_submit(form_name, ajax_page);
                                                 }
                                             })(jQuery);
                                         });
                                     } else {
                                         
                                         var link = json_obj.link;
                                         window.location = link;
                                         
                                     }
                                 })
                                 .fail(function () {
                                     // remove ajax flag element
                                     removeAjaxElement(f);
                                     // remove refresh status dialog flag element
                                     removeShowRefreshDialogFlag(f);
                     
                                     grn.component.button("#schedule_submit_button").hideSpinner();
                                     grn.component.button("#schedule_submit_button_top").hideSpinner();
                                     check_add = false;
                                 });
                     }
                     
                     function _submitUpload(f,ajax_page)
                     {
                         url_redirect = '/schedule/view?';
                         grn.component.button("#schedule_submit_button").showSpinner();
                         grn.component.button("#schedule_submit_button_top").showSpinner();
                         var request = new grn.component.ajax.request({
                             url: ajax_page,
                             dataType: "json",
                             data: jQuery(f).serialize(),
                             method: "post"
                         });
                     
                         request.send()
                                 .done( function(data, textStatus, jqXHR){
                                 removeAjaxElement(f);
                                 // remove refresh status dialog flag element
                                 removeShowRefreshDialogFlag(f);
                                 if(typeof grn_parseJson(jqXHR.responseText) === "object")
                                 {
                                     var json_obj = grn_parseJson(jqXHR.responseText);
                                     if(json_obj.code)
                                     {
                                         grn.component.button("#schedule_submit_button").hideSpinner();
                                         grn.component.button("#schedule_submit_button_top").hideSpinner();
                                         if(!json_obj.validation)
                                         {
                                             grn.component.error_handler.show(jqXHR);
                                         }
                                         else
                                         {
                                             jQuery('#show_error').css({display:''});
                                             window.scrollTo(0,0);
                                         }
                                         check_add = false;
                                     }
                                     else
                                     {
                                         if (json_obj.conflict_facility == 1) {
                                             showYN(jqXHR, function (except_date) {
                                                 //my_callback
                                                 (function ($) {
                     
                                                     jQuery("#hfExcept").val(except_date);
                                                     check_add = false;
                                                     if (grn.base.isNamespaceDefined("grn.page.schedule.add")) {
                                                         grn.page.schedule.add.schedule_submit(form_name, ajax_page);
                                                     }
                                                 })(jQuery);
                                             });
                                         } else {
                                             
                                             var link = json_obj.link;
                                             window.location = link;
                                             
                                         }
                                     }
                                 }
                                 else
                                 {
                                     setAjaxElement(f);
                                     setShowRefreshDialogFlag(f);
                                     _submitNormal(f,ajax_page);
                                 }
                             })
                             .fail(function() {
                                 // remove ajax flag element
                                 removeAjaxElement(f);
                                 // remove refresh status dialog flag element
                                 removeShowRefreshDialogFlag(f);
                     
                                 grn.component.button("#schedule_submit_button").hideSpinner();
                                 grn.component.button("#schedule_submit_button_top").hideSpinner();
                                 check_add = false;
                             });
                     }
                     
                     /**
                      * This function is called only from schedule/add.js.
                      *
                      * @param {HTMLElement} f
                      * @param {string} ajax_page
                      *
                      */
                     function _submit(f,ajax_page)
                     {
                         if (((browser_type == 'msie' && browser_ver_major < 10) || is_ios || is_android) && allow_file_attachment)
                         {
                             _submitUpload(f,ajax_page);
                         }
                         else
                         {
                             _submitNormal(f,ajax_page);
                         }
                     }
                     
                     function showYN(request, my_callback) {
                         var s = request.responseText;
                         if (typeof s != 'undefined') {
                             var ob_json = grn_parseJson(s);
                             var title;
                             var msg;
                             var html_rows = "";
                             var html_row_header = "";
                             var rows_length = 0;
                             var rows_limit = 0;
                             var html_row_more = "";
                             var is_show_more_text = false;
                             var event_except = "";
                             (function ($) {
                                 var events = ob_json.conflict_events;
                                 if (events != null && events.length > 0) {
                                     rows_length = events.length;
                                     rows_limit = events.length;
                                     if (events.length > 5) {//Limit 5 events are displayed.
                                         rows_limit = 5;
                                         is_show_more_text = true;
                                     }
                     
                                     html_row_header = "<tr><th class='nowrap-grn' style='width:30px;'>Date</th><th class='nowrap-grn'>Conflicting facility </th></tr>";
                                     for (var i = 0; i < rows_limit; i++) {
                                         html_rows += "<tr><td class='nowrap-grn'><span class='icon_list_style_grn icon_inline_grn'></span><span>" + events[i].setdatetime + "</span></td><td><span>" + events[i].col_facility + "</span></td></tr>";
                                         event_except += ";" + events[i].col_setdatetime;
                                     }
                                     for (var j = rows_limit; j < events.length; j++) {
                                         event_except += ";" + events[j].col_setdatetime;
                                     }
                                 }
                             })(jQuery);
                     
                             if (is_show_more_text == true) {
                                 html_row_more = "<div class='mTop7'>...&nbsp;" +
                                 "" +
                                 (rows_length - rows_limit) +
                                 "&nbsp;more appointments with conflicting facilities exist.</div>";
                             }
                             msg = "<div class='mBottom10'><span class='icon_attention_grn attentionMessage-grn bold_grn'>One or more facilities are conflicting in the following appointments:</span></div>";
                             msg += "<table class='table_grn table_list_1_grn'>" + html_row_header + html_rows + "</table>" + html_row_more;
                             msg += "<div class='border-partition-common-dot-grn'></div>";
                             if (ob_json.conflict_all == 1) {// all event are conflict
                                 msg += "<div class='bold_grn mTop15'>All appointments have conflicting facilities. No appointment can be added.</div>";
                             }
                             else {
                                 if (handle_type == '' || handle_type == 'add') // event is added
                                 {
                                     msg += "<div class='bold_grn mTop15'>Do you want to add only appointments with no conflicting facilities?</div>";
                                 }
                                 else    // modify
                                 {
                                     msg += "<div class='bold_grn mTop15'>Do you want to edit only appointments with no conflicting facilities?<br />Appointments with conflicting facilities are deleted, not edited.</div>";
                                 }
                             }
                     
                             var msgboxButtonType = (ob_json.conflict_all) ? GRN_MsgBoxButtons.ok : GRN_MsgBoxButtons.yesno;
                     
                             GRN_MsgBox.show(msg, title, msgboxButtonType, {
                                 ui: [],
                                 caption: {
                                     yes: 'Yes',
                                     no: 'No',
                                     ok: 'Back'
                                 },
                                 main_button: {ui: grn.component.button.UI.MAIN, autoDisable: true},
                                 callback: function (result, form) {
                                     check_add = false;
                                     grn.component.button("#schedule_submit_button").hideSpinner();
                                     grn.component.button("#schedule_submit_button_top").hideSpinner();
                                     if (result == GRN_MsgBoxResult.yes && typeof my_callback != 'undefined') {
                                         my_callback(event_except);
                                     } else {
                                         jQuery("#hfExcept").empty();
                                         if (result == GRN_MsgBoxResult.ok) {
                                             GRN_MsgBox.close();
                                         }
                                     }
                                 }
                             });
                         }
                     }
                     
                     function toggle(id)
                     {
                         var loading = document.getElementById(id + '_loading');
                         var button = document.getElementById(id + '_button');
                         
                         if (!loading)
                         {
                             loading.innerHTML = '';
                             button.disabled = false;
                             isLoading = false;
                         }
                         else
                         {
                             loading.innerHTML = '<img src="' + G.spinnerImage + '" />';
                             button.disabled = true;
                             isLoading = true;
                         }
                     }
                     
                     function setAjaxElement(f)
                     {
                         if( document.getElementById('use_ajax') ) return false;
                     
                         var element = document.createElement("input");
                         element.setAttribute("type", "hidden");
                         element.setAttribute("id", "use_ajax");
                         element.setAttribute("name", "use_ajax");
                         element.setAttribute("value", "1");
                         f.appendChild(element);
                         return true;
                     }
                     
                     function removeAjaxElement(f)
                     {
                         var use_ajax = document.getElementById('use_ajax');
                         if(use_ajax)
                         {
                             f.removeChild(use_ajax);
                         }
                     }
                     
                     function setShowRefreshDialogFlag(f)
                     {
                         if (jQuery("#attendance_check_dialog").is(":visible"))
                         {
                             if( document.getElementById('show_refresh_status_dialog') ) return;
                     
                             var element = document.createElement("input");
                             element.setAttribute("type", "hidden");
                             element.setAttribute("id", "show_refresh_status_dialog");
                             element.setAttribute("name", "show_refresh_status_dialog");
                             element.setAttribute("value", "1");
                             f.appendChild(element);
                         }
                     }
                     
                     function removeShowRefreshDialogFlag(f)
                     {
                         var show_refresh_status_dialog = document.getElementById('show_refresh_status_dialog');
                         if(show_refresh_status_dialog)
                         {
                             f.removeChild(show_refresh_status_dialog);
                         }
                     }
                     //-->
                  </script>
                  <div class="mTop10 buttonArea-grn">
                     <span id="schedule_submit_button" class="button_grn_js button1_main_grn  button1_r_margin2_grn" onclick="if (!grn.component.button(this).isDisabled()) grn.page.schedule.add.schedule_submit('schedule/repeat_modify', grn.component.url.page('schedule/update/{!!$schedule->id!!}'));"  ><a href="javascript:void(0);" role="button">Save</a></span><span id="schedule_button_cancel" class="button_grn_js button1_normal_grn" onclick="grn.page.schedule.add.schedule_cancel('{{route('frontend.schedule.view',$schedule->id)}}');"  ><a href="javascript:void(0);" role="button">Cancel</a></span>
                  </div>
               </td>
            </tr>
         </table><input type="hidden" name="_token" value="{{ csrf_token() }}" />
         <input type="hidden" name='event' value="{!!$schedule->event!!}">
         <input type="hidden" name='bdate' value="{!!$schedule->bdate!!}">
         <input type="hidden" name="uid" value="{!!$schedule->uid!!}">
         <input type="hidden" name="pattern" value="3">
      </form>
      <script language="JavaScript">
         <!--
             window.onload = function(){typeChange("schedule/repeat_modify","apply","1");}
         //-->
      </script>
      <script type="text/javascript" src="{{asset('/js/schedule_span.js')}}"></script>
      <script type="text/javascript" src="{{asset('/js/schedule_validate_date.js')}}"></script>
      <script type="text/javascript" src="{{asset('/js/timezone_info.js')}}"></script>
   </div>
</div>
@stop
@section('script')
@parent
<script src="{{asset('/js/dist/schedule.js')}}" type="text/javascript"></script>
@stop