@extends('frontend.layouts.create_schedule')
@section('content')
<table class="global_navi_title" cellpadding="0" cellmargin="0" style="padding:0px;">
   <tbody>
      <tr>
         <td width="100%" valign="bottom" nowrap="">
            <div class="global_naviAppTitle-grn">
               <img src="{!!asset('/img/schedule20.gif')!!}" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Scheduler
               </font></font>
            </div>
            <div class="global_navi-viewChange-grn">
               <ul>
                    <li role="heading" aria-level="2" class="global_naviBack-viewChangeSelect-grn"><span>Group day</span></li>
                    <li><a class="global_naviBackTab-viewChange-grn" href="{{route('frontend.schedule.index')}}">Group week</a></li>
                    <li><a class="global_naviBackTab-viewChange-grn viewChangeLeft-grn" href="{{route('frontend.schedule.personal_day')}}">Day</a></li>
                    <li><a class="global_naviBackTab-viewChange-grn" href="{{route('frontend.schedule.personal_week')}}">Week</a></li>
                    <li><a class="global_naviBackTab-viewChange-grn" href="{{route('frontend.schedule.personal_month')}}">Month</a></li>
                    <li><a class="global_naviBackTab-viewChange-grn viewChangeRight-grn" href="{{route('frontend.schedule.personal_year')}}">Year</a></li>
               </ul>
            </div>
         </td>
         <td align="right" valign="bottom" nowrap="">
         </td>
      </tr>
   </tbody>
</table>
<div class="mainarea">
   <div class="mainareaSchedule-grn" id="mainareaSchedule-grn">
      <script src="{!!asset('/js/schedule.js')!!}" type="text/javascript"></script>
      <!--menubar-->
      <div id="menu_part">
         <div id="smart_main_menu_part">
            <span class="menu_item">
            <a href="{{route('frontend.schedule.create')}}">
            <img src="{!!asset('/img/write20.png')!!}" border="0" alt="">New</a>
            </span>
            
            &nbsp;
         </div>
         <div id="smart_rare_menu_part" style="white-space:nowrap;">
            <script language="JavaScript" type="text/javascript">
               <!--
               function search_submit(search_target)
               {
                   var f = document.forms["search"];
                   f.target = '_self';
                   if(search_target == 'user')
                   {
                       var gid = document.createElement( "input" );
                       gid.type = "hidden";
                       gid.name = "gid";
                       gid.value = "search";
                       f.appendChild(gid);
                       f.action = "/schedule/group_day?";
                   }
                   else
                   {
                       var uid = document.createElement( "input" );
                       uid.type = "hidden";
                       uid.name = "uid";
                       uid.value = "";
                       f.appendChild(uid);
               
                       var referer_key = document.createElement( "input" );
                       referer_key.type = "hidden";
                       referer_key.name = "referer_key";
                       referer_key.value = "494fa9a97cc8f3a224b8d0bc54522924";
                       f.appendChild(referer_key);
               
                       var referer_bdate_value = __get_referer_bdate();
                       if ( referer_bdate_value.length > 0 )
                       {
                           var referer_bdate = document.createElement( "input" );
                           referer_bdate.type = "hidden";
                           referer_bdate.name = "referer_bdate";
                           referer_bdate.value = referer_bdate_value;
                           f.appendChild(referer_bdate);
                       }
                       f.action = "/schedule/search?";
                   }
                   f.submit();
               }
               
               var user_search = "Users & Facilities";
               var schedule_search = "Appointments";
               var type_search = 'schedule';
               
               function change_type_search()
               {
                   document.getElementById('type_search').value=type_search;
                   if(type_search == 'user')
                   {
                       type_search = 'schedule';
                       document.getElementById('type_search_schedule').innerHTML = user_search + '<span class="schedule-ButtonTypeArrowDown-grn"></span>';
                       document.getElementById('choose_type_search').innerHTML = schedule_search;
                   }
                   else
                   {
                       type_search = 'user';
                       document.getElementById('type_search_schedule').innerHTML = schedule_search + '<span class="schedule-ButtonTypeArrowDown-grn"></span>';
                       document.getElementById('choose_type_search').innerHTML = user_search;
                   }
               }
               -->
            </script>
            <div class="search_navi">
               <form name="search" action="/schedule/group_day?" onsubmit="search_submit(document.getElementById('type_search').value);">
                  <input id="type_search" type="hidden" name="type_search" value="user">
                  <div class="searchboxChangeTarget-grn">
                     <div class="searchbox-grn">
                        <div id="searchbox-schedule" class="input-text-outer-cybozu">
                           <input class="input-text-cybozu" type="text" name="search_text" autocomplete="off" value="" placeholder="Search">
                           <button class="searchbox-submit-cybozu" type="submit"></button>
                        </div>
                     </div>
                  </div>
               </form>
            </div>
         </div>
         <div class="clear_both_0px"></div>
      </div>
      <!--menubar_end-->
      <script language="JavaScript" type="text/javascript">
         <!--
         function __get_referer_bdate()
         {
             var referer_bdate = '';
             var param_elements = document.getElementsByName('bdate');
             for( var i = 0; i < param_elements.length; i++ )
             {
                 if (param_elements[i].getAttribute('value'))
                 {
                     referer_bdate = param_elements[i].getAttribute('value');
                     break;
                 }
             }
             return referer_bdate;
         }
         -->
      </script>
      <style type="text/css">
         .tableFixed{
         width:100%;
         table-layout:fixed;
         }
         .normalEvent {}
         .normalEventElement{}
         .showEventTitle{
         position:absolute;
         max-width: 300px;
         overflow:hidden;
         z-index: 20;
         }
         .hideEventTitle{}
         .hideEventTitle .normalEvent {
         overflow:hidden;
         vertical-align:top;
         }
         .userElement{
         overflow:hidden;
         }
         .nowrap_class{
         white-space:nowrap;
         padding:2px;
         overflow:hidden;
         }
      </style>
      <span id="measure_string" style="visibility:hidden;white-space:nowrap;"></span>
      <script type="text/javascript" language="javascript">
         var isScheduleGroupDay = false;
         var popup_dimensions = {};
         jQuery(document).on('mousemove', getMousePosition);
         jQuery(document).on('mouseover', getMousePosition);
         var mouse_position = null;
         function getMousePosition(e){
             mouseX = e.pageX;
             mouseY = e.pageY;
             mouse_position = {x:mouseX,y:mouseY};
             
             // hide popup with schedule Group Day
             if( isScheduleGroupDay )
             {
                 if( mouseX < popup_dimensions.left || mouseX > popup_dimensions.left + popup_dimensions.width ||
                     mouseY < popup_dimensions.top || mouseY > popup_dimensions.top + popup_dimensions.height )
                 {
                     var popup_title = document.getElementById('popup_show_title');
                     if(popup_title){
                         jQuery(popup_title).remove();
                     }
                 }
             }
             
             return true;
         }
         
         function isGroupWeekPortlet(){
             if( typeof gw_table_prefix_items !== 'undefined' ){
                 return true;
             }
             return false;
         }
         
         // page_on_load show that page is just on load
         function showFullShortTitle(check_id,schedule_id,page,page_on_load)
         {
             if(check_id == 'show_title_phonemessage')
             {
                 showTitlePhoneMsg(schedule_id);
                 return;
             }
             
             // check groupday or groupweekschedule
             if( page == "group_day" || page == "view_group_day" || page == "confirm" )
                 isScheduleGroupDay = true;
             else
                 isScheduleGroupDay = false;
         
             var show_full_title = jQuery('#' + check_id);
             if( !show_full_title.length )
                 return;
             var scheduleWrapper = jQuery('#' + schedule_id);
             var normalEventElements = jQuery(fastGetElementsByClassName(schedule_id,'div','normalEventElement'));
             var status = 0;
         
             // show full title
             if( show_full_title.prop('checked') )
             {
                 if(!page_on_load)
                 {
                     scheduleWrapper.removeClass('tableFixed hideEventTitle');
                     
                     if( isGroupWeekPortlet() ){
                         gw_table_prefix_items['classes'] = gw_table_prefix_items['classes'].filter(function(elm){
                             return elm != 'tableFixed' && elm != 'hideEventTitle';
                         });
                         gw_table_prefix_items['classes'] = gw_table_prefix_items['classes'];
                     }
                     
                     // show or hide shortcut
                     scheduleWrapper.find('div.shortcut_box_full').each(function(index, elm){
                         jQuery(elm).show();
                     });
                     scheduleWrapper.find('div.shortcut_box_short').each(function(index, elm){
                         jQuery(elm).hide();
                     });
                 }
                 
                 // remove nowrap_class of username
                 var userElements = scheduleWrapper.find('td.userBox')
                 userElements.each(function(index, userElement){
                     jQuery(userElement).removeClass('nowrap_class');
                 });
         
                 normalEventElements.each(function(index, elm){
                     jQuery(elm).off('mouseover');
                     jQuery(elm).off('mouseout');
                 });
                 status = 1;
             }
             else // short title, full when over
             {
                 if(!page_on_load)
                 {
                     scheduleWrapper.addClass('tableFixed hideEventTitle');
                     
                     if( isGroupWeekPortlet() ){
                         gw_table_prefix_items['classes'].push('tableFixed');
                         gw_table_prefix_items['classes'].push('hideEventTitle');
                     }
                     
                     // show or hide shortcut
                     scheduleWrapper.find('div.shortcut_box_full').each(function(index, elm){
                         jQuery(elm).hide();
                     });
                     scheduleWrapper.find('div.shortcut_box_short').each(function(index, elm){
                         jQuery(elm).show();
                     });
                 }
                 processDisplayUserName(schedule_id);
                 var body = document.getElementById('body');
                 normalEventElements.each(function(index, e){
                     // save schedule type
                     e.isScheduleGroupDay = isScheduleGroupDay;
                     
                     jQuery(e).on('mouseover', function(event){
                         if(typeof disable_tooltip == "undefined" || disable_tooltip == 0)
                         {
                             if(isTouchDevice()){return;}
                             // assign schedule type
                             isScheduleGroupDay = e.isScheduleGroupDay;
         
                             var popup_title = document.getElementById('popup_show_title');
                             if(popup_title){
                                 jQuery(popup_title).remove();
                             }
         
                             var normalEvent = jQuery(e).parents('td.normalEvent').get(0);
         
                             // create popup
                             var popup_title = document.createElement("div");
                             popup_title.setAttribute("id", "popup_show_title");
                             popup_title.setAttribute("class", "showEventTitle");
                             popup_title.innerHTML = e.innerHTML;
                             
                             // disable link
                             if( !isScheduleGroupDay )
                             {
                                 var a_tag = jQuery(popup_title).find('a:first-child');
                                 if(a_tag.length){
                                     a_tag.removeAttr('href');
                                 }
                             }
                             else
                             {
                                 // register star
                                 var popup_star_item = jQuery(popup_title).find('.star:first-child');
                                 var event_star_item = jQuery(e).find('.star:first-child');
                                 if(popup_star_item && event_star_item.star_list){
                                     popup_star_item.on('click', event_star_item.star_list._onClick.bind(event_star_item.star_list,popup_star_item));
                                 }
                             }
                             
                             // append to body
                             body.appendChild(popup_title);
                             
                             // position
                             if( isScheduleGroupDay )
                             {
                                 var pos = findPositionElement(e);
                                 mouse_position.x = pos.left;
                                 mouse_position.y = pos.top;
                                 jQuery(popup_title).css({top: mouse_position.y + 'px', left: mouse_position.x + 'px'});
                             }
                             else
                             {
                                 jQuery(popup_title).css({top: mouse_position.y + 5 + 'px', left: mouse_position.x + 5 + 'px'});
                             }
                             
                             // caculate width for IE
                             var IE = document.all ? true : false;
                             if(IE)
                             {
                                 var tooltip_width = measureStringByPixel(popup_title.innerHTML) + 20;
                                 var body_width = Math.max(body.clientWidth, body.scrollWidth);
                                 if( mouse_position.x + tooltip_width > body_width ){
                                     tooltip_width = body_width - mouse_position.x;
                                 }
                                 if(tooltip_width > 300){
                                     tooltip_width = '300';
                                 }
                                 jQuery(popup_title).css({width:tooltip_width + 'px'});
                             }
                             
                             // store popup dimension
                             popup_dimensions.top = mouse_position.y;
                             popup_dimensions.left = mouse_position.x;
                             popup_dimensions.width = popup_title.offsetWidth;
                             popup_dimensions.height = popup_title.offsetHeight;
                         }
                     });
                     
                     // remove popup when mouse leave
                     if( !isScheduleGroupDay )
                     {
                         jQuery(e).on('mouseout', function(event){
                             var popup_title = document.getElementById('popup_show_title');
                             if(popup_title){
                                 jQuery(popup_title).remove();
                             }
                         });
                     }
                 });
                 status = 0;
             }
             // save status
             if(!page_on_load)
             {
                 new jQuery.ajax({
                     url: show_full_title_url,
                     type: 'POST',
                     data: 'status='+status + '&page='+ page + '&csrf_ticket=1153e7462753d2c3cb7aacab2f545d8e'
                 });
                 /* GTM-101 */
                 /* Drag drop schedule */
                 /*
                 if( isScheduleGroupDay )
                 {
                     if( page == "view_group_day" )
                     {
                         dd_remove(schedule_id.substring(10));
                         dd_init("",schedule_id.substring(10));
                         dd_handle(schedule_id.substring(10));
                     }
                     else if( page == "group_day" )
                     {
                         dd_remove();
                         dd_init();
                         dd_handle();
                     }
                 }
                 */
                 /* End GTM-101 */
             }
         }
         
         // process display user name
         function processDisplayUserName(schedule_id)
         {
             var scheduleWrapper = jQuery('#' + schedule_id);
             if(!scheduleWrapper.length)
                 return;
             var userBoxs = scheduleWrapper.find('td.userBox');
             userBoxs.each(function(index, userBox){
                 jQuery(userBox).addClass('nowrap_class');
             });
         }
         
         // measure length of string by pixel
         function measureStringByPixel(data){
             var measure= document.getElementById('measure_string');
             measure.innerHTML = data;
             var width = measure.offsetWidth;
             measure.innerHTML = '';
             return width;
         }
         
         // find position on IE
         function findPositionElement(obj){
             var left = 0;
             var top = 0;
             if(obj.offsetParent)
             {
                 do {
                     left += obj.offsetLeft;
                     top += obj.offsetTop;
                 } while (obj = obj.offsetParent);
             }
             return {left:left,top:top};
         }
         
         function showTitlePhoneMsg(schedule_id)
         {
             processDisplayUserName(schedule_id);
             var body = document.getElementById('body');
             var normalEventElements = jQuery(fastGetElementsByClassName(schedule_id,'div','normalEventElement'));
             normalEventElements.each(function(index, e){
                 jQuery(e).on('mouseover', function(event){
                     var popup_title = document.getElementById('popup_show_title');
                     if(popup_title){
                         jQuery(popup_title).remove();
                     }
                     var normalEvent = jQuery(e).parents('td.normalEvent').get(0);
         
                     // create popup
                     var popup_title = document.createElement('div');
                     popup_title.setAttribute('id', 'popup_show_title');
                     popup_title.setAttribute('class', 'showEventTitle');
                     popup_title.innerHTML = e.innerHTML;
                     
                     var a_tag = jQuery(popup_title).find('a:first-child');
                     if(a_tag.length){
                         a_tag.removeAttr('href');
                     }
                     
                     // append to body
                     body.appendChild(popup_title);
                     
                     jQuery(popup_title).css({top: mouse_position.y + 5 + 'px', left: mouse_position.x + 5 + 'px'});
                     
                     // caculate width for IE
                     var IE = document.all ? true : false;
                     if(IE)
                     {
                         var tooltip_width = measureStringByPixel(popup_title.innerHTML) + 20;
                         var body_width = Math.max(body.clientWidth, body.scrollWidth);
                         if( mouse_position.x + tooltip_width > body_width ){
                             tooltip_width = body_width - mouse_position.x;
                         }
                         if(tooltip_width > 300){
                             tooltip_width = '300';
                         }
                         jQuery(popup_title).css({width:tooltip_width + 'px'});
                     }
                     
                     // store popup dimension
                     popup_dimensions.top = mouse_position.y;
                     popup_dimensions.left = mouse_position.x;
                     popup_dimensions.width = popup_title.offsetWidth;
                     popup_dimensions.height = popup_title.offsetHeight;
                 });
                 
                 jQuery(e).on('mouseout', function(event){
                     var popup_title = document.getElementById('popup_show_title');
                     if(popup_title){
                         jQuery(popup_title).remove();
                     }
                 });
             });
         }
         
         
      </script>
      <script type="text/javascript" language="javascript">
         var show_full_title_url = "/schedule/command_show_full_title?";
         jQuery(window).on('load', function(event){
             showFullShortTitle('show_full_titlegroup_day','event_list','group_day',true);
             });
      </script>
      <form name="schedule/group_day" method="GET" action="/group_day?bdate=2020-12-23&amp;uid=&amp;">
         <div class="margin_bottom">
            <table width="100%">
               <tbody>
                  <tr>
                     <td class="v-allign-middle" nowrap="nowrap">
                        <table cellspacing="0" cellpadding="0" border="0">
                           <tbody>
                              <tr>
                                 <td nowrap="nowrap">
                                    <script language="JavaScript" type="text/javascript">
                                       function setUserGroups( group_obj ) {
                                         
                                           group_obj.appendItem( new GRN_GroupItem( '16', '第1営業グループ (Priority organizations)', 'window.parent.clickUserGroup', false, '' ) );
                                        
                                           group_obj.appendItem( new GRN_GroupItem( '18', 'Cyde America (Memberships)', 'window.parent.clickUserGroup', false, '' ) );
                                        
                                           group_obj.appendItem( new GRN_GroupItem( 'r', '(Recently selected users)', 'window.parent.clickUserGroup', false, '' ) );
                                        
                                           group_obj.appendItem( new GRN_GroupItem( 'login', '(Logged-in user)', 'window.parent.clickUserGroup', false, '' ) );
                                         
                                       
                                       }
                                       function clickUserGroup( gid, name, extra_param ) {
                                           document.getElementById( 'popup_group_list_iframe' ).style.display= 'none';
                                           updateDropdownButtonTitle( name );
                                           location.href = "/schedule/group_day?bdate=2020-12-23&date=" + document.forms["schedule/group_day"].date.value + '&gid='+gid + '&p=' + extra_param;
                                       }
                                       
                                       function clickFacilityGroupTree( form_name, param ){
                                           document.getElementById( 'popup_facility_group_tree_iframe' ).style.display= 'none';
                                       
                                           if ( param['fagid'] != '0' && param['fagid'] != 'f' ) {
                                               if( param['fagid'] == 'r' || param['extra_param'] != 0 ){
                                                   updateDropdownButtonTitle( param['name'] );
                                               }
                                               else{
                                                   updateDropdownButtonTitle( param['name'] );
                                               }
                                               changeDropDownWidth( 'wrap_dropdown_facility_current' );
                                               location.href = "/schedule/group_day?bdate=2020-12-23&date=" + document.forms["schedule/group_day"].date.value + '&gid=f' + param['fagid']+ '&p='+param['extra_param'];
                                           }else {
                                               updateDropdownButtonTitle( param['name'] );
                                               changeDropDownWidth( 'wrap_dropdown_facility_current' );
                                               location.href = "/schedule/group_day?bdate=2020-12-23&date=" + document.forms["schedule/group_day"].date.value + '&gid=f' + '&p='+param['extra_param'];
                                           }
                                       }
                                       
                                       function updateDropdownButtonTitle( newTitle )
                                       {
                                           var node = window.document.getElementById( 'dropdown_current_a' );
                                           node.innerHTML = unescape( newTitle );
                                           changeDropDownWidth( 'wrap_dropdown_facility_current' );
                                       }
                                       
                                    </script>
                                    <link href="{!!asset('assets/css/fag_tree.css')!!}" rel="stylesheet" type="text/css">
                                    <table id="group-select" border="0" cellspacing="0" cellpadding="0" class="wrap_dropdown_menu" style="width: 332px;">
                                       <tbody>
                                          <tr height="20">
                                             <td id="title" class="dropdown_menu_current" height="20" nowrap="">Tìm kiếm theo phòng ban</td>
                                             <td id="user-button" class="dropdown_menu_user" style="margin:0px;paddig:0px;" valign="center" aligh="left" width="37" height="20">
                                                <img src="{!!asset('/img/user-dropdown30x20.gif')!!}" style="margin:0px;paddig:0px;" border="0">
                                             </td>
                                             <td id="facility-button" class="dropdown_menu_facility" style="margin:0px;paddig:0px;" valign="center" aligh="left" width="37" height="20">
                                                <img src="{!!asset('/img/facility-dropdown30x20.gif')!!}" style="margin:0px;paddig:0px;" border="0">
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <div id="user-popup" class="wrap_dropdown_option" style="visibility: visible; display: none;">
                                       
                                    </div>
                                    <div id="facility-popup" class="wrap_dropdown_option" style="display: none;">
                                       <iframe id="facility-popup-if" src="" style="z-index: 2; position: absolute; border: 0px; height: 242px; width: 322px;"></iframe>
                                       
                                    </div>
                                    <div id="dummy-popup" class="wrap_dropdown_option"></div>
                                    <div></div>
                                    <div id="facility-popup-dummy_root" style="border:1px solid #000000; top:-10000px; left:-10000px; position:absolute; background-color:#FFFFFF; overflow:scroll; width:1px;height:1px;">
                                       <div id="facility-popup-dummy_tree_wrap_tree1" class="wrap_tree1" style="width: 320px;">
                                           <div id="facility-popupmain-div2" class="wrap_tree2" style="width: 298px; height: 84px;"><div id="facility-popupmain-div-tree" style="background-color:#FFFFFF;"><div class="ygtvitem" id="ygtv0"><div class="ygtvchildren" id="ygtvc0"><div class="ygtvitem" id="ygtv1"><table id="ygtvtableel1" border="0" cellpadding="0" cellspacing="0" class="ygtvtable ygtvdepth0"><tbody><tr class="ygtvrow"><td id="ygtvt1" class="ygtvcell ygtvtn"><a href="#" class="ygtvspacer">&nbsp;&nbsp;&nbsp;&nbsp;</a></td><td id="ygtvcontentel1" class="ygtvcell  ygtvcontent" nowrap="nowrap"><span id="ygtvlabelel1" class="ygtvlabel ygtv-highlight0">Phòng họp tầng 3</span></td></tr></tbody></table><div class="ygtvchildren" id="ygtvc1" style="display:none;"></div></div><div class="ygtvitem" id="ygtv2"><table id="ygtvtableel2" border="0" cellpadding="0" cellspacing="0" class="ygtvtable ygtvdepth0"><tbody><tr class="ygtvrow"><td id="ygtvt2" class="ygtvcell ygtvtn"><a href="#" class="ygtvspacer">&nbsp;&nbsp;&nbsp;&nbsp;</a></td><td id="ygtvcontentel2" class="ygtvcell  ygtvcontent" nowrap="nowrap"><span id="ygtvlabelel2" class="ygtvlabel ygtv-highlight0">Phòng họp tầng 2</span></td></tr></tbody></table><div class="ygtvchildren" id="ygtvc2" style="display:none;"></div></div><div class="ygtvitem" id="ygtv3"><table id="ygtvtableel3" border="0" cellpadding="0" cellspacing="0" class="ygtvtable ygtvdepth0"><tbody><tr class="ygtvrow"><td id="ygtvt3" class="ygtvcell ygtvln"><a href="#" class="ygtvspacer">&nbsp;&nbsp;&nbsp;&nbsp;</a></td><td id="ygtvcontentel3" class="ygtvcell  ygtvcontent" nowrap="nowrap"><span id="ygtvlabelel3" class="ygtvlabel ygtv-highlight0">Phòng họp tầng 1</span></td></tr></tbody></table><div class="ygtvchildren" id="ygtvc3" style="display:none;"></div></div></div></div></div></div>
                                       </div>
                                    </div>
                                    <script type="text/javascript">
                                       (function () {
                                       
                                           var group_select_id    = 'group-select';
                                           var title_id           = 'title';
                                           var user_button_id     = 'user-button';
                                           var facility_button_id = 'facility-button';
                                           var user_popup_id      = 'user-popup';
                                           var facility_popup_id  = 'facility-popup';
                                           var is_multi_view      = false;
                                       
                                           var dropdown = new GRN_DropdownMenu(
                                               group_select_id, title_id, user_button_id, facility_button_id,
                                               GRN_DropdownMenu.prototype.PreferOrganization,
                                               user_popup_id, facility_popup_id,
                                               clickOrganizationCallback, clickFacilityGroupCallback,
                                               "" );
                                       
                                           function updateTitle( title ) {
                                               var old_width = jQuery('#' + group_select_id).outerWidth();
                                               jQuery('#' + group_select_id).css( {'width':''} );
                                               jQuery('#' + title_id).html( title );
                                               if( old_width > jQuery('#' + group_select_id).outerWidth() ) {
                                                   jQuery('#' + group_select_id).css( { 'width': old_width + 'px'} );
                                               }
                                           }
                                       
                                           function clickOrganizationCallback( group_item ) {
                                               return function(){
                                                   updateTitle( group_item.name )
                                                   dropdown.organization.hide();
                                                   
                                                   if (is_multi_view) {
                                                       
                                                       jQuery(document).trigger("scheduler.select_user_org_facility_dropdownlist.select", {gid: group_item.gid, target: dropdown});
                                                       return;
                                                   }
                                                   
                                                   location.href = "/schedule/group_day?"+ '&bdate=' + document.forms["schedule/group_day"].bdate.value + '&gid='+group_item.gid;
                                               }
                                           }
                                       
                                           function clickFacilityGroupCallback( node ) {
                                               if( node.extra_param ) { 
                                                   updateTitle( node.label );
                                               }
                                               else {
                                                   if( node.oid == 'f' ) {
                                                       updateTitle( '(All facilities)' );
                                                   }else{
                                                       updateTitle(  node.label );
                                                   }
                                               }
                                               dropdown.facility.hide();
                                       
                                               var oid = node.oid;
                                               
                                               location.href = "/schedule/group_day?"+ '&bdate=' + document.forms["schedule/group_day"].bdate.value + '&eid='+oid;
                                           }
                                           dropdown.initializeOrganization(
                                               new Array(
                                                   {!!$department!!}
                                                        ) );
                                       
                                           var group_select_width = dropdown.organization.getWidth( jQuery('#' + title_id).outerWidth() );
                                           jQuery('#' + group_select_id).css( {'width': group_select_width +"px"} );
                                       
                                           dropdown.updateTitle = updateTitle;
                                       
                                           dropdown.initializeFacilityGroup( { 'page_name': "schedule/group_day",
                                                                               'ajax_path':'/schedule/json/accessible_facility_tree?',
                                                                               'csrf_ticket':'1153e7462753d2c3cb7aacab2f545d8e',
                                                                               'callback':clickFacilityGroupCallback,
                                                                               'selectedOID':"",
                                                                               'title_width': jQuery('#' + title_id).outerWidth(),
                                                                               'node_info':
                                                                               [{!!$equipment!!}]
                                                                             });
                                       }());
                                       
                                    </script>
                                 </td>
                                 <td nowrap="nowrap"><a class="selectPulldownSub-grn" href="javascript:void(0);" onclick="javascript:popupWin('/schedule/popup_user_select?plugin_session_name=schedule%2Fgroup_day&amp;plugin_data_name=access_plugin&amp;is_post_message=&amp;selected_tid=&amp;form_name=schedule%2Fgroup_day&amp;select_name=sUID%5B%5D&amp;app_id=schedule&amp;return_page=&amp;plid=&amp;system=0&amp;include_org=1&amp;system_display=0&amp;no_multiple=0&amp;send_cgi_parameter=0&amp;multi_apply=1&amp;require_role_tab=0&amp;is_calendar=0&amp;show_group_role=1&amp;require_dynamic_roles=0&amp;require_administrator_role=0','html','','',0,0,1,1,0,1);" title="Select users"><img src="{!!asset('/img/blankB16.png')!!}" border="0" alt=""></a></td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                     <td class="v-allign-middle" align="center">
                        <span class="displaydate">{{date('D, F d, Y',strtotime($date))}}</span>
                        <span class="viewSubCalendar-grn">
                        <span id="showIcon-grn" class="showIconOff-grn" title="Show calendar">
                        <span class="subCalendar-grn"></span>
                        </span>
                        </span>
                     </td>
                     <td class="v-allign-middle" nowrap="nowrap" align="right">
                        <div class="moveButtonBlock-grn">
                           <span class="moveButtonBase-grn"><a href="/schedule/group_day?bdate={{date('Y-m-d',strtotime('- 6 days',strtotime($date)))}}&amp;uid=&amp;gid=&amp;eid=&amp;event=&amp;event_date=&amp;pid=&amp;sp=0&amp;search_text=&amp;p=" title="Previous week"><span class="moveButtonArrowLeftTwo-grn"></span></a></span><span class="moveButtonBase-grn"><a href="/schedule/group_day?bdate={{date('Y-m-d',strtotime('- 1 days',strtotime($date)))}}&amp;uid=&amp;gid=&amp;event=&amp;event_date=&amp;pid=&amp;sp=0&amp;search_text=&amp;p=" title="Previous day"><span class="moveButtonArrowLeft-grn"></span></a></span><span class="moveButtonBase-grn" title=""><a href="/schedule/group_day?bdate={{date('Y-m-d')}}&amp;uid=&amp;gid=&amp;event=&amp;event_date=&amp;pid=&amp;sp=0&amp;search_text=&amp;p=">Today</a></span><span class="moveButtonBase-grn"><a href="/schedule/group_day?bdate={{date('Y-m-d',strtotime('+ 1 days',strtotime($date)))}}&amp;uid=&amp;gid=&amp;event=&amp;event_date=&amp;pid=&amp;sp=0&amp;search_text=&amp;p=" title="Next day"><span class="moveButtonArrowRight-grn"></span></a></span><span class="moveButtonBase-grn"><a href="/schedule/group_day?bdate={{date('Y-m-d',strtotime('+ 6 days',strtotime($date)))}}&amp;uid=&amp;gid=&amp;event=&amp;event_date=&amp;pid=&amp;sp=0&amp;search_text=&amp;p=" title="Next week"><span class="moveButtonArrowRightTwo-grn"></span></a></span>
                        </div>
                     </td>
                  </tr>
               </tbody>
            </table>
         </div>
         <script language="javascript" type="text/javascript">
            <!--
            var grn_schedule_navi_command_on;
            
            var grn_schedule_navi_cal_url = "/schedule/command_navi_calendar_display?";
            
            var title_show_calendar = "Show calendar";
            var title_hide_calendar = "Hide calendar";
            
            function grn_schedule_navi_cal()
            {
                var icon_tag = window.document.getElementById( 'showIcon-grn' );
                if(YAHOO.util.Dom.hasClass('showIcon-grn', 'showIconOff-grn'))
                {
                    jQuery('#schedule_calendar').show();
            
                    jQuery('#wait_image').show();
                    if(window.document.getElementById( 'subCalendar-grn-image' ))
                    {
                        jQuery('#subCalendar-grn-image').hide();
                    }
                    
                    grn_schedule_navi_command_on = true;
                    grn_schedule_send_req('on');
                    
                    YAHOO.util.Dom.removeClass('showIcon-grn', 'showIconOff-grn');
                    YAHOO.util.Dom.addClass('showIcon-grn', 'showIconOn-grn');
                    
                    icon_tag.setAttribute("title", title_hide_calendar);
                }
                else
                {
                    if(YAHOO.util.Dom.hasClass('showIcon-grn', 'showIconOn-grn'))
                    {
                        jQuery('#schedule_calendar').hide();
            
                        grn_schedule_navi_command_on = false;
                        grn_schedule_send_req('off');
                        
                        YAHOO.util.Dom.removeClass('showIcon-grn', 'showIconOn-grn');
                        YAHOO.util.Dom.addClass('showIcon-grn', 'showIconOff-grn');
                        
                        icon_tag.setAttribute("title", title_show_calendar);
                    }
                }
            }
            
            function grn_schedule_send_req(navi_cal_display_flag)
            {
                var post_body = jQuery.param({navi_cal_display_flag:navi_cal_display_flag});
                post_body += '&csrf_ticket=1153e7462753d2c3cb7aacab2f545d8e';
                var oj = new jQuery.ajax({
                        url         : grn_schedule_navi_cal_url,
                        type        : 'POST',
                        data        : post_body,
                        complete    : grn_schedule_onloaded
                    });
            }
            
            function grn_schedule_onloaded(jqXHR)
            {
                var headers = jqXHR.getAllResponseHeaders();
                var regex = /X-Cybozu-Error/i;
                if( headers.match( regex ) )
                {
                    document.body.innerHTML = jqXHR.responseText;
                    return false;
                }
            
                jQuery('#wait_image').hide();
                if(window.document.getElementById( 'subCalendar-grn-image' ))
                {
                    jQuery('#subCalendar-grn-image').show();
                }
            
                if (grn_schedule_navi_command_on)
                {
                    jQuery('#navi_cal_label_on').show();
                    jQuery('#navi_cal_label_off').hide();
                }
                else
                {
                    jQuery('#navi_cal_label_off').show();
                    jQuery('#navi_cal_label_on').hide();
                }
            }
            
            YAHOO.util.Event.addListener( window.document.getElementById( 'showIcon-grn' ), 'click', grn_schedule_navi_cal );
            //-->
            
         </script>    
         <center>
            <img src="{!!asset('/img/spinner.gif')!!}" id="wait_image" name="wait_image" style="display:none">
            <div id="schedule_calendar" style="display:none">
               {!!$calendar_html!!}
            </div>
         </center>
         <script language="javascript" type="text/javascript">
            <!--
            function doMoveCalednar( move_date, onComplete )
            {
                var url = "/api/schedule_navi_calendar?location=schedule%2Fgroup_day&p=&search_text=&uid=&gid=&event=&event_date=&vwdate=2020-12-23"+'&cndate='+move_date;
                ajax = new jQuery.ajax({
                    url:                url,
                    type:               'GET',
                    async:               true,
                    success: function(result) {
                        jQuery('#schedule_calendar').html(result);
                    },
                    complete:           onComplete
                });
            }
            
            //-->
            
         </script>        
         <table class="day_table scheduleWrapper scheduleWrapperTimezoneBar   group_day_calendar js_customization_schedule_view_type_GROUP_DAY" id="event_list">
            <tbody>
               {!!$html!!}
            </tbody>
         </table>
         <div class="none">&nbsp;</div>
         
         <input type="hidden" name="bdate" value="{{date('Y-m-d')}}">
      </form>
   </div>
   <script src="{!!asset('/js/dragdrop-min.js')!!}" type="text/javascript"></script>
   <script src="{!!asset('/js/selector-min.js')!!}" type="text/javascript"></script>
   <script text="text/javascript">
      // namespace
      grn.base.namespace("grn.component.error_handler");
      
      (function () {
          var G = grn.component.error_handler;
      
          G.show = function (request, my_callback) {
              var s = request.responseText;
              if (s != undefined) {
                  var title = '';
                  var msg = '';
                  var json = null;
      
                  try {
                      json = JSON.parse(s);
                  } catch(e){}
      
                  if (json) {
                      var show_backtrace = "";
                      title = '';//json.code;
      
                      if (show_backtrace) {
                          msg = msg + "<div style='height:145px; overflow: auto;'><table><tr><td>";
                      }
                      msg = msg + "<div><img border='0' src='{!!asset('/img/warn100x60.gif')!!}'></div>";
                      msg = msg + "<div class='bold'>" + 'Error (' + json.code + ")</div><div>" + json.diagnosis + "</div><br>";
                      msg = msg + "<div class='bold'>" + 'Cause' + "</div><div>" + json.cause + "</div><br>";
                      msg = msg + "<div class='bold'>" + 'Countermeasure' + "</div><div>" + json.counter_measure + "</div>";
      
                      if (show_backtrace) {
                          msg = msg + "<br><hr>";
                          msg = msg + "<div class='bold'>( Beta/Debug only. by common.ini )</div><br>";
                          msg = msg + "<div class='bold'>Developer Info</div><br>";
                          if (json.developer_info) {
                              msg = msg + json.developer_info;
                          }
                          msg = msg + "<div class='bold'>Backtrace</div><br>";
                          msg = msg + "How to read backtraces(to be written) / How to read backtraces (to be written)<br>";
                          msg = msg + "<pre style='border:1px solid #6666ff; background:#eeeeff; padding:10px;'>";
                          if (json.backtrace) {
                              msg = msg + json.backtrace;
                          }
                          msg = msg + "</pre>";
                          msg = msg + "<br>$G_INPUT<br>";
                          msg = msg + "<pre style='border:1px solid #6666ff; background:#eeeeff; padding:10px;'>";
                          if (json.input) {
                              msg = msg + json.input;
                          }
                          msg = msg + "</pre>";
                          msg = msg + "</td></tr></table></div>";
                      }
                  }
                  else {
                      title = 'Error';
                      msg = grn_split_tags(s, 1000);
                  }
              }
              else {
                  title = "Error";
                  msg = "Connection failed.";
              }
      
              GRN_MsgBox.show(msg, title, GRN_MsgBoxButtons.ok, {
                  ui: [],
                  caption: {
                      ok: 'OK'
                  },
      
                  callback: function (result, form) {
                      GRN_MsgBox._remove();
                      if (typeof my_callback != 'undefined') my_callback();
                  }
              });
          };
          G.getHeader = function (request) {
              if (typeof(request.getAllResponseHeaders) == 'function') {
                  return request.getAllResponseHeaders();
              }
              else {
                  return request.getAllResponseHeaders; // for YAHOO.util.Connect.asyncRequest
              }
          };
          G.hasCybozuError = function (request) {
              var headers = G.getHeader(request);
              return (headers && headers.match(new RegExp(/X-Cybozu-Error/i))) ? true : false;
          };
          G.hasCybozuLogin = function (request) {
              var headers = G.getHeader(request);
              return (headers.match(new RegExp(/X-CybozuLogin/i)) || !headers.match(new RegExp(/X-Cybozu-User/i)));
          };
      })();
      
   </script>
</div>
<!--end of mainarea-->
<!--end of mainarea-->
</div>

@stop
@section('script')
@parent
<script>
    $('body').delegate('.button-next','click',function(){
        var date = $(this).data('next');
        var member_id = $('#select_member').val();
        $.ajax({
               type: "POST",
               url: '/api/next-schedule',
               data: {date:date,member_id:member_id},
               success: function(response){
                    $('.table-schedule').html(response.html);
                    $('#pagination').html(`
                    <a href="#" class="button-prev" data-prev='`+response.start+`'><i class="fas fa-caret-left"></i></a>
                    <a href="#" class="button-next" data-next='`+response.end+`'><i class="fas fa-caret-right"></i></a>
                    `);
                }
        });   
    });
    $('body').delegate('.button-prev','click',function(){
        var date = $(this).data('prev');
        var member_id = $('#select_member').val();
        $.ajax({
               type: "POST",
               url: '/api/prev-schedule',
               data: {date:date,member_id:member_id},
               success: function(response){
                    $('.table-schedule').html(response.html);
                    $('#pagination').html(`
                    <a href="#" class="button-prev" data-prev='`+response.start+`'><i class="fas fa-caret-left"></i></a>
                    <a href="#" class="button-next" data-next='`+response.end+`'><i class="fas fa-caret-right"></i></a>
                    `);
                }
        });   
    });
    $('#select_member').change(function(){
        var date = $('.button-prev').data('prev');
        var member_id = $(this).val();
        $.ajax({
               type: "POST",
               url: '/api/change-schedule',
               data: {date:date,member_id:member_id},
               success: function(response){
                    $('.table-schedule').html(response.html);
                    $('#list_todolist').html(response.list_html);
                }
        });
    })
    $('.add-todolist').click(function(){
        $('#modal_create_todolist').modal('show');
    })
    $('#create_todolist').submit(function(e){
        e.preventDefault();
        $.ajax({
                type: "POST",
                url: '/api/create-todolist',
                data: new FormData(this),
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(response){
                    var notifier = new Notifier();
                    var notification = notifier.notify("success", "Thêm thành công");
                    notification.push();
                    $('#list_todolist').prepend(response.html);
                }
            });   
    })
    $('body').delegate('.check-todolist','click',function(){
        if ($('input[name="todolist_id"]:checked').val() === undefined) {
            var notifier = new Notifier();
            var notification = notifier.notify("info", "Cần chọn công việc trước khi thao tác");
            notification.push();
        }else{
            var todolist_id = [];
            $('.check').each(function () {
                if ($(this).is(':checked')) {
                    todolist_id.push($(this).val());
                }
            })   
            $.ajax({
                url: '/api/change-status-todolist',
                method: 'POST',
                data: {todolist_id: todolist_id},
                success: function (response) {
                    var notifier = new Notifier();
                    var notification = notifier.notify("success", "Cập nhật thành công");
                    notification.push();
                   $('#list_todolist').html(response.html);
                }
            });
        }
    })
</script>
@stop


