<!DOCTYPE html>
<html lang="en">
   <head>
      <meta name="robots" content="noindex, nofollow, noarchive">
      <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
      <meta http-equiv="Content-Style-Type" content="text/css">
      <meta http-equiv="Content-Script-Type" content="text/javascript">
      <title>Check available times</title>
      <script src="{!!asset('assets2/js/jquery.min.js')!!}"></script>
        <link href="{{asset('assets/css/std.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('assets/css/treeview.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('assets/css/msgbox.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('assets/css/image_grn.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('assets/css/font-en.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('assets/css/Designsimple-white.css')}}" rel="stylesheet" type="text/css">
        <link rel="shortcut icon" href="{!!asset('/img/garoon.ico')!!}">
        <script src="{{asset('/js/base.js')}}" type="text/javascript"></script>
      <script language="javascript" type="text/javascript">
         <!--
         
         if( typeof grn.browser == "undefined" )
         {
             grn.browser = {};
         }
         
             grn.browser.chrome = true;
         grn.browser.version = 87;
         
         
         grn.browser.isSupportHTML5 = false;
         if( !!window.FormData )
         {
             grn.browser.isSupportHTML5 = true;
         }
         
         //-->
        </script><script src="{{asset('/js/std.js')}}" type="text/javascript"></script>
        <script src="{{asset('/js/yahoo-min.js')}}" type="text/javascript"></script>
        <script src="{{asset('/js/event-min.js')}}" type="text/javascript"></script>
        <script src="{{asset('/js/dom-min.js')}}" type="text/javascript"></script>
        <script src="{{asset('/js/connection-min.js')}}" type="text/javascript"></script>
        <script src="{{asset('/js/treeview-min.js')}}" type="text/javascript"></script>
        <script src="{{asset('/js/json-min.js')}}" type="text/javascript"></script>
        <script src="{{asset('/js/animation-min.js')}}" type="text/javascript"></script>
        <script src="{{asset('/js/yahoo-dom-event.js')}}" type="text/javascript"></script>
        <script src="{{asset('/js/autofit.js')}}" type="text/javascript"></script>
        <script src="{{asset('/js/url.js')}}" type="text/javascript"></script>
      <script>
         grn.component.url.PAGE_PREFIX = "";
         grn.component.url.PAGE_EXTENSION = "";
         grn.component.url.STATIC_URL = "/garoon3";
         grn.component.url.BUILD_DATE = "20200925.text";
      </script>
        <script src="{{asset('/js/i18n.js')}}" type="text/javascript"></script>
        <script src="{{asset('/js/resource-en.js')}}"></script>
        <script src="{{asset('/js/request.js')}}" type="text/javascript"></script>
        <script src="{{asset('/js/error_default_view.js')}}" type="text/javascript"></script>
        <script src="{{asset('/js/error_handler.js')}}" type="text/javascript"></script>
        <script src="{{asset('/js/runtime.js')}}" type="text/javascript"></script>
        <script src="{{asset('/js/commons_chunk.js')}}" type="text/javascript"></script>
        <script src="{{asset('/js/common.js')}}" type="text/javascript"></script>
        <script src="{{asset('/js/msgbox.js')}}" type="text/javascript"></script>
        <script language="JavaScript" type="text/javascript">
        <script language="JavaScript" type="text/javascript">
         grn.data = {"CSRF_TICKET":"a9c162692e1902a4c4248db6d67db480","locale":"en","login":{"id":"58","name":"Foster Brown","slash":"","timezone":"Asia\/Tokyo","language":"en","code":"brown","email":"","url":"","phone":""},"short_date_format":"&&wdayshort&&, &&mday&& &&monthfull&&","assets_cache_busting":1580698759};
      </script>
   </head>
   <body>
      <div id="body">
         <div class="marginBoth">
            <table class="layout comfirmVacantTimeHeader-grn">
               <tr>
                  <td align="left">
                     <h2 style="display:inline;" class="schedule">Check available times</h2>
                  </td>
                  <td align="right"><a href="#" onclick="window.close();"><img src="{!!asset('/img/close20.gif')!!}" border="0" alt="">Close</a></td>
               </tr>
            </table>
         </div>
         <link href="{!!asset('assets/css/schedule.css')!!}" rel="stylesheet" type="text/css">
         <link href="{!!asset('assets/css/schedule_confirm.css')!!}" rel="stylesheet" type="text/css">
         <style type="text/css">
            .tableFixed{
            width:100%;
            table-layout:fixed;
            }
            .normalEvent {}
            .normalEventElement{}
            .showEventTitle{
            position:absolute;
            max-width: 300px;
            overflow:hidden;
            z-index: 20;
            }
            .hideEventTitle{}
            .hideEventTitle .normalEvent {
            overflow:hidden;
            vertical-align:top;
            }
            .userElement{
            overflow:hidden;
            }
            .nowrap_class{
            white-space:nowrap;
            padding:2px;
            overflow:hidden;
            }
         </style>
         <span id="measure_string" style="visibility:hidden;white-space:nowrap;"></span>
         <script type="text/javascript" language="javascript">
            var isScheduleGroupDay = false;
            var popup_dimensions = {};
            jQuery(document).on('mousemove', getMousePosition);
            jQuery(document).on('mouseover', getMousePosition);
            var mouse_position = null;
            function getMousePosition(e){
                mouseX = e.pageX;
                mouseY = e.pageY;
                mouse_position = {x:mouseX,y:mouseY};
                
                // hide popup with schedule Group Day
                if( isScheduleGroupDay )
                {
                    if( mouseX < popup_dimensions.left || mouseX > popup_dimensions.left + popup_dimensions.width ||
                        mouseY < popup_dimensions.top || mouseY > popup_dimensions.top + popup_dimensions.height )
                    {
                        var popup_title = document.getElementById('popup_show_title');
                        if(popup_title){
                            jQuery(popup_title).remove();
                        }
                    }
                }
                
                return true;
            }
            
            function isGroupWeekPortlet(){
                if( typeof gw_table_prefix_items !== 'undefined' ){
                    return true;
                }
                return false;
            }
            
            // page_on_load show that page is just on load
            function showFullShortTitle(check_id,schedule_id,page,page_on_load)
            {
                if(check_id == 'show_title_phonemessage')
                {
                    showTitlePhoneMsg(schedule_id);
                    return;
                }
                
                // check groupday or groupweekschedule
                if( page == "group_day" || page == "view_group_day" || page == "confirm" )
                    isScheduleGroupDay = true;
                else
                    isScheduleGroupDay = false;
            
                var show_full_title = jQuery('#' + check_id);
                if( !show_full_title.length )
                    return;
                var scheduleWrapper = jQuery('#' + schedule_id);
                var normalEventElements = jQuery(fastGetElementsByClassName(schedule_id,'div','normalEventElement'));
                var status = 0;
            
                // show full title
                if( show_full_title.prop('checked') )
                {
                    if(!page_on_load)
                    {
                        scheduleWrapper.removeClass('tableFixed hideEventTitle');
                        
                        if( isGroupWeekPortlet() ){
                            gw_table_prefix_items['classes'] = gw_table_prefix_items['classes'].filter(function(elm){
                                return elm != 'tableFixed' && elm != 'hideEventTitle';
                            });
                            gw_table_prefix_items['classes'] = gw_table_prefix_items['classes'];
                        }
                        
                        // show or hide shortcut
                        scheduleWrapper.find('div.shortcut_box_full').each(function(index, elm){
                            jQuery(elm).show();
                        });
                        scheduleWrapper.find('div.shortcut_box_short').each(function(index, elm){
                            jQuery(elm).hide();
                        });
                    }
                    
                    // remove nowrap_class of username
                    var userElements = scheduleWrapper.find('td.userBox')
                    userElements.each(function(index, userElement){
                        jQuery(userElement).removeClass('nowrap_class');
                    });
            
                    normalEventElements.each(function(index, elm){
                        jQuery(elm).off('mouseover');
                        jQuery(elm).off('mouseout');
                    });
                    status = 1;
                }
                else // short title, full when over
                {
                    if(!page_on_load)
                    {
                        scheduleWrapper.addClass('tableFixed hideEventTitle');
                        
                        if( isGroupWeekPortlet() ){
                            gw_table_prefix_items['classes'].push('tableFixed');
                            gw_table_prefix_items['classes'].push('hideEventTitle');
                        }
                        
                        // show or hide shortcut
                        scheduleWrapper.find('div.shortcut_box_full').each(function(index, elm){
                            jQuery(elm).hide();
                        });
                        scheduleWrapper.find('div.shortcut_box_short').each(function(index, elm){
                            jQuery(elm).show();
                        });
                    }
                    processDisplayUserName(schedule_id);
                    var body = document.getElementById('body');
                    normalEventElements.each(function(index, e){
                        // save schedule type
                        e.isScheduleGroupDay = isScheduleGroupDay;
                        
                        jQuery(e).on('mouseover', function(event){
                            if(typeof disable_tooltip == "undefined" || disable_tooltip == 0)
                            {
                                if(isTouchDevice()){return;}
                                // assign schedule type
                                isScheduleGroupDay = e.isScheduleGroupDay;
            
                                var popup_title = document.getElementById('popup_show_title');
                                if(popup_title){
                                    jQuery(popup_title).remove();
                                }
            
                                var normalEvent = jQuery(e).parents('td.normalEvent').get(0);
            
                                // create popup
                                var popup_title = document.createElement("div");
                                popup_title.setAttribute("id", "popup_show_title");
                                popup_title.setAttribute("class", "showEventTitle");
                                popup_title.innerHTML = e.innerHTML;
                                
                                // disable link
                                if( !isScheduleGroupDay )
                                {
                                    var a_tag = jQuery(popup_title).find('a:first-child');
                                    if(a_tag.length){
                                        a_tag.removeAttr('href');
                                    }
                                }
                                else
                                {
                                    // register star
                                    var popup_star_item = jQuery(popup_title).find('.star:first-child');
                                    var event_star_item = jQuery(e).find('.star:first-child');
                                    if(popup_star_item && event_star_item.star_list){
                                        popup_star_item.on('click', event_star_item.star_list._onClick.bind(event_star_item.star_list,popup_star_item));
                                    }
                                }
                                
                                // append to body
                                body.appendChild(popup_title);
                                
                                // position
                                if( isScheduleGroupDay )
                                {
                                    var pos = findPositionElement(e);
                                    mouse_position.x = pos.left;
                                    mouse_position.y = pos.top;
                                    jQuery(popup_title).css({top: mouse_position.y + 'px', left: mouse_position.x + 'px'});
                                }
                                else
                                {
                                    jQuery(popup_title).css({top: mouse_position.y + 5 + 'px', left: mouse_position.x + 5 + 'px'});
                                }
                                
                                // caculate width for IE
                                var IE = document.all ? true : false;
                                if(IE)
                                {
                                    var tooltip_width = measureStringByPixel(popup_title.innerHTML) + 20;
                                    var body_width = Math.max(body.clientWidth, body.scrollWidth);
                                    if( mouse_position.x + tooltip_width > body_width ){
                                        tooltip_width = body_width - mouse_position.x;
                                    }
                                    if(tooltip_width > 300){
                                        tooltip_width = '300';
                                    }
                                    jQuery(popup_title).css({width:tooltip_width + 'px'});
                                }
                                
                                // store popup dimension
                                popup_dimensions.top = mouse_position.y;
                                popup_dimensions.left = mouse_position.x;
                                popup_dimensions.width = popup_title.offsetWidth;
                                popup_dimensions.height = popup_title.offsetHeight;
                            }
                        });
                        
                        // remove popup when mouse leave
                        if( !isScheduleGroupDay )
                        {
                            jQuery(e).on('mouseout', function(event){
                                var popup_title = document.getElementById('popup_show_title');
                                if(popup_title){
                                    jQuery(popup_title).remove();
                                }
                            });
                        }
                    });
                    status = 0;
                }
                // save status
                if(!page_on_load)
                {
                    new jQuery.ajax({
                        url: show_full_title_url,
                        type: 'POST',
                        data: 'status='+status + '&page='+ page + '&csrf_ticket=a9c162692e1902a4c4248db6d67db480'
                    });
                    /* GTM-101 */
                    /* Drag drop schedule */
                    /*
                    if( isScheduleGroupDay )
                    {
                        if( page == "view_group_day" )
                        {
                            dd_remove(schedule_id.substring(10));
                            dd_init("",schedule_id.substring(10));
                            dd_handle(schedule_id.substring(10));
                        }
                        else if( page == "group_day" )
                        {
                            dd_remove();
                            dd_init();
                            dd_handle();
                        }
                    }
                    */
                    /* End GTM-101 */
                }
            }
            
            // process display user name
            function processDisplayUserName(schedule_id)
            {
                var scheduleWrapper = jQuery('#' + schedule_id);
                if(!scheduleWrapper.length)
                    return;
                var userBoxs = scheduleWrapper.find('td.userBox');
                userBoxs.each(function(index, userBox){
                    jQuery(userBox).addClass('nowrap_class');
                });
            }
            
            // measure length of string by pixel
            function measureStringByPixel(data){
                var measure= document.getElementById('measure_string');
                measure.innerHTML = data;
                var width = measure.offsetWidth;
                measure.innerHTML = '';
                return width;
            }
            
            // find position on IE
            function findPositionElement(obj){
                var left = 0;
                var top = 0;
                if(obj.offsetParent)
                {
                    do {
                        left += obj.offsetLeft;
                        top += obj.offsetTop;
                    } while (obj = obj.offsetParent);
                }
                return {left:left,top:top};
            }
            
            function showTitlePhoneMsg(schedule_id)
            {
                processDisplayUserName(schedule_id);
                var body = document.getElementById('body');
                var normalEventElements = jQuery(fastGetElementsByClassName(schedule_id,'div','normalEventElement'));
                normalEventElements.each(function(index, e){
                    jQuery(e).on('mouseover', function(event){
                        var popup_title = document.getElementById('popup_show_title');
                        if(popup_title){
                            jQuery(popup_title).remove();
                        }
                        var normalEvent = jQuery(e).parents('td.normalEvent').get(0);
            
                        // create popup
                        var popup_title = document.createElement('div');
                        popup_title.setAttribute('id', 'popup_show_title');
                        popup_title.setAttribute('class', 'showEventTitle');
                        popup_title.innerHTML = e.innerHTML;
                        
                        var a_tag = jQuery(popup_title).find('a:first-child');
                        if(a_tag.length){
                            a_tag.removeAttr('href');
                        }
                        
                        // append to body
                        body.appendChild(popup_title);
                        
                        jQuery(popup_title).css({top: mouse_position.y + 5 + 'px', left: mouse_position.x + 5 + 'px'});
                        
                        // caculate width for IE
                        var IE = document.all ? true : false;
                        if(IE)
                        {
                            var tooltip_width = measureStringByPixel(popup_title.innerHTML) + 20;
                            var body_width = Math.max(body.clientWidth, body.scrollWidth);
                            if( mouse_position.x + tooltip_width > body_width ){
                                tooltip_width = body_width - mouse_position.x;
                            }
                            if(tooltip_width > 300){
                                tooltip_width = '300';
                            }
                            jQuery(popup_title).css({width:tooltip_width + 'px'});
                        }
                        
                        // store popup dimension
                        popup_dimensions.top = mouse_position.y;
                        popup_dimensions.left = mouse_position.x;
                        popup_dimensions.width = popup_title.offsetWidth;
                        popup_dimensions.height = popup_title.offsetHeight;
                    });
                    
                    jQuery(e).on('mouseout', function(event){
                        var popup_title = document.getElementById('popup_show_title');
                        if(popup_title){
                            jQuery(popup_title).remove();
                        }
                    });
                });
            }
            
            
         </script>
         <script type="text/javascript" language="javascript">
            var show_full_title_url = "/schedule/command_show_full_title?";
            jQuery(window).on('load', function(event){
                showFullShortTitle('show_full_titleconfirm','schedule_confirm','confirm',true);
                });
         </script>
         <script src="{!!asset('/js/schedule_confirm.js')!!}" type="text/javascript"></script>
         <form name="schedule/confirm" method="post" action="/schedule/confirm?">
            <input type="hidden" name="csrf_ticket" value="a9c162692e1902a4c4248db6d67db480">
            <input id="date" type="hidden" name="date" value="2021-01-19">
            <input type="hidden" name="uid" value="">
            <input type="hidden" name="gid" value="">
            <input type="hidden" name="parent_page_name" value="">
            <input type="hidden" name="hide_display_time_set" value="">
            <div id="one_parts">
               <div id="view">
                  <div class="margin_bottom">
                     <div class="comfirmVacantTimeDate-grn">
                        <span class="displaydate">{{date('D, F d, Y',strtotime($date))}}</span>
                        <select id="start_hour" name="start_hour" onchange="setEndTime(this.id)"  >
                           
                           {!!$start_hour_html!!}
                        </select>
                        <select id="start_minute" name="start_minute" onchange="setEndTime(this.id)" >
                           
                           {!!$start_minute_html!!}
                        </select>
                        - 
                        <select id="end_hour" name="end_hour"   >
                           
                           {!!$end_hour_html!!}
                        </select>
                        <select id="end_minute" name="end_minute"  >
                           
                           {!!$end_minute_html!!}
                        </select>
                     </div>
                     <table width="100%">
                        <tr>
                           <td nowrap>
                           </td>
                           <td align="right" nowrap>
                              <div class="moveButtonBlock-grn">
                                 <span class="moveButtonBase-grn" title="Previous week"><a href="javascript:moveCalendar('/schedule/schedule/confirm?date={{date('Y-m-d',strtotime('- 6 days',strtotime($date)))}}&amp;uid=&amp;member_arr={{$member_arr}};event=&amp;event_date=&amp;pid=&amp;sp=&amp;search_text=&amp;p=&amp;hide_display_time_set=&amp;parent_page_name=&amp;timezone=&amp;end_timezone=')"><span class="moveButtonArrowLeftTwo-grn"></span></a></span><span class="moveButtonBase-grn" title="Previous day"><a href="javascript:moveCalendar('/schedule/confirm?date={{date('Y-m-d',strtotime('- 1 days',strtotime($date)))}}&amp;uid=&amp;member_arr={{$member_arr}};gid=&amp;event=&amp;event_date=&amp;pid=&amp;sp=&amp;search_text=&amp;p=&amp;hide_display_time_set=&amp;parent_page_name=&amp;timezone=&amp;end_timezone=')"><span class="moveButtonArrowLeft-grn"></span></a></span><span class="moveButtonBase-grn" title=""><a href="javascript:moveCalendar('/schedule/confirm?date={{date('Y-m-d')}}&amp;uid=&amp;member_arr={{$member_arr}};gid=&amp;event=&amp;event_date=&amp;pid=&amp;sp=&amp;search_text=&amp;p=&amp;hide_display_time_set=&amp;parent_page_name=&amp;timezone=&amp;end_timezone=')">Today</a></span><span class="moveButtonBase-grn" title="Next day"><a href="javascript:moveCalendar('/schedule/confirm?date={{date('Y-m-d',strtotime('+1 days',strtotime($date)))}}&amp;uid=&amp;member_arr={{$member_arr}};gid=&amp;event=&amp;event_date=&amp;pid=&amp;sp=&amp;search_text=&amp;p=&amp;hide_display_time_set=&amp;parent_page_name=&amp;timezone=&amp;end_timezone=')"><span class="moveButtonArrowRight-grn"></span></a></span><span class="moveButtonBase-grn" title="Next week"><a href="javascript:moveCalendar('/schedule/confirm?date={{date('Y-m-d',strtotime('+ 6 days',strtotime($date)))}}&amp;uid=&amp;gid=&amp;member_arr={{$member_arr}};event=&amp;event_date=&amp;pid=&amp;sp=&amp;search_text=&amp;p=&amp;hide_display_time_set=&amp;parent_page_name=&amp;timezone=&amp;end_timezone=')"><span class="moveButtonArrowRightTwo-grn"></span></a></span>
                              </div>
                              <script src="{!!asset('/js/schedule_display_options_manager.js')!!}" type="text/javascript"></script>
                              <script src="{!!asset('/js/schedule_display_options.js')!!}" type="text/javascript"></script>
                              <script type="text/javascript" language="javascript">
                                 <!--
                                 
                                 GRN_ScheduleDisplayOptions['confirm'] 
                                                = GRN_ScheduleDisplayOptionFactory.create('confirm',
                                                                                          'a9c162692e1902a4c4248db6d67db480',
                                                                                          'schedule_confirm',
                                                                                          'confirm');
                                 
                                 GRN_ScheduleDisplayOptions['confirm']
                                                .createDisplayOptionOfToDo( '/schedule/command_show_todos?' );
                                 
                                 
                                 
                                 jQuery(window).on('load', function(){
                                     GRN_ScheduleDisplayOptions['confirm'].init();
                                 });
                                 
                                 //-->
                              </script>
                              <link href="{!!asset('assets/css/schedule_display_options.css')!!}" rel="stylesheet" type="text/css">
                              <div class="schedule_display_options">
                                 <div class="scheduleDisplayOptionsLink-grn">
                                    <a id="schedule_display_options_switchconfirm" href="javascript:void(0);">
                                    Options
                                    <span id="schedule_display_options_down_allowconfirm">
                                    <img src="{!!asset('/img/arrow_down_single_gray.gif')!!}" border="0" alt="" class="menu_arrow_grn">
                                    </span>
                                    <span id="schedule_display_options_up_allowconfirm" style="display:none;">
                                    <img src="{!!asset('/img/arrow_up_single_gray.gif')!!}" border="0" alt="" class="menu_arrow_grn">
                                    </span>
                                    </a>
                                 </div>
                                 <div id="schedule_display_options_dialogconfirm" class="schedule_display_options_dialog js_schedule_display_options_dropdown" style="display:none;">
                                    <div class="schedule_display_options_dialog_ch">
                                       <input type="checkbox" name="show_full_titleconfirm" id="show_full_titleconfirm" class="" value="1" onclick="showFullShortTitle('show_full_titleconfirm','schedule_confirm','confirm',false);"><label id="show_full_titleconfirm_label" for="show_full_titleconfirm" onMouseOver="this.style.color='#ff0000'" onMouseOut="this.style.color=''">             Show full subject
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </td>
                        </tr>
                     </table>
                  </div>
                  <script language="javascript" type="text/javascript">
                     <!--
                     var grn_schedule_navi_command_on;
                     
                     var grn_schedule_navi_cal_url = "/schedule/command_navi_calendar_display?";
                     
                     var title_show_calendar = "Show calendar";
                     var title_hide_calendar = "Hide calendar";
                     
                     function grn_schedule_navi_cal()
                     {
                         var icon_tag = window.document.getElementById( 'showIcon-grn' );
                         if(YAHOO.util.Dom.hasClass('showIcon-grn', 'showIconOff-grn'))
                         {
                             jQuery('#schedule_calendar').show();
                     
                             jQuery('#wait_image').show();
                             if(window.document.getElementById( 'subCalendar-grn-image' ))
                             {
                                 jQuery('#subCalendar-grn-image').hide();
                             }
                             
                             grn_schedule_navi_command_on = true;
                             grn_schedule_send_req('on');
                             
                             YAHOO.util.Dom.removeClass('showIcon-grn', 'showIconOff-grn');
                             YAHOO.util.Dom.addClass('showIcon-grn', 'showIconOn-grn');
                             
                             icon_tag.setAttribute("title", title_hide_calendar);
                         }
                         else
                         {
                             if(YAHOO.util.Dom.hasClass('showIcon-grn', 'showIconOn-grn'))
                             {
                                 jQuery('#schedule_calendar').hide();
                     
                                 grn_schedule_navi_command_on = false;
                                 grn_schedule_send_req('off');
                                 
                                 YAHOO.util.Dom.removeClass('showIcon-grn', 'showIconOn-grn');
                                 YAHOO.util.Dom.addClass('showIcon-grn', 'showIconOff-grn');
                                 
                                 icon_tag.setAttribute("title", title_show_calendar);
                             }
                         }
                     }
                     
                     function grn_schedule_send_req(navi_cal_display_flag)
                     {
                         var post_body = jQuery.param({navi_cal_display_flag:navi_cal_display_flag});
                         post_body += '&csrf_ticket=a9c162692e1902a4c4248db6d67db480';
                         var oj = new jQuery.ajax({
                                 url         : grn_schedule_navi_cal_url,
                                 type        : 'POST',
                                 data        : post_body,
                                 complete    : grn_schedule_onloaded
                             });
                     }
                     
                     function grn_schedule_onloaded(jqXHR)
                     {
                         var headers = jqXHR.getAllResponseHeaders();
                         var regex = /X-Cybozu-Error/i;
                         if( headers.match( regex ) )
                         {
                             document.body.innerHTML = jqXHR.responseText;
                             return false;
                         }
                     
                         jQuery('#wait_image').hide();
                         if(window.document.getElementById( 'subCalendar-grn-image' ))
                         {
                             jQuery('#subCalendar-grn-image').show();
                         }
                     
                         if (grn_schedule_navi_command_on)
                         {
                             jQuery('#navi_cal_label_on').show();
                             jQuery('#navi_cal_label_off').hide();
                         }
                         else
                         {
                             jQuery('#navi_cal_label_off').show();
                             jQuery('#navi_cal_label_on').hide();
                         }
                     }
                     
                     YAHOO.util.Event.addListener( window.document.getElementById( 'showIcon-grn' ), 'click', grn_schedule_navi_cal );
                     //-->
                     
                  </script>
                  <div class="divWrapper">
                     <table id="schedule_confirm" class="day_table scheduleWrapper scheduleWrapperTimezoneBar hideEventTitle tableFixed hideScheduleAbsent available_time_base">
                        {!!$html!!}
                     </table>
                  </div>
                  <div class="none">&nbsp;</div>
               </div>
               <!--view_end--->
            </div>
         </form>
         <script language="JavaScript" type="text/javascript">
            <!--
                window.focus();
            //-->
         </script>
         <div class="clear_both_1px">&nbsp;</div>
         <div id="footer" class="footer_grn">
            <table width="100%">
               <tr>
                  <td nowrap><a href="#" onclick="window.close();"><img src="{!!asset('/img/close20.gif')!!}" border="0" alt="">Close</a></td>
               </tr>
            </table>
         </div>
      </div>
      <!--body_end-->
      
      </div>
   </body>
</html>