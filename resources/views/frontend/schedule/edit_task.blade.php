@extends('frontend.layouts.create_schedule')
@section('content')
<div class="global_navi" role="heading" aria-level="2" style="border:none;" style="border:none;margin-bottom: 20px;">
   <a href="{{route('frontend.schedule.index')}}" style="height: 34px;vertical-align: middle;line-height: 50px;display: inline-block;padding: 0px 20px;font-size: 20px;border-right: 1px solid #333;margin-top: 10px;line-height: 4px;color: #333;">
   <img src="{{asset('/img/schedule20.gif')}}" border="0" alt="" style="margin-bottom: 10px;width: 30px;">
   Scheduler
   </a>
   <span class="globalNavi-item-last-grn" style="padding: 0 15px;top: 6px;font-size: 20px;color: #fe7701;font-weight: 400;">
   Edit Apointerment
   </span>
</div>
<div class="mainarea ">
   <div class="mainareaSchedule-grn">
      <script src="{{asset('/js/schedules.js')}}" type="text/javascript"></script>
      <div><span class="bold">{{date('l, F d, Y')}}</span></div>
      <h2 style="display:inline-block;" class="schedule">Edit appointment</h2>
      <form id="schedule/banner_modify" class="js_customization_form" name="schedule/banner_modify" method="post" action="/schedule/command_modify?">
         <input type="hidden" name="csrf_ticket" value="a309d275287508333ab98796f2abbd14">
         <input type="hidden" name="_token" value="{{ csrf_token() }}" />
         <input type="hidden" name="pattern" value="4" />
         <div class="js_customization_schedule_header_space"></div>
         <input type="hidden" name="dummy" value="0">
         <table class="std_form" style="border-collapse: separate;">
            <tbody>
               <tr>
                  <th>Date</th>
                  <td>
                     <div class="explanation mTop3 mBottom5">You can specify appointments with the period.</div>
                     <script language="JavaScript" type="text/javascript">
                        function change_enddate()
                        {
                            form = document.forms["schedule/banner_modify"];
                            cb_ui_select_date_change_enddate( form );
                        }
                        //-->
                     </script>
                     <script lang="javascript" type="text/javascript">
                        <!--
                        var wday_name = new Array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
                        var openIds = new Array(0);
                        //-->
                     </script>
                     <script lang="javascript" type="text/javascript" src="{{asset('/js/select_date.js')}}"></script>
                     <script language="javascript" type="text/javascript"><!--
                        var g_arrayHolidays = new Array();
                        g_arrayHolidays[0] = "2011-01-01";
                        g_arrayHolidays[1] = "2011-01-10";
                        g_arrayHolidays[2] = "2011-02-11";
                        g_arrayHolidays[3] = "2011-03-21";
                        g_arrayHolidays[4] = "2011-04-29";
                        g_arrayHolidays[5] = "2011-05-03";
                        g_arrayHolidays[6] = "2011-05-04";
                        g_arrayHolidays[7] = "2011-05-05";
                        g_arrayHolidays[8] = "2011-07-18";
                        g_arrayHolidays[9] = "2011-09-19";
                        g_arrayHolidays[10] = "2011-09-23";
                        g_arrayHolidays[11] = "2011-10-10";
                        g_arrayHolidays[12] = "2011-11-03";
                        g_arrayHolidays[13] = "2011-11-23";
                        g_arrayHolidays[14] = "2011-12-23";
                        g_arrayHolidays[15] = "2012-01-01";
                        g_arrayHolidays[16] = "2012-01-02";
                        g_arrayHolidays[17] = "2012-01-09";
                        g_arrayHolidays[18] = "2012-02-11";
                        g_arrayHolidays[19] = "2012-03-20";
                        g_arrayHolidays[20] = "2012-04-29";
                        g_arrayHolidays[21] = "2012-04-30";
                        g_arrayHolidays[22] = "2012-05-03";
                        g_arrayHolidays[23] = "2012-05-04";
                        g_arrayHolidays[24] = "2012-05-05";
                        g_arrayHolidays[25] = "2012-07-16";
                        g_arrayHolidays[26] = "2012-09-17";
                        g_arrayHolidays[27] = "2012-09-22";
                        g_arrayHolidays[28] = "2012-10-08";
                        g_arrayHolidays[29] = "2012-11-03";
                        g_arrayHolidays[30] = "2012-11-23";
                        g_arrayHolidays[31] = "2012-12-23";
                        g_arrayHolidays[32] = "2012-12-24";
                        g_arrayHolidays[33] = "2013-01-01";
                        g_arrayHolidays[34] = "2013-01-14";
                        g_arrayHolidays[35] = "2013-02-11";
                        g_arrayHolidays[36] = "2013-03-20";
                        g_arrayHolidays[37] = "2013-04-29";
                        g_arrayHolidays[38] = "2013-05-03";
                        g_arrayHolidays[39] = "2013-05-04";
                        g_arrayHolidays[40] = "2013-05-05";
                        g_arrayHolidays[41] = "2013-05-06";
                        g_arrayHolidays[42] = "2013-07-15";
                        g_arrayHolidays[43] = "2013-09-16";
                        g_arrayHolidays[44] = "2013-09-23";
                        g_arrayHolidays[45] = "2013-10-14";
                        g_arrayHolidays[46] = "2013-11-03";
                        g_arrayHolidays[47] = "2013-11-04";
                        g_arrayHolidays[48] = "2013-11-23";
                        g_arrayHolidays[49] = "2013-12-23";
                        g_arrayHolidays[50] = "2014-01-01";
                        g_arrayHolidays[51] = "2014-01-13";
                        g_arrayHolidays[52] = "2014-02-11";
                        g_arrayHolidays[53] = "2014-03-21";
                        g_arrayHolidays[54] = "2014-04-29";
                        g_arrayHolidays[55] = "2014-05-03";
                        g_arrayHolidays[56] = "2014-05-04";
                        g_arrayHolidays[57] = "2014-05-05";
                        g_arrayHolidays[58] = "2014-05-06";
                        g_arrayHolidays[59] = "2014-07-21";
                        g_arrayHolidays[60] = "2014-09-15";
                        g_arrayHolidays[61] = "2014-09-23";
                        g_arrayHolidays[62] = "2014-10-13";
                        g_arrayHolidays[63] = "2014-11-03";
                        g_arrayHolidays[64] = "2014-11-23";
                        g_arrayHolidays[65] = "2014-11-24";
                        g_arrayHolidays[66] = "2014-12-23";
                        g_arrayHolidays[67] = "2015-01-01";
                        g_arrayHolidays[68] = "2015-01-12";
                        g_arrayHolidays[69] = "2015-02-11";
                        g_arrayHolidays[70] = "2015-03-21";
                        g_arrayHolidays[71] = "2015-04-29";
                        g_arrayHolidays[72] = "2015-05-03";
                        g_arrayHolidays[73] = "2015-05-04";
                        g_arrayHolidays[74] = "2015-05-05";
                        g_arrayHolidays[75] = "2015-05-06";
                        g_arrayHolidays[76] = "2015-07-20";
                        g_arrayHolidays[77] = "2015-09-21";
                        g_arrayHolidays[78] = "2015-09-22";
                        g_arrayHolidays[79] = "2015-09-23";
                        g_arrayHolidays[80] = "2015-10-12";
                        g_arrayHolidays[81] = "2015-11-03";
                        g_arrayHolidays[82] = "2015-11-23";
                        g_arrayHolidays[83] = "2015-12-23";
                        
                        var g_arrayWorkdays = new Array();
                        
                        function cb_ui_select_date_init_date_select( form_name, year, month, day, prefix)
                        {
                            var year_select = document.forms[form_name].elements[prefix+"year"];
                            if(year_select == undefined) return false;
                            var month_select = document.forms[form_name].elements[prefix+"month"];
                            if(month_select == undefined) return false;
                            var day_select = document.forms[form_name].elements[prefix+"day"];
                            if(day_select == undefined) return false;
                        
                            var year_options = year_select.options;
                            var month_options = month_select.options;
                            var day_options = day_select.options;
                            var select_year = 0;
                        
                            for(var i = 0 ; i <= year_options.length - 1 ; i++)
                            {
                                if(year_options[i].value == year)
                                {
                                    year_options[i].selected = true;
                                    select_year = 1;
                                }
                            }
                        
                            if( ! select_year)
                            {
                                if(year_options[0].value > year)
                                {
                                    var o = document.createElement( "OPTION" );
                                    year_options.add(o);
                                    for(var i = year_options.length - 1 ; i > 0 ; i --)
                                    {
                                        var dst = year_options[i];
                                        var src = year_options[i - 1];
                                        dst.value = src.value;
                                        dst.text = src.text;
                                        dst.selected = false;
                                    }
                                    year_options[0].value = year;
                                    year_options[0].text = year + ' year';
                                    year_options[0].selected = true;
                                }
                                else
                                {
                                    var o = document.createElement( "OPTION" );
                                    o.value = year;
                                    o.text = year + ' year';
                                    o.selected = true;
                                    year_options.add( o );
                                }
                            }
                        
                            for(var i = 0 ; i <= month_options.length - 1 ; i++)
                            {
                                if(month_options[i].value == month)
                                {
                                    month_options[i].selected = true;
                                }
                            }
                        
                            var start_date = new Date(year, month - 1, 1);
                            var wday = start_date.getDay();
                            var last_day = cb_ui_select_date_get_last_day(year, month);
                            var offset = 0;
                            if( day_options.length > 0 && day_options[0].value < 1 )
                            {
                                offset = 1;
                                day_options[0].className = "";
                                day_options[0].text = "--";
                            }
                        
                            for(i = 0 ; i < last_day ; i ++)
                            {
                                day_option = i + 1;
                                opt_idx = i + offset;
                        
                                if(opt_idx >= day_options.length)
                                {
                                    cb_ui_select_date_add_option(day_options, day_option);
                                }
                                day_options[opt_idx].value = day_option;
                        
                                if(wday == 0)
                                {
                                    day_options[opt_idx].className = "s_date_sunday";
                                }
                                else if(wday == 6)
                                {
                                    day_options[opt_idx].className = "s_date_saturday";
                                }
                                else
                                {
                                    day_options[opt_idx].className = "";
                                }
                        
                                if(g_arrayHolidays != null && g_arrayHolidays.length > 0)
                                {
                                    if(cb_ui_select_date_check_holiday(g_arrayHolidays, year, month, day_option))
                                    {
                                        day_options[opt_idx].className = "s_date_holiday";
                                    }
                                }
                        
                                day_options[opt_idx].text = day_option + "(" + wday_name[wday] + ")";
                                if(day_options[opt_idx].value == day)
                                {
                                    day_options[opt_idx].selected = true;
                                }
                        
                                wday ++;
                                if(wday > 6)
                                {
                                    wday = 0;
                                }
                            }
                            while((last_day+offset) < day_options.length)
                            {
                                if(day_options.remove)
                                {
                                    day_options.remove(day_options.length - 1);
                                }
                                else
                                {
                                    day_options[day_options.length - 1] = null;
                                }
                            }
                        
                            if ( year == "1970" && month == "1" && day_options[offset].value == "1")
                            {
                                if(day_options.remove)
                                {
                                    day_options.remove(offset);
                                    day_options.remove(offset);
                                }
                                else
                                {
                                    day_options[offset] = null;
                                    day_options[offset] = null;
                                }
                            }
                              
                        }
                        var G_yearUnit = "";
                        
                        function open_iframe (form_name, prefix, frame_src)
                        {
                            if (form_name == null || form_name == "")
                            {
                                var select_year  = document.getElementById(prefix+"year");
                                var select_month = document.getElementById(prefix+"month");
                                var select_day   = document.getElementById(prefix+"day");
                            }
                            else
                            {
                                form = document.forms[form_name];
                                var select_year  = form.elements[prefix+"year"];
                                var select_month = form.elements[prefix+"month"];
                                var select_day   = form.elements[prefix+"day"];
                            }
                        
                            if(!select_year.disabled)
                            {
                                var year = select_year.options[select_year.selectedIndex].value;
                                var month = select_month.options[select_month.selectedIndex].value;
                                var day = select_day.options[select_day.selectedIndex].value;
                                frame_src = frame_src + '&date=' + year + '-' + month + '-' + day;
                        
                                var id = form_name+prefix+"SetDateCal";
                                for(i=0; i < openIds.length; i++)
                                {
                                    if (openIds[i] != id)
                                    {
                                        e = document.getElementById(openIds[i]);
                                        if( e && e.style )
                                        {
                                            if( e.style.display == "" )
                                            {
                                                e.style.display = "none";
                                            }
                                        }
                                    }
                                }
                                var f = document.getElementById(id);
                                if(f && f.src)
                                {
                                    f.src = frame_src;
                                }
                                cb_ui_select_date_display_calendar(id);
                                openIds = new Array(id);
                                if( typeof parent.update_back_step == 'function' )
                                {
                                    parent.update_back_step();
                                }
                            }
                        }
                        //-->
                     </script>
                     <select id="start_month" name="start_month" onchange="cb_ui_select_date_init_day(this.form, 'start_');change_enddate();">
                     {!!$start_month_html!!}
                     </select>
                     <select id="start_day" name="start_day" onchange="change_enddate();">
                     {!!$start_day_html!!}
                     </select>
                     <select id="start_year" name="start_year" onchange="cb_ui_select_date_init_day(this.form, 'start_');cb_ui_select_date_reset_year_range(this.form, 'start_');change_enddate();">
                     {!!$start_year_html!!}
                     </select>
                     <script language="javascript" type="text/javascript"><!--
                        function reinit_day()
                        {
                            var _prefix = "start_";
                            var _form = document.forms["schedule/banner_modify"];
                            var _select_day = _form.elements[_prefix + "day"];
                            if(_select_day != null)
                            {
                                var _day = _select_day.options[_select_day.selectedIndex].value;
                        
                                cb_ui_select_date_init_day(_form, _prefix);
                        
                                for(i = 0 ; i < _select_day.options.length ; i ++)
                                {
                                    if(_select_day.options[i].value == _day) 
                                    {
                                        _select_day.options[i].selected = true;
                                    }
                                }
                            }
                        }
                        
                        if (window.addEventListener)
                        {
                            window.addEventListener('load', reinit_day, false);
                        }
                        else if (window.attachEvent)
                        {
                            window.attachEvent('onload', reinit_day);
                        }
                        
                        //-->
                     </script><a name="" style="cursor: pointer"><img src="{{asset('/img/calendar20.gif')}}" border="0" align="absmiddle" style="cursor:hand;" onclick="open_iframe(&quot;schedule/banner_modify&quot;, &quot;start_&quot;, &quot;/schedule/popup_calendar?prefix=start_&amp;form_name=schedule/banner_modify&amp;on_change=change_enddate();&quot;);" title="To select a date in the past three years or more, use the date selection calendar."></a>
                     <iframe id="schedule/banner_modifystart_SetDateCal" frameborder="no" scrolling="no" style="display:none; position:absolute; width:308px; height:17.5em;z-index:10;" src=""></iframe> - 
                     <select id="end_month" name="end_month" onchange="cb_ui_select_date_init_day(this.form, 'end_');">
                     {!!$end_month_html!!}
                     </select>
                     <select id="end_day" name="end_day">
                     {!!$end_day_html!!}
                     </select>
                     <select id="end_year" name="end_year" onchange="cb_ui_select_date_init_day(this.form, 'end_');cb_ui_select_date_reset_year_range(this.form, 'end_');">
                     {!!$end_year_html!!}
                     </select>
                     <script language="javascript" type="text/javascript"><!--
                        function reinit_day()
                        {
                            var _prefix = "end_";
                            var _form = document.forms["schedule/banner_modify"];
                            var _select_day = _form.elements[_prefix + "day"];
                            if(_select_day != null)
                            {
                                var _day = _select_day.options[_select_day.selectedIndex].value;
                        
                                cb_ui_select_date_init_day(_form, _prefix);
                        
                                for(i = 0 ; i < _select_day.options.length ; i ++)
                                {
                                    if(_select_day.options[i].value == _day) 
                                    {
                                        _select_day.options[i].selected = true;
                                    }
                                }
                            }
                        }
                        
                        if (window.addEventListener)
                        {
                            window.addEventListener('load', reinit_day, false);
                        }
                        else if (window.attachEvent)
                        {
                            window.attachEvent('onload', reinit_day);
                        }
                        
                        //-->
                     </script><a name="" style="cursor: pointer"><img src="{{asset('/img/calendar20.gif')}}" border="0" align="absmiddle" style="cursor:hand;" onclick="open_iframe(&quot;schedule/banner_modify&quot;, &quot;end_&quot;, &quot;/schedule/popup_calendar?prefix=end_&amp;form_name=schedule/banner_modify&amp;on_change=change_enddate();&quot;);" title="To select a date in the past three years or more, use the date selection calendar."></a>
                     <iframe id="schedule/banner_modifyend_SetDateCal" frameborder="no" scrolling="no" style="display:none; position:absolute; width:308px; height:17.5em;z-index:10;" src=""></iframe>
                     <div><span id="span_time_span" class="margin_top sub_explanation"><span class="attention" id="invalid_date" style="display:none;">The end date must follow the start date.</span></span></div>
                     <div id="another_timezone" class="link_timezone"><span id="timezone_to_link"><span id="current_timezone">(UTC+09:00) Tokyo</span><span>&nbsp;→&nbsp;</span></span><a id="opne_timezone_dialog" href="javascript:;" onclick="GRN_ScheduleSelectTimezone.openTimezoneDialog();">Other time zones</a></div>
                     <script type="text/javascript" src="{{asset('/js/schedule_tz.js')}}"></script>
                     <div class="overlay" style="display: none; width: 1903px; height: 1171px;" id="background"></div>
                     <div id="timezone_dialog" class="msgbox center" style="width:auto;display:none;">
                        <div id="timezone_title" class="title">Select time zone<a id="closeRelationDialog" style="position: absolute; right: 5px;top:5px;text-decoration:none;" onclick="GRN_ScheduleSelectTimezone.closeTimezoneDialog();" href="javascript:;"><img src="{{asset('/img/close20.gif')}}" border="0" alt=""></a></div>
                        <div id="timezone_content" class="content" style="width:450px;">
                           <div class="sub_text">Select time zone you want to apply.</div>
                           <div id="timezone_block" class="timezone_block">
                              <div id="div_timezone" class="select_timezone">
                                 <div id="timezone_candidate-region-block" class="div_select_timezone">
                                    <select id="timezone_candidate-region" name="timezone_candidate-region" onchange="GRN_System_I18N_SelectTimezone.switchTimezoneRegion(this.options[this.selectedIndex].value, 'timezone_candidate');" class="select_timezone">
                                       <option value="(Favourite)">Frequently-used time zones</option>
                                       <option value="(All)">(All)</option>
                                       <option value="Etc">Other</option>
                                       <option value="Pacific">Pacific</option>
                                       <option value="America">America</option>
                                       <option value="Atlantic">Atlantic</option>
                                       <option value="Europe">Europe</option>
                                       <option value="Africa">Africa</option>
                                       <option value="Asia">Asia</option>
                                       <option value="Indian">Indian</option>
                                       <option value="Australia">Australia</option>
                                    </select>
                                 </div>
                                 <div id="all-timezone_candidate-block" style="display:none" class="div_select_timezone">
                                    <select id="all-timezone_candidate" name="all-timezone_candidate" class="select_timezone">
                                       <option value="Etc/GMT+12">(UTC-12:00) UTC-12</option>
                                       <option value="Etc/GMT+11">(UTC-11:00) UTC-11</option>
                                       <option value="Etc/GMT+10">(UTC-10:00) UTC-10</option>
                                       <option value="Pacific/Honolulu">(UTC-10:00) Honolulu</option>
                                       <option value="America/Anchorage">(UTC-09:00) Anchorage</option>
                                       <option value="Etc/GMT+9">(UTC-09:00) UTC-9</option>
                                       <option value="America/Los_Angeles">(UTC-08:00) Los Angeles</option>
                                       <option value="America/Tijuana">(UTC-08:00) Tijuana</option>
                                       <option value="Etc/GMT+8">(UTC-08:00) UTC-8</option>
                                       <option value="America/Chihuahua">(UTC-07:00) Chihuahua</option>
                                       <option value="America/Denver">(UTC-07:00) Denver</option>
                                       <option value="America/Phoenix">(UTC-07:00) Phoenix</option>
                                       <option value="Etc/GMT+7">(UTC-07:00) UTC-7</option>
                                       <option value="America/Chicago">(UTC-06:00) Chicago</option>
                                       <option value="America/Guatemala">(UTC-06:00) Guatemala</option>
                                       <option value="America/Mexico_City">(UTC-06:00) Mexico City</option>
                                       <option value="America/Regina">(UTC-06:00) Regina</option>
                                       <option value="Etc/GMT+6">(UTC-06:00) UTC-6</option>
                                       <option value="America/Bogota">(UTC-05:00) Bogota</option>
                                       <option value="America/Indiana/Indianapolis">(UTC-05:00) Indianapolis, Indiana</option>
                                       <option value="America/New_York">(UTC-05:00) New York</option>
                                       <option value="Etc/GMT+5">(UTC-05:00) UTC-5</option>
                                       <option value="America/Asuncion">(UTC-04:00) Asunción</option>
                                       <option value="America/Caracas">(UTC-04:00) Caracas</option>
                                       <option value="America/Cuiaba">(UTC-04:00) Cuiaba</option>
                                       <option value="America/Halifax">(UTC-04:00) Halifax</option>
                                       <option value="America/La_Paz">(UTC-04:00) La Paz</option>
                                       <option value="America/Manaus">(UTC-04:00) Manaus</option>
                                       <option value="America/Santiago">(UTC-04:00) Santiago</option>
                                       <option value="Etc/GMT+4">(UTC-04:00) UTC-4</option>
                                       <option value="America/St_Johns">(UTC-03:30) St Johns</option>
                                       <option value="America/Argentina/Buenos_Aires">(UTC-03:00) Buenos Aires</option>
                                       <option value="America/Cayenne">(UTC-03:00) Cayenne</option>
                                       <option value="America/Godthab">(UTC-03:00) Godthab</option>
                                       <option value="America/Montevideo">(UTC-03:00) Montevideo</option>
                                       <option value="America/Sao_Paulo">(UTC-03:00) Sao Paulo</option>
                                       <option value="Etc/GMT+3">(UTC-03:00) UTC-3</option>
                                       <option value="Atlantic/South_Georgia">(UTC-02:00) South Georgia</option>
                                       <option value="Etc/GMT+2">(UTC-02:00) UTC-2</option>
                                       <option value="Atlantic/Azores">(UTC-01:00) Azores</option>
                                       <option value="Atlantic/Cape_Verde">(UTC-01:00) Cape Verde</option>
                                       <option value="Etc/GMT+1">(UTC-01:00) UTC-1</option>
                                       <option value="Atlantic/Reykjavik">(UTC+00:00) Reykjavik</option>
                                       <option value="Europe/London">(UTC+00:00) London</option>
                                       <option value="UTC">(UTC+00:00) UTC</option>
                                       <option value="Africa/Casablanca">(UTC+01:00) Casablanca</option>
                                       <option value="Africa/Lagos">(UTC+01:00) Lagos</option>
                                       <option value="Etc/GMT-1">(UTC+01:00) UTC+1</option>
                                       <option value="Europe/Berlin">(UTC+01:00) Berlin</option>
                                       <option value="Europe/Budapest">(UTC+01:00) Budapest</option>
                                       <option value="Europe/Paris">(UTC+01:00) Paris</option>
                                       <option value="Europe/Warsaw">(UTC+01:00) Warsaw</option>
                                       <option value="Africa/Cairo">(UTC+02:00) Cairo</option>
                                       <option value="Africa/Johannesburg">(UTC+02:00) Johannesburg</option>
                                       <option value="Africa/Windhoek">(UTC+02:00) Windhoek</option>
                                       <option value="Asia/Amman">(UTC+02:00) Amman</option>
                                       <option value="Asia/Beirut">(UTC+02:00) Beirut</option>
                                       <option value="Asia/Damascus">(UTC+02:00) Damascus</option>
                                       <option value="Asia/Jerusalem">(UTC+02:00) Jerusalem</option>
                                       <option value="Etc/GMT-2">(UTC+02:00) UTC+2</option>
                                       <option value="Europe/Kiev">(UTC+02:00) Kiev</option>
                                       <option value="Africa/Nairobi">(UTC+03:00) Nairobi</option>
                                       <option value="Asia/Baghdad">(UTC+03:00) Baghdad</option>
                                       <option value="Asia/Riyadh">(UTC+03:00) Riyadh</option>
                                       <option value="Etc/GMT-3">(UTC+03:00) UTC+3</option>
                                       <option value="Europe/Istanbul">(UTC+03:00) Istanbul</option>
                                       <option value="Europe/Minsk">(UTC+03:00) Minsk</option>
                                       <option value="Europe/Moscow">(UTC+03:00) Moscow</option>
                                       <option value="Asia/Tehran">(UTC+03:30) Tehran</option>
                                       <option value="Asia/Baku">(UTC+04:00) Baku</option>
                                       <option value="Asia/Dubai">(UTC+04:00) Dubai</option>
                                       <option value="Asia/Tbilisi">(UTC+04:00) Tbilisi</option>
                                       <option value="Asia/Yerevan">(UTC+04:00) Yerevan</option>
                                       <option value="Etc/GMT-4">(UTC+04:00) UTC+4</option>
                                       <option value="Indian/Mauritius">(UTC+04:00) Mauritius</option>
                                       <option value="Asia/Kabul">(UTC+04:30) Kabul</option>
                                       <option value="Asia/Karachi">(UTC+05:00) Karachi</option>
                                       <option value="Asia/Tashkent">(UTC+05:00) Tashkent</option>
                                       <option value="Asia/Yekaterinburg">(UTC+05:00) Yekaterinburg</option>
                                       <option value="Etc/GMT-5">(UTC+05:00) UTC+5</option>
                                       <option value="Asia/Colombo">(UTC+05:30) Colombo</option>
                                       <option value="Asia/Kolkata">(UTC+05:30) Kolkata</option>
                                       <option value="Asia/Kathmandu">(UTC+05:45) Kathmandu</option>
                                       <option value="Asia/Almaty">(UTC+06:00) Almaty</option>
                                       <option value="Asia/Dhaka">(UTC+06:00) Dhaka</option>
                                       <option value="Etc/GMT-6">(UTC+06:00) UTC+6</option>
                                       <option value="Asia/Yangon">(UTC+06:30) Yangon</option>
                                       <option value="Asia/Bangkok">(UTC+07:00) Bangkok</option>
                                       <option value="Asia/Krasnoyarsk">(UTC+07:00) Krasnoyarsk</option>
                                       <option value="Asia/Novosibirsk">(UTC+07:00) Novosibirsk</option>
                                       <option value="Etc/GMT-7">(UTC+07:00) UTC+7</option>
                                       <option value="Asia/Irkutsk">(UTC+08:00) Irkutsk</option>
                                       <option value="Asia/Shanghai">(UTC+08:00) Beijing</option>
                                       <option value="Asia/Singapore">(UTC+08:00) Singapore</option>
                                       <option value="Asia/Taipei">(UTC+08:00) Taipei</option>
                                       <option value="Asia/Ulaanbaatar">(UTC+08:00) Ulaanbaatar</option>
                                       <option value="Australia/Perth">(UTC+08:00) Perth</option>
                                       <option value="Etc/GMT-8">(UTC+08:00) UTC+8</option>
                                       <option value="Asia/Seoul">(UTC+09:00) Seoul</option>
                                       <option value="Asia/Tokyo" selected="">(UTC+09:00) Tokyo</option>
                                       <option value="Asia/Yakutsk">(UTC+09:00) Yakutsk</option>
                                       <option value="Etc/GMT-9">(UTC+09:00) UTC+9</option>
                                       <option value="Australia/Adelaide">(UTC+09:30) Adelaide</option>
                                       <option value="Australia/Darwin">(UTC+09:30) Darwin</option>
                                       <option value="Asia/Vladivostok">(UTC+10:00) Vladivostok</option>
                                       <option value="Australia/Brisbane">(UTC+10:00) Brisbane</option>
                                       <option value="Australia/Hobart">(UTC+10:00) Hobart</option>
                                       <option value="Australia/Sydney">(UTC+10:00) Sydney</option>
                                       <option value="Etc/GMT-10">(UTC+10:00) UTC+10</option>
                                       <option value="Pacific/Port_Moresby">(UTC+10:00) Port Moresby</option>
                                       <option value="Asia/Magadan">(UTC+11:00) Magadan</option>
                                       <option value="Etc/GMT-11">(UTC+11:00) UTC+11</option>
                                       <option value="Pacific/Guadalcanal">(UTC+11:00) Guadalcanal</option>
                                       <option value="Asia/Kamchatka">(UTC+12:00) Kamchatka</option>
                                       <option value="Etc/GMT-12">(UTC+12:00) UTC+12</option>
                                       <option value="Pacific/Auckland">(UTC+12:00) Auckland</option>
                                       <option value="Pacific/Fiji">(UTC+12:00) Fiji</option>
                                       <option value="Pacific/Apia">(UTC+13:00) Apia</option>
                                       <option value="Pacific/Tongatapu">(UTC+13:00) Tongatapu</option>
                                    </select>
                                 </div>
                                 <div id="timezone_candidate-block" class="div_select_timezone">
                                    <select id="timezone_candidate" name="timezone_candidate" class="select_timezone">
                                       <option value="Asia/Tokyo" selected="">(UTC+09:00) Tokyo</option>
                                       <option value="America/Los_Angeles">(UTC-08:00) Los Angeles</option>
                                       <option value="Asia/Singapore">(UTC+08:00) Singapore</option>
                                    </select>
                                 </div>
                                 <script type="text/javascript">
                                    var GRN_System_I18N_SelectTimezone = {
                                        allTimezoneOptions : (function(){
                                                                 var options = [],
                                                                     elements = document.getElementById('all-timezone_candidate').getElementsByTagName('option'),
                                                                     elementsLength = elements.length;
                                                                 for( var i=0; i<elementsLength; i+=1 ){
                                                                     options.push(elements[i].cloneNode(true));
                                                                 }
                                                                 return options;
                                                             }()),
                                        favouriteTimezoneOptions : (function(){
                                                                 var options = [],
                                                                     elements = document.getElementById('timezone_candidate').getElementsByTagName('option'),
                                                                     elementsLength = elements.length;
                                                                 for( var i=0; i<elementsLength; i+=1 ){
                                                                     options.push(elements[i].cloneNode(true));
                                                                 }
                                                                 return options;
                                                             }()),
                                        switchTimezoneRegion : function(region, key) {
                                            var options,
                                                max,
                                                regionLength = region.length;
                                            
                                            if( region === '(Favourite)' ){
                                                options = this.favouriteTimezoneOptions;
                                            }else{
                                                options = this.allTimezoneOptions;
                                            }
                                            max = options.length;
                                    
                                            document.getElementById(key).innerHTML = '';
                                            
                                            for( var i=0; i<max; i+=1 ){
                                                if( region === '(All)' || region === '(Favourite)' || region === options[i].value.substr(0, regionLength) || options[i].value === '' ){
                                                    document.getElementById(key).appendChild(options[i].cloneNode(true));
                                                }
                                            }
                                        }
                                    };
                                 </script>
                              </div>
                           </div>
                        </div>
                        <div id="timezone_dialog_buttons" class="command">
                           <span id="apply_timezone" class="button_grn_js button1_main_grn  button1_r_margin2_grn" onclick="GRN_ScheduleSelectTimezone.applyTimezone();" data-auto-disable=""><a href="javascript:void(0);" role="button">Apply</a></span><span id="cancel_timezone" class="button_grn_js button1_normal_grn" onclick="GRN_ScheduleSelectTimezone.closeTimezoneDialog();" data-auto-disable=""><a href="javascript:void(0);" role="button">Cancel</a></span>
                        </div>
                     </div>
                     <input type="hidden" id="timezone" name="timezone" value="Asia/Tokyo">
                     <input type="hidden" id="end_timezone" name="end_timezone" value="Asia/Tokyo">   
                  </td>
               </tr>
               <tr>
                  <th>Subject</th>
                  <td>
                    <div class="event_add_base_grn fleft">
                        <div class="fleft"><input type="text" name="title" size="80" maxlength="100" value="{{$schedule->title}}" onkeypress="return event.keyCode != 13;" style="margin-top: 0px;"></div>
                    </div>
                  </td>
               </tr>
               <tr valign="top">
                  <th>Attendees</th>
                  <td>
                     <style type="text/css">
                        #spinner-loading-CGID {
                        background-image: url( {{asset('/img/spinner.gif')}});
                        }
                     </style>
                     <script src="{{asset('/js/pubsub.js')}}" type="text/javascript"></script>
                     <script src="{{asset('/js/member_add.js')}}" type="text/javascript"></script>
                     <script src="{{asset('/js/member_select_list.js')}}" type="text/javascript"></script>
                     <script src="{{asset('/js/pulldown_menu.js')}}" type="text/javascript"></script>
                     <script language="JavaScript" type="text/javascript">
                        <!--
                        new grn.component.member_add.MemberAdd("member_select", "schedule/banner_modify", ["sUID"], "CID",
                                {
                                    categorySelectUrl: '/api/ajax_user_add_select_by_group',
                                    searchBoxOptions: {"is_use":true,"id_searchbox":"user","url":"/api/search_members_by_keyword","append_post_data":[]},
                                    pulldownPartsOptions: {"is_use":true,"pulldown_id":"CID_pulldown"},
                                    appId: "schedule",
                                    isCalendar: true,
                                    showGroupRole: true,
                                    includeOrg: "1",
                                    accessPlugin: true,
                                    accessPluginEncoded: "YToyOntzOjQ6Im5hbWUiO3M6ODoic2NoZWR1bGUiO3M6NjoicGFyYW1zIjthOjM6e3M6NjoiYWN0aW9uIjthOjM6e2k6MDtzOjQ6InJlYWQiO2k6MTtzOjM6ImFkZCI7aToyO3M6NjoibW9kaWZ5Ijt9czoxMjoibGF4X2V2YWx1YXRlIjtiOjE7czoxMjoic2Vzc2lvbl9uYW1lIjtzOjIyOiJzY2hlZHVsZS9iYW5uZXJfbW9kaWZ5Ijt9fQ==",
                                    pluginSessionName: "schedule/banner_modify",
                                    pluginDataName: "access_plugin",
                                    addOrgWithUsers: false,
                                    showOmitted: false,
                                    useCandidateSupportParts: false,
                                    operatorAddName: "",
                                    selectAllUsersInSearch: false            }
                        );
                        //-->
                     </script>
                     <input type="hidden" name="selected_users_sUID" value="">
                     <input type="hidden" name="selected_groups_sUID" value="">
                     <input type="hidden" name="selected_roles_sUID" value="">
                     <input type="hidden" name="for_redirect_at_non_command_page[]" value="">
                     <table class="table_plain_grn selectlist_base_grn">
                        <tbody>
                           <tr>
                              <td class="vAlignTop-grn" style="padding-left:0">
                                 <table class="table_plain_grn">
                                    <tbody>
                                       <tr>
                                          <td class="buttonSlectOrder-grn">
                                             <div id="sUID_order_top" class="mBottom10">
                                                <a class="order_top_grn" aria-label="Move to top" title="Move to top" href="javascript:void(0)"></a>
                                             </div>
                                             <div id="sUID_order_up" class="mBottom10">
                                                <a class="order_up_grn" aria-label="Move up" title="Move up" href="javascript:void(0)"></a>
                                             </div>
                                             <div id="sUID_order_down" class="mBottom10">
                                                <a class="order_down_grn" aria-label="Move down" title="Move down" href="javascript:void(0)"></a>
                                             </div>
                                             <div id="sUID_order_bottom" class="mBottom10">
                                                <a class="order_bottom_grn" aria-label="Move to bottom" title="Move to bottom" href="javascript:void(0)"></a>
                                             </div>
                                          </td>
                                          <td class="item_select">
                                             <div class="selectlist_area_grn">
                                                <span id="spinner_selectlist_sUID" style="display:none;position:absolute;z-index:1;"><img src="{{asset('/img/spinner.gif')}}" border="0" alt=""></span>
                                                <div id="selectlist_base_selectlist_sUID" aria-multiselectable="true" class="selectlist_grn selectlist_l_grn focus_grn" tabindex="0" style="">
                                                   <ul id="ul_selectlist_sUID">
                                                      @foreach($schedule->member as $key=>$val)
                                                      <li id="selectlist_sUID_member_user_58" class="selectlist_sUID" data-value="{{$val->id}}" data-type="user" data-name="{{$val->full_name}}" data-code="brown" data-id="{{$val->full_name}}" data-group-path="" data-url="">
                                                         <span class="@if($schedule->uid == $val->id )selectlist_user_login_grn @else selectlist_user_grn @endif" aria-label="Login user" title="Login user"></span>
                                                         <span class="selectlist_text_grn">{{$val->full_name}}</span>
                                                      </li>
                                                      @endforeach 
                                                   </ul>
                                                   <span id="spinner_scroll_selectlist_sUID" style="display:none;"><img src="{{asset('/img/spinner.gif')}}" border="0" alt=""></span>
                                                </div>
                                             </div>
                                             <div class="textSub-grn mTop5"><a id="select_all_selectlist_sUID" class="mRight20" href="javascript:void(0);">Select all</a><a id="un_select_all_selectlist_sUID" style="display:none" class="mRight20" href="javascript:void(0);">Clear all</a></div>
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </td>
                              <td class="vAlignTop-grn item_right_left">
                                 <div class="buttonSelectMove-grn">
                                    <div class="mBottom15">
                                       <span class="aButtonStandard-grn">
                                       <a role="button" id="btn_add_sUID" style="padding-left:0;" href="javascript:void(0);">
                                       <span class="icon-buttonArrowLeft-grn"></span><span class="aButtonText-grn">Add</span>
                                       </a>
                                       </span>
                                    </div>
                                    <div>
                                       <span class="aButtonStandard-grn">
                                       <a role="button" id="btn_rmv_sUID" style="padding-right:0;" href="javascript:void(0);">
                                       <span class="aButtonText-grn">Remove</span><span class="icon-buttonArrowRightBehind-grn"></span>
                                       </a>
                                       </span>
                                    </div>
                                 </div>
                              </td>
                              <td class="vAlignTop-grn">
                                 <div class="mTop3 mBottom7 clearFix-cybozu">
                                    <script src="{{asset('/js/search_box.js')}}" type="text/javascript"></script>
                                    <div class="search_navi">
                                       <div class="searchbox-grn">
                                          <div id="searchbox-cybozu-user" class="input-text-outer-cybozu searchbox-keyword-area searchbox_keyword_grn">
                                             <input class="input-text-cybozu prefix-grn" style="width:246px;" type="text" id="keyword_user" name="" autocomplete="off" value="User search" maxlength="">
                                             <button id="searchbox-submit-user" class="searchbox-submit-cybozu" type="button" title="Search" aria-label="Search"></button>
                                          </div>
                                          <div class="clear_both_0px"></div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- category select -->
                                 <link href="{{asset('assets/css/pulldown_menu.css')}}" rel="stylesheet" type="text/css">
                                 <div class="mBottom7 nowrap-grn">
                                    <dl id="CID_pulldown" class="selectmenu_grn selectmenu_base_grn">
                                       <dt><a href="javascript:void(0)" class="nowrap-grn"><span></span><span class="selectlist_selectmenu_item_grn pulldown_head"></span><span class="pulldownbutton_arrow_down_grn mLeft3"></span></a></dt>
                                       <dd>
                                          <div class="pulldown_menu_grn" style="display: none;">
                                             <ul>
                                                <li ><a href="javascript:void(0)"><span class="vAlignMiddle-grn">Tất cả </span></a></li>
                                                @foreach($department as $key=>$val)
                                                <li data-value="{{$val->id}}" ><a href="javascript:void(0)"><span class="vAlignMiddle-grn">{{$val->name}} </span></a></li>
                                                @endforeach
                                             </ul>
                                          </div>
                                       </dd>
                                    </dl>
                                    <a class="selectPulldownSub-grn" href="javascript:void(0);" onclick="javascript:popupWin('/grn/popup_member_select?plugin_session_name=schedule%2Fbanner_modify&amp;plugin_data_name=access_plugin&amp;is_post_message=&amp;selected_tid=&amp;form_name=schedule%2Fbanner_modify&amp;select_name=sUID&amp;app_id=schedule&amp;return_page=&amp;plid=&amp;system=0&amp;include_org=1&amp;system_display=0&amp;no_multiple=0&amp;send_cgi_parameter=0&amp;multi_apply=0&amp;require_role_tab=0&amp;is_calendar=1&amp;show_group_role=1&amp;require_dynamic_roles=0&amp;require_administrator_role=0','html','1014','675',0,0,1,1,0,1);" title="Select from all organizations"><img src="{{asset('/img/blankB16.png')}}" border="0" alt=""></a>
                                 </div>
                                 <!-- category select -->
                                 <div class="selectlist_area_grn">
                                    <span id="spinner_selectlist_CID" style="position: absolute; z-index: 1; display: none;"><img src="{{asset('/img/spinner.gif')}}" border="0" alt=""></span>
                                    <div id="selectlist_base_selectlist_CID" aria-multiselectable="true" class="selectlist_grn selectlist_r_grn" tabindex="0" style="">
                                       <ul id="ul_selectlist_CID">
                                          <li id="selectlist_CID_member_group_15" class="selectlist_CID" data-value="g15" data-code="1group" data-name="第1営業グループ" data-type="group"><span class="selectlist_cal_group_grn" aria-label="Organization's appointment" title="Organization's appointment"></span><span class="selectlist_text_grn">第1営業グループ</span></li>
                                          <li id="selectlist_CID_member_user_8-15" class="selectlist_CID" data-value="8:15" data-code="tobimatsu" data-name="飛松 生" data-type="user"><span class="selectlist_user_grn" aria-label="" title=""></span><span class="selectlist_text_grn">飛松 生</span></li>
                                          <li id="selectlist_CID_member_user_39-15" class="selectlist_CID" data-value="39:15" data-code="ookane" data-name="大兼政 晋一" data-type="user"><span class="selectlist_user_grn" aria-label="" title=""></span><span class="selectlist_text_grn">大兼政 晋一</span></li>
                                          <li id="selectlist_CID_member_user_15-15" class="selectlist_CID" data-value="15:15" data-code="matsumoto" data-name="松本 琴里" data-type="user"><span class="selectlist_user_grn" aria-label="" title=""></span><span class="selectlist_text_grn">松本 琴里</span></li>
                                          <li id="selectlist_CID_member_user_57-15" class="selectlist_CID" data-value="57:15" data-code="wigman" data-name="Joan Wigman" data-type="user"><span class="selectlist_user_grn" aria-label="" title=""></span><span class="selectlist_text_grn">Joan Wigman</span></li>
                                          <li id="selectlist_CID_member_user_58-15" class="selectlist_CID" data-value="58:15" data-code="brown" data-name="Foster Brown" data-type="user"><span class="selectlist_user_login_grn" aria-label="Login user" title="Login user"></span><span class="selectlist_text_grn">Foster Brown</span></li>
                                          <li id="selectlist_CID_member_user_59-15" class="selectlist_CID" data-value="59:15" data-code="ricky" data-name="Sheldon Ricky" data-type="user"><span class="selectlist_user_grn" aria-label="" title=""></span><span class="selectlist_text_grn">Sheldon Ricky</span></li>
                                          <li id="selectlist_CID_member_user_60-15" class="selectlist_CID" data-value="60:15" data-code="machold" data-name="Rikki Machold" data-type="user"><span class="selectlist_user_grn" aria-label="" title=""></span><span class="selectlist_text_grn">Rikki Machold</span></li>
                                          <li id="selectlist_CID_member_user_66-15" class="selectlist_CID" data-value="66:15" data-code="takada" data-name="mayo takada" data-type="user"><span class="selectlist_user_grn" aria-label="" title=""></span><span class="selectlist_text_grn">mayo takada</span></li>
                                       </ul>
                                       <span id="spinner_scroll_selectlist_CID" style="display:none;"><img src="{{asset('/img/spinner.gif')}}" border="0" alt=""></span>
                                    </div>
                                 </div>
                                 <div class="textSub-grn mTop5">
                                    <a id="select_all_selectlist_CID" class="mRight20" href="javascript:void(0);">Select all</a><a id="un_select_all_selectlist_CID" style="display:none" class="mRight20" href="javascript:void(0);">Clear all</a>    <script language="JavaScript" type="text/javascript">
                                       <!--
                                       var popupMember_CID_count = 0;
                                       function popupMember_CID(Aform)
                                       {
                                           if ( ! popupMember_CID_count || document.getElementById('form_CID[]') == null)
                                           {
                                               var p = document.getElementById('div_CID[]');
                                               var c = document.createElement('DIV');
                                               p.style.display = "";
                                               p.appendChild(c);
                                               c.innerHTML = '<iframe id="form_CID[]" name="form_CID[]" frameborder="no" scrolling="no" style="position:absolute; width:0em; height:0em;" src=""></iframe>';
                                           }
                                       
                                           popupWin("","user_list",1034,675,0,0,0,1,0,1);
                                           var url = '/grn/popup_user_list?';
                                           var html = '<!DOCTYPE html><html lang="en"><body><form method="post" target="user_list" action="' + url + '">';
                                           html += '<input type="hidden" name="system_display" value="0">';
                                           var form_frame = document.getElementById('form_CID[]').contentWindow.document;
                                           var instance = grn.component.member_select_list.get_instance("CID");
                                           var values = instance.getSelectedUsersValues();
                                           html += '<input type="hidden" name="cid" value="' + values.join(':') + '">';
                                           html += '</form></body></html>';
                                           form_frame.write( html );
                                           form_frame.close();
                                           form_frame.forms[0].submit();
                                           popupMember_CID_count = 1;
                                           if( !grn.browser.chrome && !grn.browser.safari && typeof update_back_step == 'function' )
                                           {
                                               update_back_step();
                                           }
                                           return true;
                                       }
                                       //-->
                                    </script>
                                    <div id="div_CID[]" style="display:none; position:absolute; width:0em; height:0em;">&nbsp;</div>
                                    <a href="javascript:void(0);" onclick="popupMember_CID();">User details</a>
                                 </div>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </td>
               </tr>
               <tr>
                  <th>Availability </th>
                  <td>
                     <script language="JavaScript" type="text/javascript">
                        <!--
                        function appendSingleSelection(form,name,src_html)
                        {
                          var html = src_html;
                          var box = form.elements[name];
                          if ( ! box ) return html;
                          var box_value = box.options[box.selectedIndex].value;
                          
                          if ( box_value.length < 1 ) return html;
                          html += '<input type="hidden" name="' + name + '" value="' + box_value + '">';
                          return html;
                        }
                        function appendMultipleSelection(form,name,src_html)
                        {
                          var html = src_html;
                          var box = form.elements[name + '[]'];
                          if ( ! box ) return html;
                          var box_options = box.options;
                          var count = 0;
                        
                          for ( i = 0; i < box_options.length; i++ )
                          {
                            var box_option = box_options[i];
                            if (name != 'sITEM' && name != 'sUID' && ! box_option.selected ) continue;
                            if ( box_option.value.length < 1 ) continue;
                            var box_option_value = box_option.value.split(':');
                            html += '<input type="hidden" name="' + name + '[' + (count++) + ']" value="' + box_option_value[0] + '">';
                          }
                          return html;
                        }
                        var openConfirm_count = 0;
                        function openConfirm(form, pagename)
                        {
                        
                         if ( ! openConfirm_count)
                         {
                        
                           var p = document.getElementById('form_frame');
                           var c = document.createElement('DIV');
                           p.style.display = "";
                           p.appendChild(c);
                           c.innerHTML = '<iframe id="form_frame" name="form_frame" frameborder="no" scrolling="no" style="position:absolute; width:0em; height:0em;" src=""></iframe>';
                        
                         }
                        
                          var popupWindow   = popupWin_returnWin("","popup",1034,675,0,0,0,1,0,1);
                          window.addEventListener('unload', function(){if(popupWindow && !popupWindow.closed){popupWindow.close();}});
                        
                          var form_frame = window.frames['form_frame'].document;
                          var url = '/schedule/confirm?';
                          var member_add = grn.component.member_add.get_instance('member_select');
                          var html = '<!DOCTYPE html><html lang="{$html_tag_lang|escape}"><body><form method="POST" target="popup" action="' + url + '">';
                          html += '<input type="hidden" name="session_no_use" value="1">';
                        
                          //single select
                          html = appendSingleSelection(form,'start_year',html);
                          html = appendSingleSelection(form,'start_month',html);
                          html = appendSingleSelection(form,'start_day',html);
                          html = appendSingleSelection(form,'start_hour',html);
                          html = appendSingleSelection(form,'start_minute',html);
                          html = appendSingleSelection(form,'end_year',html);
                          html = appendSingleSelection(form,'end_month',html);
                          html = appendSingleSelection(form,'end_day',html);
                          html = appendSingleSelection(form,'end_hour',html);
                          html = appendSingleSelection(form,'end_minute',html);
                          //multiple select
                          html += member_add.getFirstMemberSelectList().makeHTMLTextForConfirmView(false);
                          html += member_add.candidateList.makeHTMLTextForConfirmView(true);
                          if (grn.component.facility_add) {
                            var facility_add = grn.component.facility_add.get_instance('facility_select');
                            html += facility_add.facilityList.makeHTMLTextForConfirmView(false);
                            html += facility_add.candidateList.makeHTMLTextForConfirmView(true);
                          }
                        
                        
                          if ( form.elements['uid'] )
                          {
                            html += '<input type="hidden" name="uid" value="' + form.elements['uid'].value + '">';
                          }
                          if ( form.elements['gid'] )
                          {
                            html += '<input type="hidden" name="gid" value="' + encodeURIComponent(form.elements['gid'].value) + '">';
                          }
                          if ( form.elements['bdate'] )
                          {
                            html += '<input type="hidden" name="bdate" value="' + form.elements['bdate'].value + '">';
                          }
                          html += '<input type="hidden" name="parent_page_name" value="' + pagename + '">';
                          html += '<input type="hidden" name="timezone" value="' + form.elements['timezone'].value + '">';
                          html += '<input type="hidden" name="end_timezone" value="' + form.elements['end_timezone'].value + '">';
                        
                          html += '</form></body></html>';
                          form_frame.write( html );
                          form_frame.close();
                          form_frame.forms[0].submit();
                          openConfirm_count = 1;
                          if( !grn.browser.chrome && !grn.browser.safari && typeof update_back_step == 'function' )
                          {
                              update_back_step();
                          }
                          return false;
                        }
                        //-->
                     </script>
                     <div id="form_frame" style="display:none; position:absolute; width:0em; height:0em;">&nbsp;</div>
                     <script language="JavaScript" type="text/javascript">
                        var form_name = document.getElementById('schedule/banner_modify');
                        
                     </script>
                     <span class="aButtonHighlight-grn">
                     <a href="javascript:void(0);" onclick="javascript:openConfirm(form_name, 'banner_modify');">
                     <span class="icon-blankB-grn"></span>
                     <span class="aButtonText-grn">Check availability of attendees and facilities</span>
                     </a>
                     </span>   
                  </td>
               </tr>
               <tr valign="top">
                  <th>Company information</th>
                  <td>
                     <a href="javascript:display_on_off('address_form')">Add company information▼</a>
                      <table class="address" id="address_form" style="display:none;">
                        <colgroup>
                           <col width="5%">
                           <col width="95%">
                        </colgroup>
                        <tr valign="top">
                           <th nowrap>Company name</th>
                           <td><input  type="text" name="company_name" size="52" maxlength="100" value="{{$schedule->company_name}}" onKeyPress="return event.keyCode != 13;"></td>
                        </tr>
                        <tr valign="top">
                           <th nowrap>Address</th>
                           <td><input  type="text" name="physical_address" size="52" maxlength="65535" value="{{$schedule->physical_address}}" onKeyPress="return event.keyCode != 13;"></td>
                        </tr>
                        <tr valign="top">
                           <th nowrap>Company phone number</th>
                           <td><input  type="text" name="company_telephone_number" size="50" maxlength="100" value="{{$schedule->company_telephone_number}}" onKeyPress="return event.keyCode != 13;"></td>
                        </tr>
                     </table>
                  </td>
               </tr>
               <tr valign="top">
                  <th>Notes</th>
                  <td><textarea id="textarea_id" name="memo" class="autoexpand" wrap="virtual" role="form" cols="65" rows="5" style="white-space: pre-wrap;border: 1px solid rgb(153, 153, 153)">123</textarea><textarea style="white-space: pre-wrap; left: -9999px; position: absolute; top: 0px; height: 121px;" id="dummy_textarea_textarea_id" class="" wrap="virtual" cols="65" rows="5" disabled="">123</textarea></td>
               </tr>
               <tr valign="top">
                  <th nowrap="">Attachments</th>
                  <td>
                     <script language="Javascript" type="text/javascript"><!--
                        __upload_ticket = "ecf3ba29b31cc27d9504bd11c393a01cacd9bb51791f6b9f43752305ffff625e";
                        __upload_files_url = '/api/uploaded_files?';
                        
                        __upload_msg_error          = "Failed to upload the file. (Error: ";
                        __upload_msg_error_suffix   = ")";
                        __upload_msg_filesizeover_1 = "Cannot upload the file. The file size exceeds the size limit of ";
                        __upload_msg_filesizeover_2 = ".";
                        __upload_msg_zerobyte_file  = "Cannot upload the 0 byte file.";
                        __upload_msg_cancel         = "Cancel";
                        __upload_msg_confirm1       = "Failed to attach files.";
                        __upload_msg_confirm2       = "Only the files which have already uploaded will be attached.";
                        
                        //-->
                     </script>
                     <script language="Javascript" type="text/javascript">
                        <!--
                            __upload_url = "/grn/command_add_tmp_file?";
                            os_type = "";
                            browser_type = "chrome";
                            browser_ver_major = "87";
                        //-->
                     </script>
                     <script src="{{asset('/js/upload.js')}}" type="text/javascript"></script>
                     <script src="{{asset('/js/af.js')}}" type="text/javascript"></script>
                     <div id="html5_content" style="">
                        <div style="margin-top:3px;">
                           <div id="drop_" class="drop">
                              Drop files here.
                           </div>
                           <div class="file_input_div">
                              Attach files
                              <input type="file" class="file_html5 file_input_hidden" name="file" size="40" id="file_upload_" multiple="">
                           </div>
                           <input type="hidden" name="html5" value="true" size="100">
                        </div>
                        <div class="clear_both_0px"></div>
                        <div id="upload_message">
                        </div>
                        <table id="upload_table" class="attachment_list_base_grn">
                           <tbody>
                              <tr>
                                 <td></td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                     <div class="attachment_legacy_base_grn">
                     </div>
                     <div class="clear_both_0px"></div>
                     <input type="hidden" name="upload_ticket" value="ecf3ba29b31cc27d9504bd11c393a01cacd9bb51791f6b9f43752305ffff625e">
                     <script language="Javascript" type="text/javascript">
                        if(browser_type == 'msie' && browser_ver_major >= 10)
                        {
                            jQuery('#file_upload_').keypress(function(e)
                            {
                                if(e.which == 13)
                                {
                                    e.preventDefault();
                                    jQuery('#file_upload_').click();
                                }
                            });
                        }
                        
                     </script>
                  </td>
               </tr>
               <tr>
                  <th>Percent</th>
                  <td>
                    <div class="event_add_base_grn fleft">
                        <div class="fleft"><input type="number" name="percent" size="80" maxlength="100" value="{{$schedule->percent}}" onkeypress="return event.keyCode != 13;" style="margin-top: 0px;"></div>
                    </div>
                  </td>
               </tr>
               <tr>
                  <td></td>
                  <td>
                     <script text="text/javascript">
                        // namespace
                        grn.base.namespace("grn.component.error_handler");
                        
                        (function () {
                            var G = grn.component.error_handler;
                        
                            G.show = function (request, my_callback) {
                                var s = request.responseText;
                                if (s != undefined) {
                                    var title = '';
                                    var msg = '';
                                    var json = null;
                        
                                    try {
                                        json = JSON.parse(s);
                                    } catch(e){}
                        
                                    if (json) {
                                        var show_backtrace = "";
                                        title = '';//json.code;
                        
                                        if (show_backtrace) {
                                            msg = msg + "<div style='height:145px; overflow: auto;'><table><tr><td>";
                                        }
                                        msg = msg + "<div><img border='0' src='{{asset('/img/warn100x60.gif')}}'></div>";
                                        msg = msg + "<div class='bold'>" + 'Error (' + json.code + ")</div><div>" + json.diagnosis + "</div><br>";
                                        msg = msg + "<div class='bold'>" + 'Cause' + "</div><div>" + json.cause + "</div><br>";
                                        msg = msg + "<div class='bold'>" + 'Countermeasure' + "</div><div>" + json.counter_measure + "</div>";
                        
                                        if (show_backtrace) {
                                            msg = msg + "<br><hr>";
                                            msg = msg + "<div class='bold'>( Beta/Debug only. by common.ini )</div><br>";
                                            msg = msg + "<div class='bold'>Developer Info</div><br>";
                                            if (json.developer_info) {
                                                msg = msg + json.developer_info;
                                            }
                                            msg = msg + "<div class='bold'>Backtrace</div><br>";
                                            msg = msg + "How to read backtraces(to be written) / How to read backtraces (to be written)<br>";
                                            msg = msg + "<pre style='border:1px solid #6666ff; background:#eeeeff; padding:10px;'>";
                                            if (json.backtrace) {
                                                msg = msg + json.backtrace;
                                            }
                                            msg = msg + "</pre>";
                                            msg = msg + "<br>$G_INPUT<br>";
                                            msg = msg + "<pre style='border:1px solid #6666ff; background:#eeeeff; padding:10px;'>";
                                            if (json.input) {
                                                msg = msg + json.input;
                                            }
                                            msg = msg + "</pre>";
                                            msg = msg + "</td></tr></table></div>";
                                        }
                                    }
                                    else {
                                        title = 'Error';
                                        msg = grn_split_tags(s, 1000);
                                    }
                                }
                                else {
                                    title = "Error";
                                    msg = "Connection failed.";
                                }
                        
                                GRN_MsgBox.show(msg, title, GRN_MsgBoxButtons.ok, {
                                    ui: [],
                                    caption: {
                                        ok: 'OK'
                                    },
                        
                                    callback: function (result, form) {
                                        GRN_MsgBox._remove();
                                        if (typeof my_callback != 'undefined') my_callback();
                                    }
                                });
                            };
                            G.getHeader = function (request) {
                                if (typeof(request.getAllResponseHeaders) == 'function') {
                                    return request.getAllResponseHeaders();
                                }
                                else {
                                    return request.getAllResponseHeaders; // for YAHOO.util.Connect.asyncRequest
                                }
                            };
                            G.hasCybozuError = function (request) {
                                var headers = G.getHeader(request);
                                return (headers && headers.match(new RegExp(/X-Cybozu-Error/i))) ? true : false;
                            };
                            G.hasCybozuLogin = function (request) {
                                var headers = G.getHeader(request);
                                return (headers.match(new RegExp(/X-CybozuLogin/i)) || !headers.match(new RegExp(/X-Cybozu-User/i)));
                            };
                        })();
                        
                     </script>
                     <script src="{{asset('/js/add.js')}}" type="text/javascript"></script>
                     <script language="JavaScript" type="text/javascript">
                        <!--
                        var check_add = false;
                        var browser_type = "chrome";
                        var browser_ver_major = "87";
                        var allow_file_attachment = true;
                        var is_ios = false;
                        var is_android = false;
                        var handle_type = 'modify';
                        function _submitNormal(f,ajax_page)
                        {
                            url_redirect = '/schedule/view?';
                            grn.component.button("#schedule_submit_button").showSpinner();
                            grn.component.button("#schedule_submit_button_top").showSpinner();
                        
                            var request = new grn.component.ajax.request({
                                        url: ajax_page,
                                        dataType: "json",
                                        data: jQuery(f).serialize(),
                                        method: "post"
                                    }
                            );
                        
                            request.on('beforeShowError', function (event, jqXHR) {
                                if (typeof jqXHR.responseJSON !== "undefined") {
                                    var json_obj = jqXHR.responseJSON;
                                    if (typeof json_obj.validation !== "undefined" && JSON.parse(json_obj.validation) == false) {
                                        event.preventDefault();
                                        jQuery('#show_error').css({display: ''});
                                        window.scrollTo(0, 0);
                                    }
                                }
                            });
                        
                            request.send()
                                    .done(function (data, textStatus, jqXHR) {
                                        // remove ajax flag element
                                        removeAjaxElement(f);
                                        // remove refresh status dialog flag element
                                        removeShowRefreshDialogFlag(f);
                        
                                        var json_obj = grn_parseJson(jqXHR.responseText);
                                        if (json_obj.conflict_facility == 1) {
                                            showYN(jqXHR, function (except_date) {
                                                //my_callback
                                                (function ($) {
                        
                                                    jQuery("#hfExcept").val(except_date);
                                                    check_add = false;
                                                    if (grn.base.isNamespaceDefined("grn.page.schedule.add")) {
                                                        grn.page.schedule.add.schedule_submit(form_name, ajax_page);
                                                    }
                                                })(jQuery);
                                            });
                                        } else {
                                            
                                            var link = json_obj.link;
                                            window.location = link;
                                            
                                        }
                                    })
                                    .fail(function () {
                                        // remove ajax flag element
                                        removeAjaxElement(f);
                                        // remove refresh status dialog flag element
                                        removeShowRefreshDialogFlag(f);
                        
                                        grn.component.button("#schedule_submit_button").hideSpinner();
                                        grn.component.button("#schedule_submit_button_top").hideSpinner();
                                        check_add = false;
                                    });
                        }
                        
                        function _submitUpload(f,ajax_page)
                        {
                            url_redirect = '/schedule/view?';
                            grn.component.button("#schedule_submit_button").showSpinner();
                            grn.component.button("#schedule_submit_button_top").showSpinner();
                            var request = new grn.component.ajax.request({
                                url: ajax_page,
                                dataType: "json",
                                data: jQuery(f).serialize(),
                                method: "post"
                            });
                        
                            request.send()
                                    .done( function(data, textStatus, jqXHR){
                                    removeAjaxElement(f);
                                    // remove refresh status dialog flag element
                                    removeShowRefreshDialogFlag(f);
                                    if(typeof grn_parseJson(jqXHR.responseText) === "object")
                                    {
                                        var json_obj = grn_parseJson(jqXHR.responseText);
                                        if(json_obj.code)
                                        {
                                            grn.component.button("#schedule_submit_button").hideSpinner();
                                            grn.component.button("#schedule_submit_button_top").hideSpinner();
                                            if(!json_obj.validation)
                                            {
                                                grn.component.error_handler.show(jqXHR);
                                            }
                                            else
                                            {
                                                jQuery('#show_error').css({display:''});
                                                window.scrollTo(0,0);
                                            }
                                            check_add = false;
                                        }
                                        else
                                        {
                                            if (json_obj.conflict_facility == 1) {
                                                showYN(jqXHR, function (except_date) {
                                                    //my_callback
                                                    (function ($) {
                        
                                                        jQuery("#hfExcept").val(except_date);
                                                        check_add = false;
                                                        if (grn.base.isNamespaceDefined("grn.page.schedule.add")) {
                                                            grn.page.schedule.add.schedule_submit(form_name, ajax_page);
                                                        }
                                                    })(jQuery);
                                                });
                                            } else {
                                                
                                                var link = json_obj.link;
                                                window.location = link;
                                                
                                            }
                                        }
                                    }
                                    else
                                    {
                                        setAjaxElement(f);
                                        setShowRefreshDialogFlag(f);
                                        _submitNormal(f,ajax_page);
                                    }
                                })
                                .fail(function() {
                                    // remove ajax flag element
                                    removeAjaxElement(f);
                                    // remove refresh status dialog flag element
                                    removeShowRefreshDialogFlag(f);
                        
                                    grn.component.button("#schedule_submit_button").hideSpinner();
                                    grn.component.button("#schedule_submit_button_top").hideSpinner();
                                    check_add = false;
                                });
                        }
                        
                        /**
                         * This function is called only from schedule/add.js.
                         *
                         * @param {HTMLElement} f
                         * @param {string} ajax_page
                         *
                         */
                        function _submit(f,ajax_page)
                        {
                            if (((browser_type == 'msie' && browser_ver_major < 10) || is_ios || is_android) && allow_file_attachment)
                            {
                                _submitUpload(f,ajax_page);
                            }
                            else
                            {
                                _submitNormal(f,ajax_page);
                            }
                        }
                        
                        function showYN(request, my_callback) {
                            var s = request.responseText;
                            if (typeof s != 'undefined') {
                                var ob_json = grn_parseJson(s);
                                var title;
                                var msg;
                                var html_rows = "";
                                var html_row_header = "";
                                var rows_length = 0;
                                var rows_limit = 0;
                                var html_row_more = "";
                                var is_show_more_text = false;
                                var event_except = "";
                                (function ($) {
                                    var events = ob_json.conflict_events;
                                    if (events != null && events.length > 0) {
                                        rows_length = events.length;
                                        rows_limit = events.length;
                                        if (events.length > 5) {//Limit 5 events are displayed.
                                            rows_limit = 5;
                                            is_show_more_text = true;
                                        }
                        
                                        html_row_header = "<tr><th class='nowrap-grn' style='width:30px;'>Date</th><th class='nowrap-grn'>Conflicting facility </th></tr>";
                                        for (var i = 0; i < rows_limit; i++) {
                                            html_rows += "<tr><td class='nowrap-grn'><span class='icon_list_style_grn icon_inline_grn'></span><span>" + events[i].setdatetime + "</span></td><td><span>" + events[i].col_facility + "</span></td></tr>";
                                            event_except += ";" + events[i].col_setdatetime;
                                        }
                                        for (var j = rows_limit; j < events.length; j++) {
                                            event_except += ";" + events[j].col_setdatetime;
                                        }
                                    }
                                })(jQuery);
                        
                                if (is_show_more_text == true) {
                                    html_row_more = "<div class='mTop7'>...&nbsp;" +
                                    "" +
                                    (rows_length - rows_limit) +
                                    "&nbsp;more appointments with conflicting facilities exist.</div>";
                                }
                                msg = "<div class='mBottom10'><span class='icon_attention_grn attentionMessage-grn bold_grn'>One or more facilities are conflicting in the following appointments:</span></div>";
                                msg += "<table class='table_grn table_list_1_grn'>" + html_row_header + html_rows + "</table>" + html_row_more;
                                msg += "<div class='border-partition-common-dot-grn'></div>";
                                if (ob_json.conflict_all == 1) {// all event are conflict
                                    msg += "<div class='bold_grn mTop15'>All appointments have conflicting facilities. No appointment can be added.</div>";
                                }
                                else {
                                    if (handle_type == '' || handle_type == 'add') // event is added
                                    {
                                        msg += "<div class='bold_grn mTop15'>Do you want to add only appointments with no conflicting facilities?</div>";
                                    }
                                    else    // modify
                                    {
                                        msg += "<div class='bold_grn mTop15'>Do you want to edit only appointments with no conflicting facilities?<br />Appointments with conflicting facilities are deleted, not edited.</div>";
                                    }
                                }
                        
                                var msgboxButtonType = (ob_json.conflict_all) ? GRN_MsgBoxButtons.ok : GRN_MsgBoxButtons.yesno;
                        
                                GRN_MsgBox.show(msg, title, msgboxButtonType, {
                                    ui: [],
                                    caption: {
                                        yes: 'Yes',
                                        no: 'No',
                                        ok: 'Back'
                                    },
                                    main_button: {ui: grn.component.button.UI.MAIN, autoDisable: true},
                                    callback: function (result, form) {
                                        check_add = false;
                                        grn.component.button("#schedule_submit_button").hideSpinner();
                                        grn.component.button("#schedule_submit_button_top").hideSpinner();
                                        if (result == GRN_MsgBoxResult.yes && typeof my_callback != 'undefined') {
                                            my_callback(event_except);
                                        } else {
                                            jQuery("#hfExcept").empty();
                                            if (result == GRN_MsgBoxResult.ok) {
                                                GRN_MsgBox.close();
                                            }
                                        }
                                    }
                                });
                            }
                        }
                        
                        function toggle(id)
                        {
                            var loading = document.getElementById(id + '_loading');
                            var button = document.getElementById(id + '_button');
                            
                            if (!loading)
                            {
                                loading.innerHTML = '';
                                button.disabled = false;
                                isLoading = false;
                            }
                            else
                            {
                                loading.innerHTML = '<img src="' + G.spinnerImage + '" />';
                                button.disabled = true;
                                isLoading = true;
                            }
                        }
                        
                        function setAjaxElement(f)
                        {
                            if( document.getElementById('use_ajax') ) return false;
                        
                            var element = document.createElement("input");
                            element.setAttribute("type", "hidden");
                            element.setAttribute("id", "use_ajax");
                            element.setAttribute("name", "use_ajax");
                            element.setAttribute("value", "1");
                            f.appendChild(element);
                            return true;
                        }
                        
                        function removeAjaxElement(f)
                        {
                            var use_ajax = document.getElementById('use_ajax');
                            if(use_ajax)
                            {
                                f.removeChild(use_ajax);
                            }
                        }
                        
                        function setShowRefreshDialogFlag(f)
                        {
                            if (jQuery("#attendance_check_dialog").is(":visible"))
                            {
                                if( document.getElementById('show_refresh_status_dialog') ) return;
                        
                                var element = document.createElement("input");
                                element.setAttribute("type", "hidden");
                                element.setAttribute("id", "show_refresh_status_dialog");
                                element.setAttribute("name", "show_refresh_status_dialog");
                                element.setAttribute("value", "1");
                                f.appendChild(element);
                            }
                        }
                        
                        function removeShowRefreshDialogFlag(f)
                        {
                            var show_refresh_status_dialog = document.getElementById('show_refresh_status_dialog');
                            if(show_refresh_status_dialog)
                            {
                                f.removeChild(show_refresh_status_dialog);
                            }
                        }
                        //-->
                     </script>
                     <div class="mTop10 buttonArea-grn">
                        <span id="schedule_submit_button" class="button_grn_js button1_main_grn button1_r_margin2_grn" onclick="if (!grn.component.button(this).isDisabled()) grn.page.schedule.add.schedule_submit('schedule/banner_modify','/schedule/update/{{$schedule->id}}');"><a href="javascript:void(0);" role="button">Save</a></span><span id="schedule_button_cancel" class="button_grn_js button1_normal_grn" onclick="grn.page.schedule.add.schedule_cancel('/schedule/view?event=1047&amp;bdate=2021-01-05&amp;referer_key=256ee5dd408894eaae437814e95cfc65&amp;span_cover=1');"><a href="javascript:void(0);" role="button">Cancel</a></span>
                     </div>
                  </td>
               </tr>
            </tbody>
         </table>
         <input type="hidden" name="event" value="{!!$schedule->event!!}">
         <input type="hidden" name="bdate" value="2021-01-05">
         <input type="hidden" name="uid" value="{!!$schedule->uid!!}">
      </form>
      <script type="text/javascript" src="{{asset('/js/timezone_info.js')}}"></script>
      <script type="text/javascript" src="{{asset('/js/schedule_validate_date.js')}}"></script>
      <script type="text/javascript" src="{{asset('/js/schedule.js')}}"></script>
   </div>
</div>
@stop
@section('script')
@parent
<script src="{{asset('/js/dist/schedule.js')}}" type="text/javascript"></script>
@stop