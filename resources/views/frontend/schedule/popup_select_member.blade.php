<!DOCTYPE html>
<html lang="en">
<head>
   <meta name="robots" content="noindex, nofollow, noarchive">
   <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
   <meta http-equiv="Content-Style-Type" content="text/css">
   <meta http-equiv="Content-Script-Type" content="text/javascript">
   <title>Select users</title>
   <script src="{!!asset('assets2/js/jquery.min.js')!!}"></script>
   <link href="{{asset('assets/css/std.css')}}" rel="stylesheet" type="text/css">
   <link href="{{asset('assets/css/treeview.css')}}" rel="stylesheet" type="text/css">
   <link href="{{asset('assets/css/msgbox.css')}}" rel="stylesheet" type="text/css">
   <link href="{{asset('assets/css/image_grn.css')}}" rel="stylesheet" type="text/css">
   <link href="{{asset('assets/css/font-en.css')}}" rel="stylesheet" type="text/css">
   <link href="{{asset('assets/css/Designsimple-white.css')}}" rel="stylesheet" type="text/css">
   <link rel="shortcut icon" href="/garoon3/grn/image/cybozu/garoon.ico?20200925.text">
   <script src="{{asset('/js/base.js')}}" type="text/javascript"></script>
   <script language="javascript" type="text/javascript">
      <!--

      if( typeof grn.browser == "undefined" )
      {
          grn.browser = {};
      }

          grn.browser.chrome = true;
      grn.browser.version = 87;


      grn.browser.isSupportHTML5 = false;
      if( !!window.FormData )
      {
          grn.browser.isSupportHTML5 = true;
      }

      //-->
   </script><script src="{{asset('/js/std.js')}}" type="text/javascript"></script>
   <script src="{{asset('/js/yahoo-min.js')}}" type="text/javascript"></script>
   <script src="{{asset('/js/event-min.js')}}" type="text/javascript"></script>
   <script src="{{asset('/js/dom-min.js')}}" type="text/javascript"></script>
   <script src="{{asset('/js/connection-min.js')}}" type="text/javascript"></script>
   <script src="{{asset('/js/treeview-min.js')}}" type="text/javascript"></script>
   <script src="{{asset('/js/json-min.js')}}" type="text/javascript"></script>
   <script src="{{asset('/js/animation-min.js')}}" type="text/javascript"></script>
   <script src="{{asset('/js/yahoo-dom-event.js')}}" type="text/javascript"></script>
   <script src="{{asset('/js/autofit.js')}}" type="text/javascript"></script>
   <script src="{{asset('/js/url.js')}}" type="text/javascript"></script>
   <script>
      grn.component.url.PAGE_PREFIX = "";
      grn.component.url.PAGE_EXTENSION = "";
      grn.component.url.STATIC_URL = "/garoon3";
      grn.component.url.BUILD_DATE = "20200925.text";
   </script><script src="{{asset('/js/i18n.js')}}" type="text/javascript"></script>
   <script src="{{asset('/js/resource-en.js')}}"></script>
   <script src="{{asset('/js/request.js')}}" type="text/javascript"></script>
   <script src="{{asset('/js/error_default_view.js')}}" type="text/javascript"></script>
   <script src="{{asset('/js/error_handler.js')}}" type="text/javascript"></script>
   <script src="{{asset('/js/runtime.js')}}" type="text/javascript"></script>
   <script src="{{asset('/js/commons_chunk.js')}}" type="text/javascript"></script>
   <script src="{{asset('/js/common.js')}}" type="text/javascript"></script>
   <script src="{{asset('/js/msgbox.js')}}" type="text/javascript"></script>
   <script language="JavaScript" type="text/javascript">
      grn.data = {"CSRF_TICKET":"34e66a46d7ac39607907f8904e335c3d","locale":"en","login":{"id":"58","name":"Foster Brown","slash":"","timezone":"Asia\/Tokyo","language":"en","code":"brown","email":"","url":"","phone":""},"short_date_format":"&&wdayshort&&, &&mday&& &&monthfull&&","assets_cache_busting":1580698759};
   </script>
</head>
<body>
   <div id="body">
   <script src="{{asset('/js/popup_member_select.js')}}" type="text/javascript"></script>
   <script src="{{asset('/js/member_select_list.js')}}" type="text/javascript"></script>
   <script src="{{asset('/js/member_add.js')}}" type="text/javascript"></script>
   <script src="{{asset('/js/button.js')}}" type="text/javascript"></script>
   <script language="JavaScript" type="text/javascript">
      new grn.component.popup_member_select.PopupMemberSelect("popup_{{$name}}",
              "popup_CID", "1", {
                  selected_tab: "0",
                  no_multiple: "0",
                  select_name: "{{$name}}",
                  system: "0",
                  system_display: "0",
                  item_data_name: "item",
                  item_session_name: "grn/popup_member_select",
                  multi_apply: "0",
                  plid: "",
                  show_group_role: "1",
                  require_role_tab: "0",
                  item_group_id: "0",
                  return_page: "",
                  send_cgi_parameter: "0",
                  form_name: "grn/popup_member_select",
                  member_list_name: "{{$name}}",
                  selected_list: "popup_{{$name}}",
                  app_id: "schedule",
                  s_oid: "@if(isset($department_id)) {!!$department_id!!} @else 0 @endif",
                  s_rid: "0",
                  org_id: "0",
                  is_calendar: "0",
                  include_org: "1",
                  plugin_session_name: "{{$pluginSessionName}}",
                  plugin_data_name: "{{$pluginDataName}}",
                  searchword: "",
                  is_search_result: "0",
                  more_user_offset: "100",
                  more_user_limit: "100",
                  require_dynamic_roles: "0",
                  require_administrator_role: "0",
                  selected_tid : ""
                  });
      

   </script>
   {!!$script!!}
   <form name="list" method="POST" action="/schedule/popup_member_select?">
      <input type="hidden" name="_token" value="{{ csrf_token() }}" />
      <input type="hidden" name="csrf_ticket" value="34e66a46d7ac39607907f8904e335c3d">
      <input type="hidden" name="searchword" value="">
      <input type="hidden" name="no_multiple" value="0">
      <input type="hidden" name="form_name" value="schedule/add">
      <input type="hidden" name="include_org" value="1">
      <input type="hidden" name="system" value="0">
      <input type="hidden" name="system_display" value="0">
      <input type="hidden" name="select_name" value="{{$name}}">
      <input type="hidden" name="plugin_data_name" value="{{$pluginDataName}}">
      <input type="hidden" name="plugin_session_name" value="{{$pluginSessionName}}">
      <input type="hidden" name="item_data_name" value="item">
      <input type="hidden" name="item_session_name" value="grn/popup_member_select">
      <input type="hidden" name="send_cgi_parameter" value="0">
      <input type="hidden" name="app_id" value="schedule">
      <input type="hidden" name="return_page" value="">
      <input type="hidden" name="multi_apply" value="0">
      <input type="hidden" name="plid" value="">
      <input type="hidden" name="item_group_id" value="0">
      <input type="hidden" name="require_role_tab" value="0">
      <input type="hidden" name="is_calendar" value="1">
      <input type="hidden" name="is_search_result" value="0">
      <input type="hidden" name="show_group_role" value="1">
      <input type="hidden" name="require_dynamic_roles" value="0">
      <input type="hidden" name="require_administrator_role" value="0">
      <input type="hidden" name="is_post_message" value="">
      <input type="hidden" name="selected_tid" value="">
      <input type="hidden" name="select_func" value="init">
      <input type="hidden" name="selected_tab" value="0">
      <input type="hidden" name="s_oid" value="@if(isset($department_id)) {!!$department_id!!} @else 0 @endif">
      <input type="hidden" name="org_id" value="0">
      <input type="hidden" name="selected_users_c_id" value=""/>
      <input type="hidden" name="selected_users_s_id" value=""/>
      <input type="hidden" name="s_rid" value="0">
      <div class="popup_body1_grn">
      <div class="popup_body1_sub_grn">
      <!-- Header Start -->
      <div class="popup_title_grn">
         <div class="float_left">
            <h2 style="display:inline;" class="grn">Select users</h2>
         </div>
         <a href="#" onclick="window.close();" role="button">
            <div title="Close" aria-label="Close" class="subWindowClose-grn"></div>
         </a>
         <div class="clear_both_0px"></div>
      </div>
      <!-- Header End -->
      <div class="popup_content_grn multi_select_base_grn" style="width:1000px;">
         <table class="table_plain_grn w_full_grn">
            <tbody>
               <tr>
                  <td class="vAlignTop-grn">
                     <div class="select_item_base_grn">
                        <div class="tab_menu">
                           <div class="tab">
                              <span class="tab_left_on"></span>
                              <span class="tab_on">
                              <span class="nowrap-grn ">
                              <span class="nowrap-grn "><a  href="/schedule/popup_member_select?select_func=browse&amp;selected_tab=0&amp;s_oid=0&amp;s_rid=0&amp;searchword=&amp;no_multiple=0&amp;form_name=schedule%2Fadd&amp;include_org=1&amp;system=0&amp;system_display=0&amp;select_name={{$name}}&amp;plugin_data_name=access_plugin&amp;plugin_session_name=schedule%2Fadd&amp;item_data_name=item&amp;item_session_name=grn%2Fpopup_member_select&amp;send_cgi_parameter=0&amp;app_id=schedule&amp;return_page=&amp;multi_apply=0&amp;plid=&amp;item_group_id=0&amp;require_role_tab=0&amp;is_calendar=1&amp;is_search_result=0&amp;show_group_role=1&amp;require_dynamic_roles=0&amp;require_administrator_role=0&amp;is_post_message=&amp;selected_tid=">User</a></span>
                              </span></span>
                              <span class="tab_right_on"></span>
                           </div>
                        </div>
                        <div class="tab_menu_end"></div>
                        <div class="clear_both_0px"></div>
                        <table width="100%" cellspacing="0" cellpadding="5" style="border-collapse: collapse;">
                           <tbody>
                              <tr valign="top">
                                 <script language="JavaScript">
                                    <!--

                                    YAHOO.namespace("grn");
                                    YAHOO.grn.orgTree = function(treeName, asyncURL, pageName, linkURL, selectedOID) {
                                        this._treeName = treeName;
                                        this._asyncURL = asyncURL;
                                        this._pageName = pageName;
                                        this._linkURL = linkURL;
                                        this._selectedOID = selectedOID;
                                    }

                                    YAHOO.grn.orgTree.prototype = {
                                        _tree: null,

                                        _treeName: null,
                                        _asyncURL: '',
                                        _pageName: '',
                                        _linkURL: '',
                                        _selectedOID: null,
                                        _oidKey: 'oid',

                                        _loadTreeData: function(tmp) {
                                            var obj = this;

                                            tmp.setDynamicLoad(
                                                function (node, onCompleteCallback){
                                                    node.sendExpanded = false;

                                                    var ajaxDynamicRequest = new grn.component.ajax.request(
                                                    {
                                                        url: node.url,
                                                        method: node.method,
                                                        data: node.postData + "&action=getchild",
                                                        dataType: "json",
                                                        grnRedirectOnLoginError: true
                                                    });

                                                    ajaxDynamicRequest.send().done(function (data, textStatus, jqXHR) {
                                                        jqXHR.argument = {
                                                            'node': node,
                                                            'object': obj,
                                                            'action': 'getchild',
                                                            'complete': onCompleteCallback
                                                        };
                                                        obj._getResponse(data, jqXHR);
                                                    });
                                                }
                                            );
                                        },

                                        _buildTree: function(data,node,refreshCount) {
                                            var tmpNode;

                                            if (node.children.length > 0) return;

                                            var dataCount = 0;
                                            for (var i = 0; i < data.length; i++) {
                                                if ( data[i] == null ) continue;
                                                dataCount++;

                                                var oid = data[i]['oid'];
                                                var name = data[i]['name'];
                                                var count = data[i]['count'];
                                                var expanded = data[i]['expanded'];

                                                var tmpNode = this.createNode(node, oid, name, count);

                                                tmpNode.sendExpanded = (expanded != 1);
                                                tmpNode.method="POST";
                                                tmpNode.url=this._asyncURL;
                                                tmpNode.postData="csrf_ticket=34e66a46d7ac39607907f8904e335c3d&page=" + this._pageName + "&oid=" + oid;

                                                tmpNode.selected = (oid == this._selectedOID);

                                                if ( count > 0 ) {
                                                    var children = data[i]['children'];
                                                    if (children.length > 0) {
                                                        this._buildTree(children,tmpNode,false);

                                                        if (expanded == 1) {
                                                            tmpNode.expand();
                                                        }
                                                    } else {
                                                        this._loadTreeData(tmpNode);
                                                    }

                                                }
                                            }

                                            if (refreshCount) {
                                                this._refreshCount(node, dataCount);
                                            }

                                            this._tree.draw();
                                        },

                                        _refreshCount: function(node, count) {
                                            if (count > 0) {
                                            node.html = node.html.replace( /&nbsp;\(\d+\)<\/a>/, "&nbsp;(" + count + ")</a>" );
                                            } else {
                                                node.html = node.html.replace( /&nbsp;\(\d+\)<\/a>/, "&nbsp;</a>" );
                                                node.setDynamicLoad(null);
                                            }
                                        },

                                        _getResponse: function(data, jqXHR) {
                                            refresh_count = ( jqXHR.argument['action'] == 'getchild' );
                                            jqXHR.argument.object._buildTree( data, jqXHR.argument.node,refresh_count );

                                            if (jqXHR.argument['complete'] != null) {
                                                jqXHR.argument.complete();
                                            }

                                            if (typeof jqXHR.argument.node !== "undefined") {
                                                jqXHR.argument.node.focus();
                                            }
                                        },

                                        init: function(data) {

                                            this.buildTree(data,null);

                                            var obj = this;

                                            this._tree.subscribe( 'expand', function(node) {
                                                if (node.children.length > 0 && node['sendExpanded']) {
                                                    var ajaxSubExpandRequest = new grn.component.ajax.request(
                                                    {
                                                        url: node.url,
                                                        method: node.method,
                                                        data: node.postData + "&action=expand",
                                                        dataType: "json",
                                                        grnRedirectOnLoginError: true
                                                    });

                                                    ajaxSubExpandRequest.send().done(function (data, textStatus, jqXHR) {
                                                        jqXHR.argument = {
                                                            'node': node,
                                                            'object': obj,
                                                            'action': 'expand'
                                                        };
                                                        obj._getResponse(data, jqXHR);
                                                    });
                                                }
                                            });

                                            this._tree.subscribe( 'collapse', function(node) {
                                                if (node.children.length > 0) {
                                                    obj._loadTreeData(node);
                                                    node.sendExpanded = true;

                                                    var ajaxSubCollapseRequest = new grn.component.ajax.request(
                                                    {
                                                        url: node.url,
                                                        method: node.method,
                                                        data: node.postData + "&action=collapse",
                                                        dataType: "json",
                                                        grnRedirectOnLoginError: true
                                                    });

                                                    ajaxSubCollapseRequest.send().done( function(data, textStauts, jqXHR){
                                                        jqXHR.argument = {
                                                            'node': node,
                                                            'object': obj,
                                                            'action': 'expand'
                                                        };
                                                        obj._getResponse( data, jqXHR );
                                                    });
                                                }
                                            } );

                                        },


                                        createNode: function(parent, oid, name, count) {
                                            var html = '';
                                            var url = this._linkURL;
                                            if ( !url.match( /.+\?$/ ) ) url += '&';
                                            html += "<a href='" + url + this._oidKey + "=" + oid + "'>" + name;
                                            //if (count > 0 ) html += "&nbsp;(" + count + ")";
                                            html += "</a>";

                                            html = this.setNodeStyle(oid, html);
                                            var node = new YAHOO.widget.HTMLNode(html, parent, false, true);
                                            node.href = url + this._oidKey + "=" + oid;
                                            return node;
                                        },

                                        setNodeStyle: function(oid, html) {
                                            if (oid == this._selectedOID) {
                                                html = "<div id='selected_node' class='tree-select-current'>" + html + "</div>";
                                            } else {
                                                html = "<div style='white-space: nowrap;'>" + html + "</div>";
                                            }
                                            return html;
                                        },

                                        buildTree: function(data,node) {

                                            this._tree = new YAHOO.widget.TreeView(this._treeName);

                                            var root = node;
                                            if (root == null) root = this._tree.getRoot();

                                            this._buildTree(data,root,false);

                                        },

                                        getTree: function() {
                                            return this._tree;
                                        },

                                        getSelectionTop: function() {

                                            var root = this._tree.getRoot();

                                            var count = 0;
                                            var break_flag = false;
                                            return getCount(root,count);

                                            function getCount(node, cnt) {
                                                for (var i = 0; i < node.children.length; i++ ) {
                                                    var child = node.children[i];
                                                    if (child.selected) {
                                                        break_flag = true;
                                                        break;
                                                    }
                                                    cnt++;
                                                    if (child.expanded) {
                                                        cnt = getCount(child, cnt);
                                                        if (break_flag) break;
                                                    }
                                                }

                                                return cnt;
                                            }
                                        },

                                        setOidKey: function( value ) {
                                            this._oidKey = value;
                                        }

                                    };

                                    function setTreeHeight(leftName, rightName) {

                                        var right = document.getElementById(rightName);
                                        var left = document.getElementById(leftName);

                                        var treeHeight = 300;
                                        if (right.offsetHeight > 300) {
                                            treeHeight = right.offsetHeight * 0.9;
                                        }

                                        left.style.minHeight = treeHeight + 'px';
                                    }



                                    //-->
                                 </script>                            
                                 <td class="wrap-tree-view">
                                    <div id="tree_view" class="tree-view" style="height:470px;">
                                       <span class="nowrap-grn "><a  class="tree-select-current" href="/schedule/popup_member_select?top=1&amp;select_func=browse&amp;selected_tab=0&amp;searchword=&amp;no_multiple=0&amp;form_name=schedule%2Fadd&amp;include_org=1&amp;system=0&amp;system_display=0&amp;select_name={{$name}}&amp;plugin_data_name=access_plugin&amp;plugin_session_name=schedule%2Fadd&amp;item_data_name=item&amp;item_session_name=grn%2Fpopup_member_select&amp;send_cgi_parameter=0&amp;app_id=schedule&amp;return_page=&amp;multi_apply=0&amp;plid=&amp;item_group_id=0&amp;require_role_tab=0&amp;is_calendar=1&amp;is_search_result=0&amp;show_group_role=1&amp;require_dynamic_roles=0&amp;require_administrator_role=0&amp;is_post_message=&amp;selected_tid=">(Top)</a></span>
                                       <div id="org_tree"></div>
                                    </div>
                                 </td>
                                 <td id="tree_view_right" class="wrap-tree-view-right">
                                    <div class="mBottom10 nowrap-grn">
                                       <input type="hidden" name="" value="">
                                       <div class="search_navi" style="width:210px;">
                                          <div class="searchbox-grn">
                                             <div id="searchbox-keyword-user" class="input-text-outer-cybozu searchbox-keyword-area searchbox_keyword_grn">
                                                <input type="text" maxlength="100" class="input-text-cybozu" style="width: 202px; " name="searchword" autocomplete="off" value="" placeholder="User search" onkeypress="if (event.keyCode == 13) grn.component.popup_member_select.search(this.form,'user');">
                                                <button class="searchbox-submit-cybozu" type="submit" title="Search" aria-label="Search" onclick="grn.component.popup_member_select.search(this.form,'user');"></button>
                                             </div>
                                          </div>
                                       </div>
                                       <script src="{{asset('/js/member_select_search_info.js')}}" type="text/javascript"></script>
                                       <span class="mLeft7"><span class="textSub-grn vAlignMiddle-grn"></span><a id="searchtarget_info_icon" class="mLeft2" href="javascript:void(0);" aria-label="Show items to be searched" title="Show items to be searched"><span class="icon_information_sub16_grn icon_inline_grn icon_only_grn vAlignMiddle-grn"></span></a></span>
                                       <div class="bubble has-top-prong balloon_sub_daialog_grn" id="searchtarget_info_tooltip" style="display:none">
                                          <div style="display: true;"></div>
                                          <div class="balloon_sub_dialog_base_grn balloon_search_target_grn foldback_grn">
                                             <div class="sub_title">Search in:</div>
                                             Name/Login name/Pronunciation/E-mail/Position/社員番号
                                             <div style="left: 90px;" class="top-prong">
                                                <div class="prong-dk"></div>
                                                <div class="prong-lt"></div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="clear_both_0px"></div>
                                    </div>
                                    <div class="bold_grn mBottom5">(Top)</div>
                                    <span class="inline_block_grn textSub-grn mBottom3">Members</span>
                                    <div class="selectlist_area_grn">
                                       <span id="spinner_selectlist_popup_CID" style="display:none;position:absolute;z-index:1;"><img src="{!!asset('/img/spinner.gif')!!}" border="0" alt=""></span>
                                       <div id="selectlist_base_selectlist_popup_CID" aria-multiselectable="true" class="selectlist_grn " tabindex="0" style="width:244px; height:360px;">
                                          <ul id="ul_selectlist_popup_CID">
                                              @if(isset($members))
                                               @foreach($members as $key=>$member)
                                                <li
                                                    id="selectlist_popup_CID_member_user_{{$member->id}}"
                                                    class="selectlist_popup_CID"
                                                    data-value="{{$member->id}}"
                                                    data-type="user"
                                                    data-name="{{$member->full_name}}"
                                                    data-code="{{$member->full_name}}"                    data-id="{{$member->id}}"
                                                    data-group-path="{{$member->full_name}}"
                                                    data-url="/grn/user_view?uid=51">

                                                    <span class="selectlist_user_grn" aria-label="Organization&#039;s appointment"
                                                          title="Organization&#039;s appointment"></span>
                                                    <span class="selectlist_text_grn">{{$member->full_name}}</span>
                                                </li>
                                               @endforeach
                                               @endif
                                          </ul>
                                          <span id="spinner_scroll_selectlist_popup_CID" style="display:none;"><img src="{{asset('/img/spinner.gif')}}" border="0" alt=""></span>
                                       </div>
                                    </div>
                                    <div class="textSub-grn mTop5"><a id="select_all_selectlist_popup_CID" class="mRight20" href="javascript:void(0);">Select all</a><a id="un_select_all_selectlist_popup_CID" style="display:none" class="mRight20" href="javascript:void(0);">Clear all</a></div>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </td>
                  <td class="vAlignTop-grn tAlignCenter-grn">
                     <div class="buttonSelectMove-grn" style="margin:180px 20px 0;">
                        <div class="mBottom15">
                           <span class="aButtonStandard-grn">
                           <a id="btn_add_CID" style="padding-right: 0px;" href="javascript:void(0);" role="button" onclick="grn.component.popup_member_select.addMember('selectlist_base_selectlist_popup_CID');">
                           <span class="aButtonText-grn">Add</span><span class="icon-buttonArrowRightBehind-grn"></span>
                           </a>
                           </span>
                        </div>
                        <div>
                           <span class="aButtonStandard-grn">
                           <a id="btn_rmv_CID" style="padding-left: 0px;" href="javascript:void(0);" role="button" onclick="grn.component.popup_member_select.removeMember('selectlist_base_selectlist_popup_{{$name}}');">
                           <span class="icon-buttonArrowLeft-grn"></span><span class="aButtonText-grn">Remove</span>
                           </a>
                           </span>
                        </div>
                     </div>
                  </td>
                  <td class="vAlignTop-grn">
                     <div class="select_item_candidacy_grn">
                        <div class="label_grn label_sub_grn mBottom3">Selected</div>
                        <div class="selectlist_area_grn">
                           <span id="spinner_selectlist_popup_{{$name}}" style="display:none;position:absolute;z-index:1;"><img src="{!!asset('/img/spinner.gif')!!}" border="0" alt=""></span>
                           <div id="selectlist_base_selectlist_popup_{{$name}}" aria-multiselectable="true" class="selectlist_grn selectlist_l_grn focus_grn" tabindex="0" style="width:244px; height:427px;">
                              <ul id="ul_selectlist_popup_{{$name}}">
                                  {!!$member_html!!}
                              </ul>
                              <span id="spinner_scroll_selectlist_popup_{{$name}}" style="display:none;"><img src="{{asset('/img/spinner.gif')}}" border="0" alt=""></span>
                           </div>
                        </div>
                        <div class="textSub-grn mTop5"><a id="select_all_selectlist_popup_{{$name}}" class="mRight20" href="javascript:void(0);">Select all</a><a id="un_select_all_selectlist_popup_{{$name}}" style="display:none" class="mRight20" href="javascript:void(0);">Clear all</a></div>
                     </div>
                  </td>
               </tr>
            </tbody>
         </table>
      </div>
      <script language="JavaScript">
         <!--

             var treeName = 'org_tree';
             var asyncURL = '/schedule/popup_user_select_json?';
             var pageName = 'grn%2Fpopup_member_select';
             var linkURL = '/schedule/popup_member_select?';

                         var selectedOID = 0;

             var orgTree = new YAHOO.grn.orgTree(treeName, asyncURL, pageName, linkURL, selectedOID);
             orgTree.setOidKey('s_oid');
             var treeData = {!!$department!!};

             setTreeHeight('tree_view', 'tree_view_right');



             function createExternalNodes() {
                 var tree = orgTree.getTree();
                 var root = tree.getRoot();
                     orgTree.createNode(root, -2, 'Unassigned users', 0);
                     tree.draw();
                 }

                 jQuery(window).on('load', function () {
                     orgTree.init(treeData)
                     createExternalNodes()
                 });



         //-->
      </script>
      <div class="popup_button_area_grn">
         <span class="button_grn_js button1_main_grn  button1_r_margin2_grn">
         <a role="button" href="#" onclick="grn.component.popup_member_select.apply('selectlist_base_selectlist_{{$name}}')">Apply</a></span><span class="button_grn_js button1_normal_grn"><a role="button" href="#" onclick="window.close();">Cancel</a>
         </span>
      </div>
   </form>