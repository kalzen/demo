@extends('frontend.layouts.create_schedule')
@section('content')
<table class="global_navi_title" cellpadding="0" cellmargin="0" style="padding:0px;">
    <tbody>
        <tr>
            <td width="100%" valign="bottom" nowrap="">
                <div class="global_naviAppTitle-grn">
                    <img src="{{asset('/img/schedule20.gif')}}" border="0" alt="">Scheduler
                </div>
                <div class="global_navi-viewChange-grn">
                    <ul>
                        <li><a class="global_naviBackTab-viewChange-grn viewChangeLeft-grn" href="{{route('frontend.schedule.group_day')}}">Group day</a></li>
                        <li><a class="global_naviBackTab-viewChange-grn" href="{{route('frontend.schedule.index')}}">Group week</a></li>
                        <li role="heading" aria-level="2" class="global_naviBack-viewChangeSelect-grn"><span>Day</span></li>
                        <li><a class="global_naviBackTab-viewChange-grn" href="{{route('frontend.schedule.personal_week')}}">Week</a></li>
                        <li><a class="global_naviBackTab-viewChange-grn" href="{{route('frontend.schedule.personal_month')}}">Month</a></li>
                        <li><a class="global_naviBackTab-viewChange-grn viewChangeRight-grn" href="{{route('frontend.schedule.personal_year')}}">Year</a></li>
                    </ul>
                </div>
            </td>
            <td align="right" valign="bottom" nowrap="">
            </td>
        </tr>
    </tbody>
</table>
<script src="{{asset('/js/jquery-ui-1.12.1.custom.min.js')}}" type="text/javascript"></script>
<div class="mainarea">
   <div class="mainareaSchedule-grn">
      <div id="view" class="multi_view">
         <script src="{{asset('/js/schedules.js')}}" type="text/javascript"></script>
         <!--menubar-->
         <div id="menu_part">
            <div id="smart_main_menu_part">
               <span class="menu_item">
               <a href="{{route('frontend.schedule.create')}}?bdate={{$date_now}}" >
               <img src="{{asset('/img/write20.png')}}" border="0" alt="">New</a>
               </span>
               &nbsp;
            </div>
            <div id="smart_rare_menu_part" style="white-space:nowrap;">
               <script language="JavaScript" type="text/javascript">
                  <!--
                  function search_submit()
                  {
                      var f = document.forms["search"];
                      if(!f)
                      {
                          return;
                      }
                      if(arrSearch['schedules'].firstFlag)
                      {
                          var input_search = document.getElementById( 'schedules_search_text' );
                          input_search.value = '';
                      }
                      var referer_bdate_value = __get_referer_bdate();
                      if ( referer_bdate_value.length > 0 )
                      {
                          var referer_bdate = document.createElement( "input" );
                          referer_bdate.type = "hidden";
                          referer_bdate.name = "referer_bdate";
                          referer_bdate.value = referer_bdate_value;
                          f.appendChild(referer_bdate);
                      }
                      f.submit();
                  }
                  -->
               </script>
            </div>
            <div class="clear_both_0px"></div>
         </div>
         <!--menubar_end-->
         <script language="JavaScript" type="text/javascript">
            <!--
            function __get_referer_bdate()
            {
                var referer_bdate = '';
                var param_elements = document.getElementsByName('bdate');
                for( var i = 0; i < param_elements.length; i++ )
                {
                    if (param_elements[i].getAttribute('value'))
                    {
                        referer_bdate = param_elements[i].getAttribute('value');
                        break;
                    }
                }
                return referer_bdate;
            }
            -->
         </script>
         <form class="scheduleWrapper view_main_area" name="schedule/personal_day" method="GET"
            action="/schedule/personal_day?bdate={{$date_now}}&amp;uid=&amp;gid=15">
            <div class="margin_bottom">
               <table class="personalDayUpper-grn">
                  <tr>
                     <td class="v-allign-middle" nowrap="nowrap" style="width:1%">
                        <table cellspacing="0" cellpadding="0" border="0">
                           <tbody>
                              <tr>
                                 <td nowrap="nowrap">
                                    <script language="JavaScript" type="text/javascript">
                                       function setUserGroups( group_obj ) {
                                           @foreach($departments as $key=>$val)
                                           group_obj.appendItem( new GRN_GroupItem( '{{$val->id}}', '{{$val->name}}', 'window.parent.clickUserGroup', false, '' ) );
                                           @endforeach
                                           group_obj.appendItem( new GRN_GroupItem( 'login', '(Logged-in user)', 'window.parent.clickUserGroup', false, '' ) );
                                         
                                       
                                       }
                                       function clickUserGroup( gid, name, extra_param ) {
                                           document.getElementById( 'popup_group_list_iframe' ).style.display= 'none';
                                           updateDropdownButtonTitle( name );
                                           location.href = "/schedule/personal_day?bdate={{$date_now}}&date=" + document.forms["schedule/personal_day"].date.value + '&gid='+gid + '&p=' + extra_param;
                                       }
                                       
                                       function clickFacilityGroupTree( form_name, param ){
                                           document.getElementById( 'popup_facility_group_tree_iframe' ).style.display= 'none';
                                       
                                           if ( param['fagid'] != '0' && param['fagid'] != 'f' ) {
                                               if( param['fagid'] == 'r' || param['extra_param'] != 0 ){
                                                   updateDropdownButtonTitle( param['name'] );
                                               }
                                               else{
                                                   updateDropdownButtonTitle( param['name']);
                                               }
                                               changeDropDownWidth( 'wrap_dropdown_facility_currentmain' );
                                               location.href = "/schedule/personal_day?bdate={{$date_now}}&date=" + document.forms["schedule/personal_day"].date.value + '&gid=f' + param['fagid']+ '&p='+param['extra_param'];
                                           }else {
                                               updateDropdownButtonTitle( param['name'] );
                                               changeDropDownWidth( 'wrap_dropdown_facility_currentmain' );
                                               location.href = "/schedule/personal_day?bdate={{$date_now}}&date=" + document.forms["schedule/personal_day"].date.value + '&gid=f' + '&p='+param['extra_param'];
                                           }
                                       }
                                       
                                       function updateDropdownButtonTitle( newTitle )
                                       {
                                           var node = window.document.getElementById( 'dropdown_current_a' );
                                           node.innerHTML = unescape( newTitle );
                                           changeDropDownWidth( 'wrap_dropdown_facility_current' );
                                       }
                                       
                                    </script>
                                    <link href="{{asset('/css/fag_tree.css')}}" rel="stylesheet" type="text/css">
                                    <table id="group-selectmain" border="0" cellspacing="0" cellpadding="0" class="wrap_dropdown_menu">
                                       <tbody>
                                          <tr height="20" >
                                             <td id="titlemain" class="dropdown_menu_current" height="20" nowrap></td>
                                             <td id="user-buttonmain" class="dropdown_menu_user" style="margin:0px;paddig:0px;" valign="center" aligh="left" width="37" height="20">
                                                <img src="{{asset('/img/user-dropdown30x20.gif')}}" style="margin:0px;paddig:0px;" border="0"/>
                                             </td>
                                             <td id="facility-buttonmain" class="dropdown_menu_facility" style="margin:0px;paddig:0px;" valign="center" aligh="left" width="37" height="20">
                                                <img src="{{asset('/img/facility-dropdown30x20.gif')}}" style="margin:0px;paddig:0px;" border="0"/>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <div id="user-popupmain" class="wrap_dropdown_option"></div>
                                    <div id="facility-popupmain" class="wrap_dropdown_option"></div>
                                    <div id="dummy-popupmain" class="wrap_dropdown_option"></div>
                                    <div></div>
                                    <div id="facility-popupmain-dummy_root" style="border:1px solid #000000; top:-10000px; left:-10000px; position:absolute; background-color:#FFFFFF; overflow:scroll; width:1px;height:1px;">
                                       <div id="facility-popupmain-dummy_tree_wrap_tree1" class="wrap_tree1">
                                          <div id="facility-popupmain-dummy_tree_wrap_tree2" class="wrap_tree2">
                                             <div id="facility-popupmain-dummy_tree"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <script type="text/javascript">
                                       (function () {
                                       
                                           var group_select_id    = 'group-selectmain';
                                           var title_id           = 'titlemain';
                                           var user_button_id     = 'user-buttonmain';
                                           var facility_button_id = 'facility-buttonmain';
                                           var user_popup_id      = 'user-popupmain';
                                           var facility_popup_id  = 'facility-popupmain';
                                           var is_multi_view      = true;
                                       
                                           var dropdown = new GRN_DropdownMenu(
                                               group_select_id, title_id, user_button_id, facility_button_id,
                                               GRN_DropdownMenu.prototype.PreferOrganization,
                                               user_popup_id, facility_popup_id,
                                               clickOrganizationCallback, clickFacilityGroupCallback,
                                               "/garoon3/grn/html/space.html?20200925.text" );
                                       
                                           function updateTitle( title ) {
                                               var old_width = jQuery('#' + group_select_id).outerWidth();
                                               jQuery('#' + group_select_id).css( {'width':''} );
                                               jQuery('#' + title_id).html( title );
                                               if( old_width > jQuery('#' + group_select_id).outerWidth() ) {
                                                   jQuery('#' + group_select_id).css( { 'width': old_width + 'px'} );
                                               }
                                           }
                                       
                                           function clickOrganizationCallback( group_item ) {
                                               return function(){
                                                   updateTitle( group_item.name )
                                                   dropdown.organization.hide();
                                                   
                                                   if (is_multi_view) {
                                                       
                                                       jQuery(document).trigger("scheduler.select_user_org_facility_dropdownlist.select", {gid: group_item.gid, target: dropdown});
                                                       return;
                                                   }
                                                   
                                                   location.href = "/index?"+ '&bdate=' + document.forms["schedule/personal_day"].bdate.value + '&gid='+group_item.gid;
                                               }
                                           }
                                       
                                           function clickFacilityGroupCallback( node ) {
                                               if( node.extra_param ) { //よく使う施設グループ or 最近選択した施設グループ
                                                   updateTitle( node.label );
                                               }
                                               else {
                                                   if( node.oid == 'f' ) {
                                                       updateTitle( '(All facilities)' );
                                                   }else{
                                                       updateTitle(  node.label );
                                                   }
                                               }
                                               dropdown.facility.hide();
                                       
                                               var oid = node.oid;
                                               if( oid.substr(0, 1) == 'x' ) {
                                                   oid = oid.substr( 1 );
                                               }
                                       
                                               if (is_multi_view) {
                                                   var gid = "f" + oid;
                                                   jQuery(document).trigger("scheduler.select_user_org_facility_dropdownlist.select", {
                                                       gid: gid,
                                                       target: dropdown,
                                                       node: node
                                                   });
                                                   return;
                                               }
                                               
                                               location.href = "/index?"+ '&bdate=' + document.forms["schedule/personal_day"].bdate.value + '&eid='+oid + '&p='+node.extra_param;
                                           }
                                           dropdown.initializeOrganization(
                                               new Array(
                                                   {!!$department!!}
                                                        ) );
                                       
                                           var group_select_width = dropdown.organization.getWidth( jQuery('#' + title_id).outerWidth() );
                                           jQuery('#' + group_select_id).css( {'width': group_select_width +"px"} );
                                       
                                           dropdown.updateTitle = updateTitle;
                                       
                                           dropdown.initializeFacilityGroup( { 'page_name': "portletmain",
                                                                               'ajax_path':'/api/accessible_facility_tree?',
                                                                               'csrf_ticket':'4f1f81bbb57ead9538ace7be94f376b7',
                                                                               'callback':clickFacilityGroupCallback,
                                                                               'selectedOID':"",
                                                                               'title_width': jQuery('#' + title_id).outerWidth(),
                                                                               'node_info':
                                                                               [{!!$equipment!!}]
                                                                             });
                                       }());
                                       
                                    </script>
                                 </td>
                                 <td nowrap="nowrap">
                                    <script>
                                       grn.component.url.PAGE_PREFIX = "";
                                       grn.component.url.PAGE_EXTENSION = "";
                                       grn.component.url.STATIC_URL = "/garoon3";
                                       grn.component.url.BUILD_DATE = "20200925.text";
                                    </script>    
                                    <script src="{{asset('/js/window_simple_dialog.js')}}" type="text/javascript"></script>
                                    <script id="template_window_simple_dialog_v2" type="text/template">
                                       <div class="subWindowBase-grn" id="window_dialog_v2" role="dialog">
                                           <div class="subwindow_title_grn" id="window_dialog_header">
                                             <span class="subwindow_title_base_grn nowrap-grn"><a href="javascript:void(0)" id="back_button" class="icon_back_grn icon_inline_grn mLeft10">Back</a><h2 id="window_title" class="subWindowTitleText-grn nowrap-grn inline_block_grn"></h2></span>
                                             <a href="javascript:;" role="button" title="Close" aria-label="Close">
                                                 <div id="window_dialog_close" class="subWindowClose-grn"></div>
                                             </a>
                                             <div class="clear_both_0px"></div>
                                           </div>
                                           <div class="subWindowContent-grn" id="window_content">
                                               <div class="content" style="min-height: 120px;">
                                                   <div class="tAlignCenter-grn" style=" margin-top: 30px; margin-right: auto; margin-left: auto; width: 570px; height: 90px;">
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                    </script>
                                    <script id="template_window_simple_dialog_v1" type="text/template">
                                       <div class="subWindowBase-grn" id="window_dialog_v1" style="display:none;">
                                           <table class="subWindowTable-grn">
                                               <tr id="window_dialog_header">
                                                   <td class="subWindowTitleLeft-grn"><div id="window_title" class="subWindowTitleText-grn"></div></td>
                                                   <td class="subWindowTitleRight-grn"><div id="window_dialog_close"  class="subWindowClose-grn" title="Close"></div></td>
                                               </tr>
                                               <tr>
                                                   <td colspan="2">
                                                       <div class="subWindowContent-grn" id="window_content">
                                                           <img src="{{asset('/img/spinner.gif')}}" border="0" alt="">
                                                       </div>
                                                   </td>
                                               </tr>
                                           </table>
                                       </div>
                                    </script>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                     <td class="v-allign-middle" align="center">
                        <span class="displaydate">{{date('D, F d, Y',strtotime($date_now))}}</span>
                        <span class="viewSubCalendar-grn">
                        <span id="showIcon-grn" class="showIconOff-grn" title="Show calendar">
                        <span class="subCalendar-grn" id="subCalendar-grn-image"></span>
                        <img src="{{asset('/img/spinner.gif')}}" id="wait_image" class="showicon_spinner_grn" name="wait_image" style="display:none">
                        </span>
                        </span>
                     </td>
                     <td class="v-allign-middle" nowrap="nowrap" align="right" style="width:1%">
                        <div class="moveButtonBlock-grn">
                           <span class="moveButtonBase-grn"><a href="/schedule/personal_day?bdate=2021-01-10&amp;uid=&amp;gid=15&amp;event=&amp;event_date=&amp;pid=&amp;sp=&amp;search_text=&amp;p="  title="Previous week"><span class="moveButtonArrowLeftTwo-grn"></span></a></span><span class="moveButtonBase-grn"><a href="/schedule/personal_day?bdate=2021-01-12&amp;uid=&amp;gid=15&amp;event=&amp;event_date=&amp;pid=&amp;sp=&amp;search_text=&amp;p=" title="Previous day"><span class="moveButtonArrowLeft-grn"></span></a></span><span class="moveButtonBase-grn" title=""><a href="/schedule/personal_day?bdate=&amp;uid=&amp;gid=15&amp;event=&amp;event_date=&amp;pid=&amp;sp=&amp;search_text=&amp;p=">Today</a></span><span class="moveButtonBase-grn"><a href="/schedule/personal_day?bdate=2021-01-14&amp;uid=&amp;gid=15&amp;event=&amp;event_date=&amp;pid=&amp;sp=&amp;search_text=&amp;p=" title="Next day"><span class="moveButtonArrowRight-grn"></span></a></span><span class="moveButtonBase-grn"><a href="/schedule/personal_day?bdate=2021-01-17&amp;uid=&amp;gid=15&amp;event=&amp;event_date=&amp;pid=&amp;sp=&amp;search_text=&amp;p=" title="Next week"><span class="moveButtonArrowRightTwo-grn"></span></a></span>
                        </div>
                     </td>
                  </tr>
               </table>
            </div>
            <script language="javascript" type="text/javascript">
               <!--
               var grn_schedule_navi_command_on;
               
               var grn_schedule_navi_cal_url = "/api/command_navi_calendar_display?";
               
               var title_show_calendar = "Show calendar";
               var title_hide_calendar = "Hide calendar";
               
               function grn_schedule_navi_cal()
               {
                   var icon_tag = window.document.getElementById( 'showIcon-grn' );
                   if(YAHOO.util.Dom.hasClass('showIcon-grn', 'showIconOff-grn'))
                   {
                       jQuery('#schedule_calendar').show();
               
                       jQuery('#wait_image').show();
                       if(window.document.getElementById( 'subCalendar-grn-image' ))
                       {
                           jQuery('#subCalendar-grn-image').hide();
                       }
                       
                       grn_schedule_navi_command_on = true;
                       grn_schedule_send_req('on');
                       
                       YAHOO.util.Dom.removeClass('showIcon-grn', 'showIconOff-grn');
                       YAHOO.util.Dom.addClass('showIcon-grn', 'showIconOn-grn');
                       
                       icon_tag.setAttribute("title", title_hide_calendar);
                   }
                   else
                   {
                       if(YAHOO.util.Dom.hasClass('showIcon-grn', 'showIconOn-grn'))
                       {
                           jQuery('#schedule_calendar').hide();
               
                           grn_schedule_navi_command_on = false;
                           grn_schedule_send_req('off');
                           
                           YAHOO.util.Dom.removeClass('showIcon-grn', 'showIconOn-grn');
                           YAHOO.util.Dom.addClass('showIcon-grn', 'showIconOff-grn');
                           
                           icon_tag.setAttribute("title", title_show_calendar);
                       }
                   }
               }
               
               function grn_schedule_send_req(navi_cal_display_flag)
               {
                   var post_body = jQuery.param({navi_cal_display_flag:navi_cal_display_flag});
                   post_body += '&csrf_ticket=4f1f81bbb57ead9538ace7be94f376b7';
                   var oj = new jQuery.ajax({
                           url         : grn_schedule_navi_cal_url,
                           type        : 'POST',
                           data        : post_body,
                           complete    : grn_schedule_onloaded
                       });
               }
               
               function grn_schedule_onloaded(jqXHR)
               {
                   var headers = jqXHR.getAllResponseHeaders();
                   var regex = /X-Cybozu-Error/i;
                   if( headers.match( regex ) )
                   {
                       document.body.innerHTML = jqXHR.responseText;
                       return false;
                   }
               
                   jQuery('#wait_image').hide();
                   if(window.document.getElementById( 'subCalendar-grn-image' ))
                   {
                       jQuery('#subCalendar-grn-image').show();
                   }
               
                   if (grn_schedule_navi_command_on)
                   {
                       jQuery('#navi_cal_label_on').show();
                       jQuery('#navi_cal_label_off').hide();
                   }
                   else
                   {
                       jQuery('#navi_cal_label_off').show();
                       jQuery('#navi_cal_label_on').hide();
                   }
               }
               
               YAHOO.util.Event.addListener( window.document.getElementById( 'showIcon-grn' ), 'click', grn_schedule_navi_cal );
               //-->
               
            </script>                
            <div class="day_week_calendar_navi_grn">
               <center>
                  <img src="{{asset('/img/spinner.gif')}}" id="wait_image" name="wait_image" style="display:none">
                  <div id="schedule_calendar" style="display:none">
                     {!!$calendar_html!!}
                  </div>
               </center>
               <script language="javascript" type="text/javascript">
                  <!--
                  function doMoveCalednar( move_date, onComplete )
                  {
                      var url = "/api/schedule_navi_calendar?location=schedule%2Fpersonal_day&p=&search_text=&uid=&gid=15&event=&event_date=&vwdate={{$date_now}}"+'&cndate='+move_date;
                      ajax = new jQuery.ajax({
                          url:                url,
                          type:               'GET',
                          async:               true,
                          success: function(result) {
                              jQuery('#schedule_calendar').html(result);
                          },
                          complete:           onComplete
                      });
                  }
                  
                  //-->
                  
               </script>                
            </div>
            <div class="cb-user-picker"></div>
            <div class="personal_day_calendar_base_header js_customization_schedule_view_type_DAY">
               <div class="personal_day_calendar_header">
                  <table cellpadding="0" cellspacing="0" class="personal_day_calendar_event_cell without_time_events_area"id="notime_event">
                     <tr class="js_customization_schedule_user_id">
                        <td class="personal_day_calendar_event_item without_time_event_cell">
                           <div id="personal_day_event_notime" class="personal_day_event_notime without_time_event_list"></div>
                        </td>
                     </tr>
                  </table>
                  <table cellpadding="0" cellspacing="0" class="personal_day_calendar_banner_cell all_day_area"
                     id="banner_event">
                  </table>
               </div>
            </div>
            <div class="showtime personal_day_calendar_base" id="time_event">
               <div id="personal_calendar_list" class="personal_calendar_list normal_events_area">
                  <table cellpadding="0" cellspacing="0" style="width:100%;">
                     <tr>
                        <td class="personal_day_calendar_date">
                           <div id=""
                              class="personal_day_event_list last_date" style="width:100%;">
                           </div>
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
      </div>
      <input type="hidden" name="gid" value="15">
      <input type="hidden" name="bdate" value="{{$date_now}}">
      </form>
   </div>
</div>
<script src="{{asset('/js/dragdrop-min.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/selector-min.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/date.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/pubsub.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/user_org_facility_suggest_list.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/util.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/select_user_org_facility_dialog.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/user_facility_picker.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/select_user_org_facility_dropdownlist.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/member_list.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/confirm_dialog.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/time_sheet.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/calendar_item_renderer.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/normal_event_day_stack_list.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/normal_event_renderer.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/without_time_event_renderer.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/banner_renderer.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/event_tooltip.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/date_navigator.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/mini_calendar_navigator.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/event_resize.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/calendar_footer.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/main.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/pulldown_menu.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/member_select_list.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/member_add.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/facility_add.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/dist/schedule.js')}}" type="text/javascript"></script>
<script>
   /**
    * Initialize calendar view
    */
   jQuery(function initialize_day_view() {
       new grn.js.page.schedule.DayIndex({"enableDateFooter":false,"beginDate":"{{$date_now}}","calendarWeekStart":0,"dates":[{"value":"{{$date_now}}","text":"Wed, 13 January","text_full":"Wed, January 13, 2021","type":"s_date_wednesday"}],"eventTimeUnit":10,"today":{"value":"{{$date_now}}","text":"Wed, January 13, 2021"},"numberOfDays":1,"shortDateFormat":"&&wdayshort&&, &&mday&& &&monthfull&&","minTime":8,"maxTime":19,"gid":"{{isset($_GET['uid'])? $_GET['uid'] : Auth::guard('member')->user()->id}}","eid":"{{isset($_GET['eid'])?$_GET['eid'] : ''}}","refererKey":"f84cf7b20b85f8b8ede7d8a348dda5db","enableDragAndDrop":1,"locale":"en","uid":""});
   });
   
   
</script>
<script text="text/javascript">
   // namespace
   grn.base.namespace("grn.component.error_handler");
   
   (function () {
       var G = grn.component.error_handler;
   
       G.show = function (request, my_callback) {
           var s = request.responseText;
           if (s != undefined) {
               var title = '';
               var msg = '';
               var json = null;
   
               try {
                   json = JSON.parse(s);
               } catch(e){}
   
               if (json) {
                   var show_backtrace = "";
                   title = '';//json.code;
   
                   if (show_backtrace) {
                       msg = msg + "<div style='height:145px; overflow: auto;'><table><tr><td>";
                   }
                   msg = msg + "<div><img border='0' src='/garoon3/grn/image/cybozu/warn100x60.gif?20200925.text'></div>";
                   msg = msg + "<div class='bold'>" + 'Error (' + json.code + ")</div><div>" + json.diagnosis + "</div><br>";
                   msg = msg + "<div class='bold'>" + 'Cause' + "</div><div>" + json.cause + "</div><br>";
                   msg = msg + "<div class='bold'>" + 'Countermeasure' + "</div><div>" + json.counter_measure + "</div>";
   
                   if (show_backtrace) {
                       msg = msg + "<br><hr>";
                       msg = msg + "<div class='bold'>( Beta/Debug only. by common.ini )</div><br>";
                       msg = msg + "<div class='bold'>Developer Info</div><br>";
                       if (json.developer_info) {
                           msg = msg + json.developer_info;
                       }
                       msg = msg + "<div class='bold'>Backtrace</div><br>";
                       msg = msg + "How to read backtraces(to be written) / How to read backtraces (to be written)<br>";
                       msg = msg + "<pre style='border:1px solid #6666ff; background:#eeeeff; padding:10px;'>";
                       if (json.backtrace) {
                           msg = msg + json.backtrace;
                       }
                       msg = msg + "</pre>";
                       msg = msg + "<br>$G_INPUT<br>";
                       msg = msg + "<pre style='border:1px solid #6666ff; background:#eeeeff; padding:10px;'>";
                       if (json.input) {
                           msg = msg + json.input;
                       }
                       msg = msg + "</pre>";
                       msg = msg + "</td></tr></table></div>";
                   }
               }
               else {
                   title = 'Error';
                   msg = grn_split_tags(s, 1000);
               }
           }
           else {
               title = "Error";
               msg = "Connection failed.";
           }
   
           GRN_MsgBox.show(msg, title, GRN_MsgBoxButtons.ok, {
               ui: [],
               caption: {
                   ok: 'OK'
               },
   
               callback: function (result, form) {
                   GRN_MsgBox._remove();
                   if (typeof my_callback != 'undefined') my_callback();
               }
           });
       };
       G.getHeader = function (request) {
           if (typeof(request.getAllResponseHeaders) == 'function') {
               return request.getAllResponseHeaders();
           }
           else {
               return request.getAllResponseHeaders; // for YAHOO.util.Connect.asyncRequest
           }
       };
       G.hasCybozuError = function (request) {
           var headers = G.getHeader(request);
           return (headers && headers.match(new RegExp(/X-Cybozu-Error/i))) ? true : false;
       };
       G.hasCybozuLogin = function (request) {
           var headers = G.getHeader(request);
           return (headers.match(new RegExp(/X-CybozuLogin/i)) || !headers.match(new RegExp(/X-Cybozu-User/i)));
       };
   })();
   
</script>
@stop
