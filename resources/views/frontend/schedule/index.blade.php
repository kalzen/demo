@extends('frontend.layouts.create_schedule')
@section('content')
<table class="global_navi_title" cellpadding="0" cellmargin="0" style="padding:0px;">
   <tbody>
      <tr>
         <td width="100%" valign="bottom" nowrap="">
            <div class="global_naviAppTitle-grn">
               <img src="{{asset('/img/schedule20.gif')}}" border="0" alt="">Scheduler
            </div>
            <div class="global_navi-viewChange-grn">
               <ul>
                  <li><a class="global_naviBackTab-viewChange-grn viewChangeLeft-grn" href="{{route('frontend.schedule.group_day')}}">Group day</a></li>
                  <li role="heading" aria-level="2" class="global_naviBack-viewChangeSelect-grn"><span>Group week</span></li>
                  <li><a class="global_naviBackTab-viewChange-grn" href="{{route('frontend.schedule.personal_day')}}">Day</a></li>
                  <li><a class="global_naviBackTab-viewChange-grn" href="{{route('frontend.schedule.personal_week')}}">Week</a></li>
                  <li><a class="global_naviBackTab-viewChange-grn" href="{{route('frontend.schedule.personal_month')}}">Month</a></li>
                  <li><a class="global_naviBackTab-viewChange-grn viewChangeRight-grn" href="{{route('frontend.schedule.personal_year')}}">Year</a></li>
               </ul>
            </div>
         </td>
         <td align="right" valign="bottom" nowrap="">
         </td>
      </tr>
   </tbody>
</table>
<div class="mainarea">
   <div class="mainareaSchedule-grn">
      <script src="{{asset('/js/schedule.js')}}" type="text/javascript"></script>
      <!--menubar-->
      <div id="menu_part">
         <div id="smart_main_menu_part">
            <span class="menu_item">
            <a href="/schedule/create?bdate={{date('Y-m-d')}}&amp;uid={{\Auth::guard('member')->user()->id}}">
            <img src="{{asset('/img/write20.png')}}" border="0" alt="">New</a>
            </span>
            &nbsp;
         </div>
         <div id="smart_rare_menu_part" style="white-space:nowrap;">
            <div class="search_navi">
               <form name="search" action="/schedule/index?">
                  <input id="type_search" type="hidden" name="type_search" value="user">
                  <div class="searchboxChangeTarget-grn">
                     <div class="searchbox-grn">
                        <div id="searchbox-schedule" class="input-text-outer-cybozu">
                           <input class="input-text-cybozu" placeholder="Search" type="text" value="{{isset($_GET['search_text']) ? $_GET['search_text'] : ''}}" name="search_text" autocomplete="off" value="" onkeypress="if(event.keyCode == 13) search_submit(document.getElementById('type_search').value); return event.keyCode != 13;">
                           <button class="searchbox-submit-cybozu" type="submit"></button>
                        </div>
                     </div>
                  </div>
               </form>
            </div>
         </div>
         <div class="clear_both_0px"></div>
      </div>
      <!--menubar_end-->
      <script language="JavaScript" type="text/javascript">
         <!--
         function __get_referer_bdate()
         {
             var referer_bdate = '';
             var param_elements = document.getElementsByName('bdate');
             for( var i = 0; i < param_elements.length; i++ )
             {
                 if (param_elements[i].getAttribute('value'))
                 {
                     referer_bdate = param_elements[i].getAttribute('value');
                     break;
                 }
             }
             return referer_bdate;
         }
         -->
      </script>
      <style type="text/css">
         .tableFixed{
         width:100%;
         table-layout:fixed;
         }
         .normalEvent {}
         .normalEventElement{}
         .showEventTitle{
         position:absolute;
         max-width: 300px;
         overflow:hidden;
         z-index: 20;
         }
         .hideEventTitle{}
         .hideEventTitle .normalEvent {
         overflow:hidden;
         vertical-align:top;
         }
         .userElement{
         overflow:hidden;
         }
         .nowrap_class{
         white-space:nowrap;
         padding:2px;
         overflow:hidden;
         }
      </style>
      <style type="text/css">
         .differ_tz_color{
         background-color:#FFDBDE;
         }
         .hide_event{
         display:none;
         }
      </style>
      <script type="text/javascript" language="javascript">
         var command_show_hide_absent_schedule_url = "{grn_pageurl page='schedule/command_show_hide_absent_schedule'}";
      </script>
      <form name="schedule/index" method="GET" action="/schedule/index?bdate={{$date_now}}&amp;uid=&amp;gid=15">
         <div class="margin_bottom">
            <table width="100%">
               <tbody>
                  <tr>
                     <td class="v-allign-middle" nowrap="nowrap">
                        <table cellspacing="0" cellpadding="0" border="0">
                           <tbody>
                              <tr>
                                 <td nowrap="nowrap">
                                    <script language="JavaScript" type="text/javascript">
                                       function setUserGroups( group_obj ) {
                                           @foreach($departments as $key=>$val)
                                           group_obj.appendItem( new GRN_GroupItem( '{{$val->id}}', '{{$val->name}}', 'window.parent.clickUserGroup', false, '' ) );
                                        
                                        @endforeach
                                         
                                       
                                       }
                                       function clickUserGroup( gid, name, extra_param ) {
                                           document.getElementById( 'popup_group_list_iframe' ).style.display= 'none';
                                           updateDropdownButtonTitle( name );
                                           location.href = "/schedule/index?bdate={{$date_now}}&date=" + document.forms["schedule/index"].date.value + '&gid='+gid + '&p=' + extra_param;
                                       }
                                       
                                       function clickFacilityGroupTree( form_name, param ){
                                           document.getElementById( 'popup_facility_group_tree_iframe' ).style.display= 'none';
                                       
                                           if ( param['fagid'] != '0' && param['fagid'] != 'f' ) {
                                               if( param['fagid'] == 'r' || param['extra_param'] != 0 ){
                                                   updateDropdownButtonTitle( param['name'] );
                                               }
                                               else{
                                                   updateDropdownButtonTitle( param['name']);
                                               }
                                               changeDropDownWidth( 'wrap_dropdown_facility_current' );
                                               location.href = "/schedule/index?bdate={{$date_now}}&date=" + document.forms["schedule/index"].date.value + '&gid=f' + param['fagid']+ '&p='+param['extra_param'];
                                           }else {
                                               updateDropdownButtonTitle( param['name'] );
                                               changeDropDownWidth( 'wrap_dropdown_facility_current' );
                                               location.href = "/schedule/index?bdate={{$date_now}}&date=" + document.forms["schedule/index"].date.value + '&gid=f' + '&p='+param['extra_param'];
                                           }
                                       }
                                       
                                       function updateDropdownButtonTitle( newTitle )
                                       {
                                           var node = window.document.getElementById( 'dropdown_current_a' );
                                           node.innerHTML = unescape( newTitle );
                                           changeDropDownWidth( 'wrap_dropdown_facility_current' );
                                       }
                                       
                                    </script>
                                    <link href="{{asset('assets/css/fag_tree.css')}}" rel="stylesheet" type="text/css">
                                    <table id="group-select" border="0" cellspacing="0" cellpadding="0" class="wrap_dropdown_menu" style="width: 332px;">
                                       <tbody>
                                          <tr height="20">
                                             <td id="title" class="dropdown_menu_current" height="20" nowrap="">@if(isset($_GET['gid']) &&  $_GET['gid'] > 0 ) {{\App\Department::find($_GET['gid'])->name}} @elseif(isset($_GET['eid']) && !isset($_GET['gid'])) {{\App\Equipment::find($_GET['eid'])->name}} @else Tìm theo phòng ban @endif</td>
                                             <td id="user-button" class="dropdown_menu_user" style="margin:0px;paddig:0px;" valign="center" aligh="left" width="37" height="20">
                                                <img src="{{asset('/img/user-dropdown30x20.gif')}}" style="margin:0px;paddig:0px;" border="0">
                                             </td>
                                             <td id="facility-button" class="dropdown_menu_facility" style="margin:0px;paddig:0px;" valign="center" aligh="left" width="37" height="20">
                                                <img src="{{asset('/img/facility-dropdown30x20.gif')}}" style="margin:0px;paddig:0px;" border="0">
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <div id="user-popup" class="wrap_dropdown_option"></div>
                                    <div id="facility-popup" class="wrap_dropdown_option"></div>
                                    <div id="dummy-popup" class="wrap_dropdown_option"></div>
                                    <div></div>
                                    <div id="facility-popup-dummy_root" style="border:1px solid #000000; top:-10000px; left:-10000px; position:absolute; background-color:#FFFFFF; overflow:scroll; width:1px;height:1px;">
                                      <div id="facility-popup-dummy_tree_wrap_tree1" class="wrap_tree1">
                                        <div id="facility-popup-dummy_tree_wrap_tree2" class="wrap_tree2">
                                          <div id="facility-popup-dummy_tree"></div>
                                        </div>
                                      </div>
                                    </div>
                                    <script type="text/javascript">
                                       (function () {
                                       
                                           var group_select_id    = 'group-select';
                                           var title_id           = 'title';
                                           var user_button_id     = 'user-button';
                                           var facility_button_id = 'facility-button';
                                           var user_popup_id      = 'user-popup';
                                           var facility_popup_id  = 'facility-popup';
                                           var is_multi_view      = false;
                                       
                                           var dropdown = new GRN_DropdownMenu(
                                               group_select_id, title_id, user_button_id, facility_button_id,
                                               GRN_DropdownMenu.prototype.PreferOrganization,
                                               user_popup_id, facility_popup_id,
                                               clickOrganizationCallback, clickFacilityGroupCallback,
                                               "" );
                                       
                                           function updateTitle( title ) {
                                               var old_width = jQuery('#' + group_select_id).outerWidth();
                                               jQuery('#' + group_select_id).css( {'width':''} );
                                               jQuery('#' + title_id).html( title );
                                               if( old_width > jQuery('#' + group_select_id).outerWidth() ) {
                                                   jQuery('#' + group_select_id).css( { 'width': old_width + 'px'} );
                                               }
                                           }
                                       
                                           function clickOrganizationCallback( group_item ) {
                                               return function(){
                                                   updateTitle( group_item.name )
                                                   dropdown.organization.hide();
                                                   
             
                                                   location.href = "/schedule/index?"+ '&bdate=' + document.forms["schedule/index"].bdate.value + '&gid='+group_item.gid;
                                               }
                                           }
                                       
                                           function clickFacilityGroupCallback( node ) {
                                               if( node.extra_param ) { 
                                                   updateTitle( node.label );
                                               }
                                               else {
                                                   if( node.oid == 'f' ) {
                                                       updateTitle( '(All facilities)' );
                                                   }else{
                                                       updateTitle(  node.label );
                                                   }
                                               }
                                               dropdown.facility.hide();
                                       
                                               var oid = node.oid;
                                               window.location.href = "/schedule/index?"+ '&bdate=' + document.forms["schedule/index"].bdate.value + '&eid='+oid;
                                             }
                                           dropdown.initializeOrganization(
                                               new Array(
                                                   {!!$department!!} ) );
                                       
                                           var group_select_width = dropdown.organization.getWidth( jQuery('#' + title_id).outerWidth() );
                                           jQuery('#' + group_select_id).css( {'width': group_select_width +"px"} );
                                       
                                           dropdown.updateTitle = updateTitle;
                                       
                                           dropdown.initializeFacilityGroup( { 'page_name': "schedule/index",
                                                                               'ajax_path':'/schedule/json/accessible_facility_tree?',
                                                                               'csrf_ticket':'cd9933a8846474545be419350224e341',
                                                                               'callback':clickFacilityGroupCallback,
                                                                               'selectedOID':"",
                                                                               'title_width': jQuery('#' + title_id).outerWidth(),
                                                                               'node_info':
                                                                               [{!!$equipment!!}]
                                                                             });
                                       }());
                                       
                                    </script>
                                 </td>
                                 <td nowrap="nowrap"><a class="selectPulldownSub-grn" href="javascript:void(0);" onclick="javascript:popupWin('/schedule/popup_user_select?plugin_session_name=schedule%2Findex&amp;plugin_data_name=access_plugin&amp;is_post_message=&amp;selected_tid=&amp;form_name=schedule%2Findex&amp;select_name=sUID%5B%5D&amp;app_id=schedule&amp;return_page=&amp;plid=&amp;system=0&amp;include_org=1&amp;system_display=0&amp;no_multiple=0&amp;session=start;send_cgi_parameter=0&amp;multi_apply=1&amp;require_role_tab=0&amp;is_calendar=0&amp;show_group_role=1&amp;require_dynamic_roles=0&amp;require_administrator_role=0','html','','',0,0,1,1,0,1);" title="Select users"><img src="{{asset('/img/blankB16.png')}}" border="0" alt=""></a></td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                     <td class="v-allign-middle" align="center">
                        <span class="displaydate">{{date('D, F d, Y',strtotime($date_now))}}</span>
                        <span class="viewSubCalendar-grn">
                        <span id="showIcon-grn" class="showIconOff-grn" title="Show calendar">
                        <span class="subCalendar-grn"></span>
                        </span>
                        </span>
                     </td>
                     <td class="v-allign-middle" nowrap="nowrap" align="right">
                        <div class="moveButtonBlock-grn">
                            <span class="moveButtonBase-grn"><a href="{{route('frontend.schedule.index',['bdate'=>$prev_week,'gid'=>isset($_GET['gid'])? $_GET['gid'] :'','eid'=>isset($_GET['eid'])? $_GET['eid']:''])}}" title="Previous week"><span class="moveButtonArrowLeftTwo-grn"></span></a></span><span class="moveButtonBase-grn"><a href="{{route('frontend.schedule.index',['bdate'=>date('Y-m-d',strtotime('-1 day',strtotime($date_now))),'gid'=>isset($_GET['gid'])? $_GET['gid']:'','eid'=>isset($_GET['eid'])? $_GET['eid'] :''])}}" title="Previous day"><span class="moveButtonArrowLeft-grn"></span></a></span><span class="moveButtonBase-grn" title=""><a href="{{route('frontend.schedule.index',['bdate'=>$date_now,'gid'=>isset($_GET['gid'])? $_GET['gid'] : '','eid'=>isset($_GET['eid'])? $_GET['eid'] :''])}}">Today</a></span><span class="moveButtonBase-grn"><a href="{{route('frontend.schedule.index',['bdate'=>date('Y-m-d',strtotime('+1 day',strtotime($date_now))),'gid'=>isset($_GET['gid'])? $_GET['gid'] :'','eid'=>isset($_GET['eid'])? $_GET['eid']:''])}}" title="Next day"><span class="moveButtonArrowRight-grn"></span></a></span><span class="moveButtonBase-grn"><a href="{{route('frontend.schedule.index',['bdate'=>$next_week,'gid'=>isset($_GET['gid'])? $_GET['gid']:'','eid'=>isset($_GET['eid'])? $_GET['eid']:''])}}" title="Next week"><span class="moveButtonArrowRightTwo-grn"></span></a></span>
                        </div>
                     </td>
                  </tr>
               </tbody>
            </table>
         </div>
         <script language="javascript" type="text/javascript">
            <!--
            var grn_schedule_navi_command_on;
            
            var grn_schedule_navi_cal_url = "/api/command_navi_calendar_display?";
            
            var title_show_calendar = "Show calendar";
            var title_hide_calendar = "Hide calendar";
            
            function grn_schedule_navi_cal()
            {
                var icon_tag = window.document.getElementById( 'showIcon-grn' );
                if(YAHOO.util.Dom.hasClass('showIcon-grn', 'showIconOff-grn'))
                {
                    jQuery('#schedule_calendar').show();
            
                    jQuery('#wait_image').show();
                    if(window.document.getElementById( 'subCalendar-grn-image' ))
                    {
                        jQuery('#subCalendar-grn-image').hide();
                    }
                    
                    grn_schedule_navi_command_on = true;
                    grn_schedule_send_req('on');
                    
                    YAHOO.util.Dom.removeClass('showIcon-grn', 'showIconOff-grn');
                    YAHOO.util.Dom.addClass('showIcon-grn', 'showIconOn-grn');
                    
                    icon_tag.setAttribute("title", title_hide_calendar);
                }
                else
                {
                    if(YAHOO.util.Dom.hasClass('showIcon-grn', 'showIconOn-grn'))
                    {
                        jQuery('#schedule_calendar').hide();
            
                        grn_schedule_navi_command_on = false;
                        grn_schedule_send_req('off');
                        
                        YAHOO.util.Dom.removeClass('showIcon-grn', 'showIconOn-grn');
                        YAHOO.util.Dom.addClass('showIcon-grn', 'showIconOff-grn');
                        
                        icon_tag.setAttribute("title", title_show_calendar);
                    }
                }
            }
            
            function grn_schedule_send_req(navi_cal_display_flag)
            {
                var post_body = jQuery.param({navi_cal_display_flag:navi_cal_display_flag});
                post_body += '&csrf_ticket=cd9933a8846474545be419350224e341';
                var oj = new jQuery.ajax({
                        url         : grn_schedule_navi_cal_url,
                        type        : 'POST',
                        data        : post_body,
                        complete    : grn_schedule_onloaded
                    });
            }
            
            function grn_schedule_onloaded(jqXHR)
            {
                var headers = jqXHR.getAllResponseHeaders();
                var regex = /X-Cybozu-Error/i;
                if( headers.match( regex ) )
                {
                    document.body.innerHTML = jqXHR.responseText;
                    return false;
                }
            
                jQuery('#wait_image').hide();
                if(window.document.getElementById( 'subCalendar-grn-image' ))
                {
                    jQuery('#subCalendar-grn-image').show();
                }
            
                if (grn_schedule_navi_command_on)
                {
                    jQuery('#navi_cal_label_on').show();
                    jQuery('#navi_cal_label_off').hide();
                }
                else
                {
                    jQuery('#navi_cal_label_off').show();
                    jQuery('#navi_cal_label_on').hide();
                }
            }
            
            YAHOO.util.Event.addListener( window.document.getElementById( 'showIcon-grn' ), 'click', grn_schedule_navi_cal);
            //-->
            
         </script>
         <center>
            <img src="{{asset('/img/spinner.gif')}}" id="wait_image" name="wait_image" style="display:none">
            <div id="schedule_calendar" style="display:none">
               {!!$calendar_html!!}
            </div>
         </center>
         <script language="javascript" type="text/javascript">
            <!--
            function doMoveCalednar( move_date, onComplete )
            {
                var url = "/api/schedule_navi_calendar?location=schedule%2Findex&p=&search_text=&uid=&gid=&event=&event_date=&vwdate={{$date_now}}"+'&cndate='+move_date;
                ajax = new jQuery.ajax({
                    url:                url,
                    type:               'GET',
                    async:               true,
                    success: function(result) {
                        jQuery('#schedule_calendar').html(result);
                    },
                    complete:           onComplete
                });
            }
            
            //-->
            
         </script>
         <div class="clear_both_1px">&nbsp;</div>
         <table id="schedule_groupweek" class="calendar scheduleWrapper scheduleWrapperGroupWeek group_week  group_week_calendar js_customization_schedule_view_type_GROUP_WEEK">
            <colgroup>
               <col class="group_week_calendar_column_first">
               <col class="group_week_calendar_column_common">
               <col class="group_week_calendar_column_common">
               <col class="group_week_calendar_column_common">
               <col class="group_week_calendar_column_common">
               <col class="group_week_calendar_column_common">
               <col class="group_week_calendar_column_common">
               <col class="group_week_calendar_column_common">
            </colgroup>
            <tbody id="event_list">
               {!!$html!!}                               
            </tbody>
         </table>
         <input type="hidden" name="bdate" value="{{$date_now}}">
      </form>
   </div>
   <script src="{{asset('/js/dragdrop-min.js')}}" type="text/javascript"></script>
   <script src="{{asset('/js/selector-min.js')}}" type="text/javascript"></script>
   <script text="text/javascript">
      // namespace
      grn.base.namespace("grn.component.error_handler");
      
      (function () {
          var G = grn.component.error_handler;
      
          G.show = function (request, my_callback) {
              var s = request.responseText;
              if (s != undefined) {
                  var title = '';
                  var msg = '';
                  var json = null;
      
                  try {
                      json = JSON.parse(s);
                  } catch(e){}
      
                  if (json) {
                      var show_backtrace = "";
                      title = '';//json.code;
      
                      if (show_backtrace) {
                          msg = msg + "<div style='height:145px; overflow: auto;'><table><tr><td>";
                      }
                      msg = msg + "<div><img border='0' src='/img/warn100x60.gif'></div>";
                      msg = msg + "<div class='bold'>" + 'Error (' + json.code + ")</div><div>" + json.diagnosis + "</div><br>";
                      msg = msg + "<div class='bold'>" + 'Cause' + "</div><div>" + json.cause + "</div><br>";
                      msg = msg + "<div class='bold'>" + 'Countermeasure' + "</div><div>" + json.counter_measure + "</div>";
      
                      if (show_backtrace) {
                          msg = msg + "<br><hr>";
                          msg = msg + "<div class='bold'>( Beta/Debug only. by common.ini )</div><br>";
                          msg = msg + "<div class='bold'>Developer Info</div><br>";
                          if (json.developer_info) {
                              msg = msg + json.developer_info;
                          }
                          msg = msg + "<div class='bold'>Backtrace</div><br>";
                          msg = msg + "How to read backtraces(to be written) / How to read backtraces (to be written)<br>";
                          msg = msg + "<pre style='border:1px solid #6666ff; background:#eeeeff; padding:10px;'>";
                          if (json.backtrace) {
                              msg = msg + json.backtrace;
                          }
                          msg = msg + "</pre>";
                          msg = msg + "<br>$G_INPUT<br>";
                          msg = msg + "<pre style='border:1px solid #6666ff; background:#eeeeff; padding:10px;'>";
                          if (json.input) {
                              msg = msg + json.input;
                          }
                          msg = msg + "</pre>";
                          msg = msg + "</td></tr></table></div>";
                      }
                  }
                  else {
                      title = 'Error';
                      msg = grn_split_tags(s, 1000);
                  }
              }
              else {
                  title = "Error";
                  msg = "Connection failed.";
              }
      
              GRN_MsgBox.show(msg, title, GRN_MsgBoxButtons.ok, {
                  ui: [],
                  caption: {
                      ok: 'OK'
                  },
      
                  callback: function (result, form) {
                      GRN_MsgBox._remove();
                      if (typeof my_callback != 'undefined') my_callback();
                  }
              });
          };
          G.getHeader = function (request) {
              if (typeof(request.getAllResponseHeaders) == 'function') {
                  return request.getAllResponseHeaders();
              }
              else {
                  return request.getAllResponseHeaders; // for YAHOO.util.Connect.asyncRequest
              }
          };
          G.hasCybozuError = function (request) {
              var headers = G.getHeader(request);
              return (headers && headers.match(new RegExp(/X-Cybozu-Error/i))) ? true : false;
          };
          G.hasCybozuLogin = function (request) {
              var headers = G.getHeader(request);
              return (headers.match(new RegExp(/X-CybozuLogin/i)) || !headers.match(new RegExp(/X-Cybozu-User/i)));
          };
      })();
      
   </script>
</div>
@stop
@section('script')
@parent
<script>
   $('body').delegate('.button-next','click',function(){
       var date = $(this).data('next');
       var member_id = $('#select_member').val();
       $.ajax({
              type: "POST",
              url: '/api/next-schedule',
              data: {date:date,member_id:member_id},
              success: function(response){
                   $('.table-schedule').html(response.html);
                   $('#pagination').html(`
                   <a href="#" class="button-prev" data-prev='`+response.start+`'><i class="fas fa-caret-left"></i></a>
                   <a href="#" class="button-next" data-next='`+response.end+`'><i class="fas fa-caret-right"></i></a>
                   `);
               }
       });   
   });
   $('body').delegate('.button-prev','click',function(){
       var date = $(this).data('prev');
       var member_id = $('#select_member').val();
       $.ajax({
              type: "POST",
              url: '/api/prev-schedule',
              data: {date:date,member_id:member_id},
              success: function(response){
                   $('.table-schedule').html(response.html);
                   $('#pagination').html(`
                   <a href="#" class="button-prev" data-prev='`+response.start+`'><i class="fas fa-caret-left"></i></a>
                   <a href="#" class="button-next" data-next='`+response.end+`'><i class="fas fa-caret-right"></i></a>
                   `);
               }
       });   
   });
   $('#select_member').change(function(){
       var date = $('.button-prev').data('prev');
       var member_id = $(this).val();
       $.ajax({
              type: "POST",
              url: '/api/change-schedule',
              data: {date:date,member_id:member_id},
              success: function(response){
                   $('.table-schedule').html(response.html);
                   $('#list_todolist').html(response.list_html);
               }
       });
   })
   $('.add-todolist').click(function(){
       $('#modal_create_todolist').modal('show');
   })
   $('#create_todolist').submit(function(e){
       e.preventDefault();
       $.ajax({
               type: "POST",
               url: '/api/create-todolist',
               data: new FormData(this),
               contentType: false,
               processData: false,
               dataType: 'json',
               success: function(response){
                   var notifier = new Notifier();
                   var notification = notifier.notify("success", "Thêm thành công");
                   notification.push();
                   $('#list_todolist').prepend(response.html);
               }
           });   
   })
   $('body').delegate('.check-todolist','click',function(){
       if ($('input[name="todolist_id"]:checked').val() === undefined) {
           var notifier = new Notifier();
           var notification = notifier.notify("info", "Cần chọn công việc trước khi thao tác");
           notification.push();
       }else{
           var todolist_id = [];
           $('.check').each(function () {
               if ($(this).is(':checked')) {
                   todolist_id.push($(this).val());
               }
           })   
           $.ajax({
               url: '/api/change-status-todolist',
               method: 'POST',
               data: {todolist_id: todolist_id},
               success: function (response) {
                   var notifier = new Notifier();
                   var notification = notifier.notify("success", "Cập nhật thành công");
                   notification.push();
                  $('#list_todolist').html(response.html);
               }
           });
       }
   })
</script>
@stop