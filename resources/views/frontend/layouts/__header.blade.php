<header>
    <div class="bottom-header" style="background: #333333;">
        <div>
            <div class="bottom-header-left bottom-header-left-admin">
                <div class="logo-header" style="display:flex">
                    <div id="header_pulldown_appmenu_grn" class="cloudHeader-dropdownMenu-grn header_appmenu_grn">
                        <button type="button" id="header_appmenu_title_grn" class="header_appmenu_title_grn button_style_off_grn" aria-controls="header_pulldown_appmenu_base_grn" aria-haspopup="true" aria-expanded="false"></button>
                        <div id="header_pulldown_appmenu_base_grn" class="cloudHeader-dropdownContents-grn" aria-labelledby="header_appmenu_title_grn" aria-hidden="false" style="max-height: 374.4px;">
                        <meta http-equiv="content-type" content="text/html;chaset=utf-8">
                        <div class="application_menu">
                            <span class="appmenu-item">
                               <a href="{{route('home.view')}}">
                                  <div class="icon-appMenu-portal appmenu-item-icon"></div>
                                  <div>
                                     <nobr>Protal</nobr>
                                  </div>
                               </a>
                            </span>
                            <span class="appmenu-item" title="Space" data-short_title="Space">
                               <a href="{{route('frontend.schedule.index')}}?">
                                  <div class="icon-appMenu-space appmenu-item-icon"></div>
                                  <div>
                                     <nobr>Scheduler</nobr>
                                  </div>
                               </a>
                            </span>
                            <span class="appmenu-item" title="Facilities" data-short_title="Facilities">
                               <a href="{!!route('frontend.cabinet.index')!!}">
                                  <div class="icon-appMenu-facility appmenu-item-icon"></div>
                                  <div>
                                     <nobr>Cabinet</nobr>
                                  </div>
                               </a>
                            </span>
                            <span class="appmenu-item" title="Bulletin Board" data-short_title="Bulletin Board">
                               <a href="{!!route('frontend.todo.index')!!}">
                                  <div class="icon-appMenu-bulletin appmenu-item-icon"></div>
                                  <div>
                                     <nobr>To-do List</nobr>
                                  </div>
                               </a>
                            </span>
                            <span class="appmenu-item" title="Messages" data-short_title="Messages">
                               <a href="">
                                  <div class="icon-appMenu-message appmenu-item-icon"></div>
                                  <div>
                                     <nobr>Time set</nobr>
                                  </div>
                               </a>
                            </span>
                            <span class="appmenu-item" title="Workflow" data-short_title="Workflow">
                               <a href="{{route('home.scheme')}}">
                                  <div class="icon-appMenu-address appmenu-item-icon"></div>
                                  <div>
                                     <nobr>Kaizen</nobr>
                                  </div>
                               </a>
                            </span>
                         </div>
                        </div>
                    </div>
                    <a href="@if(\Auth::guard('member')->user()->level > \App\Member::LEVEL_1) {{route('home.view')}} @else  {{route('frontend.project.index')}} @endif"><img src="{!!asset('/img/logo-ORS.png')!!}" style="margin-top: 2px;width: 250px;margin-left: 15px;"></a>
                </div>
            </div>
            <!--<form action="{{route('frontend.project.list')}}" method="get">-->
            <div class="bottom-header-right" style="padding-top:18px;float:left;">
                <div class="categorie-search-box">
                    <form id="search-form" action="" method="POST">
                        <input id="searchTag" autocomplete="" class="type ui-autocomplete-input" name="keyword" value="" type="text" placeholder="Từ khóa">
                        <button id="btnSearch"><i class="icon-search4" style="font-size:24px;"></i></button>
                    </form>
                </div>
                    <!--<input type="text" class="input-search" name="name" placeholder="Search..">-->
                
                <ul class="ul-notification">
                    <li>
                        <a href="javascript:void(0)" title="Tin nhắn" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle seen-chat"><i class="icon-bubbles4"></i>
                            <span class="badge badge-pill bg-warning-400 ml-auto ml-md-0" id='count-message'>@if($count_message > 0){!! $count_message !!}@endif</span>
                        </a>
                        <div class="dropdown-menu" id="log_member" aria-labelledby="dropdownMenuButton">
                            <div class="dropdown-content-header">
                                <span class="font-weight-semibold">Tin nhắn của bạn</span>
                                <input id="search_member_message" name="search" type="text" style="font-size:13px;width: 50%;position: absolute;top: 9px;right: 5px;height: 25px!important;">
                            </div>
                            <div class="dropdown-content-body body-message dropdown-scrollable">
                                <ul class="media-list" id='list-message'>

                                </ul>
                            </div>
                            <div class="dropdown-content-footer justify-content-center p-0">
                                <a href="#" class="bg-light text-grey w-100 py-2" data-popup="tooltip" title="Load more"></a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href="#" role="button" id="dropdownMenuLink" title="Thông báo" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle notification-menu"><i class="icon-bell2  bell"></i>
                            <span class="badge badge-pill bg-warning-400 ml-auto ml-md-0" id="count-notification">@if (count(Auth::guard('member')->user()->unreadNotifications)){{count(Auth::guard('member')->user()->unreadNotifications)}}@endif</span>
                        </a>
                        <div class="dropdown-menu" id="log_member" aria-labelledby="dropdownMenuButton" style="padding-bottom: 0px;">
                            <div class="dropdown-content-header">
                                <span class="font-weight-semibold" style="font-size:14px;">Thông báo của bạn</span>
                            </div>
                            <div class="dropdown-content-body dropdown-scrollable">
                                <ul class="media-list list-notification">
                                    @foreach( \Auth::guard('member')->user()->unreadNotifications as $val)
                                    <li class="media">
                                        <a href="javascript:void(0)" class="seen-notification" style="width:100%" data-id="{{$val->id}}" data-link="{{$val->data['link']}}">
                                            <div class="media-body">
                                                <div class="media-title">
                                                    <span class="font-weight-semibold color-blue">{{$val->data['full_name']}}</span>
                                                    <span class="text-muted float-right font-size-sm">{{$val->data['time']}} ({{date('d/m',strtotime($val->created_at))}})</span>
                                                </div>
                                                <span class="black font-small">{{$val->data['content']}}</span>
                                            </div>
                                        </a>
                                    </li> 
                                    @endforeach
                                </ul>
                            </div>
                            <div class="dropdown-content-footer justify-content-center p-0 text-center" style="border-top: 1px solid #e2e2e2;">
                                <a href="javascript:void(0)" class="text-grey w-100 py-2 seen-all-notification" title="Load more" style="font-size: 14px;">Đánh dấu đã xem tất cả</a>
                            </div>
                        </div>
                    </li>
                    @if(\Auth::guard('member')->user()->level > 1)
                    <li style="position:relative;">
                        <a @if(\Auth::guard('member')->user()->level == 5) href="{!!route('frontend.project.list',['keyword'=>'unapproved'])!!}" @else href="javascript:void(0)" @endif>
                            <i class="fas fa-user-cog"></i>
                            @if(count(\App\Project::where('status','<',4)->whereDate('created_at','=',date('Y-m-d'))->get()) > 0 && \Auth::guard('member')->user()->level == 5)
                            <span class="badge badge-pill bg-warning-400 ml-auto ml-md-0" id="unapproved">{{count(\App\Project::where('status','<',4)->whereDate('created_at',date('Y-m-d'))->get())}}</span>
                            @endif
                        </a>
                    </li>
                    @endif
                    <li>
                        <a href="#" role="button" id="dropdownMenuLink" title="Thông tin tài khoản" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">@if(is_null(\Auth::guard('member')->user()->avatar)) <i class="fas fa-user-circle"></i> @else <img style="width: 32px;height:32px;border-radius: 50%;border: 1px solid #7d7d7d;" src="{{\Auth::guard('member')->user()->avatar}}" /> @endif</a>
                        <div class="dropdown-menu infomation-member" id="log_member" aria-labelledby="dropdownMenuButton">
                            <div class="content-log-member">
                                <div class="img-avatar">
                                    <img src="@if(is_null(\Auth::guard('member')->user()->avatar)){!!asset('assets2/img/man.png')!!} @else {{\Auth::guard('member')->user()->avatar}} @endif" style="width:250px;padding:10px 20px;">
                                </div>
                                <h4 class="orange text-center">{!!\Auth::guard('member')->user()->full_name!!}</h4>
                                <div class="info-member-log">
                                    <p><span class="fl50">Mã nhân viên:</span><span>{!!\Auth::guard('member')->user()->login_id!!}</span></p>
                                    <p><span class="fl50">Chức vụ:</span><span>{!!\Auth::guard('member')->user()->position->name!!}</span></p>
                                    <p><span class="fl50">Bộ phận:</span> <span>@if(\Auth::guard('member')->user()->part) {!!\Auth::guard('member')->user()->part->name!!} @endif</span></p>
                                    <p><span class="fl50">Team: </span><span>@if(\Auth::guard('member')->user()->team) {!!\Auth::guard('member')->user()->team->name!!} @endif</span></p>
                                </div>
                            </div>
                            <div class="button-member-log text-center">
                                <a href="javascript:void(0)" class="btn" data-toggle="modal" data-target="#modal_reset_password">Thay đổi mật khẩu</a>
                                <a href="javascript:void(0)" class="btn" data-toggle="modal" data-target="#modal_update_avatar">Cập nhật ảnh đại diện</a>
                                <a href="{!!route('logoutMember')!!}" class="btn">Đăng xuất khỏi tài khoản</a>
                            </div>
                        </div>
                    </li>
                </ul>
                 <div class="top-header-right">
                     <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex" style="top:8px;">
                        <li class="nav-item dropdown">
                            <a class="nav-item nav-link dropdown-toggle mr-md-2 nav-header" style="color:#fff;" href="#" id="bd-versions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {!!trans('base.language')!!}
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="bd-versions">
                                <a class="dropdown-item @if(session('locale') == 'en') active @endif" href="{{route('frontend.language.change',['locale'=>'en'])}}">Tiếng anh</a>
                                <a class="dropdown-item @if(session('locale') == 'vi') active @endif" href="{{route('frontend.language.change',['locale'=>'vi'])}}">Tiếng Việt</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
           <!-- </form>-->
        </div>
    </div>
</header>
    
