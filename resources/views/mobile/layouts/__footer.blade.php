<!-- footer -->
<div class="appBottomMenu">
    <a href="view.html" class="item active">
        <div class="col">
            <ion-icon name="home-outline"></ion-icon>
        </div>
    </a>
    <a href="user.html" class="item">
        <div class="col">
            <ion-icon name="person-outline"></ion-icon>
        </div>
    </a>
    <a href="page-chat.html" class="item">
        <div class="col">
            <ion-icon name="chatbubble-ellipses-outline"></ion-icon>
            <span class="badge badge-danger">5</span>
        </div>
    </a>
    <a href="app-pages.html" class="item">
        <div class="col">
            <ion-icon name="notifications-outline"></ion-icon>
        </div>
    </a>
    <a href="javascript:;" class="item" data-toggle="modal" data-target="#sidebarPanel">
        <div class="col">
            <ion-icon name="menu-outline"></ion-icon>
        </div>
    </a>
</div>
<!-- * App Bottom Menu -->

<!-- App Sidebar -->
<div class="modal fade panelbox panelbox-left" id="sidebarPanel" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body p-0">

                <!-- profile box -->
                <div class="profileBox">
                    <div class="image-wrapper">
                        <img src="assets/img/sample/avatar/avatar1.jpg" alt="image" class="imaged rounded">
                    </div>
                    <div class="in">
                        <strong>Trần Hữu</strong>
                        <div class="text-muted">
                            <ion-icon name="location"></ion-icon>
                            Việt Nam
                        </div>
                    </div>
                    <a href="javascript:;" class="close-sidebar-button" data-dismiss="modal">
                        <ion-icon name="close"></ion-icon>
                    </a>
                </div>
                <!-- * profile box -->

                <ul class="listview flush transparent no-line image-listview mt-2">
                    <li>
                        <a href="index.html" class="item">
                            <div class="mr-2">
                                <img src="assets/upload/Portal.png" alt="image" class="imaged w36">
                            </div>
                            <div class="in">
                                Portal
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="scheduler.html" class="item">
                            <div class="mr-2">
                                <img src="assets/upload/Scheduler.png" alt="image" class="imaged w36">
                            </div>
                            <div class="in">
                                Scheduler
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="index.html" class="item">
                            <div class="mr-2">
                                <img src="assets/upload/Cabinet.png" alt="image" class="imaged w36">
                            </div>
                            <div class="in">
                                Cabinet
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="index.html" class="item">
                            <div class="mr-2">
                                <img src="assets/upload/To_do_list.png" alt="image" class="imaged w36">
                            </div>
                            <div class="in">
                                To-do list
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="index.html" class="item">
                            <div class="mr-2">
                                <img src="assets/upload/Time set.png" alt="image" class="imaged w36">
                            </div>
                            <div class="in">
                                Timeset
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="index.html" class="item">
                            <div class="mr-2">
                                <img src="assets/upload/kaizen.png" alt="image" class="imaged w36">
                            </div>
                            <div class="in">
                                Kaizen
                            </div>
                        </a>
                    </li>
                    <li>
                        <div class="item">
                            <div class="icon-box bg-primary">
                                <ion-icon name="moon-outline"></ion-icon>
                            </div>
                            <div class="in">
                                <div>Dark Mode</div>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input dark-mode-switch"
                                           id="darkmodesidebar">
                                    <label class="custom-control-label" for="darkmodesidebar"></label>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>

            </div>

            <!-- sidebar buttons -->
            <div class="sidebar-buttons">
                <a href="javascript:;" class="button">
                    <ion-icon name="person-outline"></ion-icon>
                </a>
                <a href="javascript:;" class="button">
                    <ion-icon name="moon-outline"></ion-icon>
                </a>
                <a href="javascript:;" class="button">
                    <ion-icon name="log-out-outline"></ion-icon>
                </a>
            </div>
        </div>
    </div>
</div>
<script src="{!!asset('mobilekit.bragherstudio.com/mobile/assets/js/lib/jquery-3.4.1.min.js')!!}"></script>
<script src="{!!asset('mobilekit.bragherstudio.com/mobile/assets/js/lib/popper.min.js')!!}"></script>
<script src="{!!asset('mobilekit.bragherstudio.com/mobile/assets/js/lib/bootstrap.min.js')!!}"></script>
<script type="module" src="https://unpkg.com/ionicons@5.2.3/dist/ionicons/ionicons.js"></script>
<script src="{!!asset('mobilekit.bragherstudio.com/mobile/assets/js/plugins/owl-carousel/owl.carousel.min.js')!!}"></script>
<script src="{!!asset('mobilekit.bragherstudio.com/mobile/assets/js/plugins/jquery-circle-progress/circle-progress.min.js')!!}"></script>
<script src="{!!asset('mobilekit.bragherstudio.com/mobile/assets/js/base.js')!!}"></script>
<script>
    setTimeout(() => {
        notification('notification-welcome', 5000);
    }, 2000);
</script>
