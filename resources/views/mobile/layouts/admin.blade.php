<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
         @include('mobile/layouts/__head')
    </head>

    <body>
        @yield('content')
        <!-- Footer -->
        @include('mobile/layouts/__footer')
        <!-- /footer -->
        <!-- /main content -->
        <!-- /page content -->
    </body>
    @yield('script')   
</html>

