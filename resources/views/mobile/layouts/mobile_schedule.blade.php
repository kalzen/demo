<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
         @include('mobile/layouts/head_schedule')
    </head>
    <body class='ui-mobile-viewport ui-overlay-a'>
        @include('mobile/layouts/__header')
        <!-- Page content -->
                @yield('content')
                 @include('mobile/layouts/__footer')
                <!-- Footer -->
                <!-- /footer -->
        <!-- /page content -->
    </body>
    @yield('script')   
</html>

