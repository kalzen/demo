<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>{{\App\Config::first()->title}}</title>
<!-- CSS -->
<!--<link href="{!!asset('assets2/css/fonts/etline-font.min.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/fonts/fontawesome/all.min.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/fonts/pe-icon-7-stroke.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/fonts/themify-icons.css')!!}" rel="stylesheet">
<link href="{!! asset('assets/css/components.min.css') !!}" rel="stylesheet" type="text/css">
<link href="{!!asset('assets2/plugins/owl.carousel/owl.carousel.min.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/plugins/slick/slick.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/bootstrap.min.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/icomoon/styles.min.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/main.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/styles.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/select2.min.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/mobile/css/style.css')!!}" rel="stylesheet">-->
<link rel="stylesheet" href="{!!asset('mobilekit.bragherstudio.com/mobile/assets/css/style.css')!!}">
<link rel="manifest" href="__manifest.json">
<script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js'></script>
<!-- Favicons -->
<link rel="apple-touch-icon" href="{!!asset('assets2/img/apple-touch-icon.png')!!}">
<link rel="icon" href="{{\App\Config::first()->favicon}}">
<script src="{!!asset('assets2/js/jquery.min.js')!!}"></script>