<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>New appointment</title>
  <!-- CSS -->

<link href="{!!asset('assets2/css/fonts/fontawesome/all.min.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/fonts/pe-icon-7-stroke.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/fonts/themify-icons.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/plugins/owl.carousel/owl.carousel.min.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/plugins/slick/slick.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/bootstrap.min.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/icomoon/styles.min.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/main.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/styles.css')!!}" rel="stylesheet">
<link href="{{asset('assets/mobile/css/jquery.mobile-1.4.5.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/mobile/css/msgbox_mobile.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/mobile/css/mobile_standard.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/mobile/css/mobile_select.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/mobile/css/mobile_change_panel.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/mobile/css/mobile_toolbar.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/mobile/css/mobile_folderlist.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/mobile/css/mobile_list.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/mobile/css/mobile_schedule.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/mobile/css/mobile_design_standard.css')}}" rel="stylesheet" type="text/css">
<link href="{!!asset('assets2/mobile/css/style.css')!!}" rel="stylesheet">
<link rel="shortcut icon" href="/garoon3/grn/image/cybozu/garoon.ico?20200925.text">
<link rel="apple-touch-icon-precomposed" href="/garoon3/grn/image/cybozu/image-mobile/apple-touch-icon.png?20200925.text">
<script src="{{asset('assets/mobile/js/jquery-2.2.4.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/mobile/js/jquery-ui-1.11.4.custom.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/base.js')}}" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
<!--

if( typeof grn.browser == "undefined" )
{
    grn.browser = {};
}

    grn.browser.chrome = true;
grn.browser.version = 87;


grn.browser.isSupportHTML5 = false;
if( !!window.FormData )
{
    grn.browser.isSupportHTML5 = true;
}

//-->
  </script>  <script src="{{asset('assets/mobile/js/custom_jquery_mobile.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/mobile/js/jquery.mobile-1.4.5.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/mobile/js/msgbox_mobile.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/mobile/js/define_cybozu_browser.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/mobile/js/mobile_common.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/mobile/js/mobile_loading.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/mobile/js/mobile_error_handler.js')}}" type="text/javascript"></script>

<script text="text/javascript">
    grn.component.mobile_error_handler.Msg = {
        'theme':"",
        'Error':'Error',
        'Cause':'Cause ',
        'CounterMeasure':'Countermeasure',
        'OK':'OK',
        'GRN_GRN_33':'( Beta/Debug only. by common.ini )',
        'GRN_GRN_34':'Developer Info',
        'GRN_GRN_35':'Backtrace',
        'GRN_GRN_36':'How to read backtraces(to be written)',
        'GRN_GRN_37':'How to read backtraces (to be written)',
        'GRN_GRN_38':'$G_INPUT',
        'show_backtrace':""
    };
</script>
  <script src="{{asset('assets/mobile/js/mobile_ajax_submit.js')}}" type="text/javascript"></script>
  <script src="{{asset('/js/url.js')}}" type="text/javascript"></script>
<script>
    grn.component.url.PAGE_PREFIX = "/scripts/garoon/grn.exe";
    grn.component.url.PAGE_EXTENSION = "";
    grn.component.url.STATIC_URL = "/garoon3";
    grn.component.url.BUILD_DATE = "20200925.text";
</script>  <script src="{{asset('/js/i18n.js')}}" type="text/javascript"></script>
  <script src="{{asset('/js/resource-en.js')}}"></script>
  <script src="{{asset('/js/request.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/error_default_view.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/error_handler.js')}}" type="text/javascript"></script>  
<script src="{{asset('/js/runtime.js')}}" type="text/javascript"></script>

<script src="{{asset('/js/commons_chunk.js')}}" type="text/javascript"></script>

    <script src="{{asset('assets/mobile/js/common_mobile.js')}}" type="text/javascript"></script>
  <script type="text/javascript" language="javascript">
    grn.data = {};
    grn.data.login = {};
    grn.data.login.id = 58;
    grn.data['CSRF_TICKET'] = "4ea9960823218f3d2f44dfae0b6fb48e";
    grn.component.define_cybozu_browser.kunai_api_version = '';
    grn.msg = {};
    grn.msg.OK = 'OK';
    grn.msg.CANCEL = 'Cancel';
    grn.msg.YES = 'Yes';
    grn.msg.NO = 'No';

    var getNumberNotificationUrl = "/scripts/garoon/grn.exe/grn/ajax_get_number_notification?";

    grn.component.mobile_common.setNextUpdateNotification(75000);

    grn.component.mobile_common.copyright = "Cybozu Garoon Version 5.5.0";
  </script>
