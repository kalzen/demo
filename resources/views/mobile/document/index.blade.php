@extends('frontend.home.home')
@section('content')
<div class="content project-content">
    <a class="next-sidebar sidebar-left"><i style="padding: 0px 10px 0px 2px;font-size: 22px;margin-top: 4px;" class="icon-arrow-right6"></i> <h4>Quản lí tài liệu</h4></a>
    <div class="form-create">
        <form id='postData' data-model='document'>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-3 col-form-label text-right">Tên tài liệu</label>
                <div class="col-sm-6">
                    <input class="form-control" name="name" type="text"> 
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 required control-label text-right text-semibold" for="images">File</label>
                <div class="col-lg-6 div-image">
                    <div class="file-input file-input-ajax-new">
                        <div class="input-group-btn input-group-append">
                            <div tabindex="500" class="btn btn-primary btn-file"><i class="icon-folder-open"></i>&nbsp; <span class="hidden-xs">Chọn</span>
                                <input type="file" id="images" class="upload-image" multiple="multiple" name="file_upload[]" data-fouc="">
                            </div>
                        </div>
                        <div class="file-preview ">
                            <div class=" file-drop-zone">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="file" class="image_data">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="form-check col-md-3" style="margin-left: 16px;">
                    <input type="checkbox" class="form-check-input" id="check_level" name="status" value="1" style="width:22px;height:22px;">
                    <label class="form-check-label" for="check_level" style="margin: 4px 0px 0px 14px;">Hiển thị</label>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-5"></div>
                <div class="col-md-7 col-sm-8 col-sm-offset-4 btn-group button-group">
                    <a href="/home" class="btn btn-sm btn-default">Quay lại</a>
                    <button type="button" class="btn btn-sm btn-primary btn-add">Thêm</button>
                </div>
            </div>
        </form>
        <table class="table datatable-basic table-bordered" id="dataTables-example" style="margin:0px">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Tên tài liệu</th>
                    <th>Trạng thái</th>
                    <th>Tác vụ</th>
                </tr>
            </thead>
            <tbody>
                @foreach($records as $key=>$record)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$record->name}}</td>
                    <td>
                       @if($record->status == 0)
                            <span class="badge badge-secondary">Ẩn</span>
                       @else
                            <span class="badge badge-success">Hiển thị</span>
                       @endif
                    </td>
                    <td>
                        <a href="javascript:void(0)" title="{!! trans('base.edit') !!}" class="success edit-data" data-id='{{$record->id}}' data-model='document'><i class="icon-pencil"></i></a>
                        <form action="{!! route('frontend.document.destroy', ['id' => $record->id]) !!}" method="POST" style="display: inline-block;margin:0px;">
                            {!! method_field('DELETE') !!}
                            {!! csrf_field() !!}
                            <a title="{!! trans('base.delete') !!}" class="delete text-danger" data-action="delete">
                                <i class="icon-close2"></i>
                            </a>              
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop