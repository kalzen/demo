
<div style="width: 100%;display:block;">
    <canvas id="income" width="300" height="400"></canvas>
    <p>Biểu đồ thống kê đề án theo tháng</p>
</div> 
<div style="width: 100%display:block;">
    <canvas id="countries" width="300!important;" height="400"></canvas>
    <p>Biểu đồ thống kê đề án theo trạng thái</p>
</div>
<div style="width: 100%display:block;">
    <canvas id="status_chart" width="300!important;" height="400"></canvas>
    <p>Biểu đồ thống kê đề án theo cấp độ</p>
</div>
<script>
    var barData = {
        labels : ["Tháng 2","Tháng 3","Tháng 4","Tháng 5","Tháng 6","Tháng 7"],
        datasets : [
            {
                fillColor : "#48A497",
                strokeColor : "#48A4D1",
                data : [100,110,105,120,150,200]
            },
        ]
    }
    var income = document.getElementById("income").getContext("2d");
    new Chart(income).Bar(barData);
    var barData1 = {
        labels : ["Cấp độ A","Cấp độ B","Cấp độ C","Cấp độ C","Không cấp độ"],
        datasets : [
            {
                fillColor : "#48A497",
                strokeColor : "#48A4D1",
                data : [100,110,105,120,100]
            },
        ]
    }
    var income1 = document.getElementById("status_chart").getContext("2d");
    new Chart(income1).Bar(barData1);
    var pieData = [
        {
            value: 10,
            color:"#3a4048",
            label: "Chờ duyệt"
        },
        {
            value : 50,
            color : "#da2f69",
            label: "Trả về"
        },
        {
            value : 10,
            color : "#26d847",
            label: "Đã duyệt"
        },
    ];

    // pie chart options
    var pieOptions = {
        segmentShowStroke : false,
        animateScale : true
    }

    // get pie chart canvas
    var countries= document.getElementById("countries").getContext("2d");

    // draw pie chart
    new Chart(countries).Pie(pieData, pieOptions);
    
</script>

