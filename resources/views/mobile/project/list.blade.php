@extends('mobile.layouts.admin')
@section('content')
<body class="page-body">
    <div class="page-content">
    <div class="row" style="margin:0px;width:100%;">
        <div class="col-md-12">
            <div class="container">
                <div class="row" style="padding-bottom: 10px;">
                    <div class="col-md-4">
                        <fieldset>
                            <legend><span class="orange">TOP TEAMS</span> of Month</legend>
                            @foreach($rank_team as $key=>$val)
                            <p>{{$key + 1}}. Tem {{$val->name}} <span>{{$val->count}} đề án</span></p>
                            @endforeach
                        </fieldset>
                    </div>
                    <div class="col-md-8">
                        <fieldset>
                            <legend><span class="orange">TOP USER</span> of Quarter</legend>
                            <table class="top-user" style="margin-left:0px;">
                                <tbody>
                                    @foreach($rank_quarter as $key=>$val)
                                    <tr>
                                        <td><p>{{$key + 1}}. {{\App\Member::find($val->id)->full_name}}</p></td>
                                        <td><p>{{\App\Member::find($val->id)->login_id}}</p></td>
                                        <td><p>Team @if(\App\Member::find($val->id)->team){{\App\Member::find($val->id)->team->name}} @endif</p></td>
                                        <td><p>{{$val->count}} đề án</p></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </fieldset>
                    </div>
                </div>
            </div>
            <div class="content">
                <div class="fillter-project text-center">
                    <ul>
                        <li><a href="javascript:void(0)" class="link-list orange" data-keyword_project="all">TẤT CẢ</a></li>
                        <li><a href="javascript:void(0)" class="link-list" data-keyword_project="pending">Đ.A CHỜ DUYỆT</a></li>
                        <li><a href="javascript:void(0)" class="link-list" data-keyword_project="return">Đ.A TRẢ VỀ</a></li>
                        <li><a href="javascript:void(0)" class="link-list" data-keyword_project="approved">Đ.A ĐÃ DUYỆT</a></li>
                        <li><a href="javascript:void(0)" class="link-list" data-keyword_project="save_member">Đ.A ĐÃ LƯU</a></li>
                        <li><a href="javascript:void(0)" class="chart-list" data-href='{{route('frontend.project.chart')}}'>BIỂU ĐỒ THỐNG KÊ</a></li>
                    </ul>
                </div>
                <div class="table-content table-member-content">
                    <div class="control-table row">
                        <div class="col-6" style="bottom: -18px;">
                            <span>Tháng </span>
                            <select name='month'>
                                @for($i=1;$i<13;$i++)
                                <option value='{{$i}}'>{{$i}}</option>
                                @endfor
                            </select>
                             <select name='year'>
                                @for($j=2020;$j < 2031;$j++)
                                <option value='{{$j}}'>{{$j}}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="col-6" id="show_list_project" style="display:inline-block;text-align: right;">
                            <div style="display:inline-flex;">
                                <p id='page_member'>1-{!!session('_list_pages')!!} trong số {!!session('_list_count')!!}</p>
                                 @if(session('list_page') > 1)
                                <a style="padding-top:15px" href="javascript:void(0)" class='forward-list-project'><img src="{!!asset('assets2/img/left-arrow.png')!!}"></a>
                                @endif
                                @if(session('_list_count') > session('_list_pages'))
                                <a style="padding-top:15px" href="javascript:void(0)" class='next-list-project'><img src="{!!asset('assets2/img/forward.png')!!}"></a>
                                @endif
                            </div>
                        </div>
                    </div>
                    <table class="table table-bordered table-member" style="margin-top:5px;">
                        <thead class="thead-dark">
                            <tr style="box-shadow: none;">
                                <th><input type="checkbox" id='check_all' value='all'></th>
                                <th>STT</th>
                                <th>Họ và tên</th>
                                <th>Đơn vị</th>
                                <th class='text-center'>Tên đề tài</th>
                                <th>Cấp độ</th>
                                <th>Trạng thái</th>
                                <th>Ngày</th>
                            </tr>
                        </thead>
                        <tbody id='records_list_project'>
                            @foreach($records as $key=>$record)
                            <tr>
                                <td class="middle"><input type="checkbox" value="{{$record->id}}" name="member_id" class='check'></td>
                                <td  class="middle">{{$key + 1}}</td>
                                <td  class="middle">@if($record->member->is_deleted == 1) <span class="red">{{$record->member->full_name}}</span> @else {{$record->member->full_name}} @endif</td>
                                <td  class="middle">@if($record->member->department){{$record->member->department->name}} @endif</td>
                                <td><a href="{!!route('frontend.project.view',$record->id)!!}">{{$record->name}}</a></td>
                                <td class="middle">@if($record->levels)<span class="badge badge-danger">{{$record->levels->name}}</span>@else<span class="badge badge-secondary">Không cấp độ</span> @endif</td>
                                <td class="middle">
                                @if($record->status == \App\Project::STATUS_CANCEL)
                                     <span class="badge badge-danger">Trả về</span>
                                @elseif($record->status < \App\Project::STATUS_ACTIVE )
                                     <span class="badge badge-secondary">Chờ duyệt</span>
                                @else
                                     <span class="badge badge-success">Đã duyệt</span>
                                @endif</td>
                                <td class="middle">{{$record->created_at()}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class='show-chart'>
                   
                </div>
            </div>
        </div>
    </div>
</body>
</div>
<div class="zoom">
    <a class="zoom-fab zoom-btn-large" id="zoomBtn"><i class="icon-menu3" style="font-size: 30px;color: #fff;"></i></a>
    <ul class="zoom-menu">
      <li><a class="zoom-fab zoom-btn-sm zoom-btn-person scale-transition scale-out export-project" href="javascript:void(0)"><img  src="{!!asset('assets2/img/excel.png')!!}"></a></li>
      <li><a class="zoom-fab zoom-btn-sm zoom-btn-doc scale-transition scale-out save-project" href="javascript:void(0)"><img  src="{!!asset('assets2/img/save (1).png')!!}" style="width:24px"></a></li>
      <li><a class="zoom-fab zoom-btn-sm zoom-btn-tangram scale-transition scale-out return-project" href="javascript:void(0)"><i class="icon-reply" title="Trả về"></i></a></li>
      <li><a class="zoom-fab zoom-btn-sm zoom-btn-report scale-transition scale-out send-project" href="javascript:void(0)"><i class="icon-forward" title='Duyệt'></i></a></li>
    </ul>
</div>
@stop
@section('script')
@parent
<script>
    $('#zoomBtn').click(function() {
        $('.zoom-btn-sm').toggleClass('scale-out');
        if (!$('.zoom-card').hasClass('scale-out')) {
          $('.zoom-card').toggleClass('scale-out');
        }
      });

      $('.zoom-btn-sm').click(function() {
        var btn = $(this);
        var card = $('.zoom-card');
        if ($('.zoom-card').hasClass('scale-out')) {
          $('.zoom-card').toggleClass('scale-out');
        }
        if (btn.hasClass('zoom-btn-person')) {
          card.css('background-color', '#d32f2f');
        } else if (btn.hasClass('zoom-btn-doc')) {
          card.css('background-color', '#fbc02d');
        } else if (btn.hasClass('zoom-btn-tangram')) {
          card.css('background-color', '#388e3c');
        } else if (btn.hasClass('zoom-btn-report')) {
          card.css('background-color', '#1976d2');
        } else {
          card.css('background-color', '#7b1fa2');
        }
      });
    $('#check_all').change(function () {
        if ($(this).is(":checked")) {
            $('.check').each(function () {
                $(this).prop('checked', true); 
            });
        } else {
            $('.check').each(function () {
                $(this).prop('checked', false); 
            });
        }
    });
    $('body').delegate('.check','change',function(){
        var member_id = [];
        $('.check').each(function () {
            if ($(this).is(':checked')) {
                member_id.push($(this).val());
            }
            $('#check_all').attr('value',member_id.join(','));
        })   
    })
    $('body').delegate('.next-list-project','click',function(){
            $.ajax({
            url: '/api/next-list-project',
            method: 'POST',
            success: function (response){
                $('#records_list_project').html(response.html);
                $('#show_list_project').html(response.show_page);
            }
        });
    });
    $('body').delegate('.forward-list-project','click',function(){
            $.ajax({
                url: '/api/forward-list-project',
                method: 'POST',
                success: function (response){
                    $('#records_list_project').html(response.html);
                    $('#show_list_project').html(response.show_page);
                }
            });
    });
    $('.link-list').click(function(){
        $('.table-member-content').attr('style','display:block');
        $('.link-list').removeClass('orange');
        $('.chart-list').removeClass('orange');
        $(this).addClass('orange');
        $(".show-chart").attr('style','display:none');
        var keyword_project = $(this).data('keyword_project');
        $.ajax({
            url:"/api/getListProject",
            type:"POST",
            data:{keyword_project:keyword_project},
            success: function (response){
                $('#records_list_project').html(response.html);
                $('#show_list_project').html(response.show_page);
            }
        })
    })
    $('select[name="month"],select[name="year"]').change(function(){
        var month = $('select[name="month"]').val();
        var year = $('select[name="year"]').val();
        var keyword = $('.link-list.orange').data('keyword');
        $.ajax({
            url:"/api/getListProject",
            type:"POST",
            data:{month:month,year:year,keyword:keyword},
            success: function (response){
                $('#records_list_project').html(response.html);
                $('#show_list_project').html(response.show_page);
            }
        })
    })
    $('body').delegate('.send-project','click',function(){
        if ($('input[type="checkbox"]:checked').val() === undefined){
            var notifier = new Notifier();
            var notification = notifier.notify("info", "Cần chọn đề án trước khi thao tác");
            notification.push();
        }else{
            var project_id = 0;
            if($('#check_all').is(":checked")){
                project_id = 'all';
            }else{
                project_id = $('#check_all').attr('value');
            }
            $.ajax({
                url: '/api/send-project',
                method: 'POST',
                data: {project_id: project_id},
                success: function (response) {
                    var notifier = new Notifier();
                    var notification = notifier.notify("success", "Duyệt đề án thành công");
                    notification.push();
                    $('input[type=checkbox]').prop('checked',false);
                }
            });
        }
    })
    $('body').delegate('.return-project','click',function(){
    if ($('input[type="checkbox"]:checked').val() === undefined){
        var notifier = new Notifier();
        var notification = notifier.notify("info", "Cần chọn đề án trước khi thao tác");
        notification.push();
    }else{
        var project_id = 0;
        if($('#check_all').is(":checked")){
            project_id = 'all';
        }else{
            project_id = $('#check_all').attr('value');
        }
        $.ajax({
            url: '/api/return-project',
            method: 'POST',
            data: {project_id: project_id},
            success: function (response) {
                var notifier = new Notifier();
                var notification = notifier.notify("success", "Đề án trả về thành công");
                notification.push();
                $('input[type=checkbox]').prop('checked',false);
            }
        });
    }
})
    $('body').delegate('.save-project','click',function(){
        if ($('input[type="checkbox"]:checked').val() === undefined){
            var notifier = new Notifier();
            var notification = notifier.notify("info", "Cần chọn đề án trước khi thao tác");
            notification.push();
        }else{
            var project_id = 0;
            if($('#check_all').is(":checked")){
                project_id = 'all';
            }else{
                project_id = $('#check_all').attr('value');
            }
             $.ajax({
                url: '/api/save-project',
                method: 'POST',
                data: {project_id: project_id},
                success: function (response) {
                    var notifier = new Notifier();
                    var notification = notifier.notify("success", "Lưu thành công");
                    notification.push();
                    $('input[type=checkbox]').prop('checked',false);
                }
            });
        }
    })
    $('.chart-list').click(function(){
        $('.link-list').removeClass('orange');
        $(this).addClass('orange');
        $('.table-member-content').attr('style','display:none');
        $(".show-chart").attr('style','display:block;text-align:center;');
        var ajax_load = "<img src='/public/assets2/img/small-loading.gif' alt='loading...' />";
         $(".show-chart").html(ajax_load).load($(this).data('href')); 
    })
    $('body').delegate('.export-project','click',function(){
        if ($('input[type="checkbox"]:checked').val() === undefined) {
            var notifier = new Notifier();
            var notification = notifier.notify("info", "Cần chọn đề án trước khi thao tác");
            notification.push();
        }else{
            var project_id = 0;
            if($('#check_all').is(":checked")){
                project_id = 'all';
            }else{
                project_id = $('#check_all').attr('value');
            }
            $.ajax({
                url: '/api/export-project',
                method: 'POST',
                data: {project_id: project_id},
                success: function (response) {
                    window.location.href=response.href;
                }
            });
        }
    })
</script>
@stop