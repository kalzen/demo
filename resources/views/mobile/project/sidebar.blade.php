<div class="sidebar">
    <div class="button-create">
        <a href="{{route('frontend.project.create')}}" class="add-project"><i class="fas fa-plus"></i> New PJ</a>
        <a class="close-sidebar"><i class="icon-cross" style="font-size:20px"></i></a>
    </div>
    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
        <a class="nav-link load-page  @if(!isset($_GET['keyword'])) active @endif" href="{!!route('frontend.project.index')!!}"><img  src="{!!asset('assets2/img/sidebar (1).png')!!}">Tất cả đề tài<span>{!!\App\Project::where('member_id',\Auth::guard('member')->user()->id)->where('is_deleted',0)->count()!!}</span></a>
        <a class="nav-link load-page @if(isset($_GET['keyword']) && $_GET['keyword'] == 'save') active @endif" href="{!!route('frontend.project.index',['keyword'=>'save'])!!}"><img  src="{!!asset('assets2/img/save.png')!!}">Đã lưu trữ<span>{!!\App\Project::where('member_id',\Auth::guard('member')->user()->id)->where('is_saved',1)->where('is_deleted',0)->count()!!}</span></a>
        <a class="nav-link load-page @if(isset($_GET['keyword']) && $_GET['keyword'] == 'send') active @endif" href="{!!route('frontend.project.index',['keyword'=>'send'])!!}"><img  src="{!!asset('assets2/img/send.png')!!}">Đã gửi<span>{!!\App\Project::where('member_id',\Auth::guard('member')->user()->id)->where('status','>',0)->where('is_deleted',0)->count()!!}</span></a>
        <a class="nav-link load-page @if(isset($_GET['keyword']) && $_GET['keyword'] == 'draft') active @endif" href="{!!route('frontend.project.index',['keyword'=>'draft'])!!}"><img  src="{!!asset('assets2/img/documents.png')!!}">Nháp<span>{!!\App\Project::where('member_id',\Auth::guard('member')->user()->id)->where('status',0)->where('is_deleted',0)->count()!!}</span></a>
        <a class="nav-link load-page @if(isset($_GET['keyword']) && $_GET['keyword'] == 'remove') active @endif" href="{!!route('frontend.project.index',['keyword'=>'remove'])!!}"><img  src="{!!asset('assets2/img/trash.png')!!}">Thùng rác<span>{!!\App\Project::where('member_id',\Auth::guard('member')->user()->id)->where('is_deleted',1)->count()!!}</span></a>
    </div>
    <div class="text-sidebar">
        <h6>Thứ hạng của bạn</h6>
        <ul>
            <li>Cá nhân ( @if($position_rank != 0){{$position_rank}} @else Chưa có đề án @endif)</li>
            <li>Nhóm ( @if($position_team_rank != 0){{$position_team_rank}} @else Chưa có đề án @endif)</li>
        </ul>
    </div>
    <div class="text-sidebar">
        <h6><span class="orange">TOP TEAMS</span> of Month</h6>
        <ul class="list-style">
            @foreach($rank_team as $key=>$val)
            <li>{{$key + 1}}. Tem {{$val->name}} <span>{{$val->count}} đề án</span></li>
            @endforeach
        </ul>
    </div>
    <div class="text-sidebar">
        <h6><span class="orange">TOP USER</span> of Quarter </h6>
        <ul class="list-style">
            @foreach($rank_quarter as $key=>$val)
            <li>{{$key + 1}}. {{\App\Member::find($val->id)->full_name}} <span>{{$val->count}} đề án</span></li>
            @endforeach
        </ul>
    </div>
</div>