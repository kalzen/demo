@extends('mobile.layouts.admin')
@section('content')
<div class="appHeader bg-primary scrolled">
        <div class="left">
            <a href="#" class="headerButton" data-toggle="modal" data-target="#sidebarPanel">
                <ion-icon name="menu-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">
            Kaizen
        </div>
        <div class="right">
            <a href="javascript:;" class="headerButton toggle-searchbox">
                <ion-icon name="search-outline"></ion-icon>
            </a>
        </div>
    </div>
    <!-- * App Header -->

    <!-- Search Component -->
    <div id="search" class="appHeader">
        <form class="search-form">
            <div class="form-group searchbox">
                <input type="text" class="form-control" placeholder="Search...">
                <i class="input-icon">
                    <ion-icon name="search-outline"></ion-icon>
                </i>
                <a href="javascript:;" class="ml-1 close toggle-searchbox">
                    <ion-icon name="close-circle"></ion-icon>
                </a>
            </div>
        </form>
    </div>
    <!-- * Search Component -->

    <!-- App Capsule -->
    <div id="appCapsule">

        <div class="header-large-title mb-3">
            <h1 class="title">Kaizen</h1>
        </div>

        <div class="section full mb-3">
            <div class="section-title">Schedule</div>
            <div class="carousel-single owl-carousel owl-theme owl-loaded owl-drag">
                <div class="card item">
                    
                    <div class="card-body">
                        <h5 class="card-title d-flex justify-content-between">
                            Today
                            <a href="#"><ion-icon name="add"></ion-icon></a> 
                        </h5>
                        <ul class="listview flush transparent image-listview">
                            <li>
                                <a href="#" class="item">
                                    <div class="in">
                                        <div>test</div>
                                        <span class="text-muted">08:00 AM-11:00 AM</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="item">
                                    <div class="in">
                                        <div>test</div>
                                        <span class="text-muted">08:00 AM-11:00 AM</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="item">
                                    <div class="in">
                                        <div>test</div>
                                        <span class="text-muted">08:00 AM-11:00 AM</span>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <a href="app-components.html" class="btn btn-primary w-100">
                            <ion-icon name="cube-outline"></ion-icon>
                            See all
                        </a>
                    </div>

                </div>
            </div>

        </div>

        <div class="section mt-3 mb-3">
            <div class="section-title">Todo-list</div>
            <div class="card">
                <div class="card-body">
                    <div class="wide-block pt-2 pb-2">

                        <ul class="nav nav-tabs style1" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#home" role="tab" aria-selected="false">
                                    Completed
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#profile" role="tab" aria-selected="false">
                                    Uncompleted
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content mt-2">
                            <div class="tab-pane fade active show" id="home" role="tabpanel">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col">To-do</th>
                                                <th scope="col">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><a href="#" class="item">inputs</a></td>
                                                <td><span class="badge badge-danger">Đã giao việc</span></td>
                                            </tr>
                                            <tr>
                                                <td><a href="#" class="item">Jacod</a></td>
                                                <td><span class="badge badge-danger">Đã giao việc</span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div> 
                            </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col">To-do</th>
                                                <th scope="col">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><a href="#" class="item">input</a></td>
                                                <td><span class="badge badge-danger">Chưa giao việc</span></td>
                                            </tr>
                                            <tr>
                                                <td><a href="#" class="item">Jacob</a></td>
                                                <td><span class="badge badge-danger">Chưa giao việc</span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div> 
                            </div>
                        </div>

                    </div>

                    <a href="app-components.html" class="btn btn-primary w-100">
                        <ion-icon name="cube-outline"></ion-icon>
                        Detail
                    </a>
                </div>
            </div>
        </div>

    </div>
@stop
