<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="api_token" content="{{\Auth::guard('member')->user() ? \Auth::guard('member')->user()->api_token : ''}}">
        <title>{{\App\Config::first()->title}}</title>
        <!-- CSS -->
        <link href="{!!asset('assets/css/std.css')!!}" rel="stylesheet" type="text/css">
        <link href="{!!asset('assets/css/schedule.css')!!}" rel="stylesheet" type="text/css">
        <link href="{{asset('assets/css/image_grn.css')}}" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{!!asset('assets2/css/jquery.transfer.css')!!}">
        <link href="{!!asset('assets2/css/fonts/etline-font.min.css')!!}" rel="stylesheet">
        <link href="{!!asset('assets2/css/fonts/fontawesome/all.min.css')!!}" rel="stylesheet">
        <link href="{!!asset('assets2/css/fonts/pe-icon-7-stroke.css')!!}" rel="stylesheet">
        <link href="{!!asset('assets2/css/fonts/themify-icons.css')!!}" rel="stylesheet">
        <link href="{!! asset('assets/css/components.min.css') !!}" rel="stylesheet" type="text/css">
        <link href="{!!asset('assets2/plugins/owl.carousel/owl.carousel.min.css')!!}" rel="stylesheet">
        <link href="{!!asset('assets2/plugins/slick/slick.css')!!}" rel="stylesheet">
        <link href="{!!asset('assets2/css/bootstrap.min.css')!!}" rel="stylesheet">
        <link href="{!!asset('assets2/css/icomoon/styles.min.css')!!}" rel="stylesheet">
        <link href="{!!asset('assets2/css/main.css')!!}" rel="stylesheet">
        <link href="{!!asset('assets2/css/styles.css')!!}" rel="stylesheet">
        <link href="{!!asset('assets2/css/select2.min.css')!!}" rel="stylesheet">
        <link href="{!!asset('assets2/css/schedule.css')!!}" rel="stylesheet">
        <link href="{!!asset('assets2/css/custom.css')!!}" rel="stylesheet">
        <link rel="stylesheet" href="{!!asset('assets2/icon_font/css/icon_font.css')!!}">
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <!-- Favicons -->
        <link rel="apple-touch-icon" href="{!!asset('assets2/img/apple-touch-icon.png')!!}">
        <link rel="icon" href="{{\App\Config::first()->favicon}}">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <script src="{!!asset('assets2/js/jquery.min.js')!!}"></script>
        <script src="{!!asset('assets2/js/utils.js')!!}"></script>
        <script src="{!!asset('assets2/js/jquery-nice-select.js')!!}"></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js'></script>
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-TSWJDSPKXP"></script>
    </head>

    <body>
        <!-- Page content -->
        <div id="page">
            <!-- Main content -->
            <div class="content-wrapper">
               
                <header class="header">
                    <div class="bottom-header" style="background: #333333;">
                        <div class="container" style="display: flex;padding-right: 0px;">
                            <div class="bottom-header-left">
                                <div class="logo-header">
                                    <a href="/"><img src="{!!asset('assets2/img/logo1.jpg')!!}" style="width: 103px;"></a>
                                </div>
                            </div>
                            <div class="bottom-header-right">
                                <form action="{!!route('loginMember')!!}" method="POST">
                                    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                                    <div class="form-group">
                                        <input type="text" class="form-control" name='login_id' placeholder="COMPANY ID">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" name='password' placeholder="PASSWORD">
                                    </div>
                                    <div class="form-group">
                                        <button type='submit' class="btn btn-color">Login</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </header>
                <div class="slider" style="position:relative">
                    <img src='{!!asset('assets2/img/banner.png')!!}' style="width:100%;height:130px;">
                </div>
                <body class="page-body">
                    <main class="main-content">
                        <section class="content-section owl-carousel-spotlight carousel-spotlight ig-carousel">
                            <div class="container">
                                <header class="header">
                                    <h5><span class="orange">TYPICAL</span> IMPROVEMENT PROJECT</h5>
                                </header>
                                <div class="position-relative">
                                    <div id="color_sel_Carousel-content" class="tab-content fl-scn relative w-100" style="padding:20px">
                    <!-- tab item -->
                    <div class="tab-pane fade show active" id="mp-01-c" role="tabpanel" aria-labelledby="mp-01-tab">
                        <div class="owl-carousel gs-carousel" data-carousel-margin="30" data-carousel-nav="true" data-carousel-navText="<span class='icon-cl-next pe-7s-angle-left'></span>, <span class='icon-cl-next pe-7s-angle-right'></span>">
                            @foreach($slides as $key=>$slide)
                            <div class="item">
                                <div class="item-cont">
                                    <figure class="owl_item_review">
                                        <div>
                                            <div class="position-relative overflow-hidden overlay-gradient">
                                                <img class="m-0-auto" src="{!!$slide->image!!}" alt="{{$slide->title}}" style="height:250px">
                                                <div class='over-text'>
                                                    <h5>{{$slide->title}}</h5>
                                                </div>
                                                <div class="overlay overlay-gradient"></div>
                                            </div>
                                        </div>
                                    </figure>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
</body>
<!-- footer -->
<footer class="footer py-9">
    <div class="bottom-footer" >
        <div class="bottom-footer-left">
            <div class="logo-footer text-right">
                <a href="javascript:void(0)"><img src="{{\App\Config::first()->image}}" style="width:250px"></a>
            </div>
        </div>
        <div class="bottom-footer-right">
            <p style="margin-bottom: 0px;margin-right: 25px;">Copyright @2020 OURANSOFT TECHNOLOGY JSC. All Rights Reserved</p>
        </div>
    </div>
    <div class="modal fade" id="modal_reset_password" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="padding-bottom:0px">
                    <h5 class="modal-title" id="exampleModalLabel"><span class="orange">Thay đổi</span> mật khẩu</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <form method="post" id='reset_password'>
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <lable class="col-md-4">Mật khẩu cũ</lable>
                                        <div class="col-md-8 form-inline pd0">
                                            <input type="password" class='form-control old-password'>
                                        </div>
                                    </div>
                                    <div class="form-group form-inline row">
                                        <lable class="col-md-4">Mật khẩu mới</lable>
                                        <div class="col-md-8 form-inline pd0">
                                            <input type="password" name='password' class='form-control news-password'>
                                        </div>
                                    </div>
                                    <div class="form-group form-inline row">
                                        <lable class="col-md-4">Nhập lại</lable>
                                        <div class="col-md-8 pd0">
                                            <input type="password" class='form-control confirm-news-password'>
                                        </div>
                                    </div>
                                    <p>(*) Chú ý: Mật khẩu tối thiểu 6 ký tự bao gồm cả số</p>
                                </div>
                            </div>
                            <div class='text-center'>
                                <button type="submit" class='submit-reset-password'>Thay mật khẩu ngay</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_update_avatar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="padding-bottom:0px">
                    <h5 class="modal-title" id="exampleModalLabel"><span class="orange">Cập nhật</span> ảnh đại diện</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <form method="post" id='update_avatar'>
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                            <div class="row">
                                <div class="col-md-12 div-image">
                                <div class="file-input file-input-ajax-new">
                                    <div class="input-group file-caption-main">
                                        <div class="input-group-btn input-group-append">
                                            <div tabindex="500" class="btn btn-primary btn-file"><span class="hidden-xs">Chọn ảnh</span>
                                                <input type="file" class="upload-image" multiple="multiple" name="avatar_upload[]" data-fouc="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="file-preview ">
                                        <div class=" file-drop-zone">
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="avatar" class="image_data">
                            </div>
                            </div>
                            <div class='text-center'>
                                <button type="submit" class='submit-update-avatar'>Cập nhật</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="chat-message">
        <div class="chat-box">
            <div class="chat-head">
                <span class="status f-online"></span>
                <h6>Admin</h6>
                <div class="more">
                    <span class="close-mesage"><i class="fa fa-times"></i></span>
                </div>
            </div>
            <div class="chat-list">
                <ul class="ps-container ps-theme-default ps-active-y">
                    <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div>
                    <div class="ps-scrollbar-y-rail" style="top: 0px; height: 290px; right: 0px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 215px;"></div></div>
                </ul>
                <div class="text-box">
                    <input type='hidden' name='receiver_id' value='' id="receiver_id" />
                    <input type='file' name='file' >
                    <textarea placeholder="Tin nhắn..." name='message' id='content_message'></textarea>
                </div>
            </div>
        </div>
    </div>
</footer>
<input type='hidden' value='{{isset(\Auth::guard('member')->user()->id)? \Auth::guard('member')->user()->id : ''}}' id="private_id" />
<input type='hidden' value='' id="receiver" />
<script src="{!!asset('assets2/js/popper.min.js')!!}"></script>
<script src="{!!asset('assets2/js/bootstrap.min.js')!!}"></script>
<script src="{!!asset('assets2/plugins/parallax/parallax.js')!!}"></script>
<script src="{!!asset('assets2/js/scripts.js')!!}"></script>
<script src="{!!asset('assets2/js/select2.min.js')!!}"></script>
<script src="{!!asset('assets2/plugins/owl.carousel/owl.carousel.min.js')!!}"></script>
<script src="{!!asset('assets2/js/Notifier.min.js')!!}"></script>
<script src="{!!asset('assets2/js/bootbox.min.js')!!}"></script>
<script src="{!!asset('assets2/js/custom.js')!!}"></script>
<script src="{!!asset('assets2/js/main.js')!!}" id="_mainJS" data-plugins="load"></script>
<script src="https://js.pusher.com/4.4/pusher.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@if(!is_null(\Auth::guard('member')->user()))
<script>
    function scrollToBottomFunc() {
            $('.ps-container').animate({
                scrollTop: $('.ps-container').get(0).scrollHeight
            }, 50);
    }
    var my_id = $('#private_id').val();
        var receiver = '';
        Pusher.logToConsole = true;
        var pusher = new Pusher('5f84d2a344876b9fea04', {
            cluster: 'ap1',
            forceTLS: true
        });
        var channel = pusher.subscribe('chat-message');
        channel.bind('send-message', function (data) {
            if (my_id === data.from) {
                $('.ps-container').append(`
                    <li class="me">
                        <div class="chat-thumb"><img src="`+data.image+`" alt="" class="avatar-img"></div>
                        <div class="notification-event">
                            <span class="chat-message-item">
                                `+data.message+`
                            </span>
                            <span class="notification-date"><time datetime="2004-07-24T18:18" class="entry-date updated">{{ date('d m, h:i a') }}</time></span>
                        </div>
                    </li>
                `);
            }else if(my_id === data.to && receiver == data.from ){
                $('.ps-container').append(`
                    <li class="you">
                        <div class="chat-thumb"><img src="`+data.image+`" alt="" class="avatar-img"></div>
                        <div class="notification-event">
                            <span class="chat-message-item">
                                `+data.message+`
                            </span>
                            <span class="notification-date"><time datetime="2004-07-24T18:18" class="entry-date updated">{{ date('d m, h:i a') }}</time></span>
                        </div>
                    </li>
                `);
            }else if(my_id === data.to && receiver != data.from){
                if($('#count-message').html() === ''){
                     $('#count-message').html(1);
                }else{
                     $('#count-message').html(parseInt($('#count-message').html()) + 1);
                }
            }
            scrollToBottomFunc()
        });
    $(document).on('keyup', '#content_message', function (e) {
        e.preventDefault();
        var message = $(this).val();
        var receiver_id = $('#receiver_id').val();
        var my_id = $('#my_id').val();
        var type = $('#type_message').val();
        if (e.keyCode == 13 && message != '' && receiver_id != '') {
            $(this).val('');
            $.ajax({
                url: '/api/send-message',
                method: 'POST',
                data: {message:message,receiver_id:receiver_id,my_id:my_id,type:type},
                success: function (data) {
                    scrollToBottomFunc();
                }
            })
        }
    });
    $('.seen-chat').click(function(){
        var id = {{\Auth::guard('member')->user()->id}};
        $('#search_member_message').val('');
        $.ajax({
            url: '/api/get-all-message',
            method: 'POST',
            data: {id:id},
            success: function (data) {
                $('#list-message').html(data.html);
            }
        })
    });
    $('#search_member_message').keyup(function(){
        var id = {{\Auth::guard('member')->user()->id}};
        var full_name = $(this).val();
        $.ajax({
            url: '/api/get-all-message',
            method: 'POST',
            data: {id:id,full_name:full_name,_token: "{{ csrf_token() }}"},
            success: function (data) {
                $('#list-message').html(data.html);
            }
        })
    })
    $('body').delegate('.message','click',function(){
            var from = $(this).data('from');
            var to = $(this).data('to');
            receiver = $(this).data('to');
            $('.tab-message').removeClass('show');
            $.ajax({
                url: '/api/get-message',
                method: 'POST',
                data: {from: from,to:to},
                success: function (response) {
                    if (response.error === false) {
                        $('#chat-message').html(response.html);
                        scrollToBottomFunc();
                    }
                }
            });
    })
    $('body').delegate('.close-mesage','click',function(){
        $('.chat-box').removeClass("show");
        return false;
    });
    $('body').delegate('.upload-file','change', function(){
            var file_data = $(this).prop('files')[0];
            var receiver_id = $('#receiver_id').val();
            var my_id = $('#my_id').val();
            var type = $('#type_message').val();
            var form_data = new FormData();
            form_data.append('file', file_data);
            form_data.append('_token','{{ csrf_token() }}');
            form_data.append('receiver_id',receiver_id);
            form_data.append('type',type);
            form_data.append('my_id',my_id);
            $.ajax({
                url: '/api/send-file-message',
                method: 'POST',
                data: form_data,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(response){
                    if(response.success == true){
                        $input.val(response.image);
                    }
                }
             });
    });
    
</script>
<script>
    $('#seen-chat').click(function(){
        $('#count-message').html('');
    });
    $('.actve-message').click(function(){
        $('.actve-message').removeClass('actve-message');
    });
    $('body').delegate('.seen-notification', 'click', function () {
        var id = $(this).data('id');
        var link = $(this).data('link');
        $.ajax({
        url: '/api/seen-notification',
                method: 'POST',
                data:{id:id, _token : '{!! csrf_token() !!}'},
                success: function (response) {
                if (response.error == false) {
                    window.location.href = link;
                    //window.open(link);
                }
        }
        });
    });
    var channel = pusher.subscribe('NotificationEvent');
        channel.bind('send-notification', function(data) {
            var newNotificationHtml = `
            <li class="media">
                <a href="javascript:void(0)" class="seen-notification" data-id="${data.id}" data-link="${data.link}" style="width:100%">
                    <div class="media-body">
                        <div class="media-title">
                            <span class="font-weight-semibold color-blue">${data.full_name}</span>
                            <span class="text-muted float-right font-size-sm">${data.time}</span>
                        </div>
                        <span class="black font-small">${data.content}</span>
                    </div>
                </a>
            </li>
            `;
            var member_id = `${data.member_id}`;
            if (member_id == {!!\Auth::guard('member')->user()->id!!}){
                $('.list-notification').prepend(newNotificationHtml);
                $('#count-notification').html(`${data.count}`);
            }
        }
    );
    window.Pusher = undefined;
    $('.new-group').click(function(){
        $('#groupchat').modal('show');
    })
    $("body").delegate( "#frmAddGroup", "submit", function(e){
        e.preventDefault();
        $.ajax({
            url: '/api/add-group',
            method: 'POST',
            data: new FormData(this),
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (response) {
                if (response.error === false) {
                    $('#groupchat').modal('hide');
                    $('#frmAddGroup')[0].reset();
                    $('.custom-control-input').each(function () {
                        $(this).parent('span').removeClass("checked");
                        $(this).prop('checked', false);
                    });
                    swal('Thêm mới nhóm thành công');
                }else{
                    swal('Tên nhóm đã tồn tạo mời nhập tên khác');
                }
            }
        });
    })
    $("#btnSearch").on('click', function (e) {
        e.preventDefault();
        if (!$('#searchTag').val().trim().length) {
            $('#searchTag').focus();
            return false;
        } else if ($('#searchTag').val().trim().match(/(\/|\'|\"|NULL|null|<|>|--|-->)/i)) {
            $('#searchTag').val('');
            $('#searchTag').attr('title', lang_pack.not_valid_character_search).attr('data-toggle', 'tooltip').tooltip();
            return false;
        } else if ($('#searchTag').attr('data-original-title')) {
            $('#searchTag').tooltip('dispose');
        }
        var keyword = $('#searchTag').val();
        var link = $('[name="poscats"] option:selected').data('route');
        location.href = link + '?keywords=' + keyword;
    });
    $('.bootstrap-select').niceSelect();
    $(document).ready(function(){
        $('body').delegate('#header_pulldown_appmenu_grn','click',function(e){
            e.stopPropagation();
            if($(this).parent().find('#header_pulldown_appmenu_base_grn').hasClass('show')){
                $('#header_pulldown_appmenu_base_grn').removeClass('show');
            }else{
                $('#header_pulldown_appmenu_base_grn').addClass('show');
            }
        })
        $(document).on("click", function(event){
            if(!$(event.target).closest(".dropdown").length){
                $('#header_pulldown_appmenu_base_grn').removeClass('show');
            }
        });
    })
    $('body').delegate('.seen-all-notification','click',function(e){
        e.preventDefault();
        $.ajax({
            url: '/api/seen-all-notification',
            method: 'POST',
            success: function (response) {
                $('.list-notification').html('');
                $('#count-notification').html('');
            }
        });
    })
</script>
@endif