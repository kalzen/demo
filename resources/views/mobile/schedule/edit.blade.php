@extends('mobile.layouts.mobile_schedule')
@section('content')
<div class="mobile_spinner_grn" style="display:none;"><span></span></div>
<div data-role="page">
   <script src="{{asset('assets/mobile/js/checkbox_mobile.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/mobile/js/validator_mobile.js')}}" type="text/javascript"></script>
   <script src="{{asset('/js/timezone_info.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/mobile/js/schedule_add.js')}}" type="text/javascript"></script>
   <form id="schedule_mobile_modify" name="schedule/mobile/modify" method="post" enctype="multipart/form-data" action="/scripts/garoon/grn.exe/schedule/mobile/command_modify?" data-ajax="false">
      <input type="hidden" name="_token" value="{{ csrf_token() }}" />
      <input type='hidden' name='pattern' value='1'>
      <input type="hidden" name='event_id' value="1047">
      <input type="hidden" name='bdate' value="2021-01-25">
      <input type="hidden" name="uid" value="58">
      <input type="hidden" name="referer_key" value="383ceb102e45b790248591f876072ee6">
      <input type="hidden" name="tmp_key" value="1611549271">
      <input type="hidden" name="allow_file_attachment" value="1">
      <div data-role="content" class="mobile-content-grn" data-theme="c">
         <!--breadcrumb-->
         <script src="{{asset('assets/mobile/js/mobile_breadcrumb.js')}}" type="text/javascript"></script>
         <div class="mobile_breadcrumbtitle_grn mobile_titlebar_content_withList_grn">
            <div class="mobile_breadcrumbtitle_left_grn " id="left_text"><a href="/scripts/garoon/grn.exe/schedule/mobile/view?event=1047&amp;bdate=2021-01-25&amp;referer_key=383ceb102e45b790248591f876072ee6">Details</a><span class="mobile_icon_breadcrumb_arrow_grn"></span></div>
            <div class="mobile_breadcrumbtitle_right_grn" id="right_text"><span class="mobile_icon_breadcrumb_grn mobile_app_schedule_s_b_grn"></span>Edit appointment</div>
         </div>
         <!--end breadcrumb-->
         <div class="mobile_schedule_grn">
            <div class="mobile-div-title-grn">
            </div>
            <!--mobile-div-title-grn-->
            <div class="mobile-div-title-grn">
               <link href="{{asset('assets/mobile/css/mobile_datepicker.css')}}" rel="stylesheet" type="text/css">
               <script src="{{asset('assets/mobile/js/mobile_datepicker.js')}}" type="text/javascript"></script>
               <script language="JavaScript" text="text/javascript">
                  (function(){
                      $(document).ready(function(){
                        $('#start').on('click',function(event){
                            datepicker.setIconOnly('');
                            datepicker.setCurrentElement(event.target);
                            datepicker.setInputAssociate('end');
                        });
                      });
                  })();
               </script>
               <script id="datepicker_header" type="text/x-template">
                  <div class="mobile_datepicker_sun_grn mobile_datepicker_listtitle_grn"><span>Sun</span></div><div class="mobile_datepicker_weekdays_grn mobile_datepicker_listtitle_grn"><span>Mon</span></div><div class="mobile_datepicker_weekdays_grn mobile_datepicker_listtitle_grn"><span>Tue</span></div><div class="mobile_datepicker_weekdays_grn mobile_datepicker_listtitle_grn"><span>Wed</span></div><div class="mobile_datepicker_weekdays_grn mobile_datepicker_listtitle_grn"><span>Thu</span></div><div class="mobile_datepicker_weekdays_grn mobile_datepicker_listtitle_grn"><span>Fri</span></div><div class="mobile_datepicker_sat_grn mobile_datepicker_listtitle_grn"><span>Sat</span></div>
               </script>
               <script id="datepicker_footer" type="text/x-template">
                  <div data-theme="c" class="mobile_button_area_grn"><div class="mobile_cancel_grn"><input type="reset" value="Close" data-inline="true" data-theme="c"/></div></div>
               </script>
               <div class="mobile-contentSubButton-grn mobile_select_date_grn mobile_float_left">
                  <a id="start" href="#dialog_datepicker" data-rel="popup" data-transition="pop" data-theme="c" data-role="button" class="mobile-btn-calender-grn">
                  <span class="mobile-icon-calender-grn"></span>
                  <span class="mobile-font-normal-grn">{{date('M /d (D) /Y',strtotime($schedule->start_date))}}</span>
                  </a>
                  <input type="hidden" id="start_set" name="start_set" value="{{date('Y-m-d',strtotime($schedule->start_date))}}">
                  <input type="hidden" id="start_year" name="start_year" value="{{date('Y',strtotime($schedule->start_date))}}">
                  <input type="hidden" id="start_month" name="start_month" value="{{date('m',strtotime($schedule->start_date))}}">
                  <input type="hidden" id="start_day" name="start_day" value="{{date('d',strtotime($schedule->start_date))}}">
               </div>
               <script language="JavaScript" text="text/javascript">
                  var settings_datepicker = {
                      container         : 'dialog_datepicker',
                      year_unit         : '',
                      month_unit        : '',
                      day_unit          : '',
                      delimiter         : '/',
                      date_order        : 'njY',
                      month_name        : 'Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec',
                      wday_name         : 'Sun,Mon,Tue,Wed,Thu,Fri,Sat',
                      ajaxURLGetHoliday : "/scripts/garoon/grn.exe/grn/ajax_get_holiday_for_mobile_datepicker?",
                      locale            : 'en'
                  };
                  
                  var datepicker;
                  $(document).ready(function(){
                      datepicker = new grn.component.mobile_datepicker(settings_datepicker);
                  });
                  
               </script>
               <div data-role="popup" id="dialog_datepicker" data-shadow="false" data-overlay-theme="b">
                  <div class="mobile_datepicker_page_grn">
                     <div class="mobile_datepicker_bar_grn">
                        <ul>
                           <li class="mobile_width_a_grn"><span class="mobile_datepicker_icon_backsub_grn"><a class="previous_month" href="#"></a></span></li>
                           <li class="mobile_width_b_grn">
                              <div data-role="fieldcontain" class="mobile_titlebar_right_grn mobile_select_grn">
                                 <select class="year_select">
                                    <option value="1970">1970</option>
                                    <option value="1971">1971</option>
                                    <option value="1972">1972</option>
                                    <option value="1973">1973</option>
                                    <option value="1974">1974</option>
                                    <option value="1975">1975</option>
                                    <option value="1976">1976</option>
                                    <option value="1977">1977</option>
                                    <option value="1978">1978</option>
                                    <option value="1979">1979</option>
                                    <option value="1980">1980</option>
                                    <option value="1981">1981</option>
                                    <option value="1982">1982</option>
                                    <option value="1983">1983</option>
                                    <option value="1984">1984</option>
                                    <option value="1985">1985</option>
                                    <option value="1986">1986</option>
                                    <option value="1987">1987</option>
                                    <option value="1988">1988</option>
                                    <option value="1989">1989</option>
                                    <option value="1990">1990</option>
                                    <option value="1991">1991</option>
                                    <option value="1992">1992</option>
                                    <option value="1993">1993</option>
                                    <option value="1994">1994</option>
                                    <option value="1995">1995</option>
                                    <option value="1996">1996</option>
                                    <option value="1997">1997</option>
                                    <option value="1998">1998</option>
                                    <option value="1999">1999</option>
                                    <option value="2000">2000</option>
                                    <option value="2001">2001</option>
                                    <option value="2002">2002</option>
                                    <option value="2003">2003</option>
                                    <option value="2004">2004</option>
                                    <option value="2005">2005</option>
                                    <option value="2006">2006</option>
                                    <option value="2007">2007</option>
                                    <option value="2008">2008</option>
                                    <option value="2009">2009</option>
                                    <option value="2010">2010</option>
                                    <option value="2011">2011</option>
                                    <option value="2012">2012</option>
                                    <option value="2013">2013</option>
                                    <option value="2014">2014</option>
                                    <option value="2015">2015</option>
                                    <option value="2016">2016</option>
                                    <option value="2017">2017</option>
                                    <option value="2018">2018</option>
                                    <option value="2019">2019</option>
                                    <option value="2020">2020</option>
                                    <option value="2021" selected>2021</option>
                                    <option value="2022">2022</option>
                                    <option value="2023">2023</option>
                                    <option value="2024">2024</option>
                                    <option value="2025">2025</option>
                                    <option value="2026">2026</option>
                                    <option value="2027">2027</option>
                                    <option value="2028">2028</option>
                                    <option value="2029">2029</option>
                                    <option value="2030">2030</option>
                                    <option value="2031">2031</option>
                                    <option value="2032">2032</option>
                                    <option value="2033">2033</option>
                                    <option value="2034">2034</option>
                                    <option value="2035">2035</option>
                                    <option value="2036">2036</option>
                                    <option value="2037">2037</option>
                                 </select>
                              </div>
                           </li>
                           <li class="mobile_width_c_grn">
                              <div data-role="fieldcontain" class="mobile_titlebar_right_grn mobile_select_grn">
                                 <select class="month_select">
                                    <option value="1" selected>Jan</option>
                                    <option value="2">Feb</option>
                                    <option value="3">Mar</option>
                                    <option value="4">Apr</option>
                                    <option value="5">May</option>
                                    <option value="6">Jun</option>
                                    <option value="7">Jul</option>
                                    <option value="8">Aug</option>
                                    <option value="9">Sep</option>
                                    <option value="10">Oct</option>
                                    <option value="11">Nov</option>
                                    <option value="12">Dec</option>
                                 </select>
                              </div>
                           </li>
                           <li class="mobile_width_d_grn"><a class="today" href="#">Today</a></li>
                           <li class="mobile_width_e_grn"><span class="mobile_datepicker_icon_advancesub_grn"><a class="next_month" href="#"></a></span></li>
                        </ul>
                     </div>
                     <!-- /mobile_datepicker_bar -->
                     <div class="mobile_datepicker_grn">
                        <div class="mobile_datepicker_list_grn">
                        </div>
                        <!-- /grid-c -->
                     </div>
                     <!-- end of mobile_datepicker_grn-->
                  </div>
               </div>
               <script language="JavaScript" text="text/javascript">
                  (function(){
                      $(document).ready(function(){
                        $('#start_set').on('change', function(event){
                          datepicker.handlerChangeValue(event);
                        });
                      });
                  })();
               </script>
               <div data-role="fieldcontain" class="mobile_select_date_grn mobile_select_time_grn">
                  <select id="start_hour" name="start_hour" >
                     {!!$start_hour_html!!}
                  </select>
               </div>
               <div data-role="fieldcontain" class="mobile_select_date_grn mobile_select_time_grn">
                  <select id="start_minute" name="start_minute">
                     {!!$start_minute_html!!}
                  </select>
               </div>
               <span class="mobile_schedule_timeline_grn"></span>
               <span class="mobile_timezone_grn">(UTC+09:00) Tokyo</span>
               <input type='hidden' id='timezone' name='timezone' value='Asia/Tokyo' />
            </div>
            <!--mobile-div-title-grn-->
            <div class="mobile-div-title-grn">
               <script language="JavaScript" text="text/javascript">
                  (function(){
                      $(document).ready(function(){
                        $('#end').on('click',function(event){
                            datepicker.setIconOnly('');
                            datepicker.setCurrentElement(event.target);
                            datepicker.setInputAssociate('');
                        });
                      });
                  })();
               </script>
               <script id="datepicker_header" type="text/x-template"></script>
               <script id="datepicker_footer" type="text/x-template">
                  <div data-theme="c" class="mobile_button_area_grn"><div class="mobile_cancel_grn"><input type="reset" value="Close" data-inline="true" data-theme="c"/></div></div>
               </script>
               <div class="mobile-contentSubButton-grn mobile_select_date_grn mobile_float_left">
                  <a id="end" href="#dialog_datepicker" data-rel="popup" data-transition="pop" data-theme="c" data-role="button" class="mobile-btn-calender-grn">
                  <span class="mobile-icon-calender-grn"></span>
                  <span class="mobile-font-normal-grn">{{date('M /d (D) /Y',strtotime($schedule->end_date))}}</span>
                  </a>
                  <input type="hidden" id="end_set" name="end_set" value="{{date('Y-m-d',strtotime($schedule->end_date))}}">
                  <input type="hidden" id="end_year" name="end_year" value="{{date('Y',strtotime($schedule->end_date))}}">
                  <input type="hidden" id="end_month" name="end_month" value="{{date('m',strtotime($schedule->end_date))}}">
                  <input type="hidden" id="end_day" name="end_day" value="{{date('d',strtotime($schedule->end_date))}}">
               </div>
               <script language="JavaScript" text="text/javascript">
                  (function(){
                      $(document).ready(function(){
                        $('#end_set').on('change', function(event){
                          datepicker.handlerChangeValue(event);
                        });
                      });
                  })();
               </script>
               <div data-role="fieldcontain" class="mobile_select_date_grn mobile_select_time_grn">
                  <select id="end_hour" name="end_hour" >
                     {!!$end_hour_html!!}
                  </select>
               </div>
               <div data-role="fieldcontain" class="mobile_select_date_grn mobile_select_time_grn">
                  <select id="end_minute" name="end_minute">
                     {!!$end_minute_html!!}
                  </select>
               </div>
               <span class="mobile_timezone_grn">(UTC+09:00) Tokyo</span>
               <input type='hidden' id='end_timezone' name='end_timezone' value='Asia/Tokyo' />
               <div id="validate_date" style="display:none;" class="mobile_schedule_list_grn mobile_schedule_information_alert_grn">
                  <span class="mobile_iconposition_attentions_grn mobile_icon_attentions_grn"></span>
                  <span class="mobile_text_information_grn">Start time is empty.</span>
               </div>
               <div id="invalid_date" style="display:none;" class="mobile_schedule_list_grn mobile_schedule_information_alert_grn">
                  <span class="mobile_iconposition_attentions_grn mobile_icon_attentions_grn"></span>
                  <span class="mobile_text_information_grn">The end time must follow the start time.</span>
               </div>
            </div>
            <div class="mobile-div-title-grn">
               <span class="mobile-label-grn">Appointment type</span>
               <a id="menu_schedule" href="#popup_menu_schedule" data-rel="popup" data-transition="pop" data-shadow="true" data-wrapperels="span" class="mobile_select_view_grn">
               <span class="mobile_event_menu_content_grn">----------</span>
               <span class="mobile_select_icon_grn"></span>
               </a>
               <input type="hidden" value="" name="menu" id="menu_schedule_value">
               <script src="{{asset('assets/mobile/js/mobile_select_menu.js')}}" type="text/javascript"></script>
               <script language="JavaScript" text="text/javascript">
                  (function(){
                  
                  
                      var settings = {
                          selectMenu       : '#menu_schedule',
                          selectMenuValue  : '#menu_schedule_value',
                          container        : '#popup_menu_schedule',
                          selectedValue    : '{{is_null($schedule->menu)? '' : $schedule->menu }}',
                          expandMenuSelect : '{{is_null($schedule->menu)? '#' : explode(';#',$schedule->menu)[0] }}',
                          selectMenuTitle  : 'Appointment type',
                          useMenuColor     : 'true',
                          defaultTitle     : '--------------------',
                          name             : 'mobileSelectMenu_popup_menu_schedule'
                      };
                  
                  
                      $(document).ready(function(){
                          var G = new grn.component.mobile_select_menu(settings);
                          G.initSize =false;
                  
                          $(document).on("popupbeforeposition","#popup_menu_schedule",function(event){
                              if(!G.initSize || G.needResize)
                              {
                                  G.setSizeSelectMenu(event);
                                  G.initSize = true;
                              }
                          });
                  
                          $('#popup_menu_schedule').on('click',function(event){
                              G.setSizeSelectMenu(event);
                          });
                  
                          $( window ).on( "resize", function( event ) {
                              var popup = $('#popup_menu_schedule');
                              if(popup.is(':visible'))
                              {
                                  G.setSizeSelectMenu(event);
                              }
                          });
                      });
                  
                  })();
               </script>
               <div data-role="popup" id="popup_menu_schedule" data-corners="false" data-overlay-theme="b" data-shadow="false">
                  <div class="mobile_select_menu_titlebar_div_grn">
                     <div class="mobile_titlebar_grn">
                        <span class="mobile_text_grn">Appointment type</span>
                        <a href="#" class="mobile_titlebar_right_grn mobile_colse_icon_grn"></a>
                     </div>
                     <ul data-role="listview" data-theme="c" class="mobile-ul-top-grn mobile_ul_grn select_menu_list">
                        <li data-icon="false" class="mobile_list_grn mobile_event_list_grn">
                           <span class="mobile_select_item"></span>
                           <a href="#" class="selection_item" data-value="">--------------------</a>
                        </li>
                        <li data-icon="false" class="mobile_list_grn mobile_event_list_grn">
                           <span class="mobile_select_item "></span>
                           <span class="mobile_event_menu_grn mobile_event_menu_color5_grn"></span>
                           <a href="#" class="selection_item" data-value="Cuộc họp;#5">Cuộc họp</a>
                        </li>
                        <li data-icon="false" class="mobile_list_grn mobile_event_list_grn">
                           <span class="mobile_select_item "></span>
                           <span class="mobile_event_menu_grn mobile_event_menu_color2_grn"></span>
                           <a href="#" class="selection_item" data-value="Hội thảo;#2">Hội thảo</a>
                        </li>
                        <li data-icon="false" class="mobile_list_grn mobile_event_list_grn">
                           <span class="mobile_select_item "></span>
                           <span class="mobile_event_menu_grn mobile_event_menu_color1_grn"></span>
                           <a href="#" class="selection_item" data-value="Đào tạo;#1">Đào tạo</a>
                        </li>
                        <li data-icon="false" class="mobile_list_grn mobile_event_list_grn">
                           <span class="mobile_select_item "></span>
                           <span class="mobile_event_menu_grn mobile_event_menu_color7_grn"></span>
                           <a href="#" class="selection_item" data-value="Công tác;#7">Công tác</a>
                        </li>
                        <li data-icon="false" class="mobile_list_grn mobile_event_list_grn">
                           <span class="mobile_select_item "></span>
                           <span class="mobile_event_menu_grn mobile_event_menu_color6_grn"></span>
                           <a href="#" class="selection_item" data-value="Ngày lễ;#6">Ngày lễ</a>
                        </li>
                        <li data-icon="false" class="mobile_list_grn mobile_event_list_grn">
                           <span class="mobile_select_item "></span>
                           <span class="mobile_event_menu_grn mobile_event_menu_color3_grn"></span>
                           <a href="#" class="selection_item" data-value="Phỏng vấn;#3">Phỏng vấn</a>
                        </li>
                        <li data-icon="false" class="mobile_list_grn mobile_event_list_grn">
                           <span class="mobile_select_item "></span>
                           <span class="mobile_event_menu_grn mobile_event_menu_color3_grn"></span>
                           <a href="#" class="selection_item" data-value="Gặp gỡ;#3">Gặp gỡ</a>
                        </li>
                     </ul>
                     <div class="mobile_select_button_area_grn">
                        <div class="mobile_cancel_grn">
                           <input class="cancel_button" type="reset" value="Close" data-inline="true" data-theme="c"/>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--mobile-div-title-grn-->
            <div class="mobile-div-title-grn">
               <span class="mobile-label-grn">Title</span>
               <input  id="title" type="text" name="title" size="50" maxlength="100" value="{{$schedule->title}}" onKeyPress="return event.keyCode != 13;">
            </div>
            <!--mobile-div-title-grn-->
            <ul data-role="listview" data-theme="c" class="mobile-li-PersonInCharge-grn mobile-div-title-grn">
               <div class="mobile-separation-grn"></div>
               <li data-icon="false">
                  <a id="attendees" class="ui-link-inherit" href="#_sUID">
                     <div class="mobile-label-small-grn">Attendees</div>
                     <div class="mobile_for_totalnumber_grn">
                     </div>
                     <span class="totalNumber-grn"></span>
                     <span class="mobile-array-todo-grn"></span>
                  </a>
               </li>
               <div class="mobile-separation-grn"></div>
               <li data-icon="false" class="ui-first-child">
                  <a id="default_public" href="#_p_sUID" class="ui-link-inherit ui-btn" id="">
                     <div class="mobile-label-small-grn">Shared</div>
                     <div class="mobile_for_totalnumber_grn"></div>
                     <!--mobile_for_totalnumber_grn--><span class="totalNumber-grn"></span><span class="mobile-array-todo-grn"></span>
                  </a>
               </li>
               <div class="mobile-separation-grn"></div>
               <li data-icon="false">
                  <a id="facilities" class="ui-link-inherit" href="#_sITEM">
                     <div class="mobile-label-small-grn">Facilities</div>
                     <div class="mobile_for_totalnumber_grn">
                     </div>
                     <!--mobile_for_totalnumber_grn-->
                     <span class="totalNumber-grn"></span>
                     <span class="mobile-array-todo-grn"></span>
                  </a>
               </li>
               <div class="mobile-separation-grn"></div>
            </ul>
            <div class="mobile-div-title-grn" style="display:none;" id="using_purpose_element">
               <span class="mobile-label-grn">Purpose<span class="mobile-required-grn">*</span></span>
               <div id="require_using_purpose" style="display:none;" class="mobile_information_alert_grn">
                  <span class="mobile_iconposition_attentions_grn mobile_icon_attentions_grn"></span>
                  <span class="mobile_text_information_grn">Purpose is required.</span>
               </div>
               <textarea id="using_purpose" name="using_purpose" class="mobile-textarea-grn autoexpand" wrap="virtual" role="form"   cols="50" rows="5" style="white-space:pre-wrap"></textarea>
            </div>
            <!--mobile-div-title-grn-->
            <div class="mobile-div-title-grn">
               <span class="mobile-label-grn">Notes</span>
               <textarea id="textarea_id" name="memo" class="mobile-textarea-grn autoexpand ui-input-text ui-shadow-inset ui-body-inherit ui-corner-all ui-textinput-autogrow" wrap="virtual" role="form" cols="50" rows="5" style="white-space: pre-wrap; height: 175px;">{{$schedule->memo}}</textarea>
            </div>
            <!--mobile-div-title-grn-->
            <div class="mobile-div-title-grn">
               <span class="mobile-label-grn">Attachments</span>
               <script src="{{asset('assets/mobile/js/mobile_attach_file.js')}}" type="text/javascript"></script>
               <div id="schedule/mobile/modify_file" class="mobile-attachmentArea-grn mobile-div-title-grn">
                  <div class="mobile-selectAttachment-grn">
                     <span>
                     <input type="file" id="schedule/mobile/modify_file_id" name="file_input" value="" class="mobile-buttonSelectAttachment-grn" onChange="grn.component.mobile_attach_file.onSelected(document.forms['schedule/mobile/modify'], this)" />
                     <input type="hidden" name="upload_ticket" value="387110b647205b3d61f44ce68fae4f34745267d037aaadb46128cb11ce6c28ac" />
                     <span class="mobile-contentSubButton-grn">
                     <a id="attach_button" href="javascript:void(0);" data-role="button" class="mobile-attachmentButton-add-grn"></a>
                     </span>
                     </span>
                  </div>
               </div>
               <!--attachmentArea-->      
            </div>
            <div class="mobile-div-title-grn">
               <span class="mobile-label-grn">Visibility</span>
               <div id="idPrivateRaidoButton" class="mobile_radiobutton_group_grn">
                  <span class="mobile_radiobutton_base_grn" style="margin-right: 14px;">
                     <span class="mobile_icon_radiobutton_grn mobile_icon_radiobuttonon_grn"></span>
                     <span class="mobile_radiobutton_grn">
                        <div class=" ui-radio"><input type="radio" name="" id="" value="0"></div>
                     </span>
                     <span class="mobile_text_grn">Public</span>
                  </span>
                  <span class="mobile_radiobutton_base_grn" style="margin-right: 14px;">
                     <span class="mobile_icon_radiobutton_grn mobile_icon_radiobuttonoff_grn"></span>
                     <span class="mobile_radiobutton_grn">
                        <div class=" ui-radio"><input type="radio" name="" id="" value="1"></div>
                     </span>
                     <span class="mobile_text_grn">Private</span>
                  </span>
               </div>
            </div>
            <div id="attendane_confirm_html" style="display:none;">
               <div class="mobile_titlebar_grn">
                  <div class="mobile_breadcrumbtitle_left_grn"><span class="mobile_icon_breadcrumb_grn mobile_app_schedule_s_b_grn"></span>Edit appointment</div>
               </div>
               <div data-role="listview" data-theme="c" class="mobile-ul-top-grn mobile_ul_grn">
                  <div class="mobile_schedule_grn mobile_overlay_wrap_grn mobile_schedule_overlay_grn">
                     <div class="mobile-div-title-grn mobile_update_title_text_grn">
                        <span><span class="mobile-label-grn">Subject:</span><span id="attendance_check_confirm_title" class="mobile_attendance_check_confirm_title"></span></span>
                     </div>
                     <div class="mobile-div-title-grn">
                        <label for="need_refresh"><a id="attendance_check" href="javascript:void(0);" class="mobile-checkboxOff-todo-grn"></a><span class="mobile_checkbox_grn"><input name="need_refresh" id="need_refresh" type="checkbox" data-role="none" class="mobile-input-grn"/></span></label><span class="mobile_btn_checkboxtext_grn">Clear all attendance responses</span>
                     </div>
                  </div>
                  <div class="mobile_changeBtnMargin_grn"></div>
                  <div data-theme="c" class="mobile_button_area_grn">
                     <div class="mobile_ok_grn">
                        <input id="confirm_dialog_btn_yes" type="button" value="Save" data-inline="true" data-theme="c"/>
                     </div>
                     <div class="mobile_cancel_grn">
                        <input id="confirm_dialog_btn_no" type="button" value="Cancel" data-inline="true" data-theme="c"/>
                     </div>
                  </div>
               </div>
            </div>
            <script src="{{asset('assets/mobile/js/handle_add.js')}}" type="text/javascript"></script>
            <script type="text/javascript" language="javascript">
               var handleAdd = grn.page.schedule.mobile.handle_add;
               handleAdd.ajaxURL = '/schedule/update/{{$schedule->id}}';
               handleAdd.defaultBackURL = '/schedule/index?';
               handleAdd.formId = 'schedule_mobile_modify';
               handleAdd.typeSchedule = 'modify';
            </script>
            <div class="mobile_margin_plus_button_grn"></div>
            <div data-theme="c" class="mobile-buttonArea-grn">
               <div class="mobile_ok_grn">
                  <input id="addBtn" type="button" value="Save" data-inline="true" data-theme="c"/>
               </div>
               <div class="mobile_cancel_grn mobile_show_overlay_js">
                  <input id="cancelBtn" class="cancel_button" type="reset" value="Cancel" data-inline="true" data-theme="c" onclick="javascript:location.href='{{route('frontend.schedule.view',$schedule->id)}}'"/>
               </div>
            </div>
         </div>
         <!-- end of mobile_addschedule_grn-->
      </div>
      <!-- end of content-->
      <script src="{{asset('assets/mobile/js/mobile_footer_for_multipage.js')}}" type="text/javascript"></script>
   </form>
</div>
<link href="{{asset('assets/mobile/css/mobile_userselect.css')}}" rel="stylesheet" type="text/css">
<script src="{{asset('assets/mobile/js/mobile_item_select.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/mobile/js/mobile_user_select.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/resource-en.js')}}"></script>
<script language="JavaScript" text="text/javascript">
   (function(){
   
       
       var settings = {
           element_id  : 'sUID',
           ajaxGetItemURL : "/api/ajax_get_user_for_mobile_selection?",
           ajaxSearchItemURL : "/api/ajax_get_user_for_mobile_selection?",
           args        : "access_plugin=YToyOntzOjQ6Im5hbWUiO3M6ODoic2NoZWR1bGUiO3M6NjoicGFyYW1zIjthOjM6e3M6NjoiYWN0aW9uIjthOjM6e2k6MDtzOjQ6InJlYWQiO2k6MTtzOjM6ImFkZCI7aToyO3M6NjoibW9kaWZ5Ijt9czoxMjoic2Vzc2lvbl9uYW1lIjtzOjIyOiJzY2hlZHVsZS9tb2JpbGUvbW9kaWZ5IjtzOjEyOiJsYXhfZXZhbHVhdGUiO2I6MTt9fQ==&plugin_session_name=schedule/mobile/modify&app_id=schedule",
           include_org : '1',
           isCalendar : '1',
           show_group_role : '1',
           isAllowedRole : '',
           src_item    : '#src_user',
           dest_item   : '#dest_user',
           type_item   : 'user',
           empty_list  : "",
           none_selected : "None",
           search_caption: "(Search results)",
           group_select_menu_id : 'category_options_sUID',
           associate_id : 'attendees',
           selectedItem : {!!$list_member_selected!!},
           page_name : '',
           default_title_group_select : 'Select group.',
           categorySelectUI : 'user_category_sUID',
           categorySelectValue : 'category_title_sUID',
           popupItemCategory : 'popup_user_category_sUID'
       };
       
   
       $(document).ready(function(){
           var G = new grn.component.mobile_user_select(settings);
           G.initSelectedItem();
   
           $(document).on("pagebeforeshow","#_sUID",function(event, data){
               G.bindEventBeforePageShow();
   
               if(typeof data.prevPage.attr('id') == 'undefined')
               {
                   G.processing = false;
                   G.saveLastState();
                   G.resetSourceItemList();
                   G.resetCategorySelect();
               }
           });
   
           $(document).on("pagebeforehide","#_sUID",function(event, data){
               if(typeof data.nextPage.attr('id') == 'undefined' && !G.adding )
               {
                   G.cancel();
               }
               G.adding = false;
           });
       });
   
   })();
</script>
<script id="src_user" type="text/x-template">
   <li data-icon="false" data-corners="false" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-iconpos="right" data-theme="c">
       <a class="selection_item ui-btn" href="#">
           <div class="mobile_add_grn"></div>
           <div class="mobile_user_photo_grn mobile_img_userPlofile_grn"></div>
           <div class="mobile_info_grn">
               <div class="mobile_position_center_grn">
                   <div class="mobile_position_width_grn">
                       <div class="mobile_user_grn"></div>
                       <div class="mobile_text_grn"></div>
                   </div>
               </div>
           </div>
       </a>
   </li>
</script>
<script id="dest_user" type="text/x-template">
   <li data-icon="false" class="mobile_selected_grn" data-corners="false" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-iconpos="right" data-theme="c">
       <div class="mobile_delete_grn"></div>
       <a class="selection_item ui-btn" href="#">
           <div class="mobile_user_photo_grn mobile_img_userPlofile_grn"></div>
           <div class="mobile_info_grn">
               <div class="mobile_position_center_grn">
                   <div class="mobile_position_width_grn">
                       <div class="mobile_user_grn"></div>
                       <div class="mobile_text_grn"></div>
                   </div>
               </div>
           </div>
           <span></span>
       </a>
   </li>
</script>
<div data-role="page" id="_sUID">
   <div data-role="content" data-theme="c" class="mobile-content-grn">
      <div class="mobile_breadcrumbtitle_grn mobile_titlebar_content_withList_grn">
         <div class="mobile_breadcrumbtitle_left_grn"><a href="#" >Edit appointment</a><span class="mobile_icon_breadcrumb_arrow_grn"></span></div>
         <div class="mobile_breadcrumbtitle_right_grn"><span class="mobile_icon_breadcrumb_grn mobile_app_schedule_s_b_grn"></span>Attendees</div>
      </div>
      <div class="mobile_seletedUser_list_grn">
         <form action="#">
            <div class="mobile_user_search_grn">
               <input id="search_user" class="search_item" type="search" name="search_user_sUID" placeholder="User search"/>
               <a href="#" class="mobile_delete_icon_grn delete_input"></a>
               <a href="#" class="mobile_icon_grn search_item_icon"></a>
            </div>
         </form>
      </div>
      <div class="mobile_user_group_grn">
         <a id="user_category_sUID" href="#popup_user_category_sUID" data-rel="popup" data-transition="pop" data-shadow="true" data-wrapperels="span" class="mobile_select_view_grn">
         <span class="mobile_event_menu_content_grn">Select group.</span>
         <span class="mobile_select_icon_grn"></span>
         </a>
         <a class="ui-link-inherit" href="#_popup_group_categories_tree_sUID">
            <div class="mobile_group_icon_grn"></div>
         </a>
         <input type="hidden" value="" id="category_options_sUID">
         <input type="hidden" value="" id="category_title_sUID">
         <script language="JavaScript" text="text/javascript">
            (function(){
            
            
                var settings = {
                    selectMenu       : '#user_category_sUID',
                    selectMenuValue  : '#category_options_sUID',
                    container        : '#popup_user_category_sUID',
                    selectedValue    : 'Select group.',
                    expandMenuSelect : '#category_title_sUID',
                    selectMenuTitle  : 'Select group',
                    useMenuColor     : '',
                    defaultTitle     : '',
                    name             : 'mobileSelectMenu_popup_user_category_sUID'
                };
            
            
                $(document).ready(function(){
                    var G = new grn.component.mobile_select_menu(settings);
                    G.initSize =false;
            
                    $(document).on("popupbeforeposition","#popup_user_category_sUID",function(event){
                        if(!G.initSize || G.needResize)
                        {
                            G.setSizeSelectMenu(event);
                            G.initSize = true;
                        }
                    });
            
                    $('#popup_user_category_sUID').on('click',function(event){
                        G.setSizeSelectMenu(event);
                    });
            
                    $( window ).on( "resize", function( event ) {
                        var popup = $('#popup_user_category_sUID');
                        if(popup.is(':visible'))
                        {
                            G.setSizeSelectMenu(event);
                        }
                    });
                });
            
            })();
         </script>
         <div data-role="popup" id="popup_user_category_sUID" data-corners="false" data-overlay-theme="b" data-shadow="false">
            <div class="mobile_select_menu_titlebar_div_grn">
               <div class="mobile_titlebar_grn">
                  <span class="mobile_text_grn">Select group</span>
                  <a href="#" class="mobile_titlebar_right_grn mobile_colse_icon_grn"></a>
               </div>
               <ul data-role="listview" data-theme="c" class="mobile-ul-top-grn mobile_ul_grn select_menu_list">
                  <li data-icon="false" class="mobile_list_grn ">
                     <span class="mobile_select_item "></span>
                     <a href="#" class="selection_item" data-value="g15">第1営業グループ(Priority organization) </a>
                  </li>
                  <li data-icon="false" class="mobile_list_grn ">
                     <span class="mobile_select_item "></span>
                     <a href="#" class="selection_item" data-value="g18">Cyde America(Membership)</a>
                  </li>
                  <li data-icon="false" class="mobile_list_grn ">
                     <span class="mobile_select_item "></span>
                     <a href="#" class="selection_item" data-value="frequent">(Recently selected users)</a>
                  </li>
                  <li data-icon="false" class="mobile_list_grn mobile_select_menu_last_item_grn">
                     <span class="mobile_select_item"></span>
                     <a href="#" data-value="">&nbsp</a>
                  </li>
               </ul>
               <div class="mobile_select_button_area_grn">
                  <div class="mobile_cancel_grn">
                     <input class="cancel_button" type="reset" value="Close" data-inline="true" data-theme="c"/>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="mobile_user_label_area_grn">
         <span class="mobile-label-grn">Users</span>
         <a class="add_all_to_selected_list mobile-label-grn" href="#">Select all</a>
      </div>
      <div class="mobile_user_list_scroll_grn mobile-user-list-none-grn">
         <div class="mobile_list_shadow_grn"></div>
         <div class="mobile_icon_grn">
            <div class="mobile_icon_up_grn"></div>
            <div class="mobile_icon_down_grn"></div>
         </div>
         <ul data-role="listview" data-theme="c" class="source_items mobile-ul-top-grn mobile-ul-bottom-grn mobile_user_list_grn">
            <li data-icon="false" class="mobile_base_disable_grn"></li>
         </ul>
      </div>
      <!--mobile_user_list_scroll_grn-->
      <div class="mobile_user_label_area_grn">
         <span class="mobile-label-grn">Selected</span>
         <a class="remove_all_from_selected_list mobile-label-grn" href="#" style="display:none;">Clear selection</a>
      </div>
      <div class="mobile_user_list_scroll_grn">
         <div class="mobile_list_shadow_grn"></div>
         <div class="order_selected_list mobile_order_icon_grn">
            <div class="mobile_order_control_grn order_top">
               <div class="mobile_order_top_grn"></div>
            </div>
            <div class="mobile_order_control_grn order_up">
               <div class="mobile_order_up_grn"></div>
            </div>
            <div class="mobile_order_control_grn order_down">
               <div class="mobile_order_down_grn"></div>
            </div>
            <div class="mobile_order_control_grn order_bottom">
               <div class="mobile_order_bottom_grn"></div>
            </div>
         </div>
         <ul data-role="listview" data-theme="c" class="selected_items mobile-ul-top-grn mobile-ul-bottom-grn mobile_user_list_grn">
         </ul>
      </div>
      <!--mobile_user_list_scroll_grn-->
      <div data-theme="c" class="mobile_button_area_grn">
         <div class="mobile_ok_grn">
            <input type="submit" value="Apply" data-inline="true" data-theme="c" />
         </div>
         <div class="mobile_cancel_grn">
            <input type="reset" value="Cancel" data-inline="true" data-theme="c"/>
         </div>
      </div>
      <input type="hidden" id="selected_groups_sUID" name="selected_groups_sUID[]" value="">
      <input type="hidden" id="selected_users_sUID" name="selected_users_sUID" value="">
   </div>
</div>
<link href="{{asset('assets/mobile/css/mobile_groupselect.css')}}" rel="stylesheet" type="text/css">
<script src="{{asset('assets/mobile/js/mobile_group_select.js')}}" type="text/javascript"></script>
<div data-role="page" id="_popup_group_categories_tree_sUID">
   <div data-role="content" data-theme="c" class="mobile-content-withList-grn">
      <div class="mobile_breadcrumbtitle_grn mobile_titlebar_content_withList_grn">
         <div class="mobile_breadcrumbtitle_left_grn"><a href="#" onclick="javascript:location.href = '#_sUID';">Attendees</a><span class="mobile_icon_breadcrumb_arrow_grn"></span></div>
         <div class="mobile_breadcrumbtitle_right_grn"><span class="mobile_icon_breadcrumb_grn mobile_app_schedule_s_b_grn"></span>Select organizations</div>
      </div>
      <ul data-role="listview" data-theme="c" class="mobile-ul-top-grn mobile-ul-bottom-grn mobile_folderlist_grn " id="group_categories_tree_sUID">
         <li id="sUIDparent_child_g_g1" data-icon="false" style="padding-left:0px !important;">
            <div id="sUIDg1" style="margin-left:0px !important;" class="mobile_folderlist_icon_arrowclose_grn mobile_folderlist_icon_grn mobile_folderlist_icon_size_grn"></div>
            <a href="#" class="mobile_folderlist_list_text_grn"><span id="sUIDdeepth_0_1" class="mobile_folderlist_text_overflow_grn">さいど株式会社</span></a>
            <div><span class="mobile_groupselect_icon_radiobuttonoff_grn"></span><span class="mobile_groupselect_radiobutton_grn"><input name="user_group_select" type="radio" value="g1"></span></div>
         </li>
         <li id="sUIDparent_child_g_g18" data-icon="false" style="padding-left:0px !important;">
            <a href="#" class="mobile_folderlist_list_text_grn"><span id="sUIDdeepth_0_18" class="mobile_folderlist_text_overflow_grn">Cyde America</span></a>
            <div><span class="mobile_groupselect_icon_radiobuttonoff_grn"></span><span class="mobile_groupselect_radiobutton_grn"><input name="user_group_select" type="radio" value="g18"></span></div>
         </li>
         <li id="sUIDparent_child_g_g21" data-icon="false" style="padding-left:0px !important;">
            <a href="#" class="mobile_folderlist_list_text_grn"><span id="sUIDdeepth_0_21" class="mobile_folderlist_text_overflow_grn">CydeChina</span></a>
            <div><span class="mobile_groupselect_icon_radiobuttonoff_grn"></span><span class="mobile_groupselect_radiobutton_grn"><input name="user_group_select" type="radio" value="g21"></span></div>
         </li>
         <li id="sUIDparent_child__-2" data-icon="false" style="padding-left:0px !important;">
            <a href="#" class="mobile_folderlist_list_text_grn"><span id="sUIDdeepth_0_-2" class="mobile_folderlist_text_overflow_grn">(Unassigned users)</span></a>
            <div><span class="mobile_groupselect_icon_radiobuttonoff_grn"></span><span class="mobile_groupselect_radiobutton_grn"><input name="user_group_select" type="radio" value="-2"></span></div>
         </li>
      </ul>
   </div>
   <!-- end of content-->
   <div class="mobile_button_area_fixed_grn">
      <div data-position="fixed" data-theme="c" data-tap-toggle="false" class="mobile_button_area_grn">
         <div class="mobile_ok_grn">
            <input type="submit" value="Apply" data-inline="true" data-theme="c"/>
         </div>
         <div class="mobile_cancel_grn">
            <input class="cancel_button" type="reset" value="Cancel" data-inline="true" data-theme="c"/>
         </div>
      </div>
   </div>
</div>
<script language="JavaScript" text="text/javascript">
   (function(){
   
       
       var settings = {
         asyncUrl      : '/api/ajax_get_sub_group_for_mobile?',
         paramName     : 'oid',
         treeName      : 'group_categories_tree_sUID',
         pageName      : 'schedule/mobile/modify/user',
         previousPage  : '_sUID',
         container     : '_popup_group_categories_tree_sUID',
         radio_name    : 'user_group_select',
         prefix_id     : 'sUID',
         prefix_before_value : 'g',
         associate_value_element_back     : 'category_options_sUID',
         associate_title_element_back     : 'category_title_sUID'
       };
       
   
       $(document).ready(function(){
           var G = new grn.component.mobile_group_select(settings);
       });
   })();
</script>
<script src="{{asset('/js/resource-en.js')}}"></script>
<script language="JavaScript" text="text/javascript">
   (function(){
   
       
       var settings = {
           element_id  : 'p_sUID',
           ajaxGetItemURL : "/api/ajax_get_user_for_mobile_selection?",
           ajaxSearchItemURL : "/api/ajax_get_user_for_mobile_selection?",
           args        : "access_plugin=YToyOntzOjQ6Im5hbWUiO3M6ODoic2NoZWR1bGUiO3M6NjoicGFyYW1zIjthOjI6e3M6NjoiYWN0aW9uIjthOjE6e2k6MDtzOjQ6InJlYWQiO31zOjEyOiJzZXNzaW9uX25hbWUiO3M6Mjc6InNjaGVkdWxlL21vYmlsZS9tb2RpZnkvdmlldyI7fX0=&plugin_session_name=schedule/mobile/modify&app_id=schedule",
           include_org : '1',
           isCalendar : '',
           show_group_role : '1',
           isAllowedRole : '',
           src_item    : '#src_user',
           dest_item   : '#dest_user',
           type_item   : 'user',
           empty_list  : "",
           none_selected : "None",
           search_caption: "(Search results)",
           group_select_menu_id : 'category_options_p_sUID',
           associate_id : 'default_public',
           selectedItem : {"list":{}},
           page_name : '',
           default_title_group_select : 'Select group.',
           categorySelectUI : 'user_category_p_sUID',
           categorySelectValue : 'category_title_p_sUID',
           popupItemCategory : 'popup_user_category_p_sUID'
       };
       
   
       $(document).ready(function(){
           var G = new grn.component.mobile_user_select(settings);
           G.initSelectedItem();
   
           $(document).on("pagebeforeshow","#_p_sUID",function(event, data){
               G.bindEventBeforePageShow();
   
               if(typeof data.prevPage.attr('id') == 'undefined')
               {
                   G.processing = false;
                   G.saveLastState();
                   G.resetSourceItemList();
                   G.resetCategorySelect();
               }
           });
   
           $(document).on("pagebeforehide","#_p_sUID",function(event, data){
               if(typeof data.nextPage.attr('id') == 'undefined' && !G.adding )
               {
                   G.cancel();
               }
               G.adding = false;
           });
       });
   
   })();
</script>
<script id="src_user" type="text/x-template">
   <li data-icon="false" data-corners="false" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-iconpos="right" data-theme="c">
       <a class="selection_item ui-btn" href="#">
           <div class="mobile_add_grn"></div>
           <div class="mobile_user_photo_grn mobile_img_userPlofile_grn"></div>
           <div class="mobile_info_grn">
               <div class="mobile_position_center_grn">
                   <div class="mobile_position_width_grn">
                       <div class="mobile_user_grn"></div>
                       <div class="mobile_text_grn"></div>
                   </div>
               </div>
           </div>
       </a>
   </li>
</script>
<script id="dest_user" type="text/x-template">
   <li data-icon="false" class="mobile_selected_grn" data-corners="false" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-iconpos="right" data-theme="c">
       <div class="mobile_delete_grn"></div>
       <a class="selection_item ui-btn" href="#">
           <div class="mobile_user_photo_grn mobile_img_userPlofile_grn"></div>
           <div class="mobile_info_grn">
               <div class="mobile_position_center_grn">
                   <div class="mobile_position_width_grn">
                       <div class="mobile_user_grn"></div>
                       <div class="mobile_text_grn"></div>
                   </div>
               </div>
           </div>
           <span></span>
       </a>
   </li>
</script>
<div data-role="page" id="_p_sUID">
   <div data-role="content" data-theme="c" class="mobile-content-grn">
      <div class="mobile_breadcrumbtitle_grn mobile_titlebar_content_withList_grn">
         <div class="mobile_breadcrumbtitle_left_grn"><a href="#" >Edit appointment</a><span class="mobile_icon_breadcrumb_arrow_grn"></span></div>
         <div class="mobile_breadcrumbtitle_right_grn"><span class="mobile_icon_breadcrumb_grn mobile_app_schedule_s_b_grn"></span>Shared</div>
      </div>
      <div class="mobile_seletedUser_list_grn">
         <form action="#">
            <div class="mobile_user_search_grn">
               <input id="search_user" class="search_item" type="search" name="search_user_p_sUID" placeholder="User search"/>
               <a href="#" class="mobile_delete_icon_grn delete_input"></a>
               <a href="#" class="mobile_icon_grn search_item_icon"></a>
            </div>
         </form>
      </div>
      <div class="mobile_user_group_grn">
         <a id="user_category_p_sUID" href="#popup_user_category_p_sUID" data-rel="popup" data-transition="pop" data-shadow="true" data-wrapperels="span" class="mobile_select_view_grn">
         <span class="mobile_event_menu_content_grn">Select group.</span>
         <span class="mobile_select_icon_grn"></span>
         </a>
         <a class="ui-link-inherit" href="#_popup_group_categories_tree_p_sUID">
            <div class="mobile_group_icon_grn"></div>
         </a>
         <input type="hidden" value="" id="category_options_p_sUID">
         <input type="hidden" value="" id="category_title_p_sUID">
         <script language="JavaScript" text="text/javascript">
            (function(){
            
            
                var settings = {
                    selectMenu       : '#user_category_p_sUID',
                    selectMenuValue  : '#category_options_p_sUID',
                    container        : '#popup_user_category_p_sUID',
                    selectedValue    : 'Select group.',
                    expandMenuSelect : '#category_title_p_sUID',
                    selectMenuTitle  : 'Select group',
                    useMenuColor     : '',
                    defaultTitle     : '',
                    name             : 'mobileSelectMenu_popup_user_category_p_sUID'
                };
            
            
                $(document).ready(function(){
                    var G = new grn.component.mobile_select_menu(settings);
                    G.initSize =false;
            
                    $(document).on("popupbeforeposition","#popup_user_category_p_sUID",function(event){
                        if(!G.initSize || G.needResize)
                        {
                            G.setSizeSelectMenu(event);
                            G.initSize = true;
                        }
                    });
            
                    $('#popup_user_category_p_sUID').on('click',function(event){
                        G.setSizeSelectMenu(event);
                    });
            
                    $( window ).on( "resize", function( event ) {
                        var popup = $('#popup_user_category_p_sUID');
                        if(popup.is(':visible'))
                        {
                            G.setSizeSelectMenu(event);
                        }
                    });
                });
            
            })();
         </script>
         <div data-role="popup" id="popup_user_category_p_sUID" data-corners="false" data-overlay-theme="b" data-shadow="false">
            <div class="mobile_select_menu_titlebar_div_grn">
               <div class="mobile_titlebar_grn">
                  <span class="mobile_text_grn">Select group</span>
                  <a href="#" class="mobile_titlebar_right_grn mobile_colse_icon_grn"></a>
               </div>
               <ul data-role="listview" data-theme="c" class="mobile-ul-top-grn mobile_ul_grn select_menu_list">
                  <li data-icon="false" class="mobile_list_grn ">
                     <span class="mobile_select_item "></span>
                     <a href="#" class="selection_item" data-value="g15">第1営業グループ(Priority organization) </a>
                  </li>
                  <li data-icon="false" class="mobile_list_grn ">
                     <span class="mobile_select_item "></span>
                     <a href="#" class="selection_item" data-value="g18">Cyde America(Membership)</a>
                  </li>
                  <li data-icon="false" class="mobile_list_grn ">
                     <span class="mobile_select_item "></span>
                     <a href="#" class="selection_item" data-value="frequent">(Recently selected users)</a>
                  </li>
                  <li data-icon="false" class="mobile_list_grn mobile_select_menu_last_item_grn">
                     <span class="mobile_select_item"></span>
                     <a href="#" data-value="">&nbsp</a>
                  </li>
               </ul>
               <div class="mobile_select_button_area_grn">
                  <div class="mobile_cancel_grn">
                     <input class="cancel_button" type="reset" value="Close" data-inline="true" data-theme="c"/>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="mobile_user_label_area_grn">
         <span class="mobile-label-grn">Users, organizations, roles</span>
         <a class="add_all_to_selected_list mobile-label-grn" href="#">Select all</a>
      </div>
      <div class="mobile_user_list_scroll_grn mobile-user-list-none-grn">
         <div class="mobile_list_shadow_grn"></div>
         <div class="mobile_icon_grn">
            <div class="mobile_icon_up_grn"></div>
            <div class="mobile_icon_down_grn"></div>
         </div>
         <ul data-role="listview" data-theme="c" class="source_items mobile-ul-top-grn mobile-ul-bottom-grn mobile_user_list_grn">
            <li data-icon="false" class="mobile_base_disable_grn"></li>
         </ul>
      </div>
      <!--mobile_user_list_scroll_grn-->
      <div class="mobile_user_label_area_grn">
         <span class="mobile-label-grn">Selected</span>
         <a class="remove_all_from_selected_list mobile-label-grn" href="#" style="display:none;">Clear selection</a>
      </div>
      <div class="mobile_user_list_scroll_grn">
         <div class="mobile_list_shadow_grn"></div>
         <div class="order_selected_list mobile_order_icon_grn">
            <div class="mobile_order_control_grn order_top">
               <div class="mobile_order_top_grn"></div>
            </div>
            <div class="mobile_order_control_grn order_up">
               <div class="mobile_order_up_grn"></div>
            </div>
            <div class="mobile_order_control_grn order_down">
               <div class="mobile_order_down_grn"></div>
            </div>
            <div class="mobile_order_control_grn order_bottom">
               <div class="mobile_order_bottom_grn"></div>
            </div>
         </div>
         <ul data-role="listview" data-theme="c" class="selected_items mobile-ul-top-grn mobile-ul-bottom-grn mobile_user_list_grn">
         </ul>
      </div>
      <!--mobile_user_list_scroll_grn-->
      <div data-theme="c" class="mobile_button_area_grn">
         <div class="mobile_ok_grn">
            <input type="submit" value="Apply" data-inline="true" data-theme="c" />
         </div>
         <div class="mobile_cancel_grn">
            <input type="reset" value="Cancel" data-inline="true" data-theme="c"/>
         </div>
      </div>
      <input type="hidden" id="selected_groups_p_sUID" name="selected_groups_p_sUID[]" value="">
      <input type="hidden" id="selected_users_p_sUID" name="selected_users_p_sUID" value="">
   </div>
</div>
<div data-role="page" id="_popup_group_categories_tree_p_sUID">
   <div data-role="content" data-theme="c" class="mobile-content-withList-grn">
      <div class="mobile_breadcrumbtitle_grn mobile_titlebar_content_withList_grn">
         <div class="mobile_breadcrumbtitle_left_grn"><a href="#" onclick="javascript:location.href = '#_p_sUID';">Shared</a><span class="mobile_icon_breadcrumb_arrow_grn"></span></div>
         <div class="mobile_breadcrumbtitle_right_grn"><span class="mobile_icon_breadcrumb_grn mobile_app_schedule_s_b_grn"></span>Select organizations</div>
      </div>
      <ul data-role="listview" data-theme="c" class="mobile-ul-top-grn mobile-ul-bottom-grn mobile_folderlist_grn " id="group_categories_tree_p_sUID">
         <li id="p-sUIDparent_child_g_g1" data-icon="false" style="padding-left:0px !important;">
            <div id="p-sUIDg1" style="margin-left:0px !important;" class="mobile_folderlist_icon_arrowclose_grn mobile_folderlist_icon_grn mobile_folderlist_icon_size_grn"></div>
            <a href="#" class="mobile_folderlist_list_text_grn"><span id="p-sUIDdeepth_0_1" class="mobile_folderlist_text_overflow_grn">さいど株式会社</span></a>
            <div><span class="mobile_groupselect_icon_radiobuttonoff_grn"></span><span class="mobile_groupselect_radiobutton_grn"><input name="user_group_select" type="radio" value="g1"></span></div>
         </li>
         <li id="p-sUIDparent_child_g_g18" data-icon="false" style="padding-left:0px !important;">
            <a href="#" class="mobile_folderlist_list_text_grn"><span id="p-sUIDdeepth_0_18" class="mobile_folderlist_text_overflow_grn">Cyde America</span></a>
            <div><span class="mobile_groupselect_icon_radiobuttonoff_grn"></span><span class="mobile_groupselect_radiobutton_grn"><input name="user_group_select" type="radio" value="g18"></span></div>
         </li>
         <li id="p-sUIDparent_child_g_g21" data-icon="false" style="padding-left:0px !important;">
            <a href="#" class="mobile_folderlist_list_text_grn"><span id="p-sUIDdeepth_0_21" class="mobile_folderlist_text_overflow_grn">CydeChina</span></a>
            <div><span class="mobile_groupselect_icon_radiobuttonoff_grn"></span><span class="mobile_groupselect_radiobutton_grn"><input name="user_group_select" type="radio" value="g21"></span></div>
         </li>
         <li id="p-sUIDparent_child__-2" data-icon="false" style="padding-left:0px !important;">
            <a href="#" class="mobile_folderlist_list_text_grn"><span id="p-sUIDdeepth_0_-2" class="mobile_folderlist_text_overflow_grn">(Unassigned users)</span></a>
            <div><span class="mobile_groupselect_icon_radiobuttonoff_grn"></span><span class="mobile_groupselect_radiobutton_grn"><input name="user_group_select" type="radio" value="-2"></span></div>
         </li>
      </ul>
   </div>
   <!-- end of content-->
   <div class="mobile_button_area_fixed_grn">
      <div data-position="fixed" data-theme="c" data-tap-toggle="false" class="mobile_button_area_grn">
         <div class="mobile_ok_grn">
            <input type="submit" value="Apply" data-inline="true" data-theme="c"/>
         </div>
         <div class="mobile_cancel_grn">
            <input class="cancel_button" type="reset" value="Cancel" data-inline="true" data-theme="c"/>
         </div>
      </div>
   </div>
</div>
<script language="JavaScript" text="text/javascript">
   (function(){
   
       
       var settings = {
         asyncUrl      : '/api/ajax_get_sub_group_for_mobile?',
         paramName     : 'oid',
         treeName      : 'group_categories_tree_p_sUID',
         pageName      : 'schedule/mobile/modify/user',
         previousPage  : '_p_sUID',
         container     : '_popup_group_categories_tree_p_sUID',
         radio_name    : 'user_group_select',
         prefix_id     : 'p-sUID',
         prefix_before_value : 'g',
         associate_value_element_back     : 'category_options_p_sUID',
         associate_title_element_back     : 'category_title_p_sUID'
       };
       
   
       $(document).ready(function(){
           var G = new grn.component.mobile_group_select(settings);
       });
   })();
</script>
<script src="{{asset('assets/mobile/js/mobile_facility_select.js')}}"></script>
<script src="{{asset('/js/resource-en.js')}}"></script>
<script language="JavaScript" text="text/javascript">
   (function(){
   
       
       var settings = {
           element_id  : 'sITEM',
           ajaxGetItemURL : "/api/accessible_facility?",
           ajaxSearchItemURL : "/api/search_facility?",
           src_item    : '#src_facility',
           dest_item   : '#dest_facility',
           type_item   : 'facility',
           empty_list  : "",
           none_selected : "None",
           search_caption: "(Search results)",
           group_select_menu_id : 'facility_category_options_sITEM',
           associate_id : 'facilities',
           selectedItem : {!!$list_facility_selected!!},
           page_name : 'schedule/mobile/modify',
           default_title_group_select : 'Select group.',
           categorySelectUI : 'facility_category_sITEM',
           categorySelectValue : 'facility_category_title_sITEM',
           popupItemCategory : 'popup_facility_category_sITEM',
           defaultUsingPurpose : "",
           keepUsingPurpose : "1",
       };
   
   
       $(document).ready(function(){
           var G = new grn.component.mobile_facility_select(settings);
           G.initSelectedItem();
           G.toggleUsingPurposeElement();
   
           $(document).on("pagebeforeshow","#_sITEM",function(event, data){
               G.bindEventBeforePageShow();
   
               if(typeof data.prevPage.attr('id') == 'undefined')
               {
                   G.processing = false;
                   G.saveLastState();
                   G.resetSourceItemList();
                   G.resetCategorySelect();
               }
           });
   
           $(document).on("pagebeforehide","#_sITEM",function(event, data){
               G.toggleUsingPurposeElement();
               if(typeof data.nextPage.attr('id') == 'undefined' && !G.adding )
               {
                   G.cancel();
               }
               G.adding = false;
           });
       });
   
   })();
</script>
<script id="src_facility" type="text/x-template">
   <li data-icon="false" data-corners="false" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-iconpos="right" data-theme="c">
       <a class="selection_item ui-btn" href="#">
           <div class="mobile_add_grn"></div>
           <div class="mobile_user_photo_grn mobile_img_facilityPlofile_grn"></div>
           <div class="mobile_info_grn">
               <div class="mobile_position_center_grn">
                   <div class="mobile_position_width_grn">
                       <div class="mobile_user_grn"></div>
                       <div class="mobile_text_grn"></div>
                   </div>
               </div>
           </div>
       </a>
   </li>
</script>
<script id="dest_facility" type="text/x-template">
   <li data-icon="false" class="mobile_selected_grn" data-corners="false" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-iconpos="right" data-theme="c">
       <div class="mobile_delete_grn"></div>
       <a class="selection_item ui-btn" href="#">
           <div class="mobile_user_photo_grn mobile_img_facilityPlofile_grn"></div>
           <div class="mobile_info_grn">
               <div class="mobile_position_center_grn">
                   <div class="mobile_position_width_grn">
                       <div class="mobile_user_grn"></div>
                       <div class="mobile_text_grn"></div>
                   </div>
               </div>
           </div>
           <span></span>
       </a>
   </li>
</script>
<div data-role="page" id="_sITEM">
   <div data-role="content" data-theme="c" class="mobile-content-grn">
      <div class="mobile_breadcrumbtitle_grn mobile_titlebar_content_withList_grn">
         <div class="mobile_breadcrumbtitle_left_grn"><a href="#" >Edit appointment</a><span class="mobile_icon_breadcrumb_arrow_grn"></span></div>
         <div class="mobile_breadcrumbtitle_right_grn">Facilities</div>
      </div>
      <div class="mobile_seletedUser_list_grn">
         <form action="#">
            <div class="mobile_user_search_grn">
               <input id="search_facility" class="search_item" type="search" name="search_facility" placeholder="Facility search"/>
               <a href="" class="mobile_delete_icon_grn delete_input"></a>
               <a href="#" class="mobile_icon_grn search_item_icon"></a>
            </div>
         </form>
      </div>
      <div class="mobile_user_group_grn">
         <a id="facility_category_sITEM" href="#popup_facility_category_sITEM" data-rel="popup" data-transition="pop" data-shadow="true" data-wrapperels="span" class="mobile_select_view_grn">
         <span class="mobile_event_menu_content_grn">Select group.</span>
         <span class="mobile_select_icon_grn"></span>
         </a>
         <a class="ui-link-inherit" href="#_popup_facility_categories_tree">
            <div class="mobile_group_icon_grn"></div>
         </a>
         <input type="hidden" value="" id="facility_category_options_sITEM">
         <input type="hidden" value="" id="facility_category_title_sITEM">
         <script language="JavaScript" text="text/javascript">
            (function(){
            
            
                var settings = {
                    selectMenu       : '#facility_category_sITEM',
                    selectMenuValue  : '#facility_category_options_sITEM',
                    container        : '#popup_facility_category_sITEM',
                    selectedValue    : 'Select group.',
                    expandMenuSelect : '#facility_category_title_sITEM',
                    selectMenuTitle  : 'Select group',
                    useMenuColor     : '',
                    defaultTitle     : '',
                    name             : 'mobileSelectMenu_popup_facility_category_sITEM'
                };
            
            
                $(document).ready(function(){
                    var G = new grn.component.mobile_select_menu(settings);
                    G.initSize =false;
            
                    $(document).on("popupbeforeposition","#popup_facility_category_sITEM",function(event){
                        if(!G.initSize || G.needResize)
                        {
                            G.setSizeSelectMenu(event);
                            G.initSize = true;
                        }
                    });
            
                    $('#popup_facility_category_sITEM').on('click',function(event){
                        G.setSizeSelectMenu(event);
                    });
            
                    $( window ).on( "resize", function( event ) {
                        var popup = $('#popup_facility_category_sITEM');
                        if(popup.is(':visible'))
                        {
                            G.setSizeSelectMenu(event);
                        }
                    });
                });
            
            })();
         </script>
         <div data-role="popup" id="popup_facility_category_sITEM" data-corners="false" data-overlay-theme="b" data-shadow="false">
            <div class="mobile_select_menu_titlebar_div_grn">
               <div class="mobile_titlebar_grn">
                  <span class="mobile_text_grn">Select group</span>
                  <a href="#" class="mobile_titlebar_right_grn mobile_colse_icon_grn"></a>
               </div>
               <ul data-role="listview" data-theme="c" class="mobile-ul-top-grn mobile_ul_grn select_menu_list">
                  @foreach($equipments as $key=>$val)
                  <li data-icon="false" class="mobile_list_grn ">
                     <span class="mobile_select_item "></span>
                     <a href="#" class="selection_item" data-value="{{$val->id}}">{{$val->name}}</a>
                  </li>
                  @endforeach
               </ul>
               <div class="mobile_select_button_area_grn">
                  <div class="mobile_cancel_grn">
                     <input class="cancel_button" type="reset" value="Close" data-inline="true" data-theme="c"/>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="mobile_user_label_area_grn">
         <span class="mobile-label-grn">Facilities</span>
         <a class="add_all_to_selected_list mobile-label-grn" href="#">Select all</a>
      </div>
      <div class="mobile_user_list_scroll_grn mobile-user-list-none-grn">
         <div class="mobile_list_shadow_grn"></div>
         <div class="mobile_icon_grn">
            <div class="mobile_icon_up_grn"></div>
            <div class="mobile_icon_down_grn"></div>
         </div>
         <ul data-role="listview" data-theme="c" class="source_items mobile-ul-top-grn mobile-ul-bottom-grn mobile_user_list_grn">
            <li data-icon="false" class="mobile_base_disable_grn"></li>
         </ul>
      </div>
      <!--mobile_user_list_scroll_grn-->
      <div class="mobile_user_label_area_grn">
         <span class="mobile-label-grn">Selected facilities</span>
         <a class="remove_all_from_selected_list mobile-label-grn" href="#" style="display:none;">Clear selection</a>
      </div>
      <div class="mobile_user_list_scroll_grn">
         <div class="mobile_list_shadow_grn"></div>
         <div class="order_selected_list mobile_order_icon_grn">
            <div class="mobile_order_control_grn order_top">
               <div class="mobile_order_top_grn"></div>
            </div>
            <div class="mobile_order_control_grn order_up">
               <div class="mobile_order_up_grn"></div>
            </div>
            <div class="mobile_order_control_grn order_down">
               <div class="mobile_order_down_grn"></div>
            </div>
            <div class="mobile_order_control_grn order_bottom">
               <div class="mobile_order_bottom_grn"></div>
            </div>
         </div>
         <ul data-role="listview" data-theme="c" class="selected_items mobile-ul-top-grn mobile-ul-bottom-grn mobile_user_list_grn">
         </ul>
      </div>
      <!--mobile_user_list_scroll_grn-->
      <div data-theme="c" class="mobile_button_area_grn">
         <div class="mobile_ok_grn">
            <input type="submit" value="Apply" data-inline="true" data-theme="c" />
         </div>
         <div class="mobile_cancel_grn">
            <input type="reset" value="Cancel" data-inline="true" data-theme="c" data-disabled="false"/>
         </div>
      </div>
      <input type="hidden" id="sITEM" name="sITEM[]" value="">
      <input type="hidden" id="checkrepeat" name="checkrepeat" value="">
      <input type="hidden" id="approval" name="approval" value="">
   </div>
</div>
<div data-role="page" id="_popup_facility_categories_tree">
   <div data-role="content" data-theme="c" class="mobile-content-withList-grn">
      <div class="mobile_breadcrumbtitle_grn mobile_titlebar_content_withList_grn">
         <div class="mobile_breadcrumbtitle_left_grn"><a href="#" onclick="javascript:location.href = '#_sITEM';">Facilities</a><span class="mobile_icon_breadcrumb_arrow_grn"></span></div>
         <div class="mobile_breadcrumbtitle_right_grn">Select facility groups</div>
      </div>
      <ul data-role="listview" data-theme="c" class="mobile-ul-top-grn mobile-ul-bottom-grn mobile_folderlist_grn " id="facility_categories_tree">
         @foreach($equipments as $key=>$val)
         <li id="parent_child__1" data-icon="false" style="padding-left:0px !important;">
            <div id="1" style="margin-left:0px !important;" class="mobile_folderlist_icon_arrowclose_grn mobile_folderlist_icon_grn mobile_folderlist_icon_size_grn"></div>
            <a href="#" class="mobile_folderlist_list_text_grn"><span id="deepth_0_1" class="mobile_folderlist_text_overflow_grn">{{$val->name}}</span></a>
            <div><span class="mobile_groupselect_icon_radiobuttonoff_grn"></span><span class="mobile_groupselect_radiobutton_grn"><input name="facility_group_select" type="radio" value="{{$val->id}}"></span></div>
         </li>
         @endforeach
      </ul>
   </div>
   <!-- end of content-->
   <div class="mobile_button_area_fixed_grn">
      <div data-position="fixed" data-theme="c" data-tap-toggle="false" class="mobile_button_area_grn">
         <div class="mobile_ok_grn">
            <input type="submit" value="Apply" data-inline="true" data-theme="c"/>
         </div>
         <div class="mobile_cancel_grn">
            <input class="cancel_button" type="reset" value="Cancel" data-inline="true" data-theme="c"/>
         </div>
      </div>
   </div>
</div>
<script language="JavaScript" text="text/javascript">
   (function(){
   
       
       var settings = {
         asyncUrl      : '/scripts/garoon/grn.exe/schedule/mobile/ajax/get_facility_sub_group?',
         paramName     : 'oid',
         treeName      : 'facility_categories_tree',
         pageName      : 'schedule/mobile/modify/facility',
         previousPage  : '_sITEM',
         container     : '_popup_facility_categories_tree',
         radio_name    : 'facility_group_select',
         prefix_id     : '',
         prefix_before_value : '',
         associate_value_element_back     : 'facility_category_options_sITEM',
         associate_title_element_back     : 'facility_category_title_sITEM'
       };
       
   
       $(document).ready(function(){
           var G = new grn.component.mobile_group_select(settings);
       });
   })();
</script>
</div>
@stop