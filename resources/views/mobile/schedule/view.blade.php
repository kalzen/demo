@extends('mobile.layouts.mobile_schedule')
@section('content')
<div class="mobile_spinner_grn" style="display:none;"><span></span></div>
<div data-role="page" data-url="/scripts/garoon/grn.exe/schedule/mobile/view?event=1047&amp;bdate=2021-01-23&amp;uid=58&amp;referer_key=dc6b970f1bccf33a08d707aa702417e9" tabindex="0" class="ui-page ui-page-theme-a ui-page-footer-fixed ui-page-active" style="padding-bottom: 51px;">
   @include('mobile/schedule/cpanel')
   <div style="display: none;" id="member_list_dialog-placeholder">
      <!-- placeholder for member_list_dialog -->
   </div>
   <div id="positionPopup" style="position:fixed;top:0px;display=none"></div>
   <script language="JavaScript" text="text/javascript">
      (function(){
          var GS = grn.component.mobile_common;
          GS.comment_add_url = '/scripts/garoon/grn.exe/schedule/mobile/comment_add?';
          GS.add_comment_args = {'event' : '1047', 'bdate' : '2021-01-23', 'uid' : '58', 'gid' : '', 'referer_key' : 'dc6b970f1bccf33a08d707aa702417e9'};
      
      })();
   </script>
   <!--GTM-1684-->
   <script src="{{asset('/assets/mobile/js/view.js')}}" type="text/javascript"></script>
   <div class="ui-panel-wrapper">
      <div data-role="content" data-theme="c" class="mobile-content-padding-grn ui-content ui-body-c" role="main">
         <!-- breadcrumb -->
         <script src="{!!asset('assets/mobile/js/mobile_breadcrumb.js')!!}" type="text/javascript"></script>
         <div class="mobile_breadcrumbtitle_grn ">
            <div class="mobile_breadcrumbtitle_left_grn " id="left_text" style="max-width: 124.2px;"><span class="mobile_icon_breadcrumb_grn mobile_app_schedule_s_b_grn"></span><a href="{{route('frontend.schedule.index')}}" class="ui-link">Schedule</a><span class="mobile_icon_breadcrumb_arrow_grn"></span></div>
            <div class="mobile_breadcrumbtitle_right_grn" id="right_text" style="width: 285px;">Details</div>
         </div>
         <!-- GTM-1630 attendance confirmation -->
         <!-- End GTM-1630 attendance confirmation -->
         <!-- title -->
         <ul data-role="none" class="mobile-ul-withStar-grn mobile-textspace-withStar-grn mobile-detailTitle-grn">
            <li>
               <script src="{{asset('/assets/mobile/js/star_mobile.js')}}" type="text/javascript"></script>
               <script language="javascript" text="text/javascript">
                  var options = {url: '/scripts/garoon/grn.exe/star/ajax_request?',
                                 csrf_ticket: 'c3b4eff85633d977030fb1f2aeeb559b'};
                  var obj_star_mobile = new grn.component.star_mobile.StarMobile();
                  obj_star_mobile.initialize(options);
               </script>                <a class="mobile-icon-starOff-grn ui-link" id="grn.schedule:event_1047:bdate_2021-01-23" onclick="obj_star_mobile.onClick(this);"></a>
               <span>
               {!!$schedule->title!!}
               </span>
            </li>
         </ul>
         <!-- content -->
         <div class="mobile-list-login-grn mobile-cmt-operate-grn mobile-todoBodyText-grn">
            <div class="mobile-separation-grn"></div>
            <!-- datetime -->
            <!-- banner -->
            @if($schedule->pattern == 1)
            <div class="mobile-list-todoDetail-grn">
               <div class="mobile-label-small-grn">Date and time</div>
               <div class="mobile_contents_detail_grn">
                  {!!date('D, F d, Y',strtotime($schedule->start_date))!!}<br>{!!date('h:i A',strtotime($schedule->start_date))!!} &nbsp;<span> - </span>&nbsp;{!!date('h:i A',strtotime($schedule->end_date))!!}
               </div>
            </div>
            @else
            <div class="mobile-list-todoDetail-grn">
                <div class="mobile-label-small-grn">Period</div>
                <div class="mobile_contents_detail_grn">
                    <span class="mobile_detail_datetime_grn">{!!date('D, F d, Y',strtotime($schedule->start_date))!!}&nbsp;<span> - </span></span><br>{!!date('D, F d, Y',strtotime($schedule->end_date))!!}
                </div>
            </div>
            @endif
            
            @if($schedule->pattern != 2)
            <ul data-role="listview" data-theme="c" class="mobile-li-PersonInCharge-grn mobile-div-title-grn mobile_facility_process_noHistory_grn ui-listview ui-group-theme-c">
               <div class="mobile-separation-grn"></div>
               <li data-icon="false" class="ui-first-child ui-last-child">
                  <a href="/scripts/garoon/grn.exe/schedule/mobile/facility_list?event=1047&amp;bdate=2021-01-23&amp;uid=58&amp;gid=&amp;referer_key=dc6b970f1bccf33a08d707aa702417e9" class="ui-btn">
                     <div class="mobile-label-small-grn">Facilities</div>
                     <div class="mobile_for_totalnumber_grn">
                        @foreach($schedule->equipment as $key=>$val)
                        <div class="mobile-font-warp-grn">
                           {!!$val->name!!}
                           <span class="mobile-label-small-grn">
                           </span>
                        </div>
                        @endforeach
                     </div>
                     <!--mobile_for_totalnumber_grn-->
                     @if(count($schedule->equipment))
                     <span class="totalNumber-grn">{!!count($schedule->equipment)!!}</span>
                     @endif
                     <span class="mobile-array-todo-grn"></span>
                  </a>
               </li>
               <!--facilities approval history link-->
            </ul>
            @endif
            <ul data-role="listview" data-theme="c" class="mobile-li-PersonInCharge-grn mobile-div-title-grn ui-listview ui-group-theme-c">
               <div class="mobile-separation-grn"></div>
               <!-- End GTM-1684 -->
               <!-- GTM-1630 attendees -->
               <li data-icon="false" class="ui-first-child ui-last-child">
                  <a href="javascript:void(0)" class="ui-btn">
                     <div class="mobile-label-small-grn">Attendees</div>
                     @foreach($schedule->member as $key=>$val)
                     <div class="mobile_for_totalnumber_grn">
                        <div class="mobile-font-warp-grn">{!!$val->full_name!!}</div>
                     </div>
                     @endforeach
                     <!--mobile_for_totalnumber_grn-->
                     @if(count($schedule->member))
                     <span class="totalNumber-grn">{!!count($schedule->member)!!}</span>
                      @endif
                     
                  </a>
               </li>
               <div class="mobile-separation-grn"></div>
            </ul>
            <!-- notes -->
            <div class="mobile-list-todoDetail-grn">
               <div class="mobile-label-small-grn">Notes</div>
               <div class="mobile-contents-todoDetail-grn">
                  <pre style="white-space:-moz-pre-wrap; white-space:pre-wrap;display:inline;">123</pre>
               </div>
            </div>
            <div class="mobile-separation-grn"></div>
            <!-- files -->
            <div class="mobile-list-todoDetail-grn">
               <div class="mobile-label-small-grn">Attachments</div>
               <div class="mobile-contents-todoDetail-grn">
                  <div class="mobile-attachmentButton-grn">
                     
                  </div>
               </div>
            </div>
            <div class="mobile-separation-grn"></div>
            <!-- registrant  -->
            <div class="mobile-list-todoDetail-grn">
               <div class="mobile-label-small-grn">Registrant</div>
               <div class="mobile-contents-todoDetail-grn mobile_todo_username_grn">{!!$schedule->created_by->full_name!!}<span class="mobile-date-list-grn">{!!date('D , F d, Y h:i A',strtotime($schedule->created_at))!!}</span></div>
            </div>
            <!-- updater -->
            <div class="mobile-list-todoDetail-grn">
               <div class="mobile-label-small-grn">Updater</div>
               <div class="mobile-contents-todoDetail-grn mobile_todo_username_grn">@if(is_null($schedule->update_person)) {!!$schedule->created_by->full_name!!} @else {!!$schedule->updater->full_name!!} @endif<span class="mobile-date-list-grn">{!!date('D , F d, Y h:i A',strtotime($schedule->updated_at))!!}</span></div>
            </div>
         </div>
      </div>
      
   </div>
   <script>
      (function () {
          var settings = {
              
              accessPluginEncoded: "YToyOntzOjQ6Im5hbWUiO3M6ODoic2NoZWR1bGUiO3M6NjoicGFyYW1zIjthOjQ6e3M6NjoiYXBwX2lkIjtzOjg6InNjaGVkdWxlIjtzOjg6ImV2ZW50X2lkIjtzOjQ6IjEwNDciO3M6NjoiYWN0aW9uIjthOjE6e2k6MDtzOjQ6InJlYWQiO31zOjc6ImZlYXR1cmUiO3M6NzoibWVudGlvbiI7fX0=",
              
          };
          var dialog = new grn.js.component.common.ui.dialog.MobileMemberListDialog(settings);
          dialog.render();
          dialog.bindDialogToMembersContainer("#comment_list");
      })();
   </script>
   <!-- footer -->
   <!-- end of footer -->
   <div data-role="footer" data-position="fixed" data-tap-toggle="false" data-theme="c" id="mobile_footer" role="contentinfo" class="ui-footer ui-bar-c ui-footer-fixed slideup">
      <div data-role="navbar" class="mobile_navbar_grn ui-navbar" id="mobile_footer_bar" role="navigation">
         <ul class="ui-grid-d">
            <li class="mobile_navbar_a_grn ui-block-a">
               @if($schedule->pattern == 1)
               <a id="footer_bar_a" href="{{route('frontend.schedule.edit',$schedule->id)}}" class="mobile_navbar_icon_a_grn mobile_icon_tool_edit_grn ui-link ui-btn"></a>
               @elseif($schedule->pattern == 2)
               <a id="footer_bar_a" href="{{route('frontend.schedule.edit_all',$schedule->id)}}" class="mobile_navbar_icon_a_grn mobile_icon_tool_edit_grn ui-link ui-btn"></a>
               @elseif($schedule->pattern == 3)
               <a id="footer_bar_a" href="{{route('frontend.schedule.edit_repeat',$schedule->id)}}" class="mobile_navbar_icon_a_grn mobile_icon_tool_edit_grn ui-link ui-btn"></a>
               @endif
               <div class="mobile_bg_grn"></div>
            </li>
            <li class="mobile_navbar_b_grn ui-block-b">
               <a id="footer_bar_b" href="/scripts/garoon/grn.exe/schedule/mobile/comment_add?event=1047&amp;bdate=2021-01-23&amp;uid=58&amp;gid=&amp;referer_key=dc6b970f1bccf33a08d707aa702417e9" class="mobile_navbar_icon_b_grn mobile_icon_tool_comment_grn ui-link ui-btn"></a>
               <div class="mobile_bg_grn"></div>
            </li>
            <li class="mobile_navbar_c_grn ui-block-c">
               <a id="footer_bar_c" href="/scripts/garoon/grn.exe/notification/mobile/index?module_id=all" class="mobile_navbar_icon_c_grn mobile_icon_tool_notify_grn ui-link ui-btn">
                  <div class="mobile_nitify_sum_position_grn"><span class="mobile_notify_sum_grn" id="mobile_footer_bar_notify_num" mid="notification">1</span></div>
               </a>
               <div class="mobile_bg_grn"></div>
            </li>
            <li class="mobile_navbar_d_grn ui-block-d">
               <a id="footer_bar_d" href="javascript:void(0);" class="mobile_navbar_icon_d_grn mobile_icon_tool_menu_grn ui-link ui-btn">
                  <div class="mobile_triangle_d_grn"></div>
               </a>
               <div class="mobile_bg_grn"></div>
            </li>
            <li class="mobile_navbar_e_grn ui-block-e">
               <a id="footer_bar_e" href="#appmenu" class="mobile_navbar_icon_e_grn mobile_icon_tool_appmenu_grn ui-link ui-btn">
                  <div class="mobile_triangle_e_grn"></div>
               </a>
               <div class="mobile_bg_grn"></div>
            </li>
         </ul>
      </div>
      <!-- navbar --> 
      <div class="mobile_navbar_menu_grn" id="operate_menu">
         <ul class="mobile_scroll_area_grn">
             @if($schedule->pattern == 1)
            <li data-icon="false" data-theme="c" class="ui-btn ui-btn-up-c">
                <a href="{{route('frontend.schedule.create',['schedule_id'=>$schedule->id])}}" class="ui-link">Reuse</a>
            </li>
            @elseif($schedule->pattern == 2)
            <li data-icon="false" data-theme="c" class="ui-btn ui-btn-up-c">
                <a href="{{route('frontend.schedule.create_all_day',['schedule_id'=>$schedule->id])}}" class="ui-link">Reuse</a>
            </li>
            @elseif($schedule->pattern == 3)
            <li data-icon="false" data-theme="c" class="ui-btn ui-btn-up-c">
                <a href="{{route('frontend.schedule.create_repeat',['schedule_id'=>$schedule->id])}}" class="ui-link">Reuse</a>
            </li>
            @endif
            @if(in_array(\Auth::guard('member')->user()->id,$schedule->member()->pluck('id')->toArray()))
            <li data-icon="false" data-theme="c" class="ui-btn ui-btn-up-c"><a href="/schedule/leave/{{$schedule->id}}" class="ui-link">Leave</a></li>
            @else
            <li data-icon="false" data-theme="c" class="ui-btn ui-btn-up-c"><a href="/schedule/participate/{{$schedule->id}}" class="ui-link">Attend</a></li>
            @endif
            <li data-icon="false" data-theme="c" class="ui-btn ui-btn-up-c"><a href="{{route('frontend.schedule.destroy',$schedule->id)}}" class="ui-link">Delete</a></li>
         </ul>
         <ul>
            <li class="mobile_refresh_grn"><a href="#" onclick="javascript:grn.component.mobile_appmenu.mobile_reload();" class="ui-link"><span></span>Refresh</a></li>
         </ul>
      </div>
      <!--navbar menu-->
   </div>
   <div class="ui-screen-hidden ui-popup-screen ui-overlay-b" id="member_list_dialog-screen"></div>
   <div class="ui-popup-container ui-popup-hidden ui-popup-truncate" id="member_list_dialog-popup">
      <div data-role="popup" id="member_list_dialog" data-position-to="#positionPopup" data-corners="false" data-overlay-theme="b" data-shadow="false" class="ui-popup ui-body-inherit">
         <div class="mobile_select_menu_titlebar_div_grn">
            <div class="mobile_titlebar_grn">
               <span class="mobile_text_grn js_dialog_title"></span>
               <a href="javascript:void(0)" data-rel="back" class="mobile_titlebar_right_grn mobile_colse_icon_grn ui-link"></a>
            </div>
            <ul data-role="listview" data-theme="" class="js_member_list mobile-top-listview-member-grn mobile-bottom-listview-member-grn mobile_ul_grn ui-listview">
            </ul>
            <div class="mobile_select_button_area_grn">
               <div class="mobile_cancel_grn">
                  <div class="ui-btn ui-input-btn ui-corner-all ui-shadow ui-btn-inline">Close<input class="cancel_button js_close_button" type="reset" value="Close" data-inline="true" data-theme=""></div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@stop
