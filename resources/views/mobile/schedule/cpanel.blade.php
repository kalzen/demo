<script src="{{asset('assets/mobile/js/mobile_appmenu_grn.js')}}" type="text/javascript"></script>
<script type="text/javascript">
      // on page load
      $(document).ready(function(){
          // register event for logout handler
          var handler = ['mobile_logout'];
          $.each(handler, function(index, element){
              var ele = $("#"+element);
              ele.click(function(event){
                  var message = "<form id=\"form_logout\" method=\"post\" action=\"\/scripts\/garoon\/grn.exe\/command_logout?\">\n    <input type=\"hidden\" name=\"csrf_ticket\" value=\"7fd6a82448ea2fa071ec8bdaa91fa0e9\">\n<\/form>\n<div data-role=\"listview\" data-theme=\"c\" class=\"mobile-ul-top-grn mobile_ul_grn\">\n    <div class=\"mobile_delete_confirm_grn\">\n        <span class=\"mobile_icon_attention_grn\"><\/span>\n        <span class=\"mobile_text_grn\"><span>Are you sure you want to logout?<\/span><\/span>\n        <span class=\"mobile_text_grn\"><span><\/span><\/span>\n    <\/div>\n    <div data-theme=\"c\" class=\"mobile_button_area_grn mobile_button_delete_grn\">\n        <div class=\"mobile_ok_grn\">\n            <input id=\"msgbox_btn_yes\" type=\"button\" value=\"Yes\" data-inline=\"true\" data-theme=\"c\"\/>\n        <\/div>\n        <div class=\"mobile_cancel_grn\">\n            <input id=\"msgbox_btn_no\" type=\"button\" value=\"No\" data-inline=\"true\" data-theme=\"c\"\/>\n        <\/div>\n    <\/div>\n<\/div>";
                  // show message box
                  var Gm = grn.component.msgbox_mobile;
                  Gm.MsgBox.showDeleteConfirm(message, {
                      callback : function(result, form){
                          if(result == Gm.MsgBoxResult.yes)
                          {
                              $("#form_logout").submit();
                              Gm.MsgBox.close();
                          }
                      }
                  });
      
                  $("#msgbox").find("div.mobile_ul_grn").css({height: "", minHeight: $(window).height() * 0.5});
                  $("#msgbox").css("top", $(window).scrollTop() + ($(window).height() - $("#msgbox").innerHeight()) / 3);
                  $(window).resize(function(){
                      $("#msgbox").find("div.mobile_ul_grn").css({height: "", minHeight: $(window).height() * 0.5});
                      $("#msgbox").css("top", $(window).scrollTop() + ($(window).height() - $("#msgbox").innerHeight()) / 3);
                  });
                  $(window).scroll(function(){
                      $("#msgbox").css("top", $(window).scrollTop() + ($(window).height() - $("#msgbox").innerHeight()) / 3);
                  });
      
                  event.stopPropagation();
              });
          });
      });
</script>
<div data-role="panel" id="appmenu" data-position="right" data-display="push" data-swipe-close="false" data-dismissible="ture" class="mobile_app_menu_grn">
  <ul id="panelUl">
     <li data-icon="false" data-theme="c" class="ui-btn ui-btn-up-c"><a href="{{route('frontend.schedule.index')}}"><span class="mobile_schedule_grn"></span>Scheduler<span style="display:none;" class="mobile_notify_num_grn" id="schedule_notify_num">0</span></a></li>
     <li data-icon="false" data-theme="c" class="ui-btn ui-btn-up-c"><a href="#"><span class="mobile_bulletin_grn"></span>Bulletin Board<span style="display:none;" class="mobile_notify_num_grn" id="bulletin_notify_num">0</span></a></li>
     <li data-icon="false" data-theme="c" class="ui-btn ui-btn-up-c"><a href="/scripts/garoon/grn.exe/notification/mobile/index?"><span class="mobile_notification_grn"></span>Notifications<span style="display:none;" class="mobile_notify_num_grn" id="notification_notify_num">0</span></a></li>
     <li data-icon="false" data-theme="c" class="ui-btn ui-btn-up-c">
        <a href="{{route('logoutMember')}}">Logout</a>
     </li>
  </ul>
  <!-- panel content goes here -->
</div>
<!--panel end-->  