@extends('mobile.layouts.mobile_schedule')
@section('content')
<div class="mobile_spinner_grn" style="display:none;"><span></span></div>
<div data-role="page">
   @include('mobile/schedule/cpanel')
   <script src="{{asset('assets/mobile/js/mobile_autocomplete_user_facility.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/mobile/js/group_day.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/mobile/js/personal_week.js')}}" type="text/javascript"></script>
   <script type="text/javascript">
      grn.Component.Autocomplete.AutocompleteUserFaci.Parameters = {
          'url':"/api/ajax_user_org_facility_list?",
          'csrf_ticket': " cae91970eaac65005f88f75000e402bb"};
      
      grn.Component.Autocomplete.AutocompleteUserFaci.Msg = {
          'place_holder': 'User\/Facility search',
          'search_text':  ''
      };
      // footer add schedule function
      grn.page.schedule.mobile.personal_week.Parameters = {
          'bdate': '{{date('Y-m-d')}}',
          'uid': '',
          'gid': '15',
          'referer_key': 'cae91970eaac65005f88f75000e402bb',
          'url': "{{route('frontend.schedule.create')}}"
      };
   </script>
   <div data-role="content" data-theme="c" class="mobile-content-withList-grn">
      <div class="mobile_breadcrumbtitle_grn mobile_titlebar_content_withList_grn">
         <div class="mobile_breadcrumbtitle_right_grn"><span class="mobile_icon_breadcrumb_grn mobile_app_schedule_s_b_grn"></span>Scheduler</div>
      </div>
      <div class="mobile_schedule_week_header_grn">
         <div class="mobile_left_grn mobile_selected_grn"><span>Personal week</span></div>
         <div class="mobile_right_grn mobile_unselected_grn"><a href="{{route('frontend.schedule.group_day')}}" >Group day</a></div>
         <a href="javascript:void(0);" id="icon_search_grn" class="mobile_schedulelist_icon_search_grn"></a>
      </div>
      <div class="mobile_schedulelist_search_grn" style="display: none">
         <div class="mobile_seletedUser_list_grn">
            <form name="SearchForm">
               <div class="mobile_user_search_grn"><input type="search" name="search_text" class="tbsearch" placeholder="User/Facility search"/><a href="javascript:void(0);" class="mobile_delete_icon_grn"></a><a id="mobile_event_search" class="mobile_icon_grn"></a></div>
               <div class="mobile_pulldown_user_incremental_search_grn">
                  <div class="mobile_userselect_space_grn"></div>
                  <div class="mobile_user_list_scroll_grn">
                     <ul data-role="listview" data-theme="c"class="mobile-ul-top-grn mobile-ul-bottom-grn mobile_user_list_grn"></ul>
                  </div>
                  <!--mobile_user_list_scroll_grn-->
               </div>
            </form>
            <div class="mobile_userlines_grn"></div>
         </div>
      </div>
      <div class="mobile_schedule_week_menu_grn">
         <a href="{{route('frontend.schedule.index')}}?bdate={{date('Y-m-d',strtotime('- 6 days',strtotime($date_now)))}}&&uid={{isset($_GET['uid']) ? $_GET['uid'] : ''}}" class="mobile_left_array_grn"></a><a href="{{route('frontend.schedule.index')}}&&uid={{isset($_GET['uid']) ? $_GET['uid'] : ''}}" class="mobile_text_grn">Today</a><a href="{{route('frontend.schedule.index')}}?bdate={{date('Y-m-d',strtotime('+ 6 days',strtotime($date_now)))}}&&uid={{isset($_GET['uid']) ? $_GET['uid'] : ''}}" class="mobile_right_array_grn"> </a>
         <link href="{{asset('assets/mobile/css/mobile_datepicker.css')}}" rel="stylesheet" type="text/css">
         <script src="{{asset('assets/mobile/js/mobile_datepicker.js')}}" type="text/javascript"></script>
         <script language="JavaScript" text="text/javascript">
            (function(){
                $(document).ready(function(){
                  $('#start').on('click',function(event){
                      datepicker.setIconOnly('true');
                      datepicker.setCurrentElement(event.target);
                      datepicker.setInputAssociate('');
                  });
                });
            })();
         </script>
         <script id="datepicker_header" type="text/x-template">
            <div class="mobile_datepicker_sun_grn mobile_datepicker_listtitle_grn"><span>Sun</span></div><div class="mobile_datepicker_weekdays_grn mobile_datepicker_listtitle_grn"><span>Mon</span></div><div class="mobile_datepicker_weekdays_grn mobile_datepicker_listtitle_grn"><span>Tue</span></div><div class="mobile_datepicker_weekdays_grn mobile_datepicker_listtitle_grn"><span>Wed</span></div><div class="mobile_datepicker_weekdays_grn mobile_datepicker_listtitle_grn"><span>Thu</span></div><div class="mobile_datepicker_weekdays_grn mobile_datepicker_listtitle_grn"><span>Fri</span></div><div class="mobile_datepicker_sat_grn mobile_datepicker_listtitle_grn"><span>Sat</span></div>
         </script>
         <script id="datepicker_footer" type="text/x-template">
            <div data-theme="c" class="mobile_button_area_grn"><div class="mobile_cancel_grn"><input type="reset" value="Close" data-inline="true" data-theme="c"/></div></div>
         </script>
         <a id="start" href="#dialog_datepicker" data-rel="popup" data-transition="pop" data-theme="c" class="mobile_right_icon_grn"></a>
         <input type="hidden" id="start_set" name="start_set" value="{{date('Y-m-d')}}">
         <script language="JavaScript" text="text/javascript">
            var settings_datepicker = {
                container         : 'dialog_datepicker',
                year_unit         : '',
                month_unit        : '',
                day_unit          : '',
                delimiter         : '/',
                date_order        : 'njY',
                month_name        : 'Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec',
                wday_name         : 'Sun,Mon,Tue,Wed,Thu,Fri,Sat',
                ajaxURLGetHoliday : "/scripts/garoon/grn.exe/grn/ajax_get_holiday_for_mobile_datepicker?",
                locale            : 'en'
            };
            
            var datepicker;
            $(document).ready(function(){
                datepicker = new grn.component.mobile_datepicker(settings_datepicker);
            });
            
         </script>
         <div data-role="popup" id="dialog_datepicker" data-shadow="false" data-overlay-theme="b">
            <div class="mobile_datepicker_page_grn">
               <div class="mobile_datepicker_bar_grn">
                  <ul>
                     <li class="mobile_width_a_grn"><span class="mobile_datepicker_icon_backsub_grn"><a class="previous_month" href="#"></a></span></li>
                     <li class="mobile_width_b_grn">
                        <div data-role="fieldcontain" class="mobile_titlebar_right_grn mobile_select_grn">
                           <select class="year_select">
                              <option value="1970">1970</option>
                              <option value="1971">1971</option>
                              <option value="1972">1972</option>
                              <option value="1973">1973</option>
                              <option value="1974">1974</option>
                              <option value="1975">1975</option>
                              <option value="1976">1976</option>
                              <option value="1977">1977</option>
                              <option value="1978">1978</option>
                              <option value="1979">1979</option>
                              <option value="1980">1980</option>
                              <option value="1981">1981</option>
                              <option value="1982">1982</option>
                              <option value="1983">1983</option>
                              <option value="1984">1984</option>
                              <option value="1985">1985</option>
                              <option value="1986">1986</option>
                              <option value="1987">1987</option>
                              <option value="1988">1988</option>
                              <option value="1989">1989</option>
                              <option value="1990">1990</option>
                              <option value="1991">1991</option>
                              <option value="1992">1992</option>
                              <option value="1993">1993</option>
                              <option value="1994">1994</option>
                              <option value="1995">1995</option>
                              <option value="1996">1996</option>
                              <option value="1997">1997</option>
                              <option value="1998">1998</option>
                              <option value="1999">1999</option>
                              <option value="2000">2000</option>
                              <option value="2001">2001</option>
                              <option value="2002">2002</option>
                              <option value="2003">2003</option>
                              <option value="2004">2004</option>
                              <option value="2005">2005</option>
                              <option value="2006">2006</option>
                              <option value="2007">2007</option>
                              <option value="2008">2008</option>
                              <option value="2009">2009</option>
                              <option value="2010">2010</option>
                              <option value="2011">2011</option>
                              <option value="2012">2012</option>
                              <option value="2013">2013</option>
                              <option value="2014">2014</option>
                              <option value="2015">2015</option>
                              <option value="2016">2016</option>
                              <option value="2017">2017</option>
                              <option value="2018">2018</option>
                              <option value="2019">2019</option>
                              <option value="2020">2020</option>
                              <option value="2021" selected>2021</option>
                              <option value="2022">2022</option>
                              <option value="2023">2023</option>
                              <option value="2024">2024</option>
                              <option value="2025">2025</option>
                              <option value="2026">2026</option>
                              <option value="2027">2027</option>
                              <option value="2028">2028</option>
                              <option value="2029">2029</option>
                              <option value="2030">2030</option>
                              <option value="2031">2031</option>
                              <option value="2032">2032</option>
                              <option value="2033">2033</option>
                              <option value="2034">2034</option>
                              <option value="2035">2035</option>
                              <option value="2036">2036</option>
                              <option value="2037">2037</option>
                           </select>
                        </div>
                     </li>
                     <li class="mobile_width_c_grn">
                        <div data-role="fieldcontain" class="mobile_titlebar_right_grn mobile_select_grn">
                           <select class="month_select">
                              <option value="1" selected>Jan</option>
                              <option value="2">Feb</option>
                              <option value="3">Mar</option>
                              <option value="4">Apr</option>
                              <option value="5">May</option>
                              <option value="6">Jun</option>
                              <option value="7">Jul</option>
                              <option value="8">Aug</option>
                              <option value="9">Sep</option>
                              <option value="10">Oct</option>
                              <option value="11">Nov</option>
                              <option value="12">Dec</option>
                           </select>
                        </div>
                     </li>
                     <li class="mobile_width_d_grn"><a class="today" href="#">Today</a></li>
                     <li class="mobile_width_e_grn"><span class="mobile_datepicker_icon_advancesub_grn"><a class="next_month" href="#"></a></span></li>
                  </ul>
               </div>
               <!-- /mobile_datepicker_bar -->
               <div class="mobile_datepicker_grn">
                  <div class="mobile_datepicker_list_grn">
                  </div>
                  <!-- /grid-c -->
               </div>
               <!-- end of mobile_datepicker_grn-->
            </div>
         </div>
         <script language="JavaScript" text="text/javascript">
            (function(){
                $(document).ready(function(){
                  $('#start_set').on('change', function(event){
                    datepicker.handlerChangeValue(event);
                  });
                });
            })();
         </script>
      </div>
      <!--weekday start-->
      {!!$mobile_html!!}
      <div class="mobile_schedule_week_menu_grn mobile_schedule_week_menu_creditTitle_grn">
     <a href="{{route('frontend.schedule.index')}}?bdate={{date('Y-m-d',strtotime('- 6 days',strtotime($date_now)))}}&&uid={{isset($_GET['uid']) ? $_GET['uid'] : ''}}" class="mobile_left_array_grn"></a><a href="{{route('frontend.schedule.index')}}&&uid={{isset($_GET['uid']) ? $_GET['uid'] : ''}}" class="mobile_text_grn">Today</a><a href="{{route('frontend.schedule.index')}}?bdate={{date('Y-m-d',strtotime('+ 6 days',strtotime($date_now)))}}&&uid={{isset($_GET['uid']) ? $_GET['uid'] : ''}}" class="mobile_right_array_grn"> </a>
         <script language="JavaScript" text="text/javascript">
            (function(){
                $(document).ready(function(){
                  $('#date_select_footer').on('click',function(event){
                      datepicker.setIconOnly('true');
                      datepicker.setCurrentElement(event.target);
                      datepicker.setInputAssociate('');
                  });
                });
            })();
         </script>
         <script id="datepicker_header" type="text/x-template"></script>
         <script id="datepicker_footer" type="text/x-template">
            <div data-theme="c" class="mobile_button_area_grn"><div class="mobile_cancel_grn"><input type="reset" value="Close" data-inline="true" data-theme="c"/></div></div>
         </script>
         <a id="date_select_footer" href="#dialog_datepicker" data-rel="popup" data-transition="pop" data-theme="c" class="mobile_right_icon_grn"></a>
         <input type="hidden" id="date_select_footer_set" name="date_select_footer_set" value="{{date('Y-m-d')}}">
         <script language="JavaScript" text="text/javascript">
            (function(){
                $(document).ready(function(){
                  $('#date_select_footer_set').on('change', function(event){
                    datepicker.handlerChangeValue(event);
                  });
                });
            })();
         </script>
      </div>
      <input type="hidden" name="hfClearTbSearch" id="hfClearTbSearch" /><input type="hidden" name="hfPageLoad" id="hfPageLoad" />
   </div>
   <!-- end of content-->
   
   <div data-role="footer" data-position="fixed" data-tap-toggle="false" data-theme="c" id="mobile_footer">
      <div data-role="navbar" class="mobile_navbar_grn" id="mobile_footer_bar">
         <ul>
            <li></li>
            <li class="mobile_navbar_b_grn">
               <a id="footer_bar_b" href="javascript:void(0);" class="mobile_navbar_icon_b_grn mobile_icon_tool_add_grn"></a>
               <div class="mobile_bg_grn"></div>
            </li>
            <li class="mobile_navbar_c_grn">
               <a id="footer_bar_c" href="/scripts/garoon/grn.exe/notification/mobile/index?module_id=all" class="mobile_navbar_icon_c_grn mobile_icon_tool_notify_grn">
                  <div class="mobile_nitify_sum_position_grn"><span style="display:none;" class="mobile_notify_sum_grn" id="mobile_footer_bar_notify_num" mid="notification" >0</span></div>
               </a>
               <div class="mobile_bg_grn"></div>
            </li>
            <li class="mobile_navbar_d_grn">
               <a id="footer_bar_d" href="javascript:void(0);" class="mobile_navbar_icon_d_grn mobile_icon_tool_menu_grn">
                  <div class="mobile_triangle_d_grn"></div>
               </a>
               <div class="mobile_bg_grn"></div>
            </li>
            <li class="mobile_navbar_e_grn">
               <a id="footer_bar_e" href="#appmenu" class="mobile_navbar_icon_e_grn mobile_icon_tool_appmenu_grn">
                  <div class="mobile_triangle_e_grn"></div>
               </a>
               <div class="mobile_bg_grn"></div>
            </li>
         </ul>
      </div>
      <!-- navbar --> 
      <div class="mobile_navbar_menu_grn" id="operate_menu">
         <ul class="mobile_scroll_area_grn">
         </ul>
         <ul>
            <li class="mobile_refresh_grn"><a href="#" onclick="javascript:grn.component.mobile_appmenu.mobile_reload();"><span></span>Refresh</a></li>
         </ul>
      </div>
      <!--navbar menu-->
   </div>
</div>
@stop