@extends('mobile.layouts.mobile_schedule')
@section('content')
<div class="mobile_spinner_grn" style="display:none;"><span></span></div>
<div data-role="page">
   <script src="{{asset('assets/mobile/js/checkbox_mobile.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/mobile/js/validator_mobile.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/mobile/js/schedule_banner_add.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/mobile/js/schedule_repeat_add.js')}}" type="text/javascript"></script>
   <form id="schedule_mobile_repeat_add" name="schedule/mobile/repeat_add" method="post" enctype="multipart/form-data" action="/schedule/store?" data-ajax="false">
      <input type="hidden" name="_token" value="{{ csrf_token() }}" />
      <!---GTM-143-->
      <input type="hidden" name ="pattern" value='3' />
      <!---end GTM-143-->
      <input type="hidden" name="RP" value="1">
      <div data-role="content" class="mobile-content-grn" data-theme="c">
         <!--breadcrumb-->
         <script src="{{asset('assets/mobile/js/mobile_breadcrumb.js')}}" type="text/javascript"></script>
         <div class="mobile_breadcrumbtitle_grn mobile_titlebar_content_withList_grn">
            <div class="mobile_breadcrumbtitle_left_grn " id="left_text"><span class="mobile_icon_breadcrumb_grn mobile_app_schedule_s_b_grn"></span><a href="{{route('frontend.schedule.index')}}">Schedule</a><span class="mobile_icon_breadcrumb_arrow_grn"></span></div>
            <div class="mobile_breadcrumbtitle_right_grn" id="right_text">New appointment</div>
         </div>
         <!--end breadcrumb-->
         <div class="mobile_schedule_grn">
            <script language="JavaScript" type="text/javascript">
               <!--
               function add_menu_submit(t)
               {
                   f = document.forms["schedule/mobile/repeat_add"];
                   f.target = '_self';
                   var tab_item = f.elements['tab'];
                   var page_name = f.elements['page_name'];
                   page_name.value = t;
                   f.action = '/scripts/garoon/grn.exe/schedule/mobile/command_change_page?';
                   if (t == 'normal')
                   {
                       if( tab_item )
                       {
                           tab_item.value = 'repeat_add';
                       }
                   }
                   else if (t == 'banner')
                   {
                       if( tab_item )
                       {
                           tab_item.value = 'repeat_add';
                       }
                   }
                   else if (t == 'repeat')
                   {
                       if( tab_item )
                       {
                           tab_item.value = 'repeat_add';
                       }
                   }
               
                   var form = $(f);
               
                   $('input').each(function(){
                       if($(this).closest('form').attr('id') != form.attr('id'))
                       {
                           $(this).appendTo(form)
                       }
                   });
               
                   $('select').each(function(){
                       if($(this).closest('form').attr('id') != form.attr('id'))
                       {
                           $(this).appendTo(form)
                       }
                   });
               
                   $('textarea').each(function(){
                       if($(this).closest('form').attr('id') != form.attr('id'))
                       {
                           $(this).appendTo(form)
                       }
                   });
               
                   // fix GRB-16207
                   var file = $('input[type=file]');
                   if(file.length > 0)
                   {
                       var checked = $('input[name=attached_file]').prop('checked');
                       if(typeof checked == 'undefined')
                       {
                           file.remove();
                       }
                   }
               
                   f.submit();
               }
               
               //-->
            </script>
            <input type="hidden" name="tab" value="">
            <input type="hidden" name="page_name" value="">
            <input type="hidden" name="bdate" value="2021-01-25">
            <input type="hidden" name="uid" value="58">
            <input type="hidden" name="gid" value="">
            <input type="hidden" name="referer_key" value="dc6b970f1bccf33a08d707aa702417e9">
            <div class="mobile_schedulelist_menu_grn mobile-div-title-grn">
               <div class="ui-grid-b">
                  <div class="ui-block-a"><a href="{{route('frontend.schedule.create')}}">Regular</a></div>
                  <div class="ui-block-b"><a href="{{route('frontend.schedule.create_all_day')}}">All day</a></div>
                  <div class="ui-block-c"><span class="mobile_schedulelist_menuhover_grn">Repeating</span></div>
               </div>
            </div>
            <ul data-role="listview" data-theme="c" class="mobile-li-PersonInCharge-grn mobile-div-title-grn">
               <div class="mobile-separation-grn"></div>
               <li data-icon="false">
                  <a href="#_repeat_condition">
                     <div class="mobile-label-small-grn">Repeating conditions</div>
                     <div class="mobile_for_totalnumber_grn">
                        <div id="repeat_condition_title" class="mobile-font-warp-grn">Every week Monday</div>
                     </div>
                     <!--mobile_for_totalnumber_grn-->
                     <span class="mobile-array-todo-grn"></span>
                  </a>
               </li>
               <div class="mobile-separation-grn"></div>
            </ul>
            <div class="mobile-div-title-grn">
               <span class="mobile-label-grn mobile_block_grn">Period</span>
               <link href="{{asset('assets/mobile/css/mobile_datepicker.css')}}" rel="stylesheet" type="text/css">
               <script src="{{asset('assets/mobile/js/mobile_datepicker.js')}}" type="text/javascript"></script>
               <script language="JavaScript" text="text/javascript">
                  (function(){
                      $(document).ready(function(){
                        $('#start').on('click',function(event){
                            datepicker.setIconOnly('');
                            datepicker.setCurrentElement(event.target);
                            datepicker.setInputAssociate('end');
                        });
                      });
                  })();
               </script>
               <script id="datepicker_header" type="text/x-template">
                  <div class="mobile_datepicker_sun_grn mobile_datepicker_listtitle_grn"><span>Sun</span></div><div class="mobile_datepicker_weekdays_grn mobile_datepicker_listtitle_grn"><span>Mon</span></div><div class="mobile_datepicker_weekdays_grn mobile_datepicker_listtitle_grn"><span>Tue</span></div><div class="mobile_datepicker_weekdays_grn mobile_datepicker_listtitle_grn"><span>Wed</span></div><div class="mobile_datepicker_weekdays_grn mobile_datepicker_listtitle_grn"><span>Thu</span></div><div class="mobile_datepicker_weekdays_grn mobile_datepicker_listtitle_grn"><span>Fri</span></div><div class="mobile_datepicker_sat_grn mobile_datepicker_listtitle_grn"><span>Sat</span></div>
               </script>
               <script id="datepicker_footer" type="text/x-template">
                  <div data-theme="c" class="mobile_button_area_grn"><div class="mobile_cancel_grn"><input type="reset" value="Close" data-inline="true" data-theme="c"/></div></div>
               </script>
               <div class="mobile-contentSubButton-grn mobile_select_date_grn ">
                  <a id="start" href="#dialog_datepicker" data-rel="popup" data-transition="pop" data-theme="c" data-role="button" class="mobile-btn-calender-grn">
                  <span class="mobile-icon-calender-grn"></span>
                  <span class="mobile-font-normal-grn">{{isset($schedule) ? date('M /d (D) /Y',strtotime($schedule->start_date)) : date('M /d (D) /Y',strtotime($date))}}</span>
                  </a>
                  <input type="hidden" id="start_set" name="start_set" value="{{isset($schedule) ? date('Y-m-d',strtotime($schedule->start_date)) : date('Y-m-d',strtotime($date))}}">
                  <input type="hidden" id="start_year" name="start_year" value="{{isset($schedule) ? date('Y',strtotime($schedule->start_date)) : date('Y',strtotime($date))}}">
                  <input type="hidden" id="start_month" name="start_month" value="{{isset($schedule) ? date('m',strtotime($schedule->start_date)) : date('m',strtotime($date))}}">
                  <input type="hidden" id="start_day" name="start_day" value="{{isset($schedule) ? date('d',strtotime($schedule->start_date)) : date('d',strtotime($date))}}">
               </div>
               <script language="JavaScript" text="text/javascript">
                  var settings_datepicker = {
                      container         : 'dialog_datepicker',
                      year_unit         : '',
                      month_unit        : '',
                      day_unit          : '',
                      delimiter         : '/',
                      date_order        : 'njY',
                      month_name        : 'Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec',
                      wday_name         : 'Sun,Mon,Tue,Wed,Thu,Fri,Sat',
                      ajaxURLGetHoliday : "//ajax_get_holiday_for_mobile_datepicker?",
                      locale            : 'en'
                  };
                  
                  var datepicker;
                  $(document).ready(function(){
                      datepicker = new grn.component.mobile_datepicker(settings_datepicker);
                  });
                  
               </script>
               <div data-role="popup" id="dialog_datepicker" data-shadow="false" data-overlay-theme="b">
                  <div class="mobile_datepicker_page_grn">
                     <div class="mobile_datepicker_bar_grn">
                        <ul>
                           <li class="mobile_width_a_grn"><span class="mobile_datepicker_icon_backsub_grn"><a class="previous_month" href="#"></a></span></li>
                           <li class="mobile_width_b_grn">
                              <div data-role="fieldcontain" class="mobile_titlebar_right_grn mobile_select_grn">
                                 <select class="year_select">
                                    {!! isset($start_year_html)? $start_year_html : $year_html!!}
                                 </select>
                              </div>
                           </li>
                           <li class="mobile_width_c_grn">
                              <div data-role="fieldcontain" class="mobile_titlebar_right_grn mobile_select_grn">
                                 <select class="month_select">
                                    {!! isset($start_month_html)? $start_month_html : $month_html!!}
                                 </select>
                              </div>
                           </li>
                           <li class="mobile_width_d_grn"><a class="today" href="#">Today</a></li>
                           <li class="mobile_width_e_grn"><span class="mobile_datepicker_icon_advancesub_grn"><a class="next_month" href="#"></a></span></li>
                        </ul>
                     </div>
                     <!-- /mobile_datepicker_bar -->
                     <div class="mobile_datepicker_grn">
                        <div class="mobile_datepicker_list_grn">
                        </div>
                        <!-- /grid-c -->
                     </div>
                     <!-- end of mobile_datepicker_grn-->
                  </div>
               </div>
               <script language="JavaScript" text="text/javascript">
                  (function(){
                      $(document).ready(function(){
                        $('#start_set').on('change', function(event){
                          datepicker.handlerChangeValue(event);
                        });
                      });
                  })();
               </script>
               <span class="mobile_schedule_timeline_grn"></span>
            </div>
            <!--mobile-div-title-grn-->
            <div class="mobile-div-title-grn">
               <script language="JavaScript" text="text/javascript">
                  (function(){
                      $(document).ready(function(){
                        $('#end').on('click',function(event){
                            datepicker.setIconOnly('');
                            datepicker.setCurrentElement(event.target);
                            datepicker.setInputAssociate('');
                        });
                      });
                  })();
               </script>
               <script id="datepicker_header" type="text/x-template"></script>
               <script id="datepicker_footer" type="text/x-template">
                  <div data-theme="c" class="mobile_button_area_grn"><div class="mobile_cancel_grn"><input type="reset" value="Close" data-inline="true" data-theme="c"/></div></div>
               </script>
               <div class="mobile-contentSubButton-grn mobile_select_date_grn ">
                  <a id="end" href="#dialog_datepicker" data-rel="popup" data-transition="pop" data-theme="c" data-role="button" class="mobile-btn-calender-grn">
                  <span class="mobile-icon-calender-grn"></span>
                  <span class="mobile-font-normal-grn">{{isset($schedule) ? date('M /d (D) /Y',strtotime($schedule->start_date)) : date('M /d (D) /Y',strtotime($date))}}</span>
                  </a>
                  <input type="hidden" id="end_set" name="end_set" value="{{isset($schedule) ? date('Y-m-d',strtotime($schedule->end_date)) : date('Y-m-d',strtotime($date))}}">
                  <input type="hidden" id="end_year" name="end_year" value="{{isset($schedule) ? date('Y',strtotime($schedule->end_date)) : date('Y',strtotime($date))}}">
                  <input type="hidden" id="end_month" name="end_month" value="{{isset($schedule) ? date('m',strtotime($schedule->end_date)) : date('m',strtotime($date))}}">
                  <input type="hidden" id="end_day" name="end_day" value="{{isset($schedule) ? date('d',strtotime($schedule->start_date)) : date('d',strtotime($date))}}">
               </div>
               <script language="JavaScript" text="text/javascript">
                  (function(){
                      $(document).ready(function(){
                        $('#end_set').on('change', function(event){
                          datepicker.handlerChangeValue(event);
                        });
                      });
                  })();
               </script>
               <div id="invalid_date" style="display:none;" class="mobile_schedule_list_grn mobile_schedule_information_alert_grn">
                  <span class="mobile_iconposition_attentions_grn mobile_icon_attentions_grn"></span>
                  <span class="mobile_text_information_grn">The end date must follow the start date.</span>
               </div>
            </div>
            <!--mobile-div-title-grn-->
            <div class="mobile-div-title-grn">
               <span class="mobile-label-grn mobile_block_grn">Time</span>
               <div data-role="fieldcontain" class="mobile_select_date_grn mobile_select_time_grn">
                  <select id="start_hour" name="start_hour" >
                     {!! isset($start_hour_html)? $start_hour_html : $hour_html!!}
                  </select>
               </div>
               <div data-role="fieldcontain" class="mobile_select_date_grn mobile_select_time_grn">
                  <select id="start_minute" name="start_minute">
                     {!! isset($start_minute_html)? $start_minute_html : $minute_html!!}
                  </select>
               </div>
               <span class="mobile_schedule_timeline_grn"></span>
               <div data-role="fieldcontain" class="mobile_select_date_grn mobile_select_time_grn">
                  <select id="end_hour" name="end_hour" >
                      {!! isset($end_hour_html)? $end_hour_html : $hour_html!!}
                  </select>
               </div>
               <div data-role="fieldcontain" class="mobile_select_date_grn mobile_select_time_grn">
                  <select id="end_minute" name="end_minute">
                     {!! isset($end_minute_html)? $end_minute_html : $minute_html!!}
                  </select>
               </div>
               <span class="mobile_timezone_grn">(UTC+07:00) VietNam</span>
               <input type='hidden' id='timezone' name='timezone' value='Asia/Ho_Chi_Minh' />
               <input type='hidden' id='end_timezone' name='end_timezone' value='Asia/Ho_Chi_Minh' />
               <div id="validate_repeat_date" style="display:none;" class="mobile_schedule_list_grn mobile_schedule_information_alert_grn">
                  <span class="mobile_iconposition_attentions_grn mobile_icon_attentions_grn"></span>
                  <span class="mobile_text_information_grn">Start time is empty.</span>
               </div>
            </div>
            <div class="mobile-div-title-grn">
               <span class="mobile-label-grn">Appointment type</span>
               <a id="menu_schedule" href="#popup_menu_schedule" data-rel="popup" data-transition="pop" data-shadow="true" data-wrapperels="span" class="mobile_select_view_grn">
               <span class="mobile_event_menu_content_grn">----------</span>
               <span class="mobile_select_icon_grn"></span>
               </a>
               <input type="hidden" value="" name="menu" id="menu_schedule_value">
               <script src="{{asset('assets/mobile/js/mobile_select_menu.js')}}" type="text/javascript"></script>
               <script language="JavaScript" text="text/javascript">
                  (function(){
                  
                  
                      var settings = {
                          selectMenu       : '#menu_schedule',
                          selectMenuValue  : '#menu_schedule_value',
                          container        : '#popup_menu_schedule',
                          selectedValue    : '',
                          expandMenuSelect : '#',
                          selectMenuTitle  : 'Appointment type',
                          useMenuColor     : 'true',
                          defaultTitle     : '--------------------',
                          name             : 'mobileSelectMenu_popup_menu_schedule'
                      };
                  
                  
                      $(document).ready(function(){
                          var G = new grn.component.mobile_select_menu(settings);
                          G.initSize =false;
                  
                          $(document).on("popupbeforeposition","#popup_menu_schedule",function(event){
                              if(!G.initSize || G.needResize)
                              {
                                  G.setSizeSelectMenu(event);
                                  G.initSize = true;
                              }
                          });
                  
                          $('#popup_menu_schedule').on('click',function(event){
                              G.setSizeSelectMenu(event);
                          });
                  
                          $( window ).on( "resize", function( event ) {
                              var popup = $('#popup_menu_schedule');
                              if(popup.is(':visible'))
                              {
                                  G.setSizeSelectMenu(event);
                              }
                          });
                      });
                  
                  })();
               </script>
               <div data-role="popup" id="popup_menu_schedule" data-corners="false" data-overlay-theme="b" data-shadow="false">
                  <div class="mobile_select_menu_titlebar_div_grn">
                     <div class="mobile_titlebar_grn">
                        <span class="mobile_text_grn">Appointment type</span>
                        <a href="#" class="mobile_titlebar_right_grn mobile_colse_icon_grn"></a>
                     </div>
                     <ul data-role="listview" data-theme="c" class="mobile-ul-top-grn mobile_ul_grn select_menu_list">
                        <li data-icon="false" class="mobile_list_grn mobile_event_list_grn">
                           <span class="mobile_select_item"></span>
                           <a href="#" class="selection_item" data-value="">--------------------</a>
                        </li>
                        <li data-icon="false" class="mobile_list_grn mobile_event_list_grn">
                           <span class="mobile_select_item "></span>
                           <span class="mobile_event_menu_grn mobile_event_menu_color5_grn"></span>
                           <a href="#" class="selection_item" data-value="Cuộc họp;#5">Cuộc họp</a>
                        </li>
                        <li data-icon="false" class="mobile_list_grn mobile_event_list_grn">
                           <span class="mobile_select_item "></span>
                           <span class="mobile_event_menu_grn mobile_event_menu_color2_grn"></span>
                           <a href="#" class="selection_item" data-value="Hội thảo;#2">Hội thảo</a>
                        </li>
                        <li data-icon="false" class="mobile_list_grn mobile_event_list_grn">
                           <span class="mobile_select_item "></span>
                           <span class="mobile_event_menu_grn mobile_event_menu_color1_grn"></span>
                           <a href="#" class="selection_item" data-value="Đào tạo;#1">Đào tạo</a>
                        </li>
                        <li data-icon="false" class="mobile_list_grn mobile_event_list_grn">
                           <span class="mobile_select_item "></span>
                           <span class="mobile_event_menu_grn mobile_event_menu_color7_grn"></span>
                           <a href="#" class="selection_item" data-value="Công tác;#7">Công tác</a>
                        </li>
                        <li data-icon="false" class="mobile_list_grn mobile_event_list_grn">
                           <span class="mobile_select_item "></span>
                           <span class="mobile_event_menu_grn mobile_event_menu_color6_grn"></span>
                           <a href="#" class="selection_item" data-value="Ngày lễ;#6">Ngày lễ</a>
                        </li>
                        <li data-icon="false" class="mobile_list_grn mobile_event_list_grn">
                           <span class="mobile_select_item "></span>
                           <span class="mobile_event_menu_grn mobile_event_menu_color3_grn"></span>
                           <a href="#" class="selection_item" data-value="Phỏng vấn;#3">Phỏng vấn</a>
                        </li>
                        <li data-icon="false" class="mobile_list_grn mobile_event_list_grn">
                           <span class="mobile_select_item "></span>
                           <span class="mobile_event_menu_grn mobile_event_menu_color3_grn"></span>
                           <a href="#" class="selection_item" data-value="Gặp gỡ;#3">Gặp gỡ</a>
                        </li>
                     </ul>
                     <div class="mobile_select_button_area_grn">
                        <div class="mobile_cancel_grn">
                           <input class="cancel_button" type="reset" value="Close" data-inline="true" data-theme="c"/>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--mobile-div-title-grn-->
            <div class="mobile-div-title-grn">
               <span class="mobile-label-grn">Title</span>
               <input  id="title" type="text" name="title" size="50" maxlength="100" onKeyPress="return event.keyCode != 13;">
            </div>
            <!--mobile-div-title-grn-->
            <ul data-role="listview" data-theme="c" class="mobile-li-PersonInCharge-grn mobile-div-title-grn">
               <div class="mobile-separation-grn"></div>
               <li data-icon="false">
                  <a id="attendees" class="ui-link-inherit" href="#_sUID">
                     <div class="mobile-label-small-grn">Attendees</div>
                     <div class="mobile_for_totalnumber_grn">
                     </div>
                     <span class="totalNumber-grn"></span>
                     <span class="mobile-array-todo-grn"></span>
                  </a>
               </li>
               <div class="mobile-separation-grn"></div>
               <li data-icon="false" class="ui-first-child">
                  <a id="default_public" href="#_p_sUID" class="ui-link-inherit ui-btn" id="">
                     <div class="mobile-label-small-grn">Shared</div>
                     <div class="mobile_for_totalnumber_grn"></div>
                     <!--mobile_for_totalnumber_grn--><span class="totalNumber-grn"></span><span class="mobile-array-todo-grn"></span>
                  </a>
               </li>
               <div class="mobile-separation-grn"></div>
               <li data-icon="false">
                  <a id="facilities" class="ui-link-inherit" href="#_sITEM">
                     <div class="mobile-label-small-grn">Facilities</div>
                     <div class="mobile_for_totalnumber_grn">
                     </div>
                     <!--mobile_for_totalnumber_grn-->
                     <span class="totalNumber-grn"></span>
                     <span class="mobile-array-todo-grn"></span>
                  </a>
               </li>
               <div class="mobile-separation-grn"></div>
            </ul>
            <div class="mobile-div-title-grn">
               <span class="mobile-label-grn">Visibility</span>
               <div id="idPrivateRaidoButton" class="mobile_radiobutton_group_grn">
                  <span class="mobile_radiobutton_base_grn" style="margin-right: 14px;">
                     <span class="mobile_icon_radiobutton_grn mobile_icon_radiobuttonon_grn"></span>
                     <span class="mobile_radiobutton_grn">
                        <div class=" ui-radio"><input type="radio" name="" id="" value="0"></div>
                     </span>
                     <span class="mobile_text_grn">Public</span>
                  </span>
                  <span class="mobile_radiobutton_base_grn" style="margin-right: 14px;">
                     <span class="mobile_icon_radiobutton_grn mobile_icon_radiobuttonoff_grn"></span>
                     <span class="mobile_radiobutton_grn">
                        <div class=" ui-radio"><input type="radio" name="" id="" value="1"></div>
                     </span>
                     <span class="mobile_text_grn">Private</span>
                  </span>
                  
               </div>
            </div>
            
            <div class="mobile-div-title-grn">
               <span class="mobile-label-grn">Notes</span>
               <textarea id="textarea_id" name="memo" class="mobile-textarea-grn autoexpand ui-input-text ui-shadow-inset ui-body-inherit ui-corner-all ui-textinput-autogrow" wrap="virtual" role="form" cols="50" rows="5" style="white-space: pre-wrap; height: 175px;"></textarea>
            </div>
            <!--mobile-div-title-grn-->
            <script src="{{asset('assets/mobile/js/handle_add.js')}}" type="text/javascript"></script>
            <script type="text/javascript" language="javascript">
               var handleAdd = grn.page.schedule.mobile.handle_add;
               handleAdd.ajaxURL = '/schedule/store?';
               handleAdd.defaultBackURL = '/schedule/index?';
               handleAdd.formId = 'schedule_mobile_repeat_add';
               handleAdd.conflict = 'One or more facilities are conflicting in the following appointments:';
               handleAdd.onlyNoConflict = 'Do you want to add only appointments with no conflicting facilities?';
               handleAdd.more = '';
               handleAdd.conflictAll = 'All appointments have conflicting facilities. No appointment can be added.';
               handleAdd.header = '<tr><th>Date</th><th>Conflicting facility </th></tr>';
            </script>
            <div data-role="popup" id="popup_duplicated" data-corners="false" data-overlay-theme="b" data-shadow="false">
               <div class="mobile_select_menu_titlebar_div_grn mobile_repeating_div_grn">
                  <div data-role="listview" data-theme="c" class="mobile_ul_grn">
                     <div class="mobile_repeating_grn">
                        <div data-role="listview" data-theme="c" class="mobile_repeating_scroll_grn">
                           <span class="mobile_icon_attention_grn"></span>
                           <span class="mobile_repeating_text_grn"></span>
                           <table width="100%" border="0" class="mobile_repeating_table_grn">
                              <tr>
                                 <th>Date</th>
                                 <th>Conflicting facility </th>
                              </tr>
                           </table>
                        </div>
                        <!--mobile_error_scroll_grn end-->
                     </div>
                     <div class="mobile_button_area_grn mobile_button_delete_grn">
                        <div class="mobile_ok_grn">
                           <input type="submit" value="Yes" data-inline="true" data-theme="c"/>
                        </div>
                        <div class="mobile_cancel_grn no_button">
                           <input type="button" value="No" data-inline="true" data-theme="c" data-disabled="false"/>
                        </div>
                        <div class="mobile_cancel_grn cancel_button">
                           <input type="reset" value="Cancel" data-inline="true" data-theme="c" data-disabled="false"/>
                        </div>
                     </div>
                  </div>
               </div>
               <!--titlebar-->
            </div>
            <!--page-->
            <div class="mobile_margin_plus_button_grn"></div>
            <div data-theme="c" class="mobile-buttonArea-grn">
               <div class="mobile_ok_grn">
                  <input id="addBtn" type="button" value="Add" data-inline="true" data-theme="c"/>
               </div>
               <div class="mobile_cancel_grn mobile_show_overlay_js">
                  <input id="cancelBtn" class="cancel_button" type="reset" value="Cancel" data-inline="true" data-theme="c" onclick="javascript:location.href='{{route('frontend.schedule.index')}}'"/>
               </div>
            </div>
         </div>
         <!-- end of mobile_addschedule_grn-->
      </div>
      <!-- end of content-->
      <script src="{{asset('assets/mobile/js/mobile_footer_for_multipage.js')}}" type="text/javascript"></script>
   </form>
</div>
<link href="{{asset('assets/mobile/css/mobile_userselect.css')}}" rel="stylesheet" type="text/css">
<script src="{{asset('assets/mobile/js/mobile_item_select.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/mobile/js/mobile_user_select.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/resource-en.js')}}"></script>
<script language="JavaScript" text="text/javascript">
   (function(){
   
       
       var settings = {
           element_id  : 'sUID',
           ajaxGetItemURL : "/api/ajax_get_user_for_mobile_selection?",
           ajaxSearchItemURL : "/api/ajax_search_user_for_mobile_selection?",
           args        : "access_plugin=YToyOntzOjQ6Im5hbWUiO3M6ODoic2NoZWR1bGUiO3M6NjoicGFyYW1zIjthOjI6e3M6NjoiYWN0aW9uIjthOjI6e2k6MDtzOjQ6InJlYWQiO2k6MTtzOjM6ImFkZCI7fXM6MTI6InNlc3Npb25fbmFtZSI7czoyNjoic2NoZWR1bGUvbW9iaWxlL3JlcGVhdF9hZGQiO319&plugin_session_name=schedule/mobile/repeat_add&app_id=schedule",
           include_org : '1',
           isCalendar : '1',
           show_group_role : '1',
           isAllowedRole : '',
           src_item    : '#src_user',
           dest_item   : '#dest_user',
           type_item   : 'user',
           empty_list  : "",
           none_selected : "None",
           search_caption: "(Search results)",
           group_select_menu_id : 'category_options_sUID',
           associate_id : 'attendees',
           selectedItem : {"list":{"0":{"type":"user","id":"{{\Auth::guard('member')->user()->id}}","foreignKey":"brown","displayName":"{{\Auth::guard('member')->user()->full_name}}","image":"{{\Auth::guard('member')->user()->avatar}}","isInvalidUser":false,"isNotUsingApp":false,"isLoginUser":true}}},
           page_name : '',
           default_title_group_select : 'Select group.',
           categorySelectUI : 'user_category_sUID',
           categorySelectValue : 'category_title_sUID',
           popupItemCategory : 'popup_user_category_sUID'
       };
       
   
       $(document).ready(function(){
           var G = new grn.component.mobile_user_select(settings);
           G.initSelectedItem();
   
           $(document).on("pagebeforeshow","#_sUID",function(event, data){
               G.bindEventBeforePageShow();
   
               if(typeof data.prevPage.attr('id') == 'undefined')
               {
                   G.processing = false;
                   G.saveLastState();
                   G.resetSourceItemList();
                   G.resetCategorySelect();
               }
           });
   
           $(document).on("pagebeforehide","#_sUID",function(event, data){
               if(typeof data.nextPage.attr('id') == 'undefined' && !G.adding )
               {
                   G.cancel();
               }
               G.adding = false;
           });
       });
   
   })();
</script>
<script id="src_user" type="text/x-template">
   <li data-icon="false" data-corners="false" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-iconpos="right" data-theme="c">
       <a class="selection_item ui-btn" href="#">
           <div class="mobile_add_grn"></div>
           <div class="mobile_user_photo_grn mobile_img_userPlofile_grn"></div>
           <div class="mobile_info_grn">
               <div class="mobile_position_center_grn">
                   <div class="mobile_position_width_grn">
                       <div class="mobile_user_grn"></div>
                       <div class="mobile_text_grn"></div>
                   </div>
               </div>
           </div>
       </a>
   </li>
</script>
<script id="dest_user" type="text/x-template">
   <li data-icon="false" class="mobile_selected_grn" data-corners="false" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-iconpos="right" data-theme="c">
       <div class="mobile_delete_grn"></div>
       <a class="selection_item ui-btn" href="#">
           <div class="mobile_user_photo_grn mobile_img_userPlofile_grn"></div>
           <div class="mobile_info_grn">
               <div class="mobile_position_center_grn">
                   <div class="mobile_position_width_grn">
                       <div class="mobile_user_grn"></div>
                       <div class="mobile_text_grn"></div>
                   </div>
               </div>
           </div>
           <span></span>
       </a>
   </li>
</script>
<div data-role="page" id="_sUID">
   <div data-role="content" data-theme="c" class="mobile-content-grn">
      <div class="mobile_breadcrumbtitle_grn mobile_titlebar_content_withList_grn">
         <div class="mobile_breadcrumbtitle_left_grn"><span class="mobile_icon_breadcrumb_grn mobile_app_schedule_s_b_grn"></span><a href="#" >New appointment</a><span class="mobile_icon_breadcrumb_arrow_grn"></span></div>
         <div class="mobile_breadcrumbtitle_right_grn">Attendees</div>
      </div>
      <div class="mobile_seletedUser_list_grn">
         <form action="#">
            <div class="mobile_user_search_grn">
               <input id="search_user" class="search_item" type="search" name="search_user_sUID" placeholder="User search"/>
               <a href="#" class="mobile_delete_icon_grn delete_input"></a>
               <a href="#" class="mobile_icon_grn search_item_icon"></a>
            </div>
         </form>
      </div>
      <div class="mobile_user_group_grn">
         <a id="user_category_sUID" href="#popup_user_category_sUID" data-rel="popup" data-transition="pop" data-shadow="true" data-wrapperels="span" class="mobile_select_view_grn">
         <span class="mobile_event_menu_content_grn">Select group.</span>
         <span class="mobile_select_icon_grn"></span>
         </a>
         <a class="ui-link-inherit" href="#_popup_group_categories_tree_sUID">
            <div class="mobile_group_icon_grn"></div>
         </a>
         <input type="hidden" value="" id="category_options_sUID">
         <input type="hidden" value="" id="category_title_sUID">
         <script language="JavaScript" text="text/javascript">
            (function(){
            
            
                var settings = {
                    selectMenu       : '#user_category_sUID',
                    selectMenuValue  : '#category_options_sUID',
                    container        : '#popup_user_category_sUID',
                    selectedValue    : 'Select group.',
                    expandMenuSelect : '#category_title_sUID',
                    selectMenuTitle  : 'Select group',
                    useMenuColor     : '',
                    defaultTitle     : '',
                    name             : 'mobileSelectMenu_popup_user_category_sUID'
                };
            
            
                $(document).ready(function(){
                    var G = new grn.component.mobile_select_menu(settings);
                    G.initSize =false;
            
                    $(document).on("popupbeforeposition","#popup_user_category_sUID",function(event){
                        if(!G.initSize || G.needResize)
                        {
                            G.setSizeSelectMenu(event);
                            G.initSize = true;
                        }
                    });
            
                    $('#popup_user_category_sUID').on('click',function(event){
                        G.setSizeSelectMenu(event);
                    });
            
                    $( window ).on( "resize", function( event ) {
                        var popup = $('#popup_user_category_sUID');
                        if(popup.is(':visible'))
                        {
                            G.setSizeSelectMenu(event);
                        }
                    });
                });
            
            })();
         </script>
         <div data-role="popup" id="popup_user_category_sUID" data-corners="false" data-overlay-theme="b" data-shadow="false">
            <div class="mobile_select_menu_titlebar_div_grn">
               <div class="mobile_titlebar_grn">
                  <span class="mobile_text_grn">Select group</span>
                  <a href="#" class="mobile_titlebar_right_grn mobile_colse_icon_grn"></a>
               </div>
               <ul data-role="listview" data-theme="c" class="mobile-ul-top-grn mobile_ul_grn select_menu_list">
                  @foreach($department as $key=>$val)
                  <li data-icon="false" class="mobile_list_grn ">
                     <span class="mobile_select_item "></span>
                     <a href="#" class="selection_item" data-value="{{$val->id}}">{{$val->name}} </a>
                  </li>
                  @endforeach
               </ul>
               <div class="mobile_select_button_area_grn">
                  <div class="mobile_cancel_grn">
                     <input class="cancel_button" type="reset" value="Close" data-inline="true" data-theme="c"/>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="mobile_user_label_area_grn">
         <span class="mobile-label-grn">Users</span>
         <a class="add_all_to_selected_list mobile-label-grn" href="#">Select all</a>
      </div>
      <div class="mobile_user_list_scroll_grn mobile-user-list-none-grn">
         <div class="mobile_list_shadow_grn"></div>
         <div class="mobile_icon_grn">
            <div class="mobile_icon_up_grn"></div>
            <div class="mobile_icon_down_grn"></div>
         </div>
         <ul data-role="listview" data-theme="c" class="source_items mobile-ul-top-grn mobile-ul-bottom-grn mobile_user_list_grn">
            <li data-icon="false" class="mobile_base_disable_grn"></li>
         </ul>
      </div>
      <!--mobile_user_list_scroll_grn-->
      <div class="mobile_user_label_area_grn">
         <span class="mobile-label-grn">Selected</span>
         <a class="remove_all_from_selected_list mobile-label-grn" href="#" style="display:none;">Clear selection</a>
      </div>
      <div class="mobile_user_list_scroll_grn">
         <div class="mobile_list_shadow_grn"></div>
         <div class="order_selected_list mobile_order_icon_grn">
            <div class="mobile_order_control_grn order_top">
               <div class="mobile_order_top_grn"></div>
            </div>
            <div class="mobile_order_control_grn order_up">
               <div class="mobile_order_up_grn"></div>
            </div>
            <div class="mobile_order_control_grn order_down">
               <div class="mobile_order_down_grn"></div>
            </div>
            <div class="mobile_order_control_grn order_bottom">
               <div class="mobile_order_bottom_grn"></div>
            </div>
         </div>
         <ul data-role="listview" data-theme="c" class="selected_items mobile-ul-top-grn mobile-ul-bottom-grn mobile_user_list_grn">
         </ul>
      </div>
      <!--mobile_user_list_scroll_grn-->
      <div data-theme="c" class="mobile_button_area_grn">
         <div class="mobile_ok_grn">
            <input type="submit" value="Apply" data-inline="true" data-theme="c" />
         </div>
         <div class="mobile_cancel_grn">
            <input type="reset" value="Cancel" data-inline="true" data-theme="c"/>
         </div>
      </div>
      <input type="hidden" id="selected_groups_sUID" name="selected_groups_sUID" value="">
      <input type="hidden" id="selected_users_sUID" name="selected_users_sUID" value="">
   </div>
</div>
<link href="{{asset('assets/mobile/css/mobile_groupselect.css')}}" rel="stylesheet" type="text/css">
<script src="{{asset('assets/mobile/js/mobile_group_select.js')}}" type="text/javascript"></script>
<div data-role="page" id="_popup_group_categories_tree_sUID">
   <div data-role="content" data-theme="c" class="mobile-content-withList-grn">
      <div class="mobile_breadcrumbtitle_grn mobile_titlebar_content_withList_grn">
         <div class="mobile_breadcrumbtitle_left_grn"><a href="#" onclick="javascript:location.href = '#_sUID';">Attendees</a><span class="mobile_icon_breadcrumb_arrow_grn"></span></div>
         <div class="mobile_breadcrumbtitle_right_grn"><span class="mobile_icon_breadcrumb_grn mobile_app_schedule_s_b_grn"></span>Select organizations</div>
      </div>
      <ul data-role="listview" data-theme="c" class="mobile-ul-top-grn mobile-ul-bottom-grn mobile_folderlist_grn " id="group_categories_tree_sUID">
         <li id="sUIDparent_child_g_g1" data-icon="false" style="padding-left:0px !important;">
            <div id="sUIDg1" style="margin-left:0px !important;" class="mobile_folderlist_icon_arrowclose_grn mobile_folderlist_icon_grn mobile_folderlist_icon_size_grn"></div>
            <a href="#" class="mobile_folderlist_list_text_grn"><span id="sUIDdeepth_0_1" class="mobile_folderlist_text_overflow_grn">さいど株式会社</span></a>
            <div><span class="mobile_groupselect_icon_radiobuttonoff_grn"></span><span class="mobile_groupselect_radiobutton_grn"><input name="user_group_select" type="radio" value="g1"></span></div>
         </li>
         <li id="sUIDparent_child_g_g18" data-icon="false" style="padding-left:0px !important;">
            <a href="#" class="mobile_folderlist_list_text_grn"><span id="sUIDdeepth_0_18" class="mobile_folderlist_text_overflow_grn">Cyde America</span></a>
            <div><span class="mobile_groupselect_icon_radiobuttonoff_grn"></span><span class="mobile_groupselect_radiobutton_grn"><input name="user_group_select" type="radio" value="g18"></span></div>
         </li>
         <li id="sUIDparent_child_g_g21" data-icon="false" style="padding-left:0px !important;">
            <a href="#" class="mobile_folderlist_list_text_grn"><span id="sUIDdeepth_0_21" class="mobile_folderlist_text_overflow_grn">CydeChina</span></a>
            <div><span class="mobile_groupselect_icon_radiobuttonoff_grn"></span><span class="mobile_groupselect_radiobutton_grn"><input name="user_group_select" type="radio" value="g21"></span></div>
         </li>
         <li id="sUIDparent_child__-2" data-icon="false" style="padding-left:0px !important;">
            <a href="#" class="mobile_folderlist_list_text_grn"><span id="sUIDdeepth_0_-2" class="mobile_folderlist_text_overflow_grn">(Unassigned users)</span></a>
            <div><span class="mobile_groupselect_icon_radiobuttonoff_grn"></span><span class="mobile_groupselect_radiobutton_grn"><input name="user_group_select" type="radio" value="-2"></span></div>
         </li>
      </ul>
   </div>
   <!-- end of content-->
   <div class="mobile_button_area_fixed_grn">
      <div data-position="fixed" data-theme="c" data-tap-toggle="false" class="mobile_button_area_grn">
         <div class="mobile_ok_grn">
            <input type="submit" value="Apply" data-inline="true" data-theme="c"/>
         </div>
         <div class="mobile_cancel_grn">
            <input class="cancel_button" type="reset" value="Cancel" data-inline="true" data-theme="c"/>
         </div>
      </div>
   </div>
</div>
<script language="JavaScript" text="text/javascript">
   (function(){
   
       
       var settings = {
         asyncUrl      : '/api/ajax_get_sub_group_for_mobile?',
         paramName     : 'oid',
         treeName      : 'group_categories_tree_sUID',
         pageName      : 'schedule/mobile/repeat_add/user',
         previousPage  : '_sUID',
         container     : '_popup_group_categories_tree_sUID',
         radio_name    : 'user_group_select',
         prefix_id     : 'sUID',
         prefix_before_value : 'g',
         associate_value_element_back     : 'category_options_sUID',
         associate_title_element_back     : 'category_title_sUID'
       };
       
   
       $(document).ready(function(){
           var G = new grn.component.mobile_group_select(settings);
       });
   })();
</script>
<script src="{{asset('/js/resource-en.js')}}"></script>
<script language="JavaScript" text="text/javascript">
   (function(){
   
       
       var settings = {
           element_id  : 'p_sUID',
           ajaxGetItemURL : "/api/ajax_get_user_for_mobile_selection?",
           ajaxSearchItemURL : "/api/ajax_get_user_for_mobile_selection?",
           args        : "access_plugin=YToyOntzOjQ6Im5hbWUiO3M6ODoic2NoZWR1bGUiO3M6NjoicGFyYW1zIjthOjI6e3M6NjoiYWN0aW9uIjthOjE6e2k6MDtzOjQ6InJlYWQiO31zOjEyOiJzZXNzaW9uX25hbWUiO3M6MzE6InNjaGVkdWxlL21vYmlsZS9yZXBlYXRfYWRkL3ZpZXciO319&plugin_session_name=schedule/mobile/repeat_add&app_id=schedule",
           include_org : '1',
           isCalendar : '',
           show_group_role : '1',
           isAllowedRole : '',
           src_item    : '#src_user',
           dest_item   : '#dest_user',
           type_item   : 'user',
           empty_list  : "",
           none_selected : "None",
           search_caption: "(Search results)",
           group_select_menu_id : 'category_options_p_sUID',
           associate_id : 'default_public',
           selectedItem : {"list":{}},
           page_name : '',
           default_title_group_select : 'Select group.',
           categorySelectUI : 'user_category_p_sUID',
           categorySelectValue : 'category_title_p_sUID',
           popupItemCategory : 'popup_user_category_p_sUID'
       };
       
   
       $(document).ready(function(){
           var G = new grn.component.mobile_user_select(settings);
           G.initSelectedItem();
   
           $(document).on("pagebeforeshow","#_p_sUID",function(event, data){
               G.bindEventBeforePageShow();
   
               if(typeof data.prevPage.attr('id') == 'undefined')
               {
                   G.processing = false;
                   G.saveLastState();
                   G.resetSourceItemList();
                   G.resetCategorySelect();
               }
           });
   
           $(document).on("pagebeforehide","#_p_sUID",function(event, data){
               if(typeof data.nextPage.attr('id') == 'undefined' && !G.adding )
               {
                   G.cancel();
               }
               G.adding = false;
           });
       });
   
   })();
</script>
<script id="src_user" type="text/x-template">
   <li data-icon="false" data-corners="false" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-iconpos="right" data-theme="c">
       <a class="selection_item ui-btn" href="#">
           <div class="mobile_add_grn"></div>
           <div class="mobile_user_photo_grn mobile_img_userPlofile_grn"></div>
           <div class="mobile_info_grn">
               <div class="mobile_position_center_grn">
                   <div class="mobile_position_width_grn">
                       <div class="mobile_user_grn"></div>
                       <div class="mobile_text_grn"></div>
                   </div>
               </div>
           </div>
       </a>
   </li>
</script>
<script id="dest_user" type="text/x-template">
   <li data-icon="false" class="mobile_selected_grn" data-corners="false" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-iconpos="right" data-theme="c">
       <div class="mobile_delete_grn"></div>
       <a class="selection_item ui-btn" href="#">
           <div class="mobile_user_photo_grn mobile_img_userPlofile_grn"></div>
           <div class="mobile_info_grn">
               <div class="mobile_position_center_grn">
                   <div class="mobile_position_width_grn">
                       <div class="mobile_user_grn"></div>
                       <div class="mobile_text_grn"></div>
                   </div>
               </div>
           </div>
           <span></span>
       </a>
   </li>
</script>
<div data-role="page" id="_p_sUID">
   <div data-role="content" data-theme="c" class="mobile-content-grn">
      <div class="mobile_breadcrumbtitle_grn mobile_titlebar_content_withList_grn">
         <div class="mobile_breadcrumbtitle_left_grn"><span class="mobile_icon_breadcrumb_grn mobile_app_schedule_s_b_grn"></span><a href="#" >New appointment</a><span class="mobile_icon_breadcrumb_arrow_grn"></span></div>
         <div class="mobile_breadcrumbtitle_right_grn">Shared</div>
      </div>
      <div class="mobile_seletedUser_list_grn">
         <form action="#">
            <div class="mobile_user_search_grn">
               <input id="search_user" class="search_item" type="search" name="search_user_p_sUID" placeholder="User search"/>
               <a href="#" class="mobile_delete_icon_grn delete_input"></a>
               <a href="#" class="mobile_icon_grn search_item_icon"></a>
            </div>
         </form>
      </div>
      <div class="mobile_user_group_grn">
         <a id="user_category_p_sUID" href="#popup_user_category_p_sUID" data-rel="popup" data-transition="pop" data-shadow="true" data-wrapperels="span" class="mobile_select_view_grn">
         <span class="mobile_event_menu_content_grn">Select group.</span>
         <span class="mobile_select_icon_grn"></span>
         </a>
         <a class="ui-link-inherit" href="#_popup_group_categories_tree_p_sUID">
            <div class="mobile_group_icon_grn"></div>
         </a>
         <input type="hidden" value="" id="category_options_p_sUID">
         <input type="hidden" value="" id="category_title_p_sUID">
         <script language="JavaScript" text="text/javascript">
            (function(){
            
            
                var settings = {
                    selectMenu       : '#user_category_p_sUID',
                    selectMenuValue  : '#category_options_p_sUID',
                    container        : '#popup_user_category_p_sUID',
                    selectedValue    : 'Select group.',
                    expandMenuSelect : '#category_title_p_sUID',
                    selectMenuTitle  : 'Select group',
                    useMenuColor     : '',
                    defaultTitle     : '',
                    name             : 'mobileSelectMenu_popup_user_category_p_sUID'
                };
            
            
                $(document).ready(function(){
                    var G = new grn.component.mobile_select_menu(settings);
                    G.initSize =false;
            
                    $(document).on("popupbeforeposition","#popup_user_category_p_sUID",function(event){
                        if(!G.initSize || G.needResize)
                        {
                            G.setSizeSelectMenu(event);
                            G.initSize = true;
                        }
                    });
            
                    $('#popup_user_category_p_sUID').on('click',function(event){
                        G.setSizeSelectMenu(event);
                    });
            
                    $( window ).on( "resize", function( event ) {
                        var popup = $('#popup_user_category_p_sUID');
                        if(popup.is(':visible'))
                        {
                            G.setSizeSelectMenu(event);
                        }
                    });
                });
            
            })();
         </script>
         <div data-role="popup" id="popup_user_category_p_sUID" data-corners="false" data-overlay-theme="b" data-shadow="false">
            <div class="mobile_select_menu_titlebar_div_grn">
               <div class="mobile_titlebar_grn">
                  <span class="mobile_text_grn">Select group</span>
                  <a href="#" class="mobile_titlebar_right_grn mobile_colse_icon_grn"></a>
               </div>
               <ul data-role="listview" data-theme="c" class="mobile-ul-top-grn mobile_ul_grn select_menu_list">
                   @foreach($department as $key=>$val)
                   <li data-icon="false" class="mobile_list_grn ">
                     <span class="mobile_select_item "></span>
                     <a href="#" class="selection_item" data-value="{{$val->id}}">{{$val->name}}</a>
                   </li>
                   @endforeach
               </ul>
               <div class="mobile_select_button_area_grn">
                  <div class="mobile_cancel_grn">
                     <input class="cancel_button" type="reset" value="Close" data-inline="true" data-theme="c"/>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="mobile_user_label_area_grn">
         <span class="mobile-label-grn">Users, organizations, roles</span>
         <a class="add_all_to_selected_list mobile-label-grn" href="#">Select all</a>
      </div>
      <div class="mobile_user_list_scroll_grn mobile-user-list-none-grn">
         <div class="mobile_list_shadow_grn"></div>
         <div class="mobile_icon_grn">
            <div class="mobile_icon_up_grn"></div>
            <div class="mobile_icon_down_grn"></div>
         </div>
         <ul data-role="listview" data-theme="c" class="source_items mobile-ul-top-grn mobile-ul-bottom-grn mobile_user_list_grn">
            <li data-icon="false" class="mobile_base_disable_grn"></li>
         </ul>
      </div>
      <!--mobile_user_list_scroll_grn-->
      <div class="mobile_user_label_area_grn">
         <span class="mobile-label-grn">Selected</span>
         <a class="remove_all_from_selected_list mobile-label-grn" href="#" style="display:none;">Clear selection</a>
      </div>
      <div class="mobile_user_list_scroll_grn">
         <div class="mobile_list_shadow_grn"></div>
         <div class="order_selected_list mobile_order_icon_grn">
            <div class="mobile_order_control_grn order_top">
               <div class="mobile_order_top_grn"></div>
            </div>
            <div class="mobile_order_control_grn order_up">
               <div class="mobile_order_up_grn"></div>
            </div>
            <div class="mobile_order_control_grn order_down">
               <div class="mobile_order_down_grn"></div>
            </div>
            <div class="mobile_order_control_grn order_bottom">
               <div class="mobile_order_bottom_grn"></div>
            </div>
         </div>
         <ul data-role="listview" data-theme="c" class="selected_items mobile-ul-top-grn mobile-ul-bottom-grn mobile_user_list_grn">
         </ul>
      </div>
      <!--mobile_user_list_scroll_grn-->
      <div data-theme="c" class="mobile_button_area_grn">
         <div class="mobile_ok_grn">
            <input type="submit" value="Apply" data-inline="true" data-theme="c" />
         </div>
         <div class="mobile_cancel_grn">
            <input type="reset" value="Cancel" data-inline="true" data-theme="c"/>
         </div>
      </div>
      <input type="hidden" id="selected_groups_p_sUID" name="selected_groups_p_sUID" value="">
      <input type="hidden" id="selected_users_p_sUID" name="selected_users_p_sUID" value="">
   </div>
</div>
<div data-role="page" id="_popup_group_categories_tree_p_sUID">
   <div data-role="content" data-theme="c" class="mobile-content-withList-grn">
      <div class="mobile_breadcrumbtitle_grn mobile_titlebar_content_withList_grn">
         <div class="mobile_breadcrumbtitle_left_grn"><a href="#" onclick="javascript:location.href = '#_p_sUID';">Shared</a><span class="mobile_icon_breadcrumb_arrow_grn"></span></div>
         <div class="mobile_breadcrumbtitle_right_grn"><span class="mobile_icon_breadcrumb_grn mobile_app_schedule_s_b_grn"></span>Select organizations</div>
      </div>
      <ul data-role="listview" data-theme="c" class="mobile-ul-top-grn mobile-ul-bottom-grn mobile_folderlist_grn " id="group_categories_tree_p_sUID">
          @foreach($department as $key=>$val)
         <li id="p-sUIDparent_child_g_g1" data-icon="false" style="padding-left:0px !important;">
            <div id="p-sUIDg1" style="margin-left:0px !important;" class="mobile_folderlist_icon_arrowclose_grn mobile_folderlist_icon_grn mobile_folderlist_icon_size_grn"></div>
            <a href="#" class="mobile_folderlist_list_text_grn"><span id="p-sUIDdeepth_0_1" class="mobile_folderlist_text_overflow_grn">{{$val->name}}</span></a>
            <div><span class="mobile_groupselect_icon_radiobuttonoff_grn"></span><span class="mobile_groupselect_radiobutton_grn"><input name="user_group_select" type="radio" value="{{$val->id}}"></span></div>
         </li>
         @endforeach
      </ul>
   </div>
   <!-- end of content-->
   <div class="mobile_button_area_fixed_grn">
      <div data-position="fixed" data-theme="c" data-tap-toggle="false" class="mobile_button_area_grn">
         <div class="mobile_ok_grn">
            <input type="submit" value="Apply" data-inline="true" data-theme="c"/>
         </div>
         <div class="mobile_cancel_grn">
            <input class="cancel_button" type="reset" value="Cancel" data-inline="true" data-theme="c"/>
         </div>
      </div>
   </div>
</div>
<script language="JavaScript" text="text/javascript">
   (function(){
   
       
       var settings = {
         asyncUrl      : '/api/ajax_get_sub_group_for_mobile?',
         paramName     : 'oid',
         treeName      : 'group_categories_tree_p_sUID',
         pageName      : 'schedule/mobile/repeat_add/user',
         previousPage  : '_p_sUID',
         container     : '_popup_group_categories_tree_p_sUID',
         radio_name    : 'user_group_select',
         prefix_id     : 'p-sUID',
         prefix_before_value : 'g',
         associate_value_element_back     : 'category_options_p_sUID',
         associate_title_element_back     : 'category_title_p_sUID'
       };
       
   
       $(document).ready(function(){
           var G = new grn.component.mobile_group_select(settings);
       });
   })();
</script>
<script src="{{asset('assets/mobile/js/mobile_facility_select.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/resource-en.js')}}"></script>
<script language="JavaScript" text="text/javascript">
   (function(){
       var settings = {
           element_id  : 'sITEM',
           ajaxGetItemURL : "/scripts/garoon/grn.exe/schedule/mobile/ajax/accessible_facility?",
           ajaxSearchItemURL : "/scripts/garoon/grn.exe/schedule/mobile/ajax/search_facility?",
           src_item    : '#src_facility',
           dest_item   : '#dest_facility',
           type_item   : 'facility',
           empty_list  : "",
           none_selected : "None",
           search_caption: "(Search results)",
           group_select_menu_id : 'facility_category_options_sITEM',
           associate_id : 'facilities',
           selectedItem : {"list":null},
           page_name : 'schedule/mobile/repeat_add',
           default_title_group_select : 'Select group.',
           categorySelectUI : 'facility_category_sITEM',
           categorySelectValue : 'facility_category_title_sITEM',
           popupItemCategory : 'popup_facility_category_sITEM',
           defaultUsingPurpose : "",
           keepUsingPurpose : "",
       };
   
   
       $(document).ready(function(){
           var G = new grn.component.mobile_facility_select(settings);
           G.initSelectedItem();
           G.toggleUsingPurposeElement();
   
           $(document).on("pagebeforeshow","#_sITEM",function(event, data){
               G.bindEventBeforePageShow();
   
               if(typeof data.prevPage.attr('id') == 'undefined')
               {
                   G.processing = false;
                   G.saveLastState();
                   G.resetSourceItemList();
                   G.resetCategorySelect();
               }
           });
   
           $(document).on("pagebeforehide","#_sITEM",function(event, data){
               G.toggleUsingPurposeElement();
               if(typeof data.nextPage.attr('id') == 'undefined' && !G.adding )
               {
                   G.cancel();
               }
               G.adding = false;
           });
       });
   
   })();
</script>
<script id="src_facility" type="text/x-template">
   <li data-icon="false" data-corners="false" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-iconpos="right" data-theme="c">
       <a class="selection_item ui-btn" href="#">
           <div class="mobile_add_grn"></div>
           <div class="mobile_user_photo_grn mobile_img_facilityPlofile_grn"></div>
           <div class="mobile_info_grn">
               <div class="mobile_position_center_grn">
                   <div class="mobile_position_width_grn">
                       <div class="mobile_user_grn"></div>
                       <div class="mobile_text_grn"></div>
                   </div>
               </div>
           </div>
       </a>
   </li>
</script>
<script id="dest_facility" type="text/x-template">
   <li data-icon="false" class="mobile_selected_grn" data-corners="false" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-iconpos="right" data-theme="c">
       <div class="mobile_delete_grn"></div>
       <a class="selection_item ui-btn" href="#">
           <div class="mobile_user_photo_grn mobile_img_facilityPlofile_grn"></div>
           <div class="mobile_info_grn">
               <div class="mobile_position_center_grn">
                   <div class="mobile_position_width_grn">
                       <div class="mobile_user_grn"></div>
                       <div class="mobile_text_grn"></div>
                   </div>
               </div>
           </div>
           <span></span>
       </a>
   </li>
</script>
<div data-role="page" id="_sITEM">
   <div data-role="content" data-theme="c" class="mobile-content-grn">
      <div class="mobile_breadcrumbtitle_grn mobile_titlebar_content_withList_grn">
         <div class="mobile_breadcrumbtitle_left_grn"><a href="#" >New appointment</a><span class="mobile_icon_breadcrumb_arrow_grn"></span></div>
         <div class="mobile_breadcrumbtitle_right_grn">Facilities</div>
      </div>
      <div class="mobile_seletedUser_list_grn">
         <form action="#">
            <div class="mobile_user_search_grn">
               <input id="search_facility" class="search_item" type="search" name="search_facility" placeholder="Facility search"/>
               <a href="" class="mobile_delete_icon_grn delete_input"></a>
               <a href="#" class="mobile_icon_grn search_item_icon"></a>
            </div>
         </form>
      </div>
      <div class="mobile_user_group_grn">
         <a id="facility_category_sITEM" href="#popup_facility_category_sITEM" data-rel="popup" data-transition="pop" data-shadow="true" data-wrapperels="span" class="mobile_select_view_grn">
         <span class="mobile_event_menu_content_grn">Select group.</span>
         <span class="mobile_select_icon_grn"></span>
         </a>
         <a class="ui-link-inherit" href="#_popup_facility_categories_tree">
            <div class="mobile_group_icon_grn"></div>
         </a>
         <input type="hidden" value="" id="facility_category_options_sITEM">
         <input type="hidden" value="" id="facility_category_title_sITEM">
         <script language="JavaScript" text="text/javascript">
            (function(){
            
            
                var settings = {
                    selectMenu       : '#facility_category_sITEM',
                    selectMenuValue  : '#facility_category_options_sITEM',
                    container        : '#popup_facility_category_sITEM',
                    selectedValue    : 'Select group.',
                    expandMenuSelect : '#facility_category_title_sITEM',
                    selectMenuTitle  : 'Select group',
                    useMenuColor     : '',
                    defaultTitle     : '',
                    name             : 'mobileSelectMenu_popup_facility_category_sITEM'
                };
            
            
                $(document).ready(function(){
                    var G = new grn.component.mobile_select_menu(settings);
                    G.initSize =false;
            
                    $(document).on("popupbeforeposition","#popup_facility_category_sITEM",function(event){
                        if(!G.initSize || G.needResize)
                        {
                            G.setSizeSelectMenu(event);
                            G.initSize = true;
                        }
                    });
            
                    $('#popup_facility_category_sITEM').on('click',function(event){
                        G.setSizeSelectMenu(event);
                    });
            
                    $( window ).on( "resize", function( event ) {
                        var popup = $('#popup_facility_category_sITEM');
                        if(popup.is(':visible'))
                        {
                            G.setSizeSelectMenu(event);
                        }
                    });
                });
            
            })();
         </script>
         <div data-role="popup" id="popup_facility_category_sITEM" data-corners="false" data-overlay-theme="b" data-shadow="false">
            <div class="mobile_select_menu_titlebar_div_grn">
               <div class="mobile_titlebar_grn">
                  <span class="mobile_text_grn">Select group</span>
                  <a href="#" class="mobile_titlebar_right_grn mobile_colse_icon_grn"></a>
               </div>
               <ul data-role="listview" data-theme="c" class="mobile-ul-top-grn mobile_ul_grn select_menu_list">
                   @foreach($equipments as $key=>$val)
                  <li data-icon="false" class="mobile_list_grn ">
                     <span class="mobile_select_item "></span>
                     <a href="#" class="selection_item" data-value="x3">overseas branches (Recently selected facility group)</a>
                  </li>
                  @endforeach
               </ul>
               <div class="mobile_select_button_area_grn">
                  <div class="mobile_cancel_grn">
                     <input class="cancel_button" type="reset" value="Close" data-inline="true" data-theme="c"/>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="mobile_user_label_area_grn">
         <span class="mobile-label-grn">Facilities</span>
         <a class="add_all_to_selected_list mobile-label-grn" href="#">Select all</a>
      </div>
      <div class="mobile_user_list_scroll_grn mobile-user-list-none-grn">
         <div class="mobile_list_shadow_grn"></div>
         <div class="mobile_icon_grn">
            <div class="mobile_icon_up_grn"></div>
            <div class="mobile_icon_down_grn"></div>
         </div>
         <ul data-role="listview" data-theme="c" class="source_items mobile-ul-top-grn mobile-ul-bottom-grn mobile_user_list_grn">
            <li data-icon="false" class="mobile_base_disable_grn"></li>
         </ul>
      </div>
      <!--mobile_user_list_scroll_grn-->
      <div class="mobile_user_label_area_grn">
         <span class="mobile-label-grn">Selected facilities</span>
         <a class="remove_all_from_selected_list mobile-label-grn" href="#" style="display:none;">Clear selection</a>
      </div>
      <div class="mobile_user_list_scroll_grn">
         <div class="mobile_list_shadow_grn"></div>
         <div class="order_selected_list mobile_order_icon_grn">
            <div class="mobile_order_control_grn order_top">
               <div class="mobile_order_top_grn"></div>
            </div>
            <div class="mobile_order_control_grn order_up">
               <div class="mobile_order_up_grn"></div>
            </div>
            <div class="mobile_order_control_grn order_down">
               <div class="mobile_order_down_grn"></div>
            </div>
            <div class="mobile_order_control_grn order_bottom">
               <div class="mobile_order_bottom_grn"></div>
            </div>
         </div>
         <ul data-role="listview" data-theme="c" class="selected_items mobile-ul-top-grn mobile-ul-bottom-grn mobile_user_list_grn">
         </ul>
      </div>
      <!--mobile_user_list_scroll_grn-->
      <div data-theme="c" class="mobile_button_area_grn">
         <div class="mobile_ok_grn">
            <input type="submit" value="Apply" data-inline="true" data-theme="c" />
         </div>
         <div class="mobile_cancel_grn">
            <input type="reset" value="Cancel" data-inline="true" data-theme="c" data-disabled="false"/>
         </div>
      </div>
      <input type="hidden" id="sITEM" name="sITEM[]" value="">
      <input type="hidden" id="checkrepeat" name="checkrepeat" value="">
      <input type="hidden" id="approval" name="approval" value="">
   </div>
</div>
<div data-role="page" id="_popup_facility_categories_tree">
   <div data-role="content" data-theme="c" class="mobile-content-withList-grn">
      <div class="mobile_breadcrumbtitle_grn mobile_titlebar_content_withList_grn">
         <div class="mobile_breadcrumbtitle_left_grn"><a href="#" onclick="javascript:location.href = '#_sITEM';">Facilities</a><span class="mobile_icon_breadcrumb_arrow_grn"></span></div>
         <div class="mobile_breadcrumbtitle_right_grn">Select facility groups</div>
      </div>
      <ul data-role="listview" data-theme="c" class="mobile-ul-top-grn mobile-ul-bottom-grn mobile_folderlist_grn " id="facility_categories_tree">
         <li id="parent_child__1" data-icon="false" style="padding-left:0px !important;">
            <div id="1" style="margin-left:0px !important;" class="mobile_folderlist_icon_arrowclose_grn mobile_folderlist_icon_grn mobile_folderlist_icon_size_grn"></div>
            <a href="#" class="mobile_folderlist_list_text_grn"><span id="deepth_0_1" class="mobile_folderlist_text_overflow_grn">本社</span></a>
            <div><span class="mobile_groupselect_icon_radiobuttonoff_grn"></span><span class="mobile_groupselect_radiobutton_grn"><input name="facility_group_select" type="radio" value="1"></span></div>
         </li>
         <li id="parent_child__9" data-icon="false" style="padding-left:0px !important;">
            <a href="#" class="mobile_folderlist_list_text_grn"><span id="deepth_0_9" class="mobile_folderlist_text_overflow_grn">東京支店</span></a>
            <div><span class="mobile_groupselect_icon_radiobuttonoff_grn"></span><span class="mobile_groupselect_radiobutton_grn"><input name="facility_group_select" type="radio" value="9"></span></div>
         </li>
         <li id="parent_child__10" data-icon="false" style="padding-left:0px !important;">
            <a href="#" class="mobile_folderlist_list_text_grn"><span id="deepth_0_10" class="mobile_folderlist_text_overflow_grn">大阪支店</span></a>
            <div><span class="mobile_groupselect_icon_radiobuttonoff_grn"></span><span class="mobile_groupselect_radiobutton_grn"><input name="facility_group_select" type="radio" value="10"></span></div>
         </li>
         <li id="parent_child__11" data-icon="false" style="padding-left:0px !important;">
            <a href="#" class="mobile_folderlist_list_text_grn"><span id="deepth_0_11" class="mobile_folderlist_text_overflow_grn">福岡支店</span></a>
            <div><span class="mobile_groupselect_icon_radiobuttonoff_grn"></span><span class="mobile_groupselect_radiobutton_grn"><input name="facility_group_select" type="radio" value="11"></span></div>
         </li>
         <li id="parent_child__4" data-icon="false" style="padding-left:0px !important;">
            <a href="#" class="mobile_folderlist_list_text_grn"><span id="deepth_0_4" class="mobile_folderlist_text_overflow_grn">Video Conferencing System</span></a>
            <div><span class="mobile_groupselect_icon_radiobuttonoff_grn"></span><span class="mobile_groupselect_radiobutton_grn"><input name="facility_group_select" type="radio" value="4"></span></div>
         </li>
         <li id="parent_child__-2" data-icon="false" style="padding-left:0px !important;">
            <a href="#" class="mobile_folderlist_list_text_grn"><span id="deepth_0_-2" class="mobile_folderlist_text_overflow_grn">(Uncategorized facilities)</span></a>
            <div><span class="mobile_groupselect_icon_radiobuttonoff_grn"></span><span class="mobile_groupselect_radiobutton_grn"><input name="facility_group_select" type="radio" value="-2"></span></div>
         </li>
         <li id="parent_child__-3" data-icon="false" style="padding-left:0px !important;">
            <a href="#" class="mobile_folderlist_list_text_grn"><span id="deepth_0_-3" class="mobile_folderlist_text_overflow_grn">(All facilities)</span></a>
            <div><span class="mobile_groupselect_icon_radiobuttonoff_grn"></span><span class="mobile_groupselect_radiobutton_grn"><input name="facility_group_select" type="radio" value="-3"></span></div>
         </li>
      </ul>
   </div>
   <!-- end of content-->
   <div class="mobile_button_area_fixed_grn">
      <div data-position="fixed" data-theme="c" data-tap-toggle="false" class="mobile_button_area_grn">
         <div class="mobile_ok_grn">
            <input type="submit" value="Apply" data-inline="true" data-theme="c"/>
         </div>
         <div class="mobile_cancel_grn">
            <input class="cancel_button" type="reset" value="Cancel" data-inline="true" data-theme="c"/>
         </div>
      </div>
   </div>
</div>
<script language="JavaScript" text="text/javascript">
   (function(){
   
       
       var settings = {
         asyncUrl      : '/scripts/garoon/grn.exe/schedule/mobile/ajax/get_facility_sub_group?',
         paramName     : 'oid',
         treeName      : 'facility_categories_tree',
         pageName      : 'schedule/mobile/repeat_add/facility',
         previousPage  : '_sITEM',
         container     : '_popup_facility_categories_tree',
         radio_name    : 'facility_group_select',
         prefix_id     : '',
         prefix_before_value : '',
         associate_value_element_back     : 'facility_category_options_sITEM',
         associate_title_element_back     : 'facility_category_title_sITEM'
       };
       
   
       $(document).ready(function(){
           var G = new grn.component.mobile_group_select(settings);
       });
   })();
</script>
<script src="{{asset('assets/mobile/js/repeat_condition.js')}}" type="text/javascript"></script>
<script language="JavaScript" text="text/javascript">
   (function(){
   
       $(document).ready(function(){
   
           $(document).on("pagebeforeshow","#_repeat_condition",function(event,data){
               if(typeof data.prevPage.attr('id') == 'undefined')
               {
                   grn.page.schedule.mobile.repeat_condition.saveLastChoice();
               }
               else
               {
                   switch (data.prevPage.attr('id'))
                   {
                       case 'popup_menu_week':
                       case 'popup_menu_weekday':
                           grn.page.schedule.mobile.repeat_condition.selectRadioBtn('week');
                           break;
                       case 'popup_menu_monthday':
                           grn.page.schedule.mobile.repeat_condition.selectRadioBtn('3month');
                           break;
                   }
               }
           });
       });
   
   })();
</script>
<div data-role="page" id="_repeat_condition">
   <div data-role="content" data-theme="c" class="mobile-content-grn">
      <!--breadcrumb-->
      <div class="mobile_breadcrumbtitle_grn mobile_titlebar_content_withList_grn">
         <div class="mobile_breadcrumbtitle_left_grn"><span class="mobile_icon_breadcrumb_grn mobile_app_schedule_s_b_grn"><a href="#" onclick="javascript:location.href = '#';">New appointment</a><span class="mobile_icon_breadcrumb_arrow_grn"></span></div>
         <div class="mobile_breadcrumbtitle_right_grn">Repeating conditions</div>
      </div>
      <!--end breadcrumb-->
      <div class="mobile_schedule_grn mobile_repeating_conditions_grn">
         <div class="mobile_list_grn">
            <ul>
               <li class="mobile_schedule_repeat_everyday">
                  <span class="mobile_icon_radiobutton_grn mobile_icon_radiobuttonoff_grn"></span>
                  <span class="mobile_radiobutton_grn">
                  <input name="" id="day" value="day" type="radio" >
                  </span>
                  <span class="mobile_text_grn">Every day</span>
               </li>
               <li class="mobile_schedule_repeat_weekday">
                  <span class="mobile_icon_radiobutton_grn mobile_icon_radiobuttonoff_grn"></span>
                  <span class="mobile_radiobutton_grn">
                  <input name="" id="weekday" value="weekday" type="radio" >
                  </span>
                  <span class="mobile_text_grn">Every weekday</span>
               </li>
               <li class="mobile_schedule_repeat_week">
                  <span class="mobile_icon_radiobutton_grn mobile_icon_radiobuttonon_grn"></span>
                  <span class="mobile_radiobutton_grn">
                  <input name="" id="week" value="week" type="radio" check='checked'>
                  </span>
                  <span class="mobile_text_grn">
                     <a id="menu_week" href="#popup_menu_week" data-rel="popup" data-transition="pop" data-shadow="true" data-wrapperels="span" class="mobile_select_view_grn mobile_select_short_view_grn">
                     <span class="mobile_event_menu_content_grn">----------</span>
                     <span class="mobile_select_icon_grn"></span>
                     </a>
                     <input type="hidden" value="" name="week" id="menu_week_value">
                     <input type="hidden" value="" id="menu_week_title">
                     <script language="JavaScript" text="text/javascript">
                        (function(){
                        
                        
                            var settings = {
                                selectMenu       : '#menu_week',
                                selectMenuValue  : '#menu_week_value',
                                container        : '#popup_menu_week',
                                selectedValue    : 'week',
                                expandMenuSelect : '#menu_week_title',
                                selectMenuTitle  : '',
                                useMenuColor     : '',
                                defaultTitle     : '',
                                name             : 'mobileSelectMenu_popup_menu_week'
                            };
                        
                        
                            $(document).ready(function(){
                                var G = new grn.component.mobile_select_menu(settings);
                                G.initSize =false;
                        
                                $(document).on("popupbeforeposition","#popup_menu_week",function(event){
                                    if(!G.initSize || G.needResize)
                                    {
                                        G.setSizeSelectMenu(event);
                                        G.initSize = true;
                                    }
                                });
                        
                                $('#popup_menu_week').on('click',function(event){
                                    G.setSizeSelectMenu(event);
                                });
                        
                                $( window ).on( "resize", function( event ) {
                                    var popup = $('#popup_menu_week');
                                    if(popup.is(':visible'))
                                    {
                                        G.setSizeSelectMenu(event);
                                    }
                                });
                            });
                        
                        })();
                     </script>
                     <div data-role="popup" id="popup_menu_week" data-corners="false" data-overlay-theme="b" data-shadow="false">
                        <div class="mobile_select_menu_titlebar_div_grn">
                           <div class="mobile_titlebar_grn">
                              <span class="mobile_text_grn"></span>
                              <a href="#" class="mobile_titlebar_right_grn mobile_colse_icon_grn"></a>
                           </div>
                           <ul data-role="listview" data-theme="c" class="mobile-ul-top-grn mobile_ul_grn select_menu_list">
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item mobile_check_grn"></span>
                                 <a href="#" class="selection_item" data-value="week">Every week</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="1stweek">Every first</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="2ndweek">Every second</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="3rdweek">Every third</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="4thweek">Every fourth</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="lastweek">Every last</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn mobile_select_menu_last_item_grn">
                                 <span class="mobile_select_item"></span>
                                 <a href="#" data-value="">&nbsp</a>
                              </li>
                           </ul>
                           <div class="mobile_select_button_area_grn">
                              <div class="mobile_cancel_grn">
                                 <input class="cancel_button" type="reset" value="Close" data-inline="true" data-theme="c"/>
                              </div>
                           </div>
                        </div>
                     </div>
                     <a id="menu_weekday" href="#popup_menu_weekday" data-rel="popup" data-transition="pop" data-shadow="true" data-wrapperels="span" class="mobile_select_view_grn mobile_select_short_view_grn">
                     <span class="mobile_event_menu_content_grn">----------</span>
                     <span class="mobile_select_icon_grn"></span>
                     </a>
                     <input type="hidden" value="" name="wday" id="menu_weekday_value">
                     <input type="hidden" value="" id="menu_weekday_title">
                     <script language="JavaScript" text="text/javascript">
                        (function(){
                        
                        
                            var settings = {
                                selectMenu       : '#menu_weekday',
                                selectMenuValue  : '#menu_weekday_value',
                                container        : '#popup_menu_weekday',
                                selectedValue    : '1',
                                expandMenuSelect : '#menu_weekday_title',
                                selectMenuTitle  : '',
                                useMenuColor     : '',
                                defaultTitle     : '',
                                name             : 'mobileSelectMenu_popup_menu_weekday'
                            };
                        
                        
                            $(document).ready(function(){
                                var G = new grn.component.mobile_select_menu(settings);
                                G.initSize =false;
                        
                                $(document).on("popupbeforeposition","#popup_menu_weekday",function(event){
                                    if(!G.initSize || G.needResize)
                                    {
                                        G.setSizeSelectMenu(event);
                                        G.initSize = true;
                                    }
                                });
                        
                                $('#popup_menu_weekday').on('click',function(event){
                                    G.setSizeSelectMenu(event);
                                });
                        
                                $( window ).on( "resize", function( event ) {
                                    var popup = $('#popup_menu_weekday');
                                    if(popup.is(':visible'))
                                    {
                                        G.setSizeSelectMenu(event);
                                    }
                                });
                            });
                        
                        })();
                     </script>
                     <div data-role="popup" id="popup_menu_weekday" data-corners="false" data-overlay-theme="b" data-shadow="false">
                        <div class="mobile_select_menu_titlebar_div_grn">
                           <div class="mobile_titlebar_grn">
                              <span class="mobile_text_grn"></span>
                              <a href="#" class="mobile_titlebar_right_grn mobile_colse_icon_grn"></a>
                           </div>
                           <ul data-role="listview" data-theme="c" class="mobile-ul-top-grn mobile_ul_grn select_menu_list">
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="0">Sunday</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item mobile_check_grn"></span>
                                 <a href="#" class="selection_item" data-value="1">Monday</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="2">Tuesday</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="3">Wednesday</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="4">Thursday</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="5">Friday</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="6">Saturday</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn mobile_select_menu_last_item_grn">
                                 <span class="mobile_select_item"></span>
                                 <a href="#" data-value="">&nbsp</a>
                              </li>
                           </ul>
                           <div class="mobile_select_button_area_grn">
                              <div class="mobile_cancel_grn">
                                 <input class="cancel_button" type="reset" value="Close" data-inline="true" data-theme="c"/>
                              </div>
                           </div>
                        </div>
                     </div>
                  </span>
               </li>
               <li class="mobile_schedule_repeat_month">
                  <span class="mobile_icon_radiobutton_grn mobile_icon_radiobuttonoff_grn"></span>
                  <span class="mobile_radiobutton_grn">
                  <input name="" id="3month" value="month" type="radio" >
                  </span>
                  <span class="mobile_text_grn">
                     <span class="mobile_textmargin_grn">Every month</span>
                     <a id="menu_monthday" href="#popup_menu_monthday" data-rel="popup" data-transition="pop" data-shadow="true" data-wrapperels="span" class="mobile_select_view_grn mobile_select_short_view_grn">
                     <span class="mobile_event_menu_content_grn">----------</span>
                     <span class="mobile_select_icon_grn"></span>
                     </a>
                     <input type="hidden" value="" name="day" id="menu_monthday_value">
                     <input type="hidden" value="" id="menu_monthday_title">
                     <script language="JavaScript" text="text/javascript">
                        (function(){
                        
                        
                            var settings = {
                                selectMenu       : '#menu_monthday',
                                selectMenuValue  : '#menu_monthday_value',
                                container        : '#popup_menu_monthday',
                                selectedValue    : '25',
                                expandMenuSelect : '#menu_monthday_title',
                                selectMenuTitle  : '',
                                useMenuColor     : '',
                                defaultTitle     : '',
                                name             : 'mobileSelectMenu_popup_menu_monthday'
                            };
                        
                        
                            $(document).ready(function(){
                                var G = new grn.component.mobile_select_menu(settings);
                                G.initSize =false;
                        
                                $(document).on("popupbeforeposition","#popup_menu_monthday",function(event){
                                    if(!G.initSize || G.needResize)
                                    {
                                        G.setSizeSelectMenu(event);
                                        G.initSize = true;
                                    }
                                });
                        
                                $('#popup_menu_monthday').on('click',function(event){
                                    G.setSizeSelectMenu(event);
                                });
                        
                                $( window ).on( "resize", function( event ) {
                                    var popup = $('#popup_menu_monthday');
                                    if(popup.is(':visible'))
                                    {
                                        G.setSizeSelectMenu(event);
                                    }
                                });
                            });
                        
                        })();
                     </script>
                     <div data-role="popup" id="popup_menu_monthday" data-corners="false" data-overlay-theme="b" data-shadow="false">
                        <div class="mobile_select_menu_titlebar_div_grn">
                           <div class="mobile_titlebar_grn">
                              <span class="mobile_text_grn"></span>
                              <a href="#" class="mobile_titlebar_right_grn mobile_colse_icon_grn"></a>
                           </div>
                           <ul data-role="listview" data-theme="c" class="mobile-ul-top-grn mobile_ul_grn select_menu_list">
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="1">1st</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="2">2nd</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="3">3rd</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="4">4th</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="5">5th</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="6">6th</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="7">7th</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="8">8th</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="9">9th</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="10">10th</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="11">11th</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="12">12th</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="13">13th</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="14">14th</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="15">15th</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="16">16th</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="17">17th</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="18">18th</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="19">19th</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="20">20th</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="21">21st</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="22">22nd</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="23">23rd</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="24">24th</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item mobile_check_grn"></span>
                                 <a href="#" class="selection_item" data-value="25">25th</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="26">26th</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="27">27th</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="28">28th</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="29">29th</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="30">30th</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="31">31st</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn ">
                                 <span class="mobile_select_item "></span>
                                 <a href="#" class="selection_item" data-value="0">Last day</a>
                              </li>
                              <li data-icon="false" class="mobile_list_grn mobile_select_menu_last_item_grn">
                                 <span class="mobile_select_item"></span>
                                 <a href="#" data-value="">&nbsp</a>
                              </li>
                           </ul>
                           <div class="mobile_select_button_area_grn">
                              <div class="mobile_cancel_grn">
                                 <input class="cancel_button" type="reset" value="Close" data-inline="true" data-theme="c"/>
                              </div>
                           </div>
                        </div>
                     </div>
                  </span>
               </li>
            </ul>
         </div>
      </div>
      <div data-theme="c" class="mobile_button_area_grn">
         <div class="mobile_ok_grn">
            <input type="submit" value="Save" data-inline="true" data-theme="c" />
         </div>
         <div class="mobile_cancel_grn">
            <input type="reset" value="Cancel" data-inline="true" data-theme="c"/>
         </div>
      </div>
   </div>
   <!-- end of content-->
   <input type="hidden" value="week" name="type" id="repeat_condition_type">
   
</div>
<!--page-->
</div>
@stop