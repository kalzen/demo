$.ajaxSetup({
        headers: {
          'Authorization': 'Bearer ' + $('meta[name="api_token"]').attr('content'),
        }
});
$('body').delegate('.modal-create-member','click',function(){    
    $('#modal_create_member').modal('show');
})
function getFile() {
  document.getElementById("upfile").click();
}
function getFile1() {
  document.getElementById("upfile1").click();
}
function sub(obj) {
    var file = obj.value;
    var file_data = obj.files[0];
    $this=$(this);
    var form_data = new FormData();
    var $input = $('.image_data');
    form_data.append('file', file_data);
    $.ajax({
        url: '/api/uploadImage',
        method: 'POST',
        data: form_data,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(response){
            if(response.success == true){
                $input.val(response.image);
                $('.image-avatar').attr('style','background-image:url('+response.image+')');
            }else{
                alert('File upload không hợp lệ');
            }

        }
     });
}
    $('.submit-create-member').click(function(e){
        e.preventDefault();
        var department_id = $.trim($('#validate_department_id').val());
        var position_id = $.trim($('#validate_position_id').val());
        var login_id = $.trim($('#validate_login_id').val());
        var level = $.trim($('#validate_level').val());
        var full_name = $.trim($('#validate_full_name').val());
        var password = $.trim($('#validate_password').val());
        var check = true;
        if(department_id == ''){
            check = false;
            $('#error_department').html('Trường bắt buộc không được để trống');
        }else{
            $('#error_department').html('');
        }
        if(position_id == ''){
            check = false;
            $('#error_position').html('Trường bắt buộc không được để trống');
        }else{
            $('#error_position').html('');
        }
        if(login_id == ''){
            check = false;
            $('#error_login').html('Trường bắt buộc không được để trống');
        }else{
            $('#error_login').html('');
        }
        if(level == ''){
            check = false;
            $('#error_level').html('Trường bắt buộc không được để trống');
        }else{
            $('#error_level').html('');
        }
        if(full_name == ''){
            check = false;
            $('#error_full_name').html('Trường bắt buộc không được để trống');
        }else{
            $('#error_full_name').html('');
        }
        if(password == ''){
            check = false;
            $('#error_password').html('Trường bắt buộc không được để trống');
        }else{
            $('#error_password').html('');
        }
        if( $('.password').val() !== $('.confirm-password').val()){
             check = false;
        }
        if(check === true){
            $('#create_member').submit();
        }else{
            e.preventDefault();
        }
    })
function sub(obj) {
    var file = obj.value;
    var file_data = obj.files[0];
    $this=$(this);
    var form_data = new FormData();
    var $input = $('.image_data');
    form_data.append('file', file_data);
    $.ajax({
        url: '/api/uploadImage',
        method: 'POST',
        data: form_data,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(response){
            if(response.success == true){
                $input.val(response.image);
                $('.image-avatar').attr('style','background-image:url('+response.image+')');
            }else{
                alert('File upload không hợp lệ');
            }

        }
     });
}
    /*$('.submit-create-member').click(function(){
        if($('.password').val() === $('.confirm-password').val()){
            $('#create_member').submit();
            var notification = notifier.notify("success", "Thêm thành viên thành công");
            notification.push();
        }else{
            var notifier = new Notifier();
            var notification = notifier.notify("warning", "Xác nhận MK không trùng khớp, mới nhập lại");
            notification.push();
        }
    })*/
    $('.submit-edit-member').click(function(){
        if($('.edit-password').val() === $('.edit-confirm-password').val()){
            $('#update_member').submit();
        }else{
            var notifier = new Notifier();
            var notification = notifier.notify("warning", "Xác nhận MK không trùng khớp, mới nhập lại");
            notification.push();
        }
    })
    $("#update_member").submit(function(e) {
        e.preventDefault();
        var form = $(this);
        $.ajax({
               type: "POST",
               url: '/api/update-member',
               data: form.serialize(),
               success: function(response){
                    if(response.success == true){
                        var notifier = new Notifier();
                        var notification = notifier.notify("success", "Sửa thông tin thành công");
                        notification.push();
                        //setTimeout(function(){ location.reload(); }, 2000);
                    }
                }
        });   
    });
    $(document).ready(function() {
        $('.select-search').select2({
            allowClear: true,
        });
    });
    $(document).ready(function() {
        $('.select').select2({
            allowClear: true,
            minimumResultsForSearch: Infinity
        });
    });
    $('.old-password').blur(function(){
        var password = $(this).val();
        $this=$(this);
        $.ajax({
            url:'/api/check-password',
            method:'POST',
            data:{password:password},
            success:function(response){
                if(response.success == 'false' && !$this.parent().find('.invalid-feedback').length){
                    $this.parent().append(`<div class="invalid-feedback">
                                                Mật khẩu không đúng
                                            </div>`);
                }else if(response.success == 'true' && $this.parent().find('.invalid-feedback').length){
                    $this.parent().find('.invalid-feedback').remove();
                }
            }
        })
    })
    $('.confirm-news-password,.news-password').blur(function(){
        $this=$(this);
        var str = $(this).val();
        if(str.match(/^(?=.*[0-9])(?=.*[a-z])([a-zA-Z0-9]{6,20})$/) == null && !$this.parent().find('.invalid-feedback').length){
            $this.parent().append(`<div class="invalid-feedback">
                                                Mật khẩu tối thiểu 6 ký tự bao gồm cả chữ số
                                            </div>`);
        }else if(str.match(/^(?=.*[0-9])(?=.*[a-z])([a-zA-Z0-9]{6,})$/) != null && $this.parent().find('.invalid-feedback').length){
            $this.parent().find('.invalid-feedback').remove();
        }
    })
    $('.confirm-news-password').blur(function(){
        if($(this).val() != $('.news-password').val()){
            $this.parent().append(`<div class="invalid-feedback">
                Nhập lại không đúng
            </div>`);
        }else{
            $this.parent().find('.invalid-feedback').remove();
        }
    });
    $('#update_avatar').submit(function(e){
        e.preventDefault();
        $this = $(this);
        $.ajax({
            url:'/api/update-avatar',
            method:'POST',
            data: new FormData(this),
            contentType:false,
            processData:false,
            dataType: 'json',
            success:function(response){
                if(response.success == 'true'){
                    var notifier = new Notifier();
                    var notification = notifier.notify("success", "Cập nhật ảnh đại diện thành công");
                    notification.push();
                    $this[0].reset();
                    $('#modal_update_avatar').modal('hide');
                }
            }
        })
    })
    $('#reset_password').submit(function(e){
        e.preventDefault();
        $this = $(this);
        $.ajax({
            url:'/api/reset-password',
            method:'POST',
            data: new FormData(this),
            contentType:false,
            processData:false,
            dataType: 'json',
            success:function(response){
                if(response.success == 'true'){
                    var notifier = new Notifier();
                    var notification = notifier.notify("success", "Thay đổi mật khẩu thành công");
                    notification.push();
                    $this[0].reset();
                    $('#modal_reset_password').modal('hide');
                }
            }
        })
    })
if($('.upload-images').length){
    $('.upload-images').each(function(){
        var images = $(this).parents('.div-image').find('.image_data').val();
        if(images === ''){
        }else{
            images = images.split(',');
            for(i=0;i < images.length;i++){
               var name = images[i];
               name = name.split('/');
               $(this).parents('.div-image').find('.file-drop-zone').append('<div class="file-preview-frame krajee-default  file-preview-initial file-sortable kv-preview-thumb">'+
                                                                                          '<div class="kv-file-content">'+
                                                                                                       '<img src="'+images[i]+'" class="file-preview-image kv-preview-data" title="'+name[4]+'" alt="'+name[4]+'" style="width:auto;height:auto;max-width:100%;max-height:100%;">'+
                                                                                           '</div>'+
                                                                                           '<div class="file-thumbnail-footer">'+
                                                                                           '<div class="file-footer-caption" title="'+name[4]+'">'+
                                                                                               '<div class="file-caption-info">'+name[4]+'</div>'+
                                                                                               '<div class="file-size-info"></div>'+
                                                                                           '</div>'+
                                                                                           '<div class="file-actions">'+
                                                                                               '<div class="file-footer-buttons">'+
                                                                                                     '<button type="button" class="kv-file-remove btn btn-link btn-icon btn-xs" title="Remove file" data-url="" data-key="1"><i class="icon-trash"></i></button>'+
                                                                                                     '<button type="button" class="kv-file-zoom btn btn-link btn-xs btn-icon" title="View Details"><i class="icon-zoomin3"></i></button>'+
                                                                                                 '</div>'+
                                                                                           '</div>'+
                                                                                       '</div>'+
                                                                                       '</div>');
            }
        }
    });

};
$('body').delegate('.upload-images','change', function() {
    $this = $(this);
    var file_data = [];
    var form_data = new FormData();
    var $input = $(this).parents('.div-image').find('.image_data');
    var images = $(this).parents('.div-image').find('.image_data').val();
    if(images === ''){
        images=[];
    }else{
        images = images.split(',');
    }
    for (var i = 0; i < $(this)[0].files.length; i++) {
        form_data.append('file[]', $(this).prop('files')[i]);
    }
    $.ajax({
        url: '/api/upload',
        method: 'POST',
        data: form_data,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(response){
            if(response.success == true){
                for(var i=0;i < response.image.length;i++ ){
                    $this.parents('.div-image').find('.file-drop-zone').append('<div class="file-preview-frame krajee-default  file-preview-initial file-sortable kv-preview-thumb">'+
                                                                                       '<div class="kv-file-content">'+
                                                                                                    '<img src="'+response.image[i]+'" class="file-preview-image kv-preview-data" title="'+response.name[i]+'" alt="'+response.name[i]+'" style="width:auto;height:auto;max-width:100%;max-height:100%;">'+
                                                                                        '</div>'+
                                                                                        '<div class="file-thumbnail-footer">'+
                                                                                        '<div class="file-footer-caption" title="'+response.name[i]+'">'+
                                                                                            '<div class="file-caption-info">'+response.name[i]+'</div>'+
                                                                                            '<div class="file-size-info"></div>'+
                                                                                        '</div>'+
                                                                                        '<div class="file-actions">'+
                                                                                            '<div class="file-footer-buttons file-button">'+
                                                                                                  '<button type="button" class="kv-file-remove btn btn-link btn-icon btn-xs" title="Remove file" data-url="" data-key="1"><i class="icon-trash"></i></button>'+

                                                                                              '</div>'+
                                                                                        '</div>'+
                                                                                    '</div>'+
                                                                                    '</div>');
                    images.push(response.image[i]);
                    var res = images.join(',');
                    $input.val(res);
                }
            }else{
                alert('File upload không hợp lệ');
            }
            }
        })
 });
 function dropHandler(ev) {
        $this = ev.target;
        var file_data = [];
        var form_data = new FormData();
        var $input = $this.parentElement.parentElement.parentElement.getElementsByClassName('image_data')[0];
        var images = $this.parentElement.parentElement.parentElement.getElementsByClassName('image_data')[0].value;
        if(images === ''){
            images=[];
        }else{
            images = images.split(',');
        }
        var form_data = new FormData();
        ev.preventDefault();
        if (ev.dataTransfer.items) {
          for (var i = 0; i < ev.dataTransfer.items.length; i++) {
            if (ev.dataTransfer.items[i].kind === 'file') {
              var file = ev.dataTransfer.items[i].getAsFile();
              form_data.append('file[]', file);
            }
          }
          $.ajax({
            url: '/api/upload',
            method: 'POST',
            data: form_data,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(response){
                if(response.success == true){
                    for(var i=0;i < response.image.length;i++ ){
                        html = '<div class="file-preview-frame krajee-default  file-preview-initial file-sortable kv-preview-thumb">'+
                                                                                           '<div class="kv-file-content">'+
                                                                                                        '<img src="'+response.image[i]+'" class="file-preview-image kv-preview-data" title="'+response.name[i]+'" alt="'+response.name[i]+'" style="width:auto;height:auto;max-width:100%;max-height:100%;">'+
                                                                                            '</div>'+
                                                                                            '<div class="file-thumbnail-footer">'+
                                                                                            '<div class="file-footer-caption" title="'+response.name[i]+'">'+
                                                                                                '<div class="file-caption-info">'+response.name[i]+'</div>'+
                                                                                                '<div class="file-size-info"></div>'+
                                                                                            '</div>'+
                                                                                            '<div class="file-actions">'+
                                                                                                '<div class="file-footer-buttons file-button">'+
                                                                                                      '<button type="button" class="kv-file-remove btn btn-link btn-icon btn-xs" title="Remove file" data-url="" data-key="1"><i class="icon-trash"></i></button>'+

                                                                                                  '</div>'+
                                                                                            '</div>'+
                                                                                        '</div>'+
                                                                                        '</div>';
                        $this.innerHTML += html;
                        images.push(response.image[i]);
                        var res = images.join(',');
                        $input.value = res;
                    }
                }else{
                    alert('File upload không hợp lệ');
                }
                }
            })
        } else {
          for (var i = 0; i < ev.dataTransfer.files.length; i++) {
            console.log('... file[' + i + '].name = ' + ev.dataTransfer.files[i].name);
          }
        }
    }
 $('body').delegate('.upload-image', 'change', function () {
    var file_data = $(this).prop('files')[0];
    $this = $(this);
    var form_data = new FormData();
    var $input = $(this).parents('.div-image').find('.image_data');
    form_data.append('file', file_data);
    $.ajax({
        url: '/api/uploadImage',
        method: 'POST',
        data: form_data,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function (response) {
            if (response.success == true) {
                $input.val(response.image);
                if (response.type === 1) {
                    $this.parents('.div-image').find('.file-drop-zone').html('<div class="file-preview-frame krajee-default  file-preview-initial file-sortable kv-preview-thumb">'+
                                                                                   '<div class="kv-file-content">'+
                                                                                                '<img src="'+response.image+'" class="file-preview-image kv-preview-data" title="'+response.name+'" alt="'+response.name+'" style="width:auto;height:auto;max-width:100%;max-height:100%;">'+
                                                                                    '</div>'+
                                                                                    '<div class="file-thumbnail-footer">'+
                                                                                    '<div class="file-footer-caption" title="'+response.name+'">'+
                                                                                        '<div class="file-caption-info">'+response.name+'</div>'+
                                                                                        '<div class="file-size-info"></div>'+
                                                                                    '</div>'+
                                                                                    
                                                                                '</div>'+
                                                                                '</div>');
                }else{
                    $this.parents('.div-image').find('.file-drop-zone').html('<div class="file-preview-frame krajee-default  file-preview-initial file-sortable kv-preview-thumb">'+
                                                                                   '<div class="kv-file-content">'+
                                                                                                '<img src="/img/file-icon.png" data-link="'+response.image+'" class="not-image file-preview-image kv-preview-data" title="'+response.name+'" alt="'+response.name+'" style="width:auto;height:auto;max-width:100%;max-height:100%;">'+
                                                                                    '</div>'+
                                                                                    '<div class="file-thumbnail-footer">'+
                                                                                    '<div class="file-footer-caption" title="'+response.name+'">'+
                                                                                        '<div class="file-caption-info">'+response.name+'</div>'+
                                                                                        '<div class="file-size-info"></div>'+
                                                                                    '</div>'+
                                                                                    
                                                                                '</div>'+
                                                                                '</div>');
                }
                $input.val(response.image);
            } else {
                alert('File upload không hợp lệ');
            }

        }
    });
});
if ($('.upload-image').length) {
    $('.upload-image').each(function () {
        var images = $(this).parents('.div-image').find('.image_data').val();
        console.log(images);
        if (images === '') {
        } else {
            images = images.split(',');
            for (i = 0; i < images.length; i++) {
                var name = images[i];
                name = name.split('/');
                $(this).parents('.div-image').find('.file-drop-zone').append('<div class="file-preview-frame krajee-default  file-preview-initial file-sortable kv-preview-thumb">' +
                    '<div class="kv-file-content">' +
                    '<img src="' + images[i] + '" class="file-preview-image kv-preview-data" title="' + name[1] + '" alt="' + name[1] + '" style="width:auto;height:auto;max-width:100%;max-height:100%;">' +
                    '</div>' +
                    '<div class="file-thumbnail-footer">' +
                    '<div class="file-footer-caption" title="' + name[1] + '">' +
                    '<div class="file-caption-info">' + name[1] + '</div>' +
                    '<div class="file-size-info"></div>' +
                    '</div>' +
                    '<div class="file-actions">' +
                    '<div class="file-footer-buttons">' +
                    '<button type="button" class="kv-file-remove btn btn-link btn-icon btn-xs" title="Remove file" data-url="" data-key="1"><i class="icon-trash"></i></button>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>');
            }
        }
    });
};
 $('body').delegate('.kv-file-remove,.fileinput-remove','click',function(){
   var image = $(this).parents('.file-preview-frame').find('.file-preview-image').attr("src");
    $.ajax({
       url: '/api/delete_image',
       method: 'POST',
       data: {link:image},
       success: function(response){
           if(response.success == true){
           }
       }
    });
   var $input = $(this).parents('.div-image').find('.image_data');
   var images = $(this).parents('.div-image').find('.image_data').val();
   images = images.split(',');
   var index = images.indexOf(image);
   if (index > -1) {
     images.splice(index, 1);
   }
   var res = images.join(',');
   $input.val(res);
   $(this).parents('.file-preview-frame').remove();
});
$('.show-hidden').click(function(){
    if($(this).hasClass('show')){
        $(this).parents('.dashboard').find('.dashboardContent').hide(300);
        $(this).html('<i class="fas fa-chevron-down"></i>')
        $(this).removeClass('show');
    }else{
        $(this).parents('.dashboard').find('.dashboardContent').show(300);
        $(this).html('<i class="fas fa-chevron-up"></i>');
        $(this).addClass('show');
    }
})



