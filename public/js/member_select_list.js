!function($) {
    "use strict";
    function get_full_list_name(list_name) {
        return "selectlist_" + list_name
    }
    if (!grn.base.isNamespaceDefined("grn.component.member_select_list")) {
        grn.base.namespace("grn.component.member_select_list");
        var member_select_list = grn.component.member_select_list
          , default_options = {
            selectedClassName: "selectlist_selected_grn",
            isCalendar: false,
            isCurrentScreen: true,
            isFacilityCandidate: false
        }
          , instances = {};
        member_select_list.MEMBER_TYPES = {
            USER: "user",
            GROUP: "group",
            ROLE: "static_role",
            DYNAMIC_ROLE: "dynamic_role",
            OMITTED: "omitted"
        },
        member_select_list.OMITTED_ID = 0;
        var PREFIXES = {
            user: "",
            group: "g",
            static_role: "r",
            dynamic_role: "dr",
            omitted: ""
        }
          , SCROLL_INTERVAL = 50
          , MEMBER_HEIGHT_PX = 18;
        member_select_list.MemberSelectList = function(list_name, options) {
            this.shortListName = list_name,
            this.listName = get_full_list_name(list_name),
            options = $.extend({}, default_options, options),
            this.selectedClassName = options.selectedClassName,
            this.isCalendar = options.isCalendar,
            this.isCurrentScreen = options.isCurrentScreen,
            this.isFacilityCandidate = options.isFacilityCandidate,
            this.lastSelectedId = null,
            this.lastRangedId = null,
            this.isMouseDown = null,
            this.isCandidateList = options.isCandidateList || false,
            this.memberAddName = options.memberAddName,
            this.isTouchSupported = "ontouchstart"in window || navigator.maxTouchPoints,
            this.isScrollToAdd = options.isScrollToAdd || false,
            this.isNeedScroll = options.isNeedScroll || false,
            this.parameterForGetMoreMember = options.parameterForGetMoreMember,
            this.isLoading = false,
            this.ajaxGetMemberListUrl = options.ajaxGetMemberListUrl;
            var self = this;
            $(function() {
                self.init()
            }),
            instances[list_name] = this
        }
        ,
        member_select_list.get_instance = function(name) {
            return instances[name]
        }
        ,
        member_select_list.MemberSelectList.prototype = {
            init: function() {
                var select_list = this;
                this.$ul_list = $("#ul_" + this.listName),
                this.$select_box = this.jQuery()("#selectlist_base_" + this.listName),
                this.$select_all = $("#select_all_" + this.listName),
                this.$un_select_all = $("#un_select_all_" + this.listName),
                this.$ul_list.on("mousedown", "li", function(event) {
                    return select_list.clickMember($(this), event),
                    select_list.setMouseDownState(),
                    select_list.isFacilityCandidate && select_list.updateAncestorsPath(),
                    !event.shiftKey
                }),
                $(document).on("mouseup", function() {
                    select_list.setMouseUpState()
                }),
                
                this.$ul_list.on("mouseenter", "li", function() {
                    select_list.tracingSelect($(this))
                }),
                this.$select_all.on("click", function() {
                    select_list.isScrollToAdd && !select_list.$select_box.hasClass("selectlist_l_grn") ? select_list._getAllMembers("select_all") : (select_list.selectAllMembers(),
                    select_list.viewUnSelectAllLink()),
                    select_list.isFacilityCandidate && select_list.updateAncestorsPath()
                }),
                this.$un_select_all.on("click", function() {
                    select_list.unSelectAllMembers(),
                    select_list.viewSelectAllLink(),
                    select_list.isFacilityCandidate && select_list.updateAncestorsPath()
                }),
                this.$select_box.on("keydown", function(event) {
                    var result = select_list.keyDown(event);
                    return select_list.isFacilityCandidate && select_list.updateAncestorsPath(),
                    result
                }),
                this.isScrollToAdd && this.isNeedScroll && !this.$select_box.hasClass("selectlist_l_grn") && this.$select_box.on("scroll", function() {
                    var viewH = $(this).height(), contentH, scrollTop;
                    $(this).get(0).scrollHeight - viewH - $(this).scrollTop() <= 1 && select_list.loadMoreMember()
                }),
                setInterval(function() {
                    select_list.scrollSelectBox()
                }, 50)
            },
            setMouseDownState: function() {
                this.isMouseDown = true;
                var select_list = this;
                this.mouseYFromList = this.$select_box.offset().top,
                $(document).on("mousemove.select_list", function(event) {
                    return select_list.mouseYFromList = event.pageY,
                    false
                })
            },
            setMouseUpState: function() {
                this.isMouseDown = false,
                this.mouseYFromList = this.$select_box.offset().top,
                $(document).off("mousemove.select_list")
            },
            scrollSelectBox: function() {
                if (this.isMouseDown) {
                    var scroll_position = this.$select_box.scrollTop()
                      , offset = this.$select_box.offset().top
                      , scroll_speed = 0
                      , MARGIN = 8
                      , select_box_height = this.$select_box.prop("clientHeight");
                    this.mouseYFromList < offset && (scroll_speed = this.mouseYFromList - offset),
                    this.mouseYFromList > offset + select_box_height - 8 && (scroll_speed = this.mouseYFromList - offset - select_box_height),
                    scroll_speed = 18 * Math.ceil(scroll_speed / 18),
                    this.$select_box.scrollTop(scroll_position + scroll_speed)
                }
            },
            orderTop: function() {
                var $selected = $("." + this.listName + "." + this.selectedClassName);
                $selected.remove(),
                this.$ul_list.prepend($selected),
                this.scrollToTop()
            },
            orderUp: function() {
                var selected_class_name = this.selectedClassName
                  , $first_child = this.$ul_list.find("li:first-child");
                if ($first_child.length && !($first_child.attr("class").indexOf(selected_class_name) >= 0)) {
                    var $selected_list = $("." + this.listName + "." + selected_class_name);
                    $.each($selected_list, function(i, selected) {
                        var $prev = $(selected).prev().not("." + selected_class_name);
                        $prev && $(selected).insertBefore($prev)
                    }),
                    this.scrollToLastSelectedMember()
                }
            },
            orderDown: function() {
                var selected_class_name = this.selectedClassName
                  , $last_child = this.$ul_list.find("li:last-child");
                if ($last_child.length && !($last_child.attr("class").indexOf(selected_class_name) >= 0)) {
                    var $selected_list = $("." + this.listName + "." + selected_class_name).get().reverse();
                    $.each($selected_list, function(i, selected) {
                        var $next = $(selected).next().not("." + selected_class_name);
                        $next && $(selected).insertAfter($next)
                    }),
                    this.scrollToLastSelectedMember()
                }
            },
            orderBottom: function() {
                var $selected = $("." + this.listName + "." + this.selectedClassName);
                $selected.remove(),
                this.$ul_list.append($selected),
                this.scrollToBottom()
            },
            scrollToTop: function() {
                this.$select_box.scrollTop(0)
            },
            scrollToBottom: function() {
                this.$select_box.scrollTop(this.$ul_list.height())
            },
            scrollToLastSelectedMember: function() {
                if (this.lastSelectedId) {
                    var $lastSelectedMember = $("#" + this.lastSelectedId);
                    $lastSelectedMember.length && this._scrollToMember($lastSelectedMember)
                }
            },
            loadMoreMember: function() {
                var request;
                this.isLoading || (this.isLoading = true,
                this.spinnerScrollOn(),
                new grn.component.ajax.request({
                    grnUrl: this.ajaxGetMemberListUrl,
                    method: "post",
                    dataType: "json",
                    data: this.parameterForGetMoreMember
                }).send().done(function(json_obj) {
                    this.appendMembers(json_obj.members_info),
                    json_obj.is_need_scroll || this.$select_box.unbind("scroll"),
                    this.setOffset()
                }
                .bind(this)).fail(function() {
                    this.$select_box.unbind("scroll")
                }
                .bind(this)).always(function() {
                    this.spinnerScrollOff(),
                    this.isLoading = false
                }
                .bind(this)))
            },
            setOffset: function() {
                this.parameterForGetMoreMember.more_user_offset = parseInt(this.parameterForGetMoreMember.more_user_offset) + parseInt(this.parameterForGetMoreMember.more_user_limit)
            },
            _getAllMembers: function(select_type) {
                var request;
                this.isLoading || (this.spinnerOn(),
                this.parameterForGetMoreMember.more_user_offset = 0,
                this.parameterForGetMoreMember.more_user_limit = -1,
                this.isLoading = true,
                new grn.component.ajax.request({
                    grnUrl: this.ajaxGetMemberListUrl,
                    method: "post",
                    dataType: "json",
                    data: this.parameterForGetMoreMember
                }).send().done(function(json_obj) {
                    this.addMembers(json_obj.members_info),
                    "select_all" == select_type && (this.selectAllMembers(),
                    this.viewUnSelectAllLink()),
                    "page_down_with_shift" == select_type && this._selectRangeMemberToLast(),
                    "page_down" == select_type && (this._unSelectAllMembers(),
                    this._selectLastMember()),
                    this.$select_box.unbind("scroll")
                }
                .bind(this)).always(function() {
                    this.spinnerOff(),
                    this.isLoading = false
                }
                .bind(this)))
            },
            _selectMember: function($clicked_member) {
                $clicked_member.addClass(this.selectedClassName),
                this.lastSelectedId = $clicked_member.attr("id"),
                this.lastRangedId = null
            },
            _toggleMember: function($clicked_member) {
                $clicked_member.toggleClass(this.selectedClassName),
                this.lastSelectedId = $clicked_member.attr("id"),
                this.lastRangedId = null
            },
            _selectRangeMembers: function($selected_member) {
                var selected_id = $selected_member.attr("id"), $last_clicked = $("#" + this.lastSelectedId), $list = this.$ul_list.find("." + this.listName), clicked_index = $list.index($selected_member), last_clicked_index = $list.index($last_clicked), gt, lt, condition = "li";
                lt = clicked_index <= last_clicked_index ? (gt = clicked_index - 1,
                last_clicked_index - clicked_index + 1) : (gt = last_clicked_index - 1,
                clicked_index - last_clicked_index + 1),
                gt >= 0 && (condition += ":gt(" + gt + ")"),
                condition += ":lt(" + lt + ")",
                this.$ul_list.find(condition).addClass(this.selectedClassName),
                this.lastRangedId = selected_id
            },
            _selectRangeMemberToFirst: function() {
                var $first = $("#ul_" + this.listName).children().first();
                this._selectRangeMembers($first)
            },
            _selectRangeMemberToLast: function() {
                var $last = $("#ul_" + this.listName).children().last();
                this._selectRangeMembers($last)
            },
            _unSelectAllMembers: function() {
                $("." + this.listName + "." + this.selectedClassName).removeClass(this.selectedClassName)
            },
            _selectAllMembers: function() {
                $("." + this.listName).addClass(this.selectedClassName)
            },
            _selectFirstMember: function() {
                var $first = $("#ul_" + this.listName).children().first();
                $first.addClass(this.selectedClassName),
                this._scrollToMember($first),
                this.lastSelectedId = $first.attr("id"),
                this.lastRangedId = null
            },
            _selectLastMember: function() {
                var $last = $("#ul_" + this.listName).children().last();
                $last.addClass(this.selectedClassName),
                this._scrollToMember($last),
                this.lastSelectedId = $last.attr("id"),
                this.lastRangedId = null
            },
            _selectAboveMember: function() {
                var last_id = this.lastRangedId ? this.lastRangedId : this.lastSelectedId;
                if (last_id) {
                    var $prev = $("#" + last_id).prev();
                    return !$prev.length || (this._unSelectAllMembers(),
                    $prev.addClass(this.selectedClassName),
                    this._scrollToMember($prev),
                    this.lastSelectedId = $prev.attr("id"),
                    this.lastRangedId = null,
                    false)
                }
                return this._selectLastMember(),
                false
            },
            _selectBelowMember: function() {
                var last_id = this.lastRangedId ? this.lastRangedId : this.lastSelectedId;
                if (last_id) {
                    var $next = $("#" + last_id).next();
                    return !$next.length || (this._unSelectAllMembers(),
                    $next.addClass(this.selectedClassName),
                    this._scrollToMember($next),
                    this.lastSelectedId = $next.attr("id"),
                    this.lastRangedId = null,
                    false)
                }
                return this._selectFirstMember(),
                false
            },
            _selectAboveMemberFurther: function() {
                var $select, $prev;
                ($prev = ($select = this.lastRangedId ? $("#" + this.lastRangedId) : $("#" + this.lastSelectedId)).prev()).length && ($select.removeClass(this.selectedClassName),
                this._selectRangeMembers($prev),
                this._scrollToMember($prev))
            },
            _selectBelowMemberFurther: function() {
                var $select, $next;
                ($next = ($select = this.lastRangedId ? $("#" + this.lastRangedId) : $("#" + this.lastSelectedId)).next()).length && ($select.removeClass(this.selectedClassName),
                this._selectRangeMembers($next),
                this._scrollToMember($next))
            },
            _scrollToMember: function($member) {
                var offset = $member.offset().top - this.$ul_list.offset().top
                  , scroll_position = this.$select_box.scrollTop()
                  , select_box_height = this.$select_box.prop("clientHeight") - 18;
                scroll_position >= offset ? this.$select_box.scrollTop(offset) : scroll_position + select_box_height <= offset && this.$select_box.scrollTop(offset - select_box_height)
            },
            removeSelectedMembers: function() {
                var remove_selected_members = this.getSelectedList().toArray();
                return $("." + this.listName + "." + this.selectedClassName).remove(),
                remove_selected_members
            },
            removeSelectedFacilities: function() {
                this.removeSelectedMembers()
            },
            removeMembers: function() {
                $("." + this.listName).remove()
            },
            unSelectAllMembers: function() {
                this._unSelectAllMembers()
            },
            selectAllMembers: function() {
                this._selectAllMembers()
            },
            clickMember: function($clicked_member, event) {
                this.isTouchSupported ? this._toggleMember($clicked_member) : event.shiftKey && this.lastSelectedId ? (this._unSelectAllMembers(),
                this._selectRangeMembers($clicked_member)) : this._isCommandKey(event) ? this._toggleMember($clicked_member) : (this._unSelectAllMembers(),
                this._selectMember($clicked_member))
            },
            tracingSelect: function($hovered_member) {
                this.isMouseDown && this.lastSelectedId && (this._unSelectAllMembers(),
                this._selectRangeMembers($hovered_member),
                this.isFacilityCandidate && this.updateAncestorsPath())
            },
            keyDown: function(event) {
                var key_code = event.keyCode, KEYS_PAGE_UP = 33, KEYS_PAGE_DOWN = 34, KEYS_END = 35, KEYS_HOME = 36, KEYS_UP, KEYS_DOWN = 40, KEYS_A = 65;
                if (key_code === 38)
                    return event.shiftKey && this.lastSelectedId ? (this._selectAboveMemberFurther(),
                    false) : this._selectAboveMember();
                if (key_code === KEYS_DOWN)
                    return event.shiftKey && this.lastSelectedId ? (this._selectBelowMemberFurther(),
                    false) : this._selectBelowMember();
                if (key_code === KEYS_PAGE_UP || key_code === KEYS_HOME) {
                    if (!event.shiftKey || !this.lastSelectedId)
                        return this._unSelectAllMembers(),
                        this._selectFirstMember(),
                        false;
                    this._selectRangeMemberToFirst()
                }
                if (key_code === KEYS_PAGE_DOWN || key_code === KEYS_END)
                    if (this.isScrollToAdd && !this.$select_box.hasClass("selectlist_l_grn")) {
                        var select_type = "page_down";
                        event.shiftKey && this.lastSelectedId && (select_type = "page_down_with_shift"),
                        this._getAllMembers(select_type)
                    } else {
                        if (!event.shiftKey || !this.lastSelectedId)
                            return this._unSelectAllMembers(),
                            this._selectLastMember(),
                            false;
                        this._selectRangeMemberToLast()
                    }
                return key_code !== KEYS_A || !this._isCommandKey(event) || (this._selectAllMembers(),
                false)
            },
            _isCommandKey: function(event) {
                return -1 !== navigator.userAgent.indexOf("Mac") ? event.metaKey : event.ctrlKey
            },
            jQuery: function() {
                return this.isCurrentScreen ? $ : window.opener.jQuery
            },
            getList: function() {
                return this.jQuery()("." + this.listName)
            },
            getValues: function() {
                return $.map(this.getList(), this.getValue)
            },
            getValue: function(target) {
                return $(target).attr("data-value").split(":")[0]
            },
            getSelectedUsersValues: function() {
                return $.map(this.getSelectedList(), this.getValue)
            },
            getSelectedList: function() {
                return $("." + this.listName + "." + this.selectedClassName)
            },
            getUsersList: function() {
                return this.jQuery()("." + this.listName + "[data-type='user']")
            },
            addMembers: function(users_info, selected_flag) {
                this.removeMembers(),
                this.appendMembers(users_info, selected_flag)
            },
            appendMembers: function(users_info, selected_flag) {
                var self = this
                  , fragmentNode = document.createDocumentFragment()
                  , class_name = this.listName;
                class_name += selected_flag ? " " + this.selectedClassName : "",
                Object.keys(users_info).forEach(function(key) {
                    var user_info = users_info[key]
                      , member_id = (self.listName + "_member_" + user_info.type + "_" + user_info.id).replace(":", "-")
                      , is_omitted_member = user_info.type === member_select_list.MEMBER_TYPES.OMITTED
                      , data = self.getLabelAndClassName(user_info)
                      , liNode = document.createElement("li");
                    if ($("#" + member_id).length <= 0) {
                        liNode.id = member_id,
                        liNode.className = class_name,
                        liNode.setAttribute("data-value", PREFIXES[user_info.type] + user_info.id + (user_info.s_oid ? ":" + user_info.s_oid : "")),
                        user_info.foreignKey && liNode.setAttribute("data-code", user_info.foreignKey),
                        liNode.setAttribute("data-name", user_info.displayName),
                        liNode.setAttribute("data-type", user_info.type);
                        var spanNode1 = document.createElement("span");
                        spanNode1.className = data.class_name,
                        is_omitted_member || (spanNode1.setAttribute("aria-label", data.label),
                        spanNode1.title = data.label);
                        var spanNode2 = document.createElement("span");
                        spanNode2.className = "selectlist_text_grn",
                        is_omitted_member && (spanNode2.className += " selectlist_text_omitted_grn"),
                        spanNode2.textContent = user_info.displayName,
                        liNode.appendChild(spanNode1),
                        liNode.appendChild(spanNode2),
                        fragmentNode.appendChild(liNode)
                    }
                }),
                $("#ul_" + this.listName).append(fragmentNode)
            },
            addFacilities: function(facilities_info, selected_flag) {
                this.removeMembers();
                var self = this
                  , fragmentNode = document.createDocumentFragment()
                  , class_name = this.listName;
                class_name += selected_flag ? " " + this.selectedClassName : "",
                facilities_info.forEach(function(facility_info) {
                    var facility_id = self.listName + "_member_facility_" + facility_info.id
                      , liNode = document.createElement("li");
                    liNode.id = facility_id,
                    liNode.className = class_name,
                    liNode.setAttribute("data-value", facility_info.id),
                    liNode.setAttribute("data-approval", facility_info.approval),
                    liNode.setAttribute("data-repeat", facility_info.repeat),
                    liNode.setAttribute("data-ancestors", facility_info.ancestors),
                    liNode.setAttribute("data-code", facility_info.code),
                    liNode.setAttribute("data-name", facility_info.name);
                    var spanNode1 = document.createElement("span");
                    spanNode1.className = "selectlist_facility_grn";
                    var spanNode2 = document.createElement("span");
                    spanNode2.className = "selectlist_text2_grn",
                    spanNode2.textContent = facility_info.name,
                    liNode.appendChild(spanNode1),
                    liNode.appendChild(spanNode2),
                    fragmentNode.appendChild(liNode)
                }),
                $("#ul_" + this.listName).append(fragmentNode),
                this.lastSelectedId = null,
                this.lastRangedId = null
            },
            updateAncestorsPath: function() {
                var $parent_path_node = $("#selected-path")
                  , list = this.getSelectedList();
                if (1 === list.length) {
                    var path = $(list[0]).attr("data-ancestors");
                    "" === path && (path = grn.component.i18n.cbMsg("grn.schedule", "GRN_SCH-808")),
                    $parent_path_node.text(path)
                } else
                    $parent_path_node.text("")
            },
            getRepeatableSelectedFacilitiesValues: function() {
                return $.map(this.getList().filter("[data-repeat=1]"), this.getValue)
            },
            spinnerOn: function() {
                $("#spinner_" + this.listName).show()
            },
            spinnerOff: function() {
                $("#spinner_" + this.listName).hide()
            },
            spinnerScrollOn: function() {
                $("#spinner_scroll_" + this.listName).show()
            },
            spinnerScrollOff: function() {
                $("#spinner_scroll_" + this.listName).hide()
            },
            viewSelectAllLink: function() {
                this.$select_all.css("display", ""),
                this.$un_select_all.css("display", "none")
            },
            viewUnSelectAllLink: function() {
                this.$un_select_all.css("display", ""),
                this.$select_all.css("display", "none")
            },
            viewSelectAllOrClearAllLink: function() {
                this.getList().length && this.getList().length == this.getSelectedList().length ? this.viewUnSelectAllLink() : this.viewSelectAllLink()
            },
            makeHTMLTextForConfirmView: function(is_only_selected) {
                var $list, self = this, name = this.shortListName;
                return $list = $(is_only_selected ? "." + this.listName + "." + this.selectedClassName : "." + this.listName),
                $.map($list, function(member, index) {
                    return '<input type="hidden" name="' + name + "[" + index + ']" value="' + self.getValue(member) + '">'
                }).join("")
            },
            getLabelAndClassName: function(user_info) {
                if (user_info.type === member_select_list.MEMBER_TYPES.USER)
                    return true === user_info.isLoginUser ? {
                        label: grn.component.i18n.cbMsg("grn.grn", "GRN_GRN-LOGIN_USER"),
                        class_name: "selectlist_user_login_grn"
                    } : true === user_info.isInvalidUser ? {
                        label: grn.component.i18n.cbMsg("grn.grn", "GRN_GRN-INACTIVE_USER"),
                        class_name: "selectlist_user_invalid_grn"
                    } : true === user_info.isNotUsingApp ? {
                        label: grn.component.i18n.cbMsg("grn.grn", "GRN_GRN-NOT_APPLICATION_USER"),
                        class_name: "selectlist_user_invalid_app_grn"
                    } : {
                        label: grn.component.i18n.cbMsg("grn.grn", "GRN_GRN-1571"),
                        class_name: "selectlist_user_grn"
                    };
                if (user_info.type === member_select_list.MEMBER_TYPES.GROUP)
                    return true === this.isCalendar ? {
                        label: grn.component.i18n.cbMsg("grn.grn", "GRN_GRN-ORGANIZATION_PLAN"),
                        class_name: "selectlist_cal_group_grn"
                    } : {
                        label: grn.component.i18n.cbMsg("grn.grn", "GRN_GRN-ORGANIZATION"),
                        class_name: "selectlist_group_grn"
                    };
                var is_static_role = user_info.type === member_select_list.MEMBER_TYPES.ROLE
                  , is_dynamic_role = user_info.type === member_select_list.MEMBER_TYPES.DYNAMIC_ROLE;
                return is_static_role || is_dynamic_role ? {
                    label: grn.component.i18n.cbMsg("grn.grn", "GRN_GRN-ROLE"),
                    class_name: "selectlist_role_grn"
                } : user_info.type === member_select_list.MEMBER_TYPES.OMITTED ? {
                    class_name: "selectlist_omitted_grn"
                } : void 0
            }
        }
    }
}(jQuery);
