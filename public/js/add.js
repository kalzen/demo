// namespace
grn.base.namespace("grn.page.schedule.add"),
function($) {
    function _schedule_add(form, url) {
        G.checkUsingPurposeNotEmpty() && ("string" === typeof form && (form = document.getElementById(form)),
        _schedule_submit(form, url))
    }
    function _schedule_submit(f, url) {
        check_add || setAjaxElement(f) && (check_add = true,
        setShowRefreshDialogFlag(f),
        G.grn_onsubmit_common(f),
        _submit(f, url))
    }
    function check_validate(){
        var check = true;
        if($('#subject').val() === ""){
            $("#validate_subject").css('display','block');
            check = false;
        }else{
            $("#validate_subject").css('display','none');
        }
        return check;
    }
    var G = grn.page.schedule.add;
    G.is_loaded || (G.init = function() {
        G.usingPurposeError = $("#using_purpose_error"),
        G.usingPurposeField = $("#using_purpose_element"),
        G.usingPurposeTextArea = $("#using_purpose"),
        G.note = $("#textarea_id"),
        G.minHeightUsingPurpose = 90,
        G.totalBackStep = 1,
        G.checkCancel = false
    }
    ,
    G.displayRequireUsingPurpose = function() {
        G.usingPurposeError.length && (G.usingPurposeError.show(),
        check_add = false)
    }
    ,
    G.hideRequireUsingPurpose = function() {
        G.usingPurposeError.length && G.usingPurposeError.hide()
    }
    ,
    G.checkUsingPurposeNotEmpty = function() {
        return !G.usingPurposeField.length || !G.usingPurposeField.is(":visible") || (0 == G.trim(G.usingPurposeTextArea.val()).length ? (G.usingPurposeError.show(),
        $(document).scrollTop(G.usingPurposeField.offset().top - 27),
        false) : (G.usingPurposeError.hide(),
        true))
    }
    ,
    G.resetUsingPurposeHeight = function() {
        G.usingPurposeTextArea.length && "" == G.usingPurposeTextArea.val() && (G.usingPurposeTextArea.css({
            height: G.minHeightUsingPurpose
        }),
        G.usingPurposeTextArea.css({
            "min-height": G.minHeightUsingPurpose
        }))
    }
    ,
    G.trim = function(str) {
        return str.replace(/^\s+|\s+$/g, "")
    }
    ,
    jQuery(window).on("load", function() {
        G.init(),
        G.resetUsingPurposeHeight(),
        "" != G.usingPurposeTextArea.val() && (G.defaultHeightPurpose = G.usingPurposeTextArea.css("height"))
    }),
    G.schedule_submit = function(form, url) {
        var handler;
        console.log(check_validate());
        if(check_validate() === true){
            (new grn.js.page.schedule.AddHandler).processTriggerEventSubmit({
                urlToSubmit: url
            }).then(function() {
                _schedule_add(form, url)
            }).catch(function(error) {
                if (grn.component.button("#schedule_submit_button").hideSpinner(),
                grn.component.button("#schedule_submit_button_top").hideSpinner(),
                !(error instanceof grn.js.component.customization.CustomizationError))
                    throw error
            })
        }
    }
    ,
    G.schedule_cancel = function(url) {
        G.checkCancel || (G.checkCancel = true,
        "back" == url ? history.length > 1 ? G.totalBackStep > 1 ? history.go(-G.totalBackStep) : history.back() : location.href = grn.component.url.page("schedule/index") : location.href = url)
    }
    ,
    G.update_back_step = function() {
        G.totalBackStep++
    }
    ,
    G.grn_onsubmit_common = function(f) {
        if (grn.base.isNamespaceDefined("grn.component.member_add")) {
            var private_select = grn.component.member_add.get_instance("private_select");
            private_select && private_select.prepareSubmit();
            var member_select = grn.component.member_add.get_instance("member_select");
            member_select && member_select.prepareSubmit()
        }
        if (grn.base.isNamespaceDefined("grn.component.facility_add")) {
            var facility_select = grn.component.facility_add.get_instance("facility_select");
            facility_select && facility_select.prepareSubmit()
        }
    }
    ,
    G.is_loaded = true)
}(jQuery);
