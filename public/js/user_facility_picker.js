!function($) {
    "use strict";
    grn.base.namespace("grn.component.schedule.user_facility_picker");
    var _options = {
        maxListMemberAdded: 20,
        minLength: 1,
        timeDelay: 100,
        url: "api/ajax_user_org_facility_list",
        container: "cb-user-picker"
    }, G;
    grn.component.schedule.user_facility_picker = function(options) {
        grn.component.pubsub.installTo(this),
        this.options = $.extend({}, _options, options),
        this._memberList = new grn.component.schedule.multi_view.member_list
    }
    ,
    grn.component.schedule.user_facility_picker.prototype = {
        MEMBER_TYPE: {
            USER: "user",
            ORGANIZATION: "org",
            FACILITY: "facility"
        },
        suggestList: null,
        viewUtil: grn.component.schedule.multi_view.util,
        htmlTemplate: {
            component: "<table class='table_plain_grn calendar_user_facility_list_base_grn'>   <tbody>       <tr>           <td style='width:1%;line-height: 18px;' class='vAlignTop-grn'>               <span class='user_facility_listitem_sub_grn small_button_base_grn'>                   <span class='aButtonStandard-grn'>                       <a href='javascript:void(0)' class='js_check_all_member'>                           <span class='listcheck_button_grn'> </span>                       </a>                   </span>               </span>           </td>            <td class='vAlignTop-grn'>               <div class='calendar_user_facility_list_grn'>                   <ul class='listitem_delete_grn js_added_list'>                       <li class='js_input_controller'>                           <span class='user_facility_listitem_sub_grn nowrap-grn'>                               <div class='searchbox-grn inline_block_grn mRight3'>                                   <div id='searchbox-schedule-schedules' class='input-text-outer-schedule searchbox-keyword-schedule'>                                       <input type='text' class='input-text-schedule' name='search_text' autocomplete='off'>                                   </div>                               </div>                               <a style='margin-left:-3px;' href='javascript:void(0);' class='action_base_grn js_add_command'>                                   <span class='icon_add_grn icon_only_grn'></span>                               </a>                           </span>                       </li>                       <li class='js_msg_over_load_selected'>                           <span class='icon_attention_grn'>                               <span class='attentionMessage-grn js_msg'></span>                           </span>                       </li>                   </ul>               </div>           </td>       </tr>   </tbody></table>",
            userFacilitySelected: "<li class='js_member'>    <span class='user_facility_listitem_grn js_color_block'>        <input type='checkbox' class='view_item_checkbox js_checkbox' checked/>            <a class='js_label_a'>                <span class='js_label'></span>            </a>        <a class='closebutton js_del_icon' href='javascript:void(0);'></a>    </span></li>",
            orgSelected: "<span class='v-allign-middle'>    <span class='js_label'></span></span>"
        },
        cssSelector: {
            classPrefix: ".",
            body: "body",
            memberAdded: ".js_member",
            membersCheckUnchecked: ".js_member input[type='checkbox']",
            memberIsChecked: ".js_member input[type='checkbox']:checked",
            displayName: ".js_label",
            onOffEvent: ".js_checkbox",
            deleteIcon: ".js_del_icon",
            label: ".js_label_a",
            colorBlock: ".js_color_block",
            elementHide: ":visible",
            input: "input",
            inputController: ".js_input_controller",
            checkAllMember: ".js_check_all_member",
            searchInput: "input[type='text']",
            addCommand: ".js_add_command",
            msgOverLoadSelected: ".js_msg_over_load_selected",
            inputIncrementalSearch: ".js_input_incremental_search",
            addedList: ".js_added_list",
            msg: ".js_msg"
        },
        cssClass: {
            inputIncrementalSearch: "js_input_incremental_search"
        },
        KEY_CODE: {
            BACK_SPACE: 8,
            ESC: 27,
            DEL: 46,
            END: 35,
            HOME: 36,
            LEFT: 37,
            UP: 38,
            RIGHT: 39,
            DOWN: 40,
            INSERT: 45,
            PAGE_UP: 33,
            PAGE_DOWN: 34,
            CAPS_LOCK: 20
        },
        KEY_WORD: {
            MEMBER_DATA: "memberData",
            TITLE: "title",
            HEAD: "head",
            CLICK: "click",
            KEY_UP: "keyup",
            MOUSE_UP: "mouseup",
            SELECTED: "selected",
            CHECKED: "checked",
            CHANGE: "change",
            CUT: "cut",
            HREF: "href",
            TARGET: "target",
            BLANK: "_blank",
            UNDEFINED: "undefined",
            MOUSE_DOWN: "mousedown",
            OBJECT: "object",
            STR_SEPARATOR: "; ",
            GREATER_LESS_SIGN: "><",
            WHICH: "which",
            BUTTON: "button",
            EMPTY: ""
        },
        JS_RESOURCES: {
            DELETE_TOOLTIP: grn.component.i18n.cbMsg("grn.schedule", "GRN_SCH-1051"),
            CHECK_UNCHECKED: grn.component.i18n.cbMsg("grn.schedule", "GRN_SCH-1050"),
            ADD: grn.component.i18n.cbMsg("grn.schedule", "GRN_SCH-1049"),
            OVER_LOAD_SELECTED: grn.component.i18n.cbMsg("grn.schedule", "GRN_SCH-1052")
        },
        _memberList: null,
        isLoading: false,
        init: function() {
            this.createComponent(),
            this.cacheElements(),
            this.suggestList = new grn.component.user_org_facility_suggest_list({
                url: this.options.url
            }),
            this.suggestList.init(),
            this.bindEvents(),
            this.$inputControl.hide(),
            this.$msgOverloadAdded.hide(),
            this.$inputController.show(),
            this.htmlTemplate.userFacilitySelected = this.htmlTemplate.userFacilitySelected.replace(/>\s+</g, this.KEY_WORD.GREATER_LESS_SIGN),
            this.htmlTemplate.orgSelected = this.htmlTemplate.orgSelected.replace(/>\s+</g, this.KEY_WORD.GREATER_LESS_SIGN)
        },
        createComponent: function() {
            var container = $(this.htmlTemplate.component.replace(/>\s+</g, this.KEY_WORD.GREATER_LESS_SIGN)).addClass(this.options.container);
            container.find(this.cssSelector.checkAllMember).prop(this.KEY_WORD.TITLE, this.JS_RESOURCES.CHECK_UNCHECKED),
            container.find(this.cssSelector.searchInput).addClass(this.cssClass.inputIncrementalSearch),
            container.find(this.cssSelector.addCommand).prop(this.KEY_WORD.TITLE, this.JS_RESOURCES.ADD),
            container.find(this.cssSelector.msgOverLoadSelected).find(this.cssSelector.msg).text(this.JS_RESOURCES.OVER_LOAD_SELECTED.replace("%d", this.options.maxListMemberAdded)),
            $(this.cssSelector.classPrefix + this.options.container).replaceWith(container)
        },
        cacheElements: function() {
            this.$body = $(this.cssSelector.body),
            this.$container = $(this.cssSelector.classPrefix + this.options.container),
            this.$checkAllMembers = this.$container.find(this.cssSelector.checkAllMember),
            this.$inputController = this.$container.find(this.cssSelector.inputController),
            this.$inputControl = this.$inputController.find(this.cssSelector.inputIncrementalSearch),
            this.$addCommand = this.$container.find(this.cssSelector.addCommand),
            this.$msgOverloadAdded = this.$container.find(this.cssSelector.msgOverLoadSelected),
            this.$addedList = this.$container.find(this.cssSelector.addedList)
        },
        bindEvents: function() {
            var self = this;
            this.$addCommand.on(this.KEY_WORD.CLICK, $.proxy(this.addInputCommand, this)),
            this.$body.on(this.KEY_WORD.KEY_UP, $.proxy(this._handleKeyUp, this)),
            this.$inputControl.on(this.KEY_WORD.CUT, $.proxy(this._cutSearchKey, this)),
            this.$inputControl.autocomplete({
                source: function(request) {
                    self.suggestList.searchUserOrgFacility(request)
                },
                minLength: this.options.minLength,
                delay: this.options.timeDelay
            }),
            this.$inputControl.on(this.KEY_WORD.MOUSE_UP, $.proxy(this._handleMouseUp, this)),
            this.$body.on(this.KEY_WORD.MOUSE_UP, $.proxy(this._handleDocumentMouseUp, this)),
            this.suggestList.on(this.KEY_WORD.SELECTED, $.proxy(this._selectMemberFromSuggestList, this)),
            this.$checkAllMembers.on(this.KEY_WORD.CLICK, $.proxy(this._checkAllMember, this)),
            this.$addedList.on(this.KEY_WORD.CLICK, this.cssSelector.onOffEvent, $.proxy(this._hasChanged, this)),
            this.$addedList.on(this.KEY_WORD.CLICK, this.cssSelector.deleteIcon, $.proxy(this._removeMemberAdded, this))
        },
        getMemberList: function() {
            return this._memberList
        },
        _cutSearchKey: function() {
            var self = this;
            setTimeout(function() {
                self.$inputControl.val().trim() || self.suggestList.$containerSuggestedList.hide()
            }, 0)
        },
        _hideInputControl: function() {
            this.$inputControl.is(this.cssSelector.elementHide) && this.$inputControl.hide(),
            this.suggestList.isSearched = false,
            this.suggestList.searchKey = null,
            this.suggestList.$suggestList.empty(),
            this.suggestList.$containerSuggestedList.hide()
        },
        addInputCommand: function(e) {
            var isShowed;
            e.preventDefault(),
            this.$inputControl.is(this.cssSelector.elementHide) ? this._hideInputControl() : (this.suggestList.$containerSuggestedList.hide(),
            this.suggestList.$suggestList.empty(),
            this.$inputControl.show(),
            this.$inputControl.focus(),
            this.$inputControl.val(this.KEY_WORD.EMPTY)),
            e.stopImmediatePropagation()
        },
        _handleDocumentMouseUp: function(e) {
            var self = this
              , targetClass = e.target.className
              , plusIcon = $(e.target).closest(this.cssSelector.addCommand).length
              , isRightClick = false;
            targetClass.indexOf(self.cssClass.inputIncrementalSearch) > 0 || (this.KEY_WORD.WHICH in e ? 3 === e.which && (isRightClick = true) : this.KEY_WORD.BUTTON in e && 2 === e.button && (isRightClick = true),
            isRightClick ? e.stopImmediatePropagation() : plusIcon || setTimeout(function() {
                self._hideInputControl()
            }, 10))
        },
        _handleMouseUp: function() {
            var self = this, oldValue, newValue;
            (oldValue = this.$inputControl.val().trim()) !== this.KEY_WORD.EMPTY && setTimeout(function() {
                (newValue = self.$inputControl.val().trim()) === self.KEY_WORD.EMPTY && (self.suggestList.searchKey = null,
                self.suggestList.$containerSuggestedList.hide())
            }, 1)
        },
        _selectMemberFromSuggestList: function(memberSelected, memberData) {
            var firingTrigger = true, member;
            typeof memberData !== this.KEY_WORD.UNDEFINED && (false !== this._memberList.add({
                id: memberData._id,
                type: memberData.type,
                name: memberData.col_display_name,
                selected: true,
                url: memberData.url || this.KEY_WORD.EMPTY
            }) && this._renderSelectedMember(memberData, true),
            this._hideInputControl())
        },
        _translateIndex: function(memberDataInput) {
            return null == memberDataInput ? null : {
                _id: memberDataInput.id,
                col_display_name: memberDataInput.name,
                type: memberDataInput.type,
                selected: memberDataInput.selected || false,
                primary_group: memberDataInput.primary_group || this.KEY_WORD.EMPTY,
                colorId: memberDataInput.colorId,
                url: memberDataInput.url || this.KEY_WORD.EMPTY
            }
        },
        _renderSelectedMember: function(memberData, isFiringTrigger) {
            var member_html, primary_group, css_class, url;
            null != memberData && (typeof memberData.selected === this.KEY_WORD.UNDEFINED && (memberData.selected = true),
            member_html = $(this.htmlTemplate.userFacilitySelected),
            memberData.type === this.MEMBER_TYPE.ORGANIZATION && member_html.find(this.cssSelector.label).replaceWith(this.htmlTemplate.orgSelected),
            (primary_group = memberData.primary_group || this.KEY_WORD.EMPTY) !== this.KEY_WORD.EMPTY && (primary_group = this.KEY_WORD.STR_SEPARATOR + primary_group),
            memberData.type === this.MEMBER_TYPE.ORGANIZATION && (memberData.col_display_name = "[" + memberData.col_display_name + "]"),
            member_html.find(this.cssSelector.displayName).text(memberData.col_display_name + primary_group),
            member_html.prop(this.KEY_WORD.MEMBER_DATA, memberData),
            member_html.find(this.cssSelector.onOffEvent).prop(this.KEY_WORD.CHECKED, memberData.selected),
            member_html.find(this.cssSelector.deleteIcon).prop(this.KEY_WORD.TITLE, this.JS_RESOURCES.DELETE_TOOLTIP),
            (url = memberData.url || this.KEY_WORD.EMPTY).match(/^javascript/) || member_html.find(this.cssSelector.label).attr(this.KEY_WORD.TARGET, this.KEY_WORD.BLANK),
            member_html.find(this.cssSelector.label).prop(this.KEY_WORD.HREF, memberData.url),
            member_html.insertBefore(this.$inputController),
            typeof this.viewUtil && (css_class = this.viewUtil.createEventBlockColorCss(memberData._id, memberData.type, this._memberList),
            member_html.children(this.cssSelector.colorBlock).addClass(css_class)),
            true === isFiringTrigger && this._triggerChange())
        },
        _setMembers: function(dataResult, textStatus, jqXHR) {
            var members, members_to_remove, count_max_member, member, member_type, member_data;
            if (typeof grn.component.error_handler !== this.KEY_WORD.UNDEFINED && grn.component.error_handler.hasCybozuError(jqXHR) && grn_parseJson(request.responseText))
                grn.component.error_handler.show(jqXHR);
            else if (this.$container.find(this.cssSelector.memberAdded).detach(),
            !(typeof dataResult !== this.KEY_WORD.OBJECT || Object.keys(dataResult).length < 1) && dataResult.facility && dataResult.user && dataResult.org) {
                members = this._memberList.getMembers(),
                members_to_remove = [];
                for (var i = count_max_member = 0; i < members.length; i++)
                    member_type = (member = members[i]).type,
                    null != (member_data = this._checkMemberExists(member, dataResult[member_type], member_type)) ? count_max_member < this.options.maxListMemberAdded && (++count_max_member,
                    member.name = member_data.col_display_name,
                    member.primary_group = member_data.primary_group || this.KEY_WORD.EMPTY,
                    member.url = member_data.url || this.KEY_WORD.EMPTY,
                    this._renderSelectedMember(this._translateIndex(member), false)) : members_to_remove.push(member);
                for (this._showInputController(count_max_member),
                i = 0; i < members_to_remove.length; i++)
                    this._memberList.remove(members_to_remove[i])
            }
        },
        _checkMemberExists: function(member, memberList, type) {
            for (var count = Object.keys(memberList).length, i = 0; i < count; i++)
                if (memberList[i]._id === member.id && member.type === type)
                    return memberList[i];
            return null
        },
        _triggerChange: function() {
            this.trigger(this.KEY_WORD.CHANGE)
        },
        _hasChanged: function(event) {
            var target_event = event.target, member_element, member_data = $(target_event).closest(this.cssSelector.memberAdded).prop(this.KEY_WORD.MEMBER_DATA), member;
            this._memberList.getById(member_data._id, member_data.type).selected = target_event.checked,
            this._triggerChange()
        },
        _removeMemberAdded: function(event) {
            event.preventDefault();
            var member_element = $(event.target).closest(this.cssSelector.memberAdded)
              , member_data = member_element.prop(this.KEY_WORD.MEMBER_DATA);
            this._memberList.remove({
                id: member_data._id,
                type: member_data.type
            }),
            member_element.remove(),
            this._triggerChange()
        },
        setMemberList: function(memberList) {
            if (memberList.resolved)
                return this._setResolvedMemberList(memberList),
                void (this.isLoading = false);
            var flag = this.KEY_WORD.SELECTED, uids = [], oids = [], fids = [], members, request;
            if (typeof memberList !== this.KEY_WORD.UNDEFINED) {
                this._memberList = memberList,
                this.isLoading = false,
                members = memberList.getMembers();
                for (var i = 0; i < members.length; i++)
                    members[i].id !== this.KEY_WORD.EMPTY && (members[i].type === this.MEMBER_TYPE.USER ? uids.push(members[i].id) : members[i].type === this.MEMBER_TYPE.ORGANIZATION ? oids.push(members[i].id) : members[i].type === this.MEMBER_TYPE.FACILITY && fids.push(members[i].id));
                return (request = new grn.component.ajax.request({
                    grnUrl: this.options.url,
                    method: "post",
                    data: {
                        uids: uids,
                        oids: oids,
                        fids: fids,
                        flag: flag
                    }
                })).send().done($.proxy(this._setMembers, this))
            }
            this.isLoading = false
        },
        _setResolvedMemberList: function(memberList) {
            var self = this;
            this._memberList = memberList;
            var members = this._memberList.getMembers()
              , count_max_member = 0;
            this.$container.find(this.cssSelector.memberAdded).remove(),
            members.forEach(function(member) {
                count_max_member < self.options.maxListMemberAdded && (++count_max_member,
                self._renderSelectedMember(self._translateIndex(member), false))
            }),
            this._showInputController(count_max_member)
        },
        _checkAllMember: function(e) {
            if (!this.isLoading) {
                var member, memberData, memberElement;
                this.isLoading = true;
                var allMembers = this.$container.find(this.cssSelector.membersCheckUnchecked)
                  , memberIsChecked = this.$container.find(this.cssSelector.memberIsChecked)
                  , self = this;
                e.preventDefault(),
                allMembers.length < 1 || (allMembers.length === memberIsChecked.length ? memberIsChecked.prop(this.KEY_WORD.CHECKED, false) : allMembers.prop(this.KEY_WORD.CHECKED, true),
                this.$container.find(this.cssSelector.memberAdded).each(function() {
                    memberElement = $(this),
                    memberData = memberElement.prop(self.KEY_WORD.MEMBER_DATA),
                    (member = self._memberList.getById(memberData._id, memberData.type)).selected = memberElement.find(self.cssSelector.input).prop(self.KEY_WORD.CHECKED)
                }),
                this._triggerChange())
            }
        },
        _showInputController: function(countMaxMember) {
            countMaxMember >= this.options.maxListMemberAdded ? (this.$inputController.hide(),
            this.$msgOverloadAdded.show()) : (this.$inputController.show(),
            this.$msgOverloadAdded.hide())
        },
        _handleKeyUp: function(e) {
            switch (e.keyCode) {
            case this.KEY_CODE.ESC:
            case this.KEY_CODE.ENTER:
                this._hideInputControl();
                break;
            case this.KEY_CODE.LEFT:
            case this.KEY_CODE.RIGHT:
            case this.KEY_CODE.UP:
            case this.KEY_CODE.DOWN:
            case this.KEY_CODE.INSERT:
            case this.KEY_CODE.END:
            case this.KEY_CODE.HOME:
            case this.KEY_CODE.PAGE_UP:
            case this.KEY_CODE.PAGE_DOWN:
            case this.KEY_CODE.CAPS_LOCK:
                break;
            case this.KEY_CODE.DEL:
            case this.KEY_CODE.BACK_SPACE:
                this.$inputControl.val().trim() === this.KEY_WORD.EMPTY && (this.suggestList.$containerSuggestedList.hide(),
                this.suggestList.$suggestList.empty())
            }
        }
    }
}(jQuery);
