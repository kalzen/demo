!function($) {
    "use strict";
    if (!grn.base.isNamespaceDefined("grn.component.member_add")) {
        grn.base.namespace("grn.component.member_add");
        var member_add = grn.component.member_add
          , instances = {}
          , MAX_MEMBER_LENGTH = 1e3
          , default_options = {
            memberListOptions: {},
            candidateListOptions: {},
            categorySelectUrl: "",
            searchBoxOptions: {
                is_use: true,
                id_searchbox: "",
                url: "",
                append_post_data: {}
            },
            pulldownPartsOptions: {
                is_use: true
            },
            includeOrg: "",
            accessPlugin: "",
            accessPluginEncoded: "",
            pluginSessionName: "",
            pluginDataName: "",
            isCalendar: false,
            isPopup: false,
            showGroupRole: true,
            addOrgWithUsers: false,
            showOmitted: false,
            useCandidateSupportParts: true,
            operatorAddName: false,
            selectAllUsersInSearch: false
        };
        member_add.MemberAdd = function(name, form_name, member_list_names, candidate_list_Name, options) {
            grn.component.pubsub && grn.component.pubsub.installTo(this),
            (options = $.extend({}, default_options, options)).memberListOptions.isCalendar = options.isCalendar,
            options.candidateListOptions.isCalendar = options.isCalendar,
            options.isPopup && (options.memberListOptions.isCurrentScreen = false,
            options.candidateListOptions.isCurrentScreen = true),
            options.memberListOptions.memberAddName = name,
            options.candidateListOptions.memberAddName = name,
            this.name = name,
            this.memberListNames = member_list_names,
            this.memberLists = {},
            this.memberListNames.forEach(function(list_name) {
                var member_list_options = $.extend({}, {
                    isCandidateList: false
                }, options.memberListOptions);
                this.memberLists[list_name] = new grn.component.member_select_list.MemberSelectList(list_name,member_list_options)
            }
            .bind(this)),
            this.candidateListName = candidate_list_Name,
            this.candidateList = new grn.component.member_select_list.MemberSelectList(this.candidateListName,$.extend({}, {
                isCandidateList: true
            }, options.candidateListOptions)),
            this.formName = form_name,
            this.userAddSelectByGroupURL = options.categorySelectUrl,
            this.searchBoxOptions = options.searchBoxOptions,
            this.pulldownPartsOptions = options.pulldownPartsOptions,
            this.appId = options.appId,
            this.includeOrg = options.includeOrg,
            this.accessPlugin = options.accessPlugin,
            this.accessPluginEncoded = options.accessPluginEncoded,
            this.pluginSessionName = options.pluginSessionName,
            this.pluginDataName = options.pluginDataName,
            this.isCalendar = options.isCalendar,
            this.isPopup = options.isPopup,
            this.showGroupRole = options.showGroupRole,
            this.addOrgWithUsers = options.addOrgWithUsers,
            this.showOmitted = options.showOmitted,
            this.operatorAddName = options.operatorAddName,
            this.selectAllUsersInSearch = options.selectAllUsersInSearch,
            this.isSearching = false,
            this.ajaxUrl = options.ajax_url;
            var self = this;
            $(function() {
                self.init(name)
            }),
            instances[name] = this
        }
        ,
        member_add.prepareSubmit = function(names) {
            var instance, index;
            for (index = 0; index < names.length; ++index)
                (instance = this.get_instance(names[index])) && instance.prepareSubmit()
        }
        ,
        member_add.get_instance = function(name) {
            return instances[name]
        }
        ,
        member_add.MemberAdd.prototype = {
            init: function() {
                var self = this;
                if (this.memberListNames.forEach(function(list_name) {
                    var member_list = self.memberLists[list_name];
                    grn.component.button("#btn_add_" + list_name).on("click", self.addMembers.bind(self, member_list, self.candidateList)),
                    grn.component.button("#btn_rmv_" + list_name).on("click", self.removeMembers.bind(self, member_list)),
                    $("#" + list_name + "_order_top").on("click", member_list.orderTop.bind(member_list)),
                    $("#" + list_name + "_order_up").on("click", member_list.orderUp.bind(member_list)),
                    $("#" + list_name + "_order_down").on("click", member_list.orderDown.bind(member_list)),
                    $("#" + list_name + "_order_bottom").on("click", member_list.orderBottom.bind(member_list))
                }),
                this.pulldownPartsOptions.is_use && !this.isPopup) {
                    var $pulldown = $("#" + self.candidateListName + "_pulldown")
                      , pulldown_options = {
                        onSelect: function(pulldown_item) {
                            self.changeCategory(pulldown_item)
                        },
                        autoClose: true
                    };
                    grn.component.pulldown_menu.pulldown_menu.initWithOptions($pulldown, pulldown_options);
                    var $selected = $pulldown.find("li[data-selected]");
                    $selected.length ? $selected.click() : $($pulldown.find("li"))[0].click()
                }
                this.searchBoxOptions.is_use && !this.isPopup && ($("#keyword_" + self.searchBoxOptions.id_searchbox).on("keypress", function(event) {
                    return 13 === event.keyCode && self.search(),
                    13 !== event.keyCode
                }),
                $("#searchbox-submit-" + self.searchBoxOptions.id_searchbox).on("click", function() {
                    self.search()
                }))
            },
            modifyMemberDOM: function(member, from, to) {
                var $member = $(member)
                  , new_id = $member.attr("id").replace(from.shortListName, to.shortListName).replace(/-(\d)*$/, "");
                return $member.clone(false).attr({
                    id: new_id,
                    class: "selectlist_" + to.shortListName + " selectlist_selected_grn"
                })[0]
            },
            addMembers: function(to, from) {
                var self = this;
                self.addingMembers(to, from).done(function() {
                    var operator_add;
                    (to.viewSelectAllOrClearAllLink(),
                    self.operatorAddName) && self.getOperatorAddInstance(self.operatorAddName).addOperators(self.name)
                })
            },
            getOperatorAddInstance: function(name) {
                return this.isPopup ? window.opener.grn.component.operator_add.get_instance(name) : grn.component.operator_add.get_instance(name)
            },
            removeMembers: function(from_list) {
                var remove_member_list = from_list.removeSelectedMembers();
                if (from_list.viewSelectAllOrClearAllLink(),
                this.operatorAddName) {
                    var operator_add = this.getOperatorAddInstance(this.operatorAddName);
                    operator_add.removeOperators(remove_member_list, from_list.shortListName),
                    operator_add.operator_add.candidateList.viewSelectAllOrClearAllLink(),
                    operator_add.operator_add.getFirstMemberSelectList().viewSelectAllOrClearAllLink()
                }
            },
            addingMembers: function(to, from) {
                to.spinnerOn();
                var self = this, from_member_list = from.getSelectedList().toArray(), to_member_list_values = to.getValues(), to_member_list = to.getList(), is_include_omitted;
                to_member_list_values.indexOf("0") >= 0 && to.removeMembers();
                for (var length = from_member_list.length, list_to_add = [], i = 0; i < length; i++) {
                    var member = from_member_list[i]
                      , value = $(member).attr("data-value").split(":")[0];
                    if (this.isPopup && to.isCandidateList)
                        list_to_add.push(this.modifyMemberDOM(member, from, to));
                    else {
                        var is_omitted;
                        if (parseInt(value) === grn.component.member_select_list.OMITTED_ID) {
                            to.removeMembers(),
                            list_to_add.push(this.modifyMemberDOM(member, from, to));
                            break
                        }
                        var index = to_member_list_values.indexOf(value);
                        if (index < 0) {
                            if (this.addOrgWithUsers && "group" == $(member).attr("data-type")) {
                                var users_belong_organization = from.getUsersList();
                                $.each(users_belong_organization, function(key, user) {
                                    value = $(user).attr("data-value").split(":")[0],
                                    (index = to_member_list_values.indexOf(value)) < 0 ? list_to_add.push(self.modifyMemberDOM(user, from, to)) : $(to_member_list.get(index)).addClass(to.selectedClassName)
                                });
                                break
                            }
                            list_to_add.push(this.modifyMemberDOM(member, from, to))
                        } else
                            $(to_member_list.get(index)).addClass(to.selectedClassName)
                    }
                }
                var deferred = $.Deferred();
                return this.isPopup ? (this._appendMembersFromPopup(to, list_to_add),
                deferred.resolve()) : length > 1e3 ? setTimeout(function() {
                    self.appendMembers(to, list_to_add),
                    deferred.resolve()
                }, 100) : (this.appendMembers(to, list_to_add),
                deferred.resolve()),
                deferred.promise()
            },
            appendMembers: function(to, list_to_add) {
                $("#ul_selectlist_" + to.shortListName).append(list_to_add),
                to.spinnerOff()
            },
            _appendMembersFromPopup: function(to, list_to_add) {
                
                var parent = window.opener.jQuery("#ul_selectlist_" + to.shortListName);
                if (to.isCandidateList) {
                    to.removeMembers();
                    var pulldown_menu = window.opener.grn.component.pulldown_menu.pulldown_menu, $pulldown = window.opener.jQuery("#" + to.shortListName + "_pulldown"), pulldownItemList;
                    $pulldown.find(".pulldown_menu_grn > ul").find("li[data-value=popup_user_select]").remove();
                    var context = window.opener.grn.component.member_add.get_instance(to.memberAddName);
                    pulldown_menu.addItem($pulldown, grn.component.i18n.cbMsg("grn.grn", "GRN_GRN-806"), "popup_user_select", {
                        callback: function(value) {
                            this.changeCategory(value)
                        }
                        .bind(context),
                        select: true
                    })
                }
                if (grn.browser.msie) {
                    var elements = parent.html();
                    list_to_add.forEach(function(val) {
                        elements += val.outerHTML
                    }),
                    parent.html(elements)
                } else
                    parent.append(list_to_add);
                to.spinnerOff()
            },
            changeCategory: function(category) {
                var $pull_down = $("#" + this.candidateListName + "_pulldown");
                grn.component.pulldown_menu.pulldown_menu.removeItemByValue($pull_down, "search");
                var post_data = {
                    gid: $(category).attr("data-value"),
                    cid: this.candidateListName,
                    app_id: this.appId,
                    include_org: this.includeOrg,
                    access_plugin: this.accessPluginEncoded,
                    is_json_required: true,
                    show_group_role: this.showGroupRole,
                    show_omitted: this.showOmitted
                }, ajaxRequest;
                this.candidateList.spinnerOn(),
                new grn.component.ajax.request({
                    url: this.userAddSelectByGroupURL,
                    method: "post",
                    dataType: "json",
                    data: post_data
                }).send().done(function(json_obj) {
                    this.candidateList.addMembers(json_obj.users_info),
                    this.candidateList.viewSelectAllLink(),
                    this.candidateList.spinnerOff(),
                    grn.component.pubsub && this.trigger("candidateListChange", json_obj.users_info)
                }
                .bind(this))
            },
            search: function() {
                var keyword = $("#keyword_" + this.searchBoxOptions.id_searchbox).val(), search_box;
                grn.component.search_box.get_instance(this.searchBoxOptions.id_searchbox).firstFlag && (keyword = "");
                var post_data = {
                    keyword: keyword,
                    search_login_name: "0"
                }, ajaxRequest;
                this.isSearching || (this.appId && (post_data.app_id = this.appId),
                this.accessPlugin && (post_data.plugin_session_name = this.pluginSessionName,
                post_data.plugin_data_name = this.pluginDataName),
                this.searchBoxOptions.append_post_data && $.each(this.searchBoxOptions.append_post_data, function(key, value) {
                    post_data[key] = value
                }),
                new grn.component.ajax.request({
                    url: this.searchBoxOptions.url,
                    method: "post",
                    dataType: "json",
                    data: post_data,
                    grnLoadingIndicator: function(option) {
                        this.isSearching = "show" === option,
                        this.isSearching ? this.candidateList.spinnerOn() : (this.candidateList.$select_box.focus(),
                        this.candidateList.spinnerOff(),
                        this.candidateList.viewSelectAllLink())
                    }
                    .bind(this)
                }).send().done(function(json_obj) {
                    this.candidateList.addMembers(json_obj.members_info, this.selectAllUsersInSearch);
                    var pulldown_menu = grn.component.pulldown_menu.pulldown_menu, $pulldown = $("#" + this.candidateListName + "_pulldown"), is_has_search_result_item;
                    pulldown_menu.isItemHasValue($pulldown, "search") || pulldown_menu.addItem($pulldown, grn.component.i18n.cbMsg("grn.grn", "GRN_GRN-807"), "search", {
                        select: true
                    }),
                    pulldown_menu.setSelectItemToHead($pulldown.find("dt"), this)
                }
                .bind(this)))
            },
            prepareSubmit: function() {
                var self = this
                  , form = document.forms[this.formName];
                this.memberListNames.forEach(function(list_name) {
                    var src = self.memberLists[list_name].getList().toArray()
                      , selected_users = form.elements["selected_users_" + list_name]
                      , selected_groups = form.elements["selected_groups_" + list_name]
                      , selected_users_value = []
                      , selected_groups_value = []
                      , member_types = grn.component.member_select_list.MEMBER_TYPES;
                    src.forEach(function(option) {
                        var value = $(option).attr("data-value")
                          , type = $(option).attr("data-type");
                        if (value) {
                            var co_value = value.split(":");
                            if (!(co_value.length <= 0))
                                switch (type) {
                                case member_types.USER:
                                case member_types.OMITTED:
                                    (isFinite(co_value[0]) || co_value[0].match(/g[0-9]+/)) && selected_users_value.push(co_value[0]),
                                    co_value.length > 1 && selected_groups_value.push(co_value[1]);
                                    break;
                                case member_types.GROUP:
                                    selected_users_value.push(co_value[0]),
                                    selected_groups_value.push(co_value[0].substr(1));
                                    break;
                                case member_types.ROLE:
                                case member_types.DYNAMIC_ROLE:
                                    selected_users_value.push(co_value[0])
                                }
                        }
                    }),
                    selected_groups.value = selected_groups_value.join(":"),
                    selected_users.value = selected_users_value.join(":")
                })
            },
            getFirstMemberSelectList: function() {
                var list_name = this.memberListNames[0];
                return this.memberLists[list_name]
            },
            updateTargetList: function(from) {
                for (var from_member_list = from.getSelectedList().toArray(), length = from_member_list.length, targets_arr = [], member_list = [], i = 0; i < length; i++) {
                    var member = from_member_list[i];
                    targets_arr.push($(member).attr("data-value").split(":")[0]),
                    member_list.push($(member).data())
                }
                var data = {
                    type: "grn.member_select_dialog.apply",
                    data: member_list
                };
                window.opener.postMessage(JSON.stringify(data), "*")
            },
            getMembers: function(is_candidate) {
                var member_list = [], member_list_instance = is_candidate ? this.candidateList : this.getFirstMemberSelectList(), $members_dom_element;
                member_list_instance.$ul_list && member_list_instance.$ul_list.find("li").each(function() {
                    var member = {}
                      , value = $(this).attr("data-value").split(":");
                    member.id = value[0],
                    value[1] && (member.group_id = value[1]),
                    $(this).attr("data-code") && (member.foreignKey = $(this).attr("data-code")),
                    member.displayName = $(this).attr("data-name"),
                    member.type = $(this).attr("data-type"),
                    member_list.push(member)
                });
                return member_list
            }
        }
    }
}(jQuery);
