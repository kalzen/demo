!function() {
    "use strict";
    var CALENDAR_EVENT_TYPE_CSS_ICON_MAP = {
        1: "calendar_event_icon_holiday",
        2: "calendar_event_icon_anniversary",
        3: "",
        4: "",
        5: "calendar_event_icon_workday"
    };
    grn.base.namespace("grn.component.schedule.multi_view.calendar_item_renderer"),
    grn.component.schedule.multi_view.calendar_item_renderer = function(settings) {
        this.settings = settings
    }
    ;
    var multi_view = grn.component.schedule.multi_view
      , url_component = grn.component.url
      , G = grn.component.schedule.multi_view.calendar_item_renderer;
    G.prototype.createNormalEventElement = function(event_data, params) {
        var settings = this.settings
          , event_css = "critical3 schedule" + settings.plid + " " + this._createEventBlockCss(event_data);
        multi_view.util.isOneDayEvent(event_data) || ("repeat" === event_data.type || "share_repeat" === event_data.type ? event_css += " group" + event_data.id + event_data.start_date.substr(0, 10) + event_data.uid + settings.plid : event_css += " group" + event_data.id + event_data.uid + settings.plid),
        params.start_at_mid_night && (event_css += " event_first_row");
        var time_html = ""
          , subject_html = "";
        if ("private"in event_data)
            time_html = event_data.showtime,
            subject_html = event_data.data;
        else {
            console.log(event_data);
            if(event_data.private === 0){
                var event_url = '/schedule/view/'+event_data.id;
            }else{
                var event_url = '#';
            }
            time_html = this._createDateTimeElement(event_data, event_url),
            subject_html = this._createSubject(event_data, event_url),
            "share_temporary" !== event_data.type && "temporary" !== event_data.type || (event_css += " temporary"),
            event_css += this._createCssIfNewEvent(event_data)
        }
        var event_element = document.createElement("div");
        return event_element.className = event_css,
        event_element.innerHTML = '<div class="event_content event_content_base"><font size="-1">' + time_html + " " + subject_html + "</font></div>",
        event_element
    }
    ,
    G.prototype.createWithoutTimeEventElement = function(event_data, params) {
        var settings = this.settings
          , event_css = "event_content_allday " + this._createEventBlockCss(event_data)
          , event_url = '/schedule/view/'+event_data.id
          , subject_html = this._createSubject(event_data, event_url);
        event_css += this._createCssIfNewEvent(event_data);
        var event_element = document.createElement("div");
        event_element.className = event_css;
        var allday_icon = "<span class='schedule_icon_allday'></span>";
        return event_element.innerHTML = '<span class="js_without_time_event_content">   <font size="-1" >' + allday_icon + subject_html + "   </font></span>",
        event_element
    }
    ,
    G.prototype.createBannerElement = function(event_data) {
        var settings = this.settings
          , event_css = "event_content_banner"
          , event_inner_css = "normalEventElement " + this._createEventBlockCss(event_data)
          , event_url = '/schedule/view/'+event_data.id
          , subject_html = this._createSubject(event_data, event_url);
        event_css += this._createCssIfNewEvent(event_data);
        var event_element = document.createElement("div");
        return event_element.className = event_css,
        event_element.innerHTML = '<div class="' + event_inner_css + ' js_banner_event_content ">' + subject_html + "</div>",
        event_element
    }
    ,
    G.prototype.createCalendarElement = function(calendar_item) {
        var element = document.createElement("div");
        element.className = "personal_week_calendar_event_item";
        var element_inner = document.createElement("div");
        element.appendChild(element_inner),
        element_inner.className = "normalEventElement";
        var icon = document.createElement("span");
        icon.className = CALENDAR_EVENT_TYPE_CSS_ICON_MAP[calendar_item.type],
        element_inner.appendChild(icon);
        var text = document.createTextNode(calendar_item.data);
        return element_inner.appendChild(text),
        element
    }
    ,
    G.prototype.createWeatherElement = function(calendar_item) {
        var escape = function(value) {
            return jQuery("<div>").text(value).html().replace(/'/g, "\\'").replace(/"/g, '\\"')
        }
          , element = document.createElement("div");
        element.className = "personal_week_calendar_event_item";
        var element_inner = document.createElement("div"), image, link;
        if (element_inner.className = "normalEventElement",
        element.appendChild(element_inner),
        calendar_item.icon) {
            var src = grn.component.url.image("cybozu/" + calendar_item.icon);
            (image = document.createElement("img")).setAttribute("border", "0"),
            image.setAttribute("src", src)
        }
        if (calendar_item.data) {
            (link = document.createElement("a")).setAttribute("href", calendar_item.data.info.url),
            element_inner.appendChild(link);
            var details = calendar_item.data.info
              , i18n = grn.component.i18n
              , tooltip_html = "<nobr>" + escape(details.weather) + "<nobr>";
            link.setAttribute("onMouseOver", "tpon(event, '" + tooltip_html + "')"),
            link.setAttribute("onMouseOut", "tpoff()"),
            link.setAttribute("target", "_blank");
            var text = document.createTextNode(calendar_item.data.location_name);
            image && link.appendChild(image),
            link.appendChild(text)
        }
        return image && !link && element_inner.appendChild(image),
        element
    }
    ,
    G.prototype.createTodoElement = function(todo, todo_type) {
        var element = document.createElement("div");
        element.className = "schedule_todo normalEventElement",
        this.settings.showTodos || (element.style.display = "none");
        var icon = document.createElement("span");
        icon.className = "schedule_icon_todo",
        element.appendChild(icon);
        var link = document.createElement("a");
        return link.textContent = todo.title,
        link.href = this._createTodoUrl(todo, todo_type),
        element.appendChild(link),
        element
    }
    ,
    G.prototype.createExpiredTodoElement = function(todo, todo_type) {
        var element = document.createElement("div");
        element.className = "schedule_expired_todo normalEventElement",
        this.settings.showExpiredTodos || (element.style.display = "none");
        var icon = document.createElement("span");
        icon.className = "schedule_icon_todo_expired",
        element.appendChild(icon);
        var link = document.createElement("a");
        return link.textContent = todo.title,
        link.href = this._createTodoUrl(todo, todo_type),
        element.appendChild(link),
        element
    }
    ,
    G.prototype._createTodoUrl = function(todo, todo_type) {
        var todoUrl = "";
        return "private" === todo_type ? todoUrl = url_component.page("todo/view/"+todo.tid
        ) : "shared" === todo_type && (todoUrl = url_component.page("space/application/todo/view", {
            spid: todo.space_id,
            tdid: todo.todo_id
        })),
        todoUrl
    }
    ,
    G.prototype._createEventBlockCss = function(event_data) {
        var member = multi_view.util.getMemberFromEvent(event_data);
        return event_data.relatedEvents ? "event_block_color_share_grn" : multi_view.util.createEventBlockColorCss(member.id, member.type, this.settings.memberList)
    }
    ,
    G.prototype._createIconsOnLeftSide = function(event_data) {
        var icons = ""
          , show_attendance_icon = event_data.attendance_check_show || false;
        return "banner" === event_data.type && (icons += "<span class='schedule_icon_banner'></span>"),
        event_data.conflict && (icons += "<span class='schedule_icon_attention'></span>"),
        show_attendance_icon && (icons += "<span class='schedule_icon_event_unanswered'></span>"),
        icons
    }
    ,
    G.prototype._createIconsOnRightSide = function(event_data) {
        var icons = "";
        return "is_private"in event_data && (icons += "<span class='schedule_icon_event_private'></span>"),
        "repeat" !== event_data.type && "share_repeat" === event_data.type && "week" === event_data.type_repeat && (icons += " <span class='schedule_icon_event_repeat'></span><img src='/img/cal_pweek20.gif' alt='Week' title='Week' class='small_link'>"),
        "repeat" !== event_data.type && "share_repeat" === event_data.type && "day" === event_data.type_repeat && (icons += " <span class='schedule_icon_event_repeat'></span><img src='/img/cal_pday20.gif' alt='Week' title='Week' class='small_link'>"),
        "repeat" !== event_data.type && "share_repeat" === event_data.type && "month" === event_data.type_repeat && (icons += " <span class='schedule_icon_event_repeat'></span><img src='/img/cal_pmon20.gif' alt='Week' title='Week' class='small_link'>"),
        "report"in event_data && (icons += "<span class='schedule_icon_report'></span>"),
        icons
    }
    ,
    G.prototype._createAppointmentType = function(event_data) {
        return '<span class="event_color' + event_data.menu_color + '_grn">' + event_data.menu + "</span>"
    }
    ,
    G.prototype._createDateTimeElement = function(event_data, event_url) {
        return '<span class="time_critical_grn"><a href="' + event_url + '">' + event_data.showtime + "</a></span>"
    }
    ,
    G.prototype._createSubject = function(event_data, event_url) {
        var settings = this.settings, subject_html = "", subject_inner_html = "", appointment_type_color = event_data.menu_color || "0", facility_name = event_data.facility_name || "", facility_items = event_data.faci_items || "", facility_placement = settings.facilityPlacement || "after_subject", creator_name = event_data.creator_name || "", duplicated_appointment_count = "", icons_left_side, icons_right_side;
        if (event_data.relatedEvents && event_data.relatedEvents.length > 0 && (duplicated_appointment_count = "<span class=event_share_number_grn>" + (event_data.relatedEvents.length + 1) + "</span>"),
        "0" !== appointment_type_color) {
            var appointment_type, title_appointment = this._createAppointmentType(event_data) + event_data.detail + duplicated_appointment_count;
            subject_inner_html += "after_subject" === facility_placement ? title_appointment + facility_name : facility_name + title_appointment,
            subject_inner_html += creator_name + facility_items
        } else
            subject_inner_html += event_data.data + duplicated_appointment_count;
        return subject_html = '<a href="' + event_url + '">' + this._createIconsOnLeftSide(event_data) + subject_inner_html + this._createIconsOnRightSide(event_data) + "</a>"
    }
    ,
    G.prototype._createCssIfNewEvent = function(event_data) {
        return event_data.id === this.settings.newEventId ? " newevent-grn" : ""
    }
}();
