!function() {
    "use strict";
    grn.base.isNamespaceDefined("grn.component.mobile_group_select") || (grn.base.namespace("grn.component.mobile_group_select"),
    grn.component.mobile_group_select = function() {
        function GroupSelect(settings) {
            this._construct(settings)
        }
        return GroupSelect.prototype = {
            _construct: function(settings) {
                this.initParams(settings),
                this.container = $("#" + settings.container),
                this.associate_value_element_back = $("#" + settings.associate_value_element_back),
                this.associate_title_element_back = $("#" + settings.associate_title_element_back),
                this.bindEventHandler(),
                this.adjustOrientationChange();
                var me = this;
                $(document).on("pagechange", function(event) {
                    me.adjustTextOverflow()
                }),
                $(window).on("resize", function(event) {
                    me.adjustTextOverflow()
                })
            },
            initParams: function(settings) {
                this.asyncUrl = settings.asyncUrl,
                this.paramName = settings.paramName,
                this.treeName = settings.treeName,
                this.pageName = settings.pageName,
                this.previousPage = settings.previousPage,
                this.radioName = settings.radio_name,
                this.prefixId = settings.prefix_id,
                this.prefixBeforeValue = settings.prefix_before_value,
                this.openClass = "mobile_folderlist_icon_arrowopen_grn",
                this.closeClass = "mobile_folderlist_icon_arrowclose_grn",
                this.checkOnClass = "mobile_groupselect_icon_radiobuttonon_grn",
                this.checkOffClass = "mobile_groupselect_icon_radiobuttonoff_grn",
                this.spinnerClass = "mobile_icon_spinner_grn"
            },
            bindEventHandler: function() {
                this.container.on("tap", "li", $.proxy(this.handleTouch, this)),
                this.container.on("touchstart", ".mobile_ok_grn", $.proxy(this.applyHandler, this)),
                this.container.on("touchstart", ".mobile_cancel_grn", $.proxy(this.closeHandler, this))
            },
            adjustOrientationChange: function(event) {
                var agent = window.navigator.userAgent, start = agent.indexOf("OS "), version;
                if (/(iPod|iPhone|iPad)/.test(agent) && start > -1 && window.Number(agent.substr(start + 3, 1).replace("_", ".")) < 7) {
                    var me = this;
                    $(window).on("orientationchange", function(event) {
                        me.container.is(":visible") && $(this).scrollTop(0)
                    })
                }
            },
            adjustTextOverflow: function() {
                $("#" + this.treeName).find("li[id^='" + this.prefixId + "parent_child_']").each(function(i) {
                    var divH = $(this).height()
                      , aLink = $(this).children("a")
                      , text = aLink.children().eq(0)
                      , ellipsisDiv = aLink.find("div.mobile_ellipsis_mark_grn");
                    text.outerHeight() > divH + 1 ? 0 === ellipsisDiv.length && aLink.append('<div class="mobile_ellipsis_mark_grn">...</div>') : ellipsisDiv.length > 0 && ellipsisDiv.remove()
                })
            },
            handleTouch: function(event) {
                var item = event.target
                  , itemObj = $(item);
                if (itemObj.hasClass(this.openClass))
                    this.collapseHandler(event);
                else if (itemObj.hasClass(this.closeClass))
                    this.expandHandler(event);
                else {
                    if (itemObj.closest("li").find("span.mobile_groupselect_icon_radiobuttonon_grn").length > 0)
                        return;
                    this.selectHandler(event)
                }
            },
            collapseHandler: function(event) {
                var element, parentId = event.target.id;
                this.collapse(parentId);
                var parent = $("#" + parentId);
                parent.hasClass(this.openClass) && (parent.removeClass(this.openClass),
                parent.addClass(this.closeClass))
            },
            collapse: function(parentId) {
                parentId = parentId.replace(this.prefixId, "");
                var child = $("#" + this.treeName).find("li[id^='" + this.prefixId + "parent_child_" + parentId + "']")
                  , me = this;
                $.each(child, function() {
                    var childIdInfo, childId = $(this).attr("id").split(me.prefixId + "parent_child_" + parentId + "_")[1];
                    "undefined" !== typeof childId && me.collapse(childId)
                }),
                child.remove()
            },
            expandHandler: function(event) {
                var item = event.target
                  , itemObj = $(item)
                  , paramName = this.paramName
                  , paramId = item.id
                  , id_substr_len = this.prefixId.length + 1;
                isNaN(parseInt(paramId.substr(0, id_substr_len))) && (paramId = paramId.substr(id_substr_len));
                var pageName = this.pageName
                  , deepInfo = itemObj.next().children().first().attr("id").split("_")
                  , deep = parseInt(deepInfo[1], 0)
                  , requestData = paramName + "=" + paramId + "&page=" + pageName;
                $(item).removeClass(this.closeClass).addClass(this.spinnerClass);
                var me = this;
                $.ajax({
                    url: me.asyncUrl,
                    dataType: "json",
                    data: requestData,
                    type: "post",
                    success: function(jsonObj, textStatus, transport) {
                        if (grn.component.mobile_error_handler.hasCybozuError(transport))
                            grn.component.mobile_error_handler.show(transport);
                        else if (jsonObj.count > 0) {
                            var frag = document.createDocumentFragment()
                              , subFolderList = $.map(jsonObj.subFolderList, function(value, index) {
                                return [value]
                            });
                            subFolderList.sort(function compare(a, b) {
                                return parseFloat(a.list_index) - parseFloat(b.list_index)
                            }),
                            $.each(subFolderList, function(key, value) {
                                var subGroup = me.buildSubGroup(value, deep + 1);
                                frag.appendChild(subGroup[0])
                            }),
                            itemObj.parent().after(frag),
                            $("#" + me.treeName).listview("refresh"),
                            me.adjustTextOverflow(),
                            $(item).hasClass(me.spinnerClass) && ($(item).removeClass(me.spinnerClass),
                            $(item).addClass(me.openClass))
                        } else
                            $(item).removeClass(me.spinnerClass).removeClass(me.openClass)
                    },
                    error: function(transport, textStatus) {
                        "abort" !== textStatus && me.showErrorMessage(transport)
                    },
                    complete: function() {
                        $(item).removeClass(me.spinnerClass)
                    }
                })
            },
            showErrorMessage: function(request) {
                var s = request.responseText;
                void 0 !== s && (document.write(s),
                document.close())
            },
            buildSubGroup: function(subGroup, deep) {
                var pixel = 8 * deep
                  , liTag = $('<li id="' + this.prefixId + "parent_child_" + this.prefixBeforeValue + subGroup.parent + "_" + this.prefixBeforeValue + subGroup.oid + '" style="padding-left:' + pixel + 'px !important" data-icon="false"></li>');
                if (parseInt(subGroup.count) > 0) {
                    var divIcon = $('<div id="' + this.prefixId + this.prefixBeforeValue + subGroup.oid + '" style="margin-left:' + pixel + 'px !important;" class="mobile_folderlist_icon_arrowclose_grn mobile_folderlist_icon_grn mobile_folderlist_icon_size_grn"></div>');
                    liTag.append(divIcon)
                }
                var tagA = $('<a href="#" class="mobile_folderlist_list_text_grn">')
                  , spanTitle = $('<span id="' + this.prefixId + "deepth_" + deep + "_" + subGroup.oid + '" class="mobile_folderlist_text_overflow_grn"></span>');
                spanTitle.text(subGroup.name),
                tagA.append(spanTitle),
                liTag.append(tagA);
                var iconRadio = $('<div><span class="mobile_groupselect_icon_radiobuttonoff_grn"></span><span class="mobile_groupselect_radiobutton_grn"><input name="' + this.radioName + '" type="radio" value="' + this.prefixBeforeValue + subGroup.oid + '"></span></div>');
                return liTag.append(iconRadio),
                liTag
            },
            selectHandler: function(event) {
                this.uncheckAllRadio();
                var item = event.target
                  , itemObj = $(item);
                itemObj.hasClass("mobile_groupselect_icon_radiobuttonoff_grn") || (itemObj = itemObj.closest("li").find("span.mobile_groupselect_icon_radiobuttonoff_grn")),
                itemObj.removeClass(this.checkOffClass),
                itemObj.addClass(this.checkOnClass),
                itemObj.siblings(".mobile_groupselect_radiobutton_grn").find('input[type="radio"]').each(function() {
                    $(this).prop("check", "checked")
                })
            },
            uncheckAllRadio: function() {
                this.container.find('input[type="radio"]').each(function() {
                    $(this).prop("check", false)
                });
                var me = this;
                this.container.find("." + this.checkOnClass).each(function() {
                    $(this).removeClass(me.checkOnClass),
                    $(this).addClass(me.checkOffClass)
                })
            },
            closeHandler: function() {
                this.uncheckAllRadio(),
                event.preventDefault(),
                event.stopPropagation(),
                this.returnPreviousScreen()
            },
            applyHandler: function() {
                var selectedItem = this.container.find("." + this.checkOnClass);
                if (selectedItem.length > 0) {
                    var selectedRadio = selectedItem.siblings(".mobile_groupselect_radiobutton_grn").find('input[type="radio"]')
                      , selectedValue = $(selectedRadio).val();
                    this.associate_value_element_back.val(selectedValue).triggerHandler("change");
                    var selectedCaption = selectedItem.parent().siblings().find(".mobile_folderlist_text_overflow_grn")
                      , selectedTilte = $(selectedCaption).html();
                    this.associate_title_element_back.val(selectedTilte).triggerHandler("change")
                }
                this.container.find('input[type="submit"]').button("refresh"),
                this.closeHandler()
            },
            returnPreviousScreen: function() {
                if ("_" == this.previousPage || 0 == this.previousPage.length){
                    location.href = "#";
                    $.mobile.changePage($.mobile.firstPage, {
                        transition: "none",
                        reverse: !0,
                        changeHash: !1,
                        fromHashChange: !0
                    });
                }else {
                    var strCurrentURL, URL, baseURL, previousURL = location.href.split("#")[0] + "#" + this.previousPage;
                    $.mobile.changePage(previousURL, {
                        transition: "none",
                        reverse: !0,
                        changeHash: !1,
                        fromHashChange: !0
                    })   
                    location.href = previousURL
                }
            }
        },
        GroupSelect
    }())
}();
