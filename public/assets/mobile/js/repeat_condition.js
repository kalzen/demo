// namespace
grn.base.namespace("grn.page.schedule.mobile.repeat_condition"),
function() {
    var G = grn.page.schedule.mobile.repeat_condition;
    G.is_loaded || (G.isSaved = false,
    G.init = function() {
        G.container = $("#_repeat_condition"),
        G.btnOK = G.container.find(".mobile_ok_grn"),
        G.btnCancel = G.container.find(".mobile_cancel_grn"),
        G.repeatConditionTitle = $("#repeat_condition_title"),
        G.checkOnClass = "mobile_icon_radiobuttonon_grn",
        G.checkOffClass = "mobile_icon_radiobuttonoff_grn"
    }
    ,
    G.handleCheckRepeatCondition = function(event) {
        G.uncheckAllRadio();
        var item = event.target
          , itemObj = $(item);
        itemObj.removeClass(G.checkOffClass),
        itemObj.addClass(G.checkOnClass),
        itemObj.siblings(".mobile_radiobutton_grn").find('input[type="radio"]').each(function() {
            $(this).prop("check", "checked"),
            G.container.find("#repeat_condition_type").val($(this).val())
        })
    }
    ,
    G.uncheckAllRadio = function() {
        G.container.find('input[type="radio"]').each(function() {
            $(this).prop("check", false)
        }),
        G.container.find("." + G.checkOnClass).each(function() {
            $(this).removeClass(G.checkOnClass),
            $(this).addClass(G.checkOffClass)
        })
    }
    ,
    G.setCondition = function() {
        G.container.find("." + G.checkOnClass).each(function() {
            var thisObj, liTag = $(this).closest("li"), conditionCaption = "";
            liTag.hasClass("mobile_schedule_repeat_everyday") && (conditionCaption = liTag.find(".mobile_text_grn").html(),
            G.repeatConditionTitle.html(conditionCaption)),
            liTag.hasClass("mobile_schedule_repeat_weekday") && (conditionCaption = liTag.find(".mobile_text_grn").html(),
            G.repeatConditionTitle.html(conditionCaption)),
            liTag.hasClass("mobile_schedule_repeat_week") && (conditionCaption = liTag.find("#menu_week > .mobile_event_menu_content_grn").html(),
            conditionCaption += " " + liTag.find("#menu_weekday > .mobile_event_menu_content_grn").html(),
            G.repeatConditionTitle.html(conditionCaption)),
            liTag.hasClass("mobile_schedule_repeat_month") && (conditionCaption = liTag.find(".mobile_textmargin_grn").html(),
            conditionCaption += " " + liTag.find("#menu_monthday > .mobile_event_menu_content_grn").html(),
            G.repeatConditionTitle.html(conditionCaption))
        })
    }
    ,
    G.handleApply = function() {
        G.setCondition(),
        G.saveLastChoice(),
        $.mobile.changePage($.mobile.firstPage, {
            transition: "none",
            reverse: !0,
            changeHash: !1,
            fromHashChange: !0
        }); 
        location.href = "#";
    }
    ,
    G.handleCancel = function() {
        G.isSaved && G.returnLastChoice(),
         $.mobile.changePage($.mobile.firstPage, {
            transition: "none",
            reverse: !0,
            changeHash: !1,
            fromHashChange: !0
        });           
        location.href = "#";
    }
    ,
    G.saveLastChoice = function() {
        G.isSaved = true,
        G.container.find('input[type="radio"]').each(function() {
            var thisObj = $(this);
            if ("checked" == thisObj.prop("check"))
                return G.lastCheck = thisObj.attr("id"),
                false
        }),
        G.lastMenuWeekValue = $("#menu_week_value").val(),
        G.lastMenuWeekTitle = $("#menu_week > .mobile_event_menu_content_grn").html(),
        G.lastMenuWeekDayValue = $("#menu_weekday_value").val(),
        G.lastMenuWeekDayTitle = $("#menu_weekday > .mobile_event_menu_content_grn").html(),
        G.lastMenuMonthDayValue = $("#menu_monthday_value").val(),
        G.lastMenuMonthDayTitle = $("#menu_monthday > .mobile_event_menu_content_grn").html()
    }
    ,
    G.returnLastChoice = function() {
        G.selectRadioBtn(G.lastCheck),
        $("#menu_week_value").val(G.lastMenuWeekValue),
        $("#menu_week_title").val(G.lastMenuWeekTitle).triggerHandler("change"),
        $("#menu_weekday_value").val(G.lastMenuWeekDayValue),
        $("#menu_weekday_title").val(G.lastMenuWeekDayTitle).triggerHandler("change"),
        $("#menu_monthday_value").val(G.lastMenuMonthDayValue),
        $("#menu_monthday_title").val(G.lastMenuMonthDayTitle).triggerHandler("change")
    }
    ,
    G.selectRadioBtn = function(id) {
        G.uncheckAllRadio();
        var radio = $("#" + id), liTag, iconBtn;
        radio.prop("check", "checked"),
        radio.closest("li").find(".mobile_icon_radiobutton_grn").addClass(G.checkOnClass)
    }
    ,
    $(document).ready(function() {
        G.init(),
        G.container.find(".mobile_icon_radiobutton_grn").click(G.handleCheckRepeatCondition),
        G.btnOK.click(G.handleApply),
        G.btnCancel.click(G.handleCancel)
    }),
    G.is_loaded = true)
}();
