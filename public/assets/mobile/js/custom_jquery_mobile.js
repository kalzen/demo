!function($){$(document).bind("mobileinit",function(){$.mobile.ajaxEnabled=false,$.mobile.pushStateEnabled=false;var need_hash_regexps=[new RegExp(/\/space\/mobile\/application\/todo\/add\?/i),new RegExp(/\/space\/mobile\/application\/todo\/add\.csp\?/i),new RegExp(/\/space\/mobile\/application\/todo\/modify\?/i),new RegExp(/\/space\/mobile\/application\/todo\/modify\.csp\?/i),new RegExp(/\/schedule\/mobile\//i)];$.each(need_hash_regexps,function(i,regexp){if(location.href.match(regexp))return $.mobile.hashListeningEnabled=true,false;$.mobile.hashListeningEnabled=false}),$.GetURLParameter=function(sParam){var sPageURL=window.location.search;if(void 0===sParam||""==$.trim(sParam))return sPageURL;if(sPageURL.length<1)return"";for(var sURLVariables=(sPageURL=sPageURL.substring(1)).split("&"),i=0;i<sURLVariables.length;i++){var sParameterName=sURLVariables[i].split("=");if(sParameterName[0]==sParam)return sParameterName[1]}return""},$.htmlEscape=function(str){return String(str).replace(/&/g,"&amp;").replace(/"/g,"&quot;").replace(/'/g,"&#39;").replace(/</g,"&lt;").replace(/>/g,"&gt;")},Object.size=function(obj){var size=0,key;for(key in obj)obj.hasOwnProperty(key)&&size++;return size}})}(jQuery);