// namespace
grn.base.namespace("grn.page.schedule.mobile.schedule_add"),
function() {
    var G = grn.page.schedule.mobile.schedule_add;
    G.is_loaded || (G.init = function() {
        G.startYearSelect = $("#start_year"),
        G.startMonthSelect = $("#start_month"),
        G.startDaySelect = $("#start_day"),
        G.startHourSelect = $("#start_hour"),
        G.startMinuteSelect = $("#start_minute"),
        G.endYearSelect = $("#end_year"),
        G.endMonthSelect = $("#end_month"),
        G.endDaySelect = $("#end_day"),
        G.endHourSelect = $("#end_hour"),
        G.endMinuteSelect = $("#end_minute"),
        G.startTimezone = $("#timezone"),
        G.endTimezone = $("#end_timezone"),
        G.validateDateStyle = $("#validate_date"),
        G.invalidDateStyle = $("#invalid_date"),
        G.addBtn = $("#addBtn"),
        G.allday = $("#notime"),
        G.changeRepeatAdd = $("#tab-repeat-schedule"),
        G.usingPurposeElement = $("#using_purpose_element"),
        G.approval = $("#approval"),
        G.timezoneInfo = GRN_TimezoneInfo
    }
    ,
    G.setEndTime = function() {
        "" != G.startHourSelect.val() && "" == G.startMinuteSelect.val() && "" == G.endHourSelect.val() && "" == G.endMinuteSelect.val() && ("23" == G.startHourSelect.val() ? G.endHourSelect.val(parseInt(G.startHourSelect.val())).trigger("change") : G.endHourSelect.val(parseInt(G.startHourSelect.val()) + 1).trigger("change"),
        G.endMinuteSelect.val(0).trigger("change"),
        G.startMinuteSelect.val(0).trigger("change"))
    }
    ,
    G.onChangeDateTime = function() {
        var startYear = parseInt(G.startYearSelect.val()),
        startMonth = parseInt(G.startMonthSelect.val()),
        startDay = parseInt(G.startDaySelect.val()),
        startHour = G.startHourSelect.val(),
        startMinute = G.startMinuteSelect.val(),
        endYear = parseInt(G.endYearSelect.val()),
        endMonth = parseInt(G.endMonthSelect.val()),
        endDay = parseInt(G.endDaySelect.val()),
        endHour = G.endHourSelect.val(),
        endMinute = G.endMinuteSelect.val(),
        startTransitions = G.timezoneInfo[G.startTimezone.val()],
        endTransitions = G.timezoneInfo[G.endTimezone.val()],
        startTransitionsLength = startTransitions.length,
        endTransitionsLength = endTransitions.length, startUTCTS, endUTCTS, startTS, endTS, startTransition, endTransition, hours, minutes, i;
        if (G.validateForm(),
        "" === startHour && "" !== endHour)
            return G.validateDateStyle.show(),
            void G.addBtn.button("disable");
        if (startTransitions && endTransitions) {
            if ("" !== startHour && "" !== endHour) {
                if ("" === startMinute && (startMinute = 0),
                "" === endMinute && (endMinute = 0),
                startUTCTS = Date.UTC(startYear, startMonth - 1, startDay, startHour, startMinute, 0) / 1e3,
                endUTCTS = Date.UTC(endYear, endMonth - 1, endDay, endHour, endMinute, 0) / 1e3,
                startTS = parseInt(startUTCTS) - parseInt(startTransitions[0].offset),
                endTS = parseInt(endUTCTS) - parseInt(endTransitions[0].offset),
                startTransitions[0].ts > startTS || endTransitions[0].ts > endTS)
                    return void G.validateForm();
                for (i = 1; i < startTransitionsLength && startTransitions[i].ts < parseInt(startUTCTS) - parseInt(startTransitions[i].offset); )
                    i += 1;
                for (startTransition = startTransitions[i - 1],
                startTS = parseInt(startUTCTS) - parseInt(startTransition.offset),
                i = 1; i < endTransitionsLength && endTransitions[i].ts < parseInt(endUTCTS) - parseInt(endTransitions[i].offset); )
                    i += 1;
                if (endTransition = endTransitions[i - 1],
                (endTS = parseInt(endUTCTS) - parseInt(endTransition.offset)) < startTS)
                
                    return G.invalidDateStyle.show(),
                    void G.addBtn.button("disable")
            }
        } else
            G.addBtn.button("disable")
    }
    ,
    G.validateForm = function() {
        G.validateDateStyle.hide(),
        G.invalidDateStyle.hide(),
        G.addBtn.button("enable")
    }
    ,
    G.disableTime = function(flag) {
        flag ? (G.startHourSelect.selectmenu("disable"),
        G.startMinuteSelect.selectmenu("disable"),
        G.endHourSelect.selectmenu("disable"),
        G.endMinuteSelect.selectmenu("disable"),
        G.startHourSelect.find("option:contains('--')").first().prop("selected", "selected").trigger("change"),
        G.startMinuteSelect.find("option:contains('--')").first().prop("selected", "selected").trigger("change"),
        G.endHourSelect.find("option:contains('--')").first().prop("selected", "selected").trigger("change"),
        G.endMinuteSelect.find("option:contains('--')").first().prop("selected", "selected").trigger("change")) : (G.startHourSelect.selectmenu("enable"),
        G.startMinuteSelect.selectmenu("enable"),
        G.endHourSelect.selectmenu("enable"),
        G.endMinuteSelect.selectmenu("enable"))
    }
    ,
    G.checkAllDay = function() {
        G.allday.hasClass("mobile-checkboxOff-todo-grn") ? G.disableTime(true) : G.disableTime(false)
    }
    ,
    G.switchOption = function() {
        var parent = $(this).parent();
        parent.hide(),
        parent.siblings(".mobile_switch_button_grn").show();
        var switchValue = parent.siblings("input").val();
        1 == parseInt(switchValue) ? parent.siblings("input").val("") : parent.siblings("input").val(1)
    }
    ,
    G.privateRaidoButton = function(event) {
        var checkOnClass = "mobile_icon_radiobuttonon_grn"
          , checkOffClass = "mobile_icon_radiobuttonoff_grn";
        $("#idPrivateRaidoButton").find(".mobile_icon_radiobutton_grn").each(function() {
            var eachObject = $(this);
            eachObject.hasClass(checkOnClass) && eachObject.removeClass(checkOnClass),
            eachObject.hasClass(checkOffClass) || eachObject.addClass(checkOffClass)
        });
        var element = $(event.target);
        element.hasClass(checkOffClass) && (element.removeClass(checkOffClass),
        element.addClass(checkOnClass),
        element.siblings(".mobile_radiobutton_grn").find('input[type="radio"]').each(function() {
            var private_setting = $(this).val();
            $("#idPrivateSetting").val(private_setting),
            2 == private_setting ? $("#idPrivateSettingPublicTo").show() : $("#idPrivateSettingPublicTo").hide()
        }))
    }
    ,
    G.checkFacility = function(event) {
        var check_repeat = $("#checkrepeat").val().split(":")
          , check_approval = $("#approval").val().split(":");
        check_repeat.indexOf("0") >= 0 || check_approval.indexOf("1") >= 0 ? grn.component.msgbox_mobile.MsgBox.show(G.msgRepeat, G.titleRepeat, grn.component.msgbox_mobile.MsgBoxButtons.yesno, {
            ui: {},
            caption: {
                yes: G.yes,
                no: G.no
            },
            callback: function(result, form) {
                result == grn.component.msgbox_mobile.MsgBoxResult.yes && add_menu_submit("repeat"),
                grn.component.msgbox_mobile.MsgBox._remove()
            }
        }) : add_menu_submit("repeat"),
        event.stopPropagation()
    }
    ,
    G.checkToggleUsingPurpose = function(event) {
        var approvalItems;
        "undefined" != G.usingPurposeElement && ($("#approval").val().split(":").indexOf("1") >= 0 ? G.usingPurposeElement.show() : G.usingPurposeElement.hide())
    }
    ,
    window.checked_redirect = false,
    $(document).on("pagebeforecreate", function(event) {
        if (location.hash && location.hash.length > 1 && !window.checked_redirect) {
            var index = location.href.indexOf("#");
            location.href = location.href.split("#")[0]
        }
        window.checked_redirect = true
    }),
    $(document).on("pagechange", function(event) {
        G.init(),
        G.onChangeDateTime(),
        G.startHourSelect.change(G.onChangeDateTime),
        G.startMinuteSelect.change(G.onChangeDateTime),
        G.startYearSelect.change(G.onChangeDateTime),
        G.startMonthSelect.change(G.onChangeDateTime),
        G.startDaySelect.change(G.onChangeDateTime),
        G.endHourSelect.change(G.onChangeDateTime),
        G.endMinuteSelect.change(G.onChangeDateTime),
        G.endYearSelect.change(G.onChangeDateTime),
        G.endMonthSelect.change(G.onChangeDateTime),
        G.endDaySelect.change(G.onChangeDateTime),
        G.startHourSelect.change(G.setEndTime),
        G.allday.click(G.checkAllDay),
        $(".mobile_icon_radiobutton_grn").click(G.privateRaidoButton),
        $(".mobile_schedule_switch_type_grn").click(G.switchOption),
        G.changeRepeatAdd.click(G.checkFacility)
    }),
    $(document).on("pagecreate", function(event) {
        var height = $("#textarea_id").css("height");
        $("#using_purpose").css({
            height: height
        })
    }),
    G.is_loaded = true)
}();
