grn.base.namespace("grn.page.schedule.mobile.group_day"),
function($) {
    $(document).ready(function() {
        var _keywords_click = "click", _keywords_tap = "tap", _keywords_keydown = "keydown", _keywords_search = "search", _keywords_resize = "resize", _keywords_post = "POST", _keywords_uids = "uids", _keywords_fids = "fids", _keywords_selected = "selected", _keywords_change = "change", _keywords_bdate = "bdate", _keywords_sp = "sp", _keywords_gid = "gid", _keywords_search_text = "search_text", searchPart = new grn.Component.Autocomplete.AutocompleteUserFaci, $personal_week_command;
        searchPart.init(),
        $("div.mobile_schedule_week_header_grn div.mobile_left_grn, div.mobile_schedule_week_header_grn div.mobile_right_grn").on(_keywords_click, function(e) {
            e.preventDefault(),
            $(this).children("a").length > 0 && (window.location.href = $(this).children("a").attr("href"))
        });
        var search_action = function(ed) {
            var gid, search_text;
            ed.preventDefault(),
            search_text = encodeURIComponent($("input.tbsearch").val().trim());
            var uids_fids = searchPart.PrepareUidsFidsForServerSearch()
              , uids = uids_fids[0]
              , fids = uids_fids[1];
            gid = "search";
            var _domain = window.location.href.replace("index", "group_day")
              , url_redirect = (_domain = _domain.substr(0, _domain.indexOf("?"))) + "?search_text=" + search_text + "&gid=" + gid + "&uids=" + uids + "&fids=" + fids;
            return $.GetURLParameter(_keywords_sp).length > 0 && (url_redirect += "&sp=0"),
            url_redirect.length > 0 && url_redirect.indexOf("?") < 0 && "&" == url_redirect[0] && (url_redirect = url_redirect.replace("&", "?")),
            window.location.href = url_redirect,
            0
        };
        $(window).on(_keywords_keydown, function(ed) {
            13 == ed.keyCode && search_action(ed)
        }),
        $("a#mobile_event_search").on(_keywords_tap, function(e) {
            e.preventDefault(),
            search_action(e)
        });
        var url_parameter = $.GetURLParameter()
          , $_div = $("div.mobile_switch_button_grn> div");
        "" != $.GetURLParameter(_keywords_gid) && $.GetURLParameter(_keywords_gid).indexOf("f") > -1 ? ($("div.mobile_switch_button_grn> a").replaceWith($("<div/>", {
            class: "mobile_right_grn mobile_selected_grn mobile_img_facility_on_grn"
        })),
        $_div.replaceWith($("<a/>", {
            class: "mobile_left_grn mobile_unselected_grn mobile_img_user_off_grn ui-link"
        }))) : $_div.addClass("mobile_img_user_on_grn"),
        $("div.mobile_switch_button_grn> a").on(_keywords_tap, function(e) {
            e.preventDefault();
            var facility_action = $(this).hasClass("mobile_img_facility_off_grn")
              , gid = $.GetURLParameter(_keywords_gid)
              , p_ = $.GetURLParameter("p");
            if (null != gid && "f" != gid && "fr" != gid && "f" != gid.substr(0, 1) || facility_action && "" == gid ? (url_parameter = url_parameter.indexOf("gid=") > -1 && "f" != gid && "fr" != gid ? url_parameter.replace("&gid=" + gid, "&gid=f").replace("?gid=" + gid, "?gid=f") : url_parameter.indexOf("?") > -1 && "f" != gid && "fr" != gid ? "&gid=f" : "?gid=f",
            null != p_ && "" != p_ && (url_parameter = url_parameter.replace("&p=" + p_, "&p=").replace("?p=" + p_, "?p="))) : url_parameter = "?bdate=" + $.GetURLParameter(_keywords_bdate) + "&gid=",
            url_parameter.indexOf("sp=") > -1) {
                var sp_number = $.GetURLParameter("sp");
                url_parameter = url_parameter.replace("&sp=" + sp_number, "&sp=0").replace("?sp=" + sp_number, "?sp=0")
            }
            if (url_parameter.indexOf("uids=") > -1) {
                var uids = $.GetURLParameter("uids");
                url_parameter = url_parameter.replace("&uids=" + uids, "&uids=").replace("?uids=" + uids, "?sp=")
            }
            if (url_parameter.indexOf("fids=") > -1) {
                var fids = $.GetURLParameter("fids");
                url_parameter = url_parameter.replace("&fids=" + fids, "&fids=").replace("?fids=" + fids, "?fids=")
            }
            if (url_parameter.indexOf("search_text=") > -1) {
                var search_text = $.GetURLParameter("search_text");
                url_parameter = url_parameter.replace("&search_text=" + search_text, "&search_text=").replace("?search_text=" + search_text, "?search_text=")
            }
            window.location.href = url_parameter
        }),
        $("a.mobile_icon_week_grn").on(_keywords_click, function(e) {
            e.preventDefault();
            var url = window.location.href;
            url = url.replace(window.location.search, "").replace(/group_day/gi, "index"),
            url += "?bdate=" + $.GetURLParameter(_keywords_bdate) + "&uid=" + $(this).attr("data-answer"),
            window.location.href = url
        }),
        $("#hf_user_facility_selected_").on(_keywords_change, function() {
            var url_params = $.GetURLParameter().replace("&uids=" + $.GetURLParameter("uids"), "").replace("?uids=" + $.GetURLParameter("uids"), "").replace("&fids=" + $.GetURLParameter("fids"), "").replace("?fids=" + $.GetURLParameter("fids"), "").replace("?search_text=" + $.GetURLParameter(_keywords_search_text), "").replace("&search_text=" + $.GetURLParameter(_keywords_search_text), "").replace("?type_search=" + $.GetURLParameter("type_search"), "").replace("&type_search=" + $.GetURLParameter("type_search"), "")
              , user_group_on = $("div.mobile_switch_button_grn> div").hasClass("mobile_img_user_on_grn")
              , id_selected = $(this).val()
              , gid = $.GetURLParameter(_keywords_gid);
            if ("" == id_selected || id_selected != _keywords_search) {
                var isNumber_;
                if (user_group_on)
                    "" == gid && "" == id_selected || (url_params.length > 0 && url_params.indexOf(_keywords_gid) > -1 ? url_params = url_params.replace("&gid=" + gid, "&gid=" + id_selected).replace("?gid=" + gid, "?gid=" + id_selected) : url_params.length > 0 && url_params.indexOf("?") > -1 ? url_params += "&gid=" + id_selected : url_params += "?gid=" + id_selected);
                else if (/^[0-9]+$/.test(gid))
                    url_params = url_params.replace("&gid=" + gid, "&gid=f" + id_selected).replace("?gid=" + gid, "?gid=f" + id_selected).replace("&p=1", "&p=").replace("?p=1", "?p=");
                else {
                    var id_result = new RegExp(/\d+/g).exec(id_selected);
                    id_result = null != id_result && "-2" != id_selected ? id_result[0] : "xr" == id_selected ? "r" : "-2" == id_selected ? "u" : "",
                    url_params = url_params.replace("&gid=" + gid, "&gid=f" + id_result).replace("?gid=" + gid, "?gid=f" + id_result).replace("?p=1", "?p=").replace("&p=1", "&p=")
                }
                var sp = $.GetURLParameter(_keywords_sp);
                "" != sp && (url_params = url_params.replace("&sp=" + sp, "").replace("?sp=" + sp, "")),
                url_params.length > 0 && url_params.indexOf("?") < 0 && "&" == url_params[0] && (url_params = url_params.replace("&", "?")),
                window.location.href = url_params
            }
        }),
        $('input[type="hidden"]#start_set, input[type="hidden"]#date_select_footer_set').on(_keywords_change, function() {
            var new_bdate = "bdate=" + $(this).val()
              , url = $.GetURLParameter();
            "" != $.GetURLParameter(_keywords_bdate) ? url = url.replace("bdate=" + $.GetURLParameter(_keywords_bdate), new_bdate) : url.indexOf("?") > -1 ? url += "&" + new_bdate : url += "?" + new_bdate,
            window.location.href = url
        });
        var G = grn.page.schedule.mobile.group_day;
        $("div#operate_menu> .mobile_scroll_area_grn:first").on("touchstart", function(e) {
            e.preventDefault();
            var _thisForm = $("form").empty();
            _thisForm.attr({
                id: "tmpaddRediectForm",
                action: G.Parameters.href,
                method: "POST"
            }),
            _thisForm.append($("<input type='hidden' id='selected_users_sUID[]' name='selected_users_sUID[]' />").val(G.Parameters.selected_users_sUID)),
            _thisForm.append($("<input type='hidden' id='sITEM[]' name='sITEM[]' />").val(G.Parameters.sITEM)),
            _thisForm.append($("<input type='hidden' id='bdate' name='bdate' />").val(G.Parameters.bdate)),
            _thisForm.append($("<input type='hidden' id='referer_key' name='referer_key' />").val(G.Parameters.referer_key)),
            _thisForm.append($("<input type='hidden' id='tab' name='tab' />").val("normal")),
            _thisForm.submit()
        }),
        $("#footer_bar_b").on("click", function(e) {
            if (e.preventDefault(),
            "undefined" != typeof G.Parameters) {
                var url_parameters = "uid=" + G.Parameters.uid + "&bdate=" + G.Parameters.bdate;
                window.location.href = G.Parameters.href + "&" + url_parameters + "&referer_key=" + G.Parameters.referer_key
            }
        }),
        $("div.mobile_schedule_day_header_grn, div.mobile_select_grn, a.mobile_select_view_grn").css("margin-top", "0")
    }),
    $(document).on("pageinit", "body div[data-role=page]:first", function() {
        $("#_popup_user_categories_tree").appendTo("body"),
        $("#_popup_facility_categories_tree").appendTo("body"),
        $("#_popup_user_categories_tree").css("margin-bottom", 63),
        $("#_popup_facility_categories_tree").css("margin-bottom", 63)
    })
}(jQuery);
