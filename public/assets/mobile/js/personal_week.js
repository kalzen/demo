!function($) {
    grn.base.namespace("grn.page.schedule.mobile.personal_week"),
    grn.page.schedule.mobile.personal_week.PersonalWeek || (grn.page.schedule.mobile.personal_week.PersonalWeek = function(options) {
        var bdate = options.bdate || ""
          , uid = options.uid || ""
          , gid = options.gid || ""
          , referer_key = options.referer_key || ""
          , url = options.url || window.location.href;
        this.goToAddPage = function() {
            "" != url && url.indexOf("?") > -1 && (url = url.substr(0, url.indexOf("?"))),
            url = (url = url.replace(/personal_week/gi, "add")) + "?bdate=" + bdate + "&uid=" + uid + "&gid=" + gid + "&referer_key=" + referer_key,
            window.location.href = url
        }
    }
    ,
    $(document).on("pageinit", function() {
        var add_event = function(e) {
            var _this = $(e), options = {
                bdate: _this.attr("data-bdate"),
                uid: _this.attr("data-uid"),
                gid: _this.attr("data-gid"),
                referer_key: _this.attr("data-ref_key"),
                url: _this.attr("data-url"),
            }, personal_week;
            new grn.page.schedule.mobile.personal_week.PersonalWeek(options).goToAddPage()
        };
        $(".mobile_week_day_title_grn").children(".mobile_right_icon_grn").on("click", function() {
            add_event(this)
        }),
        $("#footer_bar_b").on("click", function(e) {
            var personal_week;
            e.preventDefault(),
            new grn.page.schedule.mobile.personal_week.PersonalWeek(grn.page.schedule.mobile.personal_week.Parameters).goToAddPage()
        }),
        $("li.no-appointment").parent().addClass("mobile_no_appointment_ul_grn")
    }))
}(jQuery);
