!function($) {
    if (!grn.base.isNamespaceDefined("grn.Component.Autocomplete")) {
        grn.base.namespace("grn.Component.Autocomplete"),
        grn.Component.Autocomplete.AutocompleteUserFaci = function() {}
        ;
        var _keywords_click = "click"
          , _keywords_keydown = "keydown"
          , _keywords_keyup = "keyup"
          , _keywords_keypress = "keypress"
          , _keywords_input = "input"
          , _keywords_search = "search"
          , _keywords_resize = "resize"
          , _keywords_post = "POST"
          , _keywords_uids = "uids"
          , _keywords_fids = "fids"
          , _keywords_selected = "selected"
          , _keywords_change = "change"
          , _keywords_bdate = "bdate"
          , _keywords_sp = "sp"
          , _keywords_gid = "gid"
          , _keywords_search_text = "search_text"
          , _elements_icon_search = "#icon_search_grn"
          , _elements_dropDownList = ".mobile_pulldown_user_incremental_search_grn"
          , _elements_dropDownListSelected = "div.mobile_userlines_grn"
          , _elements_suggestion_part = ".mobile_schedulelist_search_grn"
          , _elements_search_input = "input.tbsearch"
          , _elements_search_input_part = ".mobile_user_search_grn"
          , _elements_icon_triangle = "a#icon_search_grn div.mobile_triangle_grn"
          , _elements_icon_delete_input = "a.mobile_delete_icon_grn"
          , _elements_icon_delete_item = "a.mobile_delete_icon_normal_grn"
          , _elements_title_bar = "div.mobile_breadcrumbtitle_right_grn"
          , _elements_mobile_user_search_grn = ".mobile_user_search_grn"
          , _elements_mobile_user_list_grn = "ul.mobile_user_list_grn"
          , _elements_mobile_schedule_week_header_grn = "div.mobile_schedule_week_header_grn"
          , _elements_mobile_user_list_scroll_grn = "div.mobile_user_list_scroll_grn"
          , _elements_hfPageLoad = "#hfPageLoad"
          , _elements_hfClearTbSearch = "#hfClearTbSearch"
          , _css_class_mobile_triangle_grn = "mobile_triangle_grn"
          , _css_class_mobile_user_search_focus_grn = "mobile_user_search_focus_grn"
          , _key_enter = 13
          , _key_tab = 9
          , _key_comma = 188
          , _key_backspace = 8
          , _key_leftarrow = 37
          , _key_uparrow = 38
          , _key_rightarrow = 39
          , _key_downarrow = 40
          , _key_exclamation = 33
          , _key_slash = 47
          , _key_colon = 58
          , _key_at = 64
          , _key_squarebricket_left = 91
          , _key_apostrof = 96
          , _key_del = 46
          , _key_esc = 27
          , G = grn.Component.Autocomplete.AutocompleteUserFaci;
        G.Msg = {},
        G.Parameters = {},
        G.prototype = {
            init: function() {
                Object.size(G.Parameters) < 1 || ($(_elements_hfPageLoad).length < 1 && console.log("Please register hidden field for auto-complete"),
                this.SearchCommand(G.Parameters))
            },
            _setOptions: function(options) {
                this._url = options.url || null
            },
            SearchCommand: function(options) {
                var fn = this;
                $(_elements_search_input).trigger("touchstart"),
                $(_elements_search_input).on("touchstart", function() {
                    $(this).focus()
                }),
                $(_elements_icon_search).on(_keywords_click, function() {
                    if ($(_elements_dropDownList).hide(),
                    $(_elements_icon_triangle).length > 0 ? $(_elements_icon_triangle).remove() : $(this).append($("<div/>", {
                        class: _css_class_mobile_triangle_grn
                    })),
                    $(_elements_suggestion_part).toggle(),
                    "block" == $(_elements_suggestion_part).css("display")) {
                        $(this).removeClass("mobile_schedulelist_icon_search_grn").addClass("mobile_schedulelist_icon_search_off_grn");
                        var search_text = G.Msg.search_text;
                        if ("" == $(_elements_hfClearTbSearch).val()) {
                            var search_text_decoded = $("<div/>").html(search_text).text();
                            $(_elements_search_input).val(search_text_decoded).focus()
                        } else
                            $(_elements_search_input).focus();
                        var str_uids = ""
                          , str_fids = "";
                        if ("" != $(_elements_hfPageLoad).val()) {
                            var result = fn.PrepareUidsFidsForServerSearch();
                            str_uids = result[0],
                            str_fids = result[1]
                        } else
                            str_uids = $.GetURLParameter(_keywords_uids),
                            str_fids = $.GetURLParameter(_keywords_fids),
                            "" == $(_elements_search_input).val() && ($(_elements_search_input).val(""),
                            $(_elements_search_input).autocomplete(_keywords_search, ""));
                        var uids_fids = fn._computedUidsFids(str_uids, str_fids);
                        (uids_fids[0].length > 0 || uids_fids[1].length > 0) && $.ajax({
                            type: _keywords_post,
                            url: options.url,
                            data: {
                                uids: uids_fids[0],
                                fids: uids_fids[1],
                                flag: _keywords_selected,
                                referer_key: options.referer_key
                            },
                            beforeSend: function() {
                                grn.component.mobile_loading.show()
                            },
                            complete: function() {
                                grn.component.mobile_loading.remove()
                            }
                        }).done(function(data) {
                            if ("undefined" != typeof data && "undefined" != typeof data.user && "undefined" != typeof data.facility)
                                $(_elements_dropDownListSelected).empty(),
                                Object.size(data.user) < 1 && Object.size(data.facility) < 1 || $.each(data, function(key, value) {
                                    $.each(value, function(key1, value1) {
                                        "undefined" != typeof value1._id && fn._createdAppendListItemSelected(value1._id, value1.col_display_name, key, value1.primary_group)
                                    })
                                });
                            else if ("undefined" != typeof data)
                                return document.write(data),
                                void document.close()
                        }).fail(function(xhr) {
                            document.write(xhr.responseText),
                            document.close()
                        })
                    } else
                        $(this).addClass("mobile_schedulelist_icon_search_grn").removeClass("mobile_schedulelist_icon_search_off_grn")
                }),
                $(_elements_search_input).focus(function() {
                    $(_elements_mobile_user_search_grn).addClass(_css_class_mobile_user_search_focus_grn),
                    "" == $(_elements_search_input).val() ? ($(_elements_dropDownList).hide(),
                    $(_elements_icon_delete_input).hide()) : $(_elements_icon_delete_input).show()
                }),
                $(_elements_search_input).blur(function() {
                    setTimeout(function() {
                        "" == $(_elements_search_input).val() ? $(_elements_icon_delete_input).hide() : $(_elements_icon_delete_input).show(),
                        $(_elements_mobile_user_search_grn).removeClass(_css_class_mobile_user_search_focus_grn)
                    }, 300)
                }),
                $(_elements_search_input).on(_keywords_input, function(event) {
                    "" == $(event.target).val() && ($(_elements_icon_delete_input).hide(),
                    $(_elements_search_input).autocomplete(_keywords_search, ""),
                    $(_elements_dropDownList).hide())
                }),
                $(_elements_search_input).on(_keywords_keyup, function(eu) {
                    eu.keyCode != _key_backspace && eu.keyCode != _key_del || 0 != $(_elements_search_input).val().length || ($(_elements_icon_delete_input).hide(),
                    $(_elements_search_input).autocomplete(_keywords_search, ""),
                    $(_elements_dropDownList).hide())
                }),
                $(_elements_search_input).on(_keywords_keydown, function(ed) {
                    $(_elements_mobile_user_search_grn).addClass(_css_class_mobile_user_search_focus_grn),
                    ed.keyCode != _key_backspace && ed.keyCode != _key_del && ed.keyCode != _key_esc && ed.keyCode != _key_uparrow && ed.keyCode != _key_leftarrow && ed.keyCode != _key_rightarrow && ed.keyCode != _key_downarrow && ed.keyCode != _key_tab && ed.keyCode != _key_enter && $(_elements_icon_delete_input).show(),
                    ed.keyCode == _key_backspace && ($(_elements_search_input).val().length = 0) && ($(_elements_search_input).autocomplete(_keywords_search, ""),
                    $(_elements_dropDownList).hide())
                }),
                $(_elements_search_input).on(_keywords_keypress, function(ep) {
                    ep.keyCode != _key_backspace && ep.keyCode != _key_del || 0 != $(_elements_search_input).val().length || ($(_elements_icon_delete_input).hide(),
                    $(_elements_search_input).autocomplete(_keywords_search, ""),
                    $(_elements_dropDownList).hide())
                }),
                $(_elements_icon_delete_input).on(_keywords_click, function() {
                    $(_elements_search_input).val(""),
                    $(_elements_hfClearTbSearch).val("1"),
                    $(_elements_icon_delete_input).hide(),
                    $(_elements_search_input).attr("placeholder", G.Msg.place_holder),
                    $(_elements_search_input).trigger("touchstart"),
                    $(_elements_search_input).on("touchstart", function() {
                        $(this).focus()
                    })
                }),
                $(_elements_search_input).autocomplete({
                    source: function(request) {
                        $.ajax({
                            type: _keywords_post,
                            url: options.url,
                            data: {
                                p: request.term,
                                flag: _keywords_search,
                                referer_key: options.referer_key
                            },
                            beforeSend: function() {
                                grn.component.mobile_loading.show()
                            },
                            complete: function() {
                                grn.component.mobile_loading.remove()
                            }
                        }).done(function(data) {
                            var $drop_down = $(_elements_dropDownList);
                            void 0 == data || void 0 == data.user && void 0 == data.facility || Object.size(data.user) < 1 && Object.size(data.facility) < 1 ? $drop_down.hide() : $drop_down.show();
                            var itemTemplate = '<li data-icon="false" data-corners="false" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-iconpos="right" data-theme="c">                                                    <a href="#" class="ui-btn">                                                        <div class="mobile_user_photo_grn mobile_img_userPlofile_grn"></div>                                                        <div class="mobile_info_grn">                                                            <div class="mobile_position_center_grn">                                                                <div class="mobile_position_width_grn">                                                                    <div class="mobile_user_grn"> {{col_display_name}} {{col_nickname}}</div>                                                                    <div class="mobile_text_grn">{{primary_group}}</div>                                                                </div>                                                            </div>                                                        </div>                                                    </a>                                            </li>';
                            itemTemplate = $(itemTemplate),
                            $(_elements_mobile_user_list_grn).empty();
                            var limit = 0;
                            if ("undefined" == typeof data.facility || "undefined" == typeof data.user)
                                return document.write(data),
                                void document.close();
                            $.each(data, function(key, value) {
                                $.each(value, function(key1, value1) {
                                    if (!(limit > 19)) {
                                        limit++;
                                        var item_i = itemTemplate.clone()
                                          , primary_group = "undefined" !== typeof value1.primary_group ? value1.primary_group : ""
                                          , nick_name = "undefined" !== typeof value1.col_nickname && "" !== value1.col_nickname ? "(" + value1.col_nickname + ")" : "";
                                        item_i.find("div.mobile_user_grn").text(value1.col_display_name + nick_name),
                                        item_i.find("div.mobile_text_grn").text(primary_group),
                                        item_i.find("div.mobile_user_photo_grn").addClass("mobile_img_" + key + "Plofile_grn"),
                                        item_i.data("data-cb-id", value1._id),
                                        item_i.data("data-cb-display_name", value1.col_display_name),
                                        item_i.data("data-cb-primary_group", primary_group),
                                        item_i.data("data-cb-item_type", key),
                                        $(_elements_mobile_user_list_grn).append(item_i)
                                    }
                                })
                            }),
                            $("div.mobile_pulldown_user_incremental_search_grn div.mobile_user_list_scroll_grn ul> li").on(_keywords_click, function() {
                                $(_elements_search_input).val("").focus(),
                                fn._createdAppendListItemSelected($(this).data("data-cb-id"), $(this).data("data-cb-display_name"), $(this).data("data-cb-item_type"), $(this).data("data-cb-primary_group")),
                                $drop_down.hide(),
                                setTimeout(function() {
                                    $(_elements_search_input).val(""),
                                    $(_elements_search_input).autocomplete(_keywords_search, "")
                                }, 50)
                            }),
                            fn._computedL()
                        }).fail(function(data) {
                            document.write(data.responseText)
                        })
                    },
                    minLength: 1,
                    delay: 500
                }),
                $(_elements_icon_delete_input).on(_keywords_click, function(e) {
                    e.preventDefault(),
                    $(_elements_dropDownList).hide(),
                    $(_elements_search_input).val(""),
                    $(_elements_search_input).autocomplete(_keywords_search, "")
                }),
                $(window).bind(_keywords_resize, function() {
                    fn._computedL()
                }).trigger(_keywords_resize)
            },
            PrepareUidsFidsForServerSearch: function() {
                var uids = ""
                  , fids = ""
                  , result = [];
                return $("div.mobile_userlines_grn div.mobile_line_grn").each(function() {
                    var _id = $(this).data("id")
                      , _type = $(this).data("type");
                    "user" === _type ? uids += "+" + _id : "facility" == _type && (fids += "+" + _id)
                }),
                uids.length > 0 && (uids = uids.substr(1)),
                fids.length > 0 && (fids = fids.substr(1)),
                result.push(uids),
                result.push(fids),
                result
            },
            _computedUidsFids: function(str_uids, str_fids) {
                var result = [];
                void 0 != str_uids && "" != str_uids || (str_uids = ""),
                void 0 != str_fids && null != str_fids || (str_fids = "");
                for (var uids_tmp = str_uids.split("+"), fids_tmp = str_fids.split("+"), uids = [], fids = [], i = 0; i < uids_tmp.length; i++)
                    $.isNumeric(uids_tmp[i]) && uids_tmp[i] > 0 && uids.push(uids_tmp[i]);
                for (var j = 0; j < fids_tmp.length; j++)
                    $.isNumeric(fids_tmp[j]) && fids_tmp[j] > 0 && fids.push(fids_tmp[j]);
                return result.push(uids),
                result.push(fids),
                result
            },
            _createdAppendListItemSelected: function(id, display, type, group) {
                var itemSelected = $('<div class="mobile_line_grn"/>');
                itemSelected.text(display + ("undefined" !== typeof group && "" !== group ? ";" + group : "")),
                itemSelected.append($('<a href="javascript:void(0);" class="mobile_delete_icon_normal_grn"></a><a href="#" class="mobile_icon_grn"></a>')),
                $(_elements_dropDownListSelected).append($(itemSelected).data("id", id).data("type", type)),
                $(_elements_icon_delete_item).on("click", function(e) {
                    e.preventDefault(),
                    $(this).parent().remove()
                }),
                $(_elements_hfPageLoad).val("0")
            },
            _computedL: function() {
                if ($(_elements_mobile_user_list_grn).children("li").length > 8) {
                    var page_height, exclusive_height, user_list_height = .7 * ($(window).height() - ($(_elements_title_bar).height() + $(_elements_mobile_schedule_week_header_grn).height() + $(_elements_search_input_part).height()));
                    user_list_height < 50 && (user_list_height = 50),
                    $(_elements_mobile_user_list_scroll_grn).height(user_list_height),
                    $(_elements_mobile_user_list_grn).height(user_list_height)
                } else
                    $(_elements_mobile_user_list_scroll_grn).removeAttr("style"),
                    $(_elements_mobile_user_list_grn).removeAttr("style")
            }
        }
    }
}(jQuery);
