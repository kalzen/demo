// namespace
grn.base.namespace("grn.page.schedule.mobile.handle_add"),
function() {
    var G = grn.page.schedule.mobile.handle_add;
    if (!G.is_loaded) {
        G.init = function() {
            G.btnAdd = $("#addBtn"),
            G.btnCancel = $("#cancelBtn"),
            G.usingPurposeElement = $("#using_purpose_element"),
            G.requireUsingPurpose = $("#require_using_purpose"),
            G.usingPurpose = $("#using_purpose")
        }
        ;
        var checkSubmit = false;
        G.ajaxSubmit = function(formId) {
            if (checkSubmit || "undefined" === typeof formId)
                return false;
            var form, submitPage = $("#" + formId).attr("action");
            if ("undefined" !== typeof submitPage) {
                G.setAjaxElement(),
                "undefined" !== typeof G.showRefreshDialog && G.showRefreshDialog && ("undefined" !== typeof G.needRefresh && G.needRefresh ? G.setRefreshAttendanceStatusElement(1) : G.setRefreshAttendanceStatusElement(0)),
                checkSubmit = true,
                grn.component.mobile_loading.show();
                var aFormData = new FormData, file = $("input[type=file]"), checked;
                if (file.length > 0)
                    "undefined" != typeof $("input[name=attached_file]").prop("checked") && aFormData.append("file", file[0].files[0]);
                $("input").each(function(i) {
                    var disabled = $(this).attr("disabled")
                      , name = $(this).attr("name");
                    if ("undefined" == typeof disabled && "undefined" != typeof name) {
                        var type, checked;
                        if ("checkbox" == $(this).attr("type"))
                            if ("undefined" == typeof $(this).prop("checked"))
                                return;
                        aFormData.append(name, $(this).val())
                    }
                }),
                $("select").each(function(i) {
                    var name = $(this).attr("name");
                    "undefined" != typeof name && aFormData.append(name, $(this).val())
                }),
                $("textarea").each(function(i) {
                    var name = $(this).attr("name");
                    "undefined" != typeof name && aFormData.append(name, $(this).val())
                });
                var xhr = new XMLHttpRequest;
                xhr.open("POST", submitPage, true),
                xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest"),
                xhr.onreadystatechange = function(oEvent) {
                    if (4 === this.readyState)
                        if (200 == xhr.status || xhr.status >= 500) {
                            try {
                                data = JSON.parse(xhr.responseText);
                                console.log(data)
                            } catch (e) {
                                return document.write(xhr.responseText),
                                document.close(),
                                false
                            }
                            if (grn.component.mobile_error_handler.hasCybozuError(xhr))
                                grn.component.mobile_loading.remove(),
                                xhr.responseJSON = data,
                                grn.component.mobile_error_handler.show(xhr);
                            else if (grn.component.mobile_loading.remove(),
                            1 == data.conflict_facility)
                                G.createDuplicatedPopup(data);
                            else {
                                var link = data.link;
                                if ("undefined" !== typeof link)
                                    return void (window.location.href = link)
                            }
                            checkSubmit = false,
                            G.removeAjaxElement(),
                            G.removeRefreshAttendanceStatusElement()
                        } else
                            grn.component.mobile_loading.remove(),
                            G.removeAjaxElement(),
                            G.removeRefreshAttendanceStatusElement()
                }
                ,
                xhr.send(aFormData)
            }
        }
        ,
        G.handleSubmit = function(event) {
            var applyRepeatModify = $("#apply");
            if (applyRepeatModify.length > 0) {
                if ("" == applyRepeatModify.val())
                    return $("#validate_range").show(),
                    void $("body").animate({
                        scrollTop: 0
                    }, "500");
                $("#validate_range").hide()
            }
            G.checkUsingPurposeNotEmpty() && (G.ajaxSubmit(G.formId),
            event.stopPropagation())
        }
        ,
        G.setAjaxElement = function() {
            if ($("#use_ajax").length > 0)
                return false;
            var element = $('<input type="hidden" id="use_ajax" name="use_ajax" value="1">');
            return $("#" + G.formId).append(element),
            true
        }
        ,
        G.removeAjaxElement = function() {
            $("#use_ajax").remove()
        }
        ,
        G.setRefreshAttendanceStatusElement = function(value) {
            if ($("#refresh_status").length > 0)
                return false;
            var element = $('<input type="hidden" id="refresh_status" name="refresh_status" value="' + value + '">');
            return $("#" + G.formId).append(element),
            true
        }
        ,
        G.removeRefreshAttendanceStatusElement = function() {
            $("#refresh_status").remove()
        }
        ,
        G.createDuplicatedPopup = function(json_obj) {
            var popupDup = $("#popup_duplicated");
            popupDup.find(".mobile_repeating_table_grn").empty(),
            popupDup.find(".mobile_repeating_table_grn").html(G.header),
            popupDup.find(".mobile_repeating_text_small_grn").remove();
            var frag = document.createDocumentFragment()
              , event_except = ""
              , rowscount = 0
              , flag = false;
            $.each(json_obj.conflict_events, function(key, value) {
                if (event_except += ";" + value.col_setdatetime,
                rowscount > 4)
                    flag = true;
                else {
                    rowscount++;
                    var trTag = $("<tr></tr>")
                      , tdTagTime = $("<td></td>").html(value.setdatetime)
                      , span = $('<span class="mobile_icon_listmarks_grn"></span>');
                    tdTagTime.prepend(span),
                    trTag.append(tdTagTime);
                    var tdTagFacility = $("<td></td>").html(value.col_facility);
                    trTag.append(tdTagFacility),
                    frag.appendChild(trTag[0])
                }
            }),
            popupDup.find(".mobile_repeating_table_grn").append(frag);
            var spanRepeatCaption = popupDup.find(".mobile_repeating_text_grn");
            if (1 == json_obj.conflict_all)
                spanRepeatCaption.html(G.conflict + "<br>" + G.conflictAll),
                popupDup.find(".mobile_ok_grn").hide(),
                popupDup.find(".no_button").hide(),
                popupDup.find(".cancel_button").show();
            else if (spanRepeatCaption.html(G.conflict + "<br>" + G.onlyNoConflict),
            popupDup.find(".mobile_ok_grn").show(),
            popupDup.find(".no_button").show(),
            popupDup.find(".cancel_button").hide(),
            flag) {
                var div_more = $('<div class="mobile_repeating_text_small_grn">...' + G.more + "</div>");
                popupDup.find(".mobile_repeating_scroll_grn").append(div_more[0])
            }
            popupDup.popup("open"),
            popupDup.find(".mobile_ok_grn").click(function(event) {
                $("#popup_duplicated").popup("close"),
                $("#hfExcept").val(event_except),
                G.handleSubmit(event)
            }),
            popupDup.find(".mobile_cancel_grn").click(function() {
                $("#hfExcept").empty(),
                $("#popup_duplicated").popup("close")
            })
        }
        ,
        G.showAttendanceConfirm = function(event) {
            var popupAttendanceConfirm, content = $("#attendane_confirm_html").html(), Gm = grn.component.msgbox_mobile;
            Gm.MsgBox.showAttendanceCheck(content, {
                callback: function(result, form) {
                    var needRefresh;
                    Gm.MsgBox.msgbox.find("#need_refresh").get(0).checked ? G.needRefresh = true : G.needRefresh = false,
                    Gm.MsgBox._remove(),
                    result == Gm.MsgBoxResult.yes && (G.showRefreshDialog = true,
                    G.handleSubmit(event))
                },
                html_id: {
                    overlay: "overlay",
                    msgbox: "confirm_dialog"
                }
            })
        }
        ,
        G.oldTime = {
            startYearSelect: $("#start_year").val(),
            startMonthSelect: $("#start_month").val(),
            startDaySelect: $("#start_day").val(),
            startHourSelect: $("#start_hour").val(),
            startMinuteSelect: $("#start_minute").val(),
            endYearSelect: $("#end_year").val(),
            endMonthSelect: $("#end_month").val(),
            endDaySelect: $("#end_day").val(),
            endHourSelect: $("#end_hour").val(),
            endMinuteSelect: $("#end_minute").val()
        },
        G.checkShowDialog = function(event) {
            if (G.checkUsingPurposeNotEmpty()) {
                var attendanceCheckElm = $("#attendance_check");
                if (0 != attendanceCheckElm.length && "" != attendanceCheckElm.val()) {
                    G.currentTime = {
                        startYearSelect: $("#start_year").val(),
                        startMonthSelect: $("#start_month").val(),
                        startDaySelect: $("#start_day").val(),
                        startHourSelect: $("#start_hour").val(),
                        startMinuteSelect: $("#start_minute").val(),
                        endYearSelect: $("#end_year").val(),
                        endMonthSelect: $("#end_month").val(),
                        endDaySelect: $("#end_day").val(),
                        endHourSelect: $("#end_hour").val(),
                        endMinuteSelect: $("#end_minute").val()
                    };
                    var arrMapCompare = ["startYearSelect", "startMonthSelect", "startDaySelect", "startHourSelect", "startMinuteSelect", "endYearSelect", "endMonthSelect", "endDaySelect", "endHourSelect", "endMinuteSelect", "startTimezone", "endTimezone"]
                      , showDialogFlag = false;
                    for (i = 0; i < arrMapCompare.length; i++)
                        if (G.oldTime[arrMapCompare[i]] !== G.currentTime[arrMapCompare[i]]) {
                            showDialogFlag = true;
                            break
                        }
                    if (showDialogFlag) {
                        var menu = $("input[name=menu]").val();
                        menu = menu.substring(0, menu.length - 3);
                        var title = $("input[name=title]").val()
                          , appointmentTitle = "";
                        0 == menu.length && 0 == title.length ? appointmentTitle = "--" : menu.length > 0 && title.length > 0 ? appointmentTitle = menu + ":" + title : (appointmentTitle += menu.length > 0 ? menu : "",
                        appointmentTitle += title.length > 0 ? title : ""),
                        $("#attendance_check_confirm_title").text(appointmentTitle),
                        G.showAttendanceConfirm(event)
                    } else
                        G.handleSubmit(event)
                } else
                    G.handleSubmit(event)
            }
        }
        ,
        G.checkUsingPurposeNotEmpty = function() {
            return "undefined" != typeof G.usingPurposeElement && G.usingPurposeElement.is(":visible") && !$.trim(G.usingPurpose.val()) ? (G.requireUsingPurpose.show(),
            false) : (G.requireUsingPurpose.hide(),
            true)
        }
        ,
        isInitial = false,
        $(document).on("pagechange", function(event) {
            isInitial || (isInitial = true,
            G.init(),
            "modify" == G.typeSchedule && G.needCheckShowRefreshDialog ? G.btnAdd.click(G.checkShowDialog) : G.btnAdd.click(G.handleSubmit))
        }),
        G.is_loaded = true
    }
}();
