grn.base.isNamespaceDefined("grn.component.mobile_item_select") || (grn.base.namespace("grn.component.mobile_item_select"),
grn.component.mobile_item_select = function() {
    function ItemSelection(settings) {
        this._construct(settings)
    }
    return ItemSelection.prototype = {
        _construct: function(settings) {
            this.processing = false,
            this.bindAgain = false,
            this.adding = false,
            this.isAjax = false,
            this.sp = 0,
            this.lastestCounter = 0,
            this.classBelongGroup = "belong_group_",
            this.keyword = "",
            this.type_item = settings.type_item,
            this.page_name = settings.page_name,
            this.args = "undefined" != typeof settings.args ? settings.args : "",
            this.ajaxGetItemURL = "undefined" != typeof settings.ajaxGetItemURL ? settings.ajaxGetItemURL : "",
            this.ajaxSearchItemURL = "undefined" != typeof settings.ajaxSearchItemURL ? settings.ajaxSearchItemURL : "",
            this.include_org = "undefined" != typeof settings.include_org ? settings.include_org : "",
            this.isCalendar = "undefined" != typeof settings.isCalendar ? settings.isCalendar : "",
            this.show_group_role = "undefined" != typeof settings.show_group_role ? settings.show_group_role : "",
            this.isAllowedRole = "undefined" != typeof settings.isAllowedRole ? settings.isAllowedRole : "",
            this.container = $("#_" + settings.element_id),
            this.associateElement = $("#" + settings.associate_id),
            this.option_search = settings.search_caption,
            this.selectedItem = settings.selectedItem,
            this.default_title_group_select = settings.default_title_group_select,
            this.initDomElement(settings),
            this.initTemplate(settings),
            this.initAttribute(settings),
            this.bindEventHandler()
        },
        initDomElement: function(settings) {
            this.dom = {
                source_item: this.container.find(".source_items"),
                dest_item: this.container.find(".selected_items"),
                orderControl: this.container.find(".order_selected_list"),
                addAllToSelectedList: this.container.find(".add_all_to_selected_list"),
                removeAllFromSelectedList: this.container.find(".remove_all_from_selected_list"),
                categorySelect: this.container.find("#" + settings.group_select_menu_id),
                categorySelectUI: this.container.find("#" + settings.categorySelectUI),
                categorySelectValue: this.container.find("#" + settings.categorySelectValue),
                popupItemCategory: this.container.find("#" + settings.popupItemCategory),
                buttonOK: this.container.find(".mobile_ok_grn"),
                buttonCancel: this.container.find(".mobile_cancel_grn"),
                lastestDestList: this.container.find(".selected_items").clone(),
                breadcrumLink: this.container.find(".mobile_breadcrumbtitle_left_grn"),
                div_loading: $('<div class="mobile_spinner_block_grn"><span class="mobile_icon_spinner_grn"></span></div>'),
                empty_list: $('<li data-icon="false" class="mobile-user-none-grn ui-li ui-li-static ui-btn-up-c ui-first-child ui-last-child"></li>').html(settings.empty_list),
                none_selected: $('<li data-icon="false" class="mobile-user-none-grn ui-li ui-li-static ui-btn-up-c ui-first-child ui-last-child"></li>').html(settings.none_selected),
                lastest_item: $('<li data-icon="false" class="mobile-list-last-grn ui-li ui-li-static ui-btn-up-c ui-last-child"></li>')
            }
        },
        initTemplate: function(settings) {
            this.template = {
                src_item: $(settings.src_item).html(),
                dest_item: $(settings.dest_item).html()
            }
        },
        initAttribute: function(settings) {},
        bindEventHandler: function() {
            var dom = this.dom
              , source_item = dom.source_item;
            source_item.on("touchstart", ".mobile_add_grn", $.proxy(this.handleAddItem, this)),
            source_item.on("touchstart", "li", $.proxy(this.handleBlurInputSearch, this)),
            source_item.scroll($.proxy(this.getMoreItems, this));
            var dest_item = this.dom.dest_item;
            dest_item.on("touchstart", ".mobile_delete_grn", $.proxy(this.handleRemoveItem, this)),
            dest_item.on("tap", ".selection_item", $.proxy(this.handleSelectItem, this)),
            dom.orderControl.on("touchstart", ".mobile_order_control_grn", $.proxy(this.handleOrder, this)),
            dom.addAllToSelectedList.on("touchstart", $.proxy(this.addAllItemFromGroup, this)),
            dom.removeAllFromSelectedList.on("touchstart", $.proxy(this.removeAllTick, this)),
            dom.categorySelect.change($.proxy(this.changeGroup, this)),
            dom.buttonOK.on("touchstart", $.proxy(this.add, this)),
            dom.buttonCancel.on("touchstart", $.proxy(this.cancel, this)),
            dom.breadcrumLink.on("click", $.proxy(this.cancel, this)),
            $(document).on("popupbeforeposition", this.dom.popupItemCategory, function(event) {
                source_item.css("overflow", "hidden"),
                dest_item.css("overflow", "hidden")
            }),
            $(document).on("popupafterclose", this.dom.popupItemCategory, function(event) {
                source_item.css("overflow", "auto"),
                dest_item.css("overflow", "auto")
            })
        },
        bindEventBeforePageShow: function(event) {
            if (!this.bindAgain) {
                var dom = this.dom;
                dom.inputSearch = this.container.find(".search_item"),
                dom.inputSearch.data("holder", dom.inputSearch.attr("placeholder")),
                dom.deleteInput = this.container.find(".delete_input"),
                dom.iconSearch = this.container.find(".search_item_icon"),
                dom.iconSearch.on("click", $.proxy(this.searchItemByTap, this)),
                dom.deleteInput.on("click", $.proxy(this.clearInput, this)),
                dom.inputSearch.keypress($.proxy(this.searchItemByKeyPress, this)),
                dom.inputSearch.keyup($.proxy(this.handleBackspace, this)),
                dom.inputSearch.focus($.proxy(this.focusSearchInput, this)),
                dom.inputSearch.blur($.proxy(this.blurSearchInput, this)),
                this.bindAgain = true
            }
        },
        searchItemByKeyPress: function(event) {
            var keycode;
            "13" == (event.keyCode ? event.keyCode : event.which) && this.handleSearchItem(event)
        },
        searchItemByTap: function(event) {
            this.handleSearchItem(event)
        },
        handleBackspace: function(event) {
            this.toggleCloseIcon()
        },
        toggleCloseIcon: function(event) {
            this.dom.inputSearch.val().length > 0 ? this.dom.deleteInput.show() : this.dom.deleteInput.hide()
        },
        handleSearchItem: function(event) {
            return this.sp = 0,
            this.keyword = this.dom.inputSearch.val(),
            this.ajaxSettings = {
                keyword: this.keyword,
                element: this.dom.source_item,
                type_list: "src_item",
                reload_list: true,
                type: "search"
            },
            this.ajaxGetItems(),
            this.container.find(".mobile_event_menu_content_grn").html(this.option_search),
            this.dom.categorySelect.val("search"),
            this.dom.popupItemCategory.find(".mobile_check_grn").removeClass("mobile_check_grn"),
            event.preventDefault(),
            event.stopPropagation(),
            false
        },
        handleBlurInputSearch: function() {
            this.dom.inputSearch.blur()
        },
        clearInput: function(event) {
            this.dom.inputSearch.val(""),
            this.dom.deleteInput.hide()
        },
        focusSearchInput: function(event) {
            this.dom.inputSearch.closest(".mobile_user_search_grn").addClass("mobile_user_search_focus_grn"),
            this.dom.inputSearch.attr("placeholder", ""),
            this.toggleCloseIcon()
        },
        blurSearchInput: function(event) {
            this.dom.inputSearch.closest(".mobile_user_search_grn").removeClass("mobile_user_search_focus_grn"),
            this.dom.inputSearch.attr("placeholder", this.dom.inputSearch.data("holder"));
            var me = this;
            setTimeout(function() {
                me.dom.deleteInput.hide()
            }, 200)
        },
        handleAddItem: function(event) {
            var element = $(event.target)
              , parent = element.closest("li")
              , id = parent.data("id")
              , gid = parent.data("gid")
              , type = parent.data("type");
            if (parent.hasClass("mobile_base_disable_grn"))
                ;
            else {
                element.hasClass("selection_item") || (element = element.closest("a"));
                var display_name = element.find("div.mobile_user_grn").text()
                  , group_name = element.find("div.mobile_text_grn").text()
                  , img = element.find(".mobile_user_photo_grn").clone()
                  , dest_item = $($.parseHTML(this.template.dest_item)).filter("*").clone()
                  , dom_dest_item = this.dom.dest_item
                  , last_item_list = dom_dest_item.find("li.mobile-list-last-grn");
                last_item_list.length > 0 ? last_item_list.remove() : (dom_dest_item.empty(),
                last_item_list = this.dom.lastest_item.clone());
                var frag = document.createDocumentFragment()
                  , classItem = this.getClassItem(type, id, gid);
                type == this.type_item && (dest_item.find("div.mobile_text_grn").text(group_name),
                dest_item.addClass(this.classBelongGroup + gid),
                dest_item.data("id", id)),
                dest_item.addClass(classItem),
                dest_item.find("div.mobile_user_grn").text(display_name),
                dest_item.data("gid", gid),
                dest_item.data("type", type),
                "facility" == type && (dest_item.data("checkrepeat", parent.data("checkrepeat")),
                dest_item.data("approval", parent.data("approval"))),
                dest_item.find(".mobile_user_photo_grn").remove(),
                dest_item.find(".selection_item").prepend(img),
                frag.appendChild(dest_item[0]),
                dom_dest_item.append(frag),
                dom_dest_item.append(last_item_list),
                parent.addClass("mobile_base_disable_grn")
            }
            event.preventDefault(),
            event.stopPropagation()
        },
        handleRemoveItem: function(event) {},
        handleSelectItem: function(event) {
            var element;
            $(event.target).closest(".mobile_selected_grn").toggleClass("mobile_selected_user_grn"),
            this.checkShowClearSelection(),
            event.preventDefault(),
            event.stopPropagation()
        },
        handleOrder: function(event) {
            var $target = $(event.target);
            $target.hasClass("mobile_order_control_grn") || ($target = $target.closest("div.mobile_order_control_grn")),
            $target.hasClass("order_top") && this.orderTop(event),
            $target.hasClass("order_up") && this.orderUp(event),
            $target.hasClass("order_down") && this.orderDown(event),
            $target.hasClass("order_bottom") && this.orderBottom(event),
            event.preventDefault(),
            event.stopPropagation()
        },
        orderTop: function(event) {
            var dest_item = this.dom.dest_item
              , list_selected = dest_item.find("li.mobile_selected_user_grn");
            if (list_selected.length > 0) {
                var frag = document.createDocumentFragment();
                list_selected.each(function() {
                    frag.appendChild(this)
                }),
                dest_item.prepend(frag)
            }
        },
        orderUp: function(event) {
            var list_selected = this.dom.dest_item.find("li.mobile_selected_user_grn");
            list_selected.length > 0 && $(list_selected[0]).prev().length > 0 && list_selected.each(function() {
                var thisObj = $(this)
                  , prev = thisObj.prev();
                prev.length > 0 && !prev.hasClass("mobile_selected_user_grn") && thisObj.after(prev)
            })
        },
        orderDown: function(event) {
            var list_selected = $(this.dom.dest_item.find("li.mobile_selected_user_grn").get().reverse());
            list_selected.length > 0 && !$(list_selected[0]).next().hasClass("mobile-list-last-grn") && list_selected.each(function() {
                var thisObj = $(this)
                  , next = thisObj.next();
                next.length > 0 && !next.hasClass("mobile_selected_user_grn") && !next.hasClass("mobile-list-last-grn") && thisObj.before(next)
            })
        },
        orderBottom: function(event) {
            var dest_item = this.dom.dest_item
              , last_item_list = dest_item.find("li.mobile-list-last-grn");
            last_item_list.remove();
            var list_selected = dest_item.find("li.mobile_selected_user_grn");
            if (list_selected.length > 0) {
                var frag = document.createDocumentFragment();
                list_selected.each(function() {
                    frag.appendChild(this)
                }),
                dest_item.append(frag),
                dest_item.append(last_item_list)
            }
        },
        changeGroup: function() {
            this.sp = 0,
            this.lastestCounter = 0;
            var selected_group = this.dom.categorySelect.val();
            selected_group.length > 0 ? ("search" == selected_group ? (this.keyword = this.dom.inputSearch.val(),
            this.ajaxSettings = {
                keyword: this.keyword,
                element: this.dom.source_item,
                type_list: "src_item",
                reload_list: true,
                type: "search"
            }) : this.ajaxSettings = {
                groupSelected: this.dom.categorySelect.val(),
                element: this.dom.source_item,
                type_list: "src_item",
                reload_list: true,
                include_org: true,
                type: "get"
            },
            this.ajaxGetItems()) : this.resetSourceItemList()
        },
        ajaxGetItems: function() {
            var ajaxURL = "";
            if ("get" == this.ajaxSettings.type ? ajaxURL = this.ajaxGetItemURL : "search" == this.ajaxSettings.type && (ajaxURL = this.ajaxSearchItemURL),
            ajaxURL.length > 0 && false == this.isAjax) {
                this.isAjax = true;
                var data_send = "";
                if ("" != this.args && (data_send += this.args),
                "get" == this.ajaxSettings.type) {
                    var groupSelected = this.ajaxSettings.groupSelected;
                    "x" == groupSelected.substr(0, 1) && (groupSelected = groupSelected.substr(1)),
                    data_send += "&gid=" + groupSelected,
                    this.ajaxSettings.include_org && "" != this.include_org && (data_send += "&include_org=" + this.include_org),
                    "" != this.show_group_role && (data_send += "&show_group_role=" + this.show_group_role)
                } else
                    "search" == this.ajaxSettings.type && (data_send += "&keyword=" + this.ajaxSettings.keyword);
                "facility" == this.type_item && (data_send += "&page_name=" + this.page_name);
                var type_list = this.ajaxSettings.type_list;
                "src_item" == type_list && (data_send += "&sp=" + this.sp);
                var element = this.ajaxSettings.element
                  , reload_list = this.ajaxSettings.reload_list;
                this.last_item_list = element.find("li.mobile-list-last-grn");
                var me = this;
                $.ajax({
                    url: ajaxURL,
                    dataType: "json",
                    data: data_send,
                    type: "post",
                    beforeSend: function() {
                        reload_list ? element.empty() : me.last_item_list.length > 0 ? me.last_item_list.remove() : element.empty(),
                        "dest_item" == type_list ? grn.component.mobile_loading.show() : element.append(me.dom.div_loading)
                    },
                    success: function(json_obj, text_status, transport) {
                        grn.component.mobile_error_handler.hasCybozuError(transport) ? grn.component.mobile_error_handler.show(json_obj) : "src_item" == type_list ? (me.sp = json_obj.offset,
                        me.lastestCounter = json_obj.total,
                        me.addItemsToSourceList(json_obj),
                        me.doAfterLoadSrcList()) : (me.addItemsToDestList(json_obj),
                        me.doAfterLoadDesList())
                    },
                    error: function(transport) {
                        grn.component.mobile_error_handler.hasCybozuLogin(transport) || (document.write(transport.responseText),
                        document.close()),
                        "dest_item" == type_list && grn.component.mobile_loading.remove()
                    },
                    complete: function() {
                        "dest_item" == type_list && grn.component.mobile_loading.remove(),
                        me.isAjax = false
                    }
                })
            }
        },
        addItemsToSourceList: function(json_obj) {
            this.frag = document.createDocumentFragment();
            var me = this;
            if ($.each(json_obj.list, function(key, value) {
                me.addElementItemToSourceList(key, value)
            }),
            this.dom.div_loading.remove(),
            this.last_item_list.length > 0)
                this.frag.appendChild(this.last_item_list[0]);
            else {
                var last_item_list = this.dom.lastest_item.clone();
                this.frag.appendChild(last_item_list[0])
            }
            this.dom.source_item.append(this.frag)
        },
        addElementItemToSourceList: function(key, value) {
            var is_add = true
              , id = "undefined" != typeof value.id ? value.id : ""
              , gid = "undefined" != typeof value.gid ? value.gid : ""
              , category_select_value = this.dom.categorySelect.val();
            "" === gid && category_select_value.match(/g[0-9]+/) && (gid = category_select_value.substr(1));
            var classItem = this.getClassItem(value.type, id, gid), item;
            if (!this.ajaxSettings.reload_list) {
                var temp = this.classItem + id, selected_item;
                this.dom.source_item.find("li." + temp).length > 0 && (is_add = false)
            }
            is_add && (template = $($.parseHTML(this.template.src_item)).filter("*").clone(),
            value.type == this.type_item && (template.find("div.mobile_text_grn").text(value.primaryGroupName),
            template.addClass(this.classBelongGroup + gid),
            template.data("id", id)),
            template.addClass(classItem),
            template.data("type", value.type),
            template.data("gid", gid),
            "facility" == value.type && (template.data("checkrepeat", value.checkrepeat),
            template.data("approval", value.approval)),
            this.addClassImgToElement(value, template),
            "group" == value.type && "" != this.isCalendar ? template.find("div.mobile_user_grn").text("[" + value.displayName + "]") : template.find("div.mobile_user_grn").text(value.displayName),
            this.dom.dest_item.find("li." + classItem).length > 0 && template.addClass("mobile_base_disable_grn"),
            this.frag.appendChild(template[0]))
        },
        addItemsToDestList: function(json_obj) {
            this.frag = document.createDocumentFragment();
            var me = this;
            if ($.each(json_obj.list, function(key, value) {
                me.addElementItemToDestList(key, value)
            }),
            this.last_item_list.length > 0)
                this.frag.appendChild(this.last_item_list[0]);
            else {
                var last_item_list = this.dom.lastest_item.clone();
                this.frag.appendChild(last_item_list[0])
            }
            this.dom.dest_item.append(this.frag)
        },
        addElementItemToDestList: function(key, value) {
            var is_add = true
              , id = "undefined" != typeof value.id ? value.id : ""
              , gid = "undefined" != typeof value.gid ? value.gid : ""
              , category_select_value = this.dom.categorySelect.val();
            "" === gid && category_select_value.match(/g[0-9]+/) && (gid = category_select_value.substr(1));
            var classItem = this.getClassItem(value.type, id, gid), selected_item;
            this.ajaxSettings.element.find("li." + classItem).length > 0 && (is_add = false),
            is_add && (template = $($.parseHTML(this.template.dest_item)).filter("*").clone(),
            value.type == this.type_item && (template.find("div.mobile_text_grn").text(value.primaryGroupName),
            template.addClass(this.classItem + id),
            template.data("id", id)),
            template.addClass(classItem),
            template.data("type", value.type),
            template.data("gid", gid),
            "facility" == value.type && (template.data("checkrepeat", value.checkrepeat),
            template.data("approval", value.approval)),
            "group" == value.type && "" != this.isCalendar ? template.find("div.mobile_user_grn").text("[" + value.displayName + "]") : template.find("div.mobile_user_grn").text(value.displayName),
            this.addClassImgToElement(value, template),
            this.frag.appendChild(template[0]))
        },
        removeItemFromList: function(id) {
            var className = "." + this.classItem + id, removed_item;
            this.dom.dest_item.find(className).remove()
        },
        removeGroupFromList: function(gid) {},
        getMoreItems: function(event) {
            if (!(this.lastestCounter <= this.sp)) {
                var target = event.target, element = $(target), scrollAmount = $(element).scrollTop(), documentHeight;
                if (target.scrollHeight - scrollAmount < 1e3) {
                    var groupSelected = this.dom.categorySelect.val();
                    this.ajaxSettings = "search" == groupSelected ? {
                        keyword: this.keyword,
                        element: this.dom.source_item,
                        type_list: "src_item",
                        reload_list: false,
                        type: "search"
                    } : {
                        groupSelected: groupSelected,
                        element: this.dom.source_item,
                        type_list: "src_item",
                        reload_list: false,
                        include_org: false,
                        type: "get"
                    },
                    this.ajaxGetItems()
                }
            }
        },
        enableItem: function(id) {
            var className = "li." + this.classItem + id, enabled_item;
            this.dom.source_item.find(className).removeClass("mobile_base_disable_grn")
        },
        enableGroup: function(gid) {},
        enableRole: function(gid) {},
        addAllItemFromGroup: function() {
            var selected_group = this.dom.categorySelect.val();
            selected_group.length > 0 && ("search" == selected_group ? (this.keyword = this.dom.inputSearch.val(),
            this.ajaxSettings = {
                keyword: this.keyword,
                element: this.dom.dest_item,
                type_list: "dest_item",
                reload_list: false,
                type: "search"
            }) : this.ajaxSettings = {
                groupSelected: this.dom.categorySelect.val(),
                element: this.dom.dest_item,
                type_list: "dest_item",
                reload_list: false,
                include_org: true,
                type: "get"
            },
            this.ajaxGetItems(),
            this.dom.source_item.find("li").addClass("mobile_base_disable_grn"),
            this.checkShowClearSelection())
        },
        removeAllTick: function() {
            var dest_item, me = this;
            this.dom.dest_item.find("li.mobile_selected_user_grn").each(function() {
                $(this).removeClass("mobile_selected_user_grn")
            }),
            this.doAfterLoadDesList()
        },
        add: function() {},
        cancel: function() {
            if (!this.processing) {
                this.processing = true,
                grn.component.mobile_loading.show();
                var me = this;
                this.dom.dest_item.empty();
                var frag = document.createDocumentFragment();
                this.dom.lastestDestList.find("li").each(function() {
                    frag.appendChild(this)
                }),
                this.dom.dest_item.append(frag),
                this.checkShowClearSelection(),
                setTimeout(function() {
                    grn.component.mobile_loading.remove(),
                    window.location.href = "#",
                    $.mobile.changePage($.mobile.firstPage, {
                    transition: "none",
                    reverse: !0,
                    changeHash: !1,
                    fromHashChange: !0
                })   
                }, 500),
                event.preventDefault(),
                event.stopPropagation()
            }
        },
        getClassItem: function(type, id, gid) {},
        saveLastState: function(list) {
            this.dom.lastestDestList = this.dom.dest_item.clone(true)
        },
        doAfterLoadSrcList: function() {
            var count;
            this.dom.source_item.find("li").length <= 1 ? (this.dom.source_item.empty(),
            this.dom.source_item.append(this.dom.empty_list.clone()),
            this.dom.source_item.closest(".mobile_user_list_scroll_grn").addClass("mobile-user-list-none-grn")) : this.dom.source_item.closest(".mobile_user_list_scroll_grn").removeClass("mobile-user-list-none-grn")
        },
        doAfterLoadDesList: function() {
            var count;
            this.dom.dest_item.find("li").length <= 1 ? (this.dom.dest_item.empty(),
            this.dom.dest_item.append(this.dom.none_selected.clone()),
            this.dom.removeAllFromSelectedList.hide()) : this.checkShowClearSelection()
        },
        addClassImgToElement: function(value, template) {
            var classImg, ariaLabel;
            "user" == value.type ? ariaLabel = value.isLoginUser ? (classImg = "mobile_img_userLoginPlofile_grn",
            grn.component.i18n.cbMsg("grn.grn", "GRN_GRN-LOGIN_USER")) : value.isInvalidUser ? (classImg = "mobile_img_userInvalidPlofile_grn",
            grn.component.i18n.cbMsg("grn.grn", "GRN_GRN-INACTIVE_USER")) : value.isNotUsingApp ? (classImg = "mobile_img_userInvalidAppPlofile_grn",
            grn.component.i18n.cbMsg("grn.grn", "GRN_GRN-NOT_APPLICATION_USER")) : (classImg = "mobile_img_userPlofile_grn",
            grn.component.i18n.cbMsg("grn.grn", "GRN_GRN-1571")) : "group" == value.type ? ariaLabel = "" != this.isCalendar ? (classImg = "mobile_img_cal_group_profile_grn",
            grn.component.i18n.cbMsg("grn.grn", "GRN_GRN-ORGANIZATION_PLAN")) : (classImg = "mobile_img_groupPlofile_grn",
            grn.component.i18n.cbMsg("grn.grn", "GRN_GRN-ORGANIZATION")) : "static_role" == value.type && (classImg = "mobile_img_role_profile_grn",
            ariaLabel = grn.component.i18n.cbMsg("grn.grn", "GRN_GRN-ROLE")),
            template.find(".mobile_user_photo_grn").addClass(classImg),
            template.find(".mobile_user_photo_grn").prop("aria-label", ariaLabel)
        },
        initSelectedItem: function() {
            this.initFlag = true,
            "undefined" !== typeof this.selectedItem && this.selectedItem.hasOwnProperty("list") && null !== this.selectedItem.list && (this.ajaxSettings = {
                element: this.dom.dest_item,
                type_list: "dest_item",
                reload_list: false,
                include_org: true,
                type: "get"
            },
            this.last_item_list = this.dom.dest_item.find("li.mobile-list-last-grn"),
            this.addItemsToDestList(this.selectedItem)),
            this.doAfterLoadDesList(),
            this.add(),
            this.initFlag = false
        },
        checkShowClearSelection: function() {
            var count_selected;
            this.dom.dest_item.find(".mobile_selected_user_grn").length > 0 ? this.dom.removeAllFromSelectedList.show() : this.dom.removeAllFromSelectedList.hide()
        },
        scrollToSelectedItemList: function() {
            var count_selected;
            this.dom.dest_item.find("li").length > 1 && $(window).scrollTop(this.dom.dest_item.offset().top)
        },
        resetSourceItemList: function() {
            this.dom.source_item.empty(),
            this.dom.source_item.append(this.dom.empty_list.clone()),
            this.dom.source_item.closest(".mobile_user_list_scroll_grn").addClass("mobile-user-list-none-grn")
        },
        resetCategorySelect: function() {
            var content = $('<span class="mobile_event_menu_content_grn">' + this.default_title_group_select + "</span>");
            this.dom.categorySelectUI.empty(),
            this.dom.categorySelectUI.append(content);
            var arrowIcon = $("<span class='mobile_select_icon_grn'></span>");
            this.dom.categorySelectUI.append(arrowIcon),
            this.dom.popupItemCategory.find(".mobile_check_grn").removeClass("mobile_check_grn"),
            this.dom.inputSearch.val(""),
            this.dom.categorySelect.val(""),
            this.sp = 0,
            this.lastestCounter = 0
        }
    },
    ItemSelection
}());
