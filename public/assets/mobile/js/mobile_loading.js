!function() {
    function setOverlayHeight() {
        null !== overlay && overlay.height($(".ui-page-active[data-role='page']").outerHeight(true))
    }
    function setSpinnerPosition() {
        if (null !== spinner) {
            var offset, top = $(window).scrollTop() + ($(window).height() - spinner.innerHeight()) / 2 + "px";
            spinner.css({
                top: top
            })
        }
    }
    grn.base.namespace("grn.component.mobile_loading");
    var G = grn.component.mobile_loading;
    $(document).ready(function() {
        $(window).resize(function() {
            setSpinnerPosition(),
            setOverlayHeight()
        })
    });
    var overlay = null
      , spinner = null;
    G.show = function() {
        overlay = $("div.mobile_spinner_grn"),
        (spinner = $("div.mobile_spinner_grn span")).css({
            marginTop: 0
        }),
        setSpinnerPosition(),
        setOverlayHeight(),
        overlay.show()
    }
    ,
    G.remove = function() {
        overlay.hide(),
        spinner = overlay = null
    }
    ,
    G.showOverlay = function() {
        overlay = $("div.mobile_spinner_grn"),
        $("div.mobile_spinner_grn span").hide(),
        setOverlayHeight(),
        overlay.show()
    }
}();
