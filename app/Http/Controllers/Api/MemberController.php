<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\MemberRepository;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use \Pusher\Pusher;

class MemberController extends Controller
{
    public function __construct(MemberRepository $memberRepo) {
        $this->memberRepo = $memberRepo;
    }
    public function seenNotification(Request $request){
        \App\Notification::where('id',$request->id)
            ->update(['read_at'=>date('Y-m-d H:i:s')]);
        return response()->json(['error' => false]);
    }
    public function seenNotificationAll(){
        \Auth::guard('member')->user()->unreadNotifications->markAsRead(); 
        return response()->json(['error' => false]);
    }
    public function editList(Request $request) {
        $member_arr = $request->get('member_id');
        if($member_arr == 'all'){
            $member_id = $this->memberRepo->getAll()->pluck('id')->toArray();
        }else{
            $member_id = explode(',',$member_arr);
        }
        Session::put('member_id', $member_id);
        $member = $this->memberRepo->find(session('member_id')[0]);
        $part_html = \App\Helpers\StringHelper::getSelectHtml(\App\Part::get(),$member->part_id);
        $department_html = \App\Helpers\StringHelper::getSelectHtml(\App\Department::get(),$member->department_id);
        $groups_html = \App\Helpers\StringHelper::getSelectHtml(\App\Groups::get(),$member->groups_id);
        $team_html = \App\Helpers\StringHelper::getSelectHtml(\App\Team::get(),$member->team_id);
        $position_html = \App\Helpers\StringHelper::getSelectHtml(\App\Position::get(),$member->position_id);
        $id = $member->id;
        $level_html='';
        for($i=1;$i<5;$i++){
            if($member->level == $i){
                $level_html .='<option value="'.$i.'" selected>'.$i.'</option>';
            }else{
                $level_html .='<option value="'.$i.'">'.$i.'</option>';
            }
                    
        }
        if(count(session('member_id')) > 1){
            $next_id = session('member_id')[1];
        }else{
            $next_id = null;
        }
        return response()->json(array('part_html' => $part_html,'department_html'=>$department_html,'groups_html'=>$groups_html,'team_html'=>$team_html,'position_html'=>$position_html,
            'login_id'=>$member->login_id,'email'=>$member->email,'full_name'=>$member->full_name,'note'=>$member->note,'id'=>$id,'next_id'=>$next_id,'avatar'=>$member->avatar,'level_html'=>$level_html));
        
    }
    public function editDetail(Request $request) {
        $member_id = $request->get('member_id');
        $member = $this->memberRepo->find($member_id);
        $part_html = \App\Helpers\StringHelper::getSelectHtml(\App\Part::get(),$member->part_id);
        $department_html = \App\Helpers\StringHelper::getSelectHtml(\App\Department::get(),$member->department_id);
        $groups_html = \App\Helpers\StringHelper::getSelectHtml(\App\Groups::get(),$member->groups_id);
        $team_html = \App\Helpers\StringHelper::getSelectHtml(\App\Team::get(),$member->team_id);
        $position_html = \App\Helpers\StringHelper::getSelectHtml(\App\Position::get(),$member->position_id);
        $level_html='';
        for($i=1;$i<5;$i++){
            if($member->level == $i){
                $level_html .='<option value="'.$i.'" selected>'.$i.'</option>';
            }else{
                $level_html .='<option value="'.$i.'">'.$i.'</option>';
            }
                    
        }
        $index = array_search($member_id, Session::get('member_id'));
        if($index !== false && $index > 0 ) {
            $prev = session('member_id')[$index-1];
        }else{
            $prev = null;
        }
        if($index !== false && $index < count(Session::get('member_id'))-1){
            $next = session('member_id')[$index+1];
        }else{
            $next = null;
        }
        return response()->json(array('part_html' => $part_html,'department_html'=>$department_html,'groups_html'=>$groups_html,'team_html'=>$team_html,'position_html'=>$position_html,
            'login_id'=>$member->login_id,'email'=>$member->email,'full_name'=>$member->full_name,'note'=>$member->note,'id'=>$member_id,'prev'=>$prev,'next'=>$next,'avatar'=>$member->avatar,'level_html'=>$level_html));
        
    }
    public function update(Request $request) {
        $input = $request->all();
        if($input['password'] == null){
            unset($input['password']);
        }else{
            $input['password'] = bcrypt($input['password']);
        }
        $this->memberRepo->update($input, $input['id']);
        return response()->json(array('success' => true));
    }
    public function nextPage() {
        $page = Session::get('page') + 1;
        Session::put('page',$page);
        $a = Carbon::now();
        
        $lastQuarter = $a->quarter;
        $records = $this->memberRepo->getIndex(10);
        $html='';
        foreach($records as $key=>$record){
            $record->project_month = \App\Project::where('member_id',$record->id)->whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))->count();
            $record->project_quarter = \App\Project::where('member_id',$record->id)->where(DB::raw('QUARTER(created_at)'), $lastQuarter)->whereYear('created_at',date('Y'))->count();
        }
        foreach($records as $key=>$record){
            $i = ((Session::get('page') - 1) * 10) + $key +1;
            $part = $record->part ? $record->part->name : '';
            $department = $record->department ? $record->department->name : '';
            $groups = $record->groups ? $record->groups->name : '';
            $team = $record->team ? $record->team->name : '';
            $position = $record->position ? $record->position->name : '';
            $count_project = $record->project ? $record->project->count() : 0 ;
            $html .='<tr>
                        <td><input type="checkbox" value="'.$record->id.'" name="member_id" class="check"></td>
                        <td>'.$i.'</td>
                        <td>'.$part.'</td>
                        <td>'.$department.'</td>
                        <td>'.$groups.'</td>
                        <td>'.$team.'</td>
                        <td>'.$record->login_id.'</td>
                        <td>'.$record->full_name.'</td>
                        <td>'.$position.'</td>
                        <td>'.$record->level.'</td>
                        <td>'.$record->project_month.'</td>
                        <td>'.$record->project_quarter.'</td>
                        <td>'.$count_project.'</td>
                    </tr>';
        }
        $start = ((Session::get('page')-1) * 10) + 1;
        $show_page ='<p id="page_member">'.$start.'-'.Session::get('_pages').' trong số '.Session::get('_count').'</p>';
        if(session('page') > 1){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="forward-page-member"><img src="public/assets2/img/left-arrow.png"></a>';
        }
        if(session('_count') > session('_pages')){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="next-page-member"><img src="public/assets2/img/forward.png"></a>';
        }
        return response()->json(array('html'=>$html,'show_page'=>$show_page));
        
    }
    public function forwardPage() {
        $page = Session::get('page') - 1;
        Session::put('page',$page);
        $records = $this->memberRepo->getIndex(10);
        $a = Carbon::now();
        
        $lastQuarter = $a->quarter;
        foreach($records as $key=>$record){
            $record->project_month = \App\Project::where('member_id',$record->id)->whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))->count();
            $record->project_quarter = \App\Project::where('member_id',$record->id)->where(DB::raw('QUARTER(created_at)'), $lastQuarter)->whereYear('created_at',date('Y'))->count();
        }
        $html='';
        foreach($records as $key=>$record){
            $i= ((Session::get('page') - 1) * 10) + $key +1;
            $part = $record->part ? $record->part->name : '';
            $department = $record->department ? $record->department->name : '';
            $groups = $record->groups ? $record->groups->name : '';
            $team = $record->team ? $record->team->name : '';
            $position = $record->position ? $record->position->name : '';
            $count_project = $record->project ? $record->project->count() : 0 ;
            $html .='<tr>
                        <td><input type="checkbox" value="'.$record->id.'" name="member_id" class="check"></td>
                        <td>'.$i.'</td>
                        <td>'.$part.'</td>
                        <td>'.$department.'</td>
                        <td>'.$groups.'</td>
                        <td>'.$team.'</td>
                        <td>'.$record->login_id.'</td>
                        <td>'.$record->full_name.'</td>
                        <td>'.$position.'</td>
                        <td>'.$record->level.'</td>
                        <td>'.$record->project_month.'</td>
                        <td>'.$record->project_quarter.'</td>
                        <td>'.$count_project.'</td>
                    </tr>';
        }
        $start = ((Session::get('page')-1) * 10) + 1;
        $show_page ='<p id="page_member">'.$start.'-'.Session::get('_pages').' trong số '.Session::get('_count').'</p>';
        if(session('page') > 1){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="forward-page-member"><img src="public/assets2/img/left-arrow.png"></a>';
        }
        if(session('_count') > session('_pages')){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="next-page-member"><img src="public/assets2/img/forward.png"></a>';
        }
        return response()->json(array('html'=>$html,'show_page'=>$show_page));
        
    }
    public function destroy(Request $request) {
        $member_arr = $request->get('member_id');
        if($member_arr == 'all'){
            $member_id = $this->memberRepo->getAll()->pluck('id')->toArray();
        }else{
            $member_id = explode(',',$member_arr);
        }
        $this->memberRepo->remove($member_id);
        Session::put('page',1);
        $records = $this->memberRepo->getIndex(10);
        $html='';
        $a = Carbon::now();
        
        $lastQuarter = $a->quarter;
        foreach($records as $key=>$record){
            $record->project_month = \App\Project::where('member_id',$record->id)->whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))->count();
            $record->project_quarter = \App\Project::where('member_id',$record->id)->where(DB::raw('QUARTER(created_at)'), $lastQuarter)->whereYear('created_at',date('Y'))->count();
        }
        foreach($records as $key=>$record){
            $i = ((Session::get('page') - 1) * 10) + $key +1;
            $part = $record->part ? $record->part->name : '';
            $department = $record->department ? $record->department->name : '';
            $groups = $record->groups ? $record->groups->name : '';
            $team = $record->team ? $record->team->name : '';
            $position = $record->position ? $record->position->name : '';
            $count_project = $record->project ? $record->project->count() : 0 ;
            $html .='<tr>
                        <td><input type="checkbox" value="'.$record->id.'" name="member_id" class="check"></td>
                        <td>'.$i.'</td>
                        <td>'.$part.'</td>
                        <td>'.$department.'</td>
                        <td>'.$groups.'</td>
                        <td>'.$team.'</td>
                        <td>'.$record->login_id.'</td>
                        <td>'.$record->full_name.'</td>
                        <td>'.$position.'</td>
                        <td>'.$record->level.'</td>
                        <td>'.$record->project_month.'</td>
                        <td>'.$record->project_quarter.'</td>
                        <td>'.$count_project.'</td>
                    </tr>';
        }
        $start = ((Session::get('page')-1) * 10) + 1;
        $show_page ='<p id="page_member">'.$start.'-'.Session::get('_pages').' trong số '.Session::get('_count').'</p>';
        if(session('page') > 1){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="forward-page-member"><img src="public/assets2/img/left-arrow.png"></a>';
        }
        if(session('_count') > session('_pages')){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="next-page-member"><img src="public/assets2/img/forward.png"></a>';
        }
        return response()->json(array('html'=>$html,'show_page'=>$show_page));
    }
    public function listMember(){
        Session::put('page',1);
        $records = $this->memberRepo->getIndex(10);
        $html='';
        $a = Carbon::now();
        
        $lastQuarter = $a->quarter;
        foreach($records as $key=>$record){
            $record->project_month = \App\Project::where('member_id',$record->id)->whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))->count();
            $record->project_quarter = \App\Project::where('member_id',$record->id)->where(DB::raw('QUARTER(created_at)'), $lastQuarter)->whereYear('created_at',date('Y'))->count();
        }
        foreach($records as $key=>$record){
            $i = ((Session::get('page') - 1) * 10) + $key +1;
            $part = $record->part ? $record->part->name : '';
            $department = $record->department ? $record->department->name : '';
            $groups = $record->groups ? $record->groups->name : '';
            $team = $record->team ? $record->team->name : '';
            $position = $record->position ? $record->position->name : '';
            $count_project = $record->project ? $record->project->count() : 0 ;
            $html .='<tr>
                        <td><input type="checkbox" value="'.$record->id.'" name="member_id" class="check"></td>
                        <td>'.$i.'</td>
                        <td>'.$part.'</td>
                        <td>'.$department.'</td>
                        <td>'.$groups.'</td>
                        <td>'.$team.'</td>
                        <td>'.$record->login_id.'</td>
                        <td>'.$record->full_name.'</td>
                        <td>'.$position.'</td>
                        <td>'.$record->level.'</td>
                        <td>'.$record->project_month.'</td>
                        <td>'.$record->project_quarter.'</td>
                        <td>'.$count_project.'</td>
                    </tr>';
        }
        $start = ((Session::get('page')-1) * 10) + 1;
        $show_page ='<p id="page_member">'.$start.'-'.Session::get('_pages').' trong số '.Session::get('_count').'</p>';
        if(session('page') > 1){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="forward-page-member"><img src="public/assets2/img/left-arrow.png"></a>';
        }
        if(session('_count') > session('_pages')){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="next-page-member"><img src="public/assets2/img/forward.png"></a>';
        }
        return response()->json(array('html'=>$html,'show_page'=>$show_page));
    }
    public function listMemberRemove(){
        $records = $this->memberRepo->getIndexRemove(10);
        $html='';
        foreach($records as $key=>$record){
            $i = ((Session::get('page') - 1) * 10) + $key +1;
            $a = Carbon::now();
            
            $lastQuarter = $a->quarter;
            foreach($records as $key=>$record){
                $record->project_month = \App\Project::where('member_id',$record->id)->whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))->count();
                $record->project_quarter = \App\Project::where('member_id',$record->id)->where(DB::raw('QUARTER(created_at)'), $lastQuarter)->whereYear('created_at',date('Y'))->count();
            }
            $part = $record->part ? $record->part->name : '';
            $department = $record->department ? $record->department->name : '';
            $groups = $record->groups ? $record->groups->name : '';
            $team = $record->team ? $record->team->name : '';
            $position = $record->position ? $record->position->name : '';
            $count_project = $record->project ? $record->project->count() : 0 ;
            $html .='<tr>
                        <td><input type="checkbox" value="'.$record->id.'" name="member_id" class="check"></td>
                        <td>'.$i.'</td>
                        <td>'.$part.'</td>
                        <td>'.$department.'</td>
                        <td>'.$groups.'</td>
                        <td>'.$team.'</td>
                        <td>'.$record->login_id.'</td>
                        <td>'.$record->full_name.'</td>
                        <td>'.$position.'</td>
                        <td>'.$record->level.'</td>
                        <td>'.$record->project_month.'</td>
                        <td>'.$record->project_quarter.'</td>
                        <td>'.$count_project.'</td>
                    </tr>';
        }
        $start = ((Session::get('page')-1) * 10) + 1;
        $show_page ='<p id="page_member">'.$start.'-'.Session::get('_pages').' trong số '.Session::get('_count').'</p>';
        if(session('page') > 1){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="forward-page-member"><img src="public/assets2/img/left-arrow.png"></a>';
        }
        if(session('_count') > session('_pages')){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="next-page-member"><img src="public/assets2/img/forward.png"></a>';
        }
        return response()->json(array('html'=>$html,'show_page'=>$show_page));
    }
    public function restoreMember(Request $request){
        $member_arr = $request->get('member_id');
        if($member_arr == 'all'){
            $member_id = $this->memberRepo->getAllRemove()->pluck('id')->toArray();
        }else{
            $member_id = explode(',',$member_arr);
        }
        $this->memberRepo->restore($member_id);
        $records = $this->memberRepo->getIndexRemove(10);
        $html='';
        foreach($records as $key=>$record){
            $i = ((Session::get('page') - 1) * 10) + $key + 1;
            $a = Carbon::now();
            
            $lastQuarter = $a->quarter;
            foreach($records as $key=>$record){
                $record->project_month = \App\Project::where('member_id',$record->id)->whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))->count();
                $record->project_quarter = \App\Project::where('member_id',$record->id)->where(DB::raw('QUARTER(created_at)'), $lastQuarter)->whereYear('created_at',date('Y'))->count();
            }
            $part = $record->part ? $record->part->name : '';
            $department = $record->department ? $record->department->name : '';
            $groups = $record->groups ? $record->groups->name : '';
            $team = $record->team ? $record->team->name : '';
            $position = $record->position ? $record->position->name : '';
            $count_project = $record->project ? $record->project->count() : 0 ;
            $html .='<tr>
                        <td><input type="checkbox" value="'.$record->id.'" name="member_id" class="check"></td>
                        <td>'.$i.'</td>
                        <td>'.$part.'</td>
                        <td>'.$department.'</td>
                        <td>'.$groups.'</td>
                        <td>'.$team.'</td>
                        <td>'.$record->login_id.'</td>
                        <td>'.$record->full_name.'</td>
                        <td>'.$position.'</td>
                        <td>'.$record->level.'</td>
                        <td>'.$record->project_month.'</td>
                        <td>'.$record->project_quarter.'</td>
                        <td>'.$count_project.'</td>
                    </tr>';
        }
        $start = ((Session::get('page')-1) * 10) + 1;
        $show_page ='<p id="page_member">'.$start.'-'.Session::get('_pages').' trong số '.Session::get('_count').'</p>';
        if(session('page') > 1){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="forward-page-member"><img src="public/assets2/img/left-arrow.png"></a>';
        }
        if(session('_count') > session('_pages')){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="next-page-member"><img src="public/assets2/img/forward.png"></a>';
        }
        return response()->json(array('html'=>$html,'show_page'=>$show_page));
    }
    public function searchMember(Request $request){
        $search = $request->all();
        $records = $this->memberRepo->search($search);
        $html='';
        $a = Carbon::now();
        
        $lastQuarter = $a->quarter;
        foreach($records as $key=>$record){
            $record->project_month = \App\Project::where('member_id',$record->id)->whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))->count();
            $record->project_quarter = \App\Project::where('member_id',$record->id)->where(DB::raw('QUARTER(created_at)'), $lastQuarter)->whereYear('created_at',date('Y'))->count();
        }
        foreach($records as $key=>$record){
            $i = ((Session::get('page') - 1) * 10) + $key + 1;
            $part = $record->part ? $record->part->name : '';
            $department = $record->department ? $record->department->name : '';
            $groups = $record->groups ? $record->groups->name : '';
            $team = $record->team ? $record->team->name : '';
            $position = $record->position ? $record->position->name : '';
            $count_project = $record->project ? $record->project->count() : 0 ;
            $html .='<tr>
                        <td><input type="checkbox" value="'.$record->id.'" name="member_id" class="check"></td>
                        <td>'.$i.'</td>
                        <td>'.$part.'</td>
                        <td>'.$department.'</td>
                        <td>'.$groups.'</td>
                        <td>'.$team.'</td>
                        <td>'.$record->login_id.'</td>
                        <td>'.$record->full_name.'</td>
                        <td>'.$position.'</td>
                        <td>'.$record->level.'</td>
                        <td>'.$record->project_month.'</td>
                        <td>'.$record->project_quarter.'</td>
                        <td>'.$count_project.'</td>
                    </tr>';
        }
        $start = ((Session::get('page')-1) * 10) + 1;
        $show_page ='<p id="page_member">'.$start.'-'.Session::get('_pages').' trong số '.Session::get('_count').'</p>';
        if(session('page') > 1){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="forward-page-member"><img src="public/assets2/img/left-arrow.png"></a>';
        }
        if(session('_count') > session('_pages')){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="next-page-member"><img src="public/assets2/img/forward.png"></a>';
        }
        return response()->json(array('success'=>true,'html'=>$html,'show_page'=>$show_page));
    }
    public function checkPassword(Request $request){
        $password = $request->get('password');
        if(Hash::check($password, \Auth::guard('member')->user()->password)) {
            return response()->json(array('success'=>'true'));
        }else{
            return response()->json(array('success'=>'false'));
        }
    }
    public function resetPassword(Request $request){
        $password = $request->get('password');
        $input['password'] = bcrypt($password);
        $this->memberRepo->update($input,\Auth::guard('member')->user()->id);
        return response()->json(array('success'=>'true'));
    }
    public function getInfo(Request $request){
        $member_id = $request->get('member_id');
        if($member_id){
            $member = $this->memberRepo->find($member_id);
        }else{
            $member = \Auth::guard('member')->user();
        }
        $part = $member->part ? $member->part->name : '';
        $team = $member->team ? $member->team->name : '';
        $html='<div class="col-md-3" style="padding-left:0px;">
                    <div class="img-member">
                        <img src="'.$member->avatar.'" style="height:175px;">
                    </div>
                </div>
                <div class="col-md-7">
                    <h3>'.$member->full_name.'</h3>
                    <p>Mã nhân viên: <span>'.$member->login_id.'</span></p>
                    <p>Chức vụ: <span>'.$member->position->name.'</span></p>
                    <p>Bộ phận: <span>'.$part.'</span></p>
                    <p>Team: <span>'.$team.'</span></p>
                </div>';
        return response()->json(array('html'=>$html));
    }
    public function getInfoMobile(Request $request){
        $member_id = $request->get('member_id');
        if($member_id){
            $member = $this->memberRepo->find($member_id);
        }else{
            $member = \Auth::guard('member')->user();
        }
        $part = $member->part ? $member->part->name : '';
        $team = $member->team ? $member->team->name : '';
        $html='<div class="col-5" style="padding-left:0px;">
                    <div class="img-member">
                        <img src="'.$member->avatar.'">
                    </div>
                </div>
                <div class="col-7">
                    <h3>'.$member->full_name.'</h3>
                    <p>Mã nhân viên: <span>'.$member->login_id.'</span></p>
                    <p>Chức vụ: <span>'.$member->position->name.'</span></p>
                    <p>Bộ phận: <span>'.$part.'</span></p>
                    <p>Team: <span>'.$team.'</span></p>
                </div>';
        return response()->json(array('html'=>$html));
    }
    public function import(Request $request)
    {
        if ($_FILES['files']) {
            // get file extension
            $extension = pathinfo($_FILES['files']['name'], PATHINFO_EXTENSION);
            if ($extension == 'csv') {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
            } elseif ($extension == 'xlsx') {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            } else {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
            }

            // file path
            $spreadsheet = $reader->load($_FILES['files']['tmp_name']);
            $allDataInSheet = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
            $member_ID=[];
            foreach ($allDataInSheet as $key => $row) {
                //Lấy dứ liệu từ dòng thứ 4
                if ($key > 1) {
                    $input['part_id'] = ($row['A'] == NULL || is_null(\App\Part::where('name',$row['A'])->first())) ? NULL : \App\Part::where('name',$row['A'])->first()->id;
                    $input['department_id'] = ($row['B'] == NULL || is_null(\App\Department::where('name',$row['B'])->first())) ? NULL : \App\Department::where('name',$row['B'])->first()->id;
                    $input['groups_id'] = ($row['C'] == NULL || is_null(\App\Groups::where('name',$row['C'])->first())) ? NULL : \App\Groups::where('name',$row['C'])->first()->id;
                    $input['team_id'] = ($row['D'] == NULL || is_null(\App\Team::where('name',$row['D'])->first())) ? NULL : \App\Team::where('name',$row['D'])->first()->id;
                    $input['position_id'] = ($row['E'] == NULL || is_null(\App\Position::where('name',$row['E'])->first())) ? NULL : \App\Position::where('name',$row['E'])->first()->id;
                    $input['login_id'] = $row['F'];
                    $input['level'] = $row['G'];
                    $input['full_name'] = $row['H'];
                    $input['password'] = bcrypt(123456);
                    $input['avatar']='/public/upload/images/Default_avatar_VHE.png';
                    $check = $this->memberRepo->checkID($input['login_id']);
                    if(!$check){
                        $this->memberRepo->create($input);
                        unset($input);
                    }else{
                        $member_ID[] = $input['login_id'];
                    }
                }
            }
        }
        if(count($member_ID) > 0){
            $message = 'ID login đã tồn tại: ';
            foreach($member_ID as $key=>$val){
                $message .= '<p>'.$val.'</p>';
            }
            return response()->json(array('success'=>false,'message'=>$message));
        }else{
            return response()->json(array('success'=>true));
        }
    }
    public function export(Request $request)
    {
        $member_arr = $request->get('member_id');
        if($member_arr == 'all'){
            $ids = $this->memberRepo->getAll()->pluck('id')->toArray();
        }else{
            $ids = explode(',',$member_arr);
        }
        $records = $this->memberRepo->export($ids);
        $a = Carbon::now();
        
        $lastQuarter = $a->quarter;
        foreach($records as $key=>$record){
            $record->project_month = \App\Project::where('member_id',$record->id)->whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))->count();
            $record->project_quarter = \App\Project::where('member_id',$record->id)->where(DB::raw('QUARTER(created_at)'), $lastQuarter)->whereYear('created_at',date('Y'))->count();
        }
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(20);
        $spreadsheet->getActiveSheet()
            ->setCellValue('A1', 'STT')->mergeCells("A1:A2")
            ->setCellValue('B1', 'Bộ phận')->mergeCells("B1:B2")
            ->setCellValue('C1', 'Phòng')->mergeCells("C1:C2")
            ->setCellValue('D1', 'Tổ nhóm')->mergeCells("D1:D2")
            ->setCellValue('E1', 'Team')->mergeCells("E1:E2")
            ->setCellValue('F1', 'ID')->mergeCells("F1:F2")
            ->setCellValue('G1', 'Họ tên')->mergeCells("G1:G2")
            ->setCellValue('H1', 'Chức vụ')->mergeCells("H1:H2")
            ->setCellValue('I1', 'Level')->mergeCells("I1:I2")
            ->setCellValue('J1', 'Thông tin đề án')->mergeCells("J1:K1")
            ->setCellValue('J2', 'Tháng hiện tại')
            ->setCellValue('K2', 'Quý hiện tại');
        $styleArray = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                )
            )
        );
        $spreadsheet->getActiveSheet()->getStyle("A1:K2" )->applyFromArray($styleArray);
        $bold_range = ['A1:A2', 'B1:B2', 'C1:C2', 'D1:D2','E1:E2','F1:F2','G1:G2','H1:H2','I1:I2','J1:K1','J2','K2'];
        foreach ($bold_range as $val) {
            $spreadsheet->getActiveSheet()->getStyle($val)->getFont()->setBold(true);
            $spreadsheet->getActiveSheet()->getStyle($val)->getAlignment()->setHorizontal('center');
        }
        $spreadsheet->getActiveSheet()->getStyle('D2:H2')->getFont()->setSize(8);
        $rows = 2;
        $no = 1;
        foreach ($records as $key=>$record)
        {
            $rows++;
            $spreadsheet->getActiveSheet()
                ->setCellValue('A' . $rows, $key + 1)    
                ->setCellValue('B' . $rows, $record->part->name)
                ->setCellValue('C' . $rows, $record->department->name)
                ->setCellValue('D' . $rows, $record->groups->name)
                ->setCellValue('E' . $rows, $record->team->name)
                ->setCellValue('F' . $rows, $record->login_id)
                ->setCellValue('G' . $rows, $record->full_name)
                ->setCellValue('H' . $rows, $record->position->name)
                ->setCellValue('I' . $rows, $record->level)
                ->setCellValue('J' . $rows, $record->project_month)
                ->setCellValue('K' . $rows, $record->project_quarter);
            //$rows++;
            $styleArray = array(
                'borders' => array(
                    'top' => array(
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    )
                )
            );
            $spreadsheet->getActiveSheet()->getStyle("A". $rows .":"."K" . $rows . "")->applyFromArray($styleArray);
        }
        header("Content-Description: File Transfer");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Bao_cao_luong_lai_xe_ngay_' . date('d/m/y') . '.xlsx"');
        header('Cache-Control: max-age=0');
        header("Content-Transfer-Encoding: binary");
        header('Expires: 0');
        $writer =  new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $timestamp = time();
        $writer->save('file/Thanh_vien_'.$timestamp.'.xls');
        $href = 'http://' . \Request::server('SERVER_NAME') . '/file/Thanh_vien_' . $timestamp . '.xls';
        return response()->json(['success' => 'false', 'href' => $href]);
    }
    public function getAllMessage(Request $request){
        $id = $request->get('id');
        if($request->get('full_name') != ''){
            $member_message = \DB::table('member')->leftjoin('messages','member.id','=','messages.to')->select('member.id','member.full_name')->where('member.id','<>',$id)->where('member.id','<>',1)
                            ->where('member.full_name','like','%'.$request->get('full_name').'%')->groupBy('member.id','member.full_name')->get();
        }else{
            $member_message = \DB::table('member')->leftjoin('messages','member.id','=','messages.to')->select('member.id','member.full_name')->where('member.id','<>',$id)->where('member.id','<>',1)
                            ->groupBy('member.id','member.full_name')->get();
        }
        foreach($member_message as $key=>$val){
            $my_id = $id;
            $recever_id = $val->id;
            $message = \App\Message::where(function ($query) use ($my_id, $recever_id) {
                                                $query->where('from', $recever_id)->where('to', $my_id);
                                            })->orWhere(function ($query) use ($recever_id, $my_id) {
                                                $query->where('from', $my_id)->where('to', $recever_id);
                                            })->orderBy('created_at','DESC')->first();
            $member_message[$key]->message = $message ? $message->message :'';
            $member_message[$key]->created = $message ? date('d/m, h:i a',strtotime($message->created_at)) :'';
            $member_message[$key]->created_at = $message ? $message->created_at :'';
            $member_message[$key]->is_read = ( $message && $message->is_read == 0 && $message->to == $my_id) ? 0 : 1 ;
            $member_message[$key]->from = \App\Member::find($val->id) ? \App\Member::find($val->id)->id : null;
        }
        $member_message = collect($member_message)->sortBy('created_at')->reverse();
        $html='';
        foreach( $member_message as $val){
            $image = !is_null(\App\Member::find($val->from)) ? (!is_null(\App\Member::find($val->from)->image) ? \App\Member::find($val->from)->image : '/assets2/img/img_avatar.png' ) : '/assets2/img/img_avatar.png';
            if($val->is_read == 0){
                $seen='actve-message';
            }else{
                $seen='';
            }
            if(strpos($val->message, '</a>') == true){
                $val->message='Đã gửi 1 file';
            };
            $html .= '<li class="media '.$seen.' content-mess top-0" class="user'.$val->id.'">
                        <a href="javascript:void(0)" style="width:100%" class="message" data-from="'.$id.'" data-to="'.$val->id.'">
                            <div class="mr-3 position-relative">
                                <img src="'.$image.'" width="36" height="36" class="rounded-circle" alt="">
                            </div>
                            <div class="media-body">
                                <div class="media-title">
                                    <span class="font-weight-semibold">'.$val->full_name.'</span>
                                    <span class="text-muted float-right font-size-sm">'.$val->created.'</span>
                                </div>
                                <span class="black"> '.mb_convert_encoding(substr($val->message,0,40), 'UTF-8', 'UTF-8').' </span>
                            </div>
                        </a>
                    </li>';
        }
        return response()->json(['error' => false,'html'=>$html]);
    }
    public function getMessage(Request $request){
        $input = $request->all();
        $my_id = $input['from'];
        $recever_id = $input['to'];
        \App\Message::where(['from' => $recever_id, 'to' => $my_id])->update(['is_read' => 1]);
        $messages = \App\Message::where(function ($query) use ($my_id, $recever_id) {
            $query->where('from', $recever_id)->where('to', $my_id);
        })->orWhere(function ($query) use ($recever_id, $my_id) {
            $query->where('from', $my_id)->where('to', $recever_id);
        })->orderBy('created_at','ASC')->get();
        $member = $this->memberRepo->find($recever_id);
        $html = '<div class="chat-box show">
            <div class="chat-head">
                <h6>'.$member->full_name.'</h6>
                <div class="more">
                    <span class="close-mesage"><i class="fa fa-times"></i></span>
                </div>
            </div>
            <div class="chat-list">
                <ul class="ps-container ps-theme-default ps-active-y">';
        foreach($messages as $key=>$val){
            $image = \App\Member::find($val->from)->image ?: '/assets2/img/img_avatar.png';
            if($val->from == $my_id){
                $html .='<li class="me">
                            <div class="chat-thumb"><img src="'.$image.'" alt="" class="avatar-img"></div>
                            <div class="notification-event">
                                <span class="chat-message-item">
                                    '.$val->message.'
                                </span>
                                <span class="notification-date"><time datetime="2004-07-24T18:18" class="entry-date updated">'.date('d M y, h:i a',strtotime($val->created_at)).'</time></span>
                            </div>
                        </li>';
            }else{
                $html .='<li class="you">
                            <div class="chat-thumb"><img src="'.$image.'" alt="" class="avatar-img"></div>
                            <div class="notification-event">
                                <span class="chat-message-item">
                                    '.$val->message.'
                                </span>
                                <span class="notification-date"><time datetime="2004-07-24T18:18" class="entry-date updated">'.date('d M y, h:i a',strtotime($val->created_at)).'</time></span>
                            </div>
                        </li>';
            }
        }
        $html .='<div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div>
                    <div class="ps-scrollbar-y-rail" style="top: 0px; height: 290px; right: 0px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 215px;"></div></div>
                </ul>
                <div class="text-box">
                    <input type="hidden" name="receiver_id" value="'.$recever_id.'" id="receiver_id" />
                    <input type="hidden" name="my_id" value="'.$my_id.'" id="my_id" />
                    <input type="hidden" name="type" value="1" id="type_message" />
                    <label class="custom-file-upload">
                        <input type="file" class="upload-file"/>
                        <i class="fa fa-cloud-upload-alt"></i>
                    </label>
                    <textarea placeholder="Tin nhắn..." name="message" id="content_message"></textarea>
                </div>
            </div>
        </div>
    </div>';
        return response()->json(['error' => false,'html'=>$html]);
    }
    public function sendMessage(Request $request)
        {
            if($request->get('type') == 1){
                $from = $request->my_id;
                $to = $request->receiver_id;
                $message = $request->message;
                $data = new \App\Message();
                $user = $this->memberRepo->find($from);
                $data->from = $from;
                $data->to = $to;
                $data->message = $message;
                $data->is_read = 0;
                $data->save();
                $options = array(
                    'cluster' => 'ap1',
                    'useTLS' => true
                );
                $pusher = new Pusher(
                            '5f84d2a344876b9fea04', '20b454d858313e2615b9', '1085064', $options
                    );
                $image = '/assets2/img/img_avatar.png';
                $data = ['from' => $from,'image'=>$image, 'to' => $to,'message'=>$message,'name'=>$user->name];
                $pusher->trigger('chat-message', 'send-message', $data);
            }else{
                $from = $request->my_id;
                $to = $request->receiver_id;
                $message = $request->message;
                $data = new \App\GroupMessage();
                $user = $this->memberRepo->find($from);
                $group = $this->groupRepo->find($to);
                $user_id = $group->user()->pluck('id')->toArray();
                $data->from = $from;
                $data->group_id = $to;
                $data->message = $message;
                $data->content = $message;
                $data->save();
                $options = array(
                    'cluster' => 'ap1',
                    'useTLS' => true
                );
                $pusher = new Pusher(
                            '5f84d2a344876b9fea04', '20b454d858313e2615b9', '1085064', $options
                    );
                $image = '/assets2/img/img_avatar.png';
                $data = ['from' => $from,'image'=>$image,'to' => $user_id,'message'=>$message,'user_name'=>$user->name,'group_id'=>$to,'content'=>$message];
                $pusher->trigger('chat-group-message', 'send-group-message', $data);
            }
        }
    public function sendFileMessage(Request $request)
    {
        $file = $request->file;
        $destinationPath = 'uploads';
        $files = $destinationPath . '/' . $file->getClientOriginalName();
        $file_name = $file->getClientOriginalName();
        $file->move($destinationPath, $file->getClientOriginalName());
        $ext = pathinfo($files, PATHINFO_EXTENSION);
        if($ext == 'jpg' || $ext == 'png' || $ext == 'gif'){
            $message = '<a href="/'.$files.'" data-lighter><img src="/'.$files.'" style="width:auto;height:60px"></a>';
        }else{
            $message = '<a href="/'.$files.'">'.$file_name.' <i class="fa fa-download"></i></a>';
        }
        if($request->get('type') == 1){
            $from = $request->my_id;
            $to = $request->receiver_id;
            $data = new \App\Message();
            $user = $this->memberRepo->find($from);
            $data->from = $from;
            $data->to = $to;
            $data->message = $message;
            $data->is_read = 0;
            $data->save();
            $options = array(
                'cluster' => 'ap1',
                'useTLS' => true
            );
            $pusher = new Pusher(
                        '5f84d2a344876b9fea04', '20b454d858313e2615b9', '1085064', $options
                );
            $image = '/assets2/img/img_avatar.png';
            $data = ['from' => $from,'image'=>$image,'to' => $to,'message'=>$message,'name'=>$user->name];
            $pusher->trigger('chat-message', 'send-message', $data);
        }else{
            $from = $request->my_id;
            $to = $request->receiver_id;
            $data = new \App\GroupMessage();
            $user = $this->memberRepo->find($from);
            $group = $this->groupRepo->find($to);
            $user_id = $group->user()->pluck('id')->toArray();
            $data->from = $from;
            $data->group_id = $to;
            $data->message = $message;
            $data->save();
            $options = array(
                'cluster' => 'ap1',
                'useTLS' => true
            );
            $pusher = new Pusher(
                        '5f84d2a344876b9fea04', '20b454d858313e2615b9', '1085064', $options
                );
            $image = '/assets2/img/img_avatar.png';
            $data = ['from' => $from,'image'=>$image,'to' => $user_id,'message'=>$message,'user_name'=>$user->name,'group_id'=>$to];
            $pusher->trigger('chat-group-message', 'send-group-message', $data);
        }
    }
    public function updateAvatar(Request $request){
        $this->memberRepo->update(['avatar'=>$request->get('avatar')],\Auth::guard('member')->user()->id);
        return response()->json(['success'=>'true']);
    }
    public function checkID(Request $request){
        $ID = $request->get('id');
        $check = $this->memberRepo->checkID($ID);
        if($check){
            return response()->json(['success'=>'true']);
        }else{
            return response()->json(['success'=>'false']);
        }
    }
    public function seachMemberByKeyword(Request $request){
        $input= $request->all();
        $members = $this->memberRepo->searchByKeyword($input['keyword']);
        $members_info = new \stdClass();
        foreach($members as $key=>$val){
            $object1 = new \stdClass();
            $object1->displayName = $val->full_name;
            $object1->foreignKey = $val->full_name;
            $object1->id = $val->id;
            $object1->type = 'user';
            $object1->isInvalidUser = false;
            $object1->isLoginUser = false;
            $object1->isNotUsingApp = false;
            $members_info->$key = $object1;
        }
        return response()->json(array('members_info'=>$members_info));
    }
    public function searchMemberByName(Request $request){
        $input = $request->all();
        if(isset($input['uids'])){
            $members = $this->memberRepo->whereArray($input['uids'])->get();
        }else{
            $members = $this->memberRepo->searchMember($request->get('p'));
        }
        $user = new \stdClass();
        foreach($members as $key=>$val){
            $user1 = new \stdClass();
            $user1->_id = $val->id;
            $user1->col_display_name = $val->full_name;
            $user1->col_display_name_language = 5;
            $user1->col_foreign_key = $val->full_name;
            $user1->col_nickname = "";
            $user1->col_normalized_sort_key = "";
            $user1->col_valid = null;
            $user1->icon_path = "";
            $user1->logged_in = false;
            $user1->url = "";
            $user->$key = $user1;
        }
        $facility = new \stdClass();
        $org = new \stdClass();
        $data = new \stdClass();
        $data->user=$user;
        $data->facility=$facility;
        $data->org=$org;
        return response()->json($data);
    }
    public function getUserForMobile(Request $request){
        if($request->get('gid')){
            $department_id = $request->get('gid');
            $members = $this->memberRepo->getByDepartment($department_id);
        }else{
            $members = $this->memberRepo->searchByKeyword($request->get('keyword'));
        }
        $data = new \stdClass();
        $list = new \stdClass();
        foreach($members as $key=>$val){
            $user1 = new \stdClass();
            $user1->id = "".$val->id."";
            $user1->displayName = $val->full_name;
            $user1->type = "user";
            $user1->foreignKey = $val->full_name;
            $user1->isNotUsingApp = false;
            $user1->isLoginUser = false;
            $user1->image = "";
            $list->$key = $user1;
        }
        $data->list= $list;
        $data->offset= count($members);
        $data->total= count($members);
        return response()->json($data);
    }
}
