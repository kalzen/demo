<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\ProjectRepository;
use Illuminate\Support\Facades\Session;
use Repositories\LogApprovedRepository;
use Repositories\LogSavedRepository;
use App\Repositories\MemberRepository;

class ProjectController extends Controller
{
     public function __construct(MemberRepository $memberRepo,ProjectRepository $projectRepo,LogApprovedRepository $logapprovedRepo,LogSavedRepository $logsavedRepo) {
        $this->projectRepo = $projectRepo;
        $this->logapprovedRepo = $logapprovedRepo;
        $this->logsavedRepo = $logsavedRepo;
        $this->memberRepo=$memberRepo;
    }
    public function nextPage(){
        $p_page = Session::get('p_page') + 1;
        Session::put('p_page',$p_page);
        $records = $this->projectRepo->getList(15);
        $html='';
        foreach($records as $key=>$record){
            if($record->levels){
                $level = $record->levels->name;
            }else{
                $level ='';
            }
            if($record->status == \App\Project::STATUS_CANCEL){
                $status = '<span class="badge badge-danger">Trả về</span>';
            }elseif($record->status < \App\Project::STATUS_ACTIVE ){
                $status = '<span class="badge badge-secondary">Chờ duyệt</span>';
            }else{
                $status = '<span class="badge badge-success">Đã duyệt</span>';
            }
            $html .='<tr>
                        <td colspan="1"><input style="width:20px;height:20px;" type="checkbox" name="project_id" class="check"></td>
                        <td style="display: inline-grid;">'.$status.'<span class="text-center">'.$level.'</span></td>
                        <td><a href="'.route('frontend.project.edit',$record->id).'"><span>'.$record->name.'</span></a></td>
                        <td><span>'.date('d',strtotime($record->created_at)).' tháng '.date('m',strtotime($record->created_at)).'</span></td>                               
                    </tr>';
        }
        $start = ((Session::get('p_page')-1) * 15) + 1;
        $show_page='<div style="display:inline-flex;">';
        $show_page .='<p id="page_project">'.$start.'-'.Session::get('_p_pages').' trong số '.Session::get('_p_count').'</p>';
        if(session('p_page') > 1){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="forward-page-project"><img src="public/assets2/img/left-arrow.png"></a>';
        }
        if(session('_p_count') > session('_p_pages')){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="next-page-project"><img src="public/assets2/img/forward.png"></a>';
        }
        $show_page.='</div>';
        return response()->json(array('html'=>$html,'show_page'=>$show_page));
        
    }
    public function forwardPage(){
        $page = Session::get('p_page') - 1;
        Session::put('p_page',$page);
        $records = $this->projectRepo->getList(15);
        $html='';
        foreach($records as $key=>$record){
            if($record->levels){
                $level = $record->levels->name;
            }else{
                $level ='';
            }
            if($record->status == \App\Project::STATUS_CANCEL){
                $status = '<span class="badge badge-danger">Trả về</span>';
            }elseif($record->status < \App\Project::STATUS_ACTIVE ){
                $status = '<span class="badge badge-secondary">Chờ duyệt</span>';
            }else{
                $status = '<span class="badge badge-success">Đã duyệt</span>';
            }
           $html .='<tr>
                        <td colspan="1"><input style="width:20px;height:20px;" type="checkbox" name="project_id" class="check"></td>
                        <td style="display: inline-grid;">'.$status.'<span class="text-center">'.$level.'</span></td>
                        <td><a href="'.route('frontend.project.edit',$record->id).'"><span>'.$record->name.'</span></a></td>
                        <td><span>'.date('d',strtotime($record->created_at)).' tháng '.date('m',strtotime($record->created_at)).'</span></td>                               
                    </tr>';
        }
        $start = ((Session::get('p_page')-1) * 15) + 1;
        $show_page='<div style="display:inline-flex;">';
        $show_page .='<p id="page_project">'.$start.'-'.Session::get('_p_pages').' trong số '.Session::get('_p_count').'</p>';
        if(session('p_page') > 1){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="forward-page-project"><img src="public/assets2/img/left-arrow.png"></a>';
        }
        if(session('_p_count') > session('_p_pages')){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="next-page-project"><img src="public/assets2/img/forward.png"></a>';
        }
        $show_page .='</div>';
        return response()->json(array('html'=>$html,'show_page'=>$show_page));
    }
    public function nextList(Request $request){
        $p_page = Session::get('list_page') + 1;
        Session::put('list_page',$p_page);
        $records = $this->projectRepo->getListProject(10,$request);
        $html='';
        foreach($records as $key=>$record){
            if($record->levels){
                $level = $record->levels->name;
            }else{
                $level ='';
            }
            if($record->status == \App\Project::STATUS_CANCEL){
                $status = '<span class="badge badge-danger">Trả về</span>';
            }elseif($record->status < \App\Project::STATUS_ACTIVE ){
                $status = '<span class="badge badge-secondary">Chờ duyệt</span>';
            }else{
                $status = '<span class="badge badge-success">Đã duyệt</span>';
            }
            if($record->member->is_deleted == 1){
                $full_name= '<span class="red">'.$record->member->full_name.'</span>';
            }else{
                $full_name= $record->member->full_name;
            }
            $html .='<tr>
                        <td  class="middle"><input type="checkbox" value="'.$record->id.'" name="member_id" class="check"></td>
                        <td  class="middle">'.++$key.'</td>
                        <td  class="middle">'.$full_name.'</td>
                        <td  class="middle">'.$record->member->department->name.'</td>
                        <td><a href="'.route('frontend.project.view',$record->id).'">'.$record->name.'</a></td>
                        <td  class="middle"><span class="badge badge-danger">'.$level.'</span></td>
                        <td  class="middle">'.$status.'</td>
                        <td  class="middle">'.$record->created_at().'</td>
                    </tr>';
        }
        $start = ((Session::get('list_page')-1) * 10) + 1;
        $show_page='<div style="display:inline-flex;">';
        $show_page .='<p id="page_member">'.$start.'-'.Session::get('_list_pages').' trong số '.Session::get('_list_count').'</p>';
        if(session('list_page') > 1){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="forward-list-project"><img src="/public/assets2/img/left-arrow.png"></a>';
        }
        if(session('_list_count') > session('_list_pages')){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="next-list-project"><img src="/public/assets2/img/forward.png"></a>';
        }
        $show_page .='</div>';
        return response()->json(array('html'=>$html,'show_page'=>$show_page));
        
    }
    public function forwardList(Request $request){
        $page = Session::get('list_page') - 1;
        Session::put('list_page',$page);
        $records = $this->projectRepo->getListProject(10,$request);
        $html='';
        foreach($records as $key=>$record){
            if($record->levels){
                $level = $record->levels->name;
            }else{
                $level ='';
            }
            if($record->status == \App\Project::STATUS_CANCEL){
                $status = '<span class="badge badge-danger">Trả về</span>';
            }elseif($record->status < \App\Project::STATUS_ACTIVE ){
                $status = '<span class="badge badge-secondary">Chờ duyệt</span>';
            }else{
                $status = '<span class="badge badge-success">Đã duyệt</span>';
            }
            if($record->member->is_deleted == 1){
                $full_name= '<span class="red">'.$record->member->full_name.'</span>';
            }else{
                $full_name= $record->member->full_name;
            }
            $html .='<tr>
                        <td  class="middle"><input type="checkbox" value="'.$record->id.'" name="member_id" class="check"></td>
                        <td  class="middle">'.++$key.'</td>
                        <td  class="middle">'.$full_name.'</td>
                        <td  class="middle">'.$record->member->department->name.'</td>
                        <td><a href="'.route('frontend.project.view',$record->id).'">'.$record->name.'</a></td>
                        <td  class="middle"><span class="badge badge-danger">'.$level.'</span></td>
                        <td  class="middle">'.$status.'</td>
                        <td  class="middle">'.$record->created_at().'</td>
                    </tr>';
        }
        $start = ((Session::get('list_page')-1) * 10) + 1;
        $show_page = '<div style="display:inline-flex;">';
        $show_page .='<p id="page_member">'.$start.'-'.Session::get('_list_pages').' trong số '.Session::get('_list_count').'</p>';
        if(session('list_page') > 1){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="forward-list-project"><img src="/public/assets2/img/left-arrow.png"></a>';
        }
        if(session('_list_count') > session('_list_pages')){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="next-list-project"><img src="/public/assets2/img/forward.png"></a>';
        }
        $show_page .='</div>';
        return response()->json(array('html'=>$html,'show_page'=>$show_page));
        
    }
    public function getListProject(Request $request){
        Session::put('list_page',1);
        if(!is_null($request->get('month'))){
            $records = $this->projectRepo->getListProject(10,$request);
        }else{
            $records = $this->projectRepo->getFillterByMember(10,$request);
        }
        $html='';
        foreach($records as $key=>$record){
            if($record->levels){
                $level = $record->levels->name;
            }else{
                $level ='';
            }
            if($record->status == \App\Project::STATUS_CANCEL){
                $status = '<span class="badge badge-danger">Trả về</span>';
            }elseif($record->status < \App\Project::STATUS_ACTIVE ){
                $status = '<span class="badge badge-secondary">Chờ duyệt</span>';
            }else{
                $status = '<span class="badge badge-success">Đã duyệt</span>';
            }
            $html .='<tr>
                        <td class="middle"><input type="checkbox" value="'.$record->id.'" name="member_id" class="check"></td>
                        <td  class="middle">'.++$key.'</td>
                        
                        <td  class="middle">'.$record->member->full_name.'</td>
                        <td  class="middle">'.$record->member->department->name.'</td>
                        <td><a href="'.route('frontend.project.view',$record->id).'">'.$record->name.'</a></td>
                        <td  class="middle"><span class="badge badge-danger">'.$level.'</span></td>
                        <td  class="middle">'.$status.'</td>
                        <td  class="middle">'.$record->created_at().'</td>
                    </tr>';
        }
        $start = ((Session::get('list_page')-1) * 10) + 1;
        $show_page ='<p id="page_member">'.$start.'-'.Session::get('_list_pages').' trong số '.Session::get('_list_count').'</p>';
        if(session('list_page') > 1){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="forward-list-project"><img src="/public/assets2/img/left-arrow.png"></a>';
        }
        if(session('_list_count') > session('_list_pages')){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="next-list-project"><img src="/public/assets2/img/forward.png"></a>';
        }
        return response()->json(array('html'=>$html,'show_page'=>$show_page));
        
    }
    public function save(Request $request){
        $project_arr = $request->get('project_id');
        if($project_arr == 'all'){
            $project_id = $this->projectRepo->all()->pluck('id')->toArray();
        }else{
            $project_id = explode(',',$project_arr);
        }
        $this->projectRepo->save($project_id);
        foreach($project_id as $val){
            $log['member_id']= \Auth::guard('member')->user()->id;
            $log['project_id'] = $val;
            $this->logsavedRepo->create($log);
        }
        return response()->json(array('success'=>true));
    }
    public function send(Request $request){
        $project_arr = $request->get('project_id');
        if($project_arr == 'all'){
            $project_id = $this->projectRepo->all()->pluck('id')->toArray();
        }else{
            $project_id = explode(',',$project_arr);
        }
        $this->projectRepo->send($project_id);
        foreach($project_id as $val){
            $log['member_id']= \Auth::guard('member')->user()->id;
            $log['project_id'] = $val;
            $log['level'] = \Auth::guard('member')->user()->level;
            $this->logapprovedRepo->create($log);
            $record = $this->projectRepo->find($val);
            if( $record->status < \App\Project::STATUS_ACTIVE || ($record->status == \App\Project::STATUS_ACTIVE && $record->level < 8)){
                $this->memberRepo->Notificate(\Auth::guard('member')->user()->level + 1, ['content' => 'Có đề án mới', 'link' => '/project/view/' . $val]);
            }
        }
        return response()->json(array('success'=>true));
    }
    public function submit(Request $request){
        $project_arr = $request->get('project_id');
        if($project_arr == 'all'){
            $project_id = $this->projectRepo->all()->pluck('id')->toArray();
        }else{
            $project_id = explode(',',$project_arr);
        }
        $this->projectRepo->send($project_id);
        return response()->json(array('success'=>true));
    }
    public function destroy(Request $request){
        $project_arr = $request->get('project_id');
        if($project_arr == 'all'){
            $project_id = $this->projectRepo->all()->pluck('id')->toArray();
        }else{
            $project_id = explode(',',$project_arr);
        }
        $this->projectRepo->remove($project_id);
        return response()->json(array('success'=>true));
    }
    public function returnProject(Request $request){
        $input = $request->all();
        $project_arr = $request->get('project_id');
        if($project_arr == 'all'){
            $project_id = $this->projectRepo->all()->pluck('id')->toArray();
        }else{
            $project_id = explode(',',$project_arr);
        }
        $input['status'] = \App\Project::STATUS_CANCEL;
        $this->logapprovedRepo->deleteProject($project_id);
        foreach($project_id as $val){
            $this->projectRepo->update($input,$val);
            $project = $this->projectRepo->find($val);
            $this->memberRepo->NotificateMember($project->member_id, ['content' => 'Đề án trả về', 'link' => '/project/edit/' . $val]);
        }
        return response()->json(array('success'=>true));
    }
    function htmlToPlainText($str){
        $str = html_entity_decode($str, ENT_QUOTES | ENT_XML1, 'UTF-8');
        $str = htmlspecialchars_decode($str);
        $str = html_entity_decode($str);
        $str = strip_tags($str);
        return $str;
    }
    public function export(Request $request)
    {
        $project_arr = $request->get('project_id');
        if($project_arr == 'all'){
            $ids = $this->projectRepo->getAll()->pluck('id')->toArray();
        }else{
            $ids = explode(',',$project_arr);
        }
        $records = $this->projectRepo->export($ids);
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(70);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $spreadsheet->getActiveSheet()
            ->setCellValue('A1', 'No')
            ->setCellValue('B1', 'User ID')
            ->setCellValue('C1', 'User Name')
            ->setCellValue('D1', 'Tên đề tài')
            ->setCellValue('E1', 'Cấp độ')
            ->setCellValue('F1', 'Ngày');
        $styleArray = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                )
            )
        );
        $spreadsheet->getActiveSheet()->getStyle("A1:F1" )->applyFromArray($styleArray);
        $bold_range = ['A1', 'B1', 'C1', 'D1','E1','F1'];
        foreach ($bold_range as $val) {
            $spreadsheet->getActiveSheet()->getStyle($val)->getFont()->setBold(true);
            $spreadsheet->getActiveSheet()->getStyle($val)->getAlignment()->setHorizontal('center');
        }
        $rows = 2;
        $no = 1;
        foreach ($records as $key=>$record)
        {
            if($record->levels){
                $level = $record->levels->name;
            }else{
                $level = '---';
            }
            $spreadsheet->getActiveSheet()
                ->setCellValue('A' . $rows, ++$key) 
                ->setCellValue('B' . $rows, $record->member->login_id)
                ->setCellValue('C' . $rows, $record->member->full_name)
                ->setCellValue('D' . $rows, $record->name)
                ->setCellValue('E' . $rows,$level)
                ->setCellValue('F' . $rows, $record->created_at()) 
                ;
            $rows++;
            $styleArray = array(
                'borders' => array(
                    'top' => array(
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    )
                )
            );
            $spreadsheet->getActiveSheet()->getStyle("A". $rows .":"."F" . $rows . "")->applyFromArray($styleArray);
        }
        header("Content-Description: File Transfer");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Bao_cao_luong_lai_xe_ngay_' . date('d/m/y') . '.xlsx"');
        header('Cache-Control: max-age=0');
        header("Content-Transfer-Encoding: binary");
        header('Expires: 0');
        $writer =  new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $timestamp = time();
        $writer->save('file/De_an_'.$timestamp.'.xls');
        $href = 'http://' . \Request::server('SERVER_NAME') . '/file/De_an_' . $timestamp . '.xls';
        return response()->json(['success' => 'false', 'href' => $href]);
    }
    public function exportProject(Request $request)
    {
        $project_arr = $request->get('project_id');
        if($project_arr == 'all'){
            $ids = $this->projectRepo->getAll()->pluck('id')->toArray();
        }else{
            $ids = explode(',',$project_arr);
        }
        $records = $this->projectRepo->export($ids);
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(70);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(70);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $spreadsheet->getActiveSheet()
            ->setCellValue('A1', 'No')
            ->setCellValue('B1', 'Tên đề tài')
            ->setCellValue('C1', 'Trước cải tiến')
            ->setCellValue('D1', 'Sau cải tiến')   
            ->setCellValue('E1', 'Cấp độ')
            ->setCellValue('F1', 'Ngày');
        $styleArray = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                )
            )
        );
        $spreadsheet->getActiveSheet()->getStyle("A1:F1" )->applyFromArray($styleArray);
        $bold_range = ['A1', 'B1', 'C1', 'D1','E1','F1'];
        foreach ($bold_range as $val) {
            $spreadsheet->getActiveSheet()->getStyle($val)->getFont()->setBold(true);
            $spreadsheet->getActiveSheet()->getStyle($val)->getAlignment()->setHorizontal('center');
        }
        $rows = 2;
        $no = 1;
        foreach ($records as $key=>$record)
        {
            if($record->levels){
                $level = $record->levels->name;
            }else{
                $level = '---';
            }
            $spreadsheet->getActiveSheet()
                ->setCellValue('A' . $rows, ++$key) 
                ->setCellValue('B' . $rows, $record->name)
                ->setCellValue('C' . $rows, $this->htmlToPlainText($record->before_content))
                ->setCellValue('D' . $rows, $this->htmlToPlainText($record->after_content))
                ->setCellValue('E' . $rows, $level)
                ->setCellValue('F' . $rows, $record->created_at()) 
                ;
            $rows++;
            $styleArray = array(
                'borders' => array(
                    'top' => array(
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    )
                )
            );
            $spreadsheet->getActiveSheet()->getStyle("A". $rows .":"."F" . $rows . "")->applyFromArray($styleArray);
        }
        header("Content-Description: File Transfer");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Bao_cao_luong_lai_xe_ngay_' . date('d/m/y') . '.xlsx"');
        header('Cache-Control: max-age=0');
        header("Content-Transfer-Encoding: binary");
        header('Expires: 0');
        $writer =  new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $timestamp = time();
        $writer->save('file/De_an_'.$timestamp.'.xls');
        $href = 'http://' . \Request::server('SERVER_NAME') . '/file/De_an_' . $timestamp . '.xls';
        return response()->json(['success' => 'false', 'href' => $href]);
    }
    public function searchProject(Request $request){
        $search = $request->all();
        $records = $this->projectRepo->search($search);
        $html='';
        foreach($records as $key=>$record){
            if($record->levels){
                $level = $record->levels->name;
            }else{
                $level ='';
            }
            if($record->status == \App\Project::STATUS_CANCEL){
                $status = '<span class="badge badge-danger">Trả về</span>';
            }elseif($record->status < \App\Project::STATUS_ACTIVE ){
                $status = '<span class="badge badge-secondary">Chờ duyệt</span>';
            }else{
                $status = '<span class="badge badge-success">Đã duyệt</span>';
            }
            $html .='<tr>
                        <td class="middle"><input type="checkbox" value="'.$record->id.'" name="member_id" class="check"></td>
                        <td  class="middle">'.++$key.'</td>
                        
                        <td  class="middle">'.$record->member->full_name.'</td>
                        <td  class="middle">'.$record->member->department->name.'</td>
                        <td><a href="'.route('frontend.project.view',$record->id).'">'.$record->name.'</a></td>
                        <td  class="middle"><span class="badge badge-danger">'.$level.'</span></td>
                        <td  class="middle">'.$status.'</td>
                        <td  class="middle">'.$record->created_at().'</td>
                    </tr>';
        }
        $start = ((Session::get('list_page')-1) * 10) + 1;
        $show_page ='<p id="page_member">'.$start.'-'.Session::get('_list_pages').' trong số '.Session::get('_list_count').'</p>';
        if(session('list_page') > 1){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="forward-list-project"><img src="/public/assets2/img/left-arrow.png"></a>';
        }
        if(session('_list_count') > session('_list_pages')){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="next-list-project"><img src="/public/assets2/img/forward.png"></a>';
        }
        return response()->json(array('html'=>$html,'show_page'=>$show_page));
    }
}
