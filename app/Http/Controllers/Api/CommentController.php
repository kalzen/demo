<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\CommentRepository;

class CommentController extends Controller
{
    public function __construct(CommentRepository $commentRepo){
        $this->commentRepo = $commentRepo;
    }
    public function send(Request $request){
        $input = $request->all();
        $input['member_id'] = \Auth::guard('member')->user()->id;
        $record = $this->commentRepo->create($input);
        $html = '
                    <div class="comment" id="comment'.$record->id.'">
                   <div class="ui icon basic tiny buttons hide"><a class="ui button delete-comment" data-comment_id="'.$record->id.'" href="javascript:void(0)"><i class="icon-cross"></i></a></div>
                   <a class="avatar"><img class="ui avatar image" src="'.(is_null($record->member->avatar) ? asset('img/user30.png') : $record->member->avatar).'"></a>
                   <div class="content">
                      <a class="author">'.$record->member->full_name.'</a>
                      <div class="metadata">
                         <div class="date">'.\App\Helpers\StringHelper::time_ago($record->created_at).'</div>
                      </div>
                      <div class="text">'.$record->comment.'</div>
                      <div class="extra images"></div>
                   </div>
               </div>';
        return response()->json(array('success' => 'true','html'=>$html));
    }
    public function delete(Request $request){
        $input = $request->all();
        $comment = $this->commentRepo->find($input['comment_id']);
        if(\Auth::guard('member')->user()->id == $comment->member_id){
            $this->commentRepo->delete($input['comment_id']);
        }
        return response()->json(array('success' => 'true','id'=>'comment'.$input['comment_id']));
    }
}
