<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\FolderRepository;

class FolderController extends Controller
{
    public function __construct(FolderRepository $folderRepo) {
        $this->folderRepo = $folderRepo;
    }
    public function getJson(Request $request){
        dd($request->all());
    }
}
