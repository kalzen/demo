<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CabinetController extends Controller
{
    public function addFile(Request $request){
        $targetDir = "/public/file/";
        $allowTypesFile = array('pdf', 'docx', 'xslx','doc','xls','jpg','png','jpeg','gif', 'PNG', 'GIF', 'JPG');
        $statusMsg = $errorMsg = $insertValuesSQL = $errorUpload = $errorUploadType = '';
        $fileName = basename($_FILES['file']['name']);
        if(strlen($fileName)!=0){
            $fileType = pathinfo($fileName);
            // Check whether file type is valid
            if(in_array($fileType['extension'], $allowTypesFile)){
                move_uploaded_file($_FILES['file']['tmp_name'],'file/'.$fileName);
                $link = $targetDir . $fileName;
                $size = \App\Helpers\StringHelper::ConvertSizeFile($_FILES['file']['size']);
                $name = $fileName;
            }
        }
        return response()->json(array('success' => 'COMPLETE','link'=>$link,'size'=>$size,'name'=>$name));
    }
}
