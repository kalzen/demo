<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\NotificationScheduleRepository;

class NotificationController extends Controller
{
    public function __construct(NotificationScheduleRepository $notiRepo) {
        $this->notiRepo = $notiRepo;
    }
    public function loadList(Request $request){
        $html = '<tr valign="top">
                    <th nowrap="">
                       <div style="padding-top: 2px;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Người gửi</font></font></div>
                    </th>
                    <th nowrap="">Nội dung</th>
                    <th nowrap="">Ngày tạo</th>
                 </tr>';
        $notifications = $this->notiRepo->getByStatus($request->get('status'));
        foreach($notifications as $key=>$val){
            $html .='<tr valign="top">
                        <td nowrap=""><span class="selectlist_user_grn"></span>'.$val->member->full_name.'</th>
                        <td nowrap="">
                            <a href="'.$val->link.'">'.$val->content.'</a>
                        </td>
                        <td nowrap="">'.$val->created_at().'</th>
                    </tr>';
        }
        return response()->json(array('html'=>$html));
    }
}
