<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\StringHelper;
use Illuminate\Support\Facades\Auth;
use Mail;
use Repositories\PartRepository;
use Repositories\DepartmentRepository;
use Repositories\GroupsRepository;
use Repositories\TeamRepository;
use Repositories\LevelRepository;
use Repositories\PositionRepository;
use App\Repositories\SlideRepository;
use App\Repositories\MemberRepository;
use Repositories\DocumentRepository;
use Repositories\EquipmentRepository;
use Repositories\WorkRepository;
use Illuminate\Support\Facades\File; 

class FrontendController extends Controller {

    //
    public function __construct(WorkRepository $workRepo,EquipmentRepository $equipmentRepo,DocumentRepository $documentRepo,MemberRepository $memberRepo,SlideRepository $slideRepo,PositionRepository $positionRepo,PartRepository $partRepo,DepartmentRepository $departmentRepo,GroupsRepository $groupsRepo,TeamRepository $teamRepo,LevelRepository $levelRepo) {
       $this->partRepo = $partRepo;
       $this->departmentRepo = $departmentRepo;
       $this->groupsRepo = $groupsRepo;
       $this->teamRepo = $teamRepo;
       $this->levelRepo = $levelRepo;
       $this->positionRepo = $positionRepo; 
       $this->slideRepo = $slideRepo;
       $this->memberRepo= $memberRepo;
       $this->documentRepo= $documentRepo;
       $this->equipmentRepo= $equipmentRepo;
       $this->workRepo= $workRepo;
    }
    public function addPart(Request $request){
        $input = $request->all();
        $part=$this->partRepo->create($input);
        $this->memberRepo->updatePart($input['member_id'],$part->id);
        return response()->json(array('success'=>true));
    }
    public function editPart(Request $request){
        $data = $this->partRepo->find($request->get('id'))->toArray();
        $part = $this->partRepo->find($request->get('id'));
        $data['select'] = \App\Helpers\StringHelper::getSelectMemberOptions(\App\Member::where('is_deleted',0)->get(),$part->member->pluck('id')->toArray()); 
        return response()->json(array('data'=>$data));
    }
    public function updatePart(Request $request){
        $input = $request->all();
        $this->memberRepo->updatePart($input['member_id'],$input['id']);
        $data = $this->partRepo->update($input,$input['id']);
        return response()->json(array('success'=>true));
    }
    //
    public function addEquipment(Request $request){
        $input = $request->all();
        $this->equipmentRepo->create($input);
        return response()->json(array('success'=>true));
    }
    public function editEquipment(Request $request){
        $data = $this->equipmentRepo->find($request->get('id'))->toArray();
        return response()->json(array('data'=>$data));
    }
    public function updateEquipment(Request $request){
        $input = $request->all();
        $this->equipmentRepo->update($input,$input['id']);
        return response()->json(array('success'=>true));
    }
    //
     public function addDepartment(Request $request){
        $input = $request->all();
        $this->departmentRepo->create($input);
        return response()->json(array('success'=>true));
    }
    public function editDepartment(Request $request){
        $data = $this->departmentRepo->find($request->get('id'))->toArray();
        return response()->json(array('data'=>$data));
    }
    public function updateDepartment(Request $request){
        $input = $request->all();
        $data = $this->departmentRepo->update($input,$input['id']);
        return response()->json(array('success'=>true));
    }
    //
    public function addGroups(Request $request){
        $input = $request->all();
        $this->groupsRepo->create($input);
        return response()->json(array('success'=>true));
    }
    public function editGroups(Request $request){
        $data = $this->groupsRepo->find($request->get('id'))->toArray();
        return response()->json(array('data'=>$data));
    }
    public function updateGroups(Request $request){
        $input = $request->all();
        $data = $this->groupsRepo->update($input,$input['id']);
        return response()->json(array('success'=>true));
    }
    //
    public function addTeam(Request $request){
        $input = $request->all();
        $team=$this->teamRepo->create($input);
        $this->memberRepo->updateTeam($input['member_id'],$team->id);
        return response()->json(array('success'=>true));
    }
    public function editTeam(Request $request){
        $data = $this->teamRepo->find($request->get('id'))->toArray();
        $team = $this->teamRepo->find($request->get('id'));
        $data['select'] = \App\Helpers\StringHelper::getSelectMemberOptions(\App\Member::where('is_deleted',0)->get(),$team->member->pluck('id')->toArray()); 
        return response()->json(array('data'=>$data));
    }
    public function updateTeam(Request $request){
        $input = $request->all();
        $this->memberRepo->updateTeam($input['member_id'],$input['id']);
        $data = $this->teamRepo->update($input,$input['id']);
        return response()->json(array('success'=>true));
    }
    //
     public function addLevel(Request $request){
        $input = $request->all();
        $this->levelRepo->create($input);
        return response()->json(array('success'=>true));
    }
    public function editLevel(Request $request){
        $data = $this->levelRepo->find($request->get('id'))->toArray();
        return response()->json(array('data'=>$data));
    }
    public function updateLevel(Request $request){
        $input = $request->all();
        $data = $this->levelRepo->update($input,$input['id']);
        return response()->json(array('success'=>true));
    }
    //
    public function addPosition(Request $request){
        $input = $request->all();
        $this->positionRepo->create($input);
        return response()->json(array('success'=>true));
    }
    public function editPosition(Request $request){
        $data = $this->positionRepo->find($request->get('id'))->toArray();
        return response()->json(array('data'=>$data));
    }
    public function updatePosition(Request $request){
        $input = $request->all();
        $data = $this->positionRepo->update($input,$input['id']);
        return response()->json(array('success'=>true));
    }
    //
    public function addSlide(Request $request){
        $input = $request->all();
        $this->slideRepo->create($input);
        return response()->json(array('success'=>true));
    }
    public function editSlide(Request $request){
        $data = $this->slideRepo->find($request->get('id'))->toArray();
        return response()->json(array('data'=>$data));
    }
    public function updateSlide(Request $request){
        $input = $request->all();
        $data = $this->slideRepo->update($input,$input['id']);
        return response()->json(array('success'=>true));
    }
    //
    public function addDocument(Request $request){
        $input = $request->all();
        $this->documentRepo->create($input);
        return response()->json(array('success'=>true));
    }
    public function editDocument(Request $request){
        $data = $this->documentRepo->find($request->get('id'))->toArray();
        return response()->json(array('data'=>$data));
    }
    public function updateDocument(Request $request){
        $input = $request->all();
        if(!isset($input['status'])){
            $input['status']=0;
        }
        $data = $this->documentRepo->update($input,$input['id']);
        return response()->json(array('success'=>true));
    }
    //
    public function addWork(Request $request){
        $input = $request->all();
        $this->workRepo->create($input);
        return response()->json(array('success'=>true));
    }
    public function editWork(Request $request){
        $data = $this->workRepo->find($request->get('id'))->toArray();
        return response()->json(array('data'=>$data));
    }
    public function updateWork(Request $request){
        $input = $request->all();
        $data = $this->workRepo->update($input,$input['id']);
        return response()->json(array('success'=>true));
    }
    public function upload(Request $request){
        $targetDir = "/upload/images/";
        $allowTypes = array('jpg','png','jpeg','gif', 'PNG', 'GIF', 'JPG', 'pdf', 'docx', 'xslx');
        $statusMsg = $errorMsg = $insertValuesSQL = $errorUpload = $errorUploadType = '';
        foreach($_FILES['file']['name'] as $key=>$val){
            $fileName = basename($val);
            if(strlen($fileName)!=0){
                $fileType = pathinfo($fileName);
                $fileName = time().'_'.$fileName;
                if(in_array($fileType['extension'], $allowTypes)){
                    // Upload file to server
                    move_uploaded_file($_FILES['file']['tmp_name'][$key],'upload/images/'.$fileName);
                }elseif(in_array($fileType['extension'], $allowTypes)){
                    move_uploaded_file($_FILES['file']['tmp_name'][$key],'upload/images/'.$fileName);
                }
                $targetFilePath = $targetDir . $fileName;
                $images[] = $targetFilePath;
                $name[] = $fileName;
            }
        }
        return response()->json(array('success' => true, 'image' => $images,'name'=>$name));
    }
    public function uploadImage(Request $request){
        $targetDir = "/upload/images/";
        $allowTypesImage = array('jpg','png','jpeg','gif', 'PNG', 'GIF', 'JPG');
        $allowTypesFile = array('pdf', 'docx', 'xslx','doc','xls');
        $statusMsg = $errorMsg = $insertValuesSQL = $errorUpload = $errorUploadType = '';
        $fileName = basename($_FILES['file']['name']);
        if(strlen($fileName)!=0){
            $fileType = pathinfo($fileName);
            $fileName = time().'_'.$fileName;
            // Check whether file type is valid
            if(in_array($fileType['extension'], $allowTypesImage)){                   
                // Upload file to server
                move_uploaded_file($_FILES['file']['tmp_name'],'upload/images/'.$fileName);
                $type = 1;
            }elseif(in_array($fileType['extension'], $allowTypesFile)){
                $targetDir = "file/";
                move_uploaded_file($_FILES['file']['tmp_name'],'file/'.$fileName);
                $type = 2;
            }
            $targetFilePath = $targetDir . $fileName;
            $images=$targetFilePath;
        }
        return response()->json(array('success' => true,'type'=>$type, 'image' => $images,'name'=>$fileName));
    }
    public function delete_image(Request $request){
         File::delete('..'.$request->get('link'));
    }
}
