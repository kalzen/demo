<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\ToDoRepository;

class ToDoController extends Controller
{
    public function __construct(ToDoRepository $todoRepo) {
        $this->todoRepo = $todoRepo;
    }
    public function loadTodo(Request $request){
        $html = '<tr valign="top">
                    <th nowrap=""></th>
                    <th nowrap="">
                       <div style="padding-top: 2px;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">To-do</font></font></div>
                    </th>
                    <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Status</font></font></th>
                    <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Due date</font></font></th>
                    <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Priority</font></font></th>
                 </tr>';
        $todos = $this->todoRepo->getByStatus($request->get('status'));
        foreach($todos as $key=>$val){
            $html .='<tr class="linetwo">
                        <td nowrap=""><input type="checkbox" name="id[]" id="id[]" class="" value="'.$val->id.'"></td>
                        <td><span class="nowrap-grn "><a href="'.route('frontend.todo.view',$val->id).'">'.$val->title.'</a></span></td>
                        <td nowrap=""><span class="badge badge-danger">'.$val->nameStatus().'</span></td>
                        <td nowrap=""> '.$val->end_date().' </td>
                        <td nowrap=""><span class="badge badge-danger">'.$val->namePriority().'</span></td>
                     </tr>';
        }
        if($request->get('status') == 0){
            $check_list_html = '<a class="complete-list-todo" href="javascript:void(0)" style="height:28px!important;">Complete</a>';
        }else{
            $check_list_html = '<a class="delete-list-todo" href="javascript:void(0)" style="height:28px!important;">Delete</a>';
        }
        return response()->json(array('html'=>$html,'check_list_html'=>$check_list_html));
    }
    public function completeList(Request $request){
        $this->todoRepo->updateList($request->get('id'));
        $todos = $this->todoRepo->getByStatus(0);
        $html = '<tr valign="top">
                    <th nowrap=""></th>
                    <th nowrap="">
                       <div style="padding-top: 2px;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">To-do</font></font></div>
                    </th>
                    <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Status</font></font></th>
                    <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Due date</font></font></th>
                    <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Priority</font></font></th>
                 </tr>';
        foreach($todos as $key=>$val){
            $html .='<tr class="linetwo">
                        <td nowrap=""><input type="checkbox" name="id[]" id="id[]" class="" value="'.$val->id.'"></td>
                        <td><span class="nowrap-grn "><a href="'.route('frontend.todo.view',$val->id).'">'.$val->title.'</a></span></td>
                        <td nowrap=""><span class="badge badge-danger">'.$val->nameStatus().'</span></td>
                        <td nowrap=""> '.$val->end_date().' </td>
                        <td nowrap=""><span class="badge badge-danger">'.$val->namePriority().'</span></td>
                     </tr>';
        }
        return response()->json(array('html'=>$html));
    }
    public function deleteList(Request $request){
        $this->todoRepo->deleteList($request->get('id'));
        $todos = $this->todoRepo->getByStatus(1);
        $html = '<tr valign="top">
                    <th nowrap=""></th>
                    <th nowrap="">
                       <div style="padding-top: 2px;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">To-do</font></font></div>
                    </th>
                    <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Category</font></font></th>
                    <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Due date</font></font></th>
                    <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Priority</font></font></th>
                 </tr>';
        foreach($todos as $key=>$val){
            $category = $val->category ? $val->category->title : '' ; 
            $due_date = is_null($val->due_date) ? 'None' : $val->due_date();
            $html .='<tr class="linetwo">
                        <td nowrap=""><input type="checkbox" name="id[]" id="id[]" class="" value="'.$val->id.'"></td>
                        <td><span class="nowrap-grn "><a href="'.route('frontend.todo.view',$val->id).'">'.$val->title.'</a></span></td>
                        <td nowrap=""> '.$category.' </td>
                        <td nowrap=""> '.$due_date.' </td>
                        <td nowrap="">
                             <span class="rank_star_base_grn">';
            for($i = 0;$i<$val->priority;$i++){
                $html .='<span class="rank_star_grn"></span>';
            }                
            $html .= '</span></td></tr>';
        }
        return response()->json(array('html'=>$html));
    }
    public function getSelectStatus(Request $request){
        $record = $this->todoRepo->find($request->get('id'));
        $status_html = \App\Helpers\StringHelper::getSelectHtml(\App\ToDo::STATUS_ARR,$record->member()->where('id',\Auth::guard('member')->user()->id)->first()->pivot->status);
        return response()->json(array('html'=>$status_html));
    }
}
