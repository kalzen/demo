<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

class AuthController extends Controller {

    public function __construct() {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin() {
        return view('backend.auth.login');
    }

    /**
     *
     * @param \Illuminate\Http\Request $request
     */
    public function postLogin(Request $request) {
        $input = [
            'username' => $request->get('username'),
            'password' => $request->get('password')
        ];

        if (Auth::attempt($input)) {
            $user = Auth::user();
            $user->save();
            session_start();
            $_SESSION['KCFINDER'] = []; //
            $_SESSION['KCFINDER'] = array('disabled' => false, 'uploadURL' => "/public/upload");
            return Redirect::route('admin.index');
        }
        return Redirect::route('login')->with('error', 'Wrong login account');
    }
    public function loginMember(Request $request) {
        $input = [
            'login_id' => $request->get('login_id'),
            'password' => $request->get('password'),
            'is_deleted'=>0,
        ];
        if (Auth::guard('member')->attempt($input)) {
            $member = Auth::guard('member')->user();
            $member['api_token'] = $member->createToken('MyApp')->accessToken;
            $member->save();
            return Redirect::route('home.view');
        }
        return Redirect::route('home.index')->with('error', 'Đăng nhập không thành công');
    }
    public function logoutMember() {
        \Auth::guard('member')->user()->tokens()->delete();
        Auth::logout();
        return Redirect::route('home.index');
    }

    /**
     *
     * @return type
     */
    public function logout() {
        unset($_SESSION);
        session_destroy();
        Auth::logout();
        return Redirect::route('login');
    }

}
