<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\ToDoRepository;
use Repositories\ToDoCategoryRepository;
use App\Repositories\MemberRepository;
use App\Repositories\CommentRepository;
use App\Repositories\MemberTodoRepository;

class ToDoController extends Controller
{
    public function __construct(MemberTodoRepository $membertodoRepo,ToDoRepository $todoRepo,ToDoCategoryRepository $todocateRepo,MemberRepository $memberRepo,CommentRepository $commentRepo){
        $this->todoRepo = $todoRepo;
        $this->todocateRepo=$todocateRepo;
        $this->memberRepo = $memberRepo;
        $this->commentRepo = $commentRepo;
        $this->membertodoRepo = $membertodoRepo;
    }
    public function index(Request $request) {
        $records = $this->todoRepo->getIndex($request);
        $categorys = $this->todocateRepo->all();
        $todos = $this->todocateRepo->all();
        if (config('global.device') != 'pc') {
            return view('mobile/todo/index', compact('records','categorys'));
        }else{
            return view('frontend/todo/index', compact('records','categorys'));
        }
    }
    public function history(Request $request) {
        $records = $this->todoRepo->getHistory($request);
        $categorys = $this->todocateRepo->all();
        if (config('global.device') != 'pc') {
            return view('mobile/todo/history', compact('records','categorys'));
        }else{
            return view('frontend/todo/history', compact('records','categorys'));
        }
    }
    public function create() {
        $category_html = \App\Helpers\StringHelper::getSelectTodoCategoryOptions($this->todocateRepo->all());
        $members = $this->memberRepo->getAll();
        $member_html = \App\Helpers\StringHelper::getSelectMemberOptions($members);
        $status_html = \App\Helpers\StringHelper::getSelectHtml(\App\ToDo::STATUS_ARR);
        $priority_html = \App\Helpers\StringHelper::getSelectHtml(\App\ToDo::PRIORITY_ARR);
        if (config('global.device') != 'pc') {
            return view('mobile/todo/create',compact('priority_html','status_html','member_html'));
        }else{
            return view('frontend/todo/create',compact('priority_html','status_html','member_html'));
        }
    }
    public function store(Request $request) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->todoRepo->validateCreate());
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $input['created_by'] = \Auth::guard('member')->user()->id;
        $todo = $this->todoRepo->create($input);
        if(isset($input['member_id']) && $input['member_id'] > 0){
            $todo->member()->attach($input['member_id']);
            $this->memberRepo->NotificateMembers($input['member_id'],['content'=>'Bạn được giao 1 công việc','link'=>route('frontend.todo.view',$todo->id)]);
        }
        if ($todo) {
            return  redirect()->route('frontend.todo.index')->with('success','Tạo mới thành công');
        }else{
            return  redirect()->route('frontend.todo.index')->with('error','Tạo mới thất bại');
        }
    }
    public function view($id){
        $record= $this->todoRepo->find($id);
        $comments = $this->commentRepo->getByToDo($id);
        $membertodo = $this->membertodoRepo->getRecord(\Auth::guard('member')->user()->id,$id);
        if (config('global.device') != 'pc') {
            return view('mobile/todo/view',compact('record','comments','membertodo'));
        }else{
            return view('frontend/todo/view',compact('record','comments','membertodo'));
        }
    }
    public function edit($id){
        $record = $this->todoRepo->find($id);
        $members = $this->memberRepo->getAll();
        $member_html = \App\Helpers\StringHelper::getSelectMemberOptions($members);
        $status_html = \App\Helpers\StringHelper::getSelectHtml(\App\ToDo::STATUS_ARR,$record->status);
        $priority_html = \App\Helpers\StringHelper::getSelectHtml(\App\ToDo::PRIORITY_ARR,$record->priority);
        if (config('global.device') != 'pc') {
            return view('mobile/todo/edit',compact('record','priority_html','status_html','member_html'));
        }else{
            return view('frontend/todo/edit',compact('record','priority_html','status_html','member_html'));
        }
    }
    public function update(Request $request,$id){
        $input = $request->all();
        if(isset($input['nolimit'])){
            $input['due_date'] = NULL;
        }else{
            $input['due_date'] = date('Y-m-d', strtotime($input['ldate_year'].'-'.$input['ldate_month'] .'-'.$input['ldate_day']));
        }
        $this->todoRepo->update($input,$id);
        if (config('global.device') != 'pc') {
            return  redirect()->route('frontend.todo.index')->with('success','Cập nhật thành công');
        }else{
            return  redirect()->route('frontend.todo.index')->with('error','Cập nhật thất bại');
        }
    }
    public function updateStatus(Request $request){
        $input = $request->all();
        foreach($input['id'] as $key=>$val){
            $input['status'] = 1;
            $input['com_date'] = date('Y-m-d');
            $this->todoRepo->update($input,$val);
        }
        if (config('global.device') != 'pc') {
            return  redirect()->route('frontend.todo.index')->with('success','Cập nhật thành công');
        }else{
            return  redirect()->route('frontend.todo.index')->with('error','Cập nhật thất bại');
        }
    }
    public function editCategory(){
        $records = $this->todocateRepo->getByMember(\Auth::guard('member')->user()->id);
        $titles = $records->pluck('title')->toArray();
        $category = implode("\r\n",$titles);
        if (config('global.device') != 'pc') {
            return view('mobile/todo/edit_category',compact('category'));
        }else{
            return view('frontend/todo/edit_category',compact('category'));
        }
    }
    public function updateCategory(Request $request){
        $input = $request->all();
        $categorys = explode("\r\n",$input['category']);
        $records = $this->todocateRepo->getByMember($input['member_id']);
        if(count($records) > 0){
            foreach($records as $key=>$record){
                if(!in_array($record->title,$categorys)){
                    $record->delete();
                }
            }
            $titles = $records->pluck('title')->toArray();
            foreach($categorys as $key=>$category){
                if(!in_array($category,$titles)){
                    $input['title'] = $category;
                    $this->todocateRepo->create($input);
                }
            }
        }else{
            foreach($categorys as $key=>$val){
                $input['title'] = $val;
                $this->todocateRepo->create($input);
            }
        }
        if (config('global.device') != 'pc') {
            return  redirect()->route('frontend.todo.index')->with('success','Cập nhật thành công');
        }else{
            return  redirect()->route('frontend.todo.index')->with('error','Cập nhật không thành công');
        }
    }
    public function destroy($id){
        $record = $this->todoRepo->find($id);
        $record->delete();
        if (config('global.device') != 'pc') {
            return  redirect()->route('frontend.todo.index')->with('success','Xóa thành công');
        }else{
            return  redirect()->route('frontend.todo.index')->with('error','Xóa không thành công');
        }
    }
    public function destroyMulti(Request $request){
        $input = $request->all();
        foreach($input['id'] as $key=>$val){
            $record = $this->todoRepo->find($val);
            $record->delete();
        }
        if (config('global.device') != 'pc') {
            return  redirect()->back()->with('success','Xóa thành công');
        }else{
            return  redirect()->back()->with('error','Xóa không thành công');
        }
    }
    public function destroyAll(){
        $this->todoRepo->deleteAll();
        if (config('global.device') != 'pc') {
            return  redirect()->back()->with('success','Xóa thành công');
        }else{
            return  redirect()->back()->with('error','Xóa không thành công');
        }
    }
    public function join($id){
        $this->membertodoRepo->updateRecord(\Auth::guard('member')->user()->id,$id,['join'=>1,'status'=>1]);
        return  redirect()->back()->with('success','Tham gia công việc thành công');
    }
    public function unjoin(Request $request){
        $this->membertodoRepo->updateRecord(\Auth::guard('member')->user()->id,$request->get('id'),['join'=>2,'reason'=>$request->reason]);
        return  redirect()->back()->with('success','Bỏ tham gia công việc thành công');
    }
    public function changeStatus(Request $request){
        $todo = $this->todoRepo->find($request->get('id'));
        $this->membertodoRepo->updateRecord(\Auth::guard('member')->user()->id,$request->get('id'),['status'=>$request->get('status')]);
        $membertodo = $this->membertodoRepo->getStatus($request->get('id'));
        if($membertodo->status > $todo->status){
            $this->todoRepo->update(['status'=>$membertodo->status],$request->get('id'));
        }
        return  redirect()->back()->with('success','Cập nhật trạng thái thành công');
    }
}
