<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PartController extends Controller
{
    public function index(){
        $records = \App\Part::all();
        $member_html = \App\Helpers\StringHelper::getSelectMemberOptions(\App\Member::where('is_deleted',0)->get()); 
        if (config('global.device') != 'pc') {
            return view('mobile/part/index', compact('records','member_html'));
        } else {
            return view('frontend/part/index', compact('records','member_html'));
        }
    }
    public function destroy($id) {
        $record = \App\Part::find($id);
        $record->delete();
        if ($record) {
            return redirect()->back()->with('success', 'Xóa thành công');
        } else {
            return redirect()->back()->with('error', 'Xóa thất bại');
        }
    }
}