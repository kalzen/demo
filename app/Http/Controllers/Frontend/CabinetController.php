<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\CabinetRepository;
use Repositories\CabinetHistoryRepository;
use File;
use ZipArchive;
use Repositories\FolderRepository;
use Repositories\DepartmentRepository;

class CabinetController extends Controller
{
   public function __construct(DepartmentRepository $departmentRepo,FolderRepository $folderRepo,CabinetRepository $cabinetRepo,CabinetHistoryRepository $historyRepo){
        $this->cabinetRepo = $cabinetRepo;
        $this->historyRepo = $historyRepo;
        $this->folderRepo = $folderRepo;
        $this->departmentRepo = $departmentRepo;
    }
    public function index(Request $request) {
        $records = $this->cabinetRepo->getIndex($request->all());
        $folders = $this->folderRepo->getAll();
        if (config('global.device') != 'pc') {
            return view('mobile/cabinet/index', compact('records','folders'));
        }else{
            return view('frontend/cabinet/index', compact('records','folders'));
        }
    }
    public function create(){
        $folders = $this->folderRepo->all();
        if (config('global.device') != 'pc') {
            return view('mobile/cabinet/create',compact('folders'));
        }else{
            return view('frontend/cabinet/create',compact('folders'));
        }
    }
    public function store(Request $request){
        $input = $request->all();
        foreach($input['file_name'] as $key=>$val){
            $record['link'] = $input['link'][$key];
            $record['file_name'] = $val;
            $record['size'] = $input['size'][$key];
            $record['memo'] = $input['memo'][$key];
            $record['subject'] = $input['subject'][$key];
            $record['update_time'] = date('Y-m-d H:i');
            $record['max_version'] = $input['max_version'][$key];
            $record['created_by'] = \Auth::guard('member')->user()->id;
            $record['updater'] = \Auth::guard('member')->user()->id;
            $cabinet = $this->cabinetRepo->create($record);
            $history['cabinet_id'] = $cabinet->id;
            $history['update_time'] = date('Y-m-d H:i');
            $history['file_name'] = $cabinet->file_name;
            $history['updater'] = $cabinet->created_by;
            $history['action'] = 'Create';
            $history['comment'] = '';
            $this->historyRepo->create($history);
        }
        if (config('global.device') != 'pc') {
            return  redirect()->route('frontend.cabinet.index')->with('success','Tạo mới thành công');
        }else{
            return  redirect()->route('frontend.cabinet.index')->with('error','Tạo mới thất bại');
        }
    }
    public function view($id){
        $record = $this->cabinetRepo->find($id);
        $historys= $this->historyRepo->getByCabinet($id);
        if (config('global.device') != 'pc') {
            return view('mobile/cabinet/view',compact('record','historys'));
        }else{
            return view('frontend/cabinet/view',compact('record','historys'));
        }
    }
    public function viewUpdate($id){
        $record = $this->cabinetRepo->find($id);
        if (config('global.device') != 'pc') {
            return view('mobile/cabinet/update',compact('record'));
        }else{
            return view('frontend/cabinet/update',compact('record'));
        }
    }
    public function viewEdit($id){
        $record = $this->cabinetRepo->find($id);
        if (config('global.device') != 'pc') {
            return view('mobile/cabinet/edit',compact('record'));
        }else{
            return view('frontend/cabinet/edit',compact('record'));
        }
    }
    public function update(Request $request,$id){
        $input = $request->all();
        $this->cabinetRepo->update($input,$id);
        if(isset($input['upload'])){
            $history['cabinet_id'] = $id;
            $history['update_time'] = date('Y-m-d H:i');
            $history['file_name'] = $input['file_name'];
            $history['updater'] = \Auth::guard('member')->user()->id;
            $history['action'] = 'Update';
            $history['comment'] = $input['comment'];
            $this->historyRepo->create($history);
        }
        return redirect()->route('frontend.cabinet.view',['id'=>$id])->with('success','Update thành công'); 
    }
    public function destroy($id){
        $res = $this->cabinetRepo->destroy($id);
        if ($res) {
            return redirect()->route('frontend.cabinet.index')->with('success', 'Xóa thành công');
        } else {
            return redirect()->route('frontend.cabinet.index')->with('error', 'Xóa thất bại');
        }
    }
    public function destroyMulti(Request $request){
        $res = $this->cabinetRepo->deleteMulti($request->get('id'));
        if ($res) {
            return redirect()->route('frontend.cabinet.index')->with('success', 'Xóa thành công');
        } else {
            return redirect()->route('frontend.cabinet.index')->with('error', 'Xóa thất bại');
        }
    }
    public function downloadMulti(Request $request){
        $zip = new ZipArchive;
        $file_names = \App\Cabinet::whereIn('id',$request->get('id'))->pluck('file_name')->toArray();
        $fileName = 'myfile.zip';
        if ($zip->open(public_path($fileName), ZipArchive::CREATE) === TRUE)
        {
            $files = File::files(public_path('file'));
            foreach ($files as $key => $value) {
                if(in_array(basename($value),$file_names)){
                    $relativeNameInZipFile = basename($value);
                    $zip->addFile($value, $relativeNameInZipFile);
                }
            }
            $zip->close();
        }
        return response()->download(public_path($fileName))->deleteFileAfterSend(true);;
    }
    public function createFolder(){
        $parent_html = \App\Helpers\StringHelper::getSelectFolderCabinet($this->folderRepo->getFolderParent());
        $departments = $this->departmentRepo->all();
        return view('frontend/cabinet/create_folder',compact('parent_html','departments'));
    }
    public function storeFolder(Request $request){
        $input = $request->all();
        $input['created_by'] = \Auth::guard('member')->user()->id;
        if($input['parent_id'] == 0){
            $input['path'] = $input['name'];
        }else{
            $folder = $this->folderRepo->find($input['parent_id']);
            $input['path'] = $folder->name.'/'.$input['name'];
        }
        $path = public_path().'/library/'.$input['path'];
        File::makeDirectory($path, $mode = 0777, true, true);
        $folder = $this->folderRepo->create($input);
        if($input['member_id'] != ''){
            $input['member_id'] = explode(':',$input['member_id']);
            $folder->member()->attach($input['member_id']);
        }
        if (config('global.device') != 'pc') {
            return  redirect()->route('frontend.cabinet.index')->with('success','Tạo mới thành công');
        }else{
            return  redirect()->route('frontend.cabinet.index')->with('error','Tạo mới thất bại');
        }
    }
            
}
