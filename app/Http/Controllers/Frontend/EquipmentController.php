<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EquipmentController extends Controller
{
    public function index(){
        $records = \App\Equipment::all();
        if (config('global.device') != 'pc') {
            return view('mobile/equipment/index', compact('records'));
        } else {
            return view('frontend/equipment/index', compact('records'));
        }
    }
    public function destroy($id) {
        $record = \App\Equipment::find($id);
        $record->delete();
        if ($record) {
            return redirect()->back()->with('success', 'Xóa thành công');
        } else {
            return redirect()->back()->with('error', 'Xóa thất bại');
        }
    }
}
