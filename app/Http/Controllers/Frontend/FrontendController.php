<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\Repositories\SlideRepository;
use App\Repositories\MemberRepository;
use Repositories\ToDoRepository;
use Repositories\DepartmentRepository;
use Repositories\EquipmentRepository;
use Repositories\NotificationScheduleRepository;

class FrontendController extends Controller {

    public function __construct(NotificationScheduleRepository $notificationSchRepo,EquipmentRepository $equipmentRepo,DepartmentRepository $departmentRepo,ToDoRepository $todoRepo,SlideRepository $slideRepo,MemberRepository $memberRepo) {
        $this->slideRepo = $slideRepo;
        $this->memberRepo = $memberRepo;
        $this->todoRepo = $todoRepo;
        $this->departmentRepo=$departmentRepo;
        $this->equipmentRepo = $equipmentRepo;
        $this->notificationSchRepo=$notificationSchRepo;
    }
    public function index() {
        $slides = $this->slideRepo->all();
        $documents = \App\Document::where('status',1)->orderBy('created_at','desc')->take(3)->get();
        $project_month = DB::table('project')
                   ->whereMonth('created_at',date('m'));
        $project_year = DB::table('project')
                   ->whereYear('created_at',date('Y'));
        $rank_month = DB::table('member')
            ->joinSub($project_month, 'project_month', function ($join) {
                $join->on('member.id', '=', 'project_month.member_id');
            })->select('member.id',DB::raw('count(project_month.id) as count'))->groupBy('member.id')->orderBy('count','DESC')->take(3)->get();
        $rank_year = DB::table('member')
            ->joinSub($project_year, 'project_year', function ($join) {
                $join->on('member.id', '=', 'project_year.member_id');
            })->select('member.id',DB::raw('count(project_year.id) as count'))->groupBy('member.id')->orderBy('count','DESC')->take(3)->get();    
        if (config('global.device') != 'pc') {
            return view('mobile/home/index',compact('rank_month','rank_year','slides','documents'));
        } else {
            return view('frontend/home/index',compact('rank_month','rank_year','slides','documents'));
        }
    }
    public function home(){
        return view('frontend/home/home');
    }
    public function create(){
        return view('frontend/home/create');
    }
    public function view(Request $request){
        if($request->get('bdate')){
            $date_now = $request->get('bdate');
        }else{
            $date_now = date('Y-m-d');
        }
        $members = $this->memberRepo->getByDepartment(\Auth::guard('member')->user()->department_id,\Auth::guard('member')->user()->id);
        $html ='<tr><td class="s_domain_week"><span class="domain">(UTC+07:00) VietNam</span></td>';
        for($i = 0; $i < 7; $i++) {
             $date = date('Y-m-d', strtotime(" +" . $i . " days",strtotime($date_now)));
             if($i == 0){
                $html .='<td class="s_date_'.strtolower(date('l',strtotime($date))).'_week" align="center">&nbsp;<a href="/schedule/group_day?bdate='.$date.'">'.date('l, d M',strtotime($date)).'</a><input type="hidden" id="day_start" value="'.date('Y-m-d',strtotime($date)).'" /></td>';
             }else{
                $html .='<td class="s_date_'.strtolower(date('l',strtotime($date))).'_week" align="center">&nbsp;<a href="/schedule/group_day?bdate='.$date.'">'.date('l, d M',strtotime($date)).'</a></td>'; 
             }
        }
        if(is_null(\Auth::guard('member')->user()->avatar)){
            $avatar = '<div class="profileImageUser-grn"></div>';
        }else{
            $avatar = '<div class="user_photo_grn" style="background-image: url('.\Auth::guard('member')->user()->avatar.');" aria-label=""></div>';
        }
        $html .='</tr><tr class="js_customization_schedule_user_id_'.\Auth::guard('member')->user()->id.'">
                    <td valign="top" class="calendar_rb_week userBox">
                        <div class="userElement profileImageBase-grn profileImageBaseSchedule-grn">
                           <dl>
                              <dt>
                                 <a >
                                    <div class="profileImage-grn">
                                       <div class="profileImageFrame-grn">
                                          '.$avatar.'
                                       </div>
                                    </div>
                                 </a>
                              </dt>
                              <dd><a >'.\Auth::guard('member')->user()->full_name.'</a></dd>
                           </dl>
                           <div class="clear_both_0px"></div>
                        </div>
                        <div class="shortcut_box_full"><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_day?bdate='.date('Y-m-d').'"><img src="/img/cal_pday20.gif" border="0" alt="Day" title="Day" class="small_link">Day</a></span><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_week?bdate='.date('Y-m-d').'"><img src="/img/cal_pweek20.gif" border="0" alt="Week" title="Week" class="small_link">Week</a></span><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_month?bdate='.date('Y-m-d').'"><img src="/img/cal_pmon20.gif" border="0" alt="Month" title="Month" class="small_link">Month</a></span></div>
                        <div class="shortcut_box_short" style="display:none"><span class="nowrap-grn schedule_userbox_item_grn"><a href="/schedule/personal_day?bdate='.date('Y-m-d').'"><img src="/img/cal_pday20.gif" border="0" alt="Day" title="Day"></a></span><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_week?bdate='.date('Y-m-d').'"><img src="/img/cal_pweek20.gif" border="0" alt="Week" title="Week" class="small_link"></a></span><span class="nowrap-grn schedule_userbox_item_grn"><a href="/schedule/personal_month?bdate='.date('Y-m-d').'"><img src="/img/cal_pmon20.gif" border="0" alt="Month" title="Month"></a></span></div>
                     </td>';
        for($i = 0; $i < 7; $i++) {
            $date = date('Y-m-d', strtotime(" +" . $i . " days",strtotime($date_now)));
            $schedules = \App\Schedule::join('member_schedule', 'member_schedule.schedule_id', '=', 'schedule.id')->where('member_schedule.member_id', \Auth::guard('member')->user()->id)
                            ->whereDate('schedule.start_date', '<=', $date)->whereDate('schedule.end_date', '>=', $date)->whereNotIn('pattern',['2','4'])->orderByRaw('TIME_FORMAT(schedule.start_date, "%H%i")','ASC')->get();
            $todos = $this->todoRepo->getListByDate(\Auth::guard('member')->user()->id, $date);
            $html .= '<td valign="top" class="s_user_week normalEvent" rel="/schedule/simple_add?bdate=2020-12-31&amp;uid=58&amp;gid=15&amp;referer_key=8b883bbc87280ec72b9d3f7195804ef0">
                        <div class="addEvent">
                           <a title="Add" href="/schedule/create?bdate='.$date.'&amp;uid='.\Auth::guard('member')->user()->id.'&amp;gid=15&amp;referer_key=8b883bbc87280ec72b9d3f7195804ef0">
                              <div class="iconWrite-grn"></div>
                           </a>
                        </div>
                        <div class="js_customization_schedule_date_'.$date.'"></div>
                        <div class="groupWeekInfo"></div>';
            foreach ($todos as $todo) {
                    $html .= '<div class="schedule_todo normalEventElement">
                                <img src="/assets/mobile/img/todoPersonalInSchedule16.png" border="0" alt=""><a href="' . route('frontend.todo.view', $todo->id) . '">' . $todo->title . '</a>
                           </div>';
            }
            foreach($schedules as $schedule){
             $menu = !is_null($schedule->menu) ? '<span class="event_color'.explode(';#',$schedule->menu)[1].'_grn">'.explode(';#',$schedule->menu)[0].'</span>' : '';
             if($schedule->pattern == 3){
                if($schedule->type_repeat == 'week' || $schedule->type_repeat == 'weekday'){
                    $repeat = '<img src="/img/repeat16.gif" border="0" alt=""><img src="/img/cal_pweek20.gif" border="0" alt="Week" title="Week" class="small_link">';
                }elseif($schedule->type_repeat == 'day'){
                    $repeat = '<img src="/img/repeat16.gif" border="0" alt=""><img src="/img/cal_pday20.gif" border="0" alt="Day" title="Day" class="small_link">';
                }else{
                    $repeat = '<img src="/img/repeat16.gif" border="0" alt=""><img src="/img/cal_pmon20.gif" border="0" alt="Month" title="Month" class="small_link">';
                }
             }else{
                $repeat = '';
             }
             $equipment_string = count($schedule->equipment) > 0 ? '<p>['.implode(',',$schedule->equipment()->pluck('name')->toArray()).']</p>' : '';
             $html .= '<div class="share normalEventElement   group_week_calendar_item">
                           <div class="listTime"><a href="'.route('frontend.schedule.view',$schedule->id).'">' .($schedule->none_time != 1 ? date('h:i A', strtotime($schedule->start_date)) . '-' . date('h:i A', strtotime($schedule->end_date)) : '' ). '</a></div>
                           <div class="'.($schedule->none_time != 1 ? 'groupWeekEventTitle' : 'groupWeekEventTitleAllday').'">
                              <a href="'.route('frontend.schedule.view',$schedule->id).'">'.$menu.' '.$schedule->title.' '.$equipment_string.' '.$repeat.'</a>
                           </div>
                        </div>';
            }
        }
        $html .='</td></tr>';
        $schedules_all = \App\Schedule::join('member_schedule', 'member_schedule.schedule_id', '=', 'schedule.id')->where('member_schedule.member_id', \Auth::guard('member')->user()->id)
                            ->whereDate('schedule.end_date', '>=', $date_now)->whereDate('schedule.start_date', '<=', date('Y-m-d', strtotime(" +6 days", strtotime($date_now))))->whereIn('pattern',['2','4'])->orderByRaw('TIME_FORMAT(schedule.start_date, "%H%i")','ASC')->get();
            foreach ($schedules_all as $key => $val) {
                $end = new \DateTime(date('Y-m-d', strtotime($val->end_date)));
                $start = new \DateTime(date('Y-m-d', strtotime($val->start_date)));
                $date = new \DateTime($date_now);
                $first = $date->diff($start);
                if ($first->invert == 1) {
                    $first = 0;
                    $first_html = '';
                    $middle = $date->diff($end)->format('%d') + 1;
                } else {
                    $first = $first->days;
                    if ($first == 0) {
                        $first_html = '';
                    } else {
                        $first_html = '<td class="br_banner" colspan="' . $first . '"><br></td>';
                    }
                    $middle = $start->diff($end)->format('%d') + 1;
                }
                if (($first + $middle) >= 7) {
                    $last = 0;
                    $last_html = '';
                    if($first == 0){
                        $middle = 7;
                    }else{
                        $middle = 7 - $first;
                    }
                } else {
                    $last = 7 - ($first + $middle);
                    $last_html = '<td class="br_banner" colspan="' . $last . '"><br></td>';
                }
                $menu = is_null($val->menu) ? '' : '<span class="event_color' . explode(';#', $val->menu)[1] . '_grn">' . explode(';#', $val->menu)[0] . '</span>';
                if($val->pattern == 2){
                    $html .= '<tr>
                                <td style="border-right:1px solid #C9C9C9"><br></td>
                                ' . $first_html . '
                                <td class="s_banner normalEvent" colspan="' . $middle . '">
                                <div class="normalEventElement"><a href="' . route('frontend.schedule.view', $val->id) . '"><img src="' . asset('/img/banner16.gif') . '" border="0" alt="">' . $menu . ' ' . $val->title . '</a></div></td>
                                ' . $last_html . '
                              </tr>';
                }else{
                    $html .= '<tr>
                                <td style="border-right:1px solid #C9C9C9"><br></td>
                                ' . $first_html . '
                                <td class="s_task normalEvent" colspan="' . $middle . '">
                                <div><a href="' . route('frontend.schedule.view', $val->id) . '"><img src="' . asset('/img/banner16.gif') . '" border="0" alt="">' . $menu . ' ' . $val->title . ' ('.$val->percent.'%)</a></div></td>
                                ' . $last_html . '
                              </tr>';
                }
        }
        $departments = $this->departmentRepo->all();
        $equipments = $this->equipmentRepo->all();
        $equipment =[];
        $department = [];
        foreach($departments as $key=>$val){
            $object2 = new \stdClass();
            $object2->id = $val->id;
            $object2->name = $val->name;
            $object2->is_selected = "";
            $object2->extra_param = "";
            $object2->type = "membership";
            $object2= json_encode($object2);
            $department[]=$object2;
        }
        foreach($equipments as $key=>$val){
            $object2 = new \stdClass();
            $object2->oid = $val->id;
            $object2->name = $val->name;
            $object2->is_selected = "";
            $object2->extra_param = "";
            $object2->count = "0";
            $object2->children = [];
            $object2= json_encode($object2);
            $equipment[]=$object2;
        }
        $department = implode(',',$department);
        $equipment = implode(',',$equipment);
        $todos = $this->todoRepo->getIndex($request);
        $notifications = \App\NotificationSchedule::where('to',\Auth::guard('member')->user()->id)->where('read_at',NULL)->orderBy('created_at','DESC')->get();
        if (config('global.device') != 'pc') {
            return view('mobile/home/view',compact('html','todos','department','equipment','notifications'));
        } else {
            return view('frontend/home/view',compact('html','todos','department','equipment','notifications'));
        }
    }
    public function changeLanguage($locale){
        if (in_array($locale, \Config::get('app.locales'))) {
          session(['locale' => $locale]);
        }
        return redirect()->back();
    }
    public function scheme(){
        if(\Auth::guard('member')->user()->level == 1){
            return Redirect::route('frontend.project.index');
        }
        if (config('global.device') != 'pc') {
            return view('mobile/home/view');
        } else {
            return view('frontend/home/old_view');
        }
    }

}
