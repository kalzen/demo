<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PositionController extends Controller
{
    public function index(){
        $records = \App\Position::all();
        if (config('global.device') != 'pc') {
            return view('mobile/position/index', compact('records'));
        } else {
            return view('frontend/position/index', compact('records'));
        }
    }
    public function destroy($id) {
        $record = \App\Position::find($id);
        $record->delete();
        if ($record) {
            return redirect()->back()->with('success', 'Xóa thành công');
        } else {
            return redirect()->back()->with('error', 'Xóa thất bại');
        }
    }
}
