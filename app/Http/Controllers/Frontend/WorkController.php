<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WorkController extends Controller
{
     public function index(){
        $records = \App\Work::all();
        if (config('global.device') != 'pc') {
            return view('mobile/work/index', compact('records'));
        } else {
            return view('frontend/work/index', compact('records'));
        }
    }
    public function destroy($id) {
        $record = \App\Work::find($id);
        $record->delete();
        if ($record) {
            return redirect()->back()->with('success', 'Xóa thành công');
        } else {
            return redirect()->back()->with('error', 'Xóa thất bại');
        }
    }
}
