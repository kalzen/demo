<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GroupsController extends Controller
{
    public function index(){
        $records = \App\Groups::all();
        if (config('global.device') != 'pc') {
            return view('mobile/groups/index', compact('records'));
        } else {
            return view('frontend/groups/index', compact('records'));
        }
    }
    public function destroy($id) {
        $record = \App\Groups::find($id);
        $record->delete();
        if ($record) {
            return redirect()->back()->with('success', 'Xóa thành công');
        } else {
            return redirect()->back()->with('error', 'Xóa thất bại');
        }
    }
}
