<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeamController extends Controller
{
    public function index(){
        $records = \App\Team::all();
        $member_html = \App\Helpers\StringHelper::getSelectMemberOptions(\App\Member::where('is_deleted',0)->get()); 
        if (config('global.device') != 'pc') {
            return view('mobile/team/index', compact('records','member_html'));
        } else {
            return view('frontend/team/index', compact('records','member_html'));
        }
    }
    public function destroy($id) {
        $record = \App\Team::find($id);
        $record->delete();
        if ($record) {
            return redirect()->back()->with('success', 'Xóa thành công');
        } else {
            return redirect()->back()->with('error', 'Xóa thất bại');
        }
    }
}
