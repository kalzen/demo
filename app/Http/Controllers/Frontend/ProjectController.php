<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\ProjectRepository;
use App\Repositories\MemberRepository;
use Illuminate\Support\Facades\Session;
use Repositories\LevelRepository;
use Carbon\Carbon;

class ProjectController extends Controller {

    public function __construct(MemberRepository $memberRepo,ProjectRepository $projectRepo,LevelRepository $levelRepo) {
        $this->projectRepo = $projectRepo;
        $this->memberRepo = $memberRepo;
        $this->levelRepo= $levelRepo;
    }

    public function index(){
        Session::put('p_page',1);
        if(isset($_GET['keyword'])){
            $records = $this->projectRepo->fillter($_GET['keyword'],15);
            $keyword = $_GET['keyword'];
        }else{
            $records = $this->projectRepo->getIndex(15);
            $keyword='';
        }
        if (config('global.device') != 'pc') {
            return view('mobile/project/index', compact('records','keyword'));
        } else {
            return view('frontend/project/index', compact('records','keyword'));
        }
    }
    public function listProject(Request $request){
        Session::put('list_page',1);
        $records = $this->projectRepo->getIndexByMember(10,$request);
        $department_html = \App\Helpers\StringHelper::getSelectHtml(\App\Department::get());
        $level_html = '<option></option><option value="0">Không cấp độ</option>';
        $level_html .= \App\Helpers\StringHelper::getSelectHtml(\App\Level::get());
        if (config('global.device') != 'pc') {
            return view('mobile/project/list', compact('records','department_html','level_html'));
        } else {
            return view('frontend/project/list', compact('records','department_html','level_html'));
        }
    }
    public function create() {
        $members = \App\Member::get();
        foreach($members as $key=>$member){
            $member->name = $member->login_id;
        }
        $member_html = \App\Helpers\StringHelper::getSelectOptions($members);
        $level_html = \App\Helpers\StringHelper::getSelectOptions($this->levelRepo->all());
        if (config('global.device') != 'pc') {
            return view('mobile/project/create', compact('member_html','level_html'));
        } else {
            return view('frontend/project/create', compact('member_html','level_html'));
        }
    }
    public function store(Request $request) {
        $input = $request->all();
        if(isset($input['draft'])){
            $input['status'] = 0;
        }else{
            $input['status'] = \Auth::guard('member')->user()->level;
        }
        if(!isset($input['member_id'])){
            $input['member_id']= \Auth::guard('member')->user()->id;
        }
        $project = $this->projectRepo->create($input);
        if($project->status > 0 && $project->type != \App\Project::TYPE_TEAM){
            $this->memberRepo->Notificate(\Auth::guard('member')->user()->level + 1, ['content' => 'Có đề án mới', 'link' => '/project/view/' . $project->id]);
        }
        if ($project->id) {
            return redirect()->route('frontend.project.index')->with('success', 'Tạo mới thành công');
        } else {
            return redirect()->route('frontend.project.index')->with('error', 'Tạo mới thất bại');
        }
    }
    public function edit($id) {
        $record = $this->projectRepo->find($id);
        $level_html = \App\Helpers\StringHelper::getSelectOptions($this->levelRepo->all(),$record->level);
        if (config('global.device') != 'pc') {
            return view('mobile/project/update', compact('record','level_html'));
        } else {
            return view('frontend/project/update', compact('record','level_html'));
        }
    }
    public function update(Request $request, $id) {
        $input = $request->all();
        $input['status'] = \Auth::guard('member')->user()->level;
        $res = $this->projectRepo->update($input, $id);
        if ($res) {
            return redirect()->route('frontend.project.index')->with('success', 'Cập nhật thành công');
        } else {
            return redirect()->route('frontend.project.index')->with('error', 'Cập nhật thất bại');
        }
    }
    public function fillter($keyword){
        Session::put('p_page',1);
        $records = $this->projectRepo->fillter($keyword,10);
        return view('frontend/project/fillter',compact('records'));
    }
    public function view($id){
        $record = $this->projectRepo->find($id);
        $number_of_reviews = \App\Project::STATUS_ACTIVE - $record->member->level;
        for($i=1;$i < $number_of_reviews + 1;$i++){
            $level =  $record->member->level + $i;
            $logapproved[$i] = \App\LogApproved::where('project_id',$record->id)->where('level',$level)->first();
            if($logapproved[$i]){
                $logapproved[$i]->count = \App\LogApproved::where('project_id',$record->id)->where('level',$level)->count();
            }
        }
        if(!isset($logapproved)){
            $logapproved = null;
        }
        $level_html = \App\Helpers\StringHelper::getSelectOptions($this->levelRepo->all(),$record->level);
        if (config('global.device') != 'pc') {
            return view('mobile/project/view', compact('record','logapproved','level_html'));
        } else {
            return view('frontend/project/view', compact('record','logapproved','level_html'));
        }
    }
    public function chart(){
        $label1 = [];
        $data1=[];
        $label2 = [];
        $data2=[];
        for($i=6;$i--;$i>0){
            $dt = Carbon::now();
            $dt->subMonth($i);
            $month = $dt->month;
            $year = $dt->year;
            $label1[] = "Tháng ".$dt->month;
            $data1[] = \App\Project::whereMonth('created_at',$month)->whereYear('created_at',$year)->where('is_destroy',0)->count();
        }
        $status_pending = \App\Project::where('status','<',\App\Project::STATUS_ACTIVE)->where('is_destroy',0)->count();
        $status_approved = \App\Project::where('status','=',\App\Project::STATUS_ACTIVE)->where('is_destroy',0)->count();
        $status_return = \App\Project::where('status','=',\App\Project::STATUS_CANCEL)->where('is_destroy',0)->count();
        for($j=0;$j<11;$j++){
            if($j == 0){
                $label2[] = 'Không cấp độ';
                $data2[]= \App\Project::where('level',null)->where('is_destroy',0)->count();
            }else{
                $label2[] = 'Cấp '.$j;
                $data2[]=\App\Project::where('level',$j)->where('is_destroy',0)->count();
            }
        }
        if (config('global.device') != 'pc') {
            return view('mobile/project/chart');
        }else{
            return view('frontend/project/chart',compact('label1','data1','status_pending','status_approved','status_return','label2','data2'));
        }
    }
}
