<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Frontend {
    public function handle($request, Closure $next){
        $config = \DB::table('config')->first();
        \View::share(['share_config' => $config]);
        $rank_member = DB::table('member')->join('project', 'project.member_id', '=', 'member.id')
                       ->select('member.id',DB::raw('count(project.id) as count'))->groupBy('member.id')->orderBy('count','DESC')->take(3)->get();
        $a = Carbon::now();
        $lastQuarter = $a->quarter;
        $project_quarter = DB::table('project')
                   ->where(DB::raw('QUARTER(created_at)'), $lastQuarter);
        $team_month = DB::table('project')
                   ->whereMonth('created_at',date('m'));
        $rank_quarter = DB::table('member')
            ->joinSub($project_quarter, 'project_quarter', function ($join) {
                $join->on('member.id', '=', 'project_quarter.member_id');
            })->select('member.id',DB::raw('count(project_quarter.id) as count'))->groupBy('member.id')->orderBy('count','DESC')->take(3)->get();
        $rank_team = DB::table('member')->joinSub($team_month, 'team_month', function ($join) {
                    $join->on('member.id', '=', 'team_month.member_id');
                })->join('team','member.team_id','=','team.id')
                ->select('team.name',DB::raw('count(team_month.id) as count'))
                ->groupBy('team.name')
                ->orderBy('count','DESC')->take(3)->get();  
        \View::share(['rank_quarter' => $rank_quarter]);
        \View::share(['rank_team' => $rank_team]);
        //$collection = collect(Member::join('project',)orderBy('wins', 'DESC')->get());
        if (!is_null(Auth::guard('member')->user())){
            $members= DB::table('member')->join('project', 'project.member_id', '=', 'member.id')
                ->select('member.id', DB::raw('count(project.id) as count'))
                ->groupBy('member.id')->orderBy('count','DESC')->get();
            foreach($members as $key=>$val){
                if(\Auth::guard('member')->user()->id == $val->id){
                    $position = $key + 1;
                }
            }
            $members_team= DB::table('member')->join('project', 'project.member_id', '=', 'member.id')
                    ->select('member.id','member.team_id',DB::raw('count(project.id) as count'))->where('member.team_id',\Auth::guard('member')->user()->team_id)
                    ->groupBy('member.id','member.team_id')->orderBy('count','DESC')->get();
            foreach($members_team as $k=>$value){
                if(\Auth::guard('member')->user()->id == $value->id){
                    $position_team = $k + 1;
                }
            }
            \View::share(['position_rank' => isset($position) ? $position : 0]);
            \View::share(['position_team_rank' => isset($position_team) ? $position_team : 0]);
            $count_message = \DB::table('messages')->where('is_read', '=', 0)->where('to', '=', \Auth::guard('member')->user()->id)->count();
            \View::share(['count_message' => $count_message]);
            $documents = \App\Document::where('status',1)->orderBy('created_at','desc')->take(3)->get();
            \View::share(['documents' => $documents]);
            return $next($request);
        } else {
            return redirect()->route('home.index');
        }
    }
    
}
