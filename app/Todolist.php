<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todolist extends Model
{
    protected $table='todolist';
    protected $fillable=['name','status','priority','member_id'];
    const priority_arr = [['id'=>1,'name'=>'Làm kế'],['id'=>2,'name'=>'Làm liền'],['id'=>3,'name'=>'Làm sau'],['id'=>4,'name'=>'Làm cuối']];
    public function priority(){
        foreach($this::priority_arr as $key=>$val){
            if($this->priority == $val['id']){
                return $val['name'];
            }
        }
    }
    public function created_at() {
        return date( "d/m/Y", strtotime($this->created_at));
    }
}
