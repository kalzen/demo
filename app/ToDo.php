<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ToDo extends Model
{
    protected $table='todo';
    protected $fillable=['title','start_date','end_date','status','note','priority','created_by','complete_date'];
    const STATUS_COMPLETE = 1;
    const STATUS_UNCOMPLETE = 0;
    const STATUS_ARR = [['id'=>1,'name'=>'Đã giao việc'],['id'=>2,'name'=>'Đã nhận việc'],['id'=>3,'name'=>'Đang làm'],['id'=>4,'name'=>'Hoàn thành']];
    const PRIORITY_ARR = [['id'=>1,'name'=>'Khẩn cấp'],['id'=>2,'name'=>'Cao'],['id'=>3,'name'=>'Bình thường'],['id'=>4,'name'=>'Thấp']];
    public function createdby(){
        return $this->hasMany('\App\Member','created_by');
    }
    public function nameStatus(){
        foreach($this::STATUS_ARR as $key=>$val){
            if($val['id'] == $this->status){
                return $val['name'];
            }
        }
    }
    public function start_date(){
        return date('d/m/Y',strtotime($this->start_date));
    }
    public function end_date(){
        return date('d/m/Y',strtotime($this->end_date));
    }
    public function namePriority(){
         foreach($this::PRIORITY_ARR as $key=>$val){
            if($val['id'] == $this->status){
                return $val['name'];
            }
        }
    }
    public function created_at(){
        return date('d/m/Y',strtotime($this->created_at));
    }
    public function category(){
        return $this->belongsTo('App\ToDoCategory','category_id');
    }
    public function member(){
         return $this->belongsToMany('\App\Member', 'member_todo', 'todo_id', 'member_id')->withPivot('status', 'reason', 'join');;
    }
}
