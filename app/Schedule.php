<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $table='schedule';
    protected $fillable=['start_date','end_date','title','memo','uid','pattern','event_id','event','bdate','update_person','private','menu','type_repeat','company_name','physical_address','company_telephone_number','percent','wday','start_repeat','end_repeat','none_time'];
    const Weekend_arr = ['0'=>'Sunday','1'=>'Monday','2'=>'Tuesday','3'=>'Wednesday','4'=>'Thursday','5'=>'Friday','6'=>'Saturday'];
    public function member() {
        return $this->belongsToMany('\App\Member', 'member_schedule', 'schedule_id', 'member_id');
    }
    public function seen() {
        return $this->belongsToMany('\App\Member', 'seen_schedule', 'schedule_id', 'member_id');
    }
    public function equipment() {
        return $this->belongsToMany('\App\Equipment', 'equipment_schedule', 'schedule_id', 'equipment_id');
    }
    public function created_by(){
        return $this->belongsTo('\App\Member','uid','id');
    }
    public function updater(){
        return $this->belongsTo('\App\Member','update_person','id');
    }
    public function file(){
        return $this->hasMany('\App\File');
    }
    public function checkConflict($member_id){
        $end_date = $this->end_date;
        $start_date = $this->start_date;
        $scheduele_ids = \App\MemberSchedule::where('member_id',$member_id)->pluck('schedule_id')->toArray();
        $check = $this::where('id','<>',$this->id)->whereIn('id',$scheduele_ids)->whereIn('pattern',['1','3'])->where('start_date','<',$end_date)->where('end_date', '>', $start_date)->get();
        if(count($check) > 0){
            return true;
        }else{
            return false;
        }
    }
}
