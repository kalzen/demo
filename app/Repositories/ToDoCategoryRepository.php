<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;

class ToDoCategoryRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\ToDoCategory';
    }
    public function getByMember($member_id){
        return $this->model->where('member_id',$member_id)->get();
    }
}
