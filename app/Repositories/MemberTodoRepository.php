<?php

namespace App\Repositories;

use Repositories\Support\AbstractRepository;

class MemberTodoRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model(){
        return 'App\MemberTodo';
    }
    public function getRecord($member_id,$todo_id){
        return $this->model->where('member_id',$member_id)->where('todo_id',$todo_id)->first();
    }
    public function updateRecord($member_id,$todo_id,$data){
        return $this->model->where('member_id',$member_id)->where('todo_id',$todo_id)->update($data);
    }
    public function getStatus($todo_id){
        return $this->model->where('todo_id',$todo_id)->where('join',1)->orderBy('status','ASC')->first();
    }
}
