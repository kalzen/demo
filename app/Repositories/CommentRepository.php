<?php

namespace App\Repositories;

use Repositories\Support\AbstractRepository;

class CommentRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Comment';
    }
    public function getByToDo($todo_id){
        return $this->model->where('todo_id',$todo_id)->orderBy('created_at','DESC')->get();
    }
}
