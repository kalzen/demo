<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;

class CabinetRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }
    public function getIndex($search){
        $query = $this->model;
        if(isset($search['folder_id'])){
            $query = $query->where('folder_id',$search['folder_id']);
        }
        if(isset($search['is_deleted'])){
            $query = $query->where('is_deleted',$search['is_deleted']);
        }else{
            $query = $query->where('is_deleted',0);
        }
        if(isset($search['sort'])){
            $query = $query->orderBy($search['sort'],$search['sortby']);
        }
        return $query->get();
    }
    public function model(){
        return 'App\Cabinet';
    }
    public function deleteMulti($ids){
        return $this->model->whereIn('id',$ids)->update(['is_deleted'=>1]);
    }
    public function destroy($id){
        return $this->model->where('id',$id)->update(['is_deleted'=>1]);
    }
}

