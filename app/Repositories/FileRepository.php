<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;

class FileRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model(){
        return 'App\File';
    }
    public function updateBySchedule($ids,$schedule_id){
        return $this->model->whereIn('id',$ids)->update(['schedule_id'=>$schedule_id]);
    }
}
