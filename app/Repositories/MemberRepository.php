<?php
namespace App\Repositories;
use App\Notifications\Notificate;
use Pusher\Pusher;


use Repositories\Support\AbstractRepository;
use Illuminate\Support\Facades\Session;

class MemberRepository extends AbstractRepository
{
    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Member';
    }
    public function validateCreate(){
        return $rules = [
            'login_id' => 'required|unique:member',
            'password' => 'required',
        ];
    }

    public function validateUpdate($id){
        return $rules = [
            'login_id' => 'required|unique:member,login_id,'.$id.',id',
        ];
    }
    public function getAll(){
        $data = $this->model->where('is_deleted',0)->orderBy('created_at','DESC')->get();
        return $data;
        
    }
    public function getAllRemove(){
        $data = $this->model->where('is_deleted',1)->orderBy('created_at','DESC')->get();
        return $data;
        
    }
    public function getIndex($limit){
        $start = (Session::get('page')-1) * $limit;
        $query = $this->model->where('is_deleted',0);
        if(isset($_GET['keywords'])){
            $query = $query->where('full_name','like','%'.$_GET['keywords'].'%');
        }
        if(Session::get('search')){
            $search = Session::get('search');
            if(isset($search['part_id'])){
                $query = $query->where('part_id',$search['part_id']);
            }
            if(isset($search['department_id'])){
                $query = $query->where('department_id',$search['department_id']);
            }
            if(isset($search['groups_id'])){
                $query = $query->where('groups_id',$search['groups_id']);
            }
            if(isset($search['team_id'])){
                $query = $query->where('team_id',$search['team_id']);
            }
        }
        Session::put('_count',count($query->get()));
        $data = $query->orderBy('created_at','DESC')->offset($start)->limit($limit)->get();
        Session::put('is_deleted',0);
        if((Session::get('page') * $limit) > Session::get('_count')){
            Session::put('_pages',Session::get('_count'));
        }else{
            Session::put('_pages',Session::get('page') * $limit);
        }
        return $data;
        
    }
    public function getIndexRemove($limit){
        $start = (Session::get('page')-1) * $limit;
        $data = $this->model->where('is_deleted',1)->orderBy('created_at','DESC')->offset($start)->limit($limit)->get();
        Session::put('_count',count($this->model->where('is_deleted',1)->get()));
        Session::put('_pageSize',$limit);
        Session::put('is_deleted',1);
        if((Session::get('page') * $limit) > Session::get('_count')){
            Session::put('_pages',Session::get('_count'));
        }else{
            Session::put('_pages',Session::get('page') * $limit);
        }
        return $data;
        
    }
    public function remove($member_id){
        $members = $this->model->whereIn('id',$member_id)->get();
        foreach($members as $key=>$member){
            $member->project()->update(['is_destroy'=>1]);
        }
        return $this->model->whereIn('id',$member_id)->update(['is_deleted'=>1]);
        
    }
    public function restore($member_id){
        $members = $this->model->whereIn('id',$member_id)->get();
        foreach($members as $key=>$member){
            $member->project()->update(['is_destroy'=>0]);
        }
        return $this->model->whereIn('id',$member_id)->update(['is_deleted'=>0]);
    }
    public function search($search){
        Session::put('search',$search);
        $query = $this->model;
        if(isset($search['part_id'])){
            $query = $query->where('part_id',$search['part_id']);
        }
        if(isset($search['department_id'])){
            $query = $query->where('department_id',$search['department_id']);
        }
        if(isset($search['groups_id'])){
            $query = $query->where('groups_id',$search['groups_id']);
        }
        if(isset($search['team_id'])){
            $query = $query->where('team_id',$search['team_id']);
        }
        $start = (Session::get('page')-1) * 10;
        Session::put('_count',count($query->where('is_deleted',Session::get('is_deleted'))->get()));
        Session::put('_pageSize',10);
        if((Session::get('page') * 10) > Session::get('_count')){
            Session::put('_pages',Session::get('_count'));
        }else{
            Session::put('_pages',Session::get('page') * 10);
        }
        $data = $query->where('is_deleted',Session::get('is_deleted'))->orderBy('created_at','DESC')->offset($start)->limit(10)->get();
        return $data;
    }
    public function checkactivation($key) {
        return $this->model->where('activation', $key)->first();
    }
    public function export($ids){
        return $this->model->whereIn('id',$ids)->get();
    }
    public function Notificate($level_id, $data) {
        $members = \App\Member::where('level', $level_id)->where('department_id',\Auth::guard('member')->user()->department_id)->get();
        $data['time'] = date("H:i");
        $data['full_name'] = \Auth::guard('member')->user()->full_name;
        foreach ($members as $member) {
            $member->notify(new Notificate($data));
            $options = array(
                'cluster' => env('PUSHER_APP_CLUSTER'),
                'encrypted' => true
            );
            $pusher = new Pusher(
                    env('PUSHER_APP_KEY') ,
                    env('PUSHER_APP_SECRET'),
                    env('PUSHER_APP_ID'), $options
            );
            $notification = $member->unreadNotifications()->orderBy('created_at', 'DESC')->first();
            $data['count'] = count($member->unreadNotifications);
            $data['member_id'] = $member->id;
            $data['id'] = $notification->id;
            $pusher->trigger('NotificationEvent', 'send-notification', $data);
        }
    }
    public function NotificateMember($member_id, $data) {
        $member = \App\Member::find($member_id);
        $data['time'] = date("H:i");
        $data['full_name'] = \Auth::guard('member')->user()->full_name;
        $member->notify(new Notificate($data));
        $options = array(
            'cluster' => env('PUSHER_APP_CLUSTER'),
            'encrypted' => true
        );
        $pusher = new Pusher(
                env('PUSHER_APP_KEY') ,
                env('PUSHER_APP_SECRET'),
                env('PUSHER_APP_ID'), $options
        );
        $notification = $member->unreadNotifications()->orderBy('created_at', 'DESC')->first();
        $data['count'] = count($member->unreadNotifications);
        $data['member_id'] = $member->id;
        $data['id'] = $notification->id;
        $pusher->trigger('NotificationEvent', 'send-notification', $data);
    }
    public function NotificateMembers($member_ids, $data) {
        $members = \App\Member::whereIn('id',$member_ids)->get();
        foreach($members as $key=>$member){
            $data['time'] = date("H:i");
            $data['full_name'] = \Auth::guard('member')->user()->full_name;
            $member->notify(new Notificate($data));
            $options = array(
                'cluster' => env('PUSHER_APP_CLUSTER'),
                'encrypted' => true
            );
            $pusher = new Pusher(
                    env('PUSHER_APP_KEY') ,
                    env('PUSHER_APP_SECRET'),
                    env('PUSHER_APP_ID'), $options
            );
            $notification = $member->unreadNotifications()->orderBy('created_at', 'DESC')->first();
            $data['count'] = count($member->unreadNotifications);
            $data['member_id'] = $member->id;
            $data['id'] = $notification->id;
            $pusher->trigger('NotificationEvent', 'send-notification', $data);
        }
    }
    public function rank(){
        $members= DB::table('member')->join('project', 'project.member_id', '=', 'member.id')
                ->select('member.id', DB::raw('count(project.id) as count'))
                ->groupBy('member.id')->orderBy('count','DESC')->get();
        foreach($member as $key=>$val){
            if(\Auth::guard('member')->user()->id == $val->id){
                $position = $key + 1;
            }
        }
        return $position;
    }
    public function updateTeam($member_id,$team_id){
        $this->model->whereNotIn('id',$member_id)->where('team_id',$team_id)->update(['team_id'=>NULL]);
        return $this->model->whereIn('id',$member_id)->update(['team_id'=>$team_id]);
    }
     public function updatePart($member_id,$part_id){
        $this->model->whereNotIn('id',$member_id)->where('part_id',$part_id)->update(['part_id'=>NULL]);
        return $this->model->whereIn('id',$member_id)->update(['part_id'=>$part_id]);
    }
    public function checkID($ID){
        return $this->model->where('login_id',$ID)->first();
    }
    public function searchByKeyword($keyword){
        return $this->model->where('full_name','like','%'.$keyword.'%')->get();
    }
    public function whereArray($ids){
        return $this->model->whereIn('id',$ids);
    }
    public function getByDepartment($department_id,$member_id=''){
        if($member_id==''){
            return $this->model->where('department_id',$department_id)->orderBy('level','DESC')->get();
        }else{
            return $this->model->where('department_id',$department_id)->where('id','<>',$member_id)->orderBy('level','DESC')->get();
        }
    }
    public function searchMember($keyword){
        return $this->model->where('full_name','like','%'.$keyword.'%')->get();
    }
    public function getBySearch($input){
        $query = $this->model;
        if(isset($input['uids']) && $input['uids'] != ''){
            $query = $query->whereIn('id',explode(' ',$input['uids']));
        }
        if(isset($input['search_text']) && $input['search_text']){
            $query = $query->where('full_name','like','%'.$input['search_text'].'%');
        }
        return $query->get();
        
    }
    
}
