<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;

class NotificationScheduleRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }
    public function model() {
        return 'App\NotificationSchedule';
    }
    public function createNotification($content,$from,$to,$link){
        foreach($to as $key=>$val){
            $this->model->create(['content'=>$content,'from'=>$from,'to'=>$val,'link'=>$link]);
        }
    }
    public function getByStatus($status){
        if($status == 0){
            return $this->model->where('read_at',NULL)->where('to',\Auth::guard('member')->user()->id)->get();
        }else{
            return $this->model->where('read_at','<>',NULL)->where('to',\Auth::guard('member')->user()->id)->get();
        }
    }
}
