<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;

class FolderRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model(){
        return 'App\Folder';
    }
    public function getFolderParent(){
        return $this->model->where('parent_id',0)->get();
    }
    public function getAll(){
        return $this->model->where('parent_id',0)->orderBy('ordering','ASC')->get();
    }
}
