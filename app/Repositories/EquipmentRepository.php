<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;

class EquipmentRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model(){
        return 'App\Equipment';
    }
    public function search($keyword){
        return $this->model->where('name','like','%'. $keyword .'%')->get();
    }
}
