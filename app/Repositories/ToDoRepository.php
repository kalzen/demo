<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;

class ToDoRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\ToDo';
    }
    public function validateCreate(){
        return $rules = [
            'title' => 'required',
        ];
    }

    public function validateUpdate($id){
        return $rules = [
            'title' => 'required',
        ];
    }
    public function  getIndex($request){
        $search = $request->all();
        $todo_ids = \Auth::guard('member')->user()->todo->pluck('id')->toArray();
        $query = $this->model->where('created_by',\Auth::guard('member')->user()->id);
        if(count($todo_ids) > 0){
            $query = $query->orWhereIn('id',$todo_ids);
        }
        $data = $query->get();
        return $data;
    }
    public function  getHistory($request){
        $search = $request->all();
        $query = $this->model;
        return $query->where('status',4)->where('created_by',\Auth::guard('member')->user()->id)->get();
    }
    public function getByStatus($status){
        if($status == 'pending'){
            $data = $this->model->whereIn('status',['1','2','3'])->where('created_by',\Auth::guard('member')->user()->id)->get();
        }else{
            $data = $this->model->where('status',4)->where('created_by',\Auth::guard('member')->user()->id)->get();
        }
        return $data;
    }
    public function deleteAll(){
        return $this->model->where('status',1)->delete();
    }
    public function updateList($ids){
        return $this->model->whereIn('id',$ids)->update(['status'=>1]);
    }
    public function deleteList($ids){
        return $this->model->whereIn('id',$ids)->delete();
    }
    public function getListByDate($member_id,$date){
        return $this->model->where('created_by',$member_id)->whereDate('end_date',$date)->where('status',0)->get();
    }
    public function getListByMember($member_id,$date){
        return $this->model->whereIn('created_by',$member_id)->whereDate('end_date',$date)->where('status',0)->get();
    }
}
