<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;
use Illuminate\Support\Facades\Session;

class ProjectRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Project';
    }

    public function validateCreate() {
        return $rules = [
            'title' => 'required|unique:project',
            'alias' => 'required',
        ];
    }

    public function validateUpdate($id) {
        return $rules = [
            'title' => 'required|unique:project,title,' . $id . ',id',
            'alias' => 'required',
        ];
    }
     public function getIndex($limit){
        Session::forget('keyword');
        $query = $this->model->where('is_deleted',0)->where('status','>',0);
        $start = (Session::get('p_page')-1) * $limit;
        Session::put('_p_count',count($query->where('member_id',\Auth::guard('member')->user()->id)->get()));
        Session::put('_p_pageSize',$limit);
        if((Session::get('p_page') * $limit) > Session::get('_p_count')){
            Session::put('_p_pages',Session::get('_p_count'));
        }else{
            Session::put('_p_pages',Session::get('p_page') * $limit);
        }
        $data = $query->where('member_id',\Auth::guard('member')->user()->id)->orderBy('created_at','DESC')->offset($start)->limit($limit)->get();
        return $data;   
    }
    public function getAll(){
        $data = $this->model->get();
        return $data;
        
    }
    public function getList($limit){
        $start = (Session::get('p_page')-1) * $limit;
        $query = $this->model->where('member_id',\Auth::guard('member')->user()->id);
        if((Session::get('keyword'))){
            $keyword = Session::get('keyword');
            if($keyword == 'save'){
                $query = $query->where('is_saved',1);
            }
            if($keyword == 'remove'){
                $query = $query->where('is_deleted',1);
            }
            if($keyword == 'draft'){
                $query = $query->where('status',0);
            }
            if($keyword == 'send'){
                $query = $query->where('status','>',0);
            }
        }
        Session::put('_p_count',count($query->get()));
        Session::put('_p_pageSize',$limit);
        if((Session::get('p_page') * $limit) > Session::get('_p_count')){
            Session::put('_p_pages',Session::get('_p_count'));
        }else{
            Session::put('_p_pages',Session::get('p_page') * $limit);
        }
        $data = $query->orderBy('created_at','DESC')->offset($start)->limit($limit)->get();
        return $data;   
    }
    public function getListProject($limit,$request){
        $start = (Session::get('list_page')-1) * $limit;
        $query = $this->model->where('is_deleted',0)->where('status','>',0);
        if((Session::get('keyword_project'))){
            $keyword = Session::get('keyword_project');
            if($keyword == 'all'){
                Session::forget('keyword_project');
            }
            if($keyword == 'pending'){
                $ids = \App\Member::where('department_id',\Auth::guard('member')->user()->department_id)->where('level','<',\Auth::guard('member')->user()->level)->pluck('id');
                $query = $query->where('status',\Auth::guard('member')->user()->level - 1)->whereIn('member_id',$ids);
                Session::put('keyword_project','save');
            }
            if($keyword == 'save'){
                $query = $query->where('is_saved',1);
                Session::put('keyword_project','save');
            }
            if($keyword == 'remove'){
                $query = $query->where('is_deleted',1);
                Session::put('keyword_project','remove');
            }
            if($keyword == 'draft'){
                $query = $query->where('status',0);
                 Session::put('keyword_project','draft');
            }
            if($keyword == 'send'){
                $query = $query->where('status','>',0);
                Session::put('keyword_project','send');
            }
            if($keyword == 'approved'){
                $query = $query->where('status','=',\App\Project::STATUS_ACTIVE);
                Session::put('keyword_project','approved');
            }
            if($keyword == 'return'){
                $query = $query->where('status','=',\App\Project::STATUS_CANCEL);
                Session::put('keyword_project','return');
            }
            if($keyword == 'save_member'){
                $ids = \App\LogSaved::where('member_id',\Auth::guard('member')->user()->id)->pluck('project_id')->toArray();
                $query = $query->whereIn('id',$ids);
                Session::put('keyword_project','save_member');
            }
        }
        
        if(isset($_GET['keywords'])){
            $query = $query->where('name','like','%'.$_GET['keywords'].'%');
        }
        if(!is_null($request->get('month'))){
            $query = $query->whereMonth('created_at',$request->get('month'))->whereYear('created_at',$request->get('year'));
        }
        Session::put('_list_count',count($query->get()));
        if((Session::get('list_page') * $limit) > Session::get('_list_count')){
            Session::put('_list_pages',Session::get('_list_count'));
        }else{
            Session::put('_list_pages',Session::get('list_page') * $limit);
        }
        $data = $query->orderBy('created_at','DESC')->offset($start)->limit($limit)->get();
        return $data;   
    }
    public function save($project_id){
        return $this->model->whereIn('id',$project_id)->update(['is_saved'=>1]);
    }
    public function send($project_id){
        return $this->model->whereIn('id',$project_id)->update(['status'=>\Auth::guard('member')->user()->level]);
    }
    public function remove($project_id){
        return $this->model->whereIn('id',$project_id)->update(['is_deleted'=>1]);
    }
    public function fillter($keyword,$limit){
        $query = $this->model->where('member_id',\Auth::guard('member')->user()->id);
        if($keyword == 'save'){
            $query = $query->where('is_saved',1);
            Session::put('keyword','save');
        }
        if($keyword == 'remove'){
            $query = $query->where('is_deleted',1);
            Session::put('keyword','remove');
        }else{
            $query = $query->where('is_deleted',0);
        }
        if($keyword == 'draft'){
            $query = $query->where('status',0);
            Session::put('keyword','draft');
        }
        if($keyword == 'send'){
            $query = $query->where('status','>',0);
            Session::put('keyword','send');
        }
        $start = (Session::get('p_page')-1) * $limit;
        Session::put('_p_count',count($query->get()));
        $data = $query->orderBy('created_at','DESC')->offset($start)->limit($limit)->get();
        Session::put('_p_pageSize',$limit);
        if((Session::get('p_page') * $limit) > Session::get('_p_count')){
            Session::put('_p_pages',Session::get('_p_count'));
        }else{
            Session::put('_p_pages',Session::get('p_page') * $limit);
        }
        return $data;
    }
    public function getIndexByMember($limit,$request){
        $start = (Session::get('list_page')-1) * $limit;
        $query = $this->model->where('is_deleted',0)->where('status','>',0);
        $keyword = $request->get('keyword');
        if($keyword == 'unapproved'){
            $query = $query->where('status','<',\App\Project::STATUS_ACTIVE)->whereDate('created_at',date('Y-m-d'));
        }
        if(isset($_GET['keywords'])){
            $query = $query->where('name','like','%'.$_GET['keywords'].'%');
        }
        Session::put('_list_count',count($query->get()));
        if((Session::get('list_page') * $limit) > Session::get('_list_count')){
            Session::put('_list_pages',Session::get('_list_count'));
        }else{
            Session::put('_list_pages',Session::get('list_page') * $limit);
        }
        $data = $query->orderBy('created_at','DESC')->offset($start)->limit($limit)->get();
        return $data;  
    }
    public function getFillterByMember($limit,$request){
        $start = (Session::get('list_page')-1) * $limit;
        $query = $this->model->where('is_deleted',0)->where('status','>',0);
        if(!is_null($request->get('keyword_project'))){
            $keyword = $request->get('keyword_project');
            if($keyword == 'pending'){
                $ids = \App\Member::where('department_id',\Auth::guard('member')->user()->department_id)->where('level','<',\Auth::guard('member')->user()->level)->pluck('id');
                $query = $query->where('status',\Auth::guard('member')->user()->level - 1)->whereIn('member_id',$ids);
                Session::put('keyword_project','pending');
            }
            if($keyword == 'return'){
                $query = $query->where('status', \App\Project::STATUS_CANCEL);
                Session::put('keyword_project','return');
            }
            if($keyword == 'approved'){
                $query = $query->where('status',\App\Project::STATUS_ACTIVE);
                Session::put('keyword_project','approved');
            }
            if($keyword == 'all'){
                Session::forget('keyword_project');
            }
            if($keyword == 'save_member'){
                $ids = \App\LogSaved::where('member_id',\Auth::guard('member')->user()->id)->pluck('project_id')->toArray();
                $query = $query->whereIn('id',$ids);
                Session::put('keyword_project','save_member');
            }
        }
        if(Session::get('search')){
            $search = Session::get('search');
            if(isset($search['full_name'])){
            $member_ids = \App\Member::where('full_name','like','%'.$search['full_name'].'%')->get()->pluck('id')->toArray();
            $query = $query->whereIn('member_id',$member_ids);
            }
            if(isset($search['department_id'])){
                $member_ids = \App\Member::where('department_id',$search['department_id'])->get()->pluck('id')->toArray();
                $query = $query->whereIn('member_id',$member_ids);
            }
            if(isset($search['level'])){
                if($search['level'] == 0){
                    $query = $query->where('level',NULL);
                }else{
                    $query = $query->where('level',$search['level']);
                }
            }
            if(isset($search['status'])){
                $search['status'] = explode(',',$search['status']);
                $query = $query->whereIn('status',$search['status']);
            }
        }
        Session::put('_list_count',count($query->get()));
        //dd(Session::get('_list_count'));
        if((Session::get('list_page') * $limit) > Session::get('_list_count')){
            Session::put('_list_pages',Session::get('_list_count'));
        }else{
            Session::put('_list_pages',Session::get('list_page') * $limit);
        }
        $data = $query->orderBy('created_at','DESC')->offset($start)->limit($limit)->get();
        return $data;  
    }
     public function export($ids){
        return $this->model->whereIn('id',$ids)->get();
    }
    public function search($search){
        Session::put('search',$search);
        $query = $this->model->where('is_deleted',0)->where('status','>',0);
        if((Session::get('keyword_project'))){
            $keyword = Session::get('keyword_project');
            if($keyword == 'all'){
                Session::forget('keyword_project');
            }
            if($keyword == 'pending'){
                $ids = \App\Member::where('department_id',\Auth::guard('member')->user()->department_id)->where('level','<',\Auth::guard('member')->user()->level)->pluck('id');
                $query = $query->where('status',\Auth::guard('member')->user()->level - 1)->whereIn('member_id',$ids);
                Session::put('keyword_project','pending');
            }
            if($keyword == 'save'){
                $query = $query->where('is_saved',1);
                Session::put('keyword_project','save');
            }
            if($keyword == 'remove'){
                $query = $query->where('is_deleted',1);
                Session::put('keyword_project','remove');
            }
            if($keyword == 'draft'){
                $query = $query->where('status',0);
                 Session::put('keyword_project','draft');
            }
            if($keyword == 'send'){
                $query = $query->where('status','>',0);
                Session::put('keyword_project','send');
            }
            if($keyword == 'approved'){
                $query = $query->where('status','=',\App\Project::STATUS_ACTIVE);
                Session::put('keyword_project','approved');
            }
            if($keyword == 'return'){
                $query = $query->where('status','=',\App\Project::STATUS_CANCEL);
                Session::put('keyword_project','return');
            }
            if($keyword == 'save_member'){
                $ids = \App\LogSaved::where('member_id',\Auth::guard('member')->user()->id)->pluck('project_id')->toArray();
                $query = $query->whereIn('id',$ids);
                Session::put('keyword_project','save_member');
            }
        }
        if(isset($search['full_name'])){
            $member_ids = \App\Member::where('full_name','like','%'.$search['full_name'].'%')->get()->pluck('id')->toArray();
            $query = $query->whereIn('member_id',$member_ids);
        }
        if(isset($search['department_id'])){
            $member_ids = \App\Member::where('department_id',$search['department_id'])->get()->pluck('id')->toArray();
            $query = $query->whereIn('member_id',$member_ids);
        }
        if(isset($search['level'])){
            if($search['level'] == 0){
                $query = $query->where('level',NULL);
            }else{
                $query = $query->where('level',$search['level']);
            }
        }
        if(isset($search['status'])){
            $search['status'] = explode(',',$search['status']);
            $query = $query->whereIn('status',$search['status']);
        }
        $start = (Session::get('list_page')-1) * 10;
        $data = $query->orderBy('created_at','DESC')->offset($start)->limit(10)->get();
        Session::put('_list_count',count($query->get()));
         if((Session::get('list_page') * 10) > Session::get('_list_count')){
            Session::put('_list_pages',Session::get('_list_count'));
        }else{
            Session::put('_list_pages',Session::get('list_page') * 10);
        }
        return $data;
    }
}
