<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ToDoCategory extends Model
{
    protected $table='todo_category';
    protected $fillable=['title','member_id'];
    public function member(){
        return $this->belongsTo('\App\Member');
    }
}
