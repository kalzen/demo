<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EquipmentSchedule extends Model
{
    protected $table='equipment_schedule';
    protected $fillable=['equipment_id','schedule_id'];
}
