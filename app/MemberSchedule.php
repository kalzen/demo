<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberSchedule extends Model
{
    protected $table='member_schedule';
    protected $fillable=['member_id','schedule_id'];
}
