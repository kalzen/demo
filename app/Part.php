<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Part extends Model
{
    protected $table='part';
    protected $fillable=['name'];
     public function member(){
        return $this->hasMany('\App\Member');
    }
}
