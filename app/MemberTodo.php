<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberTodo extends Model
{
    protected $table='member_todo';
    protected $fillable=['member_id','todo_id','status','join','reason'];
    public $timestamps = false;
}
