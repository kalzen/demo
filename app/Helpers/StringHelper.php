<?php

namespace App\Helpers;
use DateTime;
class StringHelper {

    public static function getSelectOptions($options, $selected = '') {
        $html = '<option></option>';
        foreach ($options as $option) {
            $html .= '<option value="' . $option->id . '"' . ((is_array($selected) ? in_array($option->id, $selected) : $selected == $option->id) ? 'selected' : '') . '>' . $option->name . '</option>';
        }
        return $html;
    }
    public static function getSelectFolderCabinet($options, $selected = '') {
        $html = '<option value="0">Thư mục cha</option>';
        foreach ($options as $option) {
            $html .= '<option value="' . $option->id . '"' . ((is_array($selected) ? in_array($option->id, $selected) : $selected == $option->id) ? 'selected' : '') . '>' . $option->name . '</option>';
        }
        return $html;
    }
    public static function getSelectHtml($options, $selected = '') {
        $html = '<option></option>';
        foreach ($options as $option) {
            $html .= '<option value="' . $option['id'] . '"' . ((is_array($selected) ? in_array($option['id'], $selected) : $selected == $option['id']) ? 'selected' : '') . '>' . $option['name'] . '</option>';
        }
        return $html;
    }
    public static function getSelectRoleOptions($options, $selected = '') {
        $html = '<option></option>';
        foreach ($options as $option) {
            $html .= '<option value="' . $option->id . '"' . ((is_array($selected) ? in_array($option->id, $selected) : $selected == $option->id) ? 'selected' : '') . '>' . $option->name . '</option>';
        }
        return $html;
    }
    public static function getSelectWorkOptions($options, $selected = '') {
        $html = '';
        foreach ($options as $option) {
            $html .= '<option value="' . $option->name . '"' . ((is_array($selected) ? in_array($option->name, $selected) : $selected == $option->id) ? 'selected' : '') . '>' . $option->name . '</option>';
        }
        return $html;
    }
    public static function getSelectTodoCategoryOptions($options, $selected = '') {
        $html = '';
        foreach ($options as $option) {
            $html .= '<option value="' . $option->id . '"' . ((is_array($selected) ? in_array($option->id, $selected) : $selected == $option->id) ? 'selected' : '') . '>' . $option->title . '</option>';
        }
        return $html;
    }
    public static function getSelectMemberOptions($options, $selected = '') {
        $html = '<option></option>';
        foreach ($options as $option) {
            $html .= '<option value="' . $option->id . '"' . ((is_array($selected) ? in_array($option->id, $selected) : $selected == $option->id) ? 'selected' : '') . '>' . $option->full_name . '</option>';
        }
        return $html;
    }
    public static function getSelectMemberOption($options, $selected = '') {
        $html = '';
        foreach ($options as $option) {
            $html .= '<option value="' . $option->id . '"' . ((is_array($selected) ? in_array($option->id, $selected) : $selected == $option->id) ? 'selected' : '') . '>' . $option->full_name . '</option>';
        }
        return $html;
    }
    public static function getSelectMemberSchedule($options, $selected = '') {
        $html = '';
        foreach ($options as $option) {
            $html .= '<option value="' . $option->id . '" class="member_id"' . ((is_array($selected) ? in_array($option->id, $selected) : $selected == $option->id) ? 'selected' : '') . '>' . $option->full_name . '</option>';
        }
        return $html;
    }
    public static function getSelectOptionsNormal($options, $selected = '') {
        $html = '';
        foreach ($options as $option) {
            $html .= '<option value="' . $option->id . '"' . ((is_array($selected) ? in_array($option->id, $selected) : $selected == $option->id) ? 'selected' : '') . '>' . $option->title . '</option>';
        }
        return $html;
    }

    public static function slug($str) {
        $str = trim(mb_strtolower($str));
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
        $str = preg_replace('/([\s]+)/', '-', $str);
        $str = preg_replace('/---/', '-', $str);
        return $str;
    }

    public static function removeVietnameseSign($str) {
        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
        $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
        $str = preg_replace("/(đ)/", 'd', $str);
        $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
        $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
        $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
        $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
        $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
        $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
        $str = preg_replace("/(Đ)/", 'D', $str);
        return $str;
    }

    public static function getAlias($str) {
        $str = strip_tags($str);
        $str = self::removeVietnameseSign($str);
        $allowed = "/[^A-Za-z0-9- ]/i";
        $str = preg_replace($allowed, '', $str);
        $str = trim($str);
        while (strpos($str, '  ') !== FALSE) {
            $str = str_replace('  ', ' ', $str);
        }
        $str = str_replace(' ', '-', $str);
        while (strpos($str, '--') !== FALSE) {
            $str = str_replace('--', '-', $str);
        }
        $str = strtolower($str);
        return $str;
    }

    public static function getTextInput($name, $value, $options = null) {
        return '
			<div class="form-group col-md-12">
				<input type="text" name="' . $name . '" class="form-control" placeholder="" value="' . $value . '"/>
            </div>';
    }

    public static function getNumberInput($name, $value, $options = null) {
        return '
			<div class="form-group col-md-12">
				<input type="number" name="' . $name . '" class="form-control" placeholder="" value="' . $value . '"/>
            </div>';
    }

    public static function getHtmlInput($name, $value, $options = null) {
        return '
			<div class="form-group col-md-12">
				<textarea class="ckeditor" rows="5" cols="5" name="' . $name . '">' . $value . '</textarea>
			</div>';
    }

    public static function getRadioInput($name, $value, $options = null) {
        return '<div class="form-group col-md-12">
					<label class="radio-inline">
						<input type="radio" name="' . $name . '" class="styled" value="1" ' . ($value ? 'checked' : '') . '>
						Hiển thị
					</label>

					<label class="radio-inline">
						<input type="radio" name="' . $name . '" value="0" class="styled"' . (!$value ? 'checked' : '') . '>
						Ẩn
					</label>
				</div>';
    }

    public static function getColorInput($name, $value, $options = null) {
        return '
			<div class="form-group col-md-12">
                <input type="text"class="form-control colorpicker-show-input" data-preferred-format="hex" name="' . $name . '" value="' . $value . '">
			</div>';
    }

     public static function getImageInput($name,$value,$options=null){
        $guid = '';
        if (!empty($options['guid'])) $guid = $options['guid'];
        $id = $guid.'_'.$name;
        return '			
            <div class="form-group col-md-12">
				<div class="div-image">
					<input type="file" data-guid="'.$guid.'" multiple="" id="'.$id.'"data-value="'.$value.'" data-field="'.$name.'" class="file-input-overwrite" data-show-upload="false" data-show-remove="true" onclick="BrowseServer(\''.$id.'\',\''.$guid.'\')"/>
					<input type="hidden" class="image_data" data-guid="'.$guid.'" value="'.$value.'" name="'.$name.'"/>
                    <span class="help-block">Chỉ cho phép các file ảnh có đuôi <code>jpg</code>, <code>gif</code> và <code>png</code></span>
                </div>
            </div>';
    }
    public static function ago($start_time,$end_time)
        {
            $dt = $end_time->diff($start_time);
            if ($dt->y > 0){
                $number = $dt->y;
                $unit = "Year";
            } else if ($dt->m > 0) {
                $number = $dt->m;
                $unit = "Month";
            } else if ($dt->d > 0) {
                $number = $dt->d;
                $unit = "Day";
            } else if ($dt->h > 0) {
                $number = $dt->h;
                $unit = "Hour";
            } else if ($dt->i > 0) {
                $number = $dt->i;
                $unit = "Minute";
            } else if ($dt->s > 0) {
                $number = $dt->s;
                $unit = "Second";
            }

            $unit .= $number  > 1 ? "s" : "";

            $ret = $number." ".$unit;
            return $ret;
        }
    public static function getSelectListEquipment($options){
        $html = '';
        foreach ($options as $option) {
            $html .= '<li id="selectlist_cITEM_member_facility_'.$option->id.'" class="selectlist_cITEM"
                        data-value="'.$option->id.'" data-approval="0" data-repeat="1" data-ancestors="" data-code="salesevent" data-name="'.$option->name.'">
                        <span class="selectlist_facility_grn"></span>
                        <span class="selectlist_text2_grn">'.$option->name.'</span>
                     </li>';
        }
        return $html;
    }
    public static function getSelectMonth($selected = ''){
        $html = '';
        if($selected == ''){
            $selected = date('m');
        }
        for($i=0;$i<12;$i++){
           $html .= '<option value="'.($i+1).'"' .($selected == $i+1 ? 'selected' : '').'>'.date('M',strtotime(date('Y-01-d').' +'.$i.' months')).'</option>';
        }
        return $html;
    }
    public static function getSelectDay($year = '',$month = '',$selected = ''){
        $html = '';
        if($year != '' && $month != ''){
            $start = date($year.'-'.$month.'-01');
        }else{
            $start = date('Y-m-01');
        }
        $end = date('Y-m-d',strtotime(date($start).' +1 months'));
        $start_date = new DateTime($start);
        $end_date = new DateTime($end);
        $count = date_diff($start_date, $end_date)->days;
        if($selected == ''){
            $selected = date('d');
        }
        for($i=1;$i < $count+1;$i++){
           if($year != '' && $month != ''){
                $html .= '<option value="'.$i.'"' .($selected == $i ? 'selected' : '').'>'.$i.'('.substr(date('l',strtotime(date($year.'-'.$month.'-'.$i))),  0, 3).')</option>';
           }else{
                $html .= '<option value="'.$i.'"' .($selected == $i ? 'selected' : '').'>'.$i.'('.substr(date('l',strtotime(date('Y-m-'.$i))),  0, 3).')</option>';
           }
        }
        return $html;
    }
    public static function getSelectYear($selected = ''){
        $html = '';
        if($selected == ''){
            $selected = date('Y');
        }
        $year = date('Y') - 10;
        for($i=$year ;$i < $year + 21;$i++){
           $html .= '<option value="'.$i.'"' .($selected == $i ? 'selected' : '').'>'.$i.'</option>';
        }
        return $html;
    }
    public static function getSelectHour($selected = ''){
        $html = '<option value="">--</option>';
        for($i=0 ;$i < 24;$i++){
            if($i < 10){
                $name = '0'.$i;
            }else{
                $name = $i;
            }
            $html .= '<option value="'.$i.'"' .($selected == $i && $selected != '' ? 'selected' : '').'>'.$name.'</option>';
        }
        return $html;
    }
    public static function getSelectMinute($selected = ''){
        $html = '<option value="">--</option>';
        for($i=0 ;$i < 60;$i = $i+5){
            if($i < 10){
                $name = '0'.$i;
            }else{
                $name = $i;
            }
            $html .= '<option value="'.$i.'"' .($selected == $i && $selected != '' ? 'selected' : '').'>'.$name.'</option>';
        }
        return $html;
    }
    public static function ConvertSizeFile($bytes){
        $bytes = floatval($bytes);
            $arBytes = array(
                0 => array(
                    "UNIT" => "TB",
                    "VALUE" => pow(1024, 4)
                ),
                1 => array(
                    "UNIT" => "GB",
                    "VALUE" => pow(1024, 3)
                ),
                2 => array(
                    "UNIT" => "MB",
                    "VALUE" => pow(1024, 2)
                ),
                3 => array(
                    "UNIT" => "KB",
                    "VALUE" => 1024
                ),
                4 => array(
                    "UNIT" => "B",
                    "VALUE" => 1
                ),
            );

        foreach($arBytes as $arItem)
        {
            if($bytes >= $arItem["VALUE"])
            {
                $result = $bytes / $arItem["VALUE"];
                $result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
                break;
            }
        }
        return $result;
    }
    public static function time_ago($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);
    
        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;
    
        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }
    
        if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

}
