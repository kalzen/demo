<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cabinet extends Model
{
    protected $table = 'cabinet';
    protected $fillable = [
        'subject','created_by','file_name','update_time','size','folder_id','link','max_version','updater','memo','updater','is_deleted'
    ];
    public function member(){
        return $this->belongsTo('\App\Member','created_by','id');
    }
    public function updaters(){
        return $this->belongsTo('\App\Member','updater','id');
    }
    public function folder(){
        return $this->belongsTo('\App\Folder','folder_id','id');
    }
    
}
