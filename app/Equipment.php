<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
    protected $table='equipment';
    protected $fillable=['name'];
    public function schedule() {
        return $this->belongsToMany('\App\Schedule', 'equipment_schedule', 'equipment_id', 'schedule_id');
    }
}
