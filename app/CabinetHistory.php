<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CabinetHistory extends Model
{
    protected $table = 'cabinet_history';
    protected $fillable = [
        'cabinet_id','update_time','updater','file_name','action','comment'
    ];
    public function member(){
        return $this->belongsTo('\App\Member','created_by','id');
    }
}
